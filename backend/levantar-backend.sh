cd app
mvn flyway:migrate -Dflyway.configFiles=flywayConfig/default.conf
java -Dserver.port=$PORT -Xmx300m -Xss512k -XX:CICompilerCount=2 -Dfile.encoding=UTF-8 -jar ./target/futpal-0.0.1-SNAPSHOT.jar