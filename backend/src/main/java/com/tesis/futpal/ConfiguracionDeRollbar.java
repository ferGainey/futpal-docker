package com.tesis.futpal;

import com.rollbar.notifier.Rollbar;
import com.rollbar.spring.webmvc.RollbarSpringConfigBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@Configuration()
@EnableWebMvc
@ComponentScan({
        "com.tesis.futpal",
        "com.rollbar.spring"
})
public class ConfiguracionDeRollbar {

    @Value("${spring.rollbar.accesstoken}")
    private String accessToken;

    /**
     * Register a Rollbar bean to configure App with Rollbar.
     */
    @Bean
    public Rollbar rollbar() {
        return new Rollbar(RollbarSpringConfigBuilder
                .withAccessToken(accessToken).build());
    }
}