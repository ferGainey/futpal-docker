package com.tesis.futpal.controladores;

import com.tesis.futpal.excepciones.JugadorYaTransferidoException;
import com.tesis.futpal.comunicacion.requests.transferencia.RequestCargaTransferencia;
import com.tesis.futpal.modelos.transferencia.DetalleTransferencia;
import com.tesis.futpal.servicios.ServicioTransferencia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@CrossOrigin
public class ControladorTransferencias {

    @Autowired
    private ServicioTransferencia servicioTransferencia;

    @RequestMapping(value = "/api/transferencia", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity cargarTransferencia(@RequestBody RequestCargaTransferencia requestCargaTransferencia) {
        try {
            servicioTransferencia.cargarTransferencia(requestCargaTransferencia);
            return ResponseEntity.ok().build();
        } catch (JugadorYaTransferidoException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(value = "/api/mis-transferencias", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<DetalleTransferencia>> obtenerTransferenciasDeUnEquipo() {
        List<DetalleTransferencia> detallesTransferencias = servicioTransferencia.obtenerTransferenciasDeUnEquipo();
        return ResponseEntity.ok(detallesTransferencias);
    }

    @RequestMapping(value = "/api/transferencia/rechazar", method = RequestMethod.PATCH)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<Void> rechazarTransferencia(@RequestBody Integer idTransferencia) {
        servicioTransferencia.rechazarTransferencia(idTransferencia);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/transferencia/confirmar", method = RequestMethod.PATCH)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<Void> confirmarTransferencia(@RequestBody Integer idTransferencia) {
        servicioTransferencia.confirmarTransferencia(idTransferencia);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/todas-las-transferencias-confirmadas", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<DetalleTransferencia>> obtenerTransferenciasDeTodosLosEquipos(@Valid @RequestParam Integer pagina) {
        List<DetalleTransferencia> detallesTransferencias = servicioTransferencia.obtenerTransferenciasConfirmadasDeTodosLosEquipos(pagina);
        return ResponseEntity.ok(detallesTransferencias);
    }

    @RequestMapping(value = "/api/mercado/estado", method = RequestMethod.PATCH)
    @PreAuthorize("hasAuthority('ROL_ADMINISTRADOR')")
    public ResponseEntity<Boolean> cambiarEstadoDelMercado(@RequestBody Boolean mercadoActivo) {
        boolean mercadoActivoActualizado = servicioTransferencia.cambiarEstadoDeMercado(mercadoActivo);
        return ResponseEntity.ok(mercadoActivoActualizado);
    }

    @RequestMapping(value = "/api/mercado/estado", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<Boolean> obtenerEstadoDelMercado() {
        boolean mercadoActivo = servicioTransferencia.obtenerEstadoDeMercado();
        return ResponseEntity.ok(mercadoActivo);
    }
}
