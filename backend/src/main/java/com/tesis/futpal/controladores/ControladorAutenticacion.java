package com.tesis.futpal.controladores;

import com.tesis.futpal.excepciones.UsuarioExistenteException;
import com.tesis.futpal.comunicacion.requests.RequestIngresar;
import com.tesis.futpal.comunicacion.requests.RequestRegistrarUsuario;
import com.tesis.futpal.comunicacion.responses.ResponseJwt;
import com.tesis.futpal.seguridad.jwt.JwtUtils;
import com.tesis.futpal.seguridad.servicios.UserDetailsImpl;
import com.tesis.futpal.servicios.ServicioRegistrarUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@CrossOrigin
public class ControladorAutenticacion {

    @Autowired
    private ServicioRegistrarUsuario servicioRegistrarUsuario;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/api/ingresar")
    public ResponseEntity<?> ingresar(@Valid @RequestBody RequestIngresar loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getAlias(), loginRequest.getContrasenia()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generarTokenJwt(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(new ResponseJwt(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles));
    }

    @RequestMapping(value = "/api/registrar-usuario", method = RequestMethod.POST)
    public ResponseEntity registrarUsuario(@RequestBody RequestRegistrarUsuario requestRegistrarUsuario) {
        try {
            servicioRegistrarUsuario.registrarUsuario(requestRegistrarUsuario);
            return ResponseEntity.ok().build();
        }
        catch (UsuarioExistenteException usuarioExistenteException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(usuarioExistenteException.getMessage());
        }
        catch (Exception exception) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
        }

    }

}
