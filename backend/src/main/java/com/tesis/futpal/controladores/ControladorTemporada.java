package com.tesis.futpal.controladores;

import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.servicios.ServicioTemporada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@CrossOrigin
public class ControladorTemporada {

    @Autowired
    private ServicioTemporada servicioTemporada;

    @RequestMapping(value = "/api/temporada", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<Temporada> obtenerTemporadaActual() {
        return ResponseEntity.ok(servicioTemporada.obtenerTemporadaActual());
    }

    @RequestMapping(value = "/api/temporada/avanzar", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROL_ADMINISTRADOR')")
    public ResponseEntity avanzarTemporada() {
        servicioTemporada.avanzarTemporada();
        return ResponseEntity.ok().build();
    }
}
