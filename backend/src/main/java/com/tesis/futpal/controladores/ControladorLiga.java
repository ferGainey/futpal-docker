package com.tesis.futpal.controladores;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.excepciones.LigaInexistenteException;
import com.tesis.futpal.excepciones.PartidoInexistenteException;
import com.tesis.futpal.modelos.liga.Liga;
import com.tesis.futpal.modelos.liga.PartidoLigaBase;
import com.tesis.futpal.modelos.liga.PosicionLiga;
import com.tesis.futpal.comunicacion.requests.RequestActualizarResultado;
import com.tesis.futpal.comunicacion.requests.RequestRegistrarLiga;
import com.tesis.futpal.servicios.ServicioLiga;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
public class ControladorLiga {

    @Autowired
    private ServicioLiga servicioLiga;

    @RequestMapping(value = "/api/registrar-liga", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROL_ADMINISTRADOR')")
    public ResponseEntity<Liga> registrarLiga(@Valid @RequestBody RequestRegistrarLiga requestRegistrarLiga) {
        Liga liga = servicioLiga.registrarLiga(requestRegistrarLiga);
        return ResponseEntity.ok(liga);
    }

    @RequestMapping(value = "/api/obtener-ligas-disponibles", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<Liga>> obtenerLigasDisponibles() {
        List<Liga> ligasDisponibles = servicioLiga.obtenerLigasDisponibles();
        return ResponseEntity.ok(ligasDisponibles);
    }

    @RequestMapping(value = "/api/obtener-equipos-liga", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity obtenerEquipos(@Valid @RequestParam Integer idLiga) {
        try {
            List<Equipo> equiposLiga = servicioLiga.obtenerEquipos(idLiga);
            return ResponseEntity.ok(equiposLiga);
        } catch (LigaInexistenteException ligaInexistenteException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ligaInexistenteException.getMessage());
        }
    }

    @RequestMapping(value = "/api/obtener-partidos-liga", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<PartidoLigaBase>> obtenerPartidos(Integer idLiga) {
        List<PartidoLigaBase> partidosLiga = servicioLiga.obtenerPartidos(idLiga);
        return ResponseEntity.ok(partidosLiga);
    }

    @RequestMapping(value = "/api/actualizar-partido-liga", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity actualizarResultado(@Valid @RequestBody RequestActualizarResultado requestActualizarResultado) {
        try {
            servicioLiga.actualizarResultado(requestActualizarResultado);
            return ResponseEntity.ok().build();
        } catch (PartidoInexistenteException partidoInexistenteException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(partidoInexistenteException.getMessage());
        }
    }

    @RequestMapping(value = "/api/obtener-posiciones-liga", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity obtenerPosicionesLiga(Integer idLiga) {
        try {
            List<PosicionLiga> posicionesLiga = servicioLiga.obtenerPosiciones(idLiga);
            return ResponseEntity.ok(posicionesLiga);
        } catch (LigaInexistenteException ligaInexistenteException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(ligaInexistenteException.getMessage());
        }
    }
}
