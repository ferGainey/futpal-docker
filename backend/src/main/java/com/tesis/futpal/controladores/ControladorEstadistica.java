package com.tesis.futpal.controladores;

import com.tesis.futpal.comunicacion.requests.RequestActualizarEstadisticas;
import com.tesis.futpal.comunicacion.requests.RequestObtenerEstadisticasPartido;
import com.tesis.futpal.comunicacion.responses.ResponseObtenerEstadisticasLiga;
import com.tesis.futpal.comunicacion.responses.ResponseObtenerEstadisticasPartido;
import com.tesis.futpal.servicios.ServicioEstadistica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@CrossOrigin
public class ControladorEstadistica {
    @Autowired
    private ServicioEstadistica servicioEstadistica;

    @RequestMapping(value = "/api/actualizar-estadistica", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity actualizarEstadisticas(@RequestBody RequestActualizarEstadisticas requestActualizarEstadisticas) {
        servicioEstadistica.actualizarEstadisticas(requestActualizarEstadisticas);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/obtener-estadisticas-partido", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity obtenerEstadisticas(RequestObtenerEstadisticasPartido requestObtenerEstadisticasPartido) {
        ResponseObtenerEstadisticasPartido responseObtenerEstadisticasPartido = servicioEstadistica.obtenerEstadisticas(requestObtenerEstadisticasPartido);
        return ResponseEntity.ok(responseObtenerEstadisticasPartido);
    }

    @RequestMapping(value = "/api/obtener-estadisticas-liga", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity obtenerEstadisticasLiga(Integer ligaId) {
        ResponseObtenerEstadisticasLiga responseObtenerEstadisticasLiga = servicioEstadistica.obtenerEstadisticasLiga(ligaId);
        return ResponseEntity.ok(responseObtenerEstadisticasLiga);
    }
}
