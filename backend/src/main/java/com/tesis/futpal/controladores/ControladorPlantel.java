package com.tesis.futpal.controladores;

import com.tesis.futpal.comunicacion.requests.*;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.plantel.JugadorFiltrado;
import com.tesis.futpal.modelos.plantel.JugadorPrestado;
import com.tesis.futpal.servicios.ServicioPlantel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@CrossOrigin
public class ControladorPlantel {

    @Autowired
    private ServicioPlantel servicioPlantel;

    @RequestMapping(value = "/api/agregar-jugador", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity agregarJugador(@Valid @RequestBody RequestAgregarJugador requestAgregarJugador) {
        servicioPlantel.agregarJugador(requestAgregarJugador);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/jugadores", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<Jugador>> obtenerJugadores(RequestObtenerJugadoresEquipo requestObtenerJugadoresEquipo) {
        List<Jugador> jugadores = servicioPlantel.obtenerJugadoresEquipo(requestObtenerJugadoresEquipo);
        return ResponseEntity.ok(jugadores);
    }

    @RequestMapping(value = "/api/borrar-jugador", method = RequestMethod.DELETE)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity borrarJugador(@RequestBody RequestBorrarJugador requestBorrarJugador) {
        servicioPlantel.borrarJugador(requestBorrarJugador.getJugadorId());
        return ResponseEntity.ok().build();
    }

    //ToDo: agregar validación de que el usuario solo pueda editar en ciertas circunstancias
    @RequestMapping(value = "/api/editar-jugador", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity editarJugador(@RequestBody RequestEdicionJugador requestEditarJugador) {
        servicioPlantel.editarJugador(requestEditarJugador);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/datos-jugador", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<Jugador> obtenerDatosJugador(RequestDatosJugador requestDatosJugador) {
        Jugador datosJugador = servicioPlantel.obtenerDatosJugador(requestDatosJugador.getJugadorId());
        return ResponseEntity.ok(datosJugador);
    }

    @RequestMapping(value = "/api/jugadoresPrestados", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<JugadorPrestado>> obtenerJugadoresPrestados(@RequestParam Integer equipoId) {
        List<JugadorPrestado> jugadores = servicioPlantel.obtenerJugadoresPrestados(equipoId);
        return ResponseEntity.ok(jugadores);
    }

    @RequestMapping(value = "/api/jugadoresDuenioDePase", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<Jugador>> obtenerJugadoresDuenioDePase(@RequestParam Integer equipoId) {
        List<Jugador> jugadores = servicioPlantel.obtenerJugadoresDuenioDePase(equipoId);
        return ResponseEntity.ok(jugadores);
    }

    @RequestMapping(value = "/api/jugadoresFiltrados", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<JugadorFiltrado>> obtenerJugadoresFiltrados(@RequestParam(required = false) Integer transfermarktId,
                                                                           @RequestParam(required = false) String nombre) {
        List<JugadorFiltrado> jugadores = servicioPlantel.obtenerJugadoresFiltrados(transfermarktId, nombre);
        return ResponseEntity.ok(jugadores);
    }
}
