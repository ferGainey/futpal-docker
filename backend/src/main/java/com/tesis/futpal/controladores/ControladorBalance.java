package com.tesis.futpal.controladores;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.balance.BalanceSalario;
import com.tesis.futpal.modelos.balance.DetalleBalance;
import com.tesis.futpal.excepciones.LigaInexistenteException;
import com.tesis.futpal.comunicacion.requests.balance.RequestCargaBalanceUnEquipo;
import com.tesis.futpal.servicios.ServicioBalance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@CrossOrigin
public class ControladorBalance {

    @Autowired
    private ServicioBalance servicioBalance;

    @RequestMapping(value = "/api/balance/cargar", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROL_ADMINISTRADOR')")
    public ResponseEntity<Void> cargarBalanceParaUnEquipo(@RequestBody RequestCargaBalanceUnEquipo requestCargaBalanceUnEquipo) {
        servicioBalance.cargarBalance(requestCargaBalanceUnEquipo.crearBalance());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/balance/cargar-para-liga", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROL_ADMINISTRADOR')")
    public ResponseEntity cargarBalanceParaUnaLiga(@RequestBody List<RequestCargaBalanceUnEquipo> balances) {
        List<Balance> balancesLigaParaCarga = balances.stream().map(RequestCargaBalanceUnEquipo::crearBalance).collect(Collectors.toList());
        servicioBalance.cargarBalanceParaUnaLiga(balancesLigaParaCarga);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/obtener-balances-todos-los-equipos", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<DetalleBalance>> obtenerDetalleBalanceDeTodosLosEquipos(Integer numeroTemporada) {
        List<DetalleBalance> detallesBalances = servicioBalance.obtenerDetalleBalanceDeTodosLosEquipos(numeroTemporada);
        return ResponseEntity.ok(detallesBalances);
    }

    @RequestMapping(value = "/api/obtener-detalle-balances-un-equipo", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<DetalleBalance> obtenerDetalleBalanceDeUnEquipo(Integer numeroTemporada, Integer equipoId) {
        DetalleBalance detalleBalance = servicioBalance.obtenerDetalleBalanceDeUnEquipo(numeroTemporada, equipoId);
        return ResponseEntity.ok(detalleBalance);
    }

    @RequestMapping(value = "/api/obtener-balances-un-equipo", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<Balance>> obtenerBalancesDeUnEquipoEnTemporada(Integer numeroTemporada, Integer equipoId) {
        List<Balance> balancesDelEquipo = servicioBalance.obtenerBalancesDeUnEquipoEnTemporada(numeroTemporada, equipoId);
        return ResponseEntity.ok(balancesDelEquipo);
    }

    @RequestMapping(value = "/api/obtener-salarios-liga", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<BalanceSalario>> obtenerSalariosLiga(Integer idLiga) throws LigaInexistenteException {
        List<BalanceSalario> balancesConSalariosLiga = servicioBalance.obtenerSalariosDeLaLiga(idLiga);
        return ResponseEntity.ok(balancesConSalariosLiga);
    }
}
