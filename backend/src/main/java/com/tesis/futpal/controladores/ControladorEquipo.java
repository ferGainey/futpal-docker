package com.tesis.futpal.controladores;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.comunicacion.requests.RequestAprobarEquipo;
import com.tesis.futpal.comunicacion.requests.RequestRechazarEquipo;
import com.tesis.futpal.comunicacion.requests.RequestSolicitarAprobacionEquipo;
import com.tesis.futpal.servicios.ServicioEquipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@CrossOrigin
public class ControladorEquipo {

    @Autowired
    private ServicioEquipo servicioEquipo;

    @RequestMapping(value = "/api/obtener-equipos-aprobados", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<Equipo>> obtenerTodosLosEquiposRegistradosAprobados() {
        List<Equipo> equiposAprobados = servicioEquipo.obtenerTodosLosEquiposAprobados();
        return ResponseEntity.ok(equiposAprobados);
    }

    @RequestMapping(value = "/api/obtener-equipos-pendiente-aprobacion", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity<List<Equipo>> obtenerTodosLosEquiposPendienteAprobacion() {
        List<Equipo> equiposPendienteAprobacion = servicioEquipo.obtenerTodosLosEquiposPendienteAprobacion();
        return ResponseEntity.ok(equiposPendienteAprobacion);
    }

    @RequestMapping(value = "/api/aprobar-equipo-pendiente-aprobacion", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ROL_ADMINISTRADOR')")
    public ResponseEntity aprobarEquipo(@RequestBody RequestAprobarEquipo requestAprobarEquipo) {
        servicioEquipo.aprobarEquipo(requestAprobarEquipo.getEquipoId());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/rechazar-equipo-pendiente-aprobacion", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ROL_ADMINISTRADOR')")
    public ResponseEntity rechazarEquipo(@RequestBody RequestRechazarEquipo requestRechazarEquipo) {
        servicioEquipo.rechazarEquipo(requestRechazarEquipo);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/solicitar-aprobacion-equipo", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity solicitarAprobacionEquipo(@RequestBody RequestSolicitarAprobacionEquipo requestSolicitarAprobacionEquipo) {
        servicioEquipo.solicitarAprobacionEquipo(requestSolicitarAprobacionEquipo.getEquipoId());
        return ResponseEntity.ok().build();
    }
}
