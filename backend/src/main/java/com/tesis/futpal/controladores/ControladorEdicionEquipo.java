package com.tesis.futpal.controladores;

import com.tesis.futpal.excepciones.UsuarioInexistenteException;
import com.tesis.futpal.comunicacion.requests.RequestEditarDatosEquipo;
import com.tesis.futpal.comunicacion.responses.ResponseDatosEquipo;
import com.tesis.futpal.comunicacion.responses.ResponseEquipoActualizado;
import com.tesis.futpal.servicios.ServicioEdicionEquipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
public class ControladorEdicionEquipo {

    @Autowired
    private ServicioEdicionEquipo servicioEdicionEquipo;

    @RequestMapping(value = "/api/datos-equipo", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity obtenerDatosEquipo(@Valid @RequestParam Long idUsuario) {
        try {
            ResponseDatosEquipo responseDatosEquipo = servicioEdicionEquipo.obtenerDatosEquipo(idUsuario);
            return ResponseEntity.ok(responseDatosEquipo);
        }
        catch (UsuarioInexistenteException exception) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(exception.getMessage());
        }
    }

    //ToDo: separar este método en crear y actualizar, así después separo en PUT y POST
    @RequestMapping(value = "/api/editar-equipo", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROL_USUARIO')")
    public ResponseEntity actualizarDatosEquipo(@Valid @RequestBody RequestEditarDatosEquipo requestEditarDatosEquipo) {
        try {
            ResponseEquipoActualizado responseEquipoActualizado = servicioEdicionEquipo.actualizarEquipo(requestEditarDatosEquipo);
            return ResponseEntity.ok(responseEquipoActualizado);
        }
        catch (UsuarioInexistenteException exception) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(exception.getMessage());
        }
    }
}
