package com.tesis.futpal.modelos.transferencia;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.EstadoJugador;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.repositorios.RepositorioJugador;

import javax.persistence.*;

@Entity
public class MovimientoJugador {

    private static final Long ID_ESTADO_JUGADOR_DISPONIBLE = 1L;
    private static final Integer ID_TIPO_TRANSFERENCIA_TRASPASO = 1;
    private static final Integer ID_TIPO_TRANSFERENCIA_PRESTAMO = 2;
    private static final Long ID_ESTADO_JUGADOR_PRESTADO = 2L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @ManyToOne
    private Jugador jugador;

    private Integer numeroTemporada;

    @ManyToOne
    private PeriodoTemporada periodo;

    @ManyToOne
    private Equipo equipoOrigen;

    @ManyToOne
    private Equipo equipoDestino;

    @ManyToOne
    @JsonBackReference
    private Transferencia transferencia;

    @ManyToOne
    private TipoMovimiento tipoMovimiento;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "movimientoJugador")
    @JsonManagedReference
    @JsonIgnore
    private PrestamoJugador prestamoJugador;


    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Integer getNumeroTemporada() {
        return numeroTemporada;
    }

    public void setNumeroTemporada(Integer temporada) {
        this.numeroTemporada = temporada;
    }

    public Equipo getEquipoOrigen() {
        return equipoOrigen;
    }

    public void setEquipoOrigen(Equipo equipoOrigen) {
        this.equipoOrigen = equipoOrigen;
    }

    public Equipo getEquipoDestino() {
        return equipoDestino;
    }

    public void setEquipoDestino(Equipo equipoDestino) {
        this.equipoDestino = equipoDestino;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Transferencia getTransferencia() {
        return transferencia;
    }

    public void setTransferencia(Transferencia transferencia) {
        this.transferencia = transferencia;
    }

    public PeriodoTemporada getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodoTemporada periodoTemporada) {
        this.periodo = periodoTemporada;
    }

    public void ejecutar(RepositorioJugador repositorioJugador) {
        Jugador jugador = repositorioJugador.findById(this.jugador.getId()).get();
        jugador.setEquipo(this.equipoDestino);
        cambiarElEstadoAlJugador(jugador);

        repositorioJugador.save(jugador);
    }

    private void cambiarElEstadoAlJugador(Jugador jugador) {
        // ToDo: extraer lógica de generación usando algún patrón
        if (this.getTipoMovimiento().getId().equals(ID_TIPO_TRANSFERENCIA_TRASPASO)) {
            EstadoJugador estadoJugador = new EstadoJugador();
            estadoJugador.setId(ID_ESTADO_JUGADOR_DISPONIBLE);
            jugador.setEstadoJugador(estadoJugador);
        }
        else if (this.getTipoMovimiento().getId().equals(ID_TIPO_TRANSFERENCIA_PRESTAMO)) {
            cambiarElEstadoDeUnJugadorPrestado(jugador);
        }
    }

    private void cambiarElEstadoDeUnJugadorPrestado(Jugador jugador) {
        EstadoJugador estadoJugador = new EstadoJugador();

        if (prestamoJugador.isEsFinDePrestamo()) {
            estadoJugador.setId(ID_ESTADO_JUGADOR_DISPONIBLE);
            jugador.setEstadoJugador(estadoJugador);
        }
        else {
            estadoJugador.setId(ID_ESTADO_JUGADOR_PRESTADO);
            jugador.setEstadoJugador(estadoJugador);
        }
    }

    public TipoMovimiento getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(TipoMovimiento tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public PrestamoJugador getPrestamoJugador() {
        return prestamoJugador;
    }

    public void setPrestamoJugador(PrestamoJugador prestamoJugador) {
        this.prestamoJugador = prestamoJugador;
    }
}
