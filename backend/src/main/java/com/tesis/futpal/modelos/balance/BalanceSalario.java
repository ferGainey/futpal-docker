package com.tesis.futpal.modelos.balance;

public class BalanceSalario {

    private Integer idEquipo;
    private String nombreEquipo;
    private Integer monto;

    public static BalanceSalario crearAPartirDe(Balance balance) {
        BalanceSalario balanceSalario = new BalanceSalario();
        balanceSalario.setIdEquipo(balance.getEquipo().getId().intValue());
        balanceSalario.setNombreEquipo(balance.getEquipo().getNombreEquipo());
        balanceSalario.setMonto(balance.getMonto());

        return balanceSalario;
    }

    public Integer getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Integer idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Integer getMonto() {
        return monto;
    }

}
