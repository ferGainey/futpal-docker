package com.tesis.futpal.modelos.transferencia;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.servicios.ServicioPlantel;

import javax.persistence.*;
import java.util.stream.Collectors;

@Entity
public class PrestamoJugador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @OneToOne
    @JsonBackReference
    private MovimientoJugador movimientoJugador;

    private boolean esFinDePrestamo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MovimientoJugador getMovimientoJugador() {
        return movimientoJugador;
    }

    public void setMovimientoJugador(MovimientoJugador movimientoJugador) {
        this.movimientoJugador = movimientoJugador;
    }

    public boolean isEsFinDePrestamo() {
        return esFinDePrestamo;
    }

    public void setEsFinDePrestamo(boolean esFinDePrestamo) {
        this.esFinDePrestamo = esFinDePrestamo;
    }

    public static PrestamoJugador crearAPartirDe(MovimientoJugador movimientoJugador, ServicioPlantel servicioPlantel) {
        PrestamoJugador prestamoJugador = new PrestamoJugador();
        prestamoJugador.setMovimientoJugador(movimientoJugador);

        int jugadoresEncontrados = (int) (int) servicioPlantel.obtenerJugadoresDuenioDePase(movimientoJugador.getEquipoOrigen().getId().intValue())
                .stream()
                .filter(jugador -> jugador.getId().equals(movimientoJugador.getJugador().getId())).count();
        prestamoJugador.setEsFinDePrestamo(jugadoresEncontrados == 0);

        return prestamoJugador;
    }
}
