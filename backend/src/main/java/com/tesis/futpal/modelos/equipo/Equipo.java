package com.tesis.futpal.modelos.equipo;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tesis.futpal.modelos.plantel.Jugador;

import javax.persistence.*;
import java.util.List;

@Entity
public class Equipo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    private Long idUsuario;
    private String nombreEquipo;

    @ManyToOne
    private EstadoEquipo estadoEquipo;

    @OneToMany(fetch = FetchType.EAGER)
    @JsonManagedReference
    @JoinColumn(name="equipo_id")
    private List<Jugador> jugadores;

    private String motivoUltimoCambioEstado;

    public Equipo() {

    }

    public Equipo(Long equipoId) {
        this.id = equipoId;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public void setEstadoEquipo(EstadoEquipo estado) {
        this.estadoEquipo = estado;
    }

    public EstadoEquipo getEstadoEquipo() {
        return estadoEquipo;
    }

    public List<Jugador> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<Jugador> jugadores) {
        this.jugadores = jugadores;
    }

    public void setMotivoUltimoCambioEstado(String motivoUltimoCambioEstado) {
        this.motivoUltimoCambioEstado = motivoUltimoCambioEstado;
    }

    public String getMotivoUltimoCambioEstado() {
        return motivoUltimoCambioEstado;
    }
}
