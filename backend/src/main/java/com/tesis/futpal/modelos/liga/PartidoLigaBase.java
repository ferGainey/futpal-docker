package com.tesis.futpal.modelos.liga;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.usuario.Usuario;

import javax.persistence.*;

@Entity
public class PartidoLigaBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @ManyToOne
    private Liga liga;

    @ManyToOne
    private Equipo equipoLocal;

    @ManyToOne
    private Equipo equipoVisitante;

    @ManyToOne
    private EstadoPartido estadoPartido;

    private Integer golesEquipoLocal;
    private Integer golesEquipoVisitante;
    private Integer numeroFecha;

    //último usuario editor
    @ManyToOne
    private Usuario usuario;

    public PartidoLigaBase(){}

    public PartidoLigaBase(Integer id){
        this.id = id;
    }

    public Integer getGolesEquipoLocal() {
        return golesEquipoLocal;
    }

    public void setGolesEquipoLocal(Integer golesEquipoLocal) {
        this.golesEquipoLocal = golesEquipoLocal;
    }

    public Integer getGolesEquipoVisitante() {
        return golesEquipoVisitante;
    }

    public void setGolesEquipoVisitante(Integer golesEquipoVisitante) {
        this.golesEquipoVisitante = golesEquipoVisitante;
    }

    public Equipo getEquipoLocal() {
        return equipoLocal;
    }

    public void setEquipoLocal(Equipo equipoLocal) {
        this.equipoLocal = equipoLocal;
    }

    public Equipo getEquipoVisitante() {
        return equipoVisitante;
    }

    public void setEquipoVisitante(Equipo equipoVisitante) {
        this.equipoVisitante = equipoVisitante;
    }

    public EstadoPartido getEstadoPartido() {
        return estadoPartido;
    }

    public void setEstadoPartido(EstadoPartido estadoPartido) {
        this.estadoPartido = estadoPartido;
    }

    public Integer getNumeroFecha() {
        return numeroFecha;
    }

    public void setNumeroFecha(Integer numeroFecha) {
        this.numeroFecha = numeroFecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Liga getLiga() {
        return liga;
    }

    public void setLiga(Liga liga) {
        this.liga = liga;
    }

    public Usuario getUltimoUsuarioEditor() {
        return usuario;
    }

    public void setUltimoUsuarioEditor(Usuario ultimoUsuarioEditor) {
        this.usuario = ultimoUsuarioEditor;
    }
}
