package com.tesis.futpal.modelos.transferencia;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Mercado {

    @Id
    private String caracteristica;

    private boolean activo;

    public String getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
