package com.tesis.futpal.modelos.temporada;

import javax.persistence.*;

@Entity
public class Temporada {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    private Integer numero;

    @ManyToOne
    private PeriodoTemporada periodoTemporada;
    private boolean actual;

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public PeriodoTemporada getPeriodoTemporada() {
        return periodoTemporada;
    }

    public void setPeriodoTemporada(PeriodoTemporada periodoTemporada) {
        this.periodoTemporada = periodoTemporada;
    }

    public boolean isActual() {
        return actual;
    }

    public void setActual(boolean actual) {
        this.actual = actual;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
