package com.tesis.futpal.modelos.estadisticas;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.liga.PartidoLigaBase;

import javax.persistence.*;

@Entity
public class EstadisticaTarjetaRoja {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @ManyToOne
    @JsonBackReference
    private Jugador jugador;

    @ManyToOne
    @JsonBackReference
    private Equipo equipo;

    @ManyToOne
    private PartidoLigaBase partido;

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public PartidoLigaBase getPartido() {
        return partido;
    }

    public void setPartido(PartidoLigaBase partido) {
        this.partido = partido;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
