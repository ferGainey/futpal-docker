package com.tesis.futpal.modelos.balance;

import com.tesis.futpal.modelos.equipo.Equipo;

public class DetalleBalance {

    private static final Integer ID_PERIODO_PRINCIPIO = 1;
    private static final Integer ID_PERIODO_MITAD = 2;
    private static final Integer ID_PERIODO_FINAL = 3;

    private Integer totalPeriodoPrincipio;
    private Integer totalPeriodoMitad;
    private Integer totalPeriodoFinal;
    private Integer gastosPeriodoPrincipio;
    private Integer gastosPeriodoMitad;
    private Integer gastosPeriodoFinal;
    private Integer ingresosPeriodoPrincipio;
    private Integer ingresosPeriodoMitad;
    private Integer ingresosPeriodoFinal;
    private Equipo equipo;

    public DetalleBalance() {
        this.totalPeriodoPrincipio = 0;
        this.totalPeriodoMitad = 0;
        this.totalPeriodoFinal = 0;

        this.gastosPeriodoPrincipio = 0;
        this.gastosPeriodoMitad = 0;
        this.gastosPeriodoFinal = 0;

        this.ingresosPeriodoPrincipio = 0;
        this.ingresosPeriodoMitad = 0;
        this.ingresosPeriodoFinal = 0;

    }

    public Integer getTotalPeriodoPrincipio() {
        return totalPeriodoPrincipio;
    }

    public void setTotalPeriodoPrincipio(Integer totalPeriodoPrincipio) {
        this.totalPeriodoPrincipio = totalPeriodoPrincipio;
    }

    public Integer getTotalPeriodoMitad() {
        return totalPeriodoMitad;
    }

    public void setTotalPeriodoMitad(Integer totalPeriodoMitad) {
        this.totalPeriodoMitad = totalPeriodoMitad;
    }

    public Integer getTotalPeriodoFinal() {
        return totalPeriodoFinal;
    }

    public void setTotalPeriodoFinal(Integer totalPeriodoFinal) {
        this.totalPeriodoFinal = totalPeriodoFinal;
    }

    public Integer getGastosPeriodoPrincipio() {
        return gastosPeriodoPrincipio;
    }

    public void setGastosPeriodoPrincipio(Integer gastosPeriodoPrincipio) {
        this.gastosPeriodoPrincipio = gastosPeriodoPrincipio;
    }

    public Integer getGastosPeriodoMitad() {
        return gastosPeriodoMitad;
    }

    public void setGastosPeriodoMitad(Integer gastosPeriodoMitad) {
        this.gastosPeriodoMitad = gastosPeriodoMitad;
    }

    public Integer getGastosPeriodoFinal() {
        return gastosPeriodoFinal;
    }

    public void setGastosPeriodoFinal(Integer gastosPeriodoFinal) {
        this.gastosPeriodoFinal = gastosPeriodoFinal;
    }

    public Integer getIngresosPeriodoPrincipio() {
        return ingresosPeriodoPrincipio;
    }

    public void setIngresosPeriodoPrincipio(Integer ingresosPeriodoPrincipio) {
        this.ingresosPeriodoPrincipio = ingresosPeriodoPrincipio;
    }

    public Integer getIngresosPeriodoMitad() {
        return ingresosPeriodoMitad;
    }

    public void setIngresosPeriodoMitad(Integer ingresosPeriodoMitad) {
        this.ingresosPeriodoMitad = ingresosPeriodoMitad;
    }

    public Integer getIngresosPeriodoFinal() {
        return ingresosPeriodoFinal;
    }

    public void setIngresosPeriodoFinal(Integer ingresosPeriodoFinal) {
        this.ingresosPeriodoFinal = ingresosPeriodoFinal;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public void agregarBalanceTemporadaActual(Balance balance) {
        Integer periodoBalanceId = balance.getPeriodo().getId();

        if (periodoBalanceId.equals(ID_PERIODO_PRINCIPIO)) this.agregarBalancePrincipioTemporada(balance.getMonto());
        else if (periodoBalanceId.equals(ID_PERIODO_MITAD)) this.agregarBalanceMitadTemporada(balance.getMonto());
        else if (periodoBalanceId.equals(ID_PERIODO_FINAL)) this.agregarBalanceFinalTemporada(balance.getMonto());
    }


    private void agregarBalanceFinalTemporada(Integer monto) {
        if (monto >= 0) {
            this.ingresosPeriodoFinal += monto;
        }
        else {
            this.gastosPeriodoFinal -= monto;
        }
        this.totalPeriodoFinal += monto;
    }

    private void agregarBalanceMitadTemporada(Integer monto) {
        if (monto >= 0) {
            this.ingresosPeriodoMitad += monto;
        }
        else {
            this.gastosPeriodoMitad -= monto;
        }
        this.totalPeriodoMitad += monto;
        this.totalPeriodoFinal += monto;
    }

    private void agregarBalancePrincipioTemporada(Integer monto) {
        if (monto >= 0) {
            this.ingresosPeriodoPrincipio += monto;
        }
        else {
            this.gastosPeriodoPrincipio -= monto;
        }
        this.totalPeriodoPrincipio += monto;
        this.totalPeriodoMitad += monto;
        this.totalPeriodoFinal += monto;
    }
}
