package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.excepciones.JugadorYaTransferidoException;
import com.tesis.futpal.comunicacion.requests.transferencia.JugadorTransferencia;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ValidadorTransferencia {

    private static final Long ID_ESTADO_JUGADOR_TRASPASADO = 4L;

    public void validarTraspasos(List<JugadorTransferencia> jugadoresTraspaso) throws JugadorYaTransferidoException {
        if (!hayJugadorRepetido(jugadoresTraspaso)) {
            for (JugadorTransferencia jugadorTraspaso : jugadoresTraspaso) {
                if (jugadorTraspaso.getJugador().getEstadoJugador().getId().equals(ID_ESTADO_JUGADOR_TRASPASADO)) {
                    throw new JugadorYaTransferidoException();
                }
            }
        }
        else {
            throw new JugadorYaTransferidoException();
        }
    }

    private boolean hayJugadorRepetido(List<JugadorTransferencia> jugadoresTraspaso) {
        Set<Long> items = new HashSet<>();
        Set<Long> jugadoresRepetidos = jugadoresTraspaso.stream().map(jugador -> {
            return jugador.getJugador().getId();
        }).filter(id -> !items.add(id)).collect(Collectors.toSet());
        return !jugadoresRepetidos.isEmpty();
    }
}
