package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;

public class BalanceDetalleTransferencia {

    private Integer id;
    private String detalle;
    private EquipoDetalleTransferencia equipo;
    private Integer monto;
    private Integer numeroTemporada;
    private PeriodoTemporada periodo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public EquipoDetalleTransferencia getEquipo() {
        return equipo;
    }

    public void setEquipo(EquipoDetalleTransferencia equipo) {
        this.equipo = equipo;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Integer getNumeroTemporada() {
        return numeroTemporada;
    }

    public void setNumeroTemporada(Integer numeroTemporada) {
        this.numeroTemporada = numeroTemporada;
    }

    public PeriodoTemporada getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodoTemporada periodo) {
        this.periodo = periodo;
    }

    public static BalanceDetalleTransferencia crearAPartirDe(Balance balance) {
        BalanceDetalleTransferencia balanceDetalleTransferencia = new BalanceDetalleTransferencia();

        balanceDetalleTransferencia.setDetalle(balance.getDetalle());

        EquipoDetalleTransferencia equipoDetalleTransferencia = new EquipoDetalleTransferencia();
        equipoDetalleTransferencia.setId(balance.getEquipo().getId());
        equipoDetalleTransferencia.setNombreEquipo(balance.getEquipo().getNombreEquipo());
        equipoDetalleTransferencia.setIdUsuario(balance.getEquipo().getIdUsuario());
        balanceDetalleTransferencia.setEquipo(equipoDetalleTransferencia);

        balanceDetalleTransferencia.setMonto(balance.getMonto());

        balanceDetalleTransferencia.setId(balance.getId());

        balanceDetalleTransferencia.setPeriodo(balance.getPeriodo());

        balanceDetalleTransferencia.setNumeroTemporada(balance.getNumeroTemporada());

        return balanceDetalleTransferencia;
    }
}
