package com.tesis.futpal.modelos.usuario;

import java.util.HashMap;
import java.util.Map;

public enum TipoRol {
    ROL_USUARIO(1, "Usuario con permisos basicos"),
    ROL_ADMINISTRADOR(2, "Usuario con permisos de administrador");

    TipoRol(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    private int id;
    private String descripcion;
    private static Map<Integer, TipoRol> mapaPorId = new HashMap<>();

    static {
        for (TipoRol tipoRol : values()) {
            mapaPorId.put(tipoRol.getId(), tipoRol);
        }
    }

    public Integer getId() {
        return this.id;
    }

    public static TipoRol obtenerPorId(Integer id) {
        return mapaPorId.get(id);
    }
}
