package com.tesis.futpal.modelos.equipo;

import java.util.HashMap;
import java.util.Map;

public enum EstadoEquipoEnum {
    APROBADO(1, "El equipo ya fue aprobado por un administrador"),
    PENDIENTE_APROBACION(2, "El equipo está pendiente de aprobación."),
    INEXISTENTE(3, "El equipo no existe."),
    RECHAZADO(4, "El equipo fue rechazado. Puede volver a solicitar la aprobación."),
    INICIAL(5, "Al finalizar de agregar jugadores puede solicitar la aprobación del equipo.");

    EstadoEquipoEnum(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    private int id;
    private String descripcion;
    private static Map<Integer, EstadoEquipoEnum> mapaPorId = new HashMap<>();

    static {
        for (EstadoEquipoEnum estadoEquipoEnum : values()) {
            mapaPorId.put(estadoEquipoEnum.getId(), estadoEquipoEnum);
        }
    }

    public Integer getId() {
        return this.id;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public static EstadoEquipoEnum obtenerPorId(Integer id) {
        return mapaPorId.get(id);
    }
}
