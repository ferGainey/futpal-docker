package com.tesis.futpal.modelos.plantel;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.transferencia.TipoMovimiento;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Jugador {

    private static final Long ID_ESTADO_JUGADOR_PRESTAMO_ACORDADO = 3L;
    private static final Long ID_ESTADO_JUGADOR_TRASPASO_ACORDADO = 4L;
    private static final Long ID_ESTADO_JUGADOR_DISPONIBLE = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    @JsonProperty
    private Long id;
    private String nombre;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate fechaNacimiento;
    private float valor;
    private float salario;
    private Integer media;
    private boolean activo;

    @ManyToOne
    @JsonBackReference
    private Equipo equipo;

    @ManyToOne
    private EstadoJugador estadoJugador;
    private Long idTransfermarkt;


    public Jugador(String nombre, float valor, float salario, Integer media,
                   LocalDate fechaNacimiento, Equipo equipo, Long idTransfermarkt) {
        this.setNombre(nombre);
        this.setValor(valor);
        this.setSalario(salario);
        this.setMedia(media);
        this.setFechaNacimiento(fechaNacimiento);
        this.setEquipo(equipo);
        EstadoJugador estadoJugador = new EstadoJugador();
        estadoJugador.setId(ID_ESTADO_JUGADOR_DISPONIBLE);
        this.setEstadoJugador(estadoJugador);
        this.setIdTransfermarkt(idTransfermarkt);
        this.setActivo(true);
    }

    public Jugador() {
    }

    public Jugador(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public Integer getMedia() {
        return media;
    }

    public void setMedia(Integer media) {
        this.media = media;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public EstadoJugador getEstadoJugador() {
        return estadoJugador;
    }

    public void setEstadoJugador(EstadoJugador estadoJugador) {
        this.estadoJugador = estadoJugador;
    }

    public void actualizarEstadoTransferenciaFutura(TipoMovimiento tipoMovimiento, Integer idTransferenciaTipoPrestamo) {
        if (tipoMovimiento.getId().equals(idTransferenciaTipoPrestamo)) {
            // Se comenta hasta que se empiece a usar y se arregle
            // estadoJugador.setId(ID_ESTADO_JUGADOR_PRESTAMO_ACORDADO);
        }
        else {
            EstadoJugador estadoJugador = new EstadoJugador();
            estadoJugador.setId(ID_ESTADO_JUGADOR_TRASPASO_ACORDADO);
            this.setEstadoJugador(estadoJugador);
        }
    }

    public void setIdTransfermarkt(Long idTransfermarkt) {
        this.idTransfermarkt = idTransfermarkt;
    }

    public Long getIdTransfermarkt() {
        return idTransfermarkt;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}