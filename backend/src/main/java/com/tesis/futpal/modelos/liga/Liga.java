package com.tesis.futpal.modelos.liga;

import com.tesis.futpal.modelos.temporada.Temporada;

import javax.persistence.*;

@Entity
public class Liga {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;
    private String nombre;

    @ManyToOne
    private Temporada temporada;

    public Liga() {
    }

    public Liga(String nombreLiga) {
        this.setNombre(nombreLiga);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Temporada getTemporada() {
        return temporada;
    }

    public void setTemporada(Temporada temporada) {
        this.temporada = temporada;
    }
}
