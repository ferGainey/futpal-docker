package com.tesis.futpal.modelos.transferencia;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.usuario.Usuario;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
public class Transferencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @CreationTimestamp
    private Timestamp fechaTransferencia;

    @ManyToOne
    @JsonBackReference
    private Usuario usuarioOrigen;

    @ManyToOne
    @JsonBackReference
    private Usuario usuarioDestino;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="transferencia_id")
    @JsonManagedReference
    private List<Balance> balances;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="transferencia_id")
    @JsonManagedReference
    private List<MovimientoJugador> movimientos;

    @ManyToOne
    private EstadoTransferencia estadoTransferencia;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getFechaTransferencia() {
        return fechaTransferencia;
    }

    public void setFechaTransferencia(Timestamp fechaTransferencia) {
        this.fechaTransferencia = fechaTransferencia;
    }

    public Usuario getUsuarioOrigen() {
        return usuarioOrigen;
    }

    public void setUsuarioOrigen(Usuario usuarioOrigen) {
        this.usuarioOrigen = usuarioOrigen;
    }

    public List<Balance> getBalances() {
        return balances;
    }

    public void setBalances(List<Balance> balances) {
        this.balances = balances;
    }

    public List<MovimientoJugador> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<MovimientoJugador> movimientos) {
        this.movimientos = movimientos;
    }

    public Usuario getUsuarioDestino() {
        return usuarioDestino;
    }

    public void setUsuarioDestino(Usuario usuarioDestino) {
        this.usuarioDestino = usuarioDestino;
    }

    public EstadoTransferencia getEstadoTransferencia() {
        return estadoTransferencia;
    }

    public void setEstadoTransferencia(EstadoTransferencia estadoTransferencia) {
        this.estadoTransferencia = estadoTransferencia;
    }
}
