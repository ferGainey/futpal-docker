package com.tesis.futpal.modelos.temporada;

import javax.persistence.*;

@Entity
public class PeriodoTemporada {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    private String nombre;
    private Integer proximaId;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getProximaId() {
        return proximaId;
    }

    public void setProximaId(Integer proximaId) {
        this.proximaId = proximaId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
