package com.tesis.futpal.modelos.transferencia;

import java.util.List;

public class DetalleTransferencia {

    private Integer id;
    private EquipoDetalleTransferencia equipoOrigenTransferencia;
    private EquipoDetalleTransferencia equipoDestinoTransferencia;
    private List<BalanceDetalleTransferencia> montos;
    private List<MovimientoJugadorDetalleTransferencia> movimientoJugadores;
    private EstadoTransferencia estadoTransferencia;

    public EquipoDetalleTransferencia getEquipoOrigenTransferencia() {
        return equipoOrigenTransferencia;
    }

    public void setEquipoOrigenTransferencia(EquipoDetalleTransferencia equipoOrigenTransferencia) {
        this.equipoOrigenTransferencia = equipoOrigenTransferencia;
    }

    public EquipoDetalleTransferencia getEquipoDestinoTransferencia() {
        return equipoDestinoTransferencia;
    }

    public void setEquipoDestinoTransferencia(EquipoDetalleTransferencia equipoDestinoTransferencia) {
        this.equipoDestinoTransferencia = equipoDestinoTransferencia;
    }

    public List<BalanceDetalleTransferencia> getMontos() {
        return montos;
    }

    public void setMontos(List<BalanceDetalleTransferencia> montos) {
        this.montos = montos;
    }

    public List<MovimientoJugadorDetalleTransferencia> getMovimientoJugadores() {
        return movimientoJugadores;
    }

    public void setMovimientoJugadores(List<MovimientoJugadorDetalleTransferencia> movimientoJugadores) {
        this.movimientoJugadores = movimientoJugadores;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EstadoTransferencia getEstadoTransferencia() {
        return estadoTransferencia;
    }

    public void setEstadoTransferencia(EstadoTransferencia estadoTransferencia) {
        this.estadoTransferencia = estadoTransferencia;
    }
}
