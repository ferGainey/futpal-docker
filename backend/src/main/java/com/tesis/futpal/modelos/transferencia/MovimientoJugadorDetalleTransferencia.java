package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;

public class MovimientoJugadorDetalleTransferencia {

    private Integer id;
    private Jugador jugador;
    private Integer numeroTemporada;
    private PeriodoTemporada periodo;
    private EquipoDetalleTransferencia equipoOrigen;
    private EquipoDetalleTransferencia equipoDestino;
    private TipoMovimiento tipoMovimiento;
    // Viene si es un préstamo
    private PrestamoJugador prestamoJugador;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Integer getNumeroTemporada() {
        return numeroTemporada;
    }

    public void setNumeroTemporada(Integer numeroTemporada) {
        this.numeroTemporada = numeroTemporada;
    }

    public PeriodoTemporada getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodoTemporada periodo) {
        this.periodo = periodo;
    }

    public EquipoDetalleTransferencia getEquipoOrigen() {
        return equipoOrigen;
    }

    public void setEquipoOrigen(EquipoDetalleTransferencia equipoOrigen) {
        this.equipoOrigen = equipoOrigen;
    }

    public EquipoDetalleTransferencia getEquipoDestino() {
        return equipoDestino;
    }

    public void setEquipoDestino(EquipoDetalleTransferencia equipoDestino) {
        this.equipoDestino = equipoDestino;
    }

    public static MovimientoJugadorDetalleTransferencia crearAPartirDe(MovimientoJugador movimientoJugador) {
        MovimientoJugadorDetalleTransferencia movimientoJugadorDetalleTransferencia = new MovimientoJugadorDetalleTransferencia();
        movimientoJugadorDetalleTransferencia.setId(movimientoJugador.getId());
        movimientoJugadorDetalleTransferencia.setJugador(movimientoJugador.getJugador());
        movimientoJugadorDetalleTransferencia.setNumeroTemporada(movimientoJugador.getNumeroTemporada());
        movimientoJugadorDetalleTransferencia.setPeriodo(movimientoJugador.getPeriodo());

        EquipoDetalleTransferencia equipoDetalleTransferenciaOrigen = new EquipoDetalleTransferencia();
        equipoDetalleTransferenciaOrigen.setId(movimientoJugador.getEquipoOrigen().getId());
        equipoDetalleTransferenciaOrigen.setNombreEquipo(movimientoJugador.getEquipoOrigen().getNombreEquipo());
        equipoDetalleTransferenciaOrigen.setIdUsuario(movimientoJugador.getEquipoOrigen().getIdUsuario());
        movimientoJugadorDetalleTransferencia.setEquipoOrigen(equipoDetalleTransferenciaOrigen);

        EquipoDetalleTransferencia equipoDetalleTransferenciaDestino = new EquipoDetalleTransferencia();
        equipoDetalleTransferenciaDestino.setId(movimientoJugador.getEquipoDestino().getId());
        equipoDetalleTransferenciaDestino.setNombreEquipo(movimientoJugador.getEquipoDestino().getNombreEquipo());
        equipoDetalleTransferenciaDestino.setIdUsuario(movimientoJugador.getEquipoDestino().getIdUsuario());
        movimientoJugadorDetalleTransferencia.setEquipoDestino(equipoDetalleTransferenciaDestino);

        movimientoJugadorDetalleTransferencia.setPrestamoJugador(movimientoJugador.getPrestamoJugador());

        return movimientoJugadorDetalleTransferencia;
    }

    public PrestamoJugador getPrestamoJugador() {
        return prestamoJugador;
    }

    public void setPrestamoJugador(PrestamoJugador prestamoJugador) {
        this.prestamoJugador = prestamoJugador;
    }
}
