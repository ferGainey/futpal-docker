package com.tesis.futpal.modelos.liga;

import com.tesis.futpal.modelos.equipo.Equipo;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class GeneradorPartidos {

    private static final Integer EQUIPO_FANTASMA = -1;

    public List<PartidoLigaBase> generar(List<Integer> idEquipos, boolean idaYVuelta) {
        List<PartidoLigaBase> partidosBase = new ArrayList<>();
        boolean equipoFantasmaIncluido = agregarEquipoFantasmaSiElNumeroDeEquiposEsImpar(idEquipos);

        int cantidadDeEquipos = idEquipos.size();
        int cantidadDeFechas = cantidadDeEquipos - 1;
        int partidosPorFecha = cantidadDeEquipos / 2;


        crearPartidos(idEquipos, partidosBase, cantidadDeEquipos, cantidadDeFechas, partidosPorFecha);
        if (equipoFantasmaIncluido) {
            partidosBase = limpiarPartidosDelEquipoFantasma(partidosBase);
            eliminaEquipoFantasmaDelListadoDeEquipos(idEquipos);
        }
        partidosBase = distribuirPartidos(idEquipos, partidosBase);

        if (idaYVuelta) {
            partidosBase = agregarPartidosVuelta(partidosBase);
        }

        return partidosBase;
    }

    private void eliminaEquipoFantasmaDelListadoDeEquipos(List<Integer> idEquipos) {
        idEquipos.remove(idEquipos.size() - 1);
    }

    private List<PartidoLigaBase> limpiarPartidosDelEquipoFantasma(List<PartidoLigaBase> partidosBase) {
        return partidosBase.stream().filter(partido -> partido.getEquipoLocal().getId().intValue() != EQUIPO_FANTASMA
                && partido.getEquipoVisitante().getId().intValue() != EQUIPO_FANTASMA)
                .collect(Collectors.toList());
    }

    private List<PartidoLigaBase> distribuirPartidos(List<Integer> idEquipos, List<PartidoLigaBase> partidosBase) {
        // ToDo: arreglar separación de partidos de local y visitante
        List<PartidoLigaBase> partidosDistribuidos = new ArrayList<>(partidosBase);

        // Arregla distribución local y visitante
        int idUltimoEquipo = idEquipos.get(idEquipos.size() - 1);
        List<PartidoLigaBase> partidosUltimoEquipo = obtenerPartidosEquipo(partidosDistribuidos, idUltimoEquipo);
        partidosDistribuidos = partidosDistribuidos.stream().filter(partido -> partido.getEquipoLocal().getId().intValue() != idUltimoEquipo
                && partido.getEquipoVisitante().getId().intValue() != idUltimoEquipo)
                .collect(Collectors.toList());
        for (int partido = 0; partido < partidosUltimoEquipo.size(); partido++) {
            PartidoLigaBase partidoActual = partidosUltimoEquipo.get(partido);
            if (partido % 2 == 1) {
                Equipo equipoLocal = partidoActual.getEquipoLocal();
                Equipo equipoVisitante = partidoActual.getEquipoVisitante();
                partidoActual.setEquipoLocal(equipoVisitante);
                partidoActual.setEquipoVisitante(equipoLocal);
            }
            partidosDistribuidos.add(partidoActual);
        }

        return partidosDistribuidos;
    }

    private List<PartidoLigaBase> obtenerPartidosEquipo(List<PartidoLigaBase> partidosDistribuidos, Integer idEquipo) {
        return partidosDistribuidos.stream()
                .filter(partido -> partido.getEquipoLocal().getId().intValue() == idEquipo
                        || partido.getEquipoVisitante().getId().intValue() == idEquipo)
                .collect(Collectors.toList());
    }

    private void crearPartidos(List<Integer> idEquipos, List<PartidoLigaBase> partidosBase, int cantidadDeEquipos, int cantidadDeFechas, int partidosPorFecha) {
        // Se utiliza el algoritmo cíclico.
        EstadoPartido estadoPartidoPendiente = new EstadoPartido();
        estadoPartidoPendiente.setId(EstadoPartidoEnum.PENDIENTE.getId());

        for (int fecha = 0; fecha < cantidadDeFechas; fecha++) {
            for (int partido = 0; partido < partidosPorFecha; partido++) {
                int local = (fecha + partido) % (cantidadDeEquipos - 1);
                int visitante = (cantidadDeEquipos - 1 - partido + fecha) % (cantidadDeEquipos - 1);

                if (partido == 0) {
                    visitante = cantidadDeEquipos - 1;
                }
                PartidoLigaBase partidoLigaBase = new PartidoLigaBase();
                Equipo equipoLocal = new Equipo();
                equipoLocal.setId((long) idEquipos.get(local));
                partidoLigaBase.setEquipoLocal(equipoLocal);
                Equipo equipoVisitante = new Equipo();
                equipoVisitante.setId((long) idEquipos.get(visitante));
                partidoLigaBase.setEquipoVisitante(equipoVisitante);

                partidoLigaBase.setEstadoPartido(estadoPartidoPendiente);
                partidoLigaBase.setNumeroFecha(fecha + 1);

                partidosBase.add(partidoLigaBase);
            }
        }
    }

    private boolean agregarEquipoFantasmaSiElNumeroDeEquiposEsImpar(List<Integer> idEquipos) {
        boolean laCantidadDeEquiposEsImpar = idEquipos.size() % 2 != 0;
        if (laCantidadDeEquiposEsImpar) {
            idEquipos.add(EQUIPO_FANTASMA);
        }
        return laCantidadDeEquiposEsImpar;
    }

    private List<PartidoLigaBase> agregarPartidosVuelta(List<PartidoLigaBase> partidosBase) {
        Integer fechaMaximaIda = Collections.max(partidosBase.stream().map(PartidoLigaBase::getNumeroFecha).collect(Collectors.toList()));
        List<PartidoLigaBase> partidosBaseConVuelta = new ArrayList<>(partidosBase);
        partidosBase.forEach(partidoBase -> {
            PartidoLigaBase partidoLigaBaseActual = new PartidoLigaBase();
            partidoLigaBaseActual.setEquipoLocal(partidoBase.getEquipoVisitante());
            partidoLigaBaseActual.setEquipoVisitante(partidoBase.getEquipoLocal());
            EstadoPartido estadoPartidoPendiente = new EstadoPartido();
            estadoPartidoPendiente.setId(EstadoPartidoEnum.PENDIENTE.getId());
            partidoLigaBaseActual.setEstadoPartido(estadoPartidoPendiente);
            partidoLigaBaseActual.setNumeroFecha(partidoBase.getNumeroFecha() + fechaMaximaIda);
            partidosBaseConVuelta.add(partidoLigaBaseActual);
        });
        return partidosBaseConVuelta;
    }
}
