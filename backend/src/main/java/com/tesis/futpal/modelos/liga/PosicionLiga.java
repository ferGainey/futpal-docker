package com.tesis.futpal.modelos.liga;

import com.tesis.futpal.modelos.equipo.Equipo;

public class PosicionLiga {

    private Equipo equipo;
    private Integer partidosJugados;
    private Integer golesAFavor;
    private Integer golesEnContra;
    private Integer diferenciaDeGoles;
    private Integer puntosLiga;

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public Integer getPartidosJugados() {
        return partidosJugados;
    }

    public void setPartidosJugados(Integer partidosJugados) {
        this.partidosJugados = partidosJugados;
    }

    public Integer getGolesAFavor() {
        return golesAFavor;
    }

    public void setGolesAFavor(Integer golesAFavor) {
        this.golesAFavor = golesAFavor;
    }

    public Integer getGolesEnContra() {
        return golesEnContra;
    }

    public void setGolesEnContra(Integer golesEnContra) {
        this.golesEnContra = golesEnContra;
    }

    public Integer getDiferenciaDeGoles() {
        return diferenciaDeGoles;
    }

    public void setDiferenciaDeGoles(Integer diferenciaDeGoles) {
        this.diferenciaDeGoles = diferenciaDeGoles;
    }

    public Integer getPuntosLiga() {
        return puntosLiga;
    }

    public void setPuntosLiga(Integer puntosLiga) {
        this.puntosLiga = puntosLiga;
    }
}
