package com.tesis.futpal.modelos.balance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.transferencia.Transferencia;

import javax.persistence.*;

@Entity
public class Balance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;
    private String detalle;

    @ManyToOne
    private Equipo equipo;
    private Integer monto;
    private Integer numeroTemporada;

    @ManyToOne
    private PeriodoTemporada periodo;

    @ManyToOne
    @JsonBackReference
    private Transferencia transferencia;

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setNumeroTemporada(Integer numeroTemporada) {
        this.numeroTemporada = numeroTemporada;
    }

    public Integer getNumeroTemporada() {
        return numeroTemporada;
    }

    public void setPeriodo(PeriodoTemporada periodo) {
        this.periodo = periodo;
    }

    public PeriodoTemporada getPeriodo() {
        return periodo;
    }

    public void setTransferencia(Transferencia transferencia) {
        this.transferencia = transferencia;
    }

    public Transferencia getTransferencia() {
        return transferencia;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
