package com.tesis.futpal.modelos.balance;

import com.tesis.futpal.modelos.equipo.Equipo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CalculadoraDetalleDeBalance {

    public DetalleBalance calcular(Equipo equipo, List<Balance> balances, Integer numeroTemporada) {
        DetalleBalance detalleBalance = new DetalleBalance();
        for (Balance balance : balances) {
            if (balance.getNumeroTemporada() < numeroTemporada) {
                detalleBalance.setTotalPeriodoPrincipio(detalleBalance.getTotalPeriodoPrincipio() + balance.getMonto());
                detalleBalance.setTotalPeriodoMitad(detalleBalance.getTotalPeriodoMitad() + balance.getMonto());
                detalleBalance.setTotalPeriodoFinal(detalleBalance.getTotalPeriodoFinal() + balance.getMonto());
            }
            else {
                detalleBalance.agregarBalanceTemporadaActual(balance);
            }
        }
        detalleBalance.setEquipo(equipo);
        return detalleBalance;
    }
}
