package com.tesis.futpal.modelos.liga;

import com.tesis.futpal.modelos.liga.PosicionLiga;
import org.springframework.stereotype.Component;

@Component
public class ComparadorPosicion {
    public int comparar(PosicionLiga posicion1, PosicionLiga posicion2) {
        if (!posicion1.getPuntosLiga().equals(posicion2.getPuntosLiga())) {
            return Integer.compare(posicion1.getPuntosLiga(), posicion2.getPuntosLiga());
        }
        else if (!posicion1.getDiferenciaDeGoles().equals(posicion2.getDiferenciaDeGoles())) {
            return Integer.compare(posicion1.getDiferenciaDeGoles(), posicion2.getDiferenciaDeGoles());
        }
        else if (!posicion1.getGolesAFavor().equals(posicion2.getGolesAFavor())) {
            return Integer.compare(posicion1.getGolesAFavor(), posicion2.getGolesAFavor());
        }
        else return 0;
    }
}
