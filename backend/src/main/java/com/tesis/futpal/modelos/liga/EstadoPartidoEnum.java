package com.tesis.futpal.modelos.liga;

import java.util.HashMap;
import java.util.Map;

public enum EstadoPartidoEnum {
    PENDIENTE(1, "El partido aún no se jugó"),
    FINALIZADO(2, "Ya completó el partido.");

    EstadoPartidoEnum(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    private int id;
    private String descripcion;
    private static Map<Integer, EstadoPartidoEnum> mapaPorId = new HashMap<>();

    static {
        for (EstadoPartidoEnum estadoPartidoEnum : values()) {
            mapaPorId.put(estadoPartidoEnum.getId(), estadoPartidoEnum);
        }
    }

    public Integer getId() {
        return this.id;
    }

    public static EstadoPartidoEnum obtenerPorId(Integer id) {
        return mapaPorId.get(id);
    }
}
