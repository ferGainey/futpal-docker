package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CreadorDetalle {

    @Autowired
    private RepositorioEquipo repositorioEquipo;

    public DetalleTransferencia crearAPartirDe(Transferencia transferencia) {
        DetalleTransferencia detalleTransferencia = new DetalleTransferencia();

        detalleTransferencia.setId(transferencia.getId());

        detalleTransferencia.setEstadoTransferencia(transferencia.getEstadoTransferencia());

        List<BalanceDetalleTransferencia> montos = transferencia.getBalances().stream()
                .map(BalanceDetalleTransferencia::crearAPartirDe)
                .collect(Collectors.toList());
        detalleTransferencia.setMontos(montos);

        List<MovimientoJugadorDetalleTransferencia> movimientos = transferencia.getMovimientos().stream()
                .map(MovimientoJugadorDetalleTransferencia::crearAPartirDe)
                .collect(Collectors.toList());
        detalleTransferencia.setMovimientoJugadores(movimientos);

        Equipo equipoOrigen = repositorioEquipo.findByIdUsuario(transferencia.getUsuarioOrigen().getId()).get(0);
        EquipoDetalleTransferencia equipoDetalleTransferenciaOrigen = new EquipoDetalleTransferencia();
        equipoDetalleTransferenciaOrigen.setIdUsuario(equipoOrigen.getIdUsuario());
        equipoDetalleTransferenciaOrigen.setId(equipoOrigen.getId());
        equipoDetalleTransferenciaOrigen.setNombreEquipo(equipoOrigen.getNombreEquipo());
        detalleTransferencia.setEquipoOrigenTransferencia(equipoDetalleTransferenciaOrigen);

        Equipo equipoDestino = repositorioEquipo.findByIdUsuario(transferencia.getUsuarioDestino().getId()).get(0);
        EquipoDetalleTransferencia equipoDetalleTransferenciaDestino = new EquipoDetalleTransferencia();
        equipoDetalleTransferenciaDestino.setIdUsuario(equipoDestino.getIdUsuario());
        equipoDetalleTransferenciaDestino.setId(equipoDestino.getId());
        equipoDetalleTransferenciaDestino.setNombreEquipo(equipoDestino.getNombreEquipo());
        detalleTransferencia.setEquipoDestinoTransferencia(equipoDetalleTransferenciaDestino);

        return detalleTransferencia;
    }
}
