package com.tesis.futpal.modelos.plantel;

import com.tesis.futpal.modelos.temporada.PeriodoTemporada;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class JugadorPrestado {

    @Id
    private Integer id;
    @OneToOne
    private Jugador jugador;
    private String nombreEquipoActual;
    @ManyToOne
    private PeriodoTemporada periodo;
    private Integer numeroTemporada;

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public PeriodoTemporada getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodoTemporada periodo) {
        this.periodo = periodo;
    }

    public Integer getNumeroTemporada() {
        return numeroTemporada;
    }

    public void setNumeroTemporada(Integer numeroTemporada) {
        this.numeroTemporada = numeroTemporada;
    }

    public String getNombreEquipoActual() {
        return nombreEquipoActual;
    }

    public void setNombreEquipoActual(String nombreEquipoActual) {
        this.nombreEquipoActual = nombreEquipoActual;
    }
}
