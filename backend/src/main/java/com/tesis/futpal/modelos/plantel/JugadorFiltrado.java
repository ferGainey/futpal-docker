package com.tesis.futpal.modelos.plantel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class JugadorFiltrado {

    @Id
    private Integer id;
    @OneToOne
    private Jugador jugador;
    private String nombreEquipoActual;

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public String getNombreEquipoActual() {
        return nombreEquipoActual;
    }

    public void setNombreEquipoActual(String nombreEquipoActual) {
        this.nombreEquipoActual = nombreEquipoActual;
    }
}
