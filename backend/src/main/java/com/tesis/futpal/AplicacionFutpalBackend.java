package com.tesis.futpal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplicacionFutpalBackend {

	public static void main(String[] args) {
		SpringApplication.run(AplicacionFutpalBackend.class, args);
	}
}
