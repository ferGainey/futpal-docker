package com.tesis.futpal.comunicacion.responses;

import com.tesis.futpal.modelos.equipo.Equipo;

public class ResponseEquipoActualizado {

    private String nombreEquipoActualizado;

    public ResponseEquipoActualizado(Equipo equipo) {
        this.nombreEquipoActualizado = equipo.getNombreEquipo();
    }

    public String getNombreEquipoActualizado() {
        return nombreEquipoActualizado;
    }

    public void setNombreEquipoActualizado(String nombreEquipoActualizado) {
        this.nombreEquipoActualizado = nombreEquipoActualizado;
    }
}
