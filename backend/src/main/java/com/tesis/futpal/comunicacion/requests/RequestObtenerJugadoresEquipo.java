package com.tesis.futpal.comunicacion.requests;

public class RequestObtenerJugadoresEquipo {
    private Long equipoId;

    public Long getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(Long equipoId) {
        this.equipoId = equipoId;
    }
}
