package com.tesis.futpal.comunicacion.requests;

public class RequestDatosJugador {

    private Long jugadorId;

    public Long getJugadorId() {
        return jugadorId;
    }

    public void setJugadorId(Long jugadorId) {
        this.jugadorId = jugadorId;
    }
}
