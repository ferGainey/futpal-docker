package com.tesis.futpal.comunicacion.responses;

import java.util.List;

public class ResponseJwt {
    private String token;
    private String type = "Bearer";
    private Long id;
    private String aliasUsuario;
    private String email;
    private List<String> roles;

    public ResponseJwt(String accessToken, Long id, String aliasUsuario, String email, List<String> roles) {
        this.token = accessToken;
        this.id = id;
        this.aliasUsuario = aliasUsuario;
        this.email = email;
        this.roles = roles;
    }

    public String getAccessToken() {
        return token;
    }

    public void setAccessToken(String accessToken) {
        this.token = accessToken;
    }

    public String getTokenType() {
        return type;
    }

    public void setTokenType(String tokenType) {
        this.type = tokenType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAliasUsuario() {
        return aliasUsuario;
    }

    public void setAliasUsuario(String aliasUsuario) {
        this.aliasUsuario = aliasUsuario;
    }

    public List<String> getRoles() {
        return roles;
    }
}