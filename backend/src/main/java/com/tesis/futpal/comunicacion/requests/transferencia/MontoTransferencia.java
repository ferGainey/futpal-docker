package com.tesis.futpal.comunicacion.requests.transferencia;

import com.tesis.futpal.modelos.temporada.PeriodoTemporada;

public class MontoTransferencia {

    private Integer monto;
    private Integer numeroTemporada;
    private PeriodoTemporada periodo;
    private Integer idEquipoOrigen;
    private Integer idEquipoDestino;


    public Integer getNumeroTemporada() {
        return numeroTemporada;
    }

    public void setNumeroTemporada(Integer numeroTemporada) {
        this.numeroTemporada = numeroTemporada;
    }

    public PeriodoTemporada getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodoTemporada periodo) {
        this.periodo = periodo;
    }

    public Integer getIdEquipoOrigen() {
        return idEquipoOrigen;
    }

    public void setIdEquipoOrigen(Integer idEquipoOrigen) {
        this.idEquipoOrigen = idEquipoOrigen;
    }

    public Integer getIdEquipoDestino() {
        return idEquipoDestino;
    }

    public void setIdEquipoDestino(Integer idEquipoDestino) {
        this.idEquipoDestino = idEquipoDestino;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }
}
