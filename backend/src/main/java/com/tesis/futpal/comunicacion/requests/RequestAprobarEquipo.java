package com.tesis.futpal.comunicacion.requests;

public class RequestAprobarEquipo {
    private Long equipoId;

    public void setEquipoId(Long equipoId) {
        this.equipoId = equipoId;
    }

    public Long getEquipoId() {
        return equipoId;
    }
}
