package com.tesis.futpal.comunicacion.requests;

public class RequestObtenerEstadisticasPartido {
    private Integer partidoId;

    public void setPartidoId(Integer partidoId) {
        this.partidoId = partidoId;
    }

    public Integer getPartidoId() {
        return partidoId;
    }
}
