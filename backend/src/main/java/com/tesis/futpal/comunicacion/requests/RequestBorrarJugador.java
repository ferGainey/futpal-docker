package com.tesis.futpal.comunicacion.requests;

public class RequestBorrarJugador {
    private Long jugadorId;

    public void setJugadorId(Long jugadorId) {
        this.jugadorId = jugadorId;
    }

    public Long getJugadorId() {
        return jugadorId;
    }
}
