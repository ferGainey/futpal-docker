package com.tesis.futpal.comunicacion.requests;

import javax.validation.constraints.NotBlank;

public class RequestIngresar {
    @NotBlank
    private String alias;

    @NotBlank
    private String contrasenia;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }
}
