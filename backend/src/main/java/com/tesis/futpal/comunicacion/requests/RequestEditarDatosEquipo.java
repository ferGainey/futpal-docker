package com.tesis.futpal.comunicacion.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class RequestEditarDatosEquipo {

    @NotNull
    private Long idUsuario;

    @NotBlank
    private String nuevoNombreEquipo;

    public String getNuevoNombreEquipo() {
        return nuevoNombreEquipo;
    }

    public void setNuevoNombreEquipo(String nuevoNombreEquipo) {
        this.nuevoNombreEquipo = nuevoNombreEquipo;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }
}
