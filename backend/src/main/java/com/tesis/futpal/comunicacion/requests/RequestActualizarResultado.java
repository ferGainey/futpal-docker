package com.tesis.futpal.comunicacion.requests;

import javax.validation.constraints.NotNull;

public class RequestActualizarResultado {
    @NotNull
    private Integer idPartido;
    @NotNull
    private Integer golesLocal;
    @NotNull
    private Integer golesVisitante;
    private Integer usuarioEditorId;

    public Integer getIdPartido() {
        return idPartido;
    }

    public void setIdPartido(Integer idPartido) {
        this.idPartido = idPartido;
    }

    public Integer getGolesLocal() {
        return golesLocal;
    }

    public void setGolesLocal(Integer golesLocal) {
        this.golesLocal = golesLocal;
    }

    public Integer getGolesVisitante() {
        return golesVisitante;
    }

    public void setGolesVisitante(Integer golesVisitante) {
        this.golesVisitante = golesVisitante;
    }

    public void setUsuarioEditorId(Integer usuarioEditor) {
        this.usuarioEditorId = usuarioEditor;
    }

    public Integer getUsuarioEditorId() {
        return usuarioEditorId;
    }
}
