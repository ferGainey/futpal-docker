package com.tesis.futpal.comunicacion.requests;

public class RequestSolicitarAprobacionEquipo {
    private long equipoId;

    public long getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(long equipoId) {
        this.equipoId = equipoId;
    }
}
