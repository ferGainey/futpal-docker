package com.tesis.futpal.comunicacion.requests.balance;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;

public class RequestCargaBalanceUnEquipo {

    private String detalle;
    private Long equipoId;
    private Integer monto;
    private Integer numeroTemporada;
    private Integer periodoId;

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Long getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(Long equipoId) {
        this.equipoId = equipoId;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Integer getNumeroTemporada() {
        return numeroTemporada;
    }

    public void setNumeroTemporada(Integer numeroTemporada) {
        this.numeroTemporada = numeroTemporada;
    }

    public Integer getPeriodoId() {
        return periodoId;
    }

    public void setPeriodoId(Integer periodoId) {
        this.periodoId = periodoId;
    }

    public Balance crearBalance() {
        Balance balance = new Balance();

        balance.setDetalle(this.detalle);

        Equipo equipo = new Equipo();
        equipo.setId(this.equipoId);
        balance.setEquipo(equipo);

        balance.setMonto(this.monto);

        balance.setNumeroTemporada(this.numeroTemporada);

        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(periodoId);
        balance.setPeriodo(periodoTemporada);

        return balance;
    }
}
