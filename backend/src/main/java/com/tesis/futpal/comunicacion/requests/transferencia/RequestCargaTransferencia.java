package com.tesis.futpal.comunicacion.requests.transferencia;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.transferencia.MovimientoJugador;
import com.tesis.futpal.modelos.transferencia.TipoMovimiento;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RequestCargaTransferencia {

    private static final String DESCRIPCION_TRASPASO = "Traspaso: ";
    private static final String DESCRIPCION_PRESTAMO = "Préstamo: ";
    private static final String SEPARADOR_DETALLE_BALANCE = " | ";
    private static final Integer ID_TIPO_TRANSFERENCIA_TRASPASO = 1;
    private static final Integer ID_TIPO_TRANSFERENCIA_PRESTAMO = 2;

    private Long otroEquipoId;
    private List<JugadorTransferencia> jugadoresTransferencia;
    private List<JugadorPrestamoTransferencia> jugadoresPrestamoTransferencia;
    private List<MontoTransferencia> montosTransferencias;


    public List<MovimientoJugador> crearMovimientosJugadores() {
        List<MovimientoJugador> movimientoJugadores = new ArrayList<>();

        if (jugadoresTransferencia != null && !jugadoresTransferencia.isEmpty()) {
            List<MovimientoJugador> traspasosJugadores = jugadoresTransferencia.stream().map(jugadorTransferencia ->
                    crearMovimiento(jugadorTransferencia.getIdEquipoOrigen(),
                    jugadorTransferencia.getIdEquipoDestino(),
                    jugadorTransferencia.getJugador(),
                    jugadorTransferencia.getNumeroTemporada(),
                    jugadorTransferencia.getPeriodo(),
                    ID_TIPO_TRANSFERENCIA_TRASPASO)).collect(Collectors.toList());

            movimientoJugadores.addAll(traspasosJugadores);
        }

        if (jugadoresPrestamoTransferencia != null && !jugadoresPrestamoTransferencia.isEmpty()) {
            List<MovimientoJugador> prestamosJugadores = crearPrestamos();
            movimientoJugadores.addAll(prestamosJugadores);
        }

        return movimientoJugadores;
    }

    private List<MovimientoJugador> crearPrestamos() {
        List<MovimientoJugador> movimientosPrestamos = new ArrayList<>();

        jugadoresPrestamoTransferencia.forEach(jugadorPrestamoTransferencia -> {
            MovimientoJugador movimientoComienzoPrestamo = crearMovimiento(jugadorPrestamoTransferencia.getIdEquipoOrigen(),
                    jugadorPrestamoTransferencia.getIdEquipoDestino(),
                    jugadorPrestamoTransferencia.getJugador(),
                    jugadorPrestamoTransferencia.getNumeroTemporadaComienzo(),
                    jugadorPrestamoTransferencia.getPeriodoComienzo(),
                    ID_TIPO_TRANSFERENCIA_PRESTAMO);

            MovimientoJugador movimientoFinalPrestamo = crearMovimiento(jugadorPrestamoTransferencia.getIdEquipoDestino(),
                    jugadorPrestamoTransferencia.getIdEquipoOrigen(),
                    jugadorPrestamoTransferencia.getJugador(),
                    jugadorPrestamoTransferencia.getNumeroTemporadaFinal(),
                    jugadorPrestamoTransferencia.getPeriodoFinal(),
                    ID_TIPO_TRANSFERENCIA_PRESTAMO);

            movimientosPrestamos.add(movimientoComienzoPrestamo);
            movimientosPrestamos.add(movimientoFinalPrestamo);

        });

        return movimientosPrestamos;
    }

    private MovimientoJugador crearMovimiento(Integer idEquipoOrigen, Integer idEquipoDestino, Jugador jugador,
                                              Integer numeroTemporada, PeriodoTemporada periodo, Integer idTipoTransferencia) {
        MovimientoJugador movimientoJugador = new MovimientoJugador();

        Equipo equipoOrigen = new Equipo();
        equipoOrigen.setId(idEquipoOrigen.longValue());
        movimientoJugador.setEquipoOrigen(equipoOrigen);

        Equipo equipoDestino = new Equipo();
        equipoDestino.setId(idEquipoDestino.longValue());
        movimientoJugador.setEquipoDestino(equipoDestino);

        movimientoJugador.setJugador(jugador);

        TipoMovimiento tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setId(idTipoTransferencia);
        movimientoJugador.setTipoMovimiento(tipoMovimiento);


        movimientoJugador.setNumeroTemporada(numeroTemporada);

        movimientoJugador.setPeriodo(periodo);

        return movimientoJugador;
    }

    public List<Balance> crearBalances() {
        List<Balance> balances = new ArrayList<>();
        for(MontoTransferencia montoTransferencia : montosTransferencias) {
            Balance balanceOrigen = new Balance();
            Balance balanceDestino = new Balance();

            balanceOrigen.setMonto(montoTransferencia.getMonto() * -1);
            balanceOrigen.setNumeroTemporada(montoTransferencia.getNumeroTemporada());
            balanceOrigen.setPeriodo(montoTransferencia.getPeriodo());
            balanceOrigen.setDetalle(generarDetalle());
            Equipo equipoOrigen = new Equipo();
            equipoOrigen.setId(montoTransferencia.getIdEquipoOrigen().longValue());
            balanceOrigen.setEquipo(equipoOrigen);

            balanceDestino.setMonto(montoTransferencia.getMonto());
            balanceDestino.setNumeroTemporada(montoTransferencia.getNumeroTemporada());
            balanceDestino.setPeriodo(montoTransferencia.getPeriodo());
            balanceDestino.setDetalle(generarDetalle());
            Equipo equipoDestino = new Equipo();
            equipoDestino.setId(montoTransferencia.getIdEquipoDestino().longValue());
            balanceDestino.setEquipo(equipoDestino);

            balances.add(balanceOrigen);
            balances.add(balanceDestino);
        }
        return balances;
    }

    private String generarDetalle() {
        StringBuilder detalle = new StringBuilder();
        if (jugadoresTransferencia != null && !jugadoresTransferencia.isEmpty()) {
            detalle.append(DESCRIPCION_TRASPASO);
            for(JugadorTransferencia jugadorTransferencia : jugadoresTransferencia) {
                detalle.append(jugadorTransferencia.getJugador().getNombre()).append(SEPARADOR_DETALLE_BALANCE);
            }
        }
        if (jugadoresPrestamoTransferencia != null && !jugadoresPrestamoTransferencia.isEmpty()) {
            detalle.append(DESCRIPCION_PRESTAMO);
            for(JugadorPrestamoTransferencia jugadorPrestamoTransferencia : jugadoresPrestamoTransferencia) {
                detalle.append(jugadorPrestamoTransferencia.getJugador().getNombre()).append(SEPARADOR_DETALLE_BALANCE);
            }
        }

        return detalle.toString();
    }

    public List<JugadorTransferencia> getJugadoresTransferencia() {
        return jugadoresTransferencia;
    }

    public void setJugadoresTransferencia(List<JugadorTransferencia> jugadoresTransferencia) {
        this.jugadoresTransferencia = jugadoresTransferencia;
    }

    public void setMontosTransferencias(List<MontoTransferencia> montosTransferencias) {
        this.montosTransferencias = montosTransferencias;
    }

    public List<MontoTransferencia> getMontosTransferencias() {
        return montosTransferencias;
    }

    public Long getOtroEquipoId() {
        return otroEquipoId;
    }

    public void setOtroEquipoId(Long otroEquipoId) {
        this.otroEquipoId = otroEquipoId;
    }

    public List<JugadorPrestamoTransferencia> getJugadoresPrestamoTransferencia() {
        return jugadoresPrestamoTransferencia;
    }

    public void setJugadoresPrestamoTransferencia(List<JugadorPrestamoTransferencia> jugadoresPrestamoTransferencia) {
        this.jugadoresPrestamoTransferencia = jugadoresPrestamoTransferencia;
    }
}
