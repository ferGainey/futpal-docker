package com.tesis.futpal.comunicacion.responses;

import com.tesis.futpal.modelos.estadisticas.EstadisticaMvp;
import com.tesis.futpal.modelos.plantel.JugadorEstadisticas;

public class ResponseObtenerEstadisticasPartido extends ResponseObtenerEstadisticas {

    private JugadorEstadisticas mvpJugador;

    public JugadorEstadisticas getMvpJugador() {
        return mvpJugador;
    }

    public void setMvpJugador(EstadisticaMvp mvpJugador) {
        if (mvpJugador != null) {
            JugadorEstadisticas jugadorEstadisticas = new JugadorEstadisticas();
            jugadorEstadisticas.setNombre(mvpJugador.getJugador().getNombre());
            jugadorEstadisticas.setEquipo(mvpJugador.getEquipo());
            jugadorEstadisticas.setId(mvpJugador.getJugador().getId());

            this.mvpJugador = jugadorEstadisticas;
        }
        else {
            this.mvpJugador = null;
        }

    }
}
