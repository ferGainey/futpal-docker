package com.tesis.futpal.comunicacion.requests;

public class RequestRechazarEquipo {
    private Long equipoId;
    private String motivoRechazo;

    public void setEquipoId(Long equipoId) {
        this.equipoId = equipoId;
    }

    public Long getEquipoId() {
        return equipoId;
    }

    public String getMotivoRechazo() {
        return motivoRechazo;
    }

    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }
}
