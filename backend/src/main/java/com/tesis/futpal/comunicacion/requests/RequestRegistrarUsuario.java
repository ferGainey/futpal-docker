package com.tesis.futpal.comunicacion.requests;

import java.io.Serializable;
import java.util.stream.Stream;

public class RequestRegistrarUsuario implements Serializable {
    private String alias;
    private String mail;
    private String nombre;
    private String apellido;
    private String contrasenia;

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean verificarQueTodosLosCamposSeanNoNulos() {
        return Stream.of(alias, nombre, apellido, mail, contrasenia)
                .allMatch(campo -> !(campo ==null));
    }

    public boolean verificarQueTodosLosCamposSeanNoVacios() {
        return Stream.of(alias, nombre, apellido, mail, contrasenia)
                .allMatch(valor -> !valor.isEmpty());
    }

    public void eliminarEspaciosDelPrincipioYDelFinal() {
        this.setAlias(alias.trim());
        this.setNombre(nombre.trim());
        this.setApellido(apellido.trim());
        this.setMail(mail.trim());
        this.setContrasenia(contrasenia.trim());
    }
}
