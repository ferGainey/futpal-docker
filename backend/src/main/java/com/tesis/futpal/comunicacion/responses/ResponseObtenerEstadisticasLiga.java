package com.tesis.futpal.comunicacion.responses;

import com.tesis.futpal.modelos.estadisticas.EstadisticaMvp;
import com.tesis.futpal.modelos.plantel.JugadorEstadisticas;

import java.util.List;
import java.util.stream.Collectors;

public class ResponseObtenerEstadisticasLiga extends ResponseObtenerEstadisticas {

    private List<JugadorEstadisticas> mvpJugadores;

    public List<JugadorEstadisticas> getMvpJugadores() {
        return mvpJugadores;
    }

    public void setMvpJugadoresDesdeEstadistica(List<EstadisticaMvp> mvpJugadores) {
        this.mvpJugadores = mvpJugadores.stream().map(mvpJugador -> {
            JugadorEstadisticas jugadorEstadisticas = new JugadorEstadisticas();
            jugadorEstadisticas.setNombre(mvpJugador.getJugador().getNombre());
            jugadorEstadisticas.setEquipo(mvpJugador.getEquipo());
            jugadorEstadisticas.setId(mvpJugador.getJugador().getId());
            return jugadorEstadisticas;
        }).collect(Collectors.toList());
    }

    public void setMvpJugadores(List<JugadorEstadisticas> mvpJugadores) {
        this.mvpJugadores = mvpJugadores;
    }

    public List<JugadorEstadisticas> getLesionesJugadores() {
        return lesionesJugadores;
    }

    public void setGolesJugadores(List<JugadorEstadisticas> golesJugadores) {
        this.golesJugadores = golesJugadores;
    }

    public void setTarjetasAmarillas(List<JugadorEstadisticas> tarjetasAmarillasJugadores) {
        this.tarjetasAmarillasJugadores = tarjetasAmarillasJugadores;
    }

    public void setTarjetasRojasJugadores(List<JugadorEstadisticas> tarjetasRojasJugadores) {
        this.tarjetasRojasJugadores = tarjetasRojasJugadores;
    }

    public void setLesionesJugadores(List<JugadorEstadisticas> lesionesJugadores) {
        this.lesionesJugadores = lesionesJugadores;
    }
}
