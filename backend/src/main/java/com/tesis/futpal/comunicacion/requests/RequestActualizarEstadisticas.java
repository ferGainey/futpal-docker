package com.tesis.futpal.comunicacion.requests;

import com.tesis.futpal.modelos.plantel.JugadorEstadisticas;

import java.util.List;

public class RequestActualizarEstadisticas {

    private List<JugadorEstadisticas> golesJugadores;
    private List<JugadorEstadisticas> tarjetasAmarillasJugadores;
    private List<JugadorEstadisticas> tarjetasRojasJugadores;
    private List<JugadorEstadisticas> lesionesJugadores;
    private JugadorEstadisticas mvpJugador;
    private Integer partidoId;

    public List<JugadorEstadisticas> getGolesJugadores() {
        return golesJugadores;
    }

    public void setGolesJugadores(List<JugadorEstadisticas> golesJugadores) {
        this.golesJugadores = golesJugadores;
    }

    public List<JugadorEstadisticas> getTarjetasAmarillasJugadores() {
        return tarjetasAmarillasJugadores;
    }

    public void setTarjetasAmarillasJugadores(List<JugadorEstadisticas> tarjetasAmarillasJugadores) {
        this.tarjetasAmarillasJugadores = tarjetasAmarillasJugadores;
    }

    public List<JugadorEstadisticas> getTarjetasRojasJugadores() {
        return tarjetasRojasJugadores;
    }

    public void setTarjetasRojasJugadores(List<JugadorEstadisticas> tarjetasRojasJugadores) {
        this.tarjetasRojasJugadores = tarjetasRojasJugadores;
    }

    public List<JugadorEstadisticas> getLesionesJugadores() {
        return lesionesJugadores;
    }

    public void setLesionesJugadores(List<JugadorEstadisticas> lesionesJugadores) {
        this.lesionesJugadores = lesionesJugadores;
    }

    public JugadorEstadisticas getMvpJugador() {
        return mvpJugador;
    }

    public void setMvpJugador(JugadorEstadisticas mvpJugador) {
        this.mvpJugador = mvpJugador;
    }

    public Integer getPartidoId() {
        return partidoId;
    }

    public void setPartidoId(Integer partidoId) {
        this.partidoId = partidoId;
    }
}
