package com.tesis.futpal.comunicacion.requests.transferencia;

import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;

public class JugadorTransferencia {

    private Jugador jugador;
    private Integer numeroTemporada;
    private PeriodoTemporada periodo;
    private Integer idEquipoOrigen;
    private Integer idEquipoDestino;

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Integer getNumeroTemporada() {
        return numeroTemporada;
    }

    public void setNumeroTemporada(Integer numeroTemporada) {
        this.numeroTemporada = numeroTemporada;
    }

    public PeriodoTemporada getPeriodo() {
        return periodo;
    }

    public void setPeriodo(PeriodoTemporada periodoId) {
        this.periodo = periodoId;
    }

    public Integer getIdEquipoOrigen() {
        return idEquipoOrigen;
    }

    public void setIdEquipoOrigen(Integer idEquipoOrigen) {
        this.idEquipoOrigen = idEquipoOrigen;
    }

    public Integer getIdEquipoDestino() {
        return idEquipoDestino;
    }

    public void setIdEquipoDestino(Integer idEquipoDestino) {
        this.idEquipoDestino = idEquipoDestino;
    }
}
