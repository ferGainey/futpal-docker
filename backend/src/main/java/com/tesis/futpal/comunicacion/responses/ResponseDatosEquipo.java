package com.tesis.futpal.comunicacion.responses;

import com.tesis.futpal.modelos.equipo.Equipo;

public class ResponseDatosEquipo {

    private Long idEquipo;
    private String nombreEquipo;
    private String mensaje;
    private String mensajeEstadoEquipo;
    private Integer estadoEquipoId;
    private String motivoCambioEstado;

    public ResponseDatosEquipo(Equipo equipoBuscado) {
        this.idEquipo = equipoBuscado.getId();
        this.nombreEquipo = equipoBuscado.getNombreEquipo();
    }

    public ResponseDatosEquipo() {
    }

    public Long getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(Long idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensajeEstadoEquipo() {
        return mensajeEstadoEquipo;
    }

    public void setMensajeEstadoEquipo(String mensajeEstadoEquipo) {
        this.mensajeEstadoEquipo = mensajeEstadoEquipo;
    }

    public Integer getEstadoEquipoId() {
        return estadoEquipoId;
    }

    public void setEstadoEquipoId(Integer estadoEquipoId) {
        this.estadoEquipoId = estadoEquipoId;
    }

    public String getMotivoCambioEstado() {
        return motivoCambioEstado;
    }

    public void setMotivoCambioEstado(String motivoCambioEstado) {
        this.motivoCambioEstado = motivoCambioEstado;
    }
}
