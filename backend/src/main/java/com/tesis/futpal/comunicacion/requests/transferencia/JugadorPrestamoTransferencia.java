package com.tesis.futpal.comunicacion.requests.transferencia;

import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;

public class JugadorPrestamoTransferencia {

    private Jugador jugador;
    private Integer numeroTemporadaComienzo;
    private PeriodoTemporada periodoComienzo;
    private Integer numeroTemporadaFinal;
    private PeriodoTemporada periodoFinal;
    private Integer idEquipoOrigen;
    private Integer idEquipoDestino;

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Integer getNumeroTemporadaComienzo() {
        return numeroTemporadaComienzo;
    }

    public void setNumeroTemporadaComienzo(Integer numeroTemporadaComienzo) {
        this.numeroTemporadaComienzo = numeroTemporadaComienzo;
    }

    public PeriodoTemporada getPeriodoComienzo() {
        return periodoComienzo;
    }

    public void setPeriodoComienzo(PeriodoTemporada periodoId) {
        this.periodoComienzo = periodoId;
    }

    public Integer getIdEquipoOrigen() {
        return idEquipoOrigen;
    }

    public void setIdEquipoOrigen(Integer idEquipoOrigen) {
        this.idEquipoOrigen = idEquipoOrigen;
    }

    public Integer getIdEquipoDestino() {
        return idEquipoDestino;
    }

    public void setIdEquipoDestino(Integer idEquipoDestino) {
        this.idEquipoDestino = idEquipoDestino;
    }

    public Integer getNumeroTemporadaFinal() {
        return numeroTemporadaFinal;
    }

    public void setNumeroTemporadaFinal(Integer numeroTemporadaFinal) {
        this.numeroTemporadaFinal = numeroTemporadaFinal;
    }

    public PeriodoTemporada getPeriodoFinal() {
        return periodoFinal;
    }

    public void setPeriodoFinal(PeriodoTemporada periodoFinal) {
        this.periodoFinal = periodoFinal;
    }
}
