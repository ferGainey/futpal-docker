package com.tesis.futpal.comunicacion.responses;

import com.tesis.futpal.modelos.estadisticas.EstadisticaGol;
import com.tesis.futpal.modelos.estadisticas.EstadisticaLesion;
import com.tesis.futpal.modelos.estadisticas.EstadisticaTarjetaAmarilla;
import com.tesis.futpal.modelos.estadisticas.EstadisticaTarjetaRoja;
import com.tesis.futpal.modelos.plantel.JugadorEstadisticas;

import java.util.List;
import java.util.stream.Collectors;

public class ResponseObtenerEstadisticas {

    protected List<JugadorEstadisticas> golesJugadores;
    protected List<JugadorEstadisticas> tarjetasAmarillasJugadores;
    protected List<JugadorEstadisticas> tarjetasRojasJugadores;
    protected List<JugadorEstadisticas> lesionesJugadores;


    public List<JugadorEstadisticas> getGolesJugadores() {
        return golesJugadores;
    }

    public void setGolesJugadoresDesdeEstadistica(List<EstadisticaGol> golesJugadores) {
        this.golesJugadores = golesJugadores.stream().map(estadisticaGol -> {
            JugadorEstadisticas jugadorEstadisticas = new JugadorEstadisticas();
            jugadorEstadisticas.setNombre(estadisticaGol.getJugador().getNombre());
            jugadorEstadisticas.setEquipo(estadisticaGol.getEquipo());
            jugadorEstadisticas.setId(estadisticaGol.getJugador().getId());
            jugadorEstadisticas.setCantidad(estadisticaGol.getCantidad());
            return jugadorEstadisticas;
        }).collect(Collectors.toList());
    }

    public List<JugadorEstadisticas> getTarjetasAmarillasJugadores() {
        return tarjetasAmarillasJugadores;
    }

    public void setTarjetasAmarillasJugadoresDesdeEstadistica(List<EstadisticaTarjetaAmarilla> tarjetasAmarillasJugadores) {
        this.tarjetasAmarillasJugadores = tarjetasAmarillasJugadores.stream().map(estadisticaTarjetaAmarilla -> {
            JugadorEstadisticas jugadorEstadisticas = new JugadorEstadisticas();
            jugadorEstadisticas.setNombre(estadisticaTarjetaAmarilla.getJugador().getNombre());
            jugadorEstadisticas.setEquipo(estadisticaTarjetaAmarilla.getEquipo());
            jugadorEstadisticas.setId(estadisticaTarjetaAmarilla.getJugador().getId());
            return jugadorEstadisticas;
        }).collect(Collectors.toList());
    }

    public List<JugadorEstadisticas> getTarjetasRojasJugadores() {
        return tarjetasRojasJugadores;
    }

    public void setTarjetasRojasJugadoresDesdeEstadistica(List<EstadisticaTarjetaRoja> tarjetasRojasJugadores) {
        this.tarjetasRojasJugadores = tarjetasRojasJugadores.stream().map(estadisticaTarjetaRoja -> {
            JugadorEstadisticas jugadorEstadisticas = new JugadorEstadisticas();
            jugadorEstadisticas.setNombre(estadisticaTarjetaRoja.getJugador().getNombre());
            jugadorEstadisticas.setEquipo(estadisticaTarjetaRoja.getEquipo());
            jugadorEstadisticas.setId(estadisticaTarjetaRoja.getJugador().getId());
            return jugadorEstadisticas;
        }).collect(Collectors.toList());
    }

    public List<JugadorEstadisticas> getLesionesJugadores() {
        return lesionesJugadores;
    }

    public void setLesionesJugadoresDesdeEstadistica(List<EstadisticaLesion> lesionesJugadores) {
        this.lesionesJugadores = lesionesJugadores.stream().map(estadisticaLesion -> {
            JugadorEstadisticas jugadorEstadisticas = new JugadorEstadisticas();
            jugadorEstadisticas.setNombre(estadisticaLesion.getJugador().getNombre());
            jugadorEstadisticas.setEquipo(estadisticaLesion.getEquipo());
            jugadorEstadisticas.setId(estadisticaLesion.getJugador().getId());
            return jugadorEstadisticas;
        }).collect(Collectors.toList());
    }
}
