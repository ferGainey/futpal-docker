package com.tesis.futpal.comunicacion.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

public class RequestRegistrarLiga {
    @NotBlank(message = "El campo no puede ser nulo, vacío ni contener únicamente espacios.")
    private String nombreLiga;
    @NotEmpty
    private List<Integer> idEquipos;
    private boolean esIdaYVuelta;

    public String getNombreLiga() {
        return nombreLiga;
    }

    public void setNombreLiga(String nombreLiga) {
        this.nombreLiga = nombreLiga;
    }

    public List<Integer> getIdEquipos() {
        return idEquipos;
    }

    public void setIdEquipos(List<Integer> idEquipos) {
        this.idEquipos = idEquipos;
    }

    public boolean getEsIdaYVuelta() {
        return esIdaYVuelta;
    }

    public void setEsIdaYVuelta(boolean esIdaYVuelta) {
        this.esIdaYVuelta = esIdaYVuelta;
    }
}
