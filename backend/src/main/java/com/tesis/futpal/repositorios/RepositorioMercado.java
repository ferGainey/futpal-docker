package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.transferencia.Mercado;
import com.tesis.futpal.modelos.transferencia.PrestamoJugador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioMercado extends JpaRepository<Mercado, String> {

}
