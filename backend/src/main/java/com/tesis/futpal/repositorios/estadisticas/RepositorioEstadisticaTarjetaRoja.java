package com.tesis.futpal.repositorios.estadisticas;

import com.tesis.futpal.modelos.estadisticas.EstadisticaTarjetaRoja;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioEstadisticaTarjetaRoja extends JpaRepository<EstadisticaTarjetaRoja, Integer> {
    List<EstadisticaTarjetaRoja> findByPartidoId(Integer partidoId);

    void deleteByPartidoId(Integer partidoId);

    @Query(value = "select distinct er.id, er.partido_id, er.jugador_id, er.equipo_id from estadistica_tarjeta_roja er, partido_liga_base pl, liga l " +
            "where er.partido_id = pl.id and pl.liga_id = ?1", nativeQuery = true)
    List<EstadisticaTarjetaRoja> findByLiga(Integer ligaId);
}
