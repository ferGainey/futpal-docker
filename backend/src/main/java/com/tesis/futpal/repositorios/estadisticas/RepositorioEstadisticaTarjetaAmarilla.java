package com.tesis.futpal.repositorios.estadisticas;

import com.tesis.futpal.modelos.estadisticas.EstadisticaTarjetaAmarilla;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioEstadisticaTarjetaAmarilla extends JpaRepository<EstadisticaTarjetaAmarilla, Integer> {
    List<EstadisticaTarjetaAmarilla> findByPartidoId(Integer partidoId);

    void deleteByPartidoId(Integer partidoId);

    @Query(value = "select distinct ea.id, ea.partido_id, ea.jugador_id, ea.equipo_id from estadistica_tarjeta_amarilla ea, partido_liga_base pl, liga l " +
            "where ea.partido_id = pl.id and pl.liga_id = ?1", nativeQuery = true)
    List<EstadisticaTarjetaAmarilla> findByLiga(Integer ligaId);
}
