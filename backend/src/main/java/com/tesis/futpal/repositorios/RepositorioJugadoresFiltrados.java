package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.plantel.JugadorFiltrado;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class RepositorioJugadoresFiltrados {

    @PersistenceContext
    private EntityManager entityManager;

    public List<JugadorFiltrado> obtenerJugadoresFiltrados(Integer transfermarktId, String nombre) {
        ConstructorDeConsultaJugadoresFiltrados constructorConsulta = ConstructorDeConsultaJugadoresFiltrados.nuevo();
        String definicionConsulta = constructorConsulta.conIdTransfermarkt(transfermarktId)
                                                       .conNombreDeJugador(nombre)
                                                       .consulta()
                                                       .construir();

        Query consulta = entityManager.createNativeQuery(definicionConsulta, JugadorFiltrado.class);
        constructorConsulta.establecerParametros(consulta);

        return (List<JugadorFiltrado>) consulta.getResultList();
    }
}
