package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.liga.Liga;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RepositorioLigas extends CrudRepository<Liga, Integer> {

    List<Liga> findAllByOrderByIdDesc();
}
