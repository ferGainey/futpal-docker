package com.tesis.futpal.repositorios;

import javax.persistence.Query;

public class ConstructorDeConsultaJugadoresFiltrados {

    private String nombre;
    private Integer idTransfermarkt;
    private String consulta;

    public static ConstructorDeConsultaJugadoresFiltrados nuevo() {
        return new ConstructorDeConsultaJugadoresFiltrados();
    }

    public ConstructorDeConsultaJugadoresFiltrados conNombreDeJugador(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public ConstructorDeConsultaJugadoresFiltrados conIdTransfermarkt(Integer idTransfermarkt) {
        this.idTransfermarkt = idTransfermarkt;
        return this;
    }

    public ConstructorDeConsultaJugadoresFiltrados consulta() {
        consulta = "select   " +
                "j.id id, " +
                "j.id jugador_id, " +
                "e.nombre_equipo nombre_equipo_actual " +
                "from " +
                "jugador j, " +
                "equipo e " +
                "where " +
                "1 = 1 " +
                busquedaPorNombreDeJugador() +
                busquedaPorIdTransfermarkt() +
                "and e.id = j.equipo_id " +
                "and j.activo = true;";
        return this;
    }

    private String busquedaPorNombreDeJugador() {
        return (nombre != null && !nombre.equals("")) ? "and j.nombre ilike CONCAT('%',:nombre,'%') " : "";
    }

    private String busquedaPorIdTransfermarkt() {
        return (idTransfermarkt != null) ? "and j.id_transfermarkt = :idTransfermarkt " : "";
    }

    public String construir() {
        return consulta;
    }

    public void establecerParametros(Query consulta) {
        if (nombre != null) consulta.setParameter("nombre", nombre);
        if (idTransfermarkt != null) consulta.setParameter("idTransfermarkt", idTransfermarkt);
    }
}
