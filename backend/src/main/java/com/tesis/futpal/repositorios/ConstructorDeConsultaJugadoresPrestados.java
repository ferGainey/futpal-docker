package com.tesis.futpal.repositorios;

import javax.persistence.Query;

public class ConstructorDeConsultaJugadoresPrestados {

    private Integer equipo;
    private Integer numeroTemporada;
    private String consulta;

    public static ConstructorDeConsultaJugadoresPrestados nuevo() {
        return new ConstructorDeConsultaJugadoresPrestados();
    }

    public ConstructorDeConsultaJugadoresPrestados paraEquipo(Integer equipoId) {
        this.equipo = equipoId;
        return this;
    }

    public ConstructorDeConsultaJugadoresPrestados aPartirDeTemporada(Integer numeroTemporada) {
        this.numeroTemporada = numeroTemporada;
        return this;
    }

    public ConstructorDeConsultaJugadoresPrestados consulta() {
        consulta = "select   " +
                "j.id id, " +
                "j.id jugador_id, " +
                "e.nombre_equipo nombre_equipo_actual, " +
                "mj.numero_temporada, " +
                "mj.periodo_id periodo_id   " +
                "from   " +
                "jugador j,   " +
                "movimiento_jugador mj,   " +
                "prestamo_jugador pj, " +
                "equipo e  " +
                "where   " +
                "1 = 1   " +
                "and mj.jugador_id = j.id   " +
                "and j.estado_jugador_id = 2   " +
                "and j.activo = true   " +
                "and mj.id = pj.movimiento_jugador_id   " +
                "and mj.numero_temporada >= :temporadaActual   " +
                "and pj.es_fin_de_prestamo = true   " +
                "and mj.equipo_destino_id = :equipo " +
                "and e.id = j.equipo_id;";
        return this;
    }

    public String construir() {
        return consulta;
    }

    public void establecerParametros(Query consulta) {
        consulta.setParameter("equipo", equipo);
        consulta.setParameter("temporadaActual", numeroTemporada);
    }
}
