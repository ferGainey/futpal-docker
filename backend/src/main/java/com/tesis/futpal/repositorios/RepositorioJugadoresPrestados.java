package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.plantel.JugadorPrestado;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class RepositorioJugadoresPrestados {

    @PersistenceContext
    private EntityManager entityManager;

    public List<JugadorPrestado> obtenerJugadoresPrestados(Integer equipoId, Integer numeroTemporadaActual) {
        ConstructorDeConsultaJugadoresPrestados constructorConsulta = ConstructorDeConsultaJugadoresPrestados.nuevo();
        String definicionConsulta = constructorConsulta.consulta()
                                                       .paraEquipo(equipoId)
                                                       .aPartirDeTemporada(numeroTemporadaActual)
                                                       .construir();

        Query consulta = entityManager.createNativeQuery(definicionConsulta, JugadorPrestado.class);
        constructorConsulta.establecerParametros(consulta);

        return (List<JugadorPrestado>) consulta.getResultList();
    }
}
