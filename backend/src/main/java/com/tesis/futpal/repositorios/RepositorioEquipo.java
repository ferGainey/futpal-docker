package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.equipo.Equipo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RepositorioEquipo extends CrudRepository<Equipo, Long> {

    List<Equipo> findByIdUsuario(Long idUsuario);

    List<Equipo> findByEstadoEquipoIdOrderByNombreEquipo(Integer estadoEquipoId);

    List<Equipo> findByNombreEquipo(String nombreEquipo);
}
