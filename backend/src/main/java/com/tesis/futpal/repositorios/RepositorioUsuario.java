package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.usuario.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioUsuario extends CrudRepository<Usuario, Long> {
    List<Usuario> findByAliasUsuario(String aliasUsuario);

    List<Usuario> findByMailUsuario(String mailUsuario);
}
