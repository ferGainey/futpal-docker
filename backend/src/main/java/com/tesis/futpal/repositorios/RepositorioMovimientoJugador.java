package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.transferencia.MovimientoJugador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioMovimientoJugador extends JpaRepository<MovimientoJugador, Integer> {


    @Query(value = "select * from movimiento_jugador mj " +
            "where mj.numero_temporada = ?1 and mj.periodo_id = ?2", nativeQuery = true)
    List<MovimientoJugador> findByNumeroTemporadaYPeriodoId(Integer numeroTemporadaActual, Integer periodoId);
}
