package com.tesis.futpal.repositorios.estadisticas;

import com.tesis.futpal.modelos.estadisticas.EstadisticaLesion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioEstadisticaLesion extends JpaRepository<EstadisticaLesion, Integer> {
    List<EstadisticaLesion> findByPartidoId(Integer partidoId);

    void deleteByPartidoId(Integer partidoId);

    @Query(value = "select distinct el.id, el.partido_id, el.jugador_id, el.equipo_id from estadistica_lesion el, partido_liga_base pl, liga l " +
            "where el.partido_id = pl.id and pl.liga_id = ?1", nativeQuery = true)
    List<EstadisticaLesion> findByLiga(Integer ligaId);
}
