package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.equipo.Equipo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioBalance extends JpaRepository<Balance, Integer> {
    List<Balance> findByNumeroTemporadaLessThanEqualAndEquipo(Integer numeroTemporadaDetalleBalanceSolicitada, Equipo equipo);

    List<Balance> findByNumeroTemporadaAndEquipo(Integer numeroTemporada, Equipo equipo);

    @Query(value = "select " +
            "coalesce(sum(j.salario), 0) from jugador j " +
            "where " +
            "j.equipo_id = ?1 ;", nativeQuery = true)
    Integer obtenerTotalDeSalariosDeLosJugadoresActualesDelPlantel(Long equipoId);

    @Query(value = "select " +
            "coalesce(sum(j.salario)/2, 0) from jugador j, movimiento_jugador mj, transferencia t " +
            "where " +
            "mj.numero_temporada = ?1 " +
            "and mj.periodo_id = 2 " +
            "and mj.equipo_origen_id = ?2 " +
            "and j.id = mj.jugador_id " +
            "and t.id = mj.transferencia_id " +
            "and t.estado_transferencia_id = 3;", nativeQuery = true)
    Integer obtenerLaMitadDelTotalDeSalariosDeLosJugadoresQueSeFueronAMitadDeTemporada(Integer temporadaActual, Long equipoId);

    @Query(value = "select " +
            "coalesce(sum(j.salario)/2, 0) from jugador j, movimiento_jugador mj, transferencia t " +
            "where " +
            "mj.numero_temporada = ?1 " +
            "and mj.periodo_id = 2 " +
            "and mj.equipo_destino_id = ?2 " +
            "and j.id = mj.jugador_id " +
            "and t.id = mj.transferencia_id " +
            "and t.estado_transferencia_id = 3;", nativeQuery = true)
    Integer obtenerLaMitadDelTotalDeSalariosDeLosJugadoresQueLlegaronAMitadDeTemporada(Integer temporadaActual, Long equipoId);
}
