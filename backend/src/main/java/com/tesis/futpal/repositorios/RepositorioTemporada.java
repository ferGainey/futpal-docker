package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.temporada.Temporada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioTemporada extends JpaRepository<Temporada, Integer> {

    List<Temporada> findByActual(boolean esTemporadaActual);
}
