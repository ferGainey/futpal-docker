package com.tesis.futpal.repositorios.estadisticas;

import com.tesis.futpal.modelos.estadisticas.EstadisticaMvp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioEstadisticaMvp extends JpaRepository<EstadisticaMvp, Integer> {
    EstadisticaMvp findByPartidoId(Integer partidoId);

    void deleteByPartidoId(Integer partidoId);

    @Query(value = "select distinct em.id, em.partido_id, em.jugador_id, em.equipo_id from estadistica_mvp em, partido_liga_base pl, liga l " +
            "where em.partido_id = pl.id and pl.liga_id = ?1", nativeQuery = true)
    List<EstadisticaMvp> findByLiga(Integer ligaId);
}
