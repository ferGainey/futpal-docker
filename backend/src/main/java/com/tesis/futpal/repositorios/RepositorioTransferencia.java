package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.transferencia.EstadoTransferencia;
import com.tesis.futpal.modelos.transferencia.Transferencia;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Repository
public interface RepositorioTransferencia extends JpaRepository<Transferencia, Integer> {

    @Query(value = "select * from transferencia t " +
            "where t.usuario_origen_id = ?1 and t.estado_transferencia_id != 2 " +
            "order by t.id desc", nativeQuery = true)
    List<Transferencia> findByUsuarioOrigenAndEstadoTransferenciaNoCancelado(Long usuarioQueObtieneLasTransferenciasId);

    @Query(value = "select * from transferencia t " +
            "where t.usuario_destino_id = ?1 and t.estado_transferencia_id != 2 " +
            "order by t.id desc", nativeQuery = true)
    List<Transferencia> findByUsuarioDestinoAndEstadoTransferenciaNoCancelado(Long usuarioQueRecibeLasTransferenciasId);

    @Modifying
    @Transactional
    @Query(value = "update transferencia set estado_transferencia_id=2 where estado_transferencia_id=1", nativeQuery = true)
    void cancelarTransferenciasPendientes();

    @Modifying
    @Transactional
    @Query(value = "update transferencia set estado_transferencia_id = 2 " +
            "where id in " +
            "(select t.id from transferencia t, movimiento_jugador mj " +
            "where t.estado_transferencia_id=1 " +
            "and t.id = mj.transferencia_id " +
            "and mj.jugador_id in :jugadores)", nativeQuery = true)
    void cancelarTransferenciasQueInvolucrenALosJugadores(@Param("jugadores") Set<Long> jugadores);

    Page<Transferencia> findByEstadoTransferencia(EstadoTransferencia estadoTransferenciaConfirmada, Pageable paginado);
}
