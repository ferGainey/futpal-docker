package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.liga.LigaEquipos;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RepositorioLigaEquipos extends JpaRepository<LigaEquipos, Long> {
    List<LigaEquipos> findByIdLiga(Integer idLiga);
}
