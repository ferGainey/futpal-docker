package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.transferencia.PrestamoJugador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioPrestamo extends JpaRepository<PrestamoJugador, Integer> {

}
