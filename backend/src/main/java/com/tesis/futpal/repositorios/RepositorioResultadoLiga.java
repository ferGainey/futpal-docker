package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.liga.PartidoLigaBase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioResultadoLiga extends CrudRepository<PartidoLigaBase, Integer> {
    List<PartidoLigaBase> findByLigaId(Integer idLiga);
}
