package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioPeriodoTemporada extends JpaRepository<PeriodoTemporada, Integer> {

    PeriodoTemporada findByNombre(String nombrePeriodo);
}
