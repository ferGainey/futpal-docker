package com.tesis.futpal.repositorios;

import com.tesis.futpal.modelos.plantel.Jugador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioJugador extends JpaRepository<Jugador, Long> {
    Jugador findByNombre(String nombre);
}
