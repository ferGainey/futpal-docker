package com.tesis.futpal.repositorios.estadisticas;

import com.tesis.futpal.modelos.estadisticas.EstadisticaGol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioEstadisticaGol extends JpaRepository<EstadisticaGol, Integer> {
    List<EstadisticaGol> findByPartidoId(Integer partidoId);

    void deleteByPartidoId(Integer partidoId);

    @Query(value = "select distinct eg.id, eg.partido_id, eg.jugador_id, eg.equipo_id, eg.cantidad from estadistica_gol eg, partido_liga_base pl, liga l " +
            "where eg.partido_id = pl.id and pl.liga_id = ?1", nativeQuery = true)
    List<EstadisticaGol> findByLiga(Integer ligaId);
}
