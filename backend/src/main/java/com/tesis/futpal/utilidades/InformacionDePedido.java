package com.tesis.futpal.utilidades;

import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import com.tesis.futpal.seguridad.jwt.JwtUtils;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class InformacionDePedido {

    @Autowired
    private HttpServletRequest pedido;

    @Autowired
    private RepositorioUsuario repositorioUsuario;

    @Autowired
    private JwtUtils jwtUtils;

    public Usuario obtenerUsuario() {
        String token = this.obtenerTokenAutorizacion();
        String aliasUsuario = jwtUtils.obtenerAliasUsuarioAPartirDeTokenJwt(token);

        return repositorioUsuario.findByAliasUsuario(aliasUsuario).get(0);
    }

    private String obtenerTokenAutorizacion() {
        String headerAuthorization = pedido.getHeader("Authorization");
        if(headerAuthorization != null)
            return pedido.getHeader("Authorization").replace("Bearer ", "");
        else
            return null;
    }

}
