package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.repositorios.RepositorioPeriodoTemporada;
import com.tesis.futpal.repositorios.RepositorioTemporada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ServicioTemporada {

    private static final String FINAL_TEMPORADA = "Final";
    @Autowired
    private RepositorioTemporada repositorioTemporada;
    @Autowired
    private RepositorioPeriodoTemporada repositorioPeriodoTemporada;
    @Autowired
    private ServicioTransferencia servicioTransferencia;

    public Temporada obtenerTemporadaActual() {
        return repositorioTemporada.findByActual(true).get(0);
    }

    @Async
    public void avanzarTemporada() {
        Temporada temporadaActualAntesDeAvanzar = repositorioTemporada.findByActual(true).get(0);
        Temporada nuevaTemporadaActual = new Temporada();
        PeriodoTemporada periodoTemporadaActualAntesDeAvanzar = temporadaActualAntesDeAvanzar.getPeriodoTemporada();
        if (periodoTemporadaActualAntesDeAvanzar.getNombre().equals(FINAL_TEMPORADA)) {
            nuevaTemporadaActual.setNumero(temporadaActualAntesDeAvanzar.getNumero() + 1);
        }
        else {
            nuevaTemporadaActual.setNumero(temporadaActualAntesDeAvanzar.getNumero());
        }
        PeriodoTemporada nuevoPeriodoTemporada = repositorioPeriodoTemporada.findById(periodoTemporadaActualAntesDeAvanzar.getProximaId()).get();
        nuevaTemporadaActual.setPeriodoTemporada(nuevoPeriodoTemporada);
        nuevaTemporadaActual.setActual(true);
        temporadaActualAntesDeAvanzar.setActual(false);

        repositorioTemporada.save(temporadaActualAntesDeAvanzar);
        Temporada nuevaTemporadaActualGuardada = repositorioTemporada.save(nuevaTemporadaActual);
        servicioTransferencia.ejecutarMovimientos(nuevaTemporadaActualGuardada);
        servicioTransferencia.cancelarTransferenciasPendientes();
    }
}
