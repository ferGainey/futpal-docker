package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.EstadoEquipoEnum;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipo;
import com.tesis.futpal.comunicacion.requests.RequestRechazarEquipo;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioEquipo {

    @Autowired
    private RepositorioEquipo repositorioEquipo;

    public List<Equipo> obtenerTodosLosEquiposAprobados() {
       return repositorioEquipo.findByEstadoEquipoIdOrderByNombreEquipo(EstadoEquipoEnum.APROBADO.getId());
    }

    public List<Equipo> obtenerTodosLosEquiposPendienteAprobacion() {
        return repositorioEquipo.findByEstadoEquipoIdOrderByNombreEquipo(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());
    }

    public void aprobarEquipo(long idEquipo) {
        Equipo equipo = repositorioEquipo.findById(idEquipo).get();
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.APROBADO.getId());
        equipo.setEstadoEquipo(estadoEquipo);
        equipo.setMotivoUltimoCambioEstado("");
        repositorioEquipo.save(equipo);
    }

    public void rechazarEquipo(RequestRechazarEquipo requestRechazarEquipo) {
        Equipo equipo = repositorioEquipo.findById(requestRechazarEquipo.getEquipoId()).get();
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.RECHAZADO.getId());
        equipo.setEstadoEquipo(estadoEquipo);
        equipo.setMotivoUltimoCambioEstado(requestRechazarEquipo.getMotivoRechazo());
        repositorioEquipo.save(equipo);
    }

    public void solicitarAprobacionEquipo(long idEquipo) {
        Equipo equipo = repositorioEquipo.findById(idEquipo).get();
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());
        equipo.setEstadoEquipo(estadoEquipo);
        repositorioEquipo.save(equipo);
    }
}
