package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.excepciones.JugadorYaTransferidoException;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.comunicacion.requests.transferencia.RequestCargaTransferencia;
import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.modelos.transferencia.*;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.*;
import com.tesis.futpal.utilidades.InformacionDePedido;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ServicioTransferencia {

    private static final Integer ESTADO_TRANSFERENCIA_PENDIENTE = 1;
    private static final Integer ESTADO_TRANSFERENCIA_CANCELADA = 2;
    private static final Integer ESTADO_TRANSFERENCIA_CONFIRMADA = 3;
    private static final Integer ID_TRANSFERENCIA_TIPO_PRESTAMO = 2;
    private static final String CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA = "CARGA_TRANSFERENCIAS";

    Logger logger = LoggerFactory.getLogger(ServicioTransferencia.class);

    @Autowired
    private RepositorioTransferencia repositorioTransferencia;
    @Autowired
    private RepositorioMovimientoJugador repositorioMovimientoJugador;
    @Autowired
    private RepositorioBalance repositorioBalance;
    @Autowired
    private RepositorioJugador repositorioJugador;
    @Autowired
    private RepositorioTemporada repositorioTemporada;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private RepositorioPrestamo repositorioPrestamo;
    @Autowired
    private RepositorioMercado repositorioMercado;
    @Autowired
    private ValidadorTransferencia validadorTransferencia;
    @Autowired
    private InformacionDePedido informacionDePedido;
    @Autowired
    private CreadorDetalle creadorDetalle;
    @Autowired
    private ServicioPlantel servicioPlantel;

    public void cargarTransferencia(RequestCargaTransferencia requestCargaTransferencia) throws JugadorYaTransferidoException {
        Usuario usuarioQueRealizaLaTransferencia = informacionDePedido.obtenerUsuario();
        Long idUsuarioQueRecibeLaTransferencia = repositorioEquipo.findById(requestCargaTransferencia.getOtroEquipoId()).get().getIdUsuario();
        Usuario usuarioQueRecibeLaTransferencia = repositorioUsuario.findById(idUsuarioQueRecibeLaTransferencia).get();
        Transferencia transferenciaGuardada = null;
        transferenciaGuardada = guardarMovimientosDeJugadores(requestCargaTransferencia, usuarioQueRealizaLaTransferencia, usuarioQueRecibeLaTransferencia, transferenciaGuardada);

        guardarBalances(requestCargaTransferencia, usuarioQueRealizaLaTransferencia, usuarioQueRecibeLaTransferencia, transferenciaGuardada);
    }

    public void ejecutarMovimientos(Temporada nuevaTemporadaActualGuardada) {
        List<MovimientoJugador> movimientosJugadores = repositorioMovimientoJugador.findByNumeroTemporadaYPeriodoId(nuevaTemporadaActualGuardada.getNumero(), nuevaTemporadaActualGuardada.getPeriodoTemporada().getId());
        movimientosJugadores.stream().forEach(movimientoJugador -> {
            if (movimientoJugador.getTransferencia().getEstadoTransferencia().getId().equals(ESTADO_TRANSFERENCIA_CONFIRMADA)) {
                try {
                    movimientoJugador.ejecutar(repositorioJugador);
                } catch (Exception exception) {
                    logger.error(String.valueOf(exception));
                }
            }
        });
    }

    public List<DetalleTransferencia> obtenerTransferenciasDeUnEquipo() {
        Usuario usuarioQueObtieneLasTransferencias = informacionDePedido.obtenerUsuario();
        List<DetalleTransferencia> transferenciasPedidas = repositorioTransferencia.findByUsuarioOrigenAndEstadoTransferenciaNoCancelado(usuarioQueObtieneLasTransferencias.getId()).stream()
                .map(creadorDetalle::crearAPartirDe)
                .collect(Collectors.toList());

        List<DetalleTransferencia> transferenciasRecibidas = repositorioTransferencia.findByUsuarioDestinoAndEstadoTransferenciaNoCancelado(usuarioQueObtieneLasTransferencias.getId()).stream()
                .map(creadorDetalle::crearAPartirDe)
                .collect(Collectors.toList());

        List<DetalleTransferencia> todasLasTransferenciasDelUsuario = new ArrayList<>();
        todasLasTransferenciasDelUsuario.addAll(transferenciasPedidas);
        todasLasTransferenciasDelUsuario.addAll(transferenciasRecibidas);

        todasLasTransferenciasDelUsuario.sort(Comparator.comparing(DetalleTransferencia::getId, Collections.reverseOrder()));

        return todasLasTransferenciasDelUsuario;
    }

    public List<DetalleTransferencia> obtenerTransferenciasConfirmadasDeTodosLosEquipos(Integer pagina) {
        EstadoTransferencia estadoTransferenciaConfirmada = new EstadoTransferencia();
        estadoTransferenciaConfirmada.setId(ESTADO_TRANSFERENCIA_CONFIRMADA);

        int ITEMS_POR_PAGINA = 20;
        Sort ordenamiento = Sort.by(Sort.Direction.DESC, "id");
        PageRequest paginado = PageRequest.of(pagina - 1, ITEMS_POR_PAGINA, ordenamiento);
        Page<Transferencia> paginaConTransferencias = repositorioTransferencia.findByEstadoTransferencia(estadoTransferenciaConfirmada, paginado);
        return paginaConTransferencias.getContent().stream()
                .map(creadorDetalle::crearAPartirDe)
                .collect(Collectors.toList());
    }

    public void rechazarTransferencia(Integer idTransferencia) {
        Usuario usuarioQueRechaza = informacionDePedido.obtenerUsuario();
        Transferencia transferenciaARechazar = repositorioTransferencia.findById(idTransferencia).get();
        if((transferenciaARechazar.getUsuarioOrigen().getId().equals(usuarioQueRechaza.getId()) ||
                transferenciaARechazar.getUsuarioDestino().getId().equals(usuarioQueRechaza.getId())) &&
                transferenciaARechazar.getEstadoTransferencia().getId().equals(ESTADO_TRANSFERENCIA_PENDIENTE)) {
            EstadoTransferencia estadoCancelada = new EstadoTransferencia();
            estadoCancelada.setId(ESTADO_TRANSFERENCIA_CANCELADA);
            transferenciaARechazar.setEstadoTransferencia(estadoCancelada);
            repositorioTransferencia.save(transferenciaARechazar);
        }
        else {
            //ToDo: devolver error. No es urgente notificar, porque si llega acá es por estar intentando saltear validaciones
        }
    }

    public void confirmarTransferencia(Integer idTransferencia) {
        Usuario usuarioQueConfirma = informacionDePedido.obtenerUsuario();
        Transferencia transferenciaAConfirmar = repositorioTransferencia.findById(idTransferencia).get();
        if(transferenciaAConfirmar.getUsuarioDestino().getId().equals(usuarioQueConfirma.getId()) &&
                transferenciaAConfirmar.getEstadoTransferencia().getId().equals(ESTADO_TRANSFERENCIA_PENDIENTE)) {
            EstadoTransferencia estadoConfirmada = new EstadoTransferencia();
            estadoConfirmada.setId(ESTADO_TRANSFERENCIA_CONFIRMADA);
            transferenciaAConfirmar.setEstadoTransferencia(estadoConfirmada);
            Transferencia transferenciaGuardada = repositorioTransferencia.save(transferenciaAConfirmar);
            this.ejecutarMovimientos(repositorioTemporada.findByActual(true).get(0));
            this.cambiarEstadosJugadoresTransferenciasFuturas(idTransferencia);
            this.cancelarTransferenciasPendientesQueInvolucrenALosJugadores(transferenciaGuardada.getMovimientos());
            // ToDo: Agregar método (ver si conviene otro objeto) que revise si el movimiento es de la temporada y período actual, en caso de que no le cambia el estado al jugador a Prestamo o traspaso acordado basandose en el tipo de transferencia
        }
        else {
            //ToDo: devolver error. No es urgente notificar, porque si llega acá es por estar intentando saltear validaciones
        }
    }

    public void cancelarTransferenciasPendientesQueInvolucrenALosJugadores(List<MovimientoJugador> movimientos) {
        Set<Long> jugadores = movimientos.stream()
                .map(movimientoJugador -> movimientoJugador.getJugador().getId()).collect(Collectors.toSet());
        repositorioTransferencia.cancelarTransferenciasQueInvolucrenALosJugadores(jugadores);
    }

    public void cancelarTransferenciasPendientes() {
        repositorioTransferencia.cancelarTransferenciasPendientes();
    }

    private void guardarBalances(RequestCargaTransferencia requestCargaTransferencia, Usuario usuarioQueRealizaLaTransferencia, Usuario usuarioQueRecibeLaTransferencia, Transferencia transferenciaGuardada) {
        if (requestCargaTransferencia.getMontosTransferencias() != null && !requestCargaTransferencia.getMontosTransferencias().isEmpty()) {
            List<Balance> balances = requestCargaTransferencia.crearBalances();
            if (transferenciaGuardada == null) {
                Transferencia transferencia = crearTransferencia(usuarioQueRealizaLaTransferencia, usuarioQueRecibeLaTransferencia);
                transferenciaGuardada = repositorioTransferencia.save(transferencia);
            }

            Transferencia finalTransferenciaGuardada1 = transferenciaGuardada;
            balances.forEach(balance -> {
                balance.setTransferencia(finalTransferenciaGuardada1);
                repositorioBalance.save(balance);
            });
        }
    }

    private void cambiarEstadosJugadoresTransferenciasFuturas(Integer idTransferencia) {
        Transferencia transferencia = repositorioTransferencia.findById(idTransferencia).get();
        Temporada temporadaActual = repositorioTemporada.findByActual(true).get(0);
        transferencia.getMovimientos().forEach(movimientoJugador -> {
            Jugador jugador = movimientoJugador.getJugador();

            if (!movimientoJugador.getNumeroTemporada().equals(temporadaActual.getNumero()) ||
                    !movimientoJugador.getPeriodo().getId().equals(temporadaActual.getPeriodoTemporada().getId())
            ) {
                jugador.actualizarEstadoTransferenciaFutura(movimientoJugador.getTipoMovimiento(), ID_TRANSFERENCIA_TIPO_PRESTAMO);
                if (jugador.getEstadoJugador() != null) {
                    repositorioJugador.save(jugador);
                }
            }
        });
    }

    private Transferencia guardarMovimientosDeJugadores(RequestCargaTransferencia requestCargaTransferencia, Usuario usuarioQueRealizaLaTransferencia, Usuario usuarioQueRecibeLaTransferencia, Transferencia transferenciaGuardada) throws JugadorYaTransferidoException {
        if (hayJugadoresATransferir(requestCargaTransferencia)) {
            validadorTransferencia.validarTraspasos(requestCargaTransferencia.getJugadoresTransferencia());
            List<MovimientoJugador> movimientosJugadores = requestCargaTransferencia.crearMovimientosJugadores();
            Transferencia transferencia = crearTransferencia(usuarioQueRealizaLaTransferencia, usuarioQueRecibeLaTransferencia);
            transferenciaGuardada = repositorioTransferencia.save(transferencia);
            Transferencia finalTransferenciaGuardada = transferenciaGuardada;
            movimientosJugadores.forEach(movimientoJugador -> {
                movimientoJugador.setTransferencia(finalTransferenciaGuardada);
                MovimientoJugador movimientoJugadorGuardado = repositorioMovimientoJugador.save(movimientoJugador);
                guardarPrestamoSiCorresponde(movimientoJugadorGuardado);
            });
        }
        return transferenciaGuardada;
    }

    private boolean hayJugadoresATransferir(RequestCargaTransferencia requestCargaTransferencia) {
        return (requestCargaTransferencia.getJugadoresTransferencia() != null &&
                !requestCargaTransferencia.getJugadoresTransferencia().isEmpty()) ||
                (requestCargaTransferencia.getJugadoresPrestamoTransferencia() != null &&
                !requestCargaTransferencia.getJugadoresPrestamoTransferencia().isEmpty());
    }

    private void guardarPrestamoSiCorresponde(MovimientoJugador movimientoJugador) {
        if (movimientoJugador.getTipoMovimiento().getId().equals(ID_TRANSFERENCIA_TIPO_PRESTAMO)) {
            repositorioPrestamo.save(PrestamoJugador.crearAPartirDe(movimientoJugador, servicioPlantel));
        }
    }

    private Transferencia crearTransferencia(Usuario usuarioQueRealizaLaTransferencia, Usuario usuarioQueRecibeLaTransferencia) {
        Transferencia transferencia = new Transferencia();
        transferencia.setUsuarioOrigen(usuarioQueRealizaLaTransferencia);
        transferencia.setUsuarioDestino(usuarioQueRecibeLaTransferencia);
        EstadoTransferencia estadoTransferencia = new EstadoTransferencia();
        estadoTransferencia.setId(ESTADO_TRANSFERENCIA_PENDIENTE);
        transferencia.setEstadoTransferencia(estadoTransferencia);
        return transferencia;
    }

    public boolean cambiarEstadoDeMercado(boolean mercadoActivo) {
        boolean mercadoEstaActivoActualizado = false;
        Optional<Mercado> mercadoOptional = repositorioMercado.findById(CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA);
        if (mercadoOptional.isPresent()) {
            Mercado mercadoActualizado = mercadoOptional.get();
            mercadoActualizado.setActivo(mercadoActivo);
            mercadoEstaActivoActualizado = repositorioMercado.save(mercadoActualizado).isActivo();
        }
        return mercadoEstaActivoActualizado;
    }

    public boolean obtenerEstadoDeMercado() {
        return repositorioMercado.findById(CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA).get().isActivo();
    }
}
