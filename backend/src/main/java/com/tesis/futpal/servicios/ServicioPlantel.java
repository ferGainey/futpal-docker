package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.plantel.JugadorFiltrado;
import com.tesis.futpal.modelos.plantel.JugadorPrestado;
import com.tesis.futpal.comunicacion.requests.RequestAgregarJugador;
import com.tesis.futpal.comunicacion.requests.RequestEdicionJugador;
import com.tesis.futpal.comunicacion.requests.RequestObtenerJugadoresEquipo;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.*;
import com.tesis.futpal.utilidades.InformacionDePedido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ServicioPlantel {

    private static final Long JUGADOR_DISPONIBLE = 1L;
    private static final Long ID_ESTADO_PRESTADO = 2L;
    private static final Long ID_ESTADO_TRASPASADO = 4L;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private RepositorioJugador repositorioJugador;
    @Autowired
    private RepositorioJugadoresPrestados repositorioJugadoresPrestados;
    @Autowired
    private RepositorioJugadoresFiltrados repositorioJugadoresFiltrados;
    @Autowired
    private RepositorioTemporada repositorioTemporada;
    @Autowired
    private InformacionDePedido informacionDelPedido;

    public void agregarJugador(RequestAgregarJugador requestAgregarJugador) {
        Optional<Equipo> optionalEquipo = repositorioEquipo.findById(requestAgregarJugador.getEquipoId());
        if (optionalEquipo.isPresent()) {
            Jugador jugador = new Jugador(requestAgregarJugador.getNombre(), requestAgregarJugador.getValor(),
                    requestAgregarJugador.getSalario(), requestAgregarJugador.getMedia(),
                    requestAgregarJugador.getFechaNacimiento(), optionalEquipo.get(), requestAgregarJugador.getIdTransfermarkt());
            repositorioJugador.save(jugador);
        }
    }

    public List<Jugador> obtenerJugadoresEquipo(RequestObtenerJugadoresEquipo requestObtenerJugador) {
        Optional<Equipo> optionalEquipo = repositorioEquipo.findById(requestObtenerJugador.getEquipoId());
        List<Jugador> jugadores = new ArrayList<>();
        if (optionalEquipo.isPresent()) {
            jugadores = optionalEquipo.get().getJugadores().stream().filter(Jugador::isActivo).collect(Collectors.toList());
            jugadores.sort(Comparator.comparing(Jugador::getFechaNacimiento));
        }
        return jugadores;
    }

    @Transactional
    public void borrarJugador(Long idJugador) {
        Usuario usuarioQueHaceElPedido = informacionDelPedido.obtenerUsuario();
        Jugador jugadorABorrar = repositorioJugador.findById(idJugador).get();
        Long idDeUsuario = usuarioQueHaceElPedido.getId();
        if (usuarioQueHaceElPedido.esAdministrador() || sePuedeBorrarJugador(idDeUsuario, jugadorABorrar)) {
            jugadorABorrar.setActivo(false);
            repositorioJugador.save(jugadorABorrar);
        }
    }

    private boolean sePuedeBorrarJugador(Long usuarioQueHaceElPedido, Jugador jugadorABorrar) {
        return (jugadorABorrar.getEstadoJugador().getId().equals(JUGADOR_DISPONIBLE) &&
                 jugadorABorrar.getEquipo().getId().equals(repositorioEquipo.findByIdUsuario(usuarioQueHaceElPedido).get(0).getId()));
    }

    public void editarJugador(RequestEdicionJugador requestEdicionJugador) {
        Jugador jugador = repositorioJugador.findById(requestEdicionJugador.getJugadorId()).get();
        jugador.setNombre(requestEdicionJugador.getNombre());
        jugador.setFechaNacimiento(requestEdicionJugador.getFechaNacimiento());
        jugador.setMedia(requestEdicionJugador.getMedia());
        jugador.setSalario(requestEdicionJugador.getSalario());
        jugador.setValor(requestEdicionJugador.getValor());
        jugador.setIdTransfermarkt(requestEdicionJugador.getIdTransfermarkt());

        repositorioJugador.save(jugador);
    }

    public Jugador obtenerDatosJugador(Long jugadorId) {
        return repositorioJugador.findById(jugadorId).get();
    }

    public List<JugadorPrestado> obtenerJugadoresPrestados(Integer equipoId) {
        Integer numeroTemporadaActual = repositorioTemporada.findByActual(true).get(0).getNumero();
        return repositorioJugadoresPrestados.obtenerJugadoresPrestados(equipoId, numeroTemporadaActual);
    }
    
    public List<Jugador> obtenerJugadoresDuenioDePase(Integer equipoId) {
        RequestObtenerJugadoresEquipo requestObtenerJugadoresEquipo = new RequestObtenerJugadoresEquipo();
        requestObtenerJugadoresEquipo.setEquipoId(equipoId.longValue());
        List<Jugador> jugadoresDisponibles = this.obtenerJugadoresEquipo(requestObtenerJugadoresEquipo);
        List<Jugador> jugadoresDelEquipo = jugadoresDisponibles
                .stream()
                .filter(jugador -> jugador.isActivo() && !jugador.getEstadoJugador().getId().equals(ID_ESTADO_PRESTADO)
                        && !jugador.getEstadoJugador().getId().equals(ID_ESTADO_TRASPASADO)).collect(Collectors.toList());
        List<Jugador> jugadoresPrestadosAOtrosEquipos = this.obtenerJugadoresPrestados(equipoId)
                .stream()
                .map(JugadorPrestado::getJugador).collect(Collectors.toList());
        jugadoresDelEquipo.addAll(jugadoresPrestadosAOtrosEquipos);
        jugadoresDelEquipo.sort(Comparator.comparing(Jugador::getFechaNacimiento));
        return jugadoresDelEquipo;
    }

    public List<JugadorFiltrado> obtenerJugadoresFiltrados(Integer transfermarktId, String nombre) {
        return repositorioJugadoresFiltrados.obtenerJugadoresFiltrados(transfermarktId, nombre);
    }
}
