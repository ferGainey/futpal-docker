package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.balance.BalanceSalario;
import com.tesis.futpal.modelos.balance.CalculadoraDetalleDeBalance;
import com.tesis.futpal.modelos.balance.DetalleBalance;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.excepciones.LigaInexistenteException;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.repositorios.RepositorioBalance;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServicioBalance {

    private static final Integer ID_ESTADO_TRANSFERENCIA_CONFIRMADO = 3;
    private static final Integer ID_PERIODO_MITAD = 2;
    @Autowired
    private RepositorioBalance repositorioBalance;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private CalculadoraDetalleDeBalance calculadoraDetalleDeBalance;
    @Autowired
    private ServicioLiga servicioLiga;
    @Autowired
    private ServicioTemporada servicioTemporada;

    public void cargarBalance(Balance balance) {
        repositorioBalance.save(balance);
    }

    public void cargarBalanceParaUnaLiga(List<Balance> balances) {
        repositorioBalance.saveAll(balances);
    }

    public List<DetalleBalance> obtenerDetalleBalanceDeTodosLosEquipos(Integer numeroTemporada) {
        List<Equipo> equipos = (List<Equipo>) repositorioEquipo.findAll();
        return equipos.stream().map(equipo -> {
            List<Balance> balancesEquipo = repositorioBalance.findByNumeroTemporadaLessThanEqualAndEquipo(numeroTemporada, equipo);
            List<Balance> balancesFiltrados = balancesEquipo.stream().filter(this::filtrarBalancesNoConfirmados).collect(Collectors.toList());
            return calculadoraDetalleDeBalance.calcular(equipo, balancesFiltrados, numeroTemporada);
        }).sorted(Comparator.comparing((DetalleBalance detalle) -> detalle.getEquipo().getNombreEquipo())).collect(Collectors.toList());
    }

    public DetalleBalance obtenerDetalleBalanceDeUnEquipo(Integer numeroTemporada, Integer equipoId) {
        Equipo equipo = repositorioEquipo.findById(equipoId.longValue()).get();
        List<Balance> balancesEquipo = repositorioBalance.findByNumeroTemporadaLessThanEqualAndEquipo(numeroTemporada, equipo);
        List<Balance> balancesFiltrados = balancesEquipo.stream().filter(this::filtrarBalancesNoConfirmados).collect(Collectors.toList());
        return calculadoraDetalleDeBalance.calcular(equipo, balancesFiltrados, numeroTemporada);
    }

    public List<Balance> obtenerBalancesDeUnEquipoEnTemporada(Integer numeroTemporada, Integer equipoId) {
        Equipo equipo = repositorioEquipo.findById(equipoId.longValue()).get();
        return repositorioBalance.findByNumeroTemporadaAndEquipo(numeroTemporada, equipo).stream().filter(this::filtrarBalancesNoConfirmados).collect(Collectors.toList());
    }

    public List<BalanceSalario> obtenerSalariosDeLaLiga(Integer ligaId) throws LigaInexistenteException {
        List<Equipo> equipos = servicioLiga.obtenerEquipos(ligaId);
        List<Balance> salariosDeLosEquipos = new ArrayList<>();
        Integer numeroTemporadaActual = servicioTemporada.obtenerTemporadaActual().getNumero();
        equipos.forEach(equipo -> {
            Integer salarioTotalJugadoresActuales = repositorioBalance.obtenerTotalDeSalariosDeLosJugadoresActualesDelPlantel(equipo.getId());
            Integer mitadTotalSalarioJugadoresQueSeFueron = repositorioBalance.obtenerLaMitadDelTotalDeSalariosDeLosJugadoresQueSeFueronAMitadDeTemporada(numeroTemporadaActual, equipo.getId());
            Integer mitadTotalSalarioJugadoresQueLlegaron = repositorioBalance.obtenerLaMitadDelTotalDeSalariosDeLosJugadoresQueLlegaronAMitadDeTemporada(numeroTemporadaActual, equipo.getId());
            salariosDeLosEquipos.add(crearBalanceSalario(numeroTemporadaActual, salarioTotalJugadoresActuales,
                    mitadTotalSalarioJugadoresQueSeFueron, mitadTotalSalarioJugadoresQueLlegaron, equipo));
        });
        List<BalanceSalario> balancesSalarios = salariosDeLosEquipos.stream().map(BalanceSalario::crearAPartirDe).collect(Collectors.toList());
        return balancesSalarios;
    }

    private Balance crearBalanceSalario(Integer numeroTemporadaActual, Integer salarioTotalJugadoresActuales, Integer mitadTotalSalarioJugadoresQueSeFueron,
                                        Integer mitadTotalSalarioJugadoresQueLlegaron, Equipo equipo) {
        Balance balance = new Balance();
        balance.setNumeroTemporada(numeroTemporadaActual);

        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(ID_PERIODO_MITAD);
        balance.setPeriodo(periodoTemporada);

        balance.setEquipo(equipo);
        balance.setMonto(salarioTotalJugadoresActuales + mitadTotalSalarioJugadoresQueSeFueron - mitadTotalSalarioJugadoresQueLlegaron);

        return balance;
    }

    private boolean filtrarBalancesNoConfirmados(Balance balance) {
        if (balance.getTransferencia() != null) {
            return balance.getTransferencia().getEstadoTransferencia().getId().equals(ID_ESTADO_TRANSFERENCIA_CONFIRMADO);
        }
        return true;
    }
}
