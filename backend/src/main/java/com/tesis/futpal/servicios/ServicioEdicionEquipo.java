package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.EstadoEquipoEnum;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipo;
import com.tesis.futpal.excepciones.UsuarioInexistenteException;
import com.tesis.futpal.comunicacion.requests.RequestEditarDatosEquipo;
import com.tesis.futpal.comunicacion.responses.ResponseDatosEquipo;
import com.tesis.futpal.comunicacion.responses.ResponseEquipoActualizado;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioEdicionEquipo {

    private static final String MENSAJE_NO_TIENE_EQUIPO_ASOCIADO = "No posee un equipo asociado aún. Ingrese el nombre que desea para su equipo en \"Editar Equipo\" y confirme para asociarlo.";
    @Autowired
    private RepositorioUsuario repositorioUsuario;

    @Autowired
    private RepositorioEquipo repositorioEquipo;

    public ResponseDatosEquipo obtenerDatosEquipo(Long idUsuario) throws UsuarioInexistenteException {
        verificarSiElUsuarioExiste(idUsuario);
        Equipo equipo = obtenerEquipoAsociado(idUsuario);
        ResponseDatosEquipo responseDatosEquipo = new ResponseDatosEquipo();
        if (equipo == null) {
            responseDatosEquipo.setIdEquipo(null);
            responseDatosEquipo.setNombreEquipo("");
            responseDatosEquipo.setMensaje(MENSAJE_NO_TIENE_EQUIPO_ASOCIADO);
            return responseDatosEquipo;
        }
        responseDatosEquipo.setIdEquipo(equipo.getId());
        responseDatosEquipo.setEstadoEquipoId(equipo.getEstadoEquipo().getId());
        responseDatosEquipo.setNombreEquipo(equipo.getNombreEquipo());
        responseDatosEquipo.setMensajeEstadoEquipo(equipo.getEstadoEquipo().getDescripcion());
        responseDatosEquipo.setMotivoCambioEstado(equipo.getMotivoUltimoCambioEstado());
        responseDatosEquipo.setMensaje("");
        return responseDatosEquipo;
    }


    public ResponseEquipoActualizado actualizarEquipo(RequestEditarDatosEquipo requestEditarDatosEquipo) throws UsuarioInexistenteException {
        verificarSiElUsuarioExiste(requestEditarDatosEquipo.getIdUsuario());
        Equipo equipo = obtenerEquipoAsociado(requestEditarDatosEquipo.getIdUsuario());
        if (equipo == null) {
            equipo = new Equipo();
            equipo.setNombreEquipo(requestEditarDatosEquipo.getNuevoNombreEquipo());
            equipo.setIdUsuario(requestEditarDatosEquipo.getIdUsuario());
            EstadoEquipo estadoEquipo = new EstadoEquipo();
            estadoEquipo.setId(EstadoEquipoEnum.INICIAL.getId());
            equipo.setEstadoEquipo(estadoEquipo);
            equipo.setMotivoUltimoCambioEstado("");
            equipo = repositorioEquipo.save(equipo);
            return new ResponseEquipoActualizado(equipo);
        }
        equipo.setNombreEquipo(requestEditarDatosEquipo.getNuevoNombreEquipo());
        equipo = repositorioEquipo.save(equipo);
        return new ResponseEquipoActualizado(equipo);
    }

    private Equipo obtenerEquipoAsociado(Long idUsuario) {
        List<Equipo> equipo = repositorioEquipo.findByIdUsuario(idUsuario);
        if (equipo.isEmpty()) {
            return null;
        }
        return equipo.get(0);
    }

    private void verificarSiElUsuarioExiste(Long idUsuario) throws UsuarioInexistenteException {
        if (!repositorioUsuario.findById(idUsuario).isPresent()) {
            throw new UsuarioInexistenteException();
        }
    }
}
