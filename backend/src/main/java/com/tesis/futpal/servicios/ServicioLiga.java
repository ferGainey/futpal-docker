package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.liga.*;
import com.tesis.futpal.excepciones.LigaInexistenteException;
import com.tesis.futpal.excepciones.PartidoInexistenteException;
import com.tesis.futpal.comunicacion.requests.RequestActualizarResultado;
import com.tesis.futpal.comunicacion.requests.RequestRegistrarLiga;
import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ServicioLiga {

    private static final Integer PUNTOS_PARTIDO_GANADO = 3;
    private static final Integer PUNTOS_PARTIDO_PERDIDO = 0;
    private static final Integer PUNTOS_PARTIDO_EMPATADO = 1;
    @Autowired
    private RepositorioLigas repositorioLigas;
    @Autowired
    private RepositorioLigaEquipos repositorioLigaEquipos;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private RepositorioResultadoLiga repositorioResultadoLiga;
    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private RepositorioTemporada repositorioTemporada;
    @Autowired
    private GeneradorPartidos generadorPartidos;
    @Autowired
    private ComparadorPosicion comparadorPosicion;

    public Liga registrarLiga(RequestRegistrarLiga requestRegistrarLiga) {
        Liga liga = new Liga(requestRegistrarLiga.getNombreLiga());
        Temporada temporadaActual = repositorioTemporada.findByActual(true).get(0);
        liga.setTemporada(temporadaActual);
        liga = repositorioLigas.save(liga);
        guardarEquiposEnLiga(requestRegistrarLiga.getIdEquipos(), liga.getId());
        List<PartidoLigaBase> partidos = generadorPartidos.generar(requestRegistrarLiga.getIdEquipos(), requestRegistrarLiga.getEsIdaYVuelta());
        final Liga ligaGuardada = liga;
        partidos.forEach(partidoLigaBase -> partidoLigaBase.setLiga(ligaGuardada));
        repositorioResultadoLiga.saveAll(partidos);
        return liga;
    }

    public List<Liga> obtenerLigasDisponibles() {
        return (List<Liga>) repositorioLigas.findAllByOrderByIdDesc();
    }

    private void guardarEquiposEnLiga(List<Integer> idEquipos, Integer idLiga) {
        idEquipos.stream().forEach((idEquipo) -> {
            LigaEquipos ligaEquipos = new LigaEquipos();
            ligaEquipos.setIdLiga(idLiga.intValue());
            ligaEquipos.setIdEquipo(idEquipo);
            repositorioLigaEquipos.save(ligaEquipos);
        });
    }

    public List<Equipo> obtenerEquipos(Integer idLiga) throws LigaInexistenteException {
        if (repositorioLigas.findById(idLiga).isPresent()){
            List<Long> idEquipos = repositorioLigaEquipos.findByIdLiga(idLiga)
                    .stream()
                    .map((equipoLiga) -> equipoLiga.getIdEquipo().longValue())
                    .collect(Collectors.toList());
            return (List<Equipo>) repositorioEquipo.findAllById(idEquipos);
        }
        throw new LigaInexistenteException();
    }

    public List<PartidoLigaBase> obtenerPartidos(Integer idLiga) {
       return repositorioResultadoLiga.findByLigaId(idLiga);
    }

    public void actualizarResultado(RequestActualizarResultado requestActualizarResultado) throws PartidoInexistenteException {
        Optional<PartidoLigaBase> optionalPartidoLigaBase = repositorioResultadoLiga.findById(requestActualizarResultado.getIdPartido());
        if (optionalPartidoLigaBase.isPresent()) {
            PartidoLigaBase partidoLigaBase = optionalPartidoLigaBase.get();
            partidoLigaBase.setGolesEquipoLocal(requestActualizarResultado.getGolesLocal());
            partidoLigaBase.setGolesEquipoVisitante(requestActualizarResultado.getGolesVisitante());
            EstadoPartido estadoPartidoFinalizado = new EstadoPartido();
            estadoPartidoFinalizado.setId(EstadoPartidoEnum.FINALIZADO.getId());
            partidoLigaBase.setEstadoPartido(estadoPartidoFinalizado);
            Usuario usuarioEditor = new Usuario();
            usuarioEditor.setId(requestActualizarResultado.getUsuarioEditorId().longValue());
            partidoLigaBase.setUltimoUsuarioEditor(usuarioEditor);
            repositorioResultadoLiga.save(partidoLigaBase);
        }
        else {
            throw new PartidoInexistenteException();
        }
    }

    public List<PosicionLiga> obtenerPosiciones(Integer idLiga) throws LigaInexistenteException {
        List<PartidoLigaBase> partidosLigaBase = this.obtenerPartidos(idLiga);
        List<Equipo> equiposLiga = this.obtenerEquipos(idLiga);

        List<PosicionLiga> posicionesLiga = new ArrayList<>();
        equiposLiga.stream()
                .forEach(equipo -> {
                    PosicionLiga posicionLiga = new PosicionLiga();
                    posicionLiga.setEquipo(equipo);
                    posicionLiga.setPuntosLiga(0);
                    posicionLiga.setDiferenciaDeGoles(0);
                    posicionLiga.setGolesAFavor(0);
                    posicionLiga.setGolesEnContra(0);
                    posicionLiga.setPartidosJugados(0);
                    posicionesLiga.add(posicionLiga);
                });

        partidosLigaBase.stream()
                .forEach(partidoLiga -> {
                    if (partidoLiga.getEstadoPartido().getId().equals(EstadoPartidoEnum.FINALIZADO.getId())) {
                        actualizarPosicion(partidoLiga, posicionesLiga);
                    }
                });

        posicionesLiga.sort((posicion1, posicion2) -> comparadorPosicion.comparar(posicion1, posicion2));
        Collections.reverse(posicionesLiga);

        return posicionesLiga;
    }

    private void actualizarPosicion(PartidoLigaBase partidoLiga, List<PosicionLiga> posicionesLiga) {
        Integer idEquipoLocal = partidoLiga.getEquipoLocal().getId().intValue();
        Integer idEquipoVisitante = partidoLiga.getEquipoVisitante().getId().intValue();
        PosicionLiga posicionEquipoLocal = posicionesLiga.stream().filter(posicionLiga -> posicionLiga.getEquipo().getId().equals(idEquipoLocal.longValue())).findFirst().get();
        PosicionLiga posicionEquipoVisitante = posicionesLiga.stream().filter(posicionLiga -> posicionLiga.getEquipo().getId().equals(idEquipoVisitante.longValue())).findFirst().get();

        posicionEquipoLocal.setPartidosJugados(posicionEquipoLocal.getPartidosJugados() + 1);
        posicionEquipoLocal.setGolesAFavor(posicionEquipoLocal.getGolesAFavor() + partidoLiga.getGolesEquipoLocal());
        posicionEquipoLocal.setGolesEnContra(posicionEquipoLocal.getGolesEnContra() + partidoLiga.getGolesEquipoVisitante());
        posicionEquipoLocal.setDiferenciaDeGoles(posicionEquipoLocal.getGolesAFavor() - posicionEquipoLocal.getGolesEnContra());
        posicionEquipoLocal.setPuntosLiga(posicionEquipoLocal.getPuntosLiga() + calcularPuntosPartido(partidoLiga.getGolesEquipoLocal(), partidoLiga.getGolesEquipoVisitante()));

        posicionEquipoVisitante.setPartidosJugados(posicionEquipoVisitante.getPartidosJugados() + 1);
        posicionEquipoVisitante.setGolesAFavor(posicionEquipoVisitante.getGolesAFavor() + partidoLiga.getGolesEquipoVisitante());
        posicionEquipoVisitante.setGolesEnContra(posicionEquipoVisitante.getGolesEnContra() + partidoLiga.getGolesEquipoLocal());
        posicionEquipoVisitante.setDiferenciaDeGoles(posicionEquipoVisitante.getGolesAFavor() - posicionEquipoVisitante.getGolesEnContra());
        posicionEquipoVisitante.setPuntosLiga(posicionEquipoVisitante.getPuntosLiga() + calcularPuntosPartido(partidoLiga.getGolesEquipoVisitante(), partidoLiga.getGolesEquipoLocal()));
    }

    private Integer calcularPuntosPartido(Integer golesEquipoPropio, Integer golesEquipoRival) {
        if (golesEquipoPropio > golesEquipoRival) {
            return PUNTOS_PARTIDO_GANADO;
        }
        else if (golesEquipoPropio < golesEquipoRival) {
            return PUNTOS_PARTIDO_PERDIDO;
        }
        else {
            return PUNTOS_PARTIDO_EMPATADO;
        }
    }
}
