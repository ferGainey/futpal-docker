package com.tesis.futpal.servicios;

import com.tesis.futpal.excepciones.CamposNulosException;
import com.tesis.futpal.excepciones.CamposVaciosException;
import com.tesis.futpal.excepciones.UsuarioExistenteException;
import com.tesis.futpal.comunicacion.requests.RequestRegistrarUsuario;
import com.tesis.futpal.modelos.usuario.Rol;
import com.tesis.futpal.modelos.usuario.TipoRol;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServicioRegistrarUsuario {

    private static final String MENSAJE_ALIAS_EXISTENTE = "El alias ya existe.";
    private static final String MENSAJE_MAIL_EXISTENTE = "El mail ya existe.";

    @Autowired
    private RepositorioUsuario repositorioUsuario;

    @Autowired
    private PasswordEncoder encoder;

    public Usuario registrarUsuario(RequestRegistrarUsuario requestRegistrarUsuario) throws CamposNulosException, CamposVaciosException, UsuarioExistenteException {
        verificarDatosNoNulos(requestRegistrarUsuario);
        requestRegistrarUsuario.eliminarEspaciosDelPrincipioYDelFinal();
        verificarDatosNoVacios(requestRegistrarUsuario);
        verificarSiElUsuarioYaExiste(requestRegistrarUsuario);
        Usuario usuario = crearUsuarioAPartirDe(requestRegistrarUsuario);
        agregarRolAUsuario(usuario);
        usuario = repositorioUsuario.save(usuario);
        return usuario;
    }

    private void agregarRolAUsuario(Usuario usuario) {
        Rol rolUsuario = new Rol();
        rolUsuario.setId(TipoRol.ROL_USUARIO.getId());
        List<Rol> rolesUsuario = new ArrayList<>();
        rolesUsuario.add(rolUsuario);
        usuario.setRoles(rolesUsuario);
    }

    private void verificarSiElUsuarioYaExiste(RequestRegistrarUsuario requestRegistrarUsuario) throws UsuarioExistenteException {
        if (!repositorioUsuario.findByAliasUsuario(requestRegistrarUsuario.getAlias()).isEmpty()) {
            throw new UsuarioExistenteException(MENSAJE_ALIAS_EXISTENTE);
        }
        else if (!repositorioUsuario.findByMailUsuario(requestRegistrarUsuario.getMail()).isEmpty()) {
            throw new UsuarioExistenteException(MENSAJE_MAIL_EXISTENTE);
        }
    }

    private void verificarDatosNoVacios(RequestRegistrarUsuario requestRegistrarUsuario) throws CamposVaciosException {
        if (!requestRegistrarUsuario.verificarQueTodosLosCamposSeanNoVacios()) {
            throw new CamposVaciosException();
        }
    }

    private void verificarDatosNoNulos(RequestRegistrarUsuario requestRegistrarUsuario) throws CamposNulosException {
        if (!requestRegistrarUsuario.verificarQueTodosLosCamposSeanNoNulos()) {
            throw new CamposNulosException();
        }
    }

    private Usuario crearUsuarioAPartirDe(RequestRegistrarUsuario requestRegistrarUsuario) {
        Usuario usuario = new Usuario();
        usuario.setAliasUsuario(requestRegistrarUsuario.getAlias());
        usuario.setContraseniaUsuario(encoder.encode(requestRegistrarUsuario.getContrasenia()));
        usuario.setApellidoUsuario(requestRegistrarUsuario.getApellido());
        usuario.setNombreUsuario(requestRegistrarUsuario.getNombre());
        usuario.setMailUsuario(requestRegistrarUsuario.getMail());
        return usuario;
    }

}
