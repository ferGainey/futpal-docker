package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.liga.PartidoLigaBase;
import com.tesis.futpal.modelos.estadisticas.*;
import com.tesis.futpal.modelos.plantel.JugadorEstadisticas;
import com.tesis.futpal.comunicacion.requests.RequestActualizarEstadisticas;
import com.tesis.futpal.comunicacion.requests.RequestObtenerEstadisticasPartido;
import com.tesis.futpal.comunicacion.responses.ResponseObtenerEstadisticasLiga;
import com.tesis.futpal.comunicacion.responses.ResponseObtenerEstadisticasPartido;
import com.tesis.futpal.repositorios.RepositorioLigas;
import com.tesis.futpal.repositorios.RepositorioResultadoLiga;
import com.tesis.futpal.repositorios.estadisticas.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ServicioEstadistica {
    @Autowired
    private RepositorioEstadisticaTarjetaAmarilla repositorioEstadisticaTarjetaAmarilla;
    @Autowired
    private RepositorioEstadisticaGol repositorioEstadisticaGol;
    @Autowired
    private RepositorioEstadisticaTarjetaRoja repositorioEstadisticaTarjetaRoja;
    @Autowired
    private RepositorioEstadisticaLesion repositorioEstadisticaLesion;
    @Autowired
    private RepositorioEstadisticaMvp repositorioEstadisticaMvp;
    @Autowired
    private RepositorioResultadoLiga repositorioResultadoLiga;
    @Autowired
    private RepositorioLigas repositorioLiga;

    @Transactional
    public void actualizarEstadisticas(RequestActualizarEstadisticas requestActualizarEstadisticasValido) {
        PartidoLigaBase partidoId = repositorioResultadoLiga.findById(requestActualizarEstadisticasValido.getPartidoId()).get();
        actualizarEstadisticasTarjetasAmarillas(requestActualizarEstadisticasValido.getTarjetasAmarillasJugadores(), partidoId);
        actualizarEstadisticasTarjetasRojas(requestActualizarEstadisticasValido.getTarjetasRojasJugadores(), partidoId);
        actualizarEstadisticasLesiones(requestActualizarEstadisticasValido.getLesionesJugadores(), partidoId);
        actualizarEstadisticasMvp(requestActualizarEstadisticasValido.getMvpJugador(), partidoId);
        actualizarEstadisticasGoles(requestActualizarEstadisticasValido.getGolesJugadores(), partidoId);
    }

    public ResponseObtenerEstadisticasPartido obtenerEstadisticas(RequestObtenerEstadisticasPartido requestObtenerEstadisticasPartido) {
        ResponseObtenerEstadisticasPartido responseObtenerEstadisticasPartido = new ResponseObtenerEstadisticasPartido();
        Integer partidoId = requestObtenerEstadisticasPartido.getPartidoId();

        if (repositorioResultadoLiga.findById(partidoId).isPresent()) {
            responseObtenerEstadisticasPartido.setGolesJugadoresDesdeEstadistica(repositorioEstadisticaGol.findByPartidoId(partidoId));
            responseObtenerEstadisticasPartido.setTarjetasAmarillasJugadoresDesdeEstadistica(repositorioEstadisticaTarjetaAmarilla.findByPartidoId(partidoId));
            responseObtenerEstadisticasPartido.setTarjetasRojasJugadoresDesdeEstadistica(repositorioEstadisticaTarjetaRoja.findByPartidoId(partidoId));
            responseObtenerEstadisticasPartido.setLesionesJugadoresDesdeEstadistica(repositorioEstadisticaLesion.findByPartidoId(partidoId));
            responseObtenerEstadisticasPartido.setMvpJugador(repositorioEstadisticaMvp.findByPartidoId(partidoId));
        }

        return responseObtenerEstadisticasPartido;
    }

    public ResponseObtenerEstadisticasLiga obtenerEstadisticasLiga(Integer ligaId) {
        //toDo agregar una excepcion cuando no exista la liga
        ResponseObtenerEstadisticasLiga responseObtenerEstadisticasLiga = new ResponseObtenerEstadisticasLiga();
        if (repositorioLiga.findById(ligaId).isPresent()) {
            responseObtenerEstadisticasLiga.setGolesJugadores(agruparEstadisticasGoles(repositorioEstadisticaGol.findByLiga(ligaId)));
            responseObtenerEstadisticasLiga.setTarjetasAmarillas(agruparEstadisticasAmarillas(repositorioEstadisticaTarjetaAmarilla.findByLiga(ligaId)));
            responseObtenerEstadisticasLiga.setTarjetasRojasJugadores(agruparEstadisticasRojas(repositorioEstadisticaTarjetaRoja.findByLiga(ligaId)));
            responseObtenerEstadisticasLiga.setLesionesJugadores(agruparEstadisticasLesiones(repositorioEstadisticaLesion.findByLiga(ligaId)));
            responseObtenerEstadisticasLiga.setMvpJugadores(agruparEstadisticasMvp(repositorioEstadisticaMvp.findByLiga(ligaId)));
        }
        return responseObtenerEstadisticasLiga;
    }

    private List<JugadorEstadisticas> agruparEstadisticasMvp(List<EstadisticaMvp> estadisticaMvps) {
        List<JugadorEstadisticas> mvps = new ArrayList<>();
        HashMap<Long, Integer> mvpsJugadores = new HashMap<>();

        for (EstadisticaMvp estadisticaMvpActual : estadisticaMvps) {
            Long jugadorActualId = estadisticaMvpActual.getJugador().getId();
            if (mvpsJugadores.containsKey(jugadorActualId)) {
                mvpsJugadores.put(jugadorActualId, mvpsJugadores.get(jugadorActualId) + 1);
            }
            else {
                mvpsJugadores.put(jugadorActualId, 1);
            }
        }

        estadisticaMvps.stream().forEach(estadisticaMvp -> {
            Integer cantidad = mvpsJugadores.get(estadisticaMvp.getJugador().getId());
            List<Long> agregadosIds = mvps.stream().map(JugadorEstadisticas::getId).collect(Collectors.toList());
            if (!agregadosIds.contains(estadisticaMvp.getJugador().getId())) {
                mvps.add(generarJugadorEstadisticaLesion(estadisticaMvp.getJugador().getNombre(), estadisticaMvp.getEquipo(),
                        estadisticaMvp.getJugador().getId(),cantidad));
            }
        });

        mvps.sort(Comparator.comparing(JugadorEstadisticas::getCantidad).reversed());
        return mvps;
    }

    private List<JugadorEstadisticas> agruparEstadisticasLesiones(List<EstadisticaLesion> estadisticaLesiones) {
        List<JugadorEstadisticas> lesiones = new ArrayList<>();
        HashMap<Long, Integer> lesionesJugadores = new HashMap<>();

        for (EstadisticaLesion estadisticaLesionActual : estadisticaLesiones) {
            Long jugadorActualId = estadisticaLesionActual.getJugador().getId();
            if (lesionesJugadores.containsKey(jugadorActualId)) {
                lesionesJugadores.put(jugadorActualId, lesionesJugadores.get(jugadorActualId) + 1);
            }
            else {
                lesionesJugadores.put(jugadorActualId, 1);
            }
        }

        estadisticaLesiones.stream().forEach(estadisticaLesion -> {
            Integer cantidad = lesionesJugadores.get(estadisticaLesion.getJugador().getId());
            lesiones.add(generarJugadorEstadisticaLesion(estadisticaLesion.getJugador().getNombre(), estadisticaLesion.getEquipo(),
                    estadisticaLesion.getJugador().getId(),cantidad));
        });

        lesiones.sort(Comparator.comparing(JugadorEstadisticas::getCantidad).reversed());
        return lesiones;
    }

    private JugadorEstadisticas generarJugadorEstadisticaLesion(String nombre, Equipo equipo, Long id, Integer cantidad) {
        JugadorEstadisticas jugadorEstadisticas = new JugadorEstadisticas();
        jugadorEstadisticas.setNombre(nombre);
        jugadorEstadisticas.setEquipo(equipo);
        jugadorEstadisticas.setId(id);
        jugadorEstadisticas.setCantidad(cantidad);
        return jugadorEstadisticas;
    }

    private List<JugadorEstadisticas> agruparEstadisticasRojas(List<EstadisticaTarjetaRoja> estadisticaTarjetasRojas) {
        List<JugadorEstadisticas> rojas = new ArrayList<>();
        HashMap<Long, Integer> rojasJugadores = new HashMap<>();

        for (EstadisticaTarjetaRoja estadisticaTarjetaRoja : estadisticaTarjetasRojas) {
            Long jugadorActualId = estadisticaTarjetaRoja.getJugador().getId();
            if (rojasJugadores.containsKey(jugadorActualId)) {
                rojasJugadores.put(jugadorActualId, rojasJugadores.get(jugadorActualId) + 1);
            }
            else {
                rojasJugadores.put(jugadorActualId, 1);
            }
        }

        estadisticaTarjetasRojas.stream().forEach(estadisticaRoja -> {
            Integer cantidad = rojasJugadores.get(estadisticaRoja.getJugador().getId());
            List<Long> agregadosIds = rojas.stream().map(JugadorEstadisticas::getId).collect(Collectors.toList());
            if (!agregadosIds.contains(estadisticaRoja.getJugador().getId())) {
                rojas.add(generarJugadorEstadisticaLesion(estadisticaRoja.getJugador().getNombre(), estadisticaRoja.getEquipo(),
                        estadisticaRoja.getJugador().getId(),cantidad));
            }
        });

        rojas.sort(Comparator.comparing(JugadorEstadisticas::getCantidad).reversed());
        return rojas;
    }

    private List<JugadorEstadisticas> agruparEstadisticasAmarillas(List<EstadisticaTarjetaAmarilla> estadisticaTarjetaAmarillas) {
        List<JugadorEstadisticas> amarillas = new ArrayList<>();
        HashMap<Long, Integer> amarillasJugadores = new HashMap<>();

        for (EstadisticaTarjetaAmarilla estadisticaTarjetaAmarilla : estadisticaTarjetaAmarillas) {
            Long jugadorActualId = estadisticaTarjetaAmarilla.getJugador().getId();
            if (amarillasJugadores.containsKey(jugadorActualId)) {
                amarillasJugadores.put(jugadorActualId, amarillasJugadores.get(jugadorActualId) + 1);
            }
            else {
                amarillasJugadores.put(jugadorActualId, 1);
            }
        }

        estadisticaTarjetaAmarillas.stream().forEach(estadisticaTarjetaAmarilla -> {
            Integer cantidad = amarillasJugadores.get(estadisticaTarjetaAmarilla.getJugador().getId());
            List<Long> agregadosIds = amarillas.stream().map(JugadorEstadisticas::getId).collect(Collectors.toList());
            if (!agregadosIds.contains(estadisticaTarjetaAmarilla.getJugador().getId())) {
                amarillas.add(generarJugadorEstadisticaLesion(estadisticaTarjetaAmarilla.getJugador().getNombre(), estadisticaTarjetaAmarilla.getEquipo(),
                        estadisticaTarjetaAmarilla.getJugador().getId(),cantidad));
            }
        });

        amarillas.sort(Comparator.comparing(JugadorEstadisticas::getCantidad).reversed());
        return amarillas;
    }

    private List<JugadorEstadisticas> agruparEstadisticasGoles(List<EstadisticaGol> estadisticasGoles) {
        List<JugadorEstadisticas> goles = new ArrayList<>();
        HashMap<Long, Integer> golesJugadores = new HashMap<>();

        for (EstadisticaGol estadisticaGolActual : estadisticasGoles) {
            Long jugadorActualId = estadisticaGolActual.getJugador().getId();
            if (golesJugadores.containsKey(jugadorActualId)) {
                golesJugadores.put(jugadorActualId, golesJugadores.get(jugadorActualId) + estadisticaGolActual.getCantidad());
            }
            else {
                golesJugadores.put(jugadorActualId, estadisticaGolActual.getCantidad());
            }
        }

        estadisticasGoles.stream().forEach(estadisticaGol -> {
            Integer cantidad = golesJugadores.get(estadisticaGol.getJugador().getId());
            List<Long> agregadosIds = goles.stream().map(JugadorEstadisticas::getId).collect(Collectors.toList());
            if (!agregadosIds.contains(estadisticaGol.getJugador().getId())) {
                goles.add(generarJugadorEstadisticaLesion(estadisticaGol.getJugador().getNombre(), estadisticaGol.getEquipo(),
                        estadisticaGol.getJugador().getId(),cantidad));
            }
        });

        goles.sort(Comparator.comparing(JugadorEstadisticas::getCantidad).reversed());
        return goles;
    }

    //ToDo: refactor para duplicar menos codigo
    private void actualizarEstadisticasTarjetasAmarillas(List<JugadorEstadisticas> tarjetasAmarillasJugadores, PartidoLigaBase partido) {
        repositorioEstadisticaTarjetaAmarilla.deleteByPartidoId(partido.getId());
        if (tarjetasAmarillasJugadores != null && !tarjetasAmarillasJugadores.isEmpty()) {
            tarjetasAmarillasJugadores.stream().forEach(jugadorEstadisticas -> {
                EstadisticaTarjetaAmarilla estadisticaTarjetaAmarilla = new EstadisticaTarjetaAmarilla();
                estadisticaTarjetaAmarilla.setPartido(partido);
                estadisticaTarjetaAmarilla.setEquipo(new Equipo(jugadorEstadisticas.getEquipo().getId()));
                estadisticaTarjetaAmarilla.setJugador(new Jugador(jugadorEstadisticas.getId()));
                repositorioEstadisticaTarjetaAmarilla.save(estadisticaTarjetaAmarilla);
            });
        }
    }

    private void actualizarEstadisticasTarjetasRojas(List<JugadorEstadisticas> tarjetasRojasJugadores, PartidoLigaBase partido) {
        repositorioEstadisticaTarjetaRoja.deleteByPartidoId(partido.getId());
        if (tarjetasRojasJugadores != null && !tarjetasRojasJugadores.isEmpty()) {
            tarjetasRojasJugadores.stream().forEach(jugadorEstadisticas -> {
                EstadisticaTarjetaRoja estadisticaTarjetaRoja = new EstadisticaTarjetaRoja();
                estadisticaTarjetaRoja.setPartido(partido);
                estadisticaTarjetaRoja.setEquipo(new Equipo(jugadorEstadisticas.getEquipo().getId()));
                estadisticaTarjetaRoja.setJugador(new Jugador(jugadorEstadisticas.getId()));
                repositorioEstadisticaTarjetaRoja.save(estadisticaTarjetaRoja);
            });
        }
    }

    private void actualizarEstadisticasLesiones(List<JugadorEstadisticas> lesionesJugadores, PartidoLigaBase partido) {
        repositorioEstadisticaLesion.deleteByPartidoId(partido.getId());
        if (lesionesJugadores != null && !lesionesJugadores.isEmpty()) {
            lesionesJugadores.stream().forEach(jugadorEstadisticas -> {
                EstadisticaLesion estadisticaLesion = new EstadisticaLesion();
                estadisticaLesion.setPartido(partido);
                estadisticaLesion.setEquipo(new Equipo(jugadorEstadisticas.getEquipo().getId()));
                estadisticaLesion.setJugador(new Jugador(jugadorEstadisticas.getId()));
                repositorioEstadisticaLesion.save(estadisticaLesion);
            });
        }
    }

    private void actualizarEstadisticasMvp(JugadorEstadisticas mvpJugador, PartidoLigaBase partido) {
        repositorioEstadisticaMvp.deleteByPartidoId(partido.getId());
        if (mvpJugador != null) {
            EstadisticaMvp estadisticaMvp = new EstadisticaMvp();
            estadisticaMvp.setPartido(partido);
            estadisticaMvp.setEquipo(new Equipo(mvpJugador.getEquipo().getId()));
            estadisticaMvp.setJugador(new Jugador(mvpJugador.getId()));
            repositorioEstadisticaMvp.save(estadisticaMvp);
        }
    }

    private void actualizarEstadisticasGoles(List<JugadorEstadisticas> golesJugadores, PartidoLigaBase partido) {
        repositorioEstadisticaGol.deleteByPartidoId(partido.getId());
        if (golesJugadores != null && !golesJugadores.isEmpty()) {
            golesJugadores.stream().forEach(jugadorEstadisticasRequest -> {
                EstadisticaGol estadisticaGol = new EstadisticaGol();
                estadisticaGol.setPartido(partido);
                estadisticaGol.setEquipo(new Equipo(jugadorEstadisticasRequest.getEquipo().getId()));
                estadisticaGol.setJugador(new Jugador(jugadorEstadisticasRequest.getId()));
                estadisticaGol.setCantidad(jugadorEstadisticasRequest.getCantidad());
                repositorioEstadisticaGol.save(estadisticaGol);
            });
        }
    }
}
