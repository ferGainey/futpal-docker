package com.tesis.futpal.excepciones;

public class UsuarioExistenteException extends Exception {
    private static final String MENSAJE_ERROR = "No se ha podido crear el usuario. ";

    public UsuarioExistenteException(String mensajeErrorEspecifico) {
        super(MENSAJE_ERROR + mensajeErrorEspecifico);
    }
}