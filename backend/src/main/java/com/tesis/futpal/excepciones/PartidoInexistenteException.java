package com.tesis.futpal.excepciones;

public class PartidoInexistenteException extends Exception {

    private static final String MENSAJE_PARTIDO_INEXISTENTE = "El partido no existe.";

    public PartidoInexistenteException() {
        super(MENSAJE_PARTIDO_INEXISTENTE);
    }
}
