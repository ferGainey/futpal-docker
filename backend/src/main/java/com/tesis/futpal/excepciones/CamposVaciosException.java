package com.tesis.futpal.excepciones;

public class CamposVaciosException extends Exception {

    private static final String MENSAJE_CAMPOS_VACIOS = "No puede haber campos vacíos.";

    public CamposVaciosException() {
        super(MENSAJE_CAMPOS_VACIOS);
    }

}
