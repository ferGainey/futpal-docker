package com.tesis.futpal.excepciones;

public class UsuarioInexistenteException extends Exception {
    private static final String MENSAJE_USUARIO_INEXISTENTE = "El id ingresado no está vinculado a ningún usuario.";

    public UsuarioInexistenteException() {
        super(MENSAJE_USUARIO_INEXISTENTE);
    }
}
