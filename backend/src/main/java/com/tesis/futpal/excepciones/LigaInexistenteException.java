package com.tesis.futpal.excepciones;

public class LigaInexistenteException extends Exception {

    private static final String MENSAJE_LIGA_INEXISTENTE = "La liga no existe.";

    public LigaInexistenteException() {
        super(MENSAJE_LIGA_INEXISTENTE);
    }
}
