package com.tesis.futpal.excepciones;

public class CamposNulosException extends Exception {

    private static final String MENSAJE_CAMPOS_NULOS = "No puede haber campos nulos.";

    public CamposNulosException() {
        super(MENSAJE_CAMPOS_NULOS);
    }

}
