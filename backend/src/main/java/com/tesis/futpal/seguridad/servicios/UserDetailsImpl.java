package com.tesis.futpal.seguridad.servicios;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tesis.futpal.modelos.usuario.Usuario;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String alias;

	private String email;

	@JsonIgnore
	private String contrasenia;

	private Collection<? extends GrantedAuthority> autorizacionesPorRoles;

	public UserDetailsImpl(Long id, String alias, String email, String contrasenia,
						   Collection<? extends GrantedAuthority> autorizacionesPorRoles) {
		this.id = id;
		this.alias = alias;
		this.email = email;
		this.contrasenia = contrasenia;
		this.autorizacionesPorRoles = autorizacionesPorRoles;
	}

	public static UserDetailsImpl build(Usuario usuario) {
		List<GrantedAuthority> autorizacionesPorRoles = usuario.getRoles().stream()
				.map(rol -> new SimpleGrantedAuthority(rol.getDescripcion()))
				.collect(Collectors.toList());

		return new UserDetailsImpl(
				usuario.getId(),
				usuario.getAliasUsuario(),
				usuario.getMailUsuario(),
				usuario.getContraseniaUsuario(),
				autorizacionesPorRoles);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return autorizacionesPorRoles;
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String getPassword() {
		return contrasenia;
	}

	@Override
	public String getUsername() {
		return alias;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserDetailsImpl user = (UserDetailsImpl) o;
		return Objects.equals(id, user.id);
	}
}
