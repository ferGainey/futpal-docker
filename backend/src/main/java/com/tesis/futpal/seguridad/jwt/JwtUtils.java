package com.tesis.futpal.seguridad.jwt;

import com.tesis.futpal.seguridad.servicios.UserDetailsImpl;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtUtils {
	private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

	@Value("${futpal.app.jwtSecret}")
	private String jwtSecret;

	@Value("${futpal.app.jwtExpirationMs}")
	private int jwtExpirationMs;

	public String generarTokenJwt(Authentication autenticacion) {

		UserDetailsImpl userPrincipal = (UserDetailsImpl) autenticacion.getPrincipal();

		return Jwts.builder()
				.setSubject((userPrincipal.getUsername()))
				.setIssuedAt(new Date())
				.setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}

	public String obtenerAliasUsuarioAPartirDeTokenJwt(String token) {
		return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
	}

	public boolean validarTokenJwt(String token) {
		try {
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
			return true;
		} catch (SignatureException e) {
			logger.error("Firma JWT inválida: {}", e.getMessage());
		} catch (MalformedJwtException e) {
			logger.error("Token JWT inválido: {}", e.getMessage());
		} catch (ExpiredJwtException e) {
			logger.error("El Token JWT expiró: {}", e.getMessage());
		} catch (UnsupportedJwtException e) {
			logger.error(" El Token JWT no es soportado: {}", e.getMessage());
		} catch (IllegalArgumentException e) {
			logger.error("El string del JWT está vacío: {}", e.getMessage());
		}

		return false;
	}
}
