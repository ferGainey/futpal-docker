package com.tesis.futpal.seguridad.servicios;


import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	RepositorioUsuario repositorioUsuario;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String alias) throws UsernameNotFoundException {
		Usuario usuario = repositorioUsuario.findByAliasUsuario(alias).get(0);

		return UserDetailsImpl.build(usuario);
	}

}
