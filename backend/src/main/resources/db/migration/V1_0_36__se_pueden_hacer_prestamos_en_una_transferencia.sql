CREATE TABLE prestamo_jugador(
   id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
   movimiento_jugador_id integer,
   es_fin_de_prestamo boolean,
   PRIMARY KEY (id),
   FOREIGN KEY (movimiento_jugador_id) REFERENCES movimiento_jugador(id)
);

CREATE TABLE estado_jugador(
   id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
   nombre varchar(50),
   PRIMARY KEY (id)
);

INSERT INTO estado_jugador (nombre) VALUES
('DISPONIBLE'),
('PRESTADO'),
('PRESTAMO_ACORDADO'),
('TRASPASO_ACORDADO');

ALTER TABLE jugador
ADD COLUMN estado_jugador_id integer;

CREATE TABLE tipo_movimiento(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    nombre varchar(50),
    PRIMARY KEY (id)
);

INSERT INTO tipo_movimiento (nombre) VALUES
('TRASPASO'),
('PRESTAMO');

ALTER TABLE movimiento_jugador
ADD COLUMN tipo_movimiento_id integer;