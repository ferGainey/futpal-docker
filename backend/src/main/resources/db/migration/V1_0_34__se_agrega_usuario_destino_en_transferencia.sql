ALTER TABLE transferencia
DROP COLUMN usuario_id;

ALTER TABLE transferencia
ADD COLUMN usuario_origen_id integer;

ALTER TABLE transferencia
ADD COLUMN usuario_destino_id integer;

ALTER TABLE transferencia ADD CONSTRAINT usuario_origen_id FOREIGN KEY (usuario_origen_id)
        REFERENCES usuario (id);

ALTER TABLE transferencia ADD CONSTRAINT usuario_destino_id FOREIGN KEY (usuario_destino_id)
        REFERENCES usuario (id);