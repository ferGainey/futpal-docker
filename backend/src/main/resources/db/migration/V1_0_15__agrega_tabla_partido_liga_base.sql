CREATE TABLE partido_liga_base(
   id SERIAL,
   id_liga integer,
   id_equipo_local integer,
   id_equipo_visitante integer,
   id_estado integer,
   goles_equipo_local integer,
   goles_equipo_visitante integer,
   numero_fecha integer,
   PRIMARY KEY (id),
   FOREIGN KEY (id_liga) REFERENCES liga(id)
);