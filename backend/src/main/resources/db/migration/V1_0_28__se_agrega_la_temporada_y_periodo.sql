CREATE TABLE periodo_temporada(
   id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
   nombre VARCHAR(50),
   proxima_id integer,
   PRIMARY KEY (id)
);

INSERT INTO periodo_temporada (nombre, proxima_id) VALUES
('Principio', 2),
('Mitad', 3),
('Final', 1);

CREATE TABLE temporada(
   id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
   numero VARCHAR(50),
   actual BOOLEAN,
   periodo_temporada_id integer,
   PRIMARY KEY (id),
   FOREIGN KEY (periodo_temporada_id) REFERENCES periodo_temporada(id)
);

INSERT INTO temporada (numero, actual, periodo_temporada_id) VALUES
(1, true, 1);