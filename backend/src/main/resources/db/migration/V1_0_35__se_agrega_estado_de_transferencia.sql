CREATE TABLE estado_transferencia(
   id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
   nombre VARCHAR(50),
   PRIMARY KEY (id)
);

INSERT INTO estado_transferencia (nombre) VALUES
('PENDIENTE'),
('CANCELADA'),
('CONFIRMADA');

ALTER TABLE transferencia
ADD COLUMN estado_transferencia_id integer;