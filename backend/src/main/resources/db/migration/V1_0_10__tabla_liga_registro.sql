DROP TABLE IF EXISTS liga;

CREATE TABLE liga(
   id SERIAL,
   nombre VARCHAR(100) NOT NULL,
   PRIMARY KEY (id)
);

ALTER TABLE usuario
ADD PRIMARY KEY (id);

ALTER TABLE usuario_roles
ADD FOREIGN KEY (usuario_id) REFERENCES usuario(id);