DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario(
   id SERIAL,
   alias_usuario VARCHAR(50) NOT NULL,
   mail_usuario VARCHAR(100) NOT NULL,
   nombre_usuario VARCHAR(50) NOT NULL,
   apellido_usuario VARCHAR(50) NOT NULL,
   contrasenia_usuario VARCHAR(50) NOT NULL
);