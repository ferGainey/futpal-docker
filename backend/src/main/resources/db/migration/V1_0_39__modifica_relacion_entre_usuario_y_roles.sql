ALTER TABLE usuario_roles
DROP COLUMN id;

ALTER TABLE usuario_roles
ADD PRIMARY KEY (usuario_id, rol_id);

ALTER TABLE usuario_roles
ADD CONSTRAINT fk_usuario
FOREIGN KEY (usuario_id)
REFERENCES usuario (id);

ALTER TABLE usuario_roles
ADD CONSTRAINT fk_rol
FOREIGN KEY (rol_id)
REFERENCES rol (id);