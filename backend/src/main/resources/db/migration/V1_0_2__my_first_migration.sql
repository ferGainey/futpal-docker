DROP TABLE IF EXISTS borrame;

CREATE TABLE borrame(
   id SERIAL,
   dept           CHAR(50) NOT NULL,
   emp_id         INT      NOT NULL
);