ALTER TABLE partido_liga_base
RENAME COLUMN id_equipo_local TO equipo_local_id;

ALTER TABLE partido_liga_base
RENAME COLUMN id_equipo_visitante TO equipo_visitante_id;