CREATE TABLE liga_equipos(
   id SERIAL,
   id_liga integer,
   id_equipo integer,
   PRIMARY KEY (id),
   FOREIGN KEY (id_liga) REFERENCES liga(id),
   FOREIGN KEY (id_equipo) REFERENCES equipo(id_equipo)
);

ALTER TABLE liga
DROP COLUMN id_equipos;