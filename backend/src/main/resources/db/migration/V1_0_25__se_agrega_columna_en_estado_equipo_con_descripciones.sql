ALTER TABLE estado_equipo
ADD COLUMN descripcion VARCHAR;

UPDATE estado_equipo
SET descripcion='El equipo ya fue aprobado por un administrador'
WHERE id=1;

UPDATE estado_equipo
SET descripcion='El equipo está pendiente de aprobación.'
WHERE id=2;

UPDATE estado_equipo
SET descripcion='El equipo no existe.'
WHERE id=3;

UPDATE estado_equipo
SET descripcion='El equipo fue rechazado. Puede volver a solicitar la aprobación.'
WHERE id=4;

UPDATE estado_equipo
SET descripcion='Al finalizar de agregar jugadores puede solicitar la aprobación del equipo.'
WHERE id=5;