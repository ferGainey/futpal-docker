DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario(
   id SERIAL,
   alias_usuario CHAR(50) NOT NULL,
   mail_usuario CHAR(100) NOT NULL,
   nombre_usuario CHAR(50) NOT NULL,
   apellido_usuario CHAR(50) NOT NULL,
   contrasenia_usuario CHAR(50) NOT NULL
);