ALTER TABLE partido_liga_base
RENAME COLUMN id_estado TO estado_partido_id;

ALTER TABLE partido_liga_base
RENAME COLUMN ultimo_usuario_editor_id TO usuario_id;

ALTER TABLE partido_liga_base ADD CONSTRAINT usuario_id FOREIGN KEY (usuario_id)
        REFERENCES usuario (id);

CREATE TABLE estado_partido(
   id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
   nombre VARCHAR(50),
   descripcion VARCHAR,
   PRIMARY KEY (id)
);

INSERT INTO estado_partido (nombre, descripcion) VALUES
('PENDIENTE', 'El partido aún no se jugó.'),
('FINALIZADO', 'Ya completó el partido.');