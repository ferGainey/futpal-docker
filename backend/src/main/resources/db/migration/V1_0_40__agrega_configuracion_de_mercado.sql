CREATE TABLE mercado(
    caracteristica VARCHAR NOT NULL,
    activo BOOLEAN NOT NULL,
    PRIMARY KEY (caracteristica)
);

INSERT INTO mercado (caracteristica, activo) VALUES
('CARGA_TRANSFERENCIAS', true);