CREATE TABLE rol(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    descripcion varchar,
    PRIMARY KEY (id)
);

INSERT INTO rol (descripcion) VALUES
('ROL_USUARIO'),
('ROL_ADMINISTRADOR');