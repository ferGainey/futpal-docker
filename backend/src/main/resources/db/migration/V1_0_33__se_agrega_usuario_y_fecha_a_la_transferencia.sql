ALTER TABLE transferencia
ADD COLUMN usuario_id integer;

ALTER TABLE transferencia
ADD COLUMN fecha_transferencia timestamp;

ALTER TABLE transferencia ADD CONSTRAINT usuario_id FOREIGN KEY (usuario_id)
        REFERENCES usuario (id);