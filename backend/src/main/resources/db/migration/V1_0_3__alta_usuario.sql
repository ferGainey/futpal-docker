DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario(
   id SERIAL,
   aliasUsuario CHAR(50) NOT NULL,
   mailUsuario CHAR(100) NOT NULL,
   nombreUsuario CHAR(50) NOT NULL,
   apellidoUsuario CHAR(50) NOT NULL,
   contraseniaUsuario CHAR(50) NOT NULL
);