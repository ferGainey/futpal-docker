DROP TABLE IF EXISTS usuario_roles;

CREATE TABLE usuario_roles(
   id SERIAL,
   usuario_id integer NOT NULL,
   rol_id integer NOT NULL
);