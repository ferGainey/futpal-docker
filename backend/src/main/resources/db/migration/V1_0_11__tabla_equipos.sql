DROP TABLE IF EXISTS equipo;

CREATE TABLE equipo(
   id_equipo SERIAL,
   id_usuario integer,
   nombreEquipo VARCHAR(100) NOT NULL,
   PRIMARY KEY (id_equipo),
   FOREIGN KEY (id_usuario) REFERENCES usuario(id)
);