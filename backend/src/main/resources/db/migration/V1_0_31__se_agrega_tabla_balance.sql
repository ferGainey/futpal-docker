CREATE TABLE balance(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    detalle varchar,
    equipo_id integer,
    monto integer,
    numero_temporada integer,
    periodo_id integer
);