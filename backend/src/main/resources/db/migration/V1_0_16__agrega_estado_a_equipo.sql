CREATE TABLE estado_equipo(
   id SERIAL NOT NULL,
   nombre VARCHAR(100) NOT NULL,
   PRIMARY KEY (id)
);

INSERT INTO estado_equipo (nombre) VALUES ('APROBADO');
INSERT INTO estado_equipo (nombre) VALUES ('NO_APROBADO');
INSERT INTO estado_equipo (nombre) VALUES ('INEXISTENTE');

ALTER TABLE equipo
ADD estado_equipo_id integer;

ALTER TABLE equipo ADD CONSTRAINT estado_equipo_id FOREIGN KEY (estado_equipo_id)
        REFERENCES estado_equipo (id);