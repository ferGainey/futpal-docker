CREATE TABLE transferencia(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    PRIMARY KEY (id)
);

ALTER TABLE balance
ADD COLUMN transferencia_id integer;

CREATE TABLE movimiento_jugador(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY,
    jugador_id integer NOT NULL,
    numero_temporada integer NOT NULL,
    periodo_id integer NOT NULL,
    equipo_origen_id integer NOT NULL,
    equipo_destino_id integer NOT NULL,
    transferencia_id integer NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (equipo_origen_id) REFERENCES equipo(id),
    FOREIGN KEY (equipo_destino_id) REFERENCES equipo(id)
);