CREATE TABLE estadistica_gol(
   id SERIAL,
   partido_id integer,
   jugador_id integer,
   equipo_id integer,
   cantidad integer,
   PRIMARY KEY (id),
   FOREIGN KEY (jugador_id) REFERENCES jugador(id),
   FOREIGN KEY (equipo_id) REFERENCES equipo(id),
   FOREIGN KEY (partido_id) REFERENCES partido_liga_base(id)
);

CREATE TABLE estadistica_tarjeta_amarilla(
   id SERIAL,
   partido_id integer,
   jugador_id integer,
   equipo_id integer,
   PRIMARY KEY (id),
   FOREIGN KEY (jugador_id) REFERENCES jugador(id),
   FOREIGN KEY (equipo_id) REFERENCES equipo(id),
   FOREIGN KEY (partido_id) REFERENCES partido_liga_base(id)
);

CREATE TABLE estadistica_tarjeta_roja(
   id SERIAL,
   partido_id integer,
   jugador_id integer,
   equipo_id integer,
   PRIMARY KEY (id),
   FOREIGN KEY (jugador_id) REFERENCES jugador(id),
   FOREIGN KEY (equipo_id) REFERENCES equipo(id),
   FOREIGN KEY (partido_id) REFERENCES partido_liga_base(id)
);

CREATE TABLE estadistica_lesion(
   id SERIAL,
   partido_id integer,
   jugador_id integer,
   equipo_id integer,
   PRIMARY KEY (id),
   FOREIGN KEY (jugador_id) REFERENCES jugador(id),
   FOREIGN KEY (equipo_id) REFERENCES equipo(id),
   FOREIGN KEY (partido_id) REFERENCES partido_liga_base(id)
);

CREATE TABLE estadistica_mvp(
   id SERIAL,
   partido_id integer,
   jugador_id integer,
   equipo_id integer,
   PRIMARY KEY (id),
   FOREIGN KEY (jugador_id) REFERENCES jugador(id),
   FOREIGN KEY (equipo_id) REFERENCES equipo(id),
   FOREIGN KEY (partido_id) REFERENCES partido_liga_base(id)
);