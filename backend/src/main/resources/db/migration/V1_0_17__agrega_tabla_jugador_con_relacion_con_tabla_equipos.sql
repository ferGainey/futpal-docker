ALTER TABLE liga_equipos DROP CONSTRAINT liga_equipos_id_equipo_fkey;

ALTER TABLE equipo RENAME COLUMN id_equipo TO id;

ALTER TABLE liga_equipos ADD CONSTRAINT id_equipo FOREIGN KEY (id_equipo)
        REFERENCES equipo (id);

CREATE TABLE jugador(
   id SERIAL NOT NULL,
   nombre VARCHAR(100) NOT NULL,
   anio_nacimiento integer,
   valor numeric,
   salario numeric,
   media integer,
   equipo_id integer,
   PRIMARY KEY (id),
   CONSTRAINT equipo_id FOREIGN KEY (equipo_id)
               REFERENCES equipo (id)
);