Feature: Alta de usuarios

  Scenario: Se puede dar de alta un usuario nuevo
    Given el usuario esta en la pantalla de sign up
    When crea usuario con datos validos y alias "ferg"
    Then muestra alerta de que se creo exitosamente
    And se creo el usuario correspondiente con alias "ferg"

  Scenario: Si el alias del usuario ya existe no puede ser creado
    Given hay un usuario creado con alias "usrcreado"
    And el usuario esta en la pantalla de sign up
    When crea usuario con datos validos y alias "usrcreado"
    Then no se creo el usuario con alias "usrcreado"
    And muestra alerta de que no se pudo crear el usuario por alias existente

  Scenario: Si el mail del usuario ya existe no puede ser creado
    Given hay un usuario creado con mail "usrcreado@mailfalso.com"
    And el usuario esta en la pantalla de sign up
    When crea usuario con datos validos y mail "usrcreado@mailfalso.com"
    Then no se creo el usuario con mail "usrcreado@mailfalso.com"
    And muestra alerta de que no se pudo crear el usuario por mail existente