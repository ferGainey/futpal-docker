#Feature: El usuario puede loguearse
#
#  Scenario: Un usuario registrado puede loguearse
#    Given el usuario con alias "ferg" esta registrado.
#    When el usuario con alias "ferg" se loguea
#    Then se redirige a la pantalla de inicio del usuario "ferg"
#    #And no se muestra mas la opcion para loguearse #se saca para que no demore al resto de los tests la espera a que aparezca esta opción que nunca va a aparecer. Se deja escrito para dejar como documentación.
#
#  Scenario: Un usuario registrado puede desloguearse
#    Given el usuario con alias "ferg" esta registrado
#    And el usuario con alias "ferg" esta logueado
#    When el usuario se desloguea
#    Then se redirige a la pantalla principal de la aplicacion
#
#  Scenario: Un usuario no registrado no puede loguearse
#    Given el usuario con alias "ferg" no esta registrado
#    When el usuario con alias "ferg" se loguea
#    Then se le muestra alerta de que las credenciales no son validas