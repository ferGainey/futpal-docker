Feature: Transferencias entre equipos

  Background:
    Given existe el equipo "Afalp" con usuario "titov"
    And el usuario "titov" es administrador
    And existe el equipo "Untref" con usuario "ferg"
    And el mercado esta abierto
    And el equipo "Afalp" tiene a los jugadores "Pepe" y "Salah"
    And el equipo "Untref" tiene a los jugadores "Alario" y "Maidana"
    And se está en la temporada 1 y periodo "Mitad"
    And el usuario "titov" esta logueado

  Scenario: Se transfieren jugadores entre equipos
    Given el usuario "titov" entra a la pantalla de transferencias y elige al equipo "Untref"
    And agrega para entregar en la transferencia al jugador "Pepe" y 50000
    And agrega para prestar al jugador propio "Salah"
    And agrega para recibir en la transferencia al jugador "Alario" y 25000
    When realiza la transferencia
    Then se muestra que la transferencia se realizo exitosamente

  Scenario: Se muestra error al realizar una transferencia no valida
    And el usuario "titov" entra a la pantalla de transferencias y elige al equipo "Untref"
    And agrega para entregar en la transferencia al jugador "Pepe" y 50000
    And agrega para recibir en la transferencia al jugador "Alario" y 25000
    And agrega para entregar en la transferencia al jugador "Pepe" y 50000
    And realiza la transferencia
    Then se muestra que al menos unos de los jugadores fue transferido

  Scenario: Se ejecutan transferencias en la temporada y periodo en la que estan cargadas
    Given los jugadores "Pepe" y "Alario" fueron transferidos en la temporada 1 y periodo "Mitad" de "Untref" a "Afalp"
    When avanza de periodo
    Then se mueven los jugadores a su nuevo equipo "Afalp"

  Scenario: Se ven las transferencias realizadas por un equipo
    Given los jugadores "Maidana" y "Alario" fueron transferidos en la temporada 1 y periodo "Mitad" de "Untref" a "Afalp"
    When ingresa a sus transferencias
    Then se ve la transferencia cargada de "Maidana"
    And se ve la transferencia cargada de "Alario"

  Scenario: Se rechaza una transferencia
    Given los jugadores "Alario" y "Maidana" fueron transferidos en la temporada 1 y periodo "Mitad" de "Untref" a "Afalp"
    And ingresa a sus transferencias
    When rechaza la primera transferencia
    Then se ve la transferencia cargada de "Maidana"
    And no se ve la transferencia cargada de "Alario"
    And la primera transferencia queda rechazada

  Scenario: Se confirma una transferencia
    Given los jugadores "Alario" y "Maidana" fueron transferidos en la temporada 1 y periodo "Mitad" de "Untref" a "Afalp"
    And ingresa a sus transferencias
    When confirma la primera transferencia
    Then se ve la transferencia cargada de "Maidana"
    And se ve la transferencia cargada de "Alario"
    And la primera transferencia queda confirmada

  Scenario: Se ven las transferencias realizadas por todos los equipos
    Given existe el equipo "Esfera" con usuario "maggi"
    And el equipo "Esfera" tiene a los jugadores "Mbappe" y "Neymar"
    And el equipo "Esfera" tiene a los jugadores "Messi" y "Enzo"
    And los jugadores "Maidana" y "Alario" fueron transferidos en la temporada 1 y periodo "Mitad" de "Untref" a "Afalp"
    And los jugadores "Mbappe" y "Neymar" fueron transferidos en la temporada 1 y periodo "Mitad" de "Esfera" a "Untref"
    And el jugador "Messi" fue prestado de "Esfera" del usuario "maggi" a "Afalp" del usuario "titov" desde la temporada 1 periodo "Mitad" hasta la temporada 2 periodo "Final"
    And todas las transferencias estan confirmadas
    When ingresa a todas las transferencias
    Then se ve la transferencia cargada de "Maidana"
    And se ve la transferencia cargada de "Alario"
    And se ve la transferencia cargada de "Mbappe"
    And se ve la transferencia cargada de "Neymar"
    And se ve el prestamo cargado de "Messi" desde la temporada 1 periodo "Mitad" hasta la temporada 2 periodo "Final"

  Scenario: Se cambia el estado del mercado
    When el usuario administrador deshabilita el mercado
    Then no se ve la opcion de carga de transferencias