Feature: Jugadores prestados de un equipo

  Background:
    Given existe un usuario "ferg"
    Given existe un usuario "titov"
    And se está en la temporada 4 y periodo "Final"
    And que el equipo del usuario "ferg" con id 1 esta en estado aprobado
    And existe el equipo "Dave Holland" con usuario "titov"

  Scenario: Se ven los jugadores a prestamos de mi plantel
    Given el equipo "Ferg FC" tiene a los jugadores "Messi" y "Aubameyang"
    And el jugador "Messi" fue prestado de "Ferg FC" del usuario "ferg" a "Dave Holland" del usuario "titov" desde la temporada 4 periodo "Final" hasta la temporada 5 periodo "Mitad"
    And se ejecuto el movimiento que mueve prestamo al jugador "Messi" al "Dave Holland"
    And el usuario "ferg" esta en la pantalla del plantel
    When va a la seccion de jugadores prestados
    Then se ve al jugador "Messi" prestado en "Dave Holland" hasta "Mitad" temporada 5