Feature: Hay temporadas que a su vez se dividen en períodos

  Scenario: Se muestra la temporada y periodo a un usuario
    Given existe un usuario "ferg"
    And el usuario "ferg" esta logueado
    And se está en la temporada 1 y periodo "Mitad"
    And el usuario con alias "ferg" esta logueado
    Then se le muestra que la temporada actual es "Mitad" temporada 1

  Scenario: Se avanza de periodo exitosamente
    Given existe un administrador "yid"
    And se está en la temporada 1 y periodo "Final"
    And el usuario con alias "yid" esta logueado
    And se le muestra que la temporada actual es "Final" temporada 1
    When avanza de periodo
    Then se le muestra que la temporada actual es "Principio" temporada 2