Feature: Se permite editar nombre de equipo o agregarlo en caso de que no existiera

  Background:
    Given existe un usuario "ferg"
    And se está en la temporada 1 y periodo "Mitad"

  Scenario: Si no existe el equipo muestra que todavia no tiene ningun equipo asociado
    Given el usuario "ferg" esta en la pantalla de edicion de equipo
    And el usuario "ferg" no tiene un equipo asociado
    When el usuario "ferg" entra a la pantalla de edicion de equipo
    Then muestra un mensaje avisando de que no tiene equipo asociado

  Scenario: Si no existe el equipo puedo agregarlo para el usuario
    Given el usuario "ferg" esta en la pantalla de edicion de equipo
    And el usuario "ferg" no tiene un equipo asociado
    And carga el nombre "Afalp" a su equipo
    When selecciona actualizar
    Then muestra un mensaje avisando de que se actualizo el equipo correctamente
    And muestra en la pantalla el nombre del equipo "Afalp"

  Scenario: Si existe el equipo muestra el nombre del equipo actual
    Given el usuario "ferg" tiene un equipo asociado "Afalp"
    When el usuario "ferg" entra a la pantalla de edicion de equipo
    Then muestra en la pantalla el nombre del equipo "Afalp"

  Scenario: Si existe el equipo se le puede modificar el nombre
    Given el usuario "ferg" esta en la pantalla de edicion de equipo
    Given el usuario "ferg" tiene un equipo asociado "Afalp"
    And carga el nombre "Untref" a su equipo
    When selecciona actualizar
    Then muestra en la pantalla el nombre del equipo "Untref"
    And muestra un mensaje avisando de que se actualizo el equipo correctamente
