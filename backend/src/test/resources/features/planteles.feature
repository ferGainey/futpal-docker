Feature: Se ven los planteles de todos los equipos aprobados

  Background:
    Given existe el equipo "Dave Holland" con usuario "titov"
    And el equipo "Dave Holland" tiene a los jugadores "Pepe" y "Mane"
    And se está en la temporada 1 y periodo "Mitad"


  Scenario: Se muestran los jugadores de otro equipo en la pantalla de planteles
    Given existe un usuario "ferg"
    And el usuario "ferg" esta en la pantalla de planteles
    When selecciona el equipo "Dave Holland" del combo
    Then se muestran los jugadores del equipo Dave Holland

  Scenario: se confirma un equipo pendiente de aprobacion
    Given existe un administrador "yid"
    And el equipo "Dave Holland" esta pendiente de aprobacion
    And el usuario "yid" esta en la pantalla de planteles
    When selecciona en el combo de planteles pendientes de aprobacion el equipo "Dave Holland"
    And el administrador confirma el equipo
    Then el equipo "Dave Holland" no aparece mas como pendiente de aprobacion
    Then el equipo "Dave Holland" aparece en el combo de planteles disponibles
    Then se muestra mensaje que se confirmo equipo

  Scenario: se rechaza un equipo pendiente de aprobacion
    Given existe un administrador "yid"
    And el equipo "Dave Holland" esta pendiente de aprobacion
    And el usuario "yid" esta en la pantalla de planteles
    When selecciona en el combo de planteles pendientes de aprobacion el equipo "Dave Holland"
    And el administrador rechaza el equipo
    And escribe como motivo de rechazo "Hay jugadores que ya tienen equipo"
    Then el equipo "Dave Holland" no aparece mas como pendiente de aprobacion
    Then el equipo "Dave Holland" no aparece en el combo de planteles disponibles
    Then el usuario "titov" esta en la pantalla del plantel
    Then el usuario ve que el motivo de rechazo del equipo es "Hay jugadores que ya tienen equipo"

    #puede estar en cualquier estado. ToDo agregar los tests para los otros estados aunque sea redundante
  Scenario: administrador borra un jugador de un equipo
    Given existe un administrador "yid"
    And el equipo "Dave Holland" esta pendiente de aprobacion
    #Todo: estudiar bien a fondo por qué le borra el id del equipo a los jugadores el paso anterior
    And el equipo "Dave Holland" tiene a los jugadores "Pepe" y "Mane"
    And el usuario "yid" esta en la pantalla de planteles
    And selecciona en el combo de planteles pendientes de aprobacion el equipo "Dave Holland"
    When el administrador borra al jugador "Pepe" en la pantalla de planteles
    Then el jugador "Pepe" no aparece mas en el plantel de "Dave Holland" en la pantalla de planteles

  Scenario: usuario borra un jugador de su plantel en estado rechazado
    Given el equipo "Dave Holland" esta rechazado
    #Todo: estudiar bien a fondo por qué le borra el id del equipo a los jugadores el paso anterior
    And el equipo "Dave Holland" tiene a los jugadores "Pepe" y "Mane"
    Given el usuario "titov" esta en la pantalla del plantel
    When el usuario borra al jugador "Pepe" en la pantalla de Mi Plantel
    Then el jugador "Pepe" no aparece mas en el plantel en la pantalla de Mi Plantel

  Scenario: usuario borra un jugador de su plantel en estado inicial
    Given el equipo "Dave Holland" esta en estado inicial
    #Todo: estudiar bien a fondo por qué le borra el id del equipo a los jugadores el paso anterior
    And el equipo "Dave Holland" tiene a los jugadores "Pepe" y "Mane"
    Given el usuario "titov" esta en la pantalla del plantel
    When el usuario borra al jugador "Pepe" en la pantalla de Mi Plantel
    Then el jugador "Pepe" no aparece mas en el plantel en la pantalla de Mi Plantel

    #puede editar tanto para equipos aprobados o pendiente de aprobación
  Scenario: administrador edita jugador en la pantalla de planteles
    Given existe un administrador "yid"
    And el usuario "yid" esta en la pantalla de planteles
    And selecciona el equipo "Dave Holland" del combo
    When se edita al jugador "Pepe" en la pantalla de planteles con nombre "Salah"
    Then el jugador "Pepe" no aparece mas en el plantel de "Dave Holland" en la pantalla de planteles
    Then el jugador "Salah" aparece en el plantel en la pantalla de planteles
