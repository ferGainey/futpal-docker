Feature: Creacion de un torneo formato liga

  Background:
    Given existe un administrador "yid"
    And existe el equipo administrador "Untref"
    And existen otros 5 equipos
    And se está en la temporada 1 y periodo "Mitad"
    And el administrador "yid" se encuentra en la pantalla de creacion de ligas

  Scenario: No se crea liga cuando se completa con datos invalidos
    Given completo el nombre de la liga con "  "
    When selecciona crear liga
    Then se le muestra una alerta indicando que hay campos invalidos

  Scenario: Los equipos disponibles se muestran al estar en la pantalla de creacion de liga
    Then el equipo "Untref" aparece en la pantalla como equipo disponible a agregar

  Scenario: Se agrega un equipo en la pantalla de creacion de equipos
    When agrego el equipo "Untref" a la liga
    Then el equipo "Untref" aparece en la pantalla como equipo que se va a agregar

  Scenario: Se borra un equipo agregado en la pantalla de creacion de equipos
    Given el administrador agrego al equipo "Untref"
    When el administrador borra el equipo "Untref" a la liga
    Then el equipo "Untref" no aparece en la pantalla como equipo que se va a agregar

    #TODO: ver de separar los features en creacionLiga, consultaLiga y edicionLiga
  Scenario: Un usuario puede ver las ligas disponibles
    Given que la liga "Liga de Bronce" existe
    When el usuario "ferg" entra a la pantalla de Ligas
    Then se le muestra la liga "Liga de Bronce" como liga disponible

  Scenario: Un usuario puede moverse a una liga desde las ligas disponibles
    Given que la liga "Liga de Bronce" existe
    And el usuario "ferg" entra a la pantalla de Ligas
    When el usuario selecciona la liga "Liga de Bronce"
    Then se le redirige a la pantalla de la liga "Liga de Bronce"

  Scenario: Se crea una liga exitosamente y se ven los equipos agregados en la liga cuando se va a la pantalla de la liga
    Given completo el nombre de la liga con "Liga de Bronce"
    And el administrador agrego 5 equipos a la liga al crearla
    When selecciona crear liga
    Then ve la tabla con los 5 equipos agregados
    Then se verifica que la liga es de la temporada 1

  Scenario: La tabla de la liga contiene las columnas de posiciones, goles a favor, goles en contra, diferencia de goles y puntos
    Given que la liga "Liga de Bronce" existe
    And el usuario "ferg" entra a la pantalla de Ligas
    When el usuario selecciona la liga "Liga de Bronce"
    Then se le redirige a la pantalla de la liga "Liga de Bronce"
    And en la tabla aparece una columna de posiciones
    And en la tabla aparece una columna de puntos
    And en la tabla aparece una columna de goles a favor
    And en la tabla aparece una columna de goles en contra
    And en la tabla aparece una columna de diferencia de goles
    And en la tabla aparece una columna de partidos jugados

  Scenario: Se puede ir a la pantalla de partidos de la liga
    Given que la liga "Liga de Bronce" existe
    And el usuario "ferg" entra a la pantalla de Ligas
    And el usuario selecciona la liga "Liga de Bronce"
    When el usuario selecciona la opcion Partidos
    Then se le redirige a la pantalla de partidos de la liga "Liga de Bronce"

  Scenario: Se puede volver desde el sidenav de la liga a las posiciones
    Given que la liga "Liga de Bronce" existe
    And el usuario "ferg" entra a la pantalla de Ligas
    And el usuario selecciona la liga "Liga de Bronce"
    And el usuario selecciona la opcion Partidos
    When el usuario selecciona la opcion Posiciones
    Then se le redirige a la pantalla de la liga "Liga de Bronce"
    And en la tabla aparece una columna de posiciones

  Scenario: Se crea el calendario de la liga al crearla
    Given completo el nombre de la liga con "Liga de Bronce"
    And el administrador agrego 3 equipos a la liga al crearla
    And selecciona crear liga
    When el usuario selecciona la opcion Partidos
    Then se ven los partidos entre todos los equipos de la liga

  Scenario: Se puede ir a la pantalla de mis partidos en la liga
    Given completo el nombre de la liga con "Liga de Bronce"
    And el administrador agrego 3 equipos a la liga al crearla
    And el administrador agrego al equipo "Untref"
    And selecciona crear liga
    And el usuario selecciona la opcion Partidos
    When el usuario filtra los partidos por el equipo "Untref"
    Then se le muestran solo partidos de su equipo "Untref"
# TODO: el usuario ferg se esta ejecutando como si fuera yid

