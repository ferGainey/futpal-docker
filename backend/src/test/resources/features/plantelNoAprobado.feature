Feature: Manejo de plantel no aprobado

  Background:
    Given existe un usuario "ferg"
    And se está en la temporada 1 y periodo "Mitad"
    And que el equipo del usuario "ferg" con id 1 esta en estado inicial
    And el usuario "ferg" esta en la pantalla del plantel

  Scenario: se agrega a un jugador exitosamente
    When el usuario agrega a un jugador "Lionel Messi" con su nombre, fecha nacimiento, valor, salario, media y id de transfermarkt
    Then se muestra al jugador "Lionel Messi" como agregado

  Scenario: en estado inicial se muestra que puede agregar jugadores hasta solicitar aprobacion
    Then se muestra mensaje de que puede agregar jugadores y solicitar aprobacion al terminar

  Scenario: se muestran jugadores que se agregaron previamente
    Given el usuario tiene 2 jugadores agregados
    Then se ven los 2 jugadores agregados

  Scenario: no se pueden cargar jugadores si el equipo no esta registrado con un nombre inicial
    Given el equipo del usuario "ferg" no tiene nombre inicial registrado
    Then no le permite agregar jugadores
    Then le muestra mensaje de que no puede agregar jugadores hasta que no se le asigne un nombre inicial al equipo

  Scenario: se pide aprobacion del equipo despues de que el equipo haya sido rechazado
    Given el equipo del usuario "ferg" con id 1 esta rechazado
    When el usuario selecciona solicitar aprobacion
    Then se muestra mensaje de que el equipo esta pendiente de aprobacion
