Feature: Se puede actualizar la liga con los resultados y estadisticas

  Background:
    Given que la liga "Liga de Bronce" existe
    And existe el usuario "ferg"
    And existe un administrador "yid"
    And existe el equipo administrador "Untref"
    And existen otros 5 equipos
    And se está en la temporada 1 y periodo "Mitad"
    And el usuario "ferg" esta logueado
    And el usuario "ferg" entra a la pantalla de Ligas
    And el usuario selecciona la liga "Liga de Bronce"

    #el problema parece estar en que tarda en recargar los partidos. Tal vez se podría poner un spinner o tapar de alguna manera la tabla de partidos hasta que se refresquen
  Scenario: se carga el resultado de un partido de liga
    Given hay 1 partido de liga
    And el usuario "ferg" entra a la pantalla de Ligas
    And el usuario selecciona la liga "Liga de Bronce"
    And el usuario selecciona la opcion Partidos
    When el usuario entra al editor de resultados
    And carga 3 goles al equipo local
    And carga 2 goles al equipo visitante
    And guarda el resultado
    Then se ve el resultado actualizado
    And el usuario entra al editor de resultados
    And se muestra que el usuario "ferg" actualizo el partido

  Scenario: se intenta actualizar un resultado de liga sin ingresar goles
    Given hay 1 partido de liga
    And el usuario "ferg" entra a la pantalla de Ligas
    And el usuario selecciona la liga "Liga de Bronce"
    And el usuario selecciona la opcion Partidos
    When el usuario entra al editor de resultados
    And guarda el resultado
    Then se muestra mensaje de que no puede haber campos nulos

  Scenario: la tabla de posiciones tiene los resultados reflejados
    Given hay 1 partido de liga
    And el usuario "ferg" entra a la pantalla de Ligas
    And el usuario selecciona la liga "Liga de Bronce"
    And el usuario selecciona la opcion Partidos
    And los equipos estan vinculados a la liga
    When el usuario entra al editor de resultados
    And carga 3 goles al equipo local
    And carga 2 goles al equipo visitante
    And guarda el resultado
    And el usuario selecciona la opcion Posiciones
    Then los equipos tienen los datos actualizados en la tabla

  Scenario: si ya hay un resultado cargado entonces queda precargado en el editor de resultado
    Given hay 1 partido de liga cargado
    And el usuario "ferg" entra a la pantalla de Ligas
    And el usuario selecciona la liga "Liga de Bronce"
    And el usuario selecciona la opcion Partidos
    And los equipos estan vinculados a la liga
    When el usuario entra al editor de resultados
    Then se ve el resultado actualizado en el editor de resultado