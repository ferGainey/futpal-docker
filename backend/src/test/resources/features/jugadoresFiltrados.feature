Feature: Jugadores prestados de un equipo

  Background:
    Given existe un usuario "ferg"
    Given existe un usuario "titov"
    And se está en la temporada 4 y periodo "Final"
    And que el equipo del usuario "ferg" con id 1 esta en estado aprobado
    And existe el equipo "Dave Holland" con usuario "titov"

  Scenario: Se ven los jugadores a prestamos de mi plantel
    Given el equipo "Ferg FC" tiene a los jugadores "Messi" y "Aubameyang"
    And el usuario "ferg" esta en la pantalla de planteles
    When va a la seccion de busqueda de jugadores
    And filtra por id de transfermarkt 9923 y nombre "eSsi"
    Then se ve al jugador "Messi" en el equipo "Ferg FC"