Feature: Estadísticas de la liga

  Background:
    Given existe el usuario "titov"
    And hay 4 equipos registrados en la liga "Liga de Estrellas" con jugadores
    And se está en la temporada 1 y periodo "Mitad"
    And el usuario "titov" esta logueado
    And el usuario "titov" entra a la pantalla de Ligas
    And el usuario selecciona la liga "Liga de Estrellas"
    And existe el partido entre el equipo con id 1 y el equipo con id 2

  Scenario: Se cargan las estadisticas de un partido
    Given el usuario entra al editor de resultados
    And carga 3 goles al equipo local
    And carga 2 goles al equipo visitante
    When el usuario carga los goles del partido
    And el usuario carga las tarjetas amarillas
    And el usuario carga las tarjetas rojas
    And el usuario carga las lesiones
    And And el usuario carga el MVP
    And guarda el resultado
    Then en la pantalla de informacion del partido ve los datos agregados

  Scenario: Si ya hay estadisticas cargadas previamente las muestra
    Given ya hay datos de estadisticas cargados para el partido numero 1
    And el usuario selecciona la opcion Partidos
    When el usuario entra al editor de resultados
    Then el usuario ve las estadisticas cargadas previamente de los goles
    And el usuario ve las estadisticas cargadas previamente de las tarjetas amarillas
    And el usuario ve las estadisticas cargadas previamente de las tarjetas rojas
    And el usuario ve las estadisticas cargadas previamente de las lesiones
    And el usuario ve las estadisticas cargadas previamente del mvp

  Scenario: Se puede borrar una estadistica cargada
    Given ya hay datos de estadisticas cargados para el partido numero 1
    And el usuario selecciona la opcion Partidos
    And el usuario entra al editor de resultados
    And carga 3 goles al equipo local
    And carga 2 goles al equipo visitante
    And el usuario borra el gol cargado del equipo local
    And guarda el resultado
    When el usuario entra al editor de resultados
    Then el usuario ve que no esta mas el gol borrado

  Scenario: Se ven las estadisticas de una liga
    Given ya hay datos de estadisticas cargados para el partido numero 1
    And existe el partido entre el equipo con id 1 y el equipo con id 3
    And ya hay datos de estadisticas cargados para el partido 2
    When el usuario ingresa a la pantalla de estadisticas de la liga
    Then se muestran las estadisticas cargadas de la liga