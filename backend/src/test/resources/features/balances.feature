Feature: Los balances de los equipos

  Scenario: Se carga un importe a un equipo
    Given existe un administrador "yid"
    And existe el equipo "Afalp" con usuario "titov"
    And se está en la temporada 1 y periodo "Final"
    And el equipo "Afalp" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el usuario con alias "yid" esta logueado
    When el administrador le carga 25000 al equipo "Afalp" en la temporada 1 periodo "Final"
    Then se muestra mensaje que el balance fue cargado exitosamente
    And el equipo "Afalp" tiene 75000

  Scenario: Se carga un importe a equipos de una liga
    Given existe un administrador "yid"
    And hay 4 equipos registrados en la liga "Liga B" con jugadores
    And se está en la temporada 1 y periodo "Final"
    And el equipo "Nico FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Gas FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Marc FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Mar FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el usuario con alias "yid" esta logueado
    When el administrador le carga por liga 25000 a todos los equipos en la temporada y periodo actual de la liga "Liga B"
    Then se muestra mensaje que los balances fueron cargados exitosamente
    And el equipo "Nico FC" tiene 75000
    And el equipo "Gas FC" tiene 75000
    And el equipo "Marc FC" tiene 75000
    And el equipo "Mar FC" tiene 75000

  Scenario: se cargan los balances de todos los equipos con sus gastos e ingresos
    Given existe un usuario "ferg"
    And hay 4 equipos registrados en la liga "Liga B" con jugadores
    And se está en la temporada 2 y periodo "Final"
    And el equipo "Gas FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Gas FC" tiene cargado -60000 de la temporada 2 periodo "Principio"
    And el equipo "Gas FC" tiene cargado -10000 de la temporada 2 periodo "Final"
    And el equipo "Gas FC" tiene cargado 5000 de la temporada 2 periodo "Principio"
    And el equipo "Gas FC" tiene cargado 2000 de la temporada 2 periodo "Mitad"
    And el equipo "Gas FC" tiene cargado 30000 de la temporada 2 periodo "Final"
    And el usuario con alias "ferg" esta logueado
    When el usuario entra a la pantalla de vista de balances de todos los equipos
    Then se muestran los balances de los 4 equipos
    Then el equipo "Gas FC" tiene cargado "-$5.000" en la columna total a principio de temporada
    Then el equipo "Gas FC" tiene cargado "-$3.000" en la columna total a mitad de temporada
    Then el equipo "Gas FC" tiene cargado "$17.000" en la columna total a final de temporada
    Then el equipo "Gas FC" tiene cargado "$60.000" en la columna gastos a principio de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna gastos a mitad de temporada
    Then el equipo "Gas FC" tiene cargado "$10.000" en la columna gastos a final de temporada
    Then el equipo "Gas FC" tiene cargado "$5.000" en la columna ingresos a principio de temporada
    Then el equipo "Gas FC" tiene cargado "$2.000" en la columna ingresos a mitad de temporada
    Then el equipo "Gas FC" tiene cargado "$30.000" en la columna ingresos a final de temporada

  Scenario: se cargan los balances de todos los equipos con sus gastos e ingresos y se filtra por temporada
    Given existe un usuario "ferg"
    And hay 4 equipos registrados en la liga "Liga B" con jugadores
    And se está en la temporada 2 y periodo "Final"
    And el equipo "Gas FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Gas FC" tiene cargado -60000 de la temporada 1 periodo "Principio"
    And el equipo "Gas FC" tiene cargado -10000 de la temporada 2 periodo "Final"
    And el equipo "Gas FC" tiene cargado 5000 de la temporada 2 periodo "Principio"
    And el equipo "Gas FC" tiene cargado 2000 de la temporada 3 periodo "Mitad"
    And el equipo "Gas FC" tiene cargado 30000 de la temporada 3 periodo "Final"
    And el usuario con alias "ferg" esta logueado
    When el usuario entra a la pantalla de vista de balances de todos los equipos
    And el usuario selecciona la temporada 1 en la pantalla de vista de balances de todos los equipos
    Then el equipo "Gas FC" tiene cargado "-$60.000" en la columna total a principio de temporada
    Then el equipo "Gas FC" tiene cargado "-$10.000" en la columna total a mitad de temporada
    Then el equipo "Gas FC" tiene cargado "-$10.000" en la columna total a final de temporada
    Then el equipo "Gas FC" tiene cargado "$60.000" en la columna gastos a principio de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna gastos a mitad de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna gastos a final de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna ingresos a principio de temporada
    Then el equipo "Gas FC" tiene cargado "$50.000" en la columna ingresos a mitad de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna ingresos a final de temporada

  Scenario: se obtiene el balance de un equipo para una temporada
    Given existe un usuario "ferg"
    And hay 4 equipos registrados en la liga "Liga B" con jugadores
    And se está en la temporada 2 y periodo "Final"
    And el equipo "Gas FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Gas FC" tiene cargado -60000 de la temporada 1 periodo "Principio"
    And el equipo "Gas FC" tiene cargado -10000 de la temporada 2 periodo "Final"
    And el equipo "Gas FC" tiene cargado 5000 de la temporada 2 periodo "Principio"
    And el equipo "Gas FC" tiene cargado 30000 de la temporada 3 periodo "Final" con detalle "Bono"
    And el usuario con alias "ferg" esta logueado
    When el usuario entra a la pantalla de vista de balances de un equipo
    And el usuario selecciona la temporada 3 en la pantalla de vista de balances de un equipo
    And el usuario selecciona el equipo "Gas FC" en la pantalla de vista de balances de un equipo
    Then el equipo "Gas FC" tiene cargado "-$15.000" en la columna total a principio de temporada
    Then el equipo "Gas FC" tiene cargado "-$15.000" en la columna total a mitad de temporada
    Then el equipo "Gas FC" tiene cargado "$15.000" en la columna total a final de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna gastos a principio de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna gastos a mitad de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna gastos a final de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna ingresos a principio de temporada
    Then el equipo "Gas FC" tiene cargado "$0" en la columna ingresos a mitad de temporada
    Then el equipo "Gas FC" tiene cargado "$30.000" en la columna ingresos a final de temporada
    Then cargada la transaccion con detalle "Bono" y monto "$30.000" en el periodo "Final"

  Scenario: se cargan los salarios de los equipos de una liga
    Given existe un administrador "yid"
    And hay 4 equipos registrados en la liga "Liga B" con jugadores
    And se está en la temporada 1 y periodo "Final"
    And el equipo "Nico FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Gas FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Marc FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Mar FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el usuario con alias "yid" esta logueado
    When el administrador le carga le carga los salarios a los equipos de la liga "Liga B"
    Then se muestra mensaje que los salarios fueron cargados exitosamente
    And el equipo "Nico FC" tiene 49088
    And el equipo "Gas FC" tiene 49088
    And el equipo "Marc FC" tiene 49089
    And el equipo "Mar FC" tiene 35000

  Scenario: se ven los salarios de los equipos de una liga
    Given existe un usuario "ferg"
    And hay 4 equipos registrados en la liga "Liga B" con jugadores
    And se está en la temporada 1 y periodo "Final"
    And el equipo "Nico FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Gas FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Marc FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el equipo "Mar FC" tiene cargado 50000 de la temporada 1 periodo "Mitad"
    And el usuario con alias "ferg" esta logueado
    When el usuario ve los salarios de los equipos de la liga "Liga B"
    Then se muestra que el equipo "Nico FC" paga 912
    And se muestra que el equipo "Gas FC" paga 912
    And se muestra que el equipo "Marc FC" paga 911
    And se muestra que el equipo "Mar FC" paga 15000