package com.tesis.futpal.integracion;

import com.tesis.futpal.excepciones.LigaInexistenteException;

import static org.assertj.core.api.Assertions.assertThat;

//@SpringBootTest
//@Transactional
//@Ignore("Se ignoran para el pipeline hasta implementar Test Containers")
public class ServicioBalanceTest {

//    @PersistenceContext
//    protected EntityManager entityManager;
//
//    @Autowired
//    private RepositorioBalance repositorioBalance;
//    @Autowired
//    private RepositorioEquipo repositorioEquipo;
//    @Autowired
//    private RepositorioTransferencia repositorioTransferencia;
//    @Autowired
//    private RepositorioMovimientoJugador repositorioMovimientoJugador;
//    @Autowired
//    private RepositorioJugador repositorioJugador;
//    @MockBean
//    private ServicioLiga servicioLiga;
//    @MockBean
//    private ServicioTemporada servicioTemporada;
//
//    @Autowired
//    private ServicioBalance servicioBalance;
//    private Balance balance;
//    private Equipo equipo;
//    private Jugador jugador1;
//    private Equipo equipo1;
//    private Equipo equipo2;
//    private List<BalanceSalario> respuestaBalancesSalariosLiga;
//
//
//    @BeforeEach
//    public void inicializar() {
//        this.repositorioBalance.deleteAll();
//        entityManager.createNativeQuery("ALTER SEQUENCE balance_id_seq RESTART WITH 1").executeUpdate();
//        this.repositorioJugador.deleteAll();
//        entityManager.createNativeQuery("ALTER SEQUENCE jugador_id_seq RESTART WITH 1").executeUpdate();
//        this.repositorioMovimientoJugador.deleteAll();
//        entityManager.createNativeQuery("ALTER SEQUENCE movimiento_jugador_id_seq RESTART WITH 1").executeUpdate();
//        this.repositorioTransferencia.deleteAll();
//        entityManager.createNativeQuery("ALTER SEQUENCE transferencia_id_seq RESTART WITH 1").executeUpdate();
//        this.repositorioEquipo.deleteAll();
//        entityManager.createNativeQuery("ALTER SEQUENCE equipo_id_equipo_seq RESTART WITH 1").executeUpdate();
//    }
//
//    @Test
//    public void seCargaUnBalanceParaUnEquipoExitosamente() {
//        dadoUnEquipo();
//        dadoUnBalanceValido();
//
//        cuandoSeCargaUnBalanceParaUnEquipo();
//
//        seVerificaQueSeGuardaElBalance();
//    }
//
//    @Test
//    public void seObtienenLosSalariosDeLosEquipos() throws LigaInexistenteException {
//        dadoQueElRepositorioDeLigasDevuelveDosEquipos();
//        dadoQueHayJugadoresEnLosEquipos();
//        dadoQueHayUnMovimientoAMitadDeTemporada();
//        dadoQueLaTemporadaActualEsLa3();
//
//        cuandoSeObtienenLosSalariosDeLosEquipos();
//
//        seVerificaQueSeObtieneElSalarioDeCadaEquipo();
//    }
//
//    private void seVerificaQueSeObtieneElSalarioDeCadaEquipo() {
//        BalanceSalario salarioEquipo1 = respuestaBalancesSalariosLiga.get(0);
//        BalanceSalario salarioEquipo2 = respuestaBalancesSalariosLiga.get(1);
//
//        assertThat(salarioEquipo1.getMonto()).isEqualTo(3000);
//        assertThat(salarioEquipo1.getNombreEquipo()).isEqualTo("Equipo 1");
//        assertThat(salarioEquipo1.getIdEquipo()).isEqualTo(1);
//
//        assertThat(salarioEquipo2.getMonto()).isEqualTo(4300);
//        assertThat(salarioEquipo2.getNombreEquipo()).isEqualTo("Equipo 2");
//        assertThat(salarioEquipo2.getIdEquipo()).isEqualTo(2);
//    }
//
//    private void cuandoSeObtienenLosSalariosDeLosEquipos() throws LigaInexistenteException {
//        respuestaBalancesSalariosLiga = servicioBalance.obtenerSalariosDeLaLiga(2);
//    }
//
//    private void dadoQueLaTemporadaActualEsLa3() {
//        Temporada temporada = new Temporada();
//        temporada.setNumero(3);
//        when(servicioTemporada.obtenerTemporadaActual()).thenReturn(temporada);
//    }
//
//    private void dadoQueHayUnMovimientoAMitadDeTemporada() {
//        MovimientoJugador movimientoJugador1 = new MovimientoJugador();
//
//        PeriodoTemporada periodoTemporadaMitad = new PeriodoTemporada();
//        periodoTemporadaMitad.setId(2);
//        movimientoJugador1.setPeriodo(periodoTemporadaMitad);
//
//        movimientoJugador1.setJugador(jugador1);
//        movimientoJugador1.setEquipoOrigen(equipo2);
//        movimientoJugador1.setEquipoDestino(equipo1);
//        movimientoJugador1.setNumeroTemporada(3);
//
//        TipoMovimiento traspaso = new TipoMovimiento();
//        traspaso.setId(1);
//        movimientoJugador1.setTipoMovimiento(traspaso);
//
//        Transferencia transferencia = new Transferencia();
//        EstadoTransferencia estadoTransferencia = new EstadoTransferencia();
//        estadoTransferencia.setId(3);
//        transferencia.setEstadoTransferencia(estadoTransferencia);
//        transferencia = repositorioTransferencia.save(transferencia);
//        movimientoJugador1.setTransferencia(transferencia);
//
//        repositorioMovimientoJugador.save(movimientoJugador1);
//    }
//
//    private void dadoQueHayJugadoresEnLosEquipos() {
//        jugador1 = new Jugador();
//        jugador1.setNombre("Jugador 1");
//        jugador1.setEquipo(equipo1);
//        jugador1.setSalario(1000);
//        jugador1 = repositorioJugador.save(jugador1);
//
//        Jugador jugador2 = new Jugador();
//        jugador2.setNombre("Jugador 2");
//        jugador2.setEquipo(equipo1);
//        jugador2.setSalario(2500);
//        jugador2 = repositorioJugador.save(jugador2);
//
//        Jugador jugador3 = new Jugador();
//        jugador3.setNombre("Jugador 3");
//        jugador3.setEquipo(equipo2);
//        jugador3.setSalario(800);
//        jugador3 = repositorioJugador.save(jugador3);
//
//        Jugador jugador4 = new Jugador();
//        jugador4.setNombre("Jugador 4");
//        jugador4.setEquipo(equipo2);
//        jugador4.setSalario(3000);
//        jugador4 = repositorioJugador.save(jugador4);
//    }
//
//    private void dadoQueElRepositorioDeLigasDevuelveDosEquipos() throws LigaInexistenteException {
//        equipo1 = new Equipo();
//        equipo1.setNombreEquipo("Equipo 1");
//        equipo1 = repositorioEquipo.save(equipo1);
//        equipo2 = new Equipo();
//        equipo2.setNombreEquipo("Equipo 2");
//        equipo2 = repositorioEquipo.save(equipo2);
//
//        when(servicioLiga.obtenerEquipos(anyInt())).thenReturn(Lists.list(equipo1, equipo2));
//    }
//
//    private void dadoUnEquipo() {
//        equipo = new Equipo();
//        equipo.setNombreEquipo("Esquimbo");
//        EstadoEquipo estadoEquipo = new EstadoEquipo();
//        estadoEquipo.setId(EstadoEquipoEnum.APROBADO.getId());
//        equipo.setEstadoEquipo(estadoEquipo);
//        equipo.setIdUsuario(1L);
//        equipo.setMotivoUltimoCambioEstado("");
//
//        repositorioEquipo.save(equipo);
//    }
//
//
//    private void seVerificaQueSeGuardaElBalance() {
//        Balance balanceEncontrado = repositorioBalance.findById(1).get();
//        assertThat(balanceEncontrado.getDetalle()).isEqualTo("detalle de prueba");
//        assertThat(balanceEncontrado.getEquipo().getNombreEquipo()).isEqualTo("Esquimbo");
//        assertThat(balanceEncontrado.getMonto()).isEqualTo(75000);
//        assertThat(balanceEncontrado.getNumeroTemporada()).isEqualTo(1);
//        assertThat(balanceEncontrado.getPeriodo().getId()).isEqualTo(1);
//    }
//
//    private void cuandoSeCargaUnBalanceParaUnEquipo() {
//        servicioBalance.cargarBalance(balance);
//    }
//
//    private void dadoUnBalanceValido() {
//        balance = new Balance();
//        balance.setDetalle("detalle de prueba");
//
//        balance.setEquipo(equipo);
//
//        balance.setMonto(75000);
//
//        balance.setNumeroTemporada(1);
//
//        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
//        periodoTemporada.setId(1);
//        balance.setPeriodo(periodoTemporada);
//    }
}
