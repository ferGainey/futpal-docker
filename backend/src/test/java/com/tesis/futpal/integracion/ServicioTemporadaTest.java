package com.tesis.futpal.integracion;

import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.repositorios.RepositorioTemporada;
import com.tesis.futpal.servicios.ServicioTemporada;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
@Ignore("Se ignoran para el pipeline hasta implementar Test Containers")
public class ServicioTemporadaTest {


    private static final String PRINCIPIO_TEMPORADA = "Principio";
    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    private RepositorioTemporada repositorioTemporada;

    @Autowired
    private ServicioTemporada servicioTemporada;

    private static final String FINAL_TEMPORADA = "Final";

    @BeforeEach
    public void inicializar() {
        this.repositorioTemporada.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE temporada_id_seq RESTART WITH 2").executeUpdate();
    }

    @Test
    public void seAvanzaDeTemporadaDeManeraExitosa() {
        dadoQueSeEstaAFinalDeLaTemporada2();

        cuandoSeAvanzaDeTemporada();

        seVerificaQueSeEstaEnLaTemporadaConPeriodo(3, PRINCIPIO_TEMPORADA);
    }

    private void seVerificaQueSeEstaEnLaTemporadaConPeriodo(int numeroTemporadaEsperado, String periodoTemporadaEsperado) {
        Temporada temporadaActual = repositorioTemporada.findByActual(true).get(0);
        assertThat(temporadaActual.getNumero()).isEqualTo(numeroTemporadaEsperado);
        assertThat(temporadaActual.getPeriodoTemporada().getNombre()).isEqualTo(periodoTemporadaEsperado);
    }

    private void cuandoSeAvanzaDeTemporada() {
        servicioTemporada.avanzarTemporada();
    }

    private void dadoQueSeEstaAFinalDeLaTemporada2() {
        Temporada temporada = new Temporada();
        temporada.setNumero(2);
        PeriodoTemporada finalTemporada = new PeriodoTemporada();
        finalTemporada.setNombre(FINAL_TEMPORADA);
        finalTemporada.setId(3);
        finalTemporada.setProximaId(1);
        temporada.setPeriodoTemporada(finalTemporada);
        temporada.setActual(true);
        repositorioTemporada.save(temporada);
    }
}
