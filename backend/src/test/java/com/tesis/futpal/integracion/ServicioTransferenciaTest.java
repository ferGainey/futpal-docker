package com.tesis.futpal.integracion;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.transferencia.EstadoTransferencia;
import com.tesis.futpal.modelos.transferencia.Mercado;
import com.tesis.futpal.modelos.transferencia.MovimientoJugador;
import com.tesis.futpal.modelos.transferencia.Transferencia;
import com.tesis.futpal.repositorios.*;
import com.tesis.futpal.servicios.ServicioTransferencia;
import org.junit.Ignore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
@Ignore("Se ignoran para el pipeline hasta implementar Test Containers")
public class ServicioTransferenciaTest {

//    private static final Integer ID_ESTADO_TRANSFERENCIA_PENDIENTE = 1;
//    private static final Integer ID_ESTADO_TRANSFERENCIA_CONFIRMADA = 3;
//    private static final Integer ID_ESTADO_TRANSFERENCIA_CANCELADA = 2;
//    private static final String CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA = "CARGA_TRANSFERENCIAS";
//    @PersistenceContext
//    protected EntityManager entityManager;
//
//    @Autowired
//    private RepositorioTransferencia repositorioTransferencia;
//
//    @Autowired
//    private ServicioTransferencia servicioTransferencia;
//    @Autowired
//    private RepositorioJugador repositorioJugador;
//    @Autowired
//    private RepositorioMovimientoJugador repositorioMovimientoJugador;
//    @Autowired
//    private RepositorioEquipo repositorioEquipo;
//    @Autowired
//    private RepositorioMercado repositorioMercado;
//    private MovimientoJugador movimientoJugadorGuardado;
//    private Transferencia transferenciaPendiente1;
//
//    @BeforeEach
//    public void inicializar() {
//        entityManager.createNativeQuery("ALTER SEQUENCE jugador_id_seq RESTART WITH 1").executeUpdate();
//        this.repositorioMovimientoJugador.deleteAll();
//        entityManager.createNativeQuery("ALTER SEQUENCE movimiento_jugador_id_seq RESTART WITH 1").executeUpdate();
//        this.repositorioTransferencia.deleteAll();
//        entityManager.createNativeQuery("ALTER SEQUENCE transferencia_id_seq RESTART WITH 1").executeUpdate();
//        this.repositorioJugador.deleteAll();
//        this.repositorioEquipo.deleteAll();
//        entityManager.createNativeQuery("ALTER SEQUENCE equipo_id_equipo_seq RESTART WITH 1").executeUpdate();
//    }
//
//    @Test
//    public void seCancelanLasTransferenciaPendientes() {
//        dadoQueExisten2TransferenciansPendientes();
//        dadoQueExisteUnaTransferenciaConfirmada();
//        cuandoSeCancelanLasTransferenciasPendientes();
//        seVerificaQueSeCancelaronLasTransferenciasPendientes();
//    }
//
//    @Test
//    public void seCancelanLasTransferenciaPendientesQueInvolucranALosJugadores() {
//        dadoQueExisten2TransferenciansPendientes();
//        dadoQueLaPrimeraTransferenciaInvolucraAlJugadorPepe();
//        cuandoSeCancelanLasTransferenciasPendientesQueInvolucranLosJugadoresPepe();
//        seVerificaQueSeCancelaronLasTransferenciasPendientesQueInvolucranAPepe();
//    }
//
//    @Test
//    public void seCambiaElEstadoDelMercado() {
//        dadoQueElMercadoNoEstaActivo();
//
//        cuandoCambioElEstadoDelMercadoAActivo();
//
//        seVerificaQueElMercadoEstaActivo();
//    }
//
//    private void dadoQueElMercadoNoEstaActivo() {
//        Mercado mercado = repositorioMercado.findById(CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA).get();
//        mercado.setActivo(false);
//    }
//
//    private void dadoQueExisteUnaTransferenciaConfirmada() {
//        EstadoTransferencia confirmada = new EstadoTransferencia();
//        confirmada.setId(ID_ESTADO_TRANSFERENCIA_CONFIRMADA);
//
//        Transferencia transferencia1 = new Transferencia();
//        transferencia1.setEstadoTransferencia(confirmada);
//
//        repositorioTransferencia.save(transferencia1);
//    }
//
//    private void dadoQueExisten2TransferenciansPendientes() {
//        EstadoTransferencia pendiente = new EstadoTransferencia();
//        pendiente.setId(ID_ESTADO_TRANSFERENCIA_PENDIENTE);
//
//        Transferencia transferencia1 = new Transferencia();
//        transferencia1.setEstadoTransferencia(pendiente);
//        Transferencia transferencia2 = new Transferencia();
//        transferencia2.setEstadoTransferencia(pendiente);
//
//        transferenciaPendiente1 = repositorioTransferencia.save(transferencia1);
//        repositorioTransferencia.save(transferencia2);
//    }
//
//    private void dadoQueLaPrimeraTransferenciaInvolucraAlJugadorPepe() {
//        Equipo equipo1 = new Equipo();
//        equipo1.setNombreEquipo("equipo1");
//        Equipo equipoGuardado1 = repositorioEquipo.save(equipo1);
//        Equipo equipo2 = new Equipo();
//        equipo2.setNombreEquipo("equipo2");
//        Equipo equipoGuardado2 = repositorioEquipo.save(equipo2);
//        Jugador pepe = new Jugador();
//        pepe.setNombre("pepe");
//        Jugador pepeGuardado = repositorioJugador.save(pepe);
//        MovimientoJugador movimientoJugador = new MovimientoJugador();
//        movimientoJugador.setJugador(pepeGuardado);
//        movimientoJugador.setTransferencia(transferenciaPendiente1);
//        movimientoJugador.setNumeroTemporada(1);
//        movimientoJugador.setEquipoOrigen(equipoGuardado1);
//        movimientoJugador.setEquipoDestino(equipoGuardado2);
//        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
//        periodoTemporada.setId(1);
//        movimientoJugador.setPeriodo(periodoTemporada);
//        movimientoJugadorGuardado = repositorioMovimientoJugador.save(movimientoJugador);
//    }
//
//    private void cuandoSeCancelanLasTransferenciasPendientesQueInvolucranLosJugadoresPepe() {
//        List<MovimientoJugador> movimientosJugadores = new ArrayList<>();
//        movimientosJugadores.add(movimientoJugadorGuardado);
//        servicioTransferencia.cancelarTransferenciasPendientesQueInvolucrenALosJugadores(movimientosJugadores);
//        entityManager.clear();
//    }
//
//
//    private void cuandoCambioElEstadoDelMercadoAActivo() {
//        servicioTransferencia.cambiarEstadoDeMercado(true);
//    }
//
//    private void cuandoSeCancelanLasTransferenciasPendientes() {
//        servicioTransferencia.cancelarTransferenciasPendientes();
//        entityManager.clear();
//    }
//
//    private void seVerificaQueElMercadoEstaActivo() {
//        Mercado mercadoActualizado = repositorioMercado.findById(CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA).get();
//        assertThat(mercadoActualizado.isActivo()).isTrue();
//    }
//
//    private void seVerificaQueSeCancelaronLasTransferenciasPendientes() {
//        List<Transferencia> transferencias = repositorioTransferencia.findAll();
//        assertThat(transferencias.size()).isEqualTo(3);
//        assertThat(transferencias.get(0).getEstadoTransferencia().getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_CONFIRMADA);
//        assertThat(transferencias.get(1).getEstadoTransferencia().getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_CANCELADA);
//        assertThat(transferencias.get(2).getEstadoTransferencia().getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_CANCELADA);
//    }
//
//    private void seVerificaQueSeCancelaronLasTransferenciasPendientesQueInvolucranAPepe() {
//        List<Transferencia> transferencias = repositorioTransferencia.findAll();
//        assertThat(transferencias.size()).isEqualTo(2);
//
//        assertThat(transferencias.get(0).getEstadoTransferencia().getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_PENDIENTE);
//        assertThat(transferencias.get(1).getEstadoTransferencia().getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_CANCELADA);
//    }
}
