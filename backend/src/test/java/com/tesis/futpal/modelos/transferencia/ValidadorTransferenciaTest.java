package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.excepciones.JugadorYaTransferidoException;
import com.tesis.futpal.modelos.plantel.EstadoJugador;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.comunicacion.requests.transferencia.JugadorTransferencia;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ValidadorTransferenciaTest {

    private static final Long ID_ESTADO_DISPONIBLE = 1L;
    private static final Long ID_ESTADO_TRASPASO_ACORDADO = 4L;

    @InjectMocks
    private ValidadorTransferencia validadorTransferencia;
    private List<JugadorTransferencia> jugadoresTraspaso;

    // El jugador fue transferido por un equipo (confirmada) y luego el otro equipo lo vuelve a transferir --> Válida

    @Test
    public void seValidaUnaTransferenciaDeJugadoresExitosamente() throws JugadorYaTransferidoException {
        dadoUnaListaDeJugadoresParaTraspasoSinJugadoresConTraspasoAcordado();
        cuandoSeValidaLaTransferencia();
        seVerificaQueEsExitosa();
    }

    @Test
    public void seLanzaExcepcionSiElJugadorYaSeTransfirioEnElFuturo() {
        dadoUnaListaDeJugadoresParaTraspasoConJugadoresConTraspasoAcordado();

        assertThrows(JugadorYaTransferidoException.class, this::cuandoSeValidaLaTransferencia);
    }

    @Test
    public void seLanzaExcepcionSiHayUnJugadorDuplicadoEnTraspaso() {
        dadoUnaListaDeJugadoresParaTraspasoConRepetidos();

        assertThrows(JugadorYaTransferidoException.class, this::cuandoSeValidaLaTransferencia);
    }

    private void seVerificaQueEsExitosa() {
        // pasa la verificación
    }

    private void dadoUnaListaDeJugadoresParaTraspasoSinJugadoresConTraspasoAcordado() {
        EstadoJugador disponible = new EstadoJugador();
        disponible.setId(ID_ESTADO_DISPONIBLE);
        Jugador jugadorDisponible1 = new Jugador();
        jugadorDisponible1.setId(1L);
        jugadorDisponible1.setEstadoJugador(disponible);
        Jugador jugadorDisponible2 = new Jugador();
        jugadorDisponible2.setId(2L);
        jugadorDisponible2.setEstadoJugador(disponible);

        JugadorTransferencia jugadorTraspaso1 = new JugadorTransferencia();
        jugadorTraspaso1.setJugador(jugadorDisponible1);
        JugadorTransferencia jugadorTraspaso2 = new JugadorTransferencia();
        jugadorTraspaso2.setJugador(jugadorDisponible2);
        jugadoresTraspaso = Lists.list(jugadorTraspaso1, jugadorTraspaso2);
    }

    private void dadoUnaListaDeJugadoresParaTraspasoConJugadoresConTraspasoAcordado() {
        EstadoJugador traspasoAcordado = new EstadoJugador();
        traspasoAcordado.setId(ID_ESTADO_TRASPASO_ACORDADO);
        Jugador jugadorTraspasado1 = new Jugador();
        jugadorTraspasado1.setId(1L);
        jugadorTraspasado1.setEstadoJugador(traspasoAcordado);
        Jugador jugadorTraspasado2 = new Jugador();
        jugadorTraspasado2.setId(2L);
        jugadorTraspasado2.setEstadoJugador(traspasoAcordado);

        JugadorTransferencia jugadorTraspaso1 = new JugadorTransferencia();
        jugadorTraspaso1.setJugador(jugadorTraspasado1);
        JugadorTransferencia jugadorTraspaso2 = new JugadorTransferencia();
        jugadorTraspaso2.setJugador(jugadorTraspasado2);
        jugadoresTraspaso = Lists.list(jugadorTraspaso1, jugadorTraspaso2);
    }

    private void dadoUnaListaDeJugadoresParaTraspasoConRepetidos() {
        Jugador jugador1 = new Jugador();
        jugador1.setId(1L);

        JugadorTransferencia jugadorTraspaso1 = new JugadorTransferencia();
        jugadorTraspaso1.setJugador(jugador1);
        JugadorTransferencia jugadorTraspaso2 = new JugadorTransferencia();
        jugadorTraspaso2.setJugador(jugador1);
        jugadoresTraspaso = Lists.list(jugadorTraspaso1, jugadorTraspaso2);
    }

    private void cuandoSeValidaLaTransferencia() throws JugadorYaTransferidoException {
        validadorTransferencia.validarTraspasos(jugadoresTraspaso);
    }
}