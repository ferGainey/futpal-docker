package com.tesis.futpal.modelos;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.liga.ComparadorPosicion;
import com.tesis.futpal.modelos.liga.PosicionLiga;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ComparadorPosicionTest {

    private static final String NOMBRE_EQUIPO_CCJ = "CCJ";
    private static final String NOMBRE_EQUIPO_AFALP = "Afalp";
    private static final String NOMBRE_EQUIPO_UNTREF = "Untref";
    private List<PosicionLiga> posicionesLiga;
    private ComparadorPosicion comparadorPosicion;
    private List<PosicionLiga> resultado;

    @Test
    public void elEquipoConMasPuntosQuedaPrimero() {
        dadoTresPosicionesConDiferentesPuntos();
        cuandoSeOrdenaListaDePosicionesConElComparador();
        entoncesLasPosicionesQuedanOrdenadasPorPuntos();
    }

    @Test
    public void siTienenMismosPuntosElEquipoConMasDiferenciaDeGolQuedaPrimero() {
        dadoTresPosicionesConMismosPuntosYDistintaDiferenciaDeGol();
        cuandoSeOrdenaListaDePosicionesConElComparador();
        entoncesLasPosicionesQuedanOrdenadasPorDiferenciaDeGol();
    }

    @Test
    public void siTienenMismosPuntosYMismaDiferenciaDeGolQuedaPrimeroElQueMasGolesAFavorTenga() {
        dadoTresPosicionesConMismosPuntosYMismaDiferenciaDeGolYDistintosGolesAFavor();
        cuandoSeOrdenaListaDePosicionesConElComparador();
        entoncesLasPosicionesQuedanOrdenadasPorGolesAFavor();
    }

    private void entoncesLasPosicionesQuedanOrdenadasPorGolesAFavor() {
        assertThat(posicionesLiga.get(0).getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_UNTREF);
        assertThat(posicionesLiga.get(1).getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_CCJ);
        assertThat(posicionesLiga.get(2).getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_AFALP);
    }

    private void dadoTresPosicionesConMismosPuntosYMismaDiferenciaDeGolYDistintosGolesAFavor() {
        PosicionLiga posicionLiga1 = crearPosicionLiga(NOMBRE_EQUIPO_AFALP, 3, 0, 0);
        PosicionLiga posicionLiga2 = crearPosicionLiga(NOMBRE_EQUIPO_UNTREF,3, 0, 6);
        PosicionLiga posicionLiga3 = crearPosicionLiga(NOMBRE_EQUIPO_CCJ,3, 0, 2);
        posicionesLiga = Lists.list(posicionLiga1, posicionLiga2, posicionLiga3);
    }

    private void entoncesLasPosicionesQuedanOrdenadasPorDiferenciaDeGol() {
        assertThat(posicionesLiga.get(0).getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_AFALP);
        assertThat(posicionesLiga.get(1).getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_CCJ);
        assertThat(posicionesLiga.get(2).getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_UNTREF);
    }

    private void dadoTresPosicionesConMismosPuntosYDistintaDiferenciaDeGol() {
        PosicionLiga posicionLiga1 = crearPosicionLiga(NOMBRE_EQUIPO_AFALP, 3, 10, 20);
        PosicionLiga posicionLiga2 = crearPosicionLiga(NOMBRE_EQUIPO_UNTREF,3, -15, 3);
        PosicionLiga posicionLiga3 = crearPosicionLiga(NOMBRE_EQUIPO_CCJ,3, 5, 8);
        posicionesLiga = Lists.list(posicionLiga1, posicionLiga2, posicionLiga3);
    }

    private void entoncesLasPosicionesQuedanOrdenadasPorPuntos() {
        assertThat(posicionesLiga.get(0).getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_CCJ);
        assertThat(posicionesLiga.get(1).getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_UNTREF);
        assertThat(posicionesLiga.get(2).getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_AFALP);
    }

    private void cuandoSeOrdenaListaDePosicionesConElComparador() {
        comparadorPosicion = new ComparadorPosicion();
        posicionesLiga.sort((posicion1, posicion2) -> comparadorPosicion.comparar(posicion1, posicion2));
        Collections.reverse(posicionesLiga);
        resultado = posicionesLiga;
    }

    private void dadoTresPosicionesConDiferentesPuntos() {
        PosicionLiga posicionLiga1 = crearPosicionLiga(NOMBRE_EQUIPO_AFALP, 1, 10, 20);
        PosicionLiga posicionLiga2 = crearPosicionLiga(NOMBRE_EQUIPO_UNTREF,4, -15, 3);
        PosicionLiga posicionLiga3 = crearPosicionLiga(NOMBRE_EQUIPO_CCJ,9, 5, 8);
        posicionesLiga = Lists.list(posicionLiga1, posicionLiga2, posicionLiga3);
    }

    private PosicionLiga crearPosicionLiga(String nombreEquipo, Integer puntosLiga, Integer diferenciaDeGoles, Integer golesAFavor) {
        PosicionLiga posicionLiga = new PosicionLiga();
        Equipo equipo = new Equipo();
        equipo.setNombreEquipo(nombreEquipo);
        posicionLiga.setEquipo(equipo);
        posicionLiga.setPuntosLiga(puntosLiga);
        posicionLiga.setDiferenciaDeGoles(diferenciaDeGoles);
        posicionLiga.setGolesAFavor(golesAFavor);
        return posicionLiga;
    }
}