package com.tesis.futpal.modelos;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.comunicacion.requests.transferencia.JugadorPrestamoTransferencia;
import com.tesis.futpal.comunicacion.requests.transferencia.JugadorTransferencia;
import com.tesis.futpal.comunicacion.requests.transferencia.MontoTransferencia;
import com.tesis.futpal.comunicacion.requests.transferencia.RequestCargaTransferencia;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.transferencia.MovimientoJugador;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class RequestCargaTransferenciaTest {

    private static final Integer ID_EQUIPO_AFALP = 1;
    private static final Integer ID_EQUIPO_UNTREF = 2;
    private static final Long ID_JUGADOR_1 = 1L;
    private static final Long ID_JUGADOR_2 = 2L;
    private static final Integer NUMERO_TEMPORADA_1 = 1;
    private static final Integer ID_PERIODO_1 = 1;
    private static final Integer MONTO_BALANCE_1 = 50000;
    private static final Integer NUMERO_TEMPORADA_2 = 2;
    private static final Integer ID_PERIODO_2 = 2;
    private static final Integer ID_TIPO_TRANSFERENCIA_TRASPASO = 1;
    private static final Integer ID_TIPO_TRANSFERENCIA_PRESTAMO = 2;
    private List<MovimientoJugador> resultadoListaMovimientos;
    private Jugador jugador1;
    private Jugador jugador2;
    private List<JugadorTransferencia> listaDeTranferenciasDeJugadores;
    private JugadorTransferencia jugadorTransferencia1;
    private JugadorTransferencia jugadorTransferencia2;

    private RequestCargaTransferencia requestCargaTransferencia;
    private MontoTransferencia montoTransferencia1;
    private List<MontoTransferencia> listaDeTranferenciasDeMontos;
    private List<Balance> resultadoBalances;
    private JugadorPrestamoTransferencia jugadorPrestamoTransferencia;
    private List<JugadorPrestamoTransferencia> listaDeTranferenciasDeJugadoresAPrestamo;

    @Test
    public void seCrearUnaListaDeMovimientoDeJugadoresAPartirDeUnaListaDeJugadoresDeTransferencia() {
        dadoUnaListaJugadoresDeTransferencia();

        cuandoSeCreaLaListaDeMovimientos();

        seVerificaQueSeCreaLaListaDeMovimientos();
    }

    @Test
    public void seCrearUnaListaDePrestamosDeJugadoresAPartirDeUnaListaDeJugadoresDeTransferencia() {
        dadoUnaListaJugadoresDeTransferenciaAPrestamo();

        cuandoSeCreaLaListaDeMovimientos();

        seVerificaQueSeCreaLaListaDeMovimientosDePrestamos();
    }

    @Test
    public void seCrearUnaListaDeBalancesAPartirDeUnaListaDeMontosDeTransferencia() {
        dadoUnaListaJugadoresDeMontosDeTransferencia();

        cuandoSeCreaLaListaBalances();

        seVerificaQueSeCreaLaListaDeBalances();
    }

    private void dadoUnaListaJugadoresDeTransferenciaAPrestamo() {
        PeriodoTemporada periodoTemporadaComienzo = new PeriodoTemporada();
        periodoTemporadaComienzo.setId(ID_PERIODO_1);
        PeriodoTemporada periodoTemporadaFinal = new PeriodoTemporada();
        periodoTemporadaFinal.setId(ID_PERIODO_2);

        jugador1 = new Jugador();
        jugador1.setId(ID_JUGADOR_1);

        jugadorPrestamoTransferencia = new JugadorPrestamoTransferencia();
        jugadorPrestamoTransferencia.setJugador(jugador1);
        jugadorPrestamoTransferencia.setIdEquipoDestino(ID_EQUIPO_AFALP);
        jugadorPrestamoTransferencia.setIdEquipoOrigen(ID_EQUIPO_UNTREF);
        jugadorPrestamoTransferencia.setNumeroTemporadaComienzo(NUMERO_TEMPORADA_1);
        jugadorPrestamoTransferencia.setNumeroTemporadaFinal(NUMERO_TEMPORADA_2);
        jugadorPrestamoTransferencia.setPeriodoComienzo(periodoTemporadaComienzo);
        jugadorPrestamoTransferencia.setPeriodoFinal(periodoTemporadaFinal);

        listaDeTranferenciasDeJugadoresAPrestamo = Lists.list(jugadorPrestamoTransferencia);

        requestCargaTransferencia = new RequestCargaTransferencia();
        requestCargaTransferencia.setJugadoresPrestamoTransferencia(listaDeTranferenciasDeJugadoresAPrestamo);
    }

    private void seVerificaQueSeCreaLaListaDeMovimientosDePrestamos() {
        assertThat(resultadoListaMovimientos.size()).isEqualTo(2);
        MovimientoJugador primerMovimientoJugador = resultadoListaMovimientos.get(0);
        assertThat(primerMovimientoJugador.getEquipoDestino().getId()).isEqualTo(jugadorPrestamoTransferencia.getIdEquipoDestino().longValue());
        assertThat(primerMovimientoJugador.getEquipoOrigen().getId()).isEqualTo(jugadorPrestamoTransferencia.getIdEquipoOrigen().longValue());
        assertThat(primerMovimientoJugador.getJugador().getId()).isEqualTo(jugadorPrestamoTransferencia.getJugador().getId());
        assertThat(primerMovimientoJugador.getNumeroTemporada()).isEqualTo(jugadorPrestamoTransferencia.getNumeroTemporadaComienzo());
        assertThat(primerMovimientoJugador.getPeriodo().getId()).isEqualTo(jugadorPrestamoTransferencia.getPeriodoComienzo().getId());
        assertThat(primerMovimientoJugador.getTipoMovimiento().getId()).isEqualTo(ID_TIPO_TRANSFERENCIA_PRESTAMO);

        MovimientoJugador segundoMovimientoJugador = resultadoListaMovimientos.get(1);
        assertThat(segundoMovimientoJugador.getEquipoDestino().getId()).isEqualTo(jugadorPrestamoTransferencia.getIdEquipoOrigen().longValue());
        assertThat(segundoMovimientoJugador.getEquipoOrigen().getId()).isEqualTo(jugadorPrestamoTransferencia.getIdEquipoDestino().longValue());
        assertThat(segundoMovimientoJugador.getJugador().getId()).isEqualTo(jugadorPrestamoTransferencia.getJugador().getId());
        assertThat(segundoMovimientoJugador.getNumeroTemporada()).isEqualTo(jugadorPrestamoTransferencia.getNumeroTemporadaFinal());
        assertThat(segundoMovimientoJugador.getPeriodo().getId()).isEqualTo(jugadorPrestamoTransferencia.getPeriodoFinal().getId());
        assertThat(segundoMovimientoJugador.getTipoMovimiento().getId()).isEqualTo(ID_TIPO_TRANSFERENCIA_PRESTAMO);
    }

    private void seVerificaQueSeCreaLaListaDeBalances() {
        assertThat(resultadoBalances.size()).isEqualTo(2);
        Balance primerBalance = resultadoBalances.get(0);
        assertThat(primerBalance.getEquipo().getId().intValue()).isEqualTo(montoTransferencia1.getIdEquipoOrigen());
        assertThat(primerBalance.getMonto()).isEqualTo(montoTransferencia1.getMonto() * -1);
        assertThat(primerBalance.getNumeroTemporada()).isEqualTo(montoTransferencia1.getNumeroTemporada());
        assertThat(primerBalance.getPeriodo().getId()).isEqualTo(montoTransferencia1.getPeriodo().getId());
        assertThat(primerBalance.getDetalle()).isEqualTo("Traspaso: Fernando | Préstamo: Javier | ");

        Balance segundoBalance = resultadoBalances.get(1);
        assertThat(segundoBalance.getEquipo().getId().intValue()).isEqualTo(montoTransferencia1.getIdEquipoDestino());
        assertThat(segundoBalance.getMonto()).isEqualTo(montoTransferencia1.getMonto());
        assertThat(segundoBalance.getNumeroTemporada()).isEqualTo(montoTransferencia1.getNumeroTemporada());
        assertThat(segundoBalance.getPeriodo().getId()).isEqualTo(montoTransferencia1.getPeriodo().getId());
        assertThat(segundoBalance.getDetalle()).isEqualTo("Traspaso: Fernando | Préstamo: Javier | ");
    }

    private void cuandoSeCreaLaListaBalances() {
        resultadoBalances = requestCargaTransferencia.crearBalances();
    }

    private void dadoUnaListaJugadoresDeMontosDeTransferencia() {
        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(ID_PERIODO_1);

        montoTransferencia1 = new MontoTransferencia();
        montoTransferencia1.setIdEquipoDestino(ID_EQUIPO_AFALP);
        montoTransferencia1.setIdEquipoOrigen(ID_EQUIPO_UNTREF);
        montoTransferencia1.setMonto(MONTO_BALANCE_1);
        montoTransferencia1.setNumeroTemporada(NUMERO_TEMPORADA_1);
        montoTransferencia1.setPeriodo(periodoTemporada);

        listaDeTranferenciasDeMontos = Lists.list(montoTransferencia1);

        requestCargaTransferencia = new RequestCargaTransferencia();
        Jugador jugadorTraspasado = new Jugador();
        jugadorTraspasado.setNombre("Fernando");
        Jugador jugadorPrestado = new Jugador();
        jugadorPrestado.setNombre("Javier");
        JugadorTransferencia jugadorTransferencia = new JugadorTransferencia();
        jugadorTransferencia.setJugador(jugadorTraspasado);
        JugadorPrestamoTransferencia jugadorPrestamoTransferencia = new JugadorPrestamoTransferencia();
        jugadorPrestamoTransferencia.setJugador(jugadorPrestado);
        requestCargaTransferencia.setJugadoresTransferencia(Lists.list(jugadorTransferencia));
        requestCargaTransferencia.setJugadoresPrestamoTransferencia(Lists.list(jugadorPrestamoTransferencia));
        requestCargaTransferencia.setMontosTransferencias(listaDeTranferenciasDeMontos);
    }

    private void cuandoSeCreaLaListaDeMovimientos() {
        resultadoListaMovimientos = requestCargaTransferencia.crearMovimientosJugadores();
    }

    private void dadoUnaListaJugadoresDeTransferencia() {
        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(ID_PERIODO_1);

        jugador1 = new Jugador();
        jugador1.setId(ID_JUGADOR_1);
        jugadorTransferencia1 = new JugadorTransferencia();
        jugadorTransferencia1.setIdEquipoDestino(ID_EQUIPO_AFALP);
        jugadorTransferencia1.setIdEquipoOrigen(ID_EQUIPO_UNTREF);
        jugadorTransferencia1.setJugador(jugador1);
        jugadorTransferencia1.setNumeroTemporada(NUMERO_TEMPORADA_1);
        jugadorTransferencia1.setPeriodo(periodoTemporada);

        jugador2 = new Jugador();
        jugador2.setId(ID_JUGADOR_2);
        jugadorTransferencia2 = new JugadorTransferencia();
        jugadorTransferencia2.setIdEquipoDestino(ID_EQUIPO_UNTREF);
        jugadorTransferencia2.setIdEquipoOrigen(ID_EQUIPO_AFALP);
        jugadorTransferencia2.setJugador(jugador2);
        jugadorTransferencia2.setNumeroTemporada(NUMERO_TEMPORADA_1);
        jugadorTransferencia2.setPeriodo(periodoTemporada);

        listaDeTranferenciasDeJugadores = Lists.list(jugadorTransferencia1, jugadorTransferencia2);

        requestCargaTransferencia = new RequestCargaTransferencia();
        requestCargaTransferencia.setJugadoresTransferencia(listaDeTranferenciasDeJugadores);
    }

    private void seVerificaQueSeCreaLaListaDeMovimientos() {
        assertThat(resultadoListaMovimientos.size()).isEqualTo(2);
        MovimientoJugador primerMovimientoJugador = resultadoListaMovimientos.get(0);
        assertThat(primerMovimientoJugador.getEquipoDestino().getId()).isEqualTo(jugadorTransferencia1.getIdEquipoDestino().longValue());
        assertThat(primerMovimientoJugador.getEquipoOrigen().getId()).isEqualTo(jugadorTransferencia1.getIdEquipoOrigen().longValue());
        assertThat(primerMovimientoJugador.getJugador().getId()).isEqualTo(jugadorTransferencia1.getJugador().getId());
        assertThat(primerMovimientoJugador.getNumeroTemporada()).isEqualTo(jugadorTransferencia1.getNumeroTemporada());
        assertThat(primerMovimientoJugador.getPeriodo().getId()).isEqualTo(jugadorTransferencia1.getPeriodo().getId());
        assertThat(primerMovimientoJugador.getTipoMovimiento().getId()).isEqualTo(ID_TIPO_TRANSFERENCIA_TRASPASO);

        MovimientoJugador segundoMovimientoJugador = resultadoListaMovimientos.get(1);
        assertThat(segundoMovimientoJugador.getEquipoDestino().getId()).isEqualTo(jugadorTransferencia2.getIdEquipoDestino().longValue());
        assertThat(segundoMovimientoJugador.getEquipoOrigen().getId()).isEqualTo(jugadorTransferencia2.getIdEquipoOrigen().longValue());
        assertThat(segundoMovimientoJugador.getJugador().getId()).isEqualTo(jugadorTransferencia2.getJugador().getId());
        assertThat(segundoMovimientoJugador.getNumeroTemporada()).isEqualTo(jugadorTransferencia2.getNumeroTemporada());
        assertThat(segundoMovimientoJugador.getPeriodo().getId()).isEqualTo(jugadorTransferencia2.getPeriodo().getId());
        assertThat(segundoMovimientoJugador.getTipoMovimiento().getId()).isEqualTo(ID_TIPO_TRANSFERENCIA_TRASPASO);
    }
}
