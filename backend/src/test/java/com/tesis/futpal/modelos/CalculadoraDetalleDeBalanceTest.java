package com.tesis.futpal.modelos;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.balance.CalculadoraDetalleDeBalance;
import com.tesis.futpal.modelos.balance.DetalleBalance;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CalculadoraDetalleDeBalanceTest {

    private static final Integer ID_PERIODO_PRINCIPIO = 1;
    private static final Integer ID_PERIODO_MITAD = 2;
    private static final Integer ID_PERIODO_FINAL = 3;
    private List<Balance> listaBalances;
    private Equipo equipo;
    private DetalleBalance resultadoDetalleBalance;

    @Test
    public void seCalculaElDetalleDeUnBalance() {
        dadoUnaListaDeBalances();
        dadoUnEquipo();

        cuandoSeCalculaElDetalleParaLaTemporadaYEquipo(2, equipo);

        seVerificaQueSeCreoElDetalleCorrectamenteParaLaTemporada2();
    }

    private void seVerificaQueSeCreoElDetalleCorrectamenteParaLaTemporada2() {
        assertThat(resultadoDetalleBalance.getEquipo().getNombreEquipo()).isEqualTo("Afalp");

        assertThat(resultadoDetalleBalance.getTotalPeriodoPrincipio()).isEqualTo(-2000);
        assertThat(resultadoDetalleBalance.getTotalPeriodoMitad()).isEqualTo(-2000);
        assertThat(resultadoDetalleBalance.getTotalPeriodoFinal()).isEqualTo(2000);

        assertThat(resultadoDetalleBalance.getGastosPeriodoPrincipio()).isEqualTo(7000);
        assertThat(resultadoDetalleBalance.getGastosPeriodoMitad()).isEqualTo(0);
        assertThat(resultadoDetalleBalance.getGastosPeriodoFinal()).isEqualTo(0);

        assertThat(resultadoDetalleBalance.getIngresosPeriodoPrincipio()).isEqualTo(0);
        assertThat(resultadoDetalleBalance.getIngresosPeriodoMitad()).isEqualTo(0);
        assertThat(resultadoDetalleBalance.getIngresosPeriodoFinal()).isEqualTo(4000);
    }

    private void cuandoSeCalculaElDetalleParaLaTemporadaYEquipo(int numeroTemporada, Equipo equipo) {
        CalculadoraDetalleDeBalance calculadora = new CalculadoraDetalleDeBalance();
        resultadoDetalleBalance = calculadora.calcular(equipo, listaBalances, numeroTemporada);
    }

    private void dadoUnEquipo() {
        equipo = new Equipo();
        equipo.setId(1L);
        equipo.setNombreEquipo("Afalp");
    }

    private void dadoUnaListaDeBalances() {
        PeriodoTemporada principio = new PeriodoTemporada();
        principio.setId(ID_PERIODO_PRINCIPIO);
        PeriodoTemporada mitad = new PeriodoTemporada();
        mitad.setId(ID_PERIODO_MITAD);
        PeriodoTemporada periodoFinal = new PeriodoTemporada();
        periodoFinal.setId(ID_PERIODO_FINAL);

        Balance balance1 = new Balance();
        balance1.setNumeroTemporada(1);
        balance1.setPeriodo(mitad);
        balance1.setMonto(5000);

        Balance balance2 = new Balance();
        balance2.setNumeroTemporada(2);
        balance2.setPeriodo(principio);
        balance2.setMonto(-7000);

        Balance balance3 = new Balance();
        balance3.setNumeroTemporada(2);
        balance3.setPeriodo(periodoFinal);
        balance3.setMonto(4000);

        listaBalances = Lists.list(balance1, balance2, balance3);
    }
}
