package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CreadorDetalleTest {

    private static final int ESTADO_TRANSFERENCIA_PENDIENTE = 1;
    private Balance balance;
    private MovimientoJugador movimientoJugador;
    private Transferencia transferencia;
    private DetalleTransferencia resultado;
    private Equipo equipoOrigen;
    private Equipo equipoDestino;
    private Usuario usuarioOrigen;
    private Usuario usuarioDestino;

    @Mock
    private RepositorioEquipo repositorioEquipo;
    @InjectMocks
    private CreadorDetalle creadorDetalle;

    @Test
    public void seCreaDetalleTransferenciaAPartirDeUnaTransferencia() {
        dadaUnaTransferencia();
        dadoQueSeObtieneElEquipoOrigenAPartirDelUsuario();
        dadoQueSeObtieneElEquipoDestinoAPartirDelUsuario();

        cuandoSeCreaElDetalle();

        seVerificaQueSeCreaCorrectamente();
    }

    private void dadoQueSeObtieneElEquipoDestinoAPartirDelUsuario() {
        when(repositorioEquipo.findByIdUsuario(usuarioDestino.getId())).thenReturn(Lists.list(equipoDestino));
    }

    private void dadoQueSeObtieneElEquipoOrigenAPartirDelUsuario() {
        when(repositorioEquipo.findByIdUsuario(usuarioOrigen.getId())).thenReturn(Lists.list(equipoOrigen));
    }

    private void seVerificaQueSeCreaCorrectamente() {
        assertThat(resultado.getMovimientoJugadores()).hasSize(1);
        assertThat(resultado.getMontos()).hasSize(1);
        assertThat(resultado.getId()).isEqualTo(transferencia.getId());
        assertThat(resultado.getEquipoOrigenTransferencia().getNombreEquipo()).isEqualTo(equipoOrigen.getNombreEquipo());
        assertThat(resultado.getEquipoOrigenTransferencia().getId()).isEqualTo(equipoOrigen.getId());
        assertThat(resultado.getEquipoOrigenTransferencia().getIdUsuario()).isEqualTo(equipoOrigen.getIdUsuario());
        assertThat(resultado.getEquipoDestinoTransferencia().getNombreEquipo()).isEqualTo(equipoDestino.getNombreEquipo());
        assertThat(resultado.getEquipoDestinoTransferencia().getId()).isEqualTo(equipoDestino.getId());
        assertThat(resultado.getEquipoDestinoTransferencia().getIdUsuario()).isEqualTo(equipoDestino.getIdUsuario());
        assertThat(resultado.getEstadoTransferencia().getId()).isEqualTo(ESTADO_TRANSFERENCIA_PENDIENTE);
    }

    private void cuandoSeCreaElDetalle() {
        resultado = creadorDetalle.crearAPartirDe(transferencia);
    }

    private void dadaUnaTransferencia() {
        transferencia = new Transferencia();

        transferencia.setId(1);

        EstadoTransferencia estadoTransferencia = new EstadoTransferencia();
        estadoTransferencia.setId(ESTADO_TRANSFERENCIA_PENDIENTE);
        transferencia.setEstadoTransferencia(estadoTransferencia);

        balance = new Balance();
        balance.setId(1);
        Equipo equipo = new Equipo();
        equipo.setId(1L);
        equipo.setNombreEquipo("Equipo FC");
        equipo.setIdUsuario(1L);
        balance.setEquipo(equipo);
        balance.setDetalle("Un detalle");
        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(1);
        balance.setPeriodo(periodoTemporada);
        balance.setNumeroTemporada(3);
        balance.setTransferencia(transferencia);
        balance.setMonto(50000);

        transferencia.setBalances(Lists.list(balance));

        movimientoJugador = new MovimientoJugador();
        movimientoJugador.setId(1);
        movimientoJugador.setTransferencia(transferencia);
        equipoOrigen = new Equipo();
        equipoOrigen.setId(1L);
        equipoOrigen.setNombreEquipo("Origen FC");
        equipoOrigen.setIdUsuario(1L);
        movimientoJugador.setEquipoOrigen(equipoOrigen);
        equipoDestino = new Equipo();
        equipoDestino.setId(2L);
        equipoDestino.setNombreEquipo("Destino FC");
        equipoDestino.setIdUsuario(2L);
        movimientoJugador.setEquipoDestino(equipoDestino);
        movimientoJugador.setPeriodo(periodoTemporada);
        movimientoJugador.setNumeroTemporada(3);
        Jugador jugador = new Jugador();
        jugador.setNombre("Pedro");
        jugador.setId(1L);
        movimientoJugador.setJugador(jugador);

        transferencia.setMovimientos(Lists.list(movimientoJugador));

        usuarioOrigen = new Usuario();
        usuarioOrigen.setId(1L);
        transferencia.setUsuarioOrigen(usuarioOrigen);

        usuarioDestino = new Usuario();
        usuarioDestino.setId(2L);
        transferencia.setUsuarioDestino(usuarioDestino);
    }
}
