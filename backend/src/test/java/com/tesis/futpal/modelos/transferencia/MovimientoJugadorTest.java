package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.repositorios.RepositorioJugador;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MovimientoJugadorTest {

    private static final Long ID_ESTADO_JUGADOR_DISPONIBLE = 1L;
    private static final Integer ID_TIPO_TRANSFERENCIA_TRASPASO = 1;
    private static final Integer ID_TIPO_TRANSFERENCIA_PRESTAMO = 2;
    private static final Long ID_ESTADO_JUGADOR_PRESTADO = 2L;
    private RepositorioJugador repositorioJugador;
    private MovimientoJugador movimientoJugador = new MovimientoJugador();
    @Captor
    private ArgumentCaptor<Jugador> jugadorArgumentCaptor;
    private Jugador jugadorGuardado;

    @Test
    public void seEjecutaMovimientoTraspasoExitosamente() {
        dadoQueElMovimientoTieneUnJugador();
        dadoQueElMovimientoEsUnTraspaso();
        dadoUnRepositorioDeJugador();
        dadoQueSeObtieneElJugadorCorrectamente();
        dadoQueSePuedeGuardarAlJugador();
        dadoQueElEquipoDestinoEs("Afalp");

        cuandoSeEjecutaElMovimiento();

        seGuardaAlJugadorEnSuNuevoEquipo();
        seVerificaQueSeCambiaElEstadoDelJugadorADisponible();
    }

    @Test
    public void seEjecutaMovimientoDePrestamoDeComienzoExitosamente() {
        dadoQueElMovimientoTieneUnJugador();
        dadoQueElMovimientoEsUnPrestamoDeComienzo();
        dadoUnRepositorioDeJugador();
        dadoQueSeObtieneElJugadorCorrectamente();
        dadoQueSePuedeGuardarAlJugador();
        dadoQueElEquipoDestinoEs("Afalp");

        cuandoSeEjecutaElMovimiento();

        seGuardaAlJugadorEnSuNuevoEquipo();
        seVerificaQueSeCambiaElEstadoDelJugadorAPrestado();
    }

    @Test
    public void seEjecutaMovimientoDeFinDePrestamoExitosamente() {
        dadoQueElMovimientoTieneUnJugador();
        dadoQueElMovimientoEsUnFinDePrestamo();
        dadoUnRepositorioDeJugador();
        dadoQueSeObtieneElJugadorCorrectamente();
        dadoQueSePuedeGuardarAlJugador();
        dadoQueElEquipoDestinoEs("Afalp");

        cuandoSeEjecutaElMovimiento();

        seGuardaAlJugadorEnSuNuevoEquipo();
        seVerificaQueSeCambiaElEstadoDelJugadorADisponible();
    }

    private void dadoQueElMovimientoEsUnFinDePrestamo() {
        TipoMovimiento prestamo = new TipoMovimiento();
        prestamo.setId(ID_TIPO_TRANSFERENCIA_PRESTAMO);
        PrestamoJugador finDePrestamoJugador = new PrestamoJugador();
        finDePrestamoJugador.setEsFinDePrestamo(true);

        movimientoJugador.setPrestamoJugador(finDePrestamoJugador);
        movimientoJugador.setTipoMovimiento(prestamo);
    }

    private void seVerificaQueSeCambiaElEstadoDelJugadorAPrestado() {
        assertThat(jugadorGuardado.getEstadoJugador().getId()).isEqualTo(ID_ESTADO_JUGADOR_PRESTADO);
    }

    private void dadoQueElMovimientoEsUnPrestamoDeComienzo() {
        TipoMovimiento prestamo = new TipoMovimiento();
        prestamo.setId(ID_TIPO_TRANSFERENCIA_PRESTAMO);
        PrestamoJugador prestamoJugadorDeComienzo = new PrestamoJugador();
        prestamoJugadorDeComienzo.setEsFinDePrestamo(false);

        movimientoJugador.setPrestamoJugador(prestamoJugadorDeComienzo);
        movimientoJugador.setTipoMovimiento(prestamo);
    }

    private void dadoQueElMovimientoEsUnTraspaso() {
        TipoMovimiento traspaso = new TipoMovimiento();
        traspaso.setId(ID_TIPO_TRANSFERENCIA_TRASPASO);
        movimientoJugador.setTipoMovimiento(traspaso);
    }

    private void seVerificaQueSeCambiaElEstadoDelJugadorADisponible() {
        assertThat(jugadorGuardado.getEstadoJugador().getId()).isEqualTo(ID_ESTADO_JUGADOR_DISPONIBLE);
    }

    private void dadoQueElMovimientoTieneUnJugador() {
        Jugador jugador = new Jugador();
        jugador.setId(1L);
        movimientoJugador.setJugador(jugador);
    }

    private void dadoQueElEquipoDestinoEs(String nombreClub) {
        Equipo equipo = new Equipo();
        equipo.setNombreEquipo(nombreClub);
        movimientoJugador.setEquipoDestino(equipo);
    }

    private void dadoUnRepositorioDeJugador() {
        repositorioJugador = mock(RepositorioJugador.class);
    }

    private void dadoQueSePuedeGuardarAlJugador() {
        when(repositorioJugador.save(any())).thenReturn(new Jugador());
    }

    private void dadoQueSeObtieneElJugadorCorrectamente() {
        Jugador jugador = new Jugador();
        jugador.setId(1L);
        when(repositorioJugador.findById(any())).thenReturn(java.util.Optional.of(jugador));
    }

    private void cuandoSeEjecutaElMovimiento() {
        movimientoJugador.ejecutar(repositorioJugador);
    }

    private void seGuardaAlJugadorEnSuNuevoEquipo() {
        verify(repositorioJugador, times(1)).save(jugadorArgumentCaptor.capture());
        jugadorGuardado = jugadorArgumentCaptor.getValue();
        assertThat(jugadorGuardado.getEquipo().getNombreEquipo()).isEqualTo("Afalp");
    }
}