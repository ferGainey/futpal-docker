package com.tesis.futpal.modelos;

import com.tesis.futpal.modelos.usuario.Rol;
import com.tesis.futpal.modelos.usuario.TipoRol;
import com.tesis.futpal.modelos.usuario.Usuario;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class UsuarioTest {

    private static final Long ID_USUARIO = 1L;

    @Test
    public void seObtieneQueElUsuarioEsAdministradorSiContieneElRolAdministrador() {
        Usuario usuario = new Usuario();
        usuario.setId(ID_USUARIO);
        Rol administrador = new Rol();
        administrador.setId(TipoRol.ROL_ADMINISTRADOR.getId());
        usuario.setRoles(Collections.singletonList(administrador));

        assertThat(usuario.esAdministrador()).isTrue();
    }

    @Test
    public void seObtieneQueElUsuarioNOEsAdministradorSiNOContieneElRolAdministrador() {
        Usuario usuario = new Usuario();
        usuario.setId(ID_USUARIO);
        Rol rolUsuario = new Rol();
        rolUsuario.setId(TipoRol.ROL_USUARIO.getId());
        usuario.setRoles(Collections.singletonList(rolUsuario));

        assertThat(usuario.esAdministrador()).isFalse();
    }
}
