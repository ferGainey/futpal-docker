package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.modelos.plantel.Jugador;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class JugadorTest {

    private static final Integer ID_TIPO_TRANSFERENCIA_TRASPASO = 1;
    private static final Long ID_ESTADO_JUGADOR_TRASPASO_ACORDADO = 4L;
    private static final Integer ID_TIPO_TRANSFERENCIA_PRESTAMO = 3;
    private static final Long ID_ESTADO_JUGADOR_PRESTAMO_ACORDADO = 3L;

    private Jugador jugador;

    @Test
    public void seActualizaElEstadoDeUnJugadorParaUnTraspasoFuturo() {
        dadoUnJugador();
        cuandoSeActualizaElEstadoPorUnaTransferenciaFuturaDeUnTipoTransferenciaTraspaso();
        seVerificaQueSeActualizaElEstadoATraspasoAcordado();
    }

    // ToDo: se comenta este test, ya que momentaneamente no existe más esto.
//    @Test
//    public void seActualizaElEstadoDeUnJugadorParaUnPrestamoFuturo() {
//        dadoUnJugador();
//        cuandoSeActualizaElEstadoPorUnaTransferenciaFuturaDeUnTipoTransferenciaPrestamo();
//        seVerificaQueSeActualizaElEstadoAPrestamoAcordado();
//    }

    private void dadoUnJugador() {
        jugador = new Jugador();
    }

    private void cuandoSeActualizaElEstadoPorUnaTransferenciaFuturaDeUnTipoTransferenciaTraspaso() {
        TipoMovimiento traspaso = new TipoMovimiento();
        traspaso.setId(ID_TIPO_TRANSFERENCIA_TRASPASO);
        jugador.actualizarEstadoTransferenciaFutura(traspaso, ID_TIPO_TRANSFERENCIA_PRESTAMO);
    }

    private void cuandoSeActualizaElEstadoPorUnaTransferenciaFuturaDeUnTipoTransferenciaPrestamo() {
        TipoMovimiento prestamo = new TipoMovimiento();
        prestamo.setId(ID_TIPO_TRANSFERENCIA_PRESTAMO);
        jugador.actualizarEstadoTransferenciaFutura(prestamo, ID_TIPO_TRANSFERENCIA_PRESTAMO);
    }

    private void seVerificaQueSeActualizaElEstadoAPrestamoAcordado() {
        assertThat(jugador.getEstadoJugador().getId()).isEqualTo(ID_ESTADO_JUGADOR_PRESTAMO_ACORDADO);
    }

    private void seVerificaQueSeActualizaElEstadoATraspasoAcordado() {
        assertThat(jugador.getEstadoJugador().getId()).isEqualTo(ID_ESTADO_JUGADOR_TRASPASO_ACORDADO);
    }
}
