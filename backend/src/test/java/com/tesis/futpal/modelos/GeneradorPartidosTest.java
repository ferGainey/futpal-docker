package com.tesis.futpal.modelos;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.liga.GeneradorPartidos;
import com.tesis.futpal.modelos.liga.PartidoLigaBase;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class GeneradorPartidosTest {

    private Equipo equipo1;
    private Equipo equipo2;
    private Equipo equipo3;
    private Equipo equipo4;
    private List<Integer> idEquipos;
    private GeneradorPartidos generadorPartidos;
    private List<PartidoLigaBase> resultado;

    @Test
    public void dado2EquiposGeneraUnPartidoYLeAsignaFecha() {
        dado2Equipos();
        cuandoSeGeneraPartidos(idEquipos);
        seVerificaQueSeGeneraronPartidos(1);
        seVerificaQueQueElPartidoContieneALosDosEquipos();
        seVerificaQueElPartidoCorrespondeALaFecha1();
        seVerificaQueLosEquiposNoJueganContraSiMismos();
    }

    @Test
    public void dado3EquiposGenera3PartidosYTodosJueganContraTodos() {
        dado3Equipos();
        cuandoSeGeneraPartidos(idEquipos);
        seVerificaQueSeGeneraronPartidos(3);
        seVerificaQueLosPartidosContienenALos3Equipos();
        seVerificaQueQueLosPartidosCorrespondanALaFecha1y2y3EnLigaDeTresEquipos();
        seVerificaQueLosEquiposNoJueganContraSiMismos();
    }

    @Test
    public void dado4EquiposGenera6PartidosYTodosJueganContraTodosYLesAsignaFecha() {
        dado4Equipos();
        cuandoSeGeneraPartidos(idEquipos);
        seVerificaQueSeGeneraronPartidos(6);
        seVerificaQueLosPartidosContienenALos4Equipos(3);
        seVerificaQueQueLosPartidosCorrespondanALaFecha1y2y3();
        seVerificaQueLosEquiposNoJueganContraSiMismos();
    }

    @Test
    public void dado4EquiposYConfiguradoIdaYVueltaGenera12PartidosYTodosJuegan2VecesContraTodos() {
        dado4Equipos();
        cuandoSeGeneraPartidosYSeConfiguraLigaIdaYVuelta(idEquipos);
        seVerificaQueSeGeneraronPartidos(12);
        seVerificaQueLosPartidosContienenALos4Equipos(6);
        seVerificaQueQueLosPartidosCorrespondanALaFecha1y2y3y4y5y6();
        seVerificaQueLosEquiposNoJueganContraSiMismos();
    }

    private void seVerificaQueQueLosPartidosCorrespondanALaFecha1y2y3y4y5y6() {
        List<Integer> fechasEncontradas = new ArrayList<>();
        for (PartidoLigaBase partidoLigaBase : resultado) {
            fechasEncontradas.add(partidoLigaBase.getNumeroFecha());
        }
        Integer cantidadPartidosFecha1 = fechasEncontradas.stream().filter(nroFecha -> nroFecha == 1).collect(Collectors.toList()).size();
        Integer cantidadPartidosFecha2 = fechasEncontradas.stream().filter(nroFecha -> nroFecha == 2).collect(Collectors.toList()).size();
        Integer cantidadPartidosFecha3 = fechasEncontradas.stream().filter(nroFecha -> nroFecha == 3).collect(Collectors.toList()).size();
        Integer cantidadPartidosFecha4 = fechasEncontradas.stream().filter(nroFecha -> nroFecha == 4).collect(Collectors.toList()).size();
        Integer cantidadPartidosFecha5 = fechasEncontradas.stream().filter(nroFecha -> nroFecha == 5).collect(Collectors.toList()).size();
        Integer cantidadPartidosFecha6 = fechasEncontradas.stream().filter(nroFecha -> nroFecha == 6).collect(Collectors.toList()).size();
        assertThat(cantidadPartidosFecha1).isEqualTo(2);
        assertThat(cantidadPartidosFecha2).isEqualTo(2);
        assertThat(cantidadPartidosFecha3).isEqualTo(2);
        assertThat(cantidadPartidosFecha4).isEqualTo(2);
        assertThat(cantidadPartidosFecha5).isEqualTo(2);
        assertThat(cantidadPartidosFecha6).isEqualTo(2);
    }

    private void cuandoSeGeneraPartidosYSeConfiguraLigaIdaYVuelta(List<Integer> idEquipos) {
        generadorPartidos = new GeneradorPartidos();
        resultado = generadorPartidos.generar(idEquipos, true);
    }

    private void seVerificaQueQueLosPartidosCorrespondanALaFecha1y2y3() {
        List<Integer> fechasEncontradas = new ArrayList<>();
        for (PartidoLigaBase partidoLigaBase : resultado) {
            fechasEncontradas.add(partidoLigaBase.getNumeroFecha());
        }
        Integer cantidadPartidosFecha1 = (int) fechasEncontradas.stream().filter(nroFecha -> nroFecha == 1).count();
        Integer cantidadPartidosFecha2 = (int) fechasEncontradas.stream().filter(nroFecha -> nroFecha == 2).count();
        Integer cantidadPartidosFecha3 = (int) fechasEncontradas.stream().filter(nroFecha -> nroFecha == 3).count();
        assertThat(cantidadPartidosFecha1).isEqualTo(2);
        assertThat(cantidadPartidosFecha2).isEqualTo(2);
        assertThat(cantidadPartidosFecha3).isEqualTo(2);
    }

    private void seVerificaQueLosPartidosContienenALos4Equipos(Integer cantidadPartidosPorEquipo) {
        List<Integer> idEquiposEncontrados = new ArrayList<>();
        for (PartidoLigaBase partidoLigaBase : resultado) {
            idEquiposEncontrados.add(partidoLigaBase.getEquipoLocal().getId().intValue());
            idEquiposEncontrados.add(partidoLigaBase.getEquipoVisitante().getId().intValue());
        }
        Integer cantidadDePartidosEquipo1 = (int) idEquiposEncontrados.stream().filter(id -> id == equipo1.getId().intValue()).count();
        Integer cantidadDePartidosEquipo2 = (int) idEquiposEncontrados.stream().filter(id -> id == equipo2.getId().intValue()).count();
        Integer cantidadDePartidosEquipo3 = (int) idEquiposEncontrados.stream().filter(id -> id == equipo3.getId().intValue()).count();
        Integer cantidadDePartidosEquipo4 = (int) idEquiposEncontrados.stream().filter(id -> id == equipo4.getId().intValue()).count();
        assertThat(cantidadDePartidosEquipo1).isEqualTo(cantidadPartidosPorEquipo);
        assertThat(cantidadDePartidosEquipo2).isEqualTo(cantidadPartidosPorEquipo);
        assertThat(cantidadDePartidosEquipo3).isEqualTo(cantidadPartidosPorEquipo);
        assertThat(cantidadDePartidosEquipo4).isEqualTo(cantidadPartidosPorEquipo);
    }

    private void dado4Equipos() {
        equipo1 = new Equipo();
        equipo1.setId(1L);
        equipo2 = new Equipo();
        equipo2.setId(2L);
        equipo3 = new Equipo();
        equipo3.setId(3L);
        equipo4 = new Equipo();
        equipo4.setId(4L);
        idEquipos = Lists.list(equipo1.getId().intValue(), equipo2.getId().intValue(), equipo3.getId().intValue(), equipo4.getId().intValue());
    }

    private void seVerificaQueLosEquiposNoJueganContraSiMismos() {
        Integer cantidadDePartidosEntreSiMismosDeLosEquipos = resultado
                .stream()
                .filter(partidoBase -> partidoBase.getEquipoLocal() == partidoBase.getEquipoVisitante())
                .collect(Collectors.toList()).size();
        assertThat(cantidadDePartidosEntreSiMismosDeLosEquipos).isEqualTo(0);
    }

    private void seVerificaQueQueLosPartidosCorrespondanALaFecha1y2y3EnLigaDeTresEquipos() {
        List<Integer> fechasEncontradas = new ArrayList<>();
        for (PartidoLigaBase partidoLigaBase : resultado) {
            fechasEncontradas.add(partidoLigaBase.getNumeroFecha());
        }
        Integer cantidadPartidosFecha1 = (int) fechasEncontradas.stream().filter(nroFecha -> nroFecha == 1).count();
        Integer cantidadPartidosFecha2 = (int) fechasEncontradas.stream().filter(nroFecha -> nroFecha == 2).count();
        Integer cantidadPartidosFecha3 = (int) fechasEncontradas.stream().filter(nroFecha -> nroFecha == 3).count();
        assertThat(cantidadPartidosFecha1).isEqualTo(1);
        assertThat(cantidadPartidosFecha2).isEqualTo(1);
        assertThat(cantidadPartidosFecha3).isEqualTo(1);
    }

    private void seVerificaQueLosPartidosContienenALos3Equipos() {
        List<Integer> idEquiposEncontrados = new ArrayList<>();
        for (PartidoLigaBase partidoLigaBase : resultado) {
            idEquiposEncontrados.add(partidoLigaBase.getEquipoLocal().getId().intValue());
            idEquiposEncontrados.add(partidoLigaBase.getEquipoVisitante().getId().intValue());
        }
        Integer cantidadDePartidosEquipo1 = (int) idEquiposEncontrados.stream().filter(id -> id == equipo1.getId().intValue()).count();
        Integer cantidadDePartidosEquipo2 = (int) idEquiposEncontrados.stream().filter(id -> id == equipo2.getId().intValue()).count();
        Integer cantidadDePartidosEquipo3 = (int) idEquiposEncontrados.stream().filter(id -> id == equipo3.getId().intValue()).count();
        assertThat(cantidadDePartidosEquipo1).isEqualTo(2);
        assertThat(cantidadDePartidosEquipo2).isEqualTo(2);
        assertThat(cantidadDePartidosEquipo3).isEqualTo(2);
    }

    private void seVerificaQueSeGeneraronPartidos(Integer cantidadPartidos) {
        assertThat(resultado.size()).isEqualTo(cantidadPartidos);
    }

    private void dado3Equipos() {
        equipo1 = new Equipo();
        equipo1.setId(1L);
        equipo2 = new Equipo();
        equipo2.setId(2L);
        equipo3 = new Equipo();
        equipo3.setId(3L);
        idEquipos = Lists.list(equipo1.getId().intValue(), equipo2.getId().intValue(), equipo3.getId().intValue());
    }

    private void seVerificaQueElPartidoCorrespondeALaFecha1() {
        assertThat(resultado.get(0).getNumeroFecha()).isEqualTo(1);
    }

    private void seVerificaQueQueElPartidoContieneALosDosEquipos() {
        List<Integer> idEquiposEncontrados = Lists.list(resultado.get(0).getEquipoLocal().getId().intValue(), resultado.get(0).getEquipoVisitante().getId().intValue());
        assertThat(idEquiposEncontrados).contains(resultado.get(0).getEquipoLocal().getId().intValue(), resultado.get(0).getEquipoVisitante().getId().intValue());
    }

    private void cuandoSeGeneraPartidos(List<Integer> idEquipos) {
        generadorPartidos = new GeneradorPartidos();
        resultado = generadorPartidos.generar(idEquipos, false);
    }

    private void dado2Equipos() {
        equipo1 = new Equipo();
        equipo1.setId(1L);
        equipo2 = new Equipo();
        equipo2.setId(2L);
        idEquipos = Lists.list(equipo1.getId().intValue(), equipo2.getId().intValue());
    }

    //TODO: tema localia. No es importante de momento

}
