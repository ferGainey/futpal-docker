package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.servicios.ServicioPlantel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PrestamoJugadorTest {

    private static final Long ID_EQUIPO_ORIGEN = 3L;
    private static final Long ID_EQUIPO_DESTINO = 5L;
    private static final Integer ID_MOVIMIENTO_JUGADOR = 1;
    private MovimientoJugador movimientoJugador;
    private PrestamoJugador prestamoJugadorGenerado;
    private Jugador jugador;
    private Jugador jugadorQueEstaAPrestamoEnOtroEquipo;

    @Mock
    private ServicioPlantel servicioPlantel;

    @Test
    public void seCreaPrestamoQueNoEsDeFinAPartirDeMovimiento() {
        dadoUnMovimientoDeJugadorConUnPrestamoDeUnJugadorDesdeSuEquipoOrigenAOtro();
        dadoQueSePuedenObtenerLosJugadoresDeLosQueUnEquipoEsDuenioDelPasePara(ID_EQUIPO_ORIGEN);

        cuandoSeCreaUnPrestamoAPartirDelMovimiento();

        seVerificaQueSeCreoElPrestamoDeInicioCorrectamente();
    }

    @Test
    public void seCreaPrestamoQueEsDeFinAPartirDeMovimiento() {
        dadoUnMovimientoDeJugadorConUnPrestamoDeUnJugadorDesdeOtroEquipoASuEquipoOrigen();
        dadoQueSePuedenObtenerLosJugadoresDeLosQueUnEquipoEsDuenioDelPasePara(ID_EQUIPO_DESTINO);

        cuandoSeCreaUnPrestamoAPartirDelMovimiento();

        seVerificaQueSeCreoElPrestamoDeFinCorrectamente();
    }

    private void dadoUnMovimientoDeJugadorConUnPrestamoDeUnJugadorDesdeSuEquipoOrigenAOtro() {
        movimientoJugador = new MovimientoJugador();
        movimientoJugador.setId(ID_MOVIMIENTO_JUGADOR);

        Equipo equipoOrigen = new Equipo();
        equipoOrigen.setId(ID_EQUIPO_ORIGEN);
        movimientoJugador.setEquipoOrigen(equipoOrigen);

        Equipo equipoDestino = new Equipo();
        equipoDestino.setId(ID_EQUIPO_DESTINO);
        movimientoJugador.setEquipoDestino(equipoDestino);

        jugador = new Jugador();
        jugador.setId(4L);

        movimientoJugador.setJugador(jugador);
    }

    private void dadoUnMovimientoDeJugadorConUnPrestamoDeUnJugadorDesdeOtroEquipoASuEquipoOrigen() {
        movimientoJugador = new MovimientoJugador();
        movimientoJugador.setId(ID_MOVIMIENTO_JUGADOR);

        Equipo equipoOrigen = new Equipo();
        equipoOrigen.setId(ID_EQUIPO_ORIGEN);

        Equipo equipoDestino = new Equipo();
        equipoDestino.setId(ID_EQUIPO_DESTINO);

        movimientoJugador.setEquipoOrigen(equipoDestino);
        movimientoJugador.setEquipoDestino(equipoOrigen);

        jugador = new Jugador();
        jugador.setId(4L);

        jugadorQueEstaAPrestamoEnOtroEquipo = new Jugador();
        jugadorQueEstaAPrestamoEnOtroEquipo.setId(7L);
        movimientoJugador.setJugador(jugadorQueEstaAPrestamoEnOtroEquipo);
    }

    private void dadoQueSePuedenObtenerLosJugadoresDeLosQueUnEquipoEsDuenioDelPasePara(Long idEquipo) {
        when(servicioPlantel.obtenerJugadoresDuenioDePase(idEquipo.intValue())).thenReturn(Collections.singletonList(jugador));
    }

    private void cuandoSeCreaUnPrestamoAPartirDelMovimiento() {
        prestamoJugadorGenerado = PrestamoJugador.crearAPartirDe(movimientoJugador, servicioPlantel);
    }

    private void seVerificaQueSeCreoElPrestamoDeFinCorrectamente() {
        assertThat(prestamoJugadorGenerado.getMovimientoJugador().getId()).isEqualTo(ID_MOVIMIENTO_JUGADOR);
        assertThat(prestamoJugadorGenerado.isEsFinDePrestamo()).isTrue();
    }

    private void seVerificaQueSeCreoElPrestamoDeInicioCorrectamente() {
        assertThat(prestamoJugadorGenerado.getMovimientoJugador().getId()).isEqualTo(ID_MOVIMIENTO_JUGADOR);
        assertThat(prestamoJugadorGenerado.isEsFinDePrestamo()).isFalse();
    }

}