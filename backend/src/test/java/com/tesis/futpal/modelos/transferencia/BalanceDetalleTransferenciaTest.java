package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class BalanceDetalleTransferenciaTest {

    private Balance balance;
    private BalanceDetalleTransferencia resultado;

    @Test
    public void seCreaBalanceParaDetalleDeTransferenciaCorrectamente() {
        dadoUnBalance();
        cuandoSeCreaElBalanceParaElDetalleDeTransferencia();
        seVerificaQueSeCreaCorrectamente();
    }

    private void seVerificaQueSeCreaCorrectamente() {
        assertThat(resultado.getDetalle()).isEqualTo(balance.getDetalle());
        assertThat(resultado.getEquipo().getNombreEquipo()).isEqualTo(balance.getEquipo().getNombreEquipo());
        assertThat(resultado.getEquipo().getId()).isEqualTo(balance.getEquipo().getId());
        assertThat(resultado.getEquipo().getIdUsuario()).isEqualTo(balance.getEquipo().getIdUsuario());
        assertThat(resultado.getId()).isEqualTo(balance.getId());
        assertThat(resultado.getMonto()).isEqualTo(balance.getMonto());
        assertThat(resultado.getNumeroTemporada()).isEqualTo(balance.getNumeroTemporada());
        assertThat(resultado.getPeriodo().getId()).isEqualTo(balance.getPeriodo().getId());
    }

    private void cuandoSeCreaElBalanceParaElDetalleDeTransferencia() {
        resultado = BalanceDetalleTransferencia.crearAPartirDe(balance);
    }

    private void dadoUnBalance() {
        balance = new Balance();

        balance.setId(1);

        Equipo equipo = new Equipo();
        equipo.setId(1L);
        equipo.setNombreEquipo("Equipo FC");
        equipo.setIdUsuario(1L);
        balance.setEquipo(equipo);

        balance.setDetalle("Un detalle");

        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(1);
        balance.setPeriodo(periodoTemporada);

        balance.setNumeroTemporada(3);

        Transferencia transferencia = new Transferencia();
        transferencia.setId(1);
        balance.setTransferencia(transferencia);

        balance.setMonto(50000);
    }
}
