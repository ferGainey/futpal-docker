package com.tesis.futpal.modelos.transferencia;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MovimientoJugadorDetalleTransferenciaTest {

    private MovimientoJugador movimientoJugador;
    private MovimientoJugadorDetalleTransferencia resultado;

    @Test
    public void seCreaMovimientoDeJugadorParaDetalleAPartirDeUnMovimientoDeJugador() {
        dadoUnMovimientoDeJugador();
        cuandoSeCreaElMovimientoParaElDetalle();
        seVerificaQueSeCreaCorrectamente();
    }

    private void seVerificaQueSeCreaCorrectamente() {
        assertThat(resultado.getId()).isEqualTo(movimientoJugador.getId());
        assertThat(resultado.getJugador().getId()).isEqualTo(movimientoJugador.getJugador().getId());
        assertThat(resultado.getJugador().getNombre()).isEqualTo(movimientoJugador.getJugador().getNombre());
        assertThat(resultado.getNumeroTemporada()).isEqualTo(movimientoJugador.getNumeroTemporada());
        assertThat(resultado.getPeriodo().getId()).isEqualTo(movimientoJugador.getPeriodo().getId());
        assertThat(resultado.getEquipoOrigen().getId()).isEqualTo(movimientoJugador.getEquipoOrigen().getId());
        assertThat(resultado.getEquipoOrigen().getNombreEquipo()).isEqualTo(movimientoJugador.getEquipoOrigen().getNombreEquipo());
        assertThat(resultado.getEquipoOrigen().getIdUsuario()).isEqualTo(movimientoJugador.getEquipoOrigen().getIdUsuario());
        assertThat(resultado.getEquipoDestino().getId()).isEqualTo(movimientoJugador.getEquipoDestino().getId());
        assertThat(resultado.getEquipoDestino().getNombreEquipo()).isEqualTo(movimientoJugador.getEquipoDestino().getNombreEquipo());
        assertThat(resultado.getEquipoDestino().getIdUsuario()).isEqualTo(movimientoJugador.getEquipoDestino().getIdUsuario());
        assertThat(resultado.getPrestamoJugador().getId()).isEqualTo(movimientoJugador.getPrestamoJugador().getId());
    }

    private void cuandoSeCreaElMovimientoParaElDetalle() {
        resultado = MovimientoJugadorDetalleTransferencia.crearAPartirDe(movimientoJugador);
    }

    private void dadoUnMovimientoDeJugador() {
        movimientoJugador = new MovimientoJugador();

        movimientoJugador.setId(1);

        Transferencia transferencia = new Transferencia();
        transferencia.setId(1);
        movimientoJugador.setTransferencia(transferencia);

        Equipo equipoOrigen = new Equipo();
        equipoOrigen.setId(1L);
        equipoOrigen.setNombreEquipo("Origen FC");
        equipoOrigen.setIdUsuario(1L);
        movimientoJugador.setEquipoOrigen(equipoOrigen);

        Equipo equipoDestino = new Equipo();
        equipoDestino.setId(2L);
        equipoDestino.setNombreEquipo("Destino FC");
        equipoDestino.setIdUsuario(2L);
        movimientoJugador.setEquipoDestino(equipoDestino);

        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(1);
        movimientoJugador.setPeriodo(periodoTemporada);

        movimientoJugador.setNumeroTemporada(3);

        Jugador jugador = new Jugador();
        jugador.setNombre("Pedro");
        jugador.setId(1L);
        movimientoJugador.setJugador(jugador);

        PrestamoJugador prestamoJugador = new PrestamoJugador();
        prestamoJugador.setId(1);
        movimientoJugador.setPrestamoJugador(prestamoJugador);
    }
}
