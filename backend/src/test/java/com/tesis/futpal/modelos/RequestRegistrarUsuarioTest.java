package com.tesis.futpal.modelos;

import com.tesis.futpal.comunicacion.requests.RequestRegistrarUsuario;
import com.tesis.futpal.modelos.usuario.Usuario;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class RequestRegistrarUsuarioTest {

    @Mock
    private PasswordEncoder encoder;
    @InjectMocks
    private RequestRegistrarUsuario request;
    private boolean resultadoVerificacion;
    private Usuario usuarioCreado;

    @Test
    public void siElRequestTieneTodosLosCamposNulosEntoncesElVerificarQueTodosLosCamposSeanNoNulosDevuelveFalse() {
        dadoUnRequestConTodosLosCamposNulos();
        alVerificarQueTodosLosCamposSeanNoNulos();
        seDevuelveQueEsFalso();
    }

    @Test
    public void siElRequestTieneAlgunCampoNuloEntoncesElVerificarQueTodosLosCamposSeanNoNulosDevuelveFalse() {
        dadoUnRequestConAlgunCampoNulo();
        alVerificarQueTodosLosCamposSeanNoNulos();
        seDevuelveQueEsFalso();
    }

    @Test
    public void siElRequestNoTieneNingunCampoNuloEntoncesElVerificarQueTodosLosCamposSeanNoNulosDevuelveTrue() {
        dadoUnRequestSinNingunCampoNulo();
        alVerificarQueTodosLosCamposSeanNoNulos();
        seDevuelveQueEsVerdadero();
    }

    @Test
    public void siElRequestTieneTodosLosCamposVaciosEntoncesElVerificarQueTodosLosCamposSeanNoVaciosDevuelveFalse() {
        dadoUnRequestConTodosLosCamposVacios();
        alVerificarQueTodosLosCamposSeanNoVacios();
        seDevuelveQueEsFalso();
    }

    @Test
    public void siElRequestTieneAlgunCampoVacioEntoncesElVerificarQueTodosLosCamposSeanNoVaciosDevuelveFalse() {
        dadoUnRequestConAlgunCampoVacio();
        alVerificarQueTodosLosCamposSeanNoVacios();
        seDevuelveQueEsFalso();
    }

    @Test
    public void siElRequestNoTieneNingunCampoVacioEntoncesElVerificarQueTodosLosCamposSeanNoVaciosDevuelveTrue() {
        dadoUnRequestSinNingunCampoVacio();
        alVerificarQueTodosLosCamposSeanNoVacios();
        seDevuelveQueEsVerdadero();
    }

    @Test
    public void siElRequestTieneSoloEspaciosYSeLlamaAEliminarEspaciosDelPrincipioYFinalSeTomaComoVacio() {
        dadoUnRequestDondeLosCamposTenganSoloEspacios();
        alEliminarEspaciosDelPrincipioYDelFinal();
        alVerificarQueTodosLosCamposSeanNoVacios();
        seDevuelveQueEsFalso();
    }

    @Test
    public void seEliminanLosEspaciosDelPrincipioYDelFinalDeLosCamposDelRequest() {
        dadoUnRequestDondeAlgunosCamposTenganEspaciosAlPrincipioYOFinal();
        alEliminarEspaciosDelPrincipioYDelFinal();
        entonceslosCamposQuedanUnicamenteConElContenidoSinEspaciosAlPrincipioYFinal();
    }


    private void entonceslosCamposQuedanUnicamenteConElContenidoSinEspaciosAlPrincipioYFinal() {
        assertThat(request.getAlias()).isEqualTo("alias");
        assertThat(request.getNombre()).isEqualTo("nombre segundoNombre");
        assertThat(request.getApellido()).isEqualTo("apellido");
        assertThat(request.getMail()).isEqualTo("mail");
        assertThat(request.getContrasenia()).isEqualTo("contrasenia");
    }

    private void alEliminarEspaciosDelPrincipioYDelFinal() {
        request.eliminarEspaciosDelPrincipioYDelFinal();
    }

    private void dadoUnRequestDondeAlgunosCamposTenganEspaciosAlPrincipioYOFinal() {
        request = new RequestRegistrarUsuario();
        request.setAlias("alias");
        request.setNombre(" nombre segundoNombre ");
        request.setApellido("apellido   ");
        request.setMail(" mail");
        request.setContrasenia("  contrasenia       ");
    }

    private void dadoUnRequestDondeLosCamposTenganSoloEspacios() {
        request = new RequestRegistrarUsuario();
        request.setAlias(" ");
        request.setNombre("  ");
        request.setApellido("   ");
        request.setMail(" ");
        request.setContrasenia("         ");
    }

    private void dadoUnRequestSinNingunCampoVacio() {
        request = new RequestRegistrarUsuario();
        request.setAlias("alias");
        request.setNombre("nombre");
        request.setApellido("apellido");
        request.setMail("mail@mail.com");
        request.setContrasenia("12345678");
    }

    private void dadoUnRequestConAlgunCampoVacio() {
        request = new RequestRegistrarUsuario();
        request.setAlias("");
        request.setNombre("nombre");
        request.setApellido("apellido");
        request.setMail("");
        request.setContrasenia("12345678");
    }

    private void alVerificarQueTodosLosCamposSeanNoVacios() {
        resultadoVerificacion = request.verificarQueTodosLosCamposSeanNoVacios();
    }

    private void dadoUnRequestConTodosLosCamposVacios() {
        request = new RequestRegistrarUsuario();
        request.setAlias("");
        request.setNombre("");
        request.setApellido("");
        request.setMail("");
        request.setContrasenia("");
    }

    private void seDevuelveQueEsVerdadero() {
        assertThat(resultadoVerificacion).isTrue();
    }

    private void dadoUnRequestSinNingunCampoNulo() {
        request = new RequestRegistrarUsuario();
        request.setAlias("alias");
        request.setNombre("nombre");
        request.setApellido("apellido");
        request.setMail("mail@mail.com");
        request.setContrasenia("12345678");
    }

    private void dadoUnRequestConAlgunCampoNulo() {
        request = new RequestRegistrarUsuario();
        request.setAlias("alias");
        request.setNombre("nombre");
        request.setApellido("apellido");
    }

    private void seDevuelveQueEsFalso() {
        assertThat(resultadoVerificacion).isFalse();
    }

    private void alVerificarQueTodosLosCamposSeanNoNulos() {
        resultadoVerificacion = request.verificarQueTodosLosCamposSeanNoNulos();
    }

    private void dadoUnRequestConTodosLosCamposNulos() {
        request = new RequestRegistrarUsuario();
    }
}
