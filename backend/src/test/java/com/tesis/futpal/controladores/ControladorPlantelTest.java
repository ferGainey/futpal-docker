package com.tesis.futpal.controladores;

import com.tesis.futpal.comunicacion.requests.*;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.plantel.JugadorFiltrado;
import com.tesis.futpal.modelos.plantel.JugadorPrestado;
import com.tesis.futpal.servicios.ServicioPlantel;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ControladorPlantelTest {

    private static final Long EQUIPO_ID = 1L;
    private static final Long ID_JUGADOR = 1L;
    @Mock
    private ServicioPlantel servicioPlantel;

    @InjectMocks
    private ControladorPlantel controladorPlantel;
    private ResponseEntity respuesta;

    @Test
    public void alObtenerDatosDeUnEquipoAPartirDeUnUsuarioDeManeraCorrectaDevuelve200ConLosDatosEsperados() {
        dadoQueElServicioAgregarJugadorSeEjecutaExitosamente();
        cuandoSeAgregaUnJugador();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void seObtienenLosJugadoresDeUnPlantel() {
        dadoQueElServicioDeObtenerJugadoresSeEjecutaExitosamente();
        cuandoSeConsultanLosJugadoresDeUnEquipo();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void seBorraUnJugadorDeUnPlantel() {
        dadoQueElServicioDeBorrarUnJugadorSeEjecutaExitosamente();
        cuandoSeBorraUnJugador();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void seEditaUnJugadorDeUnPlantel() {
        dadoQueElServicioDeEdicionDeUnJugadorSeEjecutaExitosamente();
        cuandoSeEditaUnJugador();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void seObtienenLosDatosDeUnJugador() {
        dadoQueElServicioDeObtenerDatosJugadorSeEjecutaExitosamente();
        cuandoSeConsultanLosDatosDeUnJugador();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void seObtienenLosJugadoresPrestados() {
        dadoQueElServicioDeObtenerJugadoresPrestadosSeEjecutaExitosamente();
        cuandoSePidenLosJugadoresPrestados();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void seObtienenLosJugadoresDeLosCualesSeEsDuenioDelPase() {
        dadoQueElServicioDeObtenerJugadoresDuenioDePaseSeEjecutaExitosamente();
        cuandoSeConsultanLosJugadoresDeLosQueSeEsDuenioDelPase();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void seObtienenLosJugadoresFiltrados() {
        dadoQueElServicioDeObtenerJugadoresFiltradosSeEjecutaExitosamente();
        cuandoSeConsultanLosJugadoresFiltrados();
        seVerificaQueLaRespuestaSeaUn200();
    }

    private void dadoQueElServicioDeObtenerJugadoresFiltradosSeEjecutaExitosamente() {
        when(servicioPlantel.obtenerJugadoresFiltrados(anyInt(),anyString())).thenReturn(Lists.list(new JugadorFiltrado()));
    }

    private void dadoQueElServicioDeObtenerJugadoresDuenioDePaseSeEjecutaExitosamente() {
        when(servicioPlantel.obtenerJugadoresDuenioDePase(anyInt())).thenReturn(Lists.emptyList());
    }

    private void cuandoSeConsultanLosJugadoresFiltrados() {
        respuesta = controladorPlantel.obtenerJugadoresFiltrados(3, "nombre");
    }

    private void cuandoSeConsultanLosJugadoresDeLosQueSeEsDuenioDelPase() {
        respuesta = controladorPlantel.obtenerJugadoresDuenioDePase(EQUIPO_ID.intValue());
    }

    private void cuandoSePidenLosJugadoresPrestados() {
        respuesta = controladorPlantel.obtenerJugadoresPrestados(2);
    }

    private void dadoQueElServicioDeObtenerJugadoresPrestadosSeEjecutaExitosamente() {
        when(servicioPlantel.obtenerJugadoresPrestados(any(Integer.class))).thenReturn(Lists.list(new JugadorPrestado()));
    }

    private void cuandoSeConsultanLosDatosDeUnJugador() {
        RequestDatosJugador requestDatosJugador = new RequestDatosJugador();
        requestDatosJugador.setJugadorId(ID_JUGADOR);
        respuesta = controladorPlantel.obtenerDatosJugador(requestDatosJugador);
    }

    private void dadoQueElServicioDeObtenerDatosJugadorSeEjecutaExitosamente() {
        when(servicioPlantel.obtenerDatosJugador(anyLong())).thenReturn(new Jugador());
    }

    private void cuandoSeEditaUnJugador() {
        RequestEdicionJugador requestEditarJugador = new RequestEdicionJugador();
        respuesta = controladorPlantel.editarJugador(requestEditarJugador);
    }

    private void dadoQueElServicioDeEdicionDeUnJugadorSeEjecutaExitosamente() {
        doNothing().when(servicioPlantel).editarJugador(any(RequestEdicionJugador.class));
    }

    private void cuandoSeBorraUnJugador() {
        RequestBorrarJugador requestBorrarJugador = new RequestBorrarJugador();
        requestBorrarJugador.setJugadorId(ID_JUGADOR);
        respuesta = controladorPlantel.borrarJugador(requestBorrarJugador);
    }

    private void dadoQueElServicioDeBorrarUnJugadorSeEjecutaExitosamente() {
        doNothing().when(servicioPlantel).borrarJugador(ID_JUGADOR);
    }

    private void cuandoSeConsultanLosJugadoresDeUnEquipo() {
        RequestObtenerJugadoresEquipo requestObtenerJugadoresEquipo = new RequestObtenerJugadoresEquipo();
        requestObtenerJugadoresEquipo.setEquipoId(EQUIPO_ID);
        respuesta = controladorPlantel.obtenerJugadores(requestObtenerJugadoresEquipo);
    }

    private void dadoQueElServicioDeObtenerJugadoresSeEjecutaExitosamente() {
        when(servicioPlantel.obtenerJugadoresEquipo(any(RequestObtenerJugadoresEquipo.class))).thenReturn(Lists.emptyList());
    }

    private void seVerificaQueLaRespuestaSeaUn200() {
        assertThat(respuesta.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void cuandoSeAgregaUnJugador() {
        RequestAgregarJugador requestAgregarJugador = new RequestAgregarJugador();
        respuesta = controladorPlantel.agregarJugador(requestAgregarJugador);
    }

    private void dadoQueElServicioAgregarJugadorSeEjecutaExitosamente() {
        Mockito.doNothing().when(servicioPlantel).agregarJugador(any(RequestAgregarJugador.class));
    }
}
