package com.tesis.futpal.controladores;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.excepciones.UsuarioInexistenteException;
import com.tesis.futpal.comunicacion.requests.RequestEditarDatosEquipo;
import com.tesis.futpal.comunicacion.responses.ResponseDatosEquipo;
import com.tesis.futpal.comunicacion.responses.ResponseEquipoActualizado;
import com.tesis.futpal.servicios.ServicioEdicionEquipo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ControladorEdicionEquipoTest {

    private static final Long ID_EQUIPO_BUSCADO = 2L;
    private static final String NOMBRE_EQUIPO_BUSCADO = "Gainey FC";
    private static final String MENSAJE_USUARIO_INEXISTENTE = "El id ingresado no está vinculado a ningún usuario.";
    private static final String NUEVO_NOMBRE_EQUIPO = "Afalp";
    private Long ID_USUARIO = 1L;
    @Mock
    private ServicioEdicionEquipo servicioEdicionEquipo;
    @InjectMocks
    private ControladorEdicionEquipo controladorEdicionEquipo;
    private ResponseEntity resultado;

    @Test
    public void alObtenerDatosDeUnEquipoAPartirDeUnUsuarioDeManeraCorrectaDevuelve200ConLosDatosEsperados() throws UsuarioInexistenteException {
        dadoQueElServicioEdicionEquipoObtieneLosDatosDelEquipoExitosamenteConElId(ID_USUARIO);
        cuandoObtengoLosDatosDelEquipoAPartirDelUsuario();
        verificoQueLaRespuestaSeaUn200QueContengaLosDatosDelEquipo();
    }

    @Test
    public void alObtenerDatosDeUnEquipoAPartirDeUnUsuarioDeManeraCorrectaDevuelve200() throws UsuarioInexistenteException {
        dadoQueElServicioEdicionEquipoObtieneLosDatosDelEquipoExitosamenteConElId(ID_USUARIO);
        cuandoObtengoLosDatosDelEquipoAPartirDelUsuario();
        verificoQueLaRespuestaSeaUn200QueContengaLosDatosDelEquipo();
    }

    @Test
    public void intentarObtenerDatosDeUnEquipoDeUnUsuarioInexistenteDevuelve409() throws UsuarioInexistenteException {
        dadoQueElServicioEdicionEquipoDevuelveUnaExcepcionPorUsuarioInexistenteAlObtenerDatosEquipo(ID_USUARIO);
        cuandoObtengoLosDatosDelEquipoAPartirDelUsuario();
        verificoQueLaRespuestaSeaUn409ConMensaje(MENSAJE_USUARIO_INEXISTENTE);
    }

    @Test
    public void alActualizarLosDatosDeUnEquipoDeManeraCorrectaDevuelve200ConLosDatosEsperados() throws UsuarioInexistenteException {
        dadoQueElServicioEdicionEquipoActualizaLosDatosDelEquipoExitosamenteConElId(ID_USUARIO, NUEVO_NOMBRE_EQUIPO);
        cuandoActualizoLosDatosDelEquipoAPartirDelUsuario();
        verificoQueLaRespuestaSeaUn200QueContengaElNuevoNombreDelEquipo();
    }

    @Test
    public void intentarActualizarDatosDeUnEquipoDeUnUsuarioInexistenteDevuelve409() throws UsuarioInexistenteException {
        dadoQueElServicioEdicionEquipoDevuelveUnaExcepcionPorUsuarioInexistenteAlEditarDatosEquipo(ID_USUARIO, NUEVO_NOMBRE_EQUIPO);
        cuandoActualizoLosDatosDelEquipoAPartirDelUsuario();
        verificoQueLaRespuestaSeaUn409ConMensaje(MENSAJE_USUARIO_INEXISTENTE);
    }

    private void dadoQueElServicioEdicionEquipoDevuelveUnaExcepcionPorUsuarioInexistenteAlEditarDatosEquipo(Long idUsuario, String nuevoNombreEquipo) throws UsuarioInexistenteException {
        when(servicioEdicionEquipo.actualizarEquipo(any(RequestEditarDatosEquipo.class))).thenThrow(new UsuarioInexistenteException());
    }

    private void verificoQueLaRespuestaSeaUn200QueContengaElNuevoNombreDelEquipo() {
        assertThat(resultado.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(((ResponseEquipoActualizado)resultado.getBody()).getNombreEquipoActualizado()).isEqualTo(NUEVO_NOMBRE_EQUIPO);
    }

    private void cuandoActualizoLosDatosDelEquipoAPartirDelUsuario() {
        RequestEditarDatosEquipo requestEditarDatosEquipo = new RequestEditarDatosEquipo();
        requestEditarDatosEquipo.setIdUsuario(ID_USUARIO);
        requestEditarDatosEquipo.setNuevoNombreEquipo(NUEVO_NOMBRE_EQUIPO);
        resultado = controladorEdicionEquipo.actualizarDatosEquipo(requestEditarDatosEquipo);
    }

    private void dadoQueElServicioEdicionEquipoActualizaLosDatosDelEquipoExitosamenteConElId(Long idUsuario, String nuevoNombreEquipo) throws UsuarioInexistenteException {
        Equipo equipo = new Equipo();
        equipo.setId(2L);
        equipo.setNombreEquipo(nuevoNombreEquipo);
        ResponseEquipoActualizado responseEquipoActualizado = new ResponseEquipoActualizado(equipo);
        when(servicioEdicionEquipo.actualizarEquipo(any(RequestEditarDatosEquipo.class))).thenReturn(responseEquipoActualizado);
    }

    private void verificoQueLaRespuestaSeaUn409ConMensaje(String mensaje) {
        assertThat(resultado.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        assertThat(resultado.getBody()).isEqualTo(mensaje);
    }

    private void dadoQueElServicioEdicionEquipoDevuelveUnaExcepcionPorUsuarioInexistenteAlObtenerDatosEquipo(Long idUsuario) throws UsuarioInexistenteException {
        when(servicioEdicionEquipo.obtenerDatosEquipo(idUsuario)).thenThrow(new UsuarioInexistenteException());
    }

    private void verificoQueLaRespuestaSeaUn200QueContengaLosDatosDelEquipo() {
        assertThat(resultado.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(((ResponseDatosEquipo)resultado.getBody()).getIdEquipo()).isEqualTo(ID_EQUIPO_BUSCADO);
        assertThat(((ResponseDatosEquipo)resultado.getBody()).getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_BUSCADO);
    }

    private void cuandoObtengoLosDatosDelEquipoAPartirDelUsuario() {
        resultado = controladorEdicionEquipo.obtenerDatosEquipo(ID_USUARIO);
    }

    private void dadoQueElServicioEdicionEquipoObtieneLosDatosDelEquipoExitosamenteConElId(Long id) throws UsuarioInexistenteException {
        Equipo equipoBuscado = new Equipo();
        equipoBuscado.setId(ID_EQUIPO_BUSCADO);
        equipoBuscado.setNombreEquipo(NOMBRE_EQUIPO_BUSCADO);
        ResponseDatosEquipo responseDatosEquipo = new ResponseDatosEquipo(equipoBuscado);
        when(servicioEdicionEquipo.obtenerDatosEquipo(id)).thenReturn(responseDatosEquipo);
    }
}
