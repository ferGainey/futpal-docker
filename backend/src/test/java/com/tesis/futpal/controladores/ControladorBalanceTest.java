package com.tesis.futpal.controladores;


import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.balance.BalanceSalario;
import com.tesis.futpal.modelos.balance.DetalleBalance;
import com.tesis.futpal.excepciones.LigaInexistenteException;
import com.tesis.futpal.comunicacion.requests.balance.RequestCargaBalanceUnEquipo;
import com.tesis.futpal.servicios.ServicioBalance;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ControladorBalanceTest {

    private static final Integer NUMERO_TEMPORADA_1 = 1;
    private static final Integer EQUIPO_ID = 1;
    private RequestCargaBalanceUnEquipo requestCargaBalanceUnEquipo;
    @Mock
    private ServicioBalance servicioBalance;

    @InjectMocks
    private ControladorBalance controladorBalance;
    private ResponseEntity resultado;
    private List<RequestCargaBalanceUnEquipo> cargasParaUnaLiga;
    private ResponseEntity<List<DetalleBalance>> resultadoObtenerTodosLosDetallesDeBalance;
    private ResponseEntity<DetalleBalance> respuestaDetalleBalanceUnEquipo;
    private ResponseEntity<List<Balance>> respuestaBalancesDeUnEquipoEnUnaTemporada;
    private ResponseEntity<List<BalanceSalario>> respuestaSalariosDeUnaLiga;

    @Test
    public void seCargaBalanceParaUnEquipoExitosamente() {
        dadoUnRequestParaCargaDeBalanceDeUnEquipoValido();
        dadoQueElServicioBalanceCargaElBalanceDeUnEquipoExitosamente();

        cuandoSeCargaUnBalance();

        seVerificaQueLaRespuestaEsUn200();
    }

    @Test
    public void seCargaBalanceParaUnaLigaExitosamente() {
        dadoUnRequestParaCargaDeBalanceDeUnaLigaValido();
        dadoQueElServicioBalanceCargaElBalanceDeUnaLigaExitosamente();

        cuandoSeCargaUnBalanceParaUnaLiga();

        seVerificaQueLaRespuestaEsUn200();
    }

    @Test
    public void seObtienenLosDetalleDeBalanceDeTodosLosEquipos() {
        dadoQueElServicioBalanceDevuelveLosDetalleDeBalanceDeTodosLosEquiposExitosamente();

        cuandoSeObtienenLosDetalleDeBalanceDeTodosLosEquipos();

        seVerificaQueLaRespuestaEsUn200QueContieneUnaListaDeDetallesDeBalances();
    }

    @Test
    public void seObtienenLosDetalleDeBalanceDeUnEquipo() {
        dadoQueElServicioBalanceDevuelveLosDetalleDeBalanceDeUnEquiposExitosamente();

        cuandoSeObtienenLosDetalleDeBalanceDeUnEquipos();

        seVerificaQueLaRespuestaEsUn200QueContieneUnDetalleDeBalances();
    }

    @Test
    public void seObtienenLosBalancesDeUnEquipoParaUnaTemporada() {
        dadoQueElServicioBalanceDevuelveLosBalancesDeUnEquipoParaUnaTemporadaExitosamente();

        cuandoSeObtienenLosBalancesDeUnEquiposParaUnaTemporada();

        seVerificaQueLaRespuestaEsUn200QueContieneUnaListaDeBalances();
    }

    @Test
    public void seObtienenLosSalariosParaUnaLiga() throws LigaInexistenteException {
        dadoQueElServicioBalanceDevuelveLosBalancesConLosSalariosDeUnaLigaExitosamente();

        cuandoSeObtienenLosSalariosDeUnaLiga();

        seVerificaQueLaRespuestaEsUn200QueContieneUnaListaDeBalancesConLosSalarios();
    }

    private void dadoQueElServicioBalanceDevuelveLosBalancesConLosSalariosDeUnaLigaExitosamente() throws LigaInexistenteException {
        when(servicioBalance.obtenerSalariosDeLaLiga(anyInt())).thenReturn(Lists.list(new BalanceSalario()));
    }

    private void dadoQueElServicioBalanceCargaElBalanceDeUnEquipoExitosamente() {
        doNothing().when(servicioBalance).cargarBalance(any(Balance.class));
    }

    private void dadoUnRequestParaCargaDeBalanceDeUnEquipoValido() {
        requestCargaBalanceUnEquipo = new RequestCargaBalanceUnEquipo();
    }

    private void dadoQueElServicioBalanceDevuelveLosBalancesDeUnEquipoParaUnaTemporadaExitosamente() {
        Balance balance = new Balance();
        when(servicioBalance.obtenerBalancesDeUnEquipoEnTemporada(NUMERO_TEMPORADA_1, EQUIPO_ID)).thenReturn(Collections.singletonList(balance));
    }

    private void dadoQueElServicioBalanceDevuelveLosDetalleDeBalanceDeUnEquiposExitosamente() {
        when(servicioBalance.obtenerDetalleBalanceDeUnEquipo(NUMERO_TEMPORADA_1, EQUIPO_ID)).thenReturn(new DetalleBalance());
    }

    private void dadoQueElServicioBalanceDevuelveLosDetalleDeBalanceDeTodosLosEquiposExitosamente() {
        DetalleBalance detalleBalance = new DetalleBalance();
        when(servicioBalance.obtenerDetalleBalanceDeTodosLosEquipos(NUMERO_TEMPORADA_1)).thenReturn(Collections.singletonList(detalleBalance));
    }

    private void dadoQueElServicioBalanceCargaElBalanceDeUnaLigaExitosamente() {
        doNothing().when(servicioBalance).cargarBalanceParaUnaLiga(anyList());
    }

    private void dadoUnRequestParaCargaDeBalanceDeUnaLigaValido() {
        cargasParaUnaLiga = Collections.singletonList(new RequestCargaBalanceUnEquipo());
    }

    private void cuandoSeObtienenLosSalariosDeUnaLiga() throws LigaInexistenteException {
        respuestaSalariosDeUnaLiga = controladorBalance.obtenerSalariosLiga(2);
    }

    private void cuandoSeObtienenLosBalancesDeUnEquiposParaUnaTemporada() {
        respuestaBalancesDeUnEquipoEnUnaTemporada = controladorBalance.obtenerBalancesDeUnEquipoEnTemporada(NUMERO_TEMPORADA_1, EQUIPO_ID);
    }

    private void cuandoSeObtienenLosDetalleDeBalanceDeUnEquipos() {
        respuestaDetalleBalanceUnEquipo = controladorBalance.obtenerDetalleBalanceDeUnEquipo(NUMERO_TEMPORADA_1, EQUIPO_ID);
    }

    private void cuandoSeObtienenLosDetalleDeBalanceDeTodosLosEquipos() {
        resultadoObtenerTodosLosDetallesDeBalance = controladorBalance.obtenerDetalleBalanceDeTodosLosEquipos(NUMERO_TEMPORADA_1);
    }

    private void cuandoSeCargaUnBalanceParaUnaLiga() {
        resultado = controladorBalance.cargarBalanceParaUnaLiga(cargasParaUnaLiga);
    }

    private void cuandoSeCargaUnBalance() {
        resultado = controladorBalance.cargarBalanceParaUnEquipo(requestCargaBalanceUnEquipo);
    }

    private void seVerificaQueLaRespuestaEsUn200QueContieneUnaListaDeBalances() {
        assertThat(respuestaBalancesDeUnEquipoEnUnaTemporada.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(respuestaBalancesDeUnEquipoEnUnaTemporada.getBody().get(0).getClass()).isEqualTo(Balance.class);
    }

    private void seVerificaQueLaRespuestaEsUn200QueContieneUnaListaDeBalancesConLosSalarios() {
        assertThat(respuestaSalariosDeUnaLiga.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(respuestaSalariosDeUnaLiga.getBody().get(0).getClass()).isEqualTo(BalanceSalario.class);
    }

    private void seVerificaQueLaRespuestaEsUn200QueContieneUnDetalleDeBalances() {
        assertThat(respuestaDetalleBalanceUnEquipo.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(respuestaDetalleBalanceUnEquipo.getBody().getClass()).isEqualTo(DetalleBalance.class);
    }

    private void seVerificaQueLaRespuestaEsUn200() {
        assertThat(resultado.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void seVerificaQueLaRespuestaEsUn200QueContieneUnaListaDeDetallesDeBalances() {
        assertThat(resultadoObtenerTodosLosDetallesDeBalance.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(resultadoObtenerTodosLosDetallesDeBalance.getBody().get(0)).isInstanceOf(DetalleBalance.class);
    }
}