package com.tesis.futpal.controladores;

import com.tesis.futpal.comunicacion.requests.RequestActualizarEstadisticas;
import com.tesis.futpal.comunicacion.requests.RequestObtenerEstadisticasPartido;
import com.tesis.futpal.comunicacion.responses.ResponseObtenerEstadisticasLiga;
import com.tesis.futpal.comunicacion.responses.ResponseObtenerEstadisticasPartido;
import com.tesis.futpal.servicios.ServicioEstadistica;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ControladorEstadisticaTest {

    private static final Integer LIGA_ID = 1;
    private RequestActualizarEstadisticas requestActualizarEstadisticasValido;
    @Mock
    private ServicioEstadistica servicioEstadistica;
    @InjectMocks
    private ControladorEstadistica controladorEstadistica;
    private ResponseEntity resultado;
    private RequestObtenerEstadisticasPartido requestObtenerEstadisticasPartidoPartidoValido;

    @Test
    public void seActualizanLasEstadisticasDeManeraExitosa() {
        dadoUnRequestDeEstadisticaValido();
        dadoQueElServicioParaActualizarLasEstadisticasRespondeCorrectamente();
        cuandoSeActualizanLasEstadisticas();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void seObtienenLasEstadisticasDeUnPartidoDeManeraExitosa() {
        dadoUnRequestDeObtenerEstadisticaValido();
        dadoQueElServicioParaObtenerLasEstadisticasRespondeCorrectamente();
        cuandoSeObtienenLasEstadisticas();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void seObtienenLasEstadisticasDeLaLigaDeManeraExitosa() {
        dadoQueElServicioParaObtenerLasEstadisticasDeLaLigaRespondeCorrectamente();
        cuandoSeObtienenLasEstadisticasDeLaLiga();
        seVerificaQueLaRespuestaSeaUn200();
    }

    private void cuandoSeObtienenLasEstadisticasDeLaLiga() {
        resultado = controladorEstadistica.obtenerEstadisticasLiga(LIGA_ID);
    }

    private void dadoQueElServicioParaObtenerLasEstadisticasDeLaLigaRespondeCorrectamente() {
        when(servicioEstadistica.obtenerEstadisticasLiga(LIGA_ID)).thenReturn(new ResponseObtenerEstadisticasLiga());
    }

    private void cuandoSeObtienenLasEstadisticas() {
        resultado = controladorEstadistica.obtenerEstadisticas(requestObtenerEstadisticasPartidoPartidoValido);
    }

    private void dadoQueElServicioParaObtenerLasEstadisticasRespondeCorrectamente() {
        when(servicioEstadistica.obtenerEstadisticas(requestObtenerEstadisticasPartidoPartidoValido)).thenReturn(new ResponseObtenerEstadisticasPartido());
    }

    private void dadoUnRequestDeObtenerEstadisticaValido() {
        requestObtenerEstadisticasPartidoPartidoValido = new RequestObtenerEstadisticasPartido();
    }

    private void cuandoSeActualizanLasEstadisticas() {
        resultado = controladorEstadistica.actualizarEstadisticas(requestActualizarEstadisticasValido);
    }

    private void seVerificaQueLaRespuestaSeaUn200() {
        assertThat(resultado.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void dadoQueElServicioParaActualizarLasEstadisticasRespondeCorrectamente() {
        doNothing().when(servicioEstadistica).actualizarEstadisticas(requestActualizarEstadisticasValido);
    }

    private void dadoUnRequestDeEstadisticaValido() {
        requestActualizarEstadisticasValido = new RequestActualizarEstadisticas();
    }

}