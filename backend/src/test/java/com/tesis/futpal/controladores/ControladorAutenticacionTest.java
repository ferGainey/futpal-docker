package com.tesis.futpal.controladores;

import com.tesis.futpal.comunicacion.requests.RequestRegistrarUsuario;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.excepciones.CamposNulosException;
import com.tesis.futpal.excepciones.CamposVaciosException;
import com.tesis.futpal.excepciones.UsuarioExistenteException;
import com.tesis.futpal.servicios.ServicioRegistrarUsuario;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ControladorAutenticacionTest {

    private static final String MENSAJE_CAMPOS_NULOS_EXCEPTION = "No puede haber campos nulos.";
    private static final String MENSAJE_CAMPOS_VACIOS_EXCEPTION = "No puede haber campos vacíos.";
    private static final String MENSAJE_USUARIO_EXISTENTE_EXCEPTION = "No se ha podido crear el usuario. ";
    private static final String MENSAJE_ALIAS_EXISTENTE = "El alias ya existe.";
    private static final String MENSAJE_MAIL_EXISTENTE = "El mail ya existe.";
    @Mock
    private ServicioRegistrarUsuario servicioRegistrarUsuario;

    @InjectMocks
    private ControladorAutenticacion controladorAutenticacion;

    private ResponseEntity respuesta;

    @Test
    public void alRealizarElAltaDeUnUsuarioDeManeraCorrectaDevuelve200() throws UsuarioExistenteException, CamposNulosException, CamposVaciosException {
        dadoQueElServicioDeAltaUsuarioEjecutaElAltaExitosamente();
        cuandoRealizoElAlta();
        verificoQueLaRespuestaSeaUn200();
    }

    @Test
    public void alRealizarElAltaDeUnUsuarioConCamposNulosDevuelve400() throws UsuarioExistenteException, CamposNulosException, CamposVaciosException {
        dadoQueElServicioDeAltaUsuarioDevuelveCamposNulosException();
        cuandoRealizoElAlta();
        verificoQueLaRespuestaSeaUn400ConElMensaje(MENSAJE_CAMPOS_NULOS_EXCEPTION);
    }

    @Test
    public void alRealizarElAltaDeUnUsuarioConCamposVaciosDevuelve400() throws UsuarioExistenteException, CamposNulosException, CamposVaciosException {
        dadoQueElServicioDeAltaUsuarioDevuelveCamposVaciosException();
        cuandoRealizoElAlta();
        verificoQueLaRespuestaSeaUn400ConElMensaje(MENSAJE_CAMPOS_VACIOS_EXCEPTION);
    }

    @Test
    public void alRealizarElAltaDeUnUsuarioConAliasYaExistenteDevuelve409() throws UsuarioExistenteException, CamposNulosException, CamposVaciosException {
        dadoQueElServicioDeAltaUsuarioDevuelveUsuarioExistenteException(MENSAJE_ALIAS_EXISTENTE);
        cuandoRealizoElAlta();
        verificoQueLaRespuestaSeaUn409ConElMensaje(MENSAJE_USUARIO_EXISTENTE_EXCEPTION + MENSAJE_ALIAS_EXISTENTE);
    }

    @Test
    public void alRealizarElAltaDeUnUsuarioConMailYaExistenteDevuelve409() throws UsuarioExistenteException, CamposNulosException, CamposVaciosException {
        dadoQueElServicioDeAltaUsuarioDevuelveUsuarioExistenteException(MENSAJE_MAIL_EXISTENTE);
        cuandoRealizoElAlta();
        verificoQueLaRespuestaSeaUn409ConElMensaje(MENSAJE_USUARIO_EXISTENTE_EXCEPTION + MENSAJE_MAIL_EXISTENTE);
    }

    private void verificoQueLaRespuestaSeaUn409ConElMensaje(String mensajeEsperado) {
        assertThat(respuesta.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        assertThat(respuesta.getBody()).isEqualTo(mensajeEsperado);
    }

    private void dadoQueElServicioDeAltaUsuarioDevuelveUsuarioExistenteException(String motivoError) throws UsuarioExistenteException, CamposVaciosException, CamposNulosException {
        when(servicioRegistrarUsuario.registrarUsuario(any())).thenThrow(new UsuarioExistenteException(motivoError));
    }

    private void dadoQueElServicioDeAltaUsuarioDevuelveCamposVaciosException() throws UsuarioExistenteException, CamposVaciosException, CamposNulosException {
        when(servicioRegistrarUsuario.registrarUsuario(any())).thenThrow(new CamposVaciosException());
    }

    private void verificoQueLaRespuestaSeaUn400ConElMensaje(String mensajeError) {
        assertThat(respuesta.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(respuesta.getBody()).isEqualTo(mensajeError);
    }

    private void dadoQueElServicioDeAltaUsuarioDevuelveCamposNulosException() throws UsuarioExistenteException, CamposVaciosException, CamposNulosException {
        when(servicioRegistrarUsuario.registrarUsuario(any())).thenThrow(new CamposNulosException());
    }

    private void verificoQueLaRespuestaSeaUn200() {
        assertThat(respuesta.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void cuandoRealizoElAlta() {
        respuesta = controladorAutenticacion.registrarUsuario(new RequestRegistrarUsuario());
    }

    private void dadoQueElServicioDeAltaUsuarioEjecutaElAltaExitosamente() throws UsuarioExistenteException, CamposVaciosException, CamposNulosException {
        when(servicioRegistrarUsuario.registrarUsuario(any())).thenReturn(new Usuario());
    }
}
