package com.tesis.futpal.controladores;

import com.tesis.futpal.excepciones.JugadorYaTransferidoException;
import com.tesis.futpal.comunicacion.requests.transferencia.RequestCargaTransferencia;
import com.tesis.futpal.modelos.transferencia.DetalleTransferencia;
import com.tesis.futpal.servicios.ServicioTransferencia;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ControladorTransferenciasTest {

    private static final Integer ID_TRANSFERENCIA = 1;
    private ResponseEntity respuestaCargarTransferencia;
    @Mock
    private ServicioTransferencia servicioTransferencia;
    @InjectMocks
    private ControladorTransferencias controladorTransferencias;
    private RequestCargaTransferencia requestCargaTransferencia;
    private ResponseEntity<List<DetalleTransferencia>> resultadoObtenerTransferencias;
    private ResponseEntity<Void> respuestaRechazarTransferencia;
    private ResponseEntity<Void> respuestaAceptarTransferencia;
    private ResponseEntity<Boolean> respuestaCambiarEstadoMercado;
    private ResponseEntity<Boolean> respuestaObtenerEstadoDelMercado;

    @Test
    public void seCargaUnaTransferenciaExitosamente() throws JugadorYaTransferidoException {
        dadoUnRequestParaCargarUnaTransferenciaValido();
        dadoQueElServicioTransferenciaCargaLaTransferenciaExitosamente();

        cuandoSeCargaUnaTransferenciaBalance();

        seVerificaQueLaRespuestaEsUn200();
    }

    @Test
    public void seDevuelveErrorAlCargarUnaTransferenciaDeUnJugadorYaExistente() throws JugadorYaTransferidoException {
        dadoUnRequestParaCargarUnaTransferenciaValido();
        dadoQueElServicioTransferenciaLanzaExcepcionDeJugadorExistente();

        cuandoSeCargaUnaTransferenciaBalance();

        seVerificaQueLaRespuestaEsUn400();
    }

    @Test
    public void seObtienenLasTransferenciasDeUnEquipoExitosamente() {
        dadoQueElServicioDeObtenerTransferenciasDeUnEquipoDevuelveLasTransferenciasExitosamente();

        cuandoSeObtienenLasTransferenciasDeUnEquipo();

        seVerificaQueLaRespuestaDeObtenerTransferenciasEsUn200();
    }

    @Test
    public void seObtienenLasTransferenciasDeTodosLosEquiposExitosamente() {
        dadoQueElServicioDeObtenerTransferenciasDeTodosLosEquipoDevuelveLasTransferenciasExitosamente();

        cuandoSeObtienenLasTransferenciasDeTodosLosEquipos();

        seVerificaQueLaRespuestaDeObtenerTransferenciasEsUn200();
    }

    @Test
    public void seRechazaUnaTransferencia() {
        dadoQueElServicioTransferenciaRechazaCorrectamenteLaTransferencia();

        cuandoSeRechazaLaTransferencia();

        seVerificaQueLaRespuestaDeRechazarTransferenciaEsUn200();
    }

    @Test
    public void seConfirmaUnaTransferencia() {
        dadoQueElServicioTransferenciaAceptaCorrectamenteLaTransferencia();

        cuandoSeConfirmaLaTransferencia();

        seVerificaQueLaRespuestaDeAceptarTransferenciaEsUn200();
    }

    @Test
    public void seCambiaElEstadoDelMercado() {
        dadoQueElServicioDeTransferenciaPermiteCambiarElEstadoDelMercado();

        cuandoSeCambiaElEstadoDelMercado();

        seVerificaQueLaRespuestaDeCambiarEstadoDelMercadoEsUn200ConElNuevoEstado();
    }

    @Test
    public void seobtieneElEstadoDelMercado() {
        dadoQueElServicioDeTransferenciaDevuelveElEstadoDelMercado();

        cuandoSeObtieneElEstadoDelMercado();

        seVerificaQueLaRespuestaDeObtenerEstadoDelMercadoEsUn200ConElEstado();
    }

    private void dadoQueElServicioDeTransferenciaDevuelveElEstadoDelMercado() {
        when(servicioTransferencia.obtenerEstadoDeMercado()).thenReturn(true);
    }

    private void dadoQueElServicioDeTransferenciaPermiteCambiarElEstadoDelMercado() {
        when(servicioTransferencia.cambiarEstadoDeMercado(false)).thenReturn(true);
    }

    private void dadoQueElServicioDeObtenerTransferenciasDeTodosLosEquipoDevuelveLasTransferenciasExitosamente() {
        List<DetalleTransferencia> detallesTransferencias = Lists.emptyList();
        when(servicioTransferencia.obtenerTransferenciasConfirmadasDeTodosLosEquipos(1)).thenReturn(detallesTransferencias);
    }

    private void dadoQueElServicioTransferenciaAceptaCorrectamenteLaTransferencia() {
        doNothing().when(servicioTransferencia).confirmarTransferencia(anyInt());
    }

    private void dadoQueElServicioTransferenciaRechazaCorrectamenteLaTransferencia() {
        doNothing().when(servicioTransferencia).rechazarTransferencia(anyInt());
    }

    private void dadoQueElServicioDeObtenerTransferenciasDeUnEquipoDevuelveLasTransferenciasExitosamente() {
        List<DetalleTransferencia> detallesTransferencias = Lists.emptyList();
        when(servicioTransferencia.obtenerTransferenciasDeUnEquipo()).thenReturn(detallesTransferencias);
    }

    private void dadoQueElServicioTransferenciaLanzaExcepcionDeJugadorExistente() throws JugadorYaTransferidoException {
        doThrow(JugadorYaTransferidoException.class).when(servicioTransferencia).cargarTransferencia(any());
    }

    private void dadoUnRequestParaCargarUnaTransferenciaValido() {
        requestCargaTransferencia = new RequestCargaTransferencia();
    }

    private void dadoQueElServicioTransferenciaCargaLaTransferenciaExitosamente() throws JugadorYaTransferidoException {
        doNothing().when(servicioTransferencia).cargarTransferencia(any(RequestCargaTransferencia.class));
    }

    private void cuandoSeObtieneElEstadoDelMercado() {
        respuestaObtenerEstadoDelMercado = controladorTransferencias.obtenerEstadoDelMercado();
    }

    private void cuandoSeCambiaElEstadoDelMercado() {
        respuestaCambiarEstadoMercado = controladorTransferencias.cambiarEstadoDelMercado(false);
    }

    private void cuandoSeCargaUnaTransferenciaBalance() {
        respuestaCargarTransferencia = controladorTransferencias.cargarTransferencia(requestCargaTransferencia);
    }

    private void cuandoSeObtienenLasTransferenciasDeUnEquipo() {
        resultadoObtenerTransferencias = controladorTransferencias.obtenerTransferenciasDeUnEquipo();
    }

    private void cuandoSeRechazaLaTransferencia() {
        respuestaRechazarTransferencia = controladorTransferencias.rechazarTransferencia(ID_TRANSFERENCIA);
    }

    private void cuandoSeObtienenLasTransferenciasDeTodosLosEquipos() {
        resultadoObtenerTransferencias = controladorTransferencias.obtenerTransferenciasDeTodosLosEquipos(1);
    }

    private void cuandoSeConfirmaLaTransferencia() {
        respuestaAceptarTransferencia = controladorTransferencias.confirmarTransferencia(ID_TRANSFERENCIA);
    }

    private void seVerificaQueLaRespuestaEsUn200() {
        assertThat(respuestaCargarTransferencia.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void seVerificaQueLaRespuestaEsUn400() {
        assertThat(respuestaCargarTransferencia.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    private void seVerificaQueLaRespuestaDeObtenerTransferenciasEsUn200() {
        assertThat(resultadoObtenerTransferencias.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void seVerificaQueLaRespuestaDeRechazarTransferenciaEsUn200() {
        assertThat(respuestaRechazarTransferencia.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void seVerificaQueLaRespuestaDeAceptarTransferenciaEsUn200() {
        assertThat(respuestaAceptarTransferencia.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void seVerificaQueLaRespuestaDeCambiarEstadoDelMercadoEsUn200ConElNuevoEstado() {
        assertThat(respuestaCambiarEstadoMercado.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(respuestaCambiarEstadoMercado.getBody()).isEqualTo(true);
    }

    private void seVerificaQueLaRespuestaDeObtenerEstadoDelMercadoEsUn200ConElEstado() {
        assertThat(respuestaObtenerEstadoDelMercado.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(respuestaObtenerEstadoDelMercado.getBody()).isEqualTo(true);
    }
}
