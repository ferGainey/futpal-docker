package com.tesis.futpal.controladores;

import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.servicios.ServicioTemporada;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ControladorTemporadaTest {

    @Mock
    private ServicioTemporada servicioTemporada;
    @InjectMocks
    private ControladorTemporada controladorTemporada;
    private ResponseEntity<Temporada> respuestaObtenerTemporada;
    private ResponseEntity resultadoAvanzarTemporada;

    @Test
    public void alConsultarLaTemporadaActualDevuelve200() {
        dadoQueElServicioTemporadaEjecutaObtenerTemporadaActualExitosamente();
        cuandoSeRealizaLaConsultaDeLaTemporadaActual();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void alAvanzarDeTemporadaDevuelve200() {
        dadoQueElServicioTemporadaEjecutaAvanzarTemporadaExitosamente();
        cuandoSeRealizaElAvanceDeTemporada();
        seVerificaQueLaRespuestaAvanzarTemporadaSeaUn200();
    }

    private void seVerificaQueLaRespuestaAvanzarTemporadaSeaUn200() {
        assertThat(resultadoAvanzarTemporada.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void cuandoSeRealizaElAvanceDeTemporada() {
        resultadoAvanzarTemporada = controladorTemporada.avanzarTemporada();
    }

    private void dadoQueElServicioTemporadaEjecutaAvanzarTemporadaExitosamente() {
        doNothing().when(servicioTemporada).avanzarTemporada();
    }

    private void seVerificaQueLaRespuestaSeaUn200() {
        assertThat(respuestaObtenerTemporada.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void cuandoSeRealizaLaConsultaDeLaTemporadaActual() {
        respuestaObtenerTemporada = controladorTemporada.obtenerTemporadaActual();
    }

    private void dadoQueElServicioTemporadaEjecutaObtenerTemporadaActualExitosamente() {
        when(servicioTemporada.obtenerTemporadaActual()).thenReturn(new Temporada());
    }
}
