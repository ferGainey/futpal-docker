package com.tesis.futpal.controladores;

import com.tesis.futpal.modelos.liga.*;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.excepciones.LigaInexistenteException;
import com.tesis.futpal.excepciones.PartidoInexistenteException;
import com.tesis.futpal.comunicacion.requests.RequestActualizarResultado;
import com.tesis.futpal.comunicacion.requests.RequestRegistrarLiga;
import com.tesis.futpal.servicios.ServicioLiga;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ControladorLigaTest {

    private static final String MENSAJE_LIGA_INEXISTENTE = "La liga no existe.";
    private static final String MENSAJE_PARTIDO_INEXISTENTE = "El partido no existe.";
    @Mock
    ServicioLiga servicioLiga;
    @InjectMocks
    private ControladorLiga controladorLiga;
    private ResponseEntity respuesta;
    private static final Integer ID_LIGA = 1;

    @Test
    public void alRealizarElRegistroDeUnaLigaDeManeraCorrectaDevuelve200() {
        dadoQueElServicioDeRegistroLigaEjecutaElRegistroExitosamente();
        cuandoRealizoElRegistro();
        verificoQueLaRespuestaSeaUn200();
    }

    @Test
    public void alConsultarLasLigasDisponiblesDevuelve200() {
        dadoQueElServicioDeObtenerLigasDisponiblesSeEjecutaExitosamente();
        cuandoRealizoLaConsultaDeLigasDisponibles();
        verificoQueLaRespuestaSeaUn200();
        verificoQueLaRespuestaContengaLasLigasDisponiblesDevueltasPorElServicio();
    }

    @Test
    public void alConsultarLosEquiposDeLaLigasDevuelve200() throws LigaInexistenteException {
        dadoQueElServicioLigaObtieneLosEquiposDisponiblesExitosamente();
        cuandoRealizoLaConsultaDeEquiposDisponibles();
        verificoQueLaRespuestaSeaUn200();
        verificoQueLaRespuestaContengaLosEquiposDisponiblesDevueltasPorElServicio();
    }

    @Test
    public void alConsultarLosEquiposDeUnaLigaInexistenteDevuelve409() throws LigaInexistenteException {
        dadoQueElServicioLigaAlObtenerLosEquiposDevuelveLigaInexistenteException();
        cuandoRealizoLaConsultaDeEquiposDisponibles();
        verificoQueLaRespuestaSeaUn409ConElMensaje(MENSAJE_LIGA_INEXISTENTE);
    }

    @Test
    public void alConsultarLosPartidosDeLaLigaDevuelve200ConLosPartidos() {
        dadoQueElServicioDeObtenerPartidosSeEjecutaExitosamente();
        cuandoRealizoLaConsultaDeLosPartidos();
        verificoQueLaRespuestaSeaUn200();
        verificoQueLaRespuestaContengaLosPartidosDevueltosPorElServicio();
    }

    @Test
    public void alActualizarUnResultadoDeLigaDevuelve200() throws PartidoInexistenteException {
        dadoQueElServicioDeActualizarResultadoSeEjecutaExitosamente();
        cuandoSeActualizaUnResultado();
        verificoQueSeLlamoAlServicioActualizarResultado();
        verificoQueLaRespuestaSeaUn200();
    }

    @Test
    public void alActualizarElResultadoDeUnPartidoInexistenteDevuelve409() throws PartidoInexistenteException {
        dadoQueElServicioActualizarPartidoDevuelvePartidoInexistenteException();
        cuandoSeActualizaUnResultado();
        verificoQueLaRespuestaSeaUn409ConElMensaje(MENSAJE_PARTIDO_INEXISTENTE);
    }

    @Test
    public void alConsultarLasPosicionesDeLaLigaDevuelve200ConLasPosiciones() throws LigaInexistenteException {
        dadoQueElServicioDeObtenerPosicionesSeEjecutaExitosamente();
        cuandoRealizoLaConsultaDeLasPosiciones();
        verificoQueLaRespuestaSeaUn200();
        verificoQueLaRespuestaContengaLasPosicionesDevueltasPorElServicio();
    }

    private void verificoQueLaRespuestaContengaLasPosicionesDevueltasPorElServicio() {
        assertThat(((List<PosicionLiga>)respuesta.getBody()).size()).isEqualTo(2);
    }

    private void cuandoRealizoLaConsultaDeLasPosiciones() {
        respuesta = controladorLiga.obtenerPosicionesLiga(ID_LIGA);
    }

    private void dadoQueElServicioDeObtenerPosicionesSeEjecutaExitosamente() throws LigaInexistenteException {
        PosicionLiga posicionLiga1 = new PosicionLiga();
        PosicionLiga posicionLiga2 = new PosicionLiga();
        List<PosicionLiga> posicionesLiga = Lists.list(posicionLiga1, posicionLiga2);
        when(servicioLiga.obtenerPosiciones(ID_LIGA)).thenReturn(posicionesLiga);
    }

    private void dadoQueElServicioActualizarPartidoDevuelvePartidoInexistenteException() throws PartidoInexistenteException {
        Mockito.doThrow(new PartidoInexistenteException()).when(servicioLiga).actualizarResultado(any());
    }

    private void verificoQueSeLlamoAlServicioActualizarResultado() throws PartidoInexistenteException {
        verify(servicioLiga, times(1)).actualizarResultado(any(RequestActualizarResultado.class));
    }

    private void cuandoSeActualizaUnResultado() throws PartidoInexistenteException {
        RequestActualizarResultado requestActualizarResultado = new RequestActualizarResultado();
        respuesta = controladorLiga.actualizarResultado(requestActualizarResultado);
    }

    private void dadoQueElServicioDeActualizarResultadoSeEjecutaExitosamente() throws PartidoInexistenteException {
        Mockito.doNothing().when(servicioLiga).actualizarResultado(any(RequestActualizarResultado.class));
    }

    private void verificoQueLaRespuestaContengaLosPartidosDevueltosPorElServicio() {
        List<PartidoLigaBase> partidos = ((List<PartidoLigaBase>)respuesta.getBody());
        PartidoLigaBase partido1 = partidos.get(0);
        PartidoLigaBase partido2 = partidos.get(1);

        assertThat(partido1.getLiga().getId().intValue()).isEqualTo(ID_LIGA);
        assertThat(partido1.getEquipoLocal().getId()).isEqualTo(1);
        assertThat(partido1.getEquipoVisitante().getId()).isEqualTo(2);
        assertThat(partido1.getEquipoLocal().getNombreEquipo()).isEqualTo("Afalp");
        assertThat(partido1.getEquipoVisitante().getNombreEquipo()).isEqualTo("Untref");
        assertThat(partido1.getGolesEquipoLocal()).isEqualTo(5);
        assertThat(partido1.getGolesEquipoVisitante()).isEqualTo(3);
        assertThat(partido1.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.PENDIENTE.getId());
        assertThat(partido1.getNumeroFecha()).isEqualTo(1);

        assertThat(partido2.getLiga().getId().intValue()).isEqualTo(ID_LIGA);
        assertThat(partido2.getEquipoLocal().getId()).isEqualTo(2);
        assertThat(partido2.getEquipoVisitante().getId()).isEqualTo(1);
        assertThat(partido2.getEquipoLocal().getNombreEquipo()).isEqualTo("Untref");
        assertThat(partido2.getEquipoVisitante().getNombreEquipo()).isEqualTo("Afalp");
        assertThat(partido2.getGolesEquipoLocal()).isEqualTo(0);
        assertThat(partido2.getGolesEquipoVisitante()).isEqualTo(4);
        assertThat(partido2.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.PENDIENTE.getId());
        assertThat(partido2.getNumeroFecha()).isEqualTo(2);
    }

    private void cuandoRealizoLaConsultaDeLosPartidos() {
        respuesta = controladorLiga.obtenerPartidos(ID_LIGA);
    }

    private void dadoQueElServicioDeObtenerPartidosSeEjecutaExitosamente() {
        Liga liga = new Liga();
        liga.setId(ID_LIGA);
        EstadoPartido estadoPartidoPendiente = new EstadoPartido();
        estadoPartidoPendiente.setId(EstadoPartidoEnum.PENDIENTE.getId());

        PartidoLigaBase partido1 = new PartidoLigaBase();
        partido1.setLiga(liga);
        Equipo equipoLocal1 = new Equipo();
        equipoLocal1.setId(1L);
        equipoLocal1.setNombreEquipo("Afalp");
        Equipo equipoVisitante1 = new Equipo();
        equipoVisitante1.setId(2L);
        equipoVisitante1.setNombreEquipo("Untref");
        partido1.setEquipoLocal(equipoLocal1);
        partido1.setEquipoVisitante(equipoVisitante1);
        partido1.setGolesEquipoLocal(5);
        partido1.setGolesEquipoVisitante(3);
        partido1.setEstadoPartido(estadoPartidoPendiente);
        partido1.setNumeroFecha(1);

        PartidoLigaBase partido2 = new PartidoLigaBase();
        partido2.setLiga(liga);
        Equipo equipoLocal2 = new Equipo();
        equipoLocal2.setId(2L);
        equipoLocal2.setNombreEquipo("Untref");
        Equipo equipoVisitante2 = new Equipo();
        equipoVisitante2.setId(1L);
        equipoVisitante2.setNombreEquipo("Afalp");
        partido2.setEquipoLocal(equipoLocal2);
        partido2.setEquipoVisitante(equipoVisitante2);
        partido2.setGolesEquipoLocal(0);
        partido2.setGolesEquipoVisitante(4);
        partido2.setEstadoPartido(estadoPartidoPendiente);
        partido2.setNumeroFecha(2);

        List<PartidoLigaBase> partidos = Lists.list(partido1, partido2);

        when(servicioLiga.obtenerPartidos(ID_LIGA)).thenReturn(partidos);
    }

    private void verificoQueLaRespuestaSeaUn409ConElMensaje(String mensaje) {
        assertThat(respuesta.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        assertThat(respuesta.getBody()).isEqualTo(mensaje);
    }

    private void dadoQueElServicioLigaAlObtenerLosEquiposDevuelveLigaInexistenteException() throws LigaInexistenteException {
        when(servicioLiga.obtenerEquipos(ID_LIGA)).thenThrow(new LigaInexistenteException());
    }

    private void cuandoRealizoLaConsultaDeEquiposDisponibles() {
        respuesta = controladorLiga.obtenerEquipos(ID_LIGA);
    }

    private void verificoQueLaRespuestaContengaLosEquiposDisponiblesDevueltasPorElServicio() {
        List<Equipo> equipos = ((List<Equipo>) respuesta.getBody());
        Equipo equipo1 = equipos.get(0);
        Equipo equipo2 = equipos.get(1);
        assertThat(equipo1.getIdUsuario()).isEqualTo(1L);
        assertThat(equipo1.getNombreEquipo()).isEqualTo("Afalp");
        assertThat(equipo1.getId()).isEqualTo(1L);
        assertThat(equipo2.getIdUsuario()).isEqualTo(2L);
        assertThat(equipo2.getNombreEquipo()).isEqualTo("Untref");
        assertThat(equipo2.getId()).isEqualTo(2L);
    }

    private void dadoQueElServicioLigaObtieneLosEquiposDisponiblesExitosamente() throws LigaInexistenteException {
        Equipo equipo1 = new Equipo();
        equipo1.setIdUsuario(1L);
        equipo1.setNombreEquipo("Afalp");
        equipo1.setId(1L);
        Equipo equipo2 = new Equipo();
        equipo2.setIdUsuario(2L);
        equipo2.setNombreEquipo("Untref");
        equipo2.setId(2L);
        List<Equipo> equiposLiga = Lists.list(equipo1, equipo2);
        when(servicioLiga.obtenerEquipos(ID_LIGA)).thenReturn(equiposLiga);
    }

    private void verificoQueLaRespuestaContengaLasLigasDisponiblesDevueltasPorElServicio() {
        assertThat(((List<Liga>)respuesta.getBody()).get(0).getId()).isEqualTo(1L);
        assertThat(((List<Liga>)respuesta.getBody()).get(0).getNombre()).isEqualTo("Mi Liga");
    }

    private void cuandoRealizoLaConsultaDeLigasDisponibles() {
        respuesta = controladorLiga.obtenerLigasDisponibles();
    }

    private void dadoQueElServicioDeObtenerLigasDisponiblesSeEjecutaExitosamente() {
        Liga ligaCreada = new Liga();
        ligaCreada.setNombre("Mi Liga");
        ligaCreada.setId(1);
        when(servicioLiga.obtenerLigasDisponibles()).thenReturn(Collections.singletonList(ligaCreada));
    }

    private void verificoQueLaRespuestaSeaUn200() {
        assertThat(respuesta.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void cuandoRealizoElRegistro() {
        respuesta = controladorLiga.registrarLiga(new RequestRegistrarLiga());
    }

    private void dadoQueElServicioDeRegistroLigaEjecutaElRegistroExitosamente() {
        when(servicioLiga.registrarLiga(any(RequestRegistrarLiga.class))).thenReturn(new Liga());
    }
}
