package com.tesis.futpal.controladores;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.comunicacion.requests.RequestAprobarEquipo;
import com.tesis.futpal.comunicacion.requests.RequestRechazarEquipo;
import com.tesis.futpal.comunicacion.requests.RequestSolicitarAprobacionEquipo;
import com.tesis.futpal.servicios.ServicioEquipo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ControladorEquipoTest {

    private static final Long ID_EQUIPO_ELEGIDO = 1L;
    @Mock
    private ServicioEquipo servicioEquipo;
    @InjectMocks
    private ControladorEquipo controladorEquipo;
    private ResponseEntity respuesta;

    @Test
    public void alConsultarTodosLosEquiposDisponiblesDevuelve200() {
        dadoQueElServicioEquipoEjecutaElObtenerTodosLosEquiposAprobadosExitosamente();
        cuandoSeRealizaLaConsultaDeTodosLosEquiposAprobados();
        seVerificaQueLaRespuestaSeaUn200();
        verificoQueLaRespuestaDeTodosLosEquiposDisponiblesContengaUnEquipo();
    }
    @Test
    public void alConsultarTodosLosEquiposPendienteDeAprobacionDevuelve200() {
        dadoQueElServicioEquipoEjecutaElObtenerTodosLosEquiposPendienteAprobacionExitosamente();
        cuandoSeRealizaLaConsultaDeTodosLosEquiposPendienteAprobacion();
        seVerificaQueLaRespuestaSeaUn200();
        seVerificaQueLaRespuestaDeTodosLosEquiposPendienteAprobacionContengaUnEquipo();
    }

    @Test
    public void alAprobarUnEquipoExitosamenteDevuelve200() {
        dadoQueElServicioEquipoAlAprobarEquipoSeEjecutaExitosamente();
        cuandoSeApruebaUnEquipo();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void alRechazarUnEquipoExitosamenteDevuelve200() {
        dadoQueElServicioEquipoAlRechazarEquipoSeEjecutaExitosamente();
        cuandoSeRechazaUnEquipo();
        seVerificaQueLaRespuestaSeaUn200();
    }

    @Test
    public void alSolicitarAprobacionDeUnEquipoExitosamenteDevuelve200() {
        dadoQueElServicioEquipoDeAprobacionDeEquipoSeEjecutaExitosamente();
        cuandoSeSolicitaLaAprobacionDeUnEquipo();
        seVerificaQueLaRespuestaSeaUn200();
    }

    private void cuandoSeSolicitaLaAprobacionDeUnEquipo() {
        RequestSolicitarAprobacionEquipo requestSolicitarAprobacionEquipo = new RequestSolicitarAprobacionEquipo();
        requestSolicitarAprobacionEquipo.setEquipoId(ID_EQUIPO_ELEGIDO);
        respuesta = controladorEquipo.solicitarAprobacionEquipo(requestSolicitarAprobacionEquipo);
    }

    private void dadoQueElServicioEquipoDeAprobacionDeEquipoSeEjecutaExitosamente() {
        doNothing().when(servicioEquipo).solicitarAprobacionEquipo(anyLong());
    }

    private void cuandoSeRechazaUnEquipo() {
        RequestRechazarEquipo requestRechazarEquipo = new RequestRechazarEquipo();
        requestRechazarEquipo.setEquipoId(ID_EQUIPO_ELEGIDO);
        requestRechazarEquipo.setMotivoRechazo("Jugadores repetidos");
        respuesta = controladorEquipo.rechazarEquipo(requestRechazarEquipo);
    }

    private void dadoQueElServicioEquipoAlRechazarEquipoSeEjecutaExitosamente() {
        doNothing().when(servicioEquipo).rechazarEquipo(any(RequestRechazarEquipo.class));
    }

    private void cuandoSeApruebaUnEquipo() {
        RequestAprobarEquipo requestAprobarEquipo = new RequestAprobarEquipo();
        requestAprobarEquipo.setEquipoId(ID_EQUIPO_ELEGIDO);
        respuesta = controladorEquipo.aprobarEquipo(requestAprobarEquipo);
    }

    private void dadoQueElServicioEquipoAlAprobarEquipoSeEjecutaExitosamente() {
        doNothing().when(servicioEquipo).aprobarEquipo(anyLong());
    }

    private void cuandoSeRealizaLaConsultaDeTodosLosEquiposPendienteAprobacion() {
        respuesta = controladorEquipo.obtenerTodosLosEquiposPendienteAprobacion();
    }

    private void dadoQueElServicioEquipoEjecutaElObtenerTodosLosEquiposPendienteAprobacionExitosamente() {
        List<Equipo> listaEquipos = Collections.singletonList(new Equipo());
        when(servicioEquipo.obtenerTodosLosEquiposPendienteAprobacion()).thenReturn(listaEquipos);
    }


    private void verificoQueLaRespuestaDeTodosLosEquiposDisponiblesContengaUnEquipo() {
        assertThat(((List<Equipo>)respuesta.getBody()).size()).isEqualTo(1);
    }

    private void seVerificaQueLaRespuestaDeTodosLosEquiposPendienteAprobacionContengaUnEquipo() {
        assertThat(((List<Equipo>) respuesta.getBody()).size()).isEqualTo(1);
    }

    private void seVerificaQueLaRespuestaSeaUn200() {
        assertThat(respuesta.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private void cuandoSeRealizaLaConsultaDeTodosLosEquiposAprobados() {
        respuesta = controladorEquipo.obtenerTodosLosEquiposRegistradosAprobados();
    }

    private void dadoQueElServicioEquipoEjecutaElObtenerTodosLosEquiposAprobadosExitosamente() {
        List<Equipo> listaEquipos = Collections.singletonList(new Equipo());
        when(servicioEquipo.obtenerTodosLosEquiposAprobados()).thenReturn(listaEquipos);
    }
}
