package com.tesis.futpal.repositorios;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConstructorDeConsultaJugadoresPrestadosTest {

    @Test
    public void seCreaLaConsultaCorrectamente() {
        String consultaCreada = ConstructorDeConsultaJugadoresPrestados.nuevo()
                .consulta()
                .paraEquipo(3)
                .construir();
        String consultaEsperada = "select   " +
                "j.id id, " +
                "j.id jugador_id, " +
                "e.nombre_equipo nombre_equipo_actual, " +
                "mj.numero_temporada, " +
                "mj.periodo_id periodo_id   " +
                "from   " +
                "jugador j,   " +
                "movimiento_jugador mj,   " +
                "prestamo_jugador pj, " +
                "equipo e  " +
                "where   " +
                "1 = 1   " +
                "and mj.jugador_id = j.id   " +
                "and j.estado_jugador_id = 2   " +
                "and j.activo = true   " +
                "and mj.id = pj.movimiento_jugador_id   " +
                "and mj.numero_temporada >= :temporadaActual   " +
                "and pj.es_fin_de_prestamo = true   " +
                "and mj.equipo_destino_id = :equipo " +
                "and e.id = j.equipo_id;";
        assertThat(consultaCreada).isEqualTo(consultaEsperada);
    }
}