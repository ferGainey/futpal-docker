package com.tesis.futpal.repositorios;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConstructorDeConsultaJugadoresFiltradosTest {

    @Test
    public void seCreaLaConsultaCorrectamenteSiSeFiltraSoloPorNombre() {
        String consultaCreada = ConstructorDeConsultaJugadoresFiltrados.nuevo()
                .conNombreDeJugador("reus")
                .consulta()
                .construir();

        String consultaEsperada = "select   " +
                "j.id id, " +
                "j.id jugador_id, " +
                "e.nombre_equipo nombre_equipo_actual " +
                "from " +
                "jugador j, " +
                "equipo e " +
                "where " +
                "1 = 1 " +
                "and j.nombre ilike CONCAT('%',:nombre,'%') " +
                "and e.id = j.equipo_id " +
                "and j.activo = true;";
        assertThat(consultaCreada).isEqualTo(consultaEsperada);
    }

    @Test
    public void seCreaLaConsultaCorrectamenteSiSeFiltraSoloPorIdTransfermarkt() {
        String consultaCreada = ConstructorDeConsultaJugadoresFiltrados.nuevo()
                .conIdTransfermarkt(123456)
                .consulta()
                .construir();

        String consultaEsperada = "select   " +
                "j.id id, " +
                "j.id jugador_id, " +
                "e.nombre_equipo nombre_equipo_actual " +
                "from " +
                "jugador j, " +
                "equipo e " +
                "where " +
                "1 = 1 " +
                "and j.id_transfermarkt = :idTransfermarkt " +
                "and e.id = j.equipo_id " +
                "and j.activo = true;";
        assertThat(consultaCreada).isEqualTo(consultaEsperada);
    }

    @Test
    public void seCreaLaConsultaCorrectamenteSiSeFiltraPorIdTransfermarktYNombre() {
        String consultaCreada = ConstructorDeConsultaJugadoresFiltrados.nuevo()
                .conIdTransfermarkt(123456)
                .conNombreDeJugador("reus")
                .consulta()
                .construir();

        String consultaEsperada = "select   " +
                "j.id id, " +
                "j.id jugador_id, " +
                "e.nombre_equipo nombre_equipo_actual " +
                "from " +
                "jugador j, " +
                "equipo e " +
                "where " +
                "1 = 1 " +
                "and j.nombre ilike CONCAT('%',:nombre,'%') " +
                "and j.id_transfermarkt = :idTransfermarkt " +
                "and e.id = j.equipo_id " +
                "and j.activo = true;";
        assertThat(consultaCreada).isEqualTo(consultaEsperada);
    }
}