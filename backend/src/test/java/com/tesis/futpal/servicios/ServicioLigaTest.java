package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.excepciones.LigaInexistenteException;
import com.tesis.futpal.excepciones.PartidoInexistenteException;
import com.tesis.futpal.modelos.liga.*;
import com.tesis.futpal.comunicacion.requests.RequestActualizarResultado;
import com.tesis.futpal.comunicacion.requests.RequestRegistrarLiga;
import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.*;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServicioLigaTest {

    private static final Integer ID_LIGA_CREADA = 1;
    private static final int ID_EQUIPO_AFALP = 1;
    private static final int ID_EQUIPO_UNTREF = 2;
    private static final Integer ID_PARTIDO_ACTUALIZAR = 1;
    private static final Integer GOLES_LOCAL = 3;
    private static final Integer GOLES_VISITANTE = 2;
    private static final String NOMBRE_EQUIPO_AFALP = "Afalp";
    private static final String NOMBRE_EQUIPO_UNTREF = "Untref";
    private static final Integer ID_USUARIO = 1;
    @Mock
    private RepositorioLigas repositorioLigas;
    @Mock
    private RepositorioLigaEquipos repositorioLigaEquipos;
    @Mock
    private RepositorioEquipo repositorioEquipo;
    @Mock
    private RepositorioResultadoLiga repositorioResultadoLiga;
    @Mock
    private RepositorioTemporada repositorioTemporada;
    @Mock
    private GeneradorPartidos generadorPartidos;
    @Mock
    private ComparadorPosicion comparadorPosicion;

    @InjectMocks
    private ServicioLiga servicioLiga;

    @Captor
    ArgumentCaptor<LigaEquipos> ligaEquiposCaptor;
    @Captor
    ArgumentCaptor<List<PartidoLigaBase>> partidosLigaBaseCaptor;
    @Captor
    ArgumentCaptor<PartidoLigaBase> partidoLigaBaseCaptor;
    private RequestRegistrarLiga request;
    private Liga ligaCreada;
    private List<Liga> ligasDisponibles;
    private Liga liga1;
    private List<Equipo> resultadoObtenerEquiposLiga;
    private List<PartidoLigaBase> resultadoObtenerPartidosLiga;
    private RequestActualizarResultado requestActualizarResultado;
    private List<PosicionLiga> respuestaPosicionesLiga;

    @Test
    public void dadoUnRequestValidoSeEjecutaCorrectamenteElRegistroDeLaLiga() {
        dadoUnRequestValido();
        dadoQueSePuedeGuardarLaLiga();
        dadoQueSePuedenGuardarLosEquiposEnLaLiga();
        dadoQueSeGeneranLosPartidosCorrectamente();
        dadoQueSePuedenGuardarLosPartidos();
        dadoQueSePuedeObtenerLaTemporada();
        alIntentarRegistrarUnaLiga();
        entoncesSeCreaLaLiga();
        entoncesSeAgreganLosEquiposALaLiga();
        entoncesSeGeneranLosPartidos();
    }

    @Test
    public void seObtienenLasLigasDisponiblesExitosamente() {
        dadoQueExistenDosLigas();
        cuandoSeObtienenLasLigasDisponibles();
        seVerificaQueLasLigasContienenLosDatosCorrectos();
    }

    @Test
    public void seObtienenLosEquiposDeLaLigaExitosamente() throws LigaInexistenteException {
        dadoQueExisteLaLiga1();
        dadoQueExisten3Equipos();
        cuandoSeObtienenLosEquiposDeLaLiga();
        seVerificaQueLaRespuestaContieneLos3Equipos();
    }

    @Test
    public void seLanzaExcepcionSiLaLigaNoExiste() {
        dadoQueNoExisteLaLiga1();
        alIntentarObtenerLosEquiposseVerificaQueSeLanzaExcepcionPorLigaInexistente("La liga no existe.");
    }

    @Test
    public void seObtienenLosPartidosDeLaLigaExitosamente() {
        dadoQueExisten2Partidos();
        cuandoSeObtienenLosPartidosDeLaLiga();
        seVerificaQueLaRespuestaContieneLos2Partidos();
    }

    @Test
    public void seActualizaUnResultadoExitosamente() throws PartidoInexistenteException {
        dadoQueExisteElPartidoAActualizar();
        dadoUnRequestActualizarResultadoValido();
        dadoQueSePuedeGuardarElResultado();
        cuandoSeActualizaElPartido();
        seVerificaQueSeGuardoElPartidoActualizado();
    }

    @Test
    public void seObtienenLasPosicionesDeLaLigaExitosamente() throws LigaInexistenteException {
        dadoQueExisteLaLiga1();
        dadoQueExistenDosEquipos();
        dadoQueExisten2Partidos();
        dadoQueSePuedeEjecutarElComparadorDePosiciones();
        cuandoSeObtienenLasPosicionesDeLaLiga();
        seVerificaQueLosResultadosSeVenReflejadosEnLasPosiciones();
    }

    @Test
    public void seLanzaExcepcionSiElPartidoNoExiste() {
        dadoQueNoExisteElPartido();
        alIntentarObtenerLosEquiposseVerificaQueSeLanzaExcepcionPorPartidoInexistente("El partido no existe.");
    }

    private void dadoQueSePuedeObtenerLaTemporada() {
        Temporada temporada = new Temporada();
        temporada.setId(1);
        temporada.setActual(true);
        temporada.setNumero(1);
        when(repositorioTemporada.findByActual(anyBoolean())).thenReturn(Collections.singletonList(temporada));
    }

    private void dadoQueSePuedeEjecutarElComparadorDePosiciones() {
        when(comparadorPosicion.comparar(any(), any())).thenCallRealMethod();
    }

    private void dadoQueExistenDosEquipos() {
        LigaEquipos ligaEquipos1 = new LigaEquipos();
        ligaEquipos1.setId(1);
        ligaEquipos1.setIdEquipo(1);
        ligaEquipos1.setIdLiga(1);
        LigaEquipos ligaEquipos2 = new LigaEquipos();
        ligaEquipos2.setId(2);
        ligaEquipos2.setIdEquipo(2);
        ligaEquipos2.setIdLiga(1);
        LigaEquipos ligaEquipos3 = new LigaEquipos();

        List<LigaEquipos> ligaEquipos = Lists.list(ligaEquipos1, ligaEquipos2);
        when(repositorioLigaEquipos.findByIdLiga(liga1.getId())).thenReturn(ligaEquipos);

        Equipo equipo1 = new Equipo();
        equipo1.setIdUsuario(1L);
        equipo1.setNombreEquipo("Afalp");
        equipo1.setId(1L);
        Equipo equipo2 = new Equipo();
        equipo2.setIdUsuario(2L);
        equipo2.setNombreEquipo("Untref");
        equipo2.setId(2L);

        List<Equipo> equipos = Lists.list(equipo1, equipo2);

        List<Long> idEquipos = Lists.list(ligaEquipos1.getIdEquipo().longValue(), ligaEquipos2.getIdEquipo().longValue());
        when(repositorioEquipo.findAllById(idEquipos)).thenReturn(equipos);
    }

    private void cuandoSeObtienenLasPosicionesDeLaLiga() throws LigaInexistenteException {
        respuestaPosicionesLiga = servicioLiga.obtenerPosiciones(ID_LIGA_CREADA);
    }

    private void seVerificaQueLosResultadosSeVenReflejadosEnLasPosiciones() {
        PosicionLiga posicion1 = respuestaPosicionesLiga.get(0);
        assertThat(posicion1.getEquipo().getId()).isEqualTo(ID_EQUIPO_AFALP);
        assertThat(posicion1.getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_AFALP);
        assertThat(posicion1.getPartidosJugados()).isEqualTo(2);
        assertThat(posicion1.getGolesAFavor()).isEqualTo(9);
        assertThat(posicion1.getGolesEnContra()).isEqualTo(3);
        assertThat(posicion1.getDiferenciaDeGoles()).isEqualTo(6);
        assertThat(posicion1.getPuntosLiga()).isEqualTo(6);

        PosicionLiga posicion2 = respuestaPosicionesLiga.get(1);
        assertThat(posicion2.getEquipo().getId()).isEqualTo(ID_EQUIPO_UNTREF);
        assertThat(posicion2.getEquipo().getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO_UNTREF);
        assertThat(posicion2.getPartidosJugados()).isEqualTo(2);
        assertThat(posicion2.getGolesAFavor()).isEqualTo(3);
        assertThat(posicion2.getGolesEnContra()).isEqualTo(9);
        assertThat(posicion2.getDiferenciaDeGoles()).isEqualTo(-6);
        assertThat(posicion2.getPuntosLiga()).isEqualTo(0);
    }

    private void dadoQueNoExisteElPartido() {
        when(repositorioResultadoLiga.findById(anyInt())).thenReturn(Optional.empty());
    }

    private void alIntentarObtenerLosEquiposseVerificaQueSeLanzaExcepcionPorPartidoInexistente(String mensajeEsperado) {
        RequestActualizarResultado requestActualizarResultadoPartidoInexistente = new RequestActualizarResultado();
        requestActualizarResultadoPartidoInexistente.setIdPartido(1);
        PartidoInexistenteException exception = assertThrows(
                PartidoInexistenteException.class, () -> {
                    servicioLiga.actualizarResultado(requestActualizarResultadoPartidoInexistente);
                });
        assertThat(exception.getMessage()).isEqualTo(mensajeEsperado);
    }

    private void dadoQueSePuedeGuardarElResultado() {
        when(repositorioResultadoLiga.save(any(PartidoLigaBase.class))).thenReturn(new PartidoLigaBase());
    }

    private void seVerificaQueSeGuardoElPartidoActualizado() {
        verify(repositorioResultadoLiga, times(1)).save(partidoLigaBaseCaptor.capture());
        PartidoLigaBase partidoGuardado = partidoLigaBaseCaptor.getValue();
        assertThat(partidoGuardado.getId()).isEqualTo(requestActualizarResultado.getIdPartido());
        assertThat(partidoGuardado.getId()).isEqualTo(ID_PARTIDO_ACTUALIZAR);
        assertThat(partidoGuardado.getGolesEquipoLocal()).isEqualTo(requestActualizarResultado.getGolesLocal());
        assertThat(partidoGuardado.getGolesEquipoVisitante()).isEqualTo(requestActualizarResultado.getGolesVisitante());
        assertThat(partidoGuardado.getUltimoUsuarioEditor().getId().intValue()).isEqualTo(ID_USUARIO);
        assertThat(partidoGuardado.getEstadoPartido().getId()).isEqualTo(2);
    }

    private void dadoUnRequestActualizarResultadoValido() {
        requestActualizarResultado = new RequestActualizarResultado();
        requestActualizarResultado.setIdPartido(ID_PARTIDO_ACTUALIZAR);
        requestActualizarResultado.setGolesLocal(GOLES_LOCAL);
        requestActualizarResultado.setGolesVisitante(GOLES_VISITANTE);
        requestActualizarResultado.setUsuarioEditorId(ID_USUARIO);
    }

    private void cuandoSeActualizaElPartido() throws PartidoInexistenteException {
        servicioLiga.actualizarResultado(requestActualizarResultado);
    }

    private void dadoQueExisteElPartidoAActualizar() {
        PartidoLigaBase partidoLigaBase = new PartidoLigaBase();
        partidoLigaBase.setId(ID_PARTIDO_ACTUALIZAR);
        partidoLigaBase.setNumeroFecha(1);
        EstadoPartido estadoPartidoPendiente = new EstadoPartido();
        estadoPartidoPendiente.setId(EstadoPartidoEnum.PENDIENTE.getId());
        partidoLigaBase.setEstadoPartido(estadoPartidoPendiente);
        Equipo equipoAfalp = new Equipo();
        equipoAfalp.setId((long) ID_EQUIPO_AFALP);
        equipoAfalp.setNombreEquipo("Afalp");
        Equipo equipoUntref = new Equipo();
        equipoUntref.setId((long) ID_EQUIPO_UNTREF);
        equipoUntref.setNombreEquipo("Untref");
        partidoLigaBase.setEquipoLocal(equipoUntref);
        partidoLigaBase.setEquipoVisitante(equipoAfalp);
        when(repositorioResultadoLiga.findById(ID_PARTIDO_ACTUALIZAR)).thenReturn(java.util.Optional.of(partidoLigaBase));
    }

    private void dadoQueSePuedenGuardarLosPartidos() {
        when(repositorioResultadoLiga.saveAll(anyIterable())).thenReturn(new ArrayList<>());
    }

    private void dadoQueSeGeneranLosPartidosCorrectamente() {
        PartidoLigaBase partidoLigaBase1 = generarPartidoBasePendiente(ID_LIGA_CREADA,1, 1, 2);
        PartidoLigaBase partidoLigaBase3 = generarPartidoBasePendiente(ID_LIGA_CREADA,2, 1, 3);
        PartidoLigaBase partidoLigaBase4 = generarPartidoBasePendiente(ID_LIGA_CREADA,2, 2, 8);
        PartidoLigaBase partidoLigaBase2 = generarPartidoBasePendiente(ID_LIGA_CREADA,1, 3, 8);
        PartidoLigaBase partidoLigaBase5 = generarPartidoBasePendiente(ID_LIGA_CREADA,3, 1, 8);
        PartidoLigaBase partidoLigaBase6 = generarPartidoBasePendiente(ID_LIGA_CREADA,3, 2, 3);
        List<PartidoLigaBase> partidosBase = Lists.list(partidoLigaBase1, partidoLigaBase2, partidoLigaBase3, partidoLigaBase4, partidoLigaBase5, partidoLigaBase6);
        when(generadorPartidos.generar(request.getIdEquipos(), false)).thenReturn(partidosBase);
    }

    private PartidoLigaBase generarPartidoBasePendiente(int idLiga, int nroFecha, int idEquipoLocal, int idEquipoVisitante) {
        PartidoLigaBase partidoLigaBase = new PartidoLigaBase();
        Liga liga = new Liga();
        liga.setId(idLiga);
        partidoLigaBase.setLiga(liga);
        partidoLigaBase.setNumeroFecha(nroFecha);
        Equipo equipoLocal = new Equipo();
        equipoLocal.setId((long) idEquipoLocal);
        partidoLigaBase.setEquipoLocal(equipoLocal);
        Equipo equipoVisitante = new Equipo();
        equipoVisitante.setId((long) idEquipoVisitante);
        partidoLigaBase.setEquipoVisitante(equipoVisitante);
        EstadoPartido estadoPartidoPendiente = new EstadoPartido();
        estadoPartidoPendiente.setId(EstadoPartidoEnum.PENDIENTE.getId());
        partidoLigaBase.setEstadoPartido(estadoPartidoPendiente);
        return partidoLigaBase;
    }

    private void seVerificaQueLaRespuestaContieneLos2Partidos() {
        List<PartidoLigaBase> partidos = resultadoObtenerPartidosLiga;
        PartidoLigaBase partido1 = partidos.get(0);
        PartidoLigaBase partido2 = partidos.get(1);

        assertThat(partido1.getLiga().getId()).isEqualTo(ID_LIGA_CREADA.intValue());
        assertThat(partido1.getId()).isEqualTo(1);
        assertThat(partido1.getEquipoLocal().getId()).isEqualTo(ID_EQUIPO_AFALP);
        assertThat(partido1.getEquipoVisitante().getId()).isEqualTo(ID_EQUIPO_UNTREF);
        assertThat(partido1.getEquipoLocal().getNombreEquipo()).isEqualTo("Afalp");
        assertThat(partido1.getEquipoVisitante().getNombreEquipo()).isEqualTo("Untref");
        assertThat(partido1.getGolesEquipoLocal()).isEqualTo(5);
        assertThat(partido1.getGolesEquipoVisitante()).isEqualTo(3);
        assertThat(partido1.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.FINALIZADO.getId());
        assertThat(partido1.getNumeroFecha()).isEqualTo(1);

        assertThat(partido2.getLiga().getId()).isEqualTo(ID_LIGA_CREADA.intValue());
        assertThat(partido2.getId()).isEqualTo(2);
        assertThat(partido2.getEquipoLocal().getId()).isEqualTo(ID_EQUIPO_UNTREF);
        assertThat(partido2.getEquipoVisitante().getId()).isEqualTo(ID_EQUIPO_AFALP);
        assertThat(partido2.getEquipoLocal().getNombreEquipo()).isEqualTo("Untref");
        assertThat(partido2.getEquipoVisitante().getNombreEquipo()).isEqualTo("Afalp");
        assertThat(partido2.getGolesEquipoLocal()).isEqualTo(0);
        assertThat(partido2.getGolesEquipoVisitante()).isEqualTo(4);
        assertThat(partido2.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.FINALIZADO.getId());
        assertThat(partido2.getNumeroFecha()).isEqualTo(2);
        assertThat(partido2.getUltimoUsuarioEditor().getAliasUsuario()).isEqualTo("ferg");
    }

    private void cuandoSeObtienenLosPartidosDeLaLiga() {
        resultadoObtenerPartidosLiga = servicioLiga.obtenerPartidos(ID_LIGA_CREADA);
    }

    private void dadoQueExisten2Partidos() {
        PartidoLigaBase partido1 = new PartidoLigaBase();
        Equipo equipoAfalp = new Equipo();
        equipoAfalp.setId((long) ID_EQUIPO_AFALP);
        equipoAfalp.setNombreEquipo("Afalp");
        Equipo equipoUntref = new Equipo();
        equipoUntref.setId((long) ID_EQUIPO_UNTREF);
        equipoUntref.setNombreEquipo("Untref");
        Liga liga = new Liga();
        liga.setId(ID_LIGA_CREADA);


        EstadoPartido estadoPartidoFinalizado = new EstadoPartido();
        estadoPartidoFinalizado.setId(EstadoPartidoEnum.FINALIZADO.getId());

        partido1.setLiga(liga);
        partido1.setId(1);
        partido1.setEquipoLocal(equipoAfalp);
        partido1.setEquipoVisitante(equipoUntref);
        partido1.setGolesEquipoLocal(5);
        partido1.setGolesEquipoVisitante(3);
        partido1.setEstadoPartido(estadoPartidoFinalizado);
        partido1.setNumeroFecha(1);
        PartidoLigaBase partido2 = new PartidoLigaBase();
        partido2.setLiga(liga);
        partido2.setId(2);
        partido2.setEquipoLocal(equipoUntref);
        partido2.setEquipoVisitante(equipoAfalp);
        partido2.setGolesEquipoLocal(0);
        partido2.setGolesEquipoVisitante(4);
        partido2.setEstadoPartido(estadoPartidoFinalizado);
        partido2.setNumeroFecha(2);
        Usuario ultimoUsuarioEditor = new Usuario();
        ultimoUsuarioEditor.setAliasUsuario("ferg");
        ultimoUsuarioEditor.setId(1L);
        partido2.setUltimoUsuarioEditor(ultimoUsuarioEditor);
        List<PartidoLigaBase> partidos = Lists.list(partido1, partido2);

        when(repositorioResultadoLiga.findByLigaId(ID_LIGA_CREADA)).thenReturn(partidos);
    }

    private void alIntentarObtenerLosEquiposseVerificaQueSeLanzaExcepcionPorLigaInexistente(String mensajeEsperado) {
        LigaInexistenteException exception = assertThrows(
                LigaInexistenteException.class, () -> {
                    servicioLiga.obtenerEquipos(ID_LIGA_CREADA);
                });
        assertThat(exception.getMessage()).isEqualTo(mensajeEsperado);
    }

    private void dadoQueNoExisteLaLiga1() {
        when(repositorioLigas.findById(1)).thenReturn(java.util.Optional.empty());
    }

    private void seVerificaQueLaRespuestaContieneLos3Equipos() {
        List<Equipo> equipos = resultadoObtenerEquiposLiga;
        Equipo equipo1 = equipos.get(0);
        Equipo equipo2 = equipos.get(1);
        Equipo equipo3 = equipos.get(2);
        assertThat(equipo1.getId()).isEqualTo(1);
        assertThat(equipo2.getId()).isEqualTo(2);
        assertThat(equipo3.getId()).isEqualTo(3);
    }

    private void cuandoSeObtienenLosEquiposDeLaLiga() throws LigaInexistenteException {
        resultadoObtenerEquiposLiga = servicioLiga.obtenerEquipos(ID_LIGA_CREADA);
    }

    private void dadoQueExisteLaLiga1() {
        liga1 = new Liga();
        liga1.setId(1);
        liga1.setNombre("Liga 1");
        when(repositorioLigas.findById(1)).thenReturn(java.util.Optional.ofNullable(liga1));
    }

    private void dadoQueExisten3Equipos() {
        LigaEquipos ligaEquipos1 = new LigaEquipos();
        ligaEquipos1.setId(1);
        ligaEquipos1.setIdEquipo(1);
        ligaEquipos1.setIdLiga(1);
        LigaEquipos ligaEquipos2 = new LigaEquipos();
        ligaEquipos2.setId(2);
        ligaEquipos2.setIdEquipo(2);
        ligaEquipos2.setIdLiga(1);
        LigaEquipos ligaEquipos3 = new LigaEquipos();
        ligaEquipos3.setId(3);
        ligaEquipos3.setIdEquipo(3);
        ligaEquipos3.setIdLiga(1);
        List<LigaEquipos> ligaEquipos = Lists.list(ligaEquipos1, ligaEquipos2, ligaEquipos3);
        when(repositorioLigaEquipos.findByIdLiga(liga1.getId())).thenReturn(ligaEquipos);

        Equipo equipo1 = new Equipo();
        equipo1.setIdUsuario(1L);
        equipo1.setNombreEquipo("Afalp");
        equipo1.setId(1L);
        Equipo equipo2 = new Equipo();
        equipo2.setIdUsuario(2L);
        equipo2.setNombreEquipo("Untref");
        equipo2.setId(2L);
        Equipo equipo3 = new Equipo();
        equipo3.setIdUsuario(3L);
        equipo3.setNombreEquipo("CCJ");
        equipo3.setId(3L);
        List<Equipo> equipos = Lists.list(equipo1, equipo2, equipo3);

        List<Long> idEquipos = Lists.list(ligaEquipos1.getIdEquipo().longValue(), ligaEquipos2.getIdEquipo().longValue(), ligaEquipos3.getIdEquipo().longValue());
        when(repositorioEquipo.findAllById(idEquipos)).thenReturn(equipos);
    }

    private void dadoQueSePuedenGuardarLosEquiposEnLaLiga() {
        LigaEquipos ligaEquipos = new LigaEquipos();
        when(repositorioLigaEquipos.save(any(LigaEquipos.class))).thenReturn(ligaEquipos);
    }

    private void entoncesSeAgreganLosEquiposALaLiga() {
        verify(repositorioLigaEquipos, times(4)).save(ligaEquiposCaptor.capture());

        List<LigaEquipos> ligaEquiposCapturados = ligaEquiposCaptor.getAllValues();
        assertThat(ligaEquiposCapturados.get(0).getIdEquipo()).isEqualTo(request.getIdEquipos().get(0));
        assertThat(ligaEquiposCapturados.get(1).getIdEquipo()).isEqualTo(request.getIdEquipos().get(1));
        assertThat(ligaEquiposCapturados.get(0).getIdLiga()).isEqualTo(ligaCreada.getId().intValue());
        assertThat(ligaEquiposCapturados.get(1).getIdLiga()).isEqualTo(ligaCreada.getId().intValue());
    }

    private void seVerificaQueLasLigasContienenLosDatosCorrectos() {
        Liga liga1 = ligasDisponibles.get(0);
        Liga liga2 = ligasDisponibles.get(1);
        assertThat(liga1.getId()).isEqualTo(1L);
        assertThat(liga1.getNombre()).isEqualTo("Liga 1");
        assertThat(liga2.getId()).isEqualTo(2L);
        assertThat(liga2.getNombre()).isEqualTo("Liga 2");
    }

    private void cuandoSeObtienenLasLigasDisponibles() {
        ligasDisponibles = servicioLiga.obtenerLigasDisponibles();
    }

    private void dadoQueExistenDosLigas() {
        Liga liga1 = new Liga();
        liga1.setId(1);
        liga1.setNombre("Liga 1");
        Liga liga2 = new Liga();
        liga2.setId(2);
        liga2.setNombre("Liga 2");
        List<Liga> ligasDisponibles = new ArrayList<>();
        ligasDisponibles.add(liga1);
        ligasDisponibles.add(liga2);
        when(repositorioLigas.findAllByOrderByIdDesc()).thenReturn(ligasDisponibles);
    }

    private void entoncesSeCreaLaLiga() {
        verify(repositorioLigas, times(1)).save(any(Liga.class));
        verify(repositorioLigaEquipos, times(4)).save(any(LigaEquipos.class));
        assertThat(ligaCreada.getId()).isEqualTo(1L);
        assertThat(ligaCreada.getNombre()).isEqualTo(request.getNombreLiga());
    }

    private void alIntentarRegistrarUnaLiga() {
        ligaCreada = servicioLiga.registrarLiga(request);
    }

    private void dadoQueSePuedeGuardarLaLiga() {
        Liga liga = new Liga();
        liga.setId(ID_LIGA_CREADA);
        liga.setNombre("Liga de Bronce");
        when(repositorioLigas.save(any(Liga.class))).thenReturn(liga);
    }

    private void dadoUnRequestValido() {
        List<Integer> idEquipos = Lists.list(1,2,3,8);
        request = new RequestRegistrarLiga();
        request.setNombreLiga("Liga de Bronce");
        request.setIdEquipos(idEquipos);
    }

    private void entoncesSeGeneranLosPartidos() {
        verify(repositorioResultadoLiga, times(1)).saveAll(partidosLigaBaseCaptor.capture());
        List<PartidoLigaBase> partidosLiga = partidosLigaBaseCaptor.getValue();
        PartidoLigaBase partido1 = partidosLiga.get(0);
        PartidoLigaBase partido2 = partidosLiga.get(1);
        PartidoLigaBase partido3 = partidosLiga.get(2);
        PartidoLigaBase partido4 = partidosLiga.get(3);
        PartidoLigaBase partido5 = partidosLiga.get(4);
        PartidoLigaBase partido6 = partidosLiga.get(5);

        assertThat(partido1.getLiga().getId()).isEqualTo(ID_LIGA_CREADA.intValue());
        assertThat(partido1.getNumeroFecha()).isEqualTo(1);
        assertThat(partido1.getEquipoLocal().getId()).isEqualTo(1);
        assertThat(partido1.getEquipoVisitante().getId()).isEqualTo(2);
        assertThat(partido1.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.PENDIENTE.getId());

        assertThat(partido2.getLiga().getId()).isEqualTo(ID_LIGA_CREADA.intValue());
        assertThat(partido2.getNumeroFecha()).isEqualTo(1);
        assertThat(partido2.getEquipoLocal().getId()).isEqualTo(3);
        assertThat(partido2.getEquipoVisitante().getId()).isEqualTo(8);
        assertThat(partido2.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.PENDIENTE.getId());

        assertThat(partido3.getLiga().getId()).isEqualTo(ID_LIGA_CREADA.intValue());
        assertThat(partido3.getNumeroFecha()).isEqualTo(2);
        assertThat(partido3.getEquipoLocal().getId()).isEqualTo(1);
        assertThat(partido3.getEquipoVisitante().getId()).isEqualTo(3);
        assertThat(partido3.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.PENDIENTE.getId());

        assertThat(partido4.getLiga().getId()).isEqualTo(ID_LIGA_CREADA.intValue());
        assertThat(partido4.getNumeroFecha()).isEqualTo(2);
        assertThat(partido4.getEquipoLocal().getId()).isEqualTo(2);
        assertThat(partido4.getEquipoVisitante().getId()).isEqualTo(8);
        assertThat(partido4.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.PENDIENTE.getId());

        assertThat(partido5.getLiga().getId()).isEqualTo(ID_LIGA_CREADA.intValue());
        assertThat(partido5.getNumeroFecha()).isEqualTo(3);
        assertThat(partido5.getEquipoLocal().getId()).isEqualTo(1);
        assertThat(partido5.getEquipoVisitante().getId()).isEqualTo(8);
        assertThat(partido5.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.PENDIENTE.getId());

        assertThat(partido6.getLiga().getId()).isEqualTo(ID_LIGA_CREADA.intValue());
        assertThat(partido6.getNumeroFecha()).isEqualTo(3);
        assertThat(partido6.getEquipoLocal().getId()).isEqualTo(2);
        assertThat(partido6.getEquipoVisitante().getId()).isEqualTo(3);
        assertThat(partido6.getEstadoPartido().getId()).isEqualTo(EstadoPartidoEnum.PENDIENTE.getId());
    }
}
