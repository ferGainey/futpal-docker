package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.excepciones.JugadorYaTransferidoException;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.comunicacion.requests.transferencia.JugadorPrestamoTransferencia;
import com.tesis.futpal.comunicacion.requests.transferencia.JugadorTransferencia;
import com.tesis.futpal.comunicacion.requests.transferencia.MontoTransferencia;
import com.tesis.futpal.comunicacion.requests.transferencia.RequestCargaTransferencia;
import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.modelos.transferencia.*;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.*;
import com.tesis.futpal.utilidades.InformacionDePedido;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServicioTransferenciaTest {

    private static final Integer MONTO_BALANCE_1 = 50000;
    private static final Long OTRO_EQUIPO_ID = 2L;
    private static final Long ID_USUARIO_QUE_RECIBE_LA_TRANSFERENCIA = 2L;
    private static final Long ID_USUARIO_QUE_HACE_EL_PEDIDO = 1L;
    private static final Integer ESTADO_TRANSFERENCIA_PENDIENTE = 1;
    private static final Integer ID_TRANSFERENCIA_A_RECHAZAR = 5;
    private static final Integer ID_ESTADO_TRANSFERENCIA_PENDIENTE = 1;
    private static final Integer ID_ESTADO_TRANSFERENCIA_CANCELADA = 2;
    private static final Integer ID_TRANSFERENCIA_A_CONFIRMAR = 5;
    private static final Integer ID_ESTADO_TRANSFERENCIA_CONFIRMADA = 3;
    private static final Integer NUMERO_TEMPORADA_2 = 2;
    private static final Integer ID_TIPO_TRANSFERENCIA_TRASPASO = 1;
    private static final Integer ID_TIPO_TRANSFERENCIA_PRESTAMO = 2;

    @Mock
    private RepositorioTransferencia repositorioTransferencia;
    @Mock
    private RepositorioMovimientoJugador repositorioMovimientoJugador;
    @Mock
    private RepositorioBalance repositorioBalance;
    @Mock
    private RepositorioTemporada repositorioTemporada;
    @Mock
    private RepositorioEquipo repositorioEquipo;
    @Mock
    private RepositorioUsuario repositorioUsuario;
    @Mock
    private RepositorioJugador repositorioJugador;
    @Mock
    private RepositorioPrestamo repositorioPrestamo;
    @Mock
    private RepositorioMercado repositorioMercado;
    @Mock
    private ValidadorTransferencia validadorTransferencia;
    @Mock
    private InformacionDePedido informacionPedido;
    @Mock
    private CreadorDetalle creadorDetalle;

    @InjectMocks
    private ServicioTransferencia servicioTransferencia;

    @Mock
    private ServicioPlantel servicioPlantel;

    private static final Integer ID_EQUIPO_AFALP = 1;
    private static final Integer ID_EQUIPO_UNTREF = 2;
    private static final Long ID_JUGADOR_1 = 1L;
    private static final Integer NUMERO_TEMPORADA_1 = 1;
    private static final Integer ID_PERIODO_1 = 1;
    private static final String CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA = "CARGA_TRANSFERENCIAS";
    private RequestCargaTransferencia requestCargaTransferencia;

    @Captor
    ArgumentCaptor<MovimientoJugador> movimientoJugadorArgumentCaptor;
    @Captor
    ArgumentCaptor<Balance> balanceArgumentCaptor;
    @Captor
    ArgumentCaptor<Transferencia> transferenciaArgumentCaptor;
    @Captor
    ArgumentCaptor<EstadoTransferencia> estadoTransferenciaArgumentCaptor;
    private Usuario usuarioQueHaceElPedido;
    private List<DetalleTransferencia> resultadoObtenerTransferencias;
    private MovimientoJugador movimientoJugadorTraspaso;
    private MovimientoJugador movimientoJugadorPrestamoPrincipio;
    private MovimientoJugador movimientoJugadorPrestamoFinal;
    private Temporada temporadaActual;
    private PeriodoTemporada periodoTemporadaActual;
    private Transferencia transferencia;


    @Test
    public void alRealizarUnaTransferenciaSeGuardaJuntoConLosMovimientosDeLosJugadores() throws JugadorYaTransferidoException {
        dadoUnRequestDeTransferenciaConJugadores();
        dadoQueSeCreanLosMovimientosDeLosJugadoresCorrectamente();
        dadoQueLaTransferenciaTieneUnaListaDeJugadores();
        dadoQueLaTransferenciaTieneUnaListaConUnJugadorAPrestamo();
        dadoQueLaTransferenciaEsValida();
        dadoQueSeObtieneElUsuarioDelTokenCorrectamente();
        dadoQueSeObtieneElEquipoQueRecibeLaTransferenciaCorrectamente();
        dadoQueSeObtieneElUsuarioDelEquipoQueRecibeLaTransferenciaCorrectamente();
        dadoQueSePuedenGuardarLosMovimientos();
        dadoQueSePuedeGuardarElPrestamoCorrectamente();

        cuandoSeRealizaUnaTransferencia();

        seVerificaQueSeGuardaLaTransferencia();
        seVerificaQueSeGuardanLosMovimientos();
        seVerificaQueSeGuardoElPrestamo();
    }

    @Test
    public void seObtienenLasTransferenciasDeUnEquipoExitosamente() {
        dadoQueSeObtieneElUsuarioDelTokenCorrectamente();
        dadoQueSeObtienenLasTransferenciasEnLasQueElUsuarioFueQuienLasInicio();
        dadoQueSeObtienenLasTransferenciasEnLasQueElUsuarioFueQuienLasRecibio();
        dadoQueSeCreaElDetalleDeTransferenciaCorrectamente();

        cuandoSeObtienenLasTransferenciasDeUnUsuario();

        seVerificaQueSeObtienenCorrectamente();
    }

    @Test
    public void seObtienenLasTransferenciasDeTodosLosEquiposExitosamente() {
        dadoQueSePuedenObtenerTodasLasTransferenciasConfirmadasDelRepositorio();
        dadoQueSeCreaElDetalleDeTransferenciaCorrectamente();

        cuandoSeObtienenLasTransferenciasConfirmadasDeTodosLosEquipos();

        seVerificaQueSeObtienenCorrectamente();
        seVerificaQueSeFiltroPorTransferenciasConfirmadas();
    }

    @Test
    public void alRealizarUnaTransferenciaSeGuardaJuntoConLosMontosDeLaTransferencia() throws JugadorYaTransferidoException {
        dadoUnRequestDeTransferenciaConMontos();
        dadoQueLaTransferenciaTieneUnaListaDeMontos();
        dadoQueSeObtieneElUsuarioDelTokenCorrectamente();
        dadoQueSeObtieneElEquipoQueRecibeLaTransferenciaCorrectamente();
        dadoQueSeObtieneElUsuarioDelEquipoQueRecibeLaTransferenciaCorrectamente();

        cuandoSeRealizaUnaTransferencia();

        seVerificaQueSeGuardaLaTransferencia();
        seVerificaQueSeGuardanLosBalances();
    }

    @Test
    public void seLanzaExcepcionAlRealizarUnaTransferenciaSiYaEstaTransferidoElJugador() throws JugadorYaTransferidoException {
        dadoUnRequestDeTransferenciaConJugadores();
        dadoQueLaTransferenciaTieneUnaListaDeJugadores();
        dadoQueElValidadorLanzaExcepcionDeJugadorYaTransferido();
        dadoQueSeObtieneElUsuarioDelTokenCorrectamente();
        dadoQueSeObtieneElEquipoQueRecibeLaTransferenciaCorrectamente();
        dadoQueSeObtieneElUsuarioDelEquipoQueRecibeLaTransferenciaCorrectamente();

        assertThrows(JugadorYaTransferidoException.class, this::cuandoSeRealizaUnaTransferencia);
    }

    @Test
    public void seRechazaUnaTransferenciaExitosamente() {
        dadoQueSeObtieneElUsuarioDelTokenCorrectamente();
        dadoQueExisteLaTransferenciaPendienteParaElUsuario();
        dadoQueSePuedeObtenerLaTransferencia();

        cuandoSeRechazaLaTransferencia();

        seVerificaQueSeActualizaElEstadoDeLaTransferenciaARechazada();
    }

    @Test
    public void seRechazaUnaTransferenciaExitosamenteSiendoElUsuarioQueRealizoLaOferta() {
        dadoQueSeObtieneElUsuarioDelTokenCorrectamente();
        dadoQueExisteLaTransferenciaPendienteParaElUsuarioQueRealizoLaOferta();
        dadoQueSePuedeObtenerLaTransferencia();

        cuandoSeRechazaLaTransferencia();

        seVerificaQueSeActualizaElEstadoDeLaTransferenciaARechazada();
    }

    @Test
    public void seConfirmaUnaTransferenciaExitosamenteConMovimientosPendientes() {
        dadoQueSeObtieneElUsuarioDelTokenCorrectamente();
        dadoQueExisteLaTransferenciaPendienteParaElUsuario();
        dadoQueSePuedeObtenerLaTemporadaActual();
        dadoQueSePuedenEjecutarLosMovimientosDeLosJugadoresParaLaTemporadaActual();
        dadoQueNoHayMovimientosFuturosDeJugadores();
        dadoQueSePuedeObtenerLaTransferencia();
        dadoQueSePuedeGuardarLaTransferencia();
        dadoQueSePuedenCancelarLasTransferenciasPendientesQueInvolucranAJugadoresDeLaTransferenciaActual();

        cuandoSeConfirmaLaTransferencia();

        seVerificaQueSeActualizaElEstadoDeLaTransferenciaAConfirmada();
        seVerificaQueSeEjecutenLosMovimientos();
        seVerificaQueSeCancelanLasTransferenciasPendientesQueInvolucranAJugadoresDeLaTransferenciaActual();
    }

    @Test
    public void seConfirmaUnaTransferenciaExitosamenteSinMovimientosPendientes() {
        dadoQueSeObtieneElUsuarioDelTokenCorrectamente();
        dadoQueExisteLaTransferenciaPendienteParaElUsuario();
        dadoQueSePuedeObtenerLaTemporadaActual();
        dadoQueExistenMovimientosDeJugadoresParaLaTemporadaActualDelUsuario();
        dadoQueNoHayMovimientosFuturosDeJugadores();
        dadoQueSePuedeObtenerLaTransferencia();
        dadoQueSePuedeGuardarLaTransferencia();
        dadoQueSePuedenCancelarLasTransferenciasPendientesQueInvolucranAJugadoresDeLaTransferenciaActual();

        cuandoSeConfirmaLaTransferencia();

        seVerificaQueSeActualizaElEstadoDeLaTransferenciaAConfirmada();
        seVerificaQueNoSeEjecutanMovimientos();
        seVerificaQueSeCancelanLasTransferenciasPendientesQueInvolucranAJugadoresDeLaTransferenciaActual();
    }

    @Test
    public void seObtieneElEstadoDelMercado() {
        dadoQueElRepositorioDevuelveElEstadoDelMercado();
        cuandoSeObtieneElEstadoDelMercado();
        seVerificaQueSeObtengaElEstadoDelMercado();
    }

    private void seVerificaQueSeObtengaElEstadoDelMercado() {
        verify(repositorioMercado, times(1)).findById(anyString());
    }

    private void cuandoSeObtieneElEstadoDelMercado() {
        servicioTransferencia.obtenerEstadoDeMercado();
    }

    private void dadoQueElRepositorioDevuelveElEstadoDelMercado() {
        Mercado mercado = new Mercado();
        mercado.setActivo(true);
        when(repositorioMercado.findById(CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA)).thenReturn(java.util.Optional.of(mercado));
    }

    private void dadoQueSePuedeGuardarLaTransferencia() {
        Transferencia transferencia = new Transferencia();
        transferencia.setMovimientos(Collections.emptyList());
        when(repositorioTransferencia.save(any())).thenReturn(transferencia);
    }

    private void dadoQueSePuedenCancelarLasTransferenciasPendientesQueInvolucranAJugadoresDeLaTransferenciaActual() {
        doNothing().when(repositorioTransferencia).cancelarTransferenciasQueInvolucrenALosJugadores(any());
    }

    private void dadoQueLaTransferenciaEsValida() throws JugadorYaTransferidoException {
        doNothing().when(validadorTransferencia).validarTraspasos(anyList());
    }

    private void dadoQueElValidadorLanzaExcepcionDeJugadorYaTransferido() throws JugadorYaTransferidoException {
        doThrow(JugadorYaTransferidoException.class).when(validadorTransferencia).validarTraspasos(anyList());
    }

    private void dadoQueNoHayMovimientosFuturosDeJugadores() {
        MovimientoJugador movimientoJugador = new MovimientoJugador();
        movimientoJugador.setNumeroTemporada(temporadaActual.getNumero());
        movimientoJugador.setPeriodo(periodoTemporadaActual);
        transferencia.setMovimientos(Lists.list(movimientoJugador));
        when(repositorioTransferencia.findById(anyInt())).thenReturn(java.util.Optional.of(transferencia));
    }

    private void dadoQueSePuedeObtenerElJugadorDelMovimiento() {
        Jugador jugador = new Jugador();
        Equipo equipo = new Equipo();
        equipo.setId(1L);
        jugador.setEquipo(equipo);
        when(repositorioJugador.findById(anyLong())).thenReturn(java.util.Optional.of(jugador));
    }

    private void dadoQueSePuedenGuardarLosMovimientos() {
        when(repositorioMovimientoJugador.save(any()))
                .thenReturn(movimientoJugadorTraspaso)
                .thenReturn(movimientoJugadorPrestamoPrincipio)
                .thenReturn(movimientoJugadorPrestamoFinal);
    }

    private void seVerificaQueSeGuardoElPrestamo() {
        verify(repositorioPrestamo, times(2)).save(any(PrestamoJugador.class));
    }

    private void dadoQueSePuedeGuardarElPrestamoCorrectamente() {
        Jugador jugador = new Jugador();
        jugador.setId(95L);
        when(servicioPlantel.obtenerJugadoresDuenioDePase(ID_EQUIPO_AFALP)).thenReturn(Collections.singletonList(jugador));
        when(repositorioPrestamo.save(any(PrestamoJugador.class))).thenReturn(new PrestamoJugador());
    }

    private void dadoQueLaTransferenciaTieneUnaListaConUnJugadorAPrestamo() {
        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(ID_PERIODO_1);

        Jugador jugador1 = new Jugador();
        jugador1.setId(ID_JUGADOR_1);
        JugadorPrestamoTransferencia jugadorPrestamoTransferencia = new JugadorPrestamoTransferencia();
        jugadorPrestamoTransferencia.setIdEquipoDestino(ID_EQUIPO_AFALP);
        jugadorPrestamoTransferencia.setIdEquipoOrigen(ID_EQUIPO_UNTREF);
        jugadorPrestamoTransferencia.setJugador(jugador1);
        jugadorPrestamoTransferencia.setNumeroTemporadaFinal(NUMERO_TEMPORADA_1);
        jugadorPrestamoTransferencia.setPeriodoFinal(periodoTemporada);
        jugadorPrestamoTransferencia.setNumeroTemporadaComienzo(NUMERO_TEMPORADA_2);
        jugadorPrestamoTransferencia.setPeriodoComienzo(periodoTemporada);

        List<JugadorPrestamoTransferencia> listaDeTranferenciasDeJugadoresAPrestamo = Lists.list(jugadorPrestamoTransferencia);

        requestCargaTransferencia.setJugadoresPrestamoTransferencia(listaDeTranferenciasDeJugadoresAPrestamo);
    }

    private void seVerificaQueSeFiltroPorTransferenciasConfirmadas() {
        verify(repositorioTransferencia, times(1)).findByEstadoTransferencia(estadoTransferenciaArgumentCaptor.capture(), any());
        EstadoTransferencia estadoTransferenciaPorElCualSeFiltra = estadoTransferenciaArgumentCaptor.getValue();
        assertThat(estadoTransferenciaPorElCualSeFiltra.getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_CONFIRMADA);
    }

    private void cuandoSeObtienenLasTransferenciasConfirmadasDeTodosLosEquipos() {
        resultadoObtenerTransferencias = servicioTransferencia.obtenerTransferenciasConfirmadasDeTodosLosEquipos(1);
    }

    private void dadoQueSePuedenObtenerTodasLasTransferenciasConfirmadasDelRepositorio() {
        Transferencia transferencia1 = new Transferencia();
        Transferencia transferencia2 = new Transferencia();
        List<Transferencia> transferencias = new ArrayList<>();
        transferencias.add(transferencia1);
        transferencias.add(transferencia2);
        Page<Transferencia> paginaConTransferencias = new PageImpl(transferencias);
        when(repositorioTransferencia.findByEstadoTransferencia(any(EstadoTransferencia.class), any())).thenReturn(paginaConTransferencias);
    }

    private void seVerificaQueNoSeEjecutanMovimientos() {
        verify(repositorioJugador, times(0)).save(any());
    }

    private void dadoQueExistenMovimientosDeJugadoresParaLaTemporadaActualDelUsuario() {
        when(repositorioMovimientoJugador.findByNumeroTemporadaYPeriodoId(anyInt(), anyInt())).thenReturn(Lists.emptyList());
    }

    private void seVerificaQueSeEjecutenLosMovimientos() {
        verify(repositorioJugador, times(1)).save(any());
    }

    private void dadoQueSePuedeObtenerLaTemporadaActual() {
        temporadaActual = new Temporada();
        temporadaActual.setId(1);
        temporadaActual.setNumero(2);
        periodoTemporadaActual = new PeriodoTemporada();
        periodoTemporadaActual.setId(1);
        temporadaActual.setPeriodoTemporada(periodoTemporadaActual);
        when(repositorioTemporada.findByActual(true)).thenReturn(Lists.list(temporadaActual));
    }

    private void dadoQueSePuedenEjecutarLosMovimientosDeLosJugadoresParaLaTemporadaActual() {
        MovimientoJugador movimientoJugador = new MovimientoJugador();
        Equipo equipoDestino = new Equipo();
        movimientoJugador.setEquipoDestino(equipoDestino);
        Jugador jugador = new Jugador();
        jugador.setId(1L);
        movimientoJugador.setJugador(jugador);
        Transferencia transferencia = new Transferencia();
        EstadoTransferencia estadoTransferencia = new EstadoTransferencia();
        estadoTransferencia.setId(ID_ESTADO_TRANSFERENCIA_CONFIRMADA);
        transferencia.setEstadoTransferencia(estadoTransferencia);
        movimientoJugador.setTransferencia(transferencia);
        TipoMovimiento tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setId(1);
        movimientoJugador.setTipoMovimiento(tipoMovimiento);

        when(repositorioMovimientoJugador.findByNumeroTemporadaYPeriodoId(anyInt(), anyInt())).thenReturn(Lists.list(movimientoJugador));

        when(repositorioJugador.findById(anyLong())).thenReturn(java.util.Optional.of(jugador));
        when(repositorioJugador.save(any())).thenReturn(new Jugador());
    }

    private void seVerificaQueSeActualizaElEstadoDeLaTransferenciaAConfirmada() {
        verify(repositorioTransferencia, times(1)).save(transferenciaArgumentCaptor.capture());
        Transferencia transferenciaGuardada = transferenciaArgumentCaptor.getValue();
        assertThat(transferenciaGuardada.getEstadoTransferencia().getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_CONFIRMADA);
    }

    private void cuandoSeConfirmaLaTransferencia() {
        servicioTransferencia.confirmarTransferencia(ID_TRANSFERENCIA_A_CONFIRMAR);
    }

    private void seVerificaQueSeActualizaElEstadoDeLaTransferenciaARechazada() {
        verify(repositorioTransferencia, times(1)).save(transferenciaArgumentCaptor.capture());
        Transferencia transferenciaGuardada = transferenciaArgumentCaptor.getValue();
        assertThat(transferenciaGuardada.getEstadoTransferencia().getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_CANCELADA);
    }

    private void cuandoSeRechazaLaTransferencia() {
        servicioTransferencia.rechazarTransferencia(ID_TRANSFERENCIA_A_RECHAZAR);
    }

    private void dadoQueExisteLaTransferenciaPendienteParaElUsuario() {
        transferencia = new Transferencia();
        EstadoTransferencia estadoTransferenciaPendiente = new EstadoTransferencia();
        estadoTransferenciaPendiente.setId(ID_ESTADO_TRANSFERENCIA_PENDIENTE);
        transferencia.setEstadoTransferencia(estadoTransferenciaPendiente);
        transferencia.setUsuarioDestino(usuarioQueHaceElPedido);

        Usuario otroUsuario = new Usuario();
        otroUsuario.setId(8L);
        transferencia.setUsuarioOrigen(otroUsuario);
    }

    private void dadoQueExisteLaTransferenciaPendienteParaElUsuarioQueRealizoLaOferta() {
        transferencia = new Transferencia();
        EstadoTransferencia estadoTransferenciaPendiente = new EstadoTransferencia();
        estadoTransferenciaPendiente.setId(ID_ESTADO_TRANSFERENCIA_PENDIENTE);
        transferencia.setEstadoTransferencia(estadoTransferenciaPendiente);
        transferencia.setUsuarioOrigen(usuarioQueHaceElPedido);
    }

    private void dadoQueSePuedeObtenerLaTransferencia() {
        when(repositorioTransferencia.findById(anyInt())).thenReturn(java.util.Optional.of(transferencia));
    }


    private void dadoQueSeObtienenLasTransferenciasEnLasQueElUsuarioFueQuienLasRecibio() {
        Transferencia transferencia = new Transferencia();
        transferencia.setId(2);
        when(repositorioTransferencia.findByUsuarioDestinoAndEstadoTransferenciaNoCancelado(usuarioQueHaceElPedido.getId())).thenReturn(Lists.list(transferencia));
    }

    private void seVerificaQueSeObtienenCorrectamente() {
        assertThat(resultadoObtenerTransferencias).hasSize(2);
    }

    private void cuandoSeObtienenLasTransferenciasDeUnUsuario() {
        resultadoObtenerTransferencias = servicioTransferencia.obtenerTransferenciasDeUnEquipo();
    }

    private void dadoQueSeCreaElDetalleDeTransferenciaCorrectamente() {
        DetalleTransferencia detalleTransferencia = new DetalleTransferencia();
        detalleTransferencia.setId(1);
        when(creadorDetalle.crearAPartirDe(any())).thenReturn(detalleTransferencia);
    }

    private void dadoQueSeObtienenLasTransferenciasEnLasQueElUsuarioFueQuienLasInicio() {
        Transferencia transferencia = new Transferencia();
        transferencia.setId(1);
        when(repositorioTransferencia.findByUsuarioOrigenAndEstadoTransferenciaNoCancelado(usuarioQueHaceElPedido.getId())).thenReturn(Lists.list(transferencia));
    }

    private void dadoQueSeObtieneElUsuarioDelEquipoQueRecibeLaTransferenciaCorrectamente() {
        Usuario usuario = new Usuario();
        usuario.setId(ID_USUARIO_QUE_RECIBE_LA_TRANSFERENCIA);
        when(repositorioUsuario.findById(ID_USUARIO_QUE_RECIBE_LA_TRANSFERENCIA)).thenReturn(java.util.Optional.of(usuario));
    }

    private void dadoQueSeObtieneElEquipoQueRecibeLaTransferenciaCorrectamente() {
        Equipo equipo = new Equipo();
        equipo.setIdUsuario(ID_USUARIO_QUE_RECIBE_LA_TRANSFERENCIA);
        when(repositorioEquipo.findById(OTRO_EQUIPO_ID)).thenReturn(java.util.Optional.of(equipo));
    }

    private void dadoQueSeObtieneElUsuarioDelTokenCorrectamente() {
        usuarioQueHaceElPedido = new Usuario();
        usuarioQueHaceElPedido.setId(ID_USUARIO_QUE_HACE_EL_PEDIDO);
        when(informacionPedido.obtenerUsuario()).thenReturn(usuarioQueHaceElPedido);
    }

    private void seVerificaQueSeGuardanLosBalances() {
        verify(repositorioBalance, times(2)).save(balanceArgumentCaptor.capture());
    }

    private void dadoQueLaTransferenciaTieneUnaListaDeMontos() {
        MontoTransferencia montoTransferencia1 = new MontoTransferencia();
        montoTransferencia1.setIdEquipoDestino(ID_EQUIPO_AFALP);
        montoTransferencia1.setIdEquipoOrigen(ID_EQUIPO_UNTREF);
        montoTransferencia1.setMonto(MONTO_BALANCE_1);
        montoTransferencia1.setNumeroTemporada(NUMERO_TEMPORADA_1);
        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(ID_PERIODO_1);
        montoTransferencia1.setPeriodo(periodoTemporada);

        List<MontoTransferencia> listaDeTranferenciasDeMontos = Lists.list(montoTransferencia1);
        requestCargaTransferencia.setMontosTransferencias(listaDeTranferenciasDeMontos);
        when(requestCargaTransferencia.getMontosTransferencias()).thenReturn(listaDeTranferenciasDeMontos);
    }

    private void seVerificaQueSeGuardanLosMovimientos() {
        verify(repositorioMovimientoJugador, times(3)).save(movimientoJugadorArgumentCaptor.capture());

    }

    private void seVerificaQueSeGuardaLaTransferencia() {
        verify(repositorioTransferencia, times(1)).save(transferenciaArgumentCaptor.capture());
        Transferencia transferenciaGuardada = transferenciaArgumentCaptor.getValue();
        assertThat(transferenciaGuardada.getEstadoTransferencia().getId()).isEqualTo(ESTADO_TRANSFERENCIA_PENDIENTE);
        assertThat(transferenciaGuardada.getUsuarioOrigen().getId()).isEqualTo(ID_USUARIO_QUE_HACE_EL_PEDIDO);
        assertThat(transferenciaGuardada.getUsuarioDestino().getId()).isEqualTo(ID_USUARIO_QUE_RECIBE_LA_TRANSFERENCIA);
    }

    private void cuandoSeRealizaUnaTransferencia() throws JugadorYaTransferidoException {
        servicioTransferencia.cargarTransferencia(requestCargaTransferencia);
    }

    private void dadoUnRequestDeTransferenciaConJugadores() {
        requestCargaTransferencia = mock(RequestCargaTransferencia.class);
        movimientoJugadorTraspaso = new MovimientoJugador();
        movimientoJugadorTraspaso.setNumeroTemporada(1);
        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(1);
        movimientoJugadorTraspaso.setPeriodo(periodoTemporada);
        Equipo equipo = new Equipo();
        equipo.setId(1L);
        movimientoJugadorTraspaso.setEquipoOrigen(equipo);
        Jugador jugador = new Jugador();
        jugador.setId(1L);
        jugador.setEquipo(equipo);
        movimientoJugadorTraspaso.setJugador(jugador);
        TipoMovimiento tipoMovimiento = new TipoMovimiento();
        tipoMovimiento.setId(ID_TIPO_TRANSFERENCIA_TRASPASO);
        movimientoJugadorTraspaso.setTipoMovimiento(tipoMovimiento);

        TipoMovimiento tipoMovimientoPrestamo = new TipoMovimiento();
        tipoMovimientoPrestamo.setId(ID_TIPO_TRANSFERENCIA_PRESTAMO);
        Equipo equipoOrigen = new Equipo();
        equipoOrigen.setId(1L);
        Equipo equipoDestino = new Equipo();
        equipoDestino.setId(2L);
        Jugador jugadorPrestamo = new Jugador();
        jugadorPrestamo.setId(1L);
        jugadorPrestamo.setEquipo(equipoOrigen);
        PeriodoTemporada periodoTemporadaPrestamo = new PeriodoTemporada();
        periodoTemporadaPrestamo.setId(1);

        movimientoJugadorPrestamoPrincipio = new MovimientoJugador();
        movimientoJugadorPrestamoPrincipio.setJugador(jugadorPrestamo);
        movimientoJugadorPrestamoPrincipio.setNumeroTemporada(1);
        movimientoJugadorPrestamoPrincipio.setPeriodo(periodoTemporadaPrestamo);
        movimientoJugadorPrestamoPrincipio.setEquipoOrigen(equipoOrigen);
        movimientoJugadorPrestamoPrincipio.setEquipoDestino(equipoDestino);
        movimientoJugadorPrestamoPrincipio.setTipoMovimiento(tipoMovimientoPrestamo);

        movimientoJugadorPrestamoFinal = new MovimientoJugador();
        movimientoJugadorPrestamoFinal.setJugador(jugadorPrestamo);
        movimientoJugadorPrestamoFinal.setNumeroTemporada(2);
        movimientoJugadorPrestamoFinal.setPeriodo(periodoTemporadaPrestamo);
        movimientoJugadorPrestamoFinal.setEquipoOrigen(equipoDestino);
        movimientoJugadorPrestamoFinal.setEquipoDestino(equipoOrigen);
        movimientoJugadorPrestamoFinal.setTipoMovimiento(tipoMovimientoPrestamo);

        when(requestCargaTransferencia.getJugadoresTransferencia()).thenReturn(Lists.list(new JugadorTransferencia()));
        when(requestCargaTransferencia.getOtroEquipoId()).thenReturn(OTRO_EQUIPO_ID);
    }

    private void dadoQueSeCreanLosMovimientosDeLosJugadoresCorrectamente() {
        when(requestCargaTransferencia.crearMovimientosJugadores()).thenReturn(Lists.list(movimientoJugadorTraspaso, movimientoJugadorPrestamoPrincipio, movimientoJugadorPrestamoFinal));
    }

    private void dadoUnRequestDeTransferenciaConMontos() {
        requestCargaTransferencia = mock(RequestCargaTransferencia.class);
        Balance balance1 = new Balance();
        Balance balance2 = new Balance();

        when(requestCargaTransferencia.getOtroEquipoId()).thenReturn(OTRO_EQUIPO_ID);
        when(requestCargaTransferencia.crearBalances()).thenReturn(Lists.list(balance1, balance2));
    }

    private void dadoQueLaTransferenciaTieneUnaListaDeJugadores() {
        PeriodoTemporada periodoTemporada = new PeriodoTemporada();
        periodoTemporada.setId(ID_PERIODO_1);

        Jugador jugador1 = new Jugador();
        jugador1.setId(ID_JUGADOR_1);
        JugadorTransferencia jugadorTransferencia1 = new JugadorTransferencia();
        jugadorTransferencia1.setIdEquipoDestino(ID_EQUIPO_AFALP);
        jugadorTransferencia1.setIdEquipoOrigen(ID_EQUIPO_UNTREF);
        jugadorTransferencia1.setJugador(jugador1);
        jugadorTransferencia1.setNumeroTemporada(NUMERO_TEMPORADA_1);
        jugadorTransferencia1.setPeriodo(periodoTemporada);

        List<JugadorTransferencia> listaDeTranferenciasDeJugadores = Lists.list(jugadorTransferencia1);

        requestCargaTransferencia.setJugadoresTransferencia(listaDeTranferenciasDeJugadores);
    }

    private void seVerificaQueSeCancelanLasTransferenciasPendientesQueInvolucranAJugadoresDeLaTransferenciaActual() {
        verify(repositorioTransferencia, times(1)).cancelarTransferenciasQueInvolucrenALosJugadores(any());
    }
}