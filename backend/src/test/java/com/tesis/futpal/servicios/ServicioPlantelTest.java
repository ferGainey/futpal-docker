package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipoEnum;
import com.tesis.futpal.modelos.plantel.EstadoJugador;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.plantel.JugadorFiltrado;
import com.tesis.futpal.modelos.plantel.JugadorPrestado;
import com.tesis.futpal.comunicacion.requests.RequestAgregarJugador;
import com.tesis.futpal.comunicacion.requests.RequestEdicionJugador;
import com.tesis.futpal.comunicacion.requests.RequestObtenerJugadoresEquipo;
import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.*;
import com.tesis.futpal.utilidades.InformacionDePedido;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServicioPlantelTest {

    private static final Long JUGADOR_ID = 1L;
    private Jugador jugador;
    private static final Long ID_ESTADO_JUGADOR_DISPONIBLE = 1L;
    private static final Long ID_USUARIO = 2L;
    private static final Long ID_ESTADO_JUGADOR_PRESTADO = 2L;
    private static final Long ID_ESTADO_JUGADOR_TRASPASADO = 4L;
    @Mock
    private RepositorioEquipo repositorioEquipo;
    @Mock
    private RepositorioJugador repositorioJugador;
    @Mock
    private RepositorioJugadoresPrestados repositorioJugadoresPrestados;
    @Mock
    private RepositorioJugadoresFiltrados repositorioJugadoresFiltrados;
    @Mock
    private RepositorioTemporada repositorioTemporada;
    @Mock
    private InformacionDePedido informacionDelPedido;
    @InjectMocks
    @Spy
    private ServicioPlantel servicioPlantel;

    private RequestAgregarJugador requestAgregarJugador;
    @Captor
    ArgumentCaptor<Jugador> jugadorAGuardar;
    private Equipo equipoJugador;
    private static final Long EQUIPO_ID = 1L;
    private List<Jugador> respuestaObtenerJugadoresEquipo;
    private Jugador respuestaDatosJugador;
    private Usuario usuario;
    private List<Jugador> respuestaObtenerJugadoresDuenioDePase;

    @Test
    public void seAgregaUnJugadorAUnEquipo() {
        dadoQueElEquipoExiste();
        dadoUnRequestDeUnJugadorAAgregar();
        dadoQueSePuedeGuardarElJugador();
        alAgregarUnJugador();
        seVerificaQueSeGuardoElJugador();
    }

    @Test
    public void seObtienenLosJugadoresDeUnEquipo() {
        dadoQueElEquipoExiste();
        dadoQueSePuedenObtenerLosJugadores();
        alConsultarLosJugadores();
        seVerificaQueSeObtienenLosJugadores();
    }

    @Test
    public void seBorraUnJugadorDeUnEquipoSiendoDuenioDelJugadorDisponible() {
        dadoQueElUsuarioEsDuenioDelJugador();
        dadoQueElUsuarioExiste();
        dadoQueElEquipoDelUsuarioExiste();
        dadoQueElUsuarioNoEsAdministrador();
        dadoQueElRepositorioPuedeBorrarElJugador();
        alBorrarUnJugador();
        seVerificaQueSeBorraAlJugador();
    }

    @Test
    public void seBorraUnJugadorDeUnEquipoSiendoAdministrador() {
        dadoQueElUsuarioExiste();
        dadoQueElUsuarioEsAdministrador();
        dadoQueElUsuarioNoEsDuenioDelJugador();
        dadoQueElRepositorioPuedeBorrarElJugador();
        alBorrarUnJugador();
        seVerificaQueSeBorraAlJugador();
    }

    @Test
    public void noSeBorraUnJugadorDeUnEquipoNoSiendoAdministradorNiDuenio() {
        dadoQueElUsuarioExiste();
        dadoQueElEquipoDelUsuarioExiste();
        dadoQueElUsuarioNoEsAdministrador();
        dadoQueElUsuarioNoEsDuenioDelJugador();
        alBorrarUnJugador();
        seVerificaQueNoSeBorraAlJugador();
    }

    @Test
    public void seEditaUnJugador() {
        dadoQueElJugadorExiste();
        dadoQueSePuedeEditarElJugador();
        alEditarUnJugador();
        seVerificaQueSeEditaAlJugador();
    }

    @Test
    public void seObtienenLosDatosDeUnJugador() {
        dadoQueElJugadorExiste();
        alObtenerLosDatosDeUnJugador();
        seVerificaQueLosDatosSonCorrectos();
    }

    @Test
    public void seObtienenLosJugadoresPrestados() {
        dadoQueElRepositorioDeJugadoresPrestadosDevuelveLosJugadoresCorrectamente();
        dadoQueSePuedeObtenerLaTemporadaActual();
        cuandoSeObtienenLosJugadoresPrestados();
        seVerificaQueSeRealizaLaConsulta();
    }

    @Test
    public void seObtienenLosJugadoresFiltrados() {
        dadoQueElRepositorioDeJugadoresFiltradosDevuelveLosJugadoresCorrectamente();
        cuandoSeObtienenLosJugadoresFiltrados();
        seVerificaQueSeRealizaLaConsultaDeJugadoresFiltrados();
    }

    @Test
    public void seObtienenLosJugadoresDeLosQueSeEsDuenioDelPase() {
        dadoQueAlObtenerLosJugadoresDeUnEquipoSeObtieneUnJugadorDisponibleUnoPrestadoYOtroTraspasado();
        dadoQueAlObtenerLosJugadoresDadosAPrestamoSeObtienenDosJugadores();
        alObtenerLosJugadoresDeLosQueSeEsDuenioDelPase();
        seVerificaQueSeObtienenSoloLosJugadoresDisponiblesYPrestadosAOtrosEquipos();
    }

    private void dadoQueAlObtenerLosJugadoresDadosAPrestamoSeObtienenDosJugadores() {
        EstadoJugador estadoJugadorPrestado = new EstadoJugador();
        estadoJugadorPrestado.setId(ID_ESTADO_JUGADOR_PRESTADO);
        LocalDate fechaDeNacimiento = LocalDate.now();

        Jugador jugador1 = new Jugador();
        jugador1.setEstadoJugador(estadoJugadorPrestado);
        jugador1.setFechaNacimiento(fechaDeNacimiento);
        JugadorPrestado jugadorPrestado1 = new JugadorPrestado();
        jugadorPrestado1.setJugador(jugador1);

        Jugador jugador2 = new Jugador();
        jugador2.setFechaNacimiento(fechaDeNacimiento);
        jugador2.setEstadoJugador(estadoJugadorPrestado);
        JugadorPrestado jugadorPrestado2 = new JugadorPrestado();
        jugadorPrestado2.setJugador(jugador2);

        Mockito.doReturn(Lists.list(jugadorPrestado1, jugadorPrestado2)).when(servicioPlantel).obtenerJugadoresPrestados(anyInt());
    }

    private void dadoQueAlObtenerLosJugadoresDeUnEquipoSeObtieneUnJugadorDisponibleUnoPrestadoYOtroTraspasado() {
        Equipo equipo = new Equipo();
        equipo.setId(1L);
        LocalDate fechaDeNacimiento = LocalDate.now();

        Jugador jugadorDisponible = new Jugador();
        EstadoJugador estadoJugadorDisponible = new EstadoJugador();
        estadoJugadorDisponible.setId(ID_ESTADO_JUGADOR_DISPONIBLE);
        jugadorDisponible.setEstadoJugador(estadoJugadorDisponible);
        jugadorDisponible.setFechaNacimiento(fechaDeNacimiento);
        jugadorDisponible.setActivo(true);

        Jugador jugadorPrestado = new Jugador();
        EstadoJugador estadoJugadorPrestado = new EstadoJugador();
        estadoJugadorPrestado.setId(ID_ESTADO_JUGADOR_PRESTADO);
        jugadorPrestado.setEstadoJugador(estadoJugadorPrestado);
        jugadorPrestado.setFechaNacimiento(fechaDeNacimiento);
        jugadorPrestado.setActivo(true);

        Jugador jugadorTraspasado = new Jugador();
        EstadoJugador estadoJugadorTraspasado = new EstadoJugador();
        estadoJugadorTraspasado.setId(ID_ESTADO_JUGADOR_TRASPASADO);
        jugadorTraspasado.setEstadoJugador(estadoJugadorTraspasado);
        jugadorTraspasado.setFechaNacimiento(fechaDeNacimiento);

        Mockito.doReturn(Lists.list(jugadorDisponible, jugadorPrestado, jugadorTraspasado)).when(servicioPlantel).obtenerJugadoresEquipo(any());
    }

    private void dadoQueSePuedeObtenerLaTemporadaActual() {
        Temporada temporada = new Temporada();
        temporada.setNumero(5);
        when(repositorioTemporada.findByActual(anyBoolean())).thenReturn(Collections.singletonList(temporada));
    }

    private void dadoQueElRepositorioDeJugadoresFiltradosDevuelveLosJugadoresCorrectamente() {
        when(repositorioJugadoresFiltrados.obtenerJugadoresFiltrados(anyInt(), anyString())).thenReturn(Collections.singletonList(new JugadorFiltrado()));
    }

    private void dadoQueElRepositorioDeJugadoresPrestadosDevuelveLosJugadoresCorrectamente() {
        when(repositorioJugadoresPrestados.obtenerJugadoresPrestados(anyInt(), anyInt())).thenReturn(Collections.singletonList(new JugadorPrestado()));
    }

    private void dadoQueElUsuarioEsAdministrador() {
        when(usuario.esAdministrador()).thenReturn(true);
    }

    private void dadoQueElUsuarioNoEsDuenioDelJugador() {
        Long ID_ESTADO_DISPONIBLE = 1L;

        jugador = new Jugador();
        jugador.setId(JUGADOR_ID);
        Equipo equipo = new Equipo();
        equipo.setId(99999L);
        jugador.setEquipo(equipo);
        EstadoJugador estadoNoDisponible = new EstadoJugador();
        estadoNoDisponible.setId(ID_ESTADO_DISPONIBLE);
        jugador.setEstadoJugador(estadoNoDisponible);

        when(repositorioJugador.findById(jugador.getId())).thenReturn(java.util.Optional.of(jugador));
    }

    private void dadoQueElUsuarioEsDuenioDelJugador() {
        Long ID_ESTADO_DISPONIBLE = 1L;

        jugador = new Jugador();
        jugador.setId(JUGADOR_ID);

        EstadoJugador estadoDisponible = new EstadoJugador();
        estadoDisponible.setId(ID_ESTADO_DISPONIBLE);
        jugador.setEstadoJugador(estadoDisponible);

        Equipo equipo = new Equipo();
        equipo.setId(4L);
        jugador.setEquipo(equipo);

        when(repositorioJugador.findById(anyLong())).thenReturn(java.util.Optional.of(jugador));
    }

    private void dadoQueElUsuarioNoEsAdministrador() {
        when(usuario.esAdministrador()).thenReturn(false);
    }

    private void dadoQueElUsuarioExiste() {
        usuario = mock(Usuario.class);
        when(usuario.getId()).thenReturn(ID_USUARIO);
        when(informacionDelPedido.obtenerUsuario()).thenReturn(usuario);
    }

    private void seVerificaQueLosDatosSonCorrectos() {
        assertThat(respuestaDatosJugador.getNombre()).isEqualTo("Cristiano Ronaldo");
        assertThat(respuestaDatosJugador.getFechaNacimiento().getYear()).isEqualTo(1985);
        assertThat(respuestaDatosJugador.getMedia()).isEqualTo(93);
        assertThat(respuestaDatosJugador.getSalario()).isEqualTo(20000000);
        assertThat(respuestaDatosJugador.getValor()).isEqualTo(75000000);
    }

    private void alObtenerLosJugadoresDeLosQueSeEsDuenioDelPase() {
        respuestaObtenerJugadoresDuenioDePase = servicioPlantel.obtenerJugadoresDuenioDePase(EQUIPO_ID.intValue());
    }

    private void alObtenerLosDatosDeUnJugador() {
        respuestaDatosJugador = servicioPlantel.obtenerDatosJugador(JUGADOR_ID);
    }

    private void cuandoSeObtienenLosJugadoresFiltrados() {
        servicioPlantel.obtenerJugadoresFiltrados(123456, "reus");
    }

    private void cuandoSeObtienenLosJugadoresPrestados() {
        servicioPlantel.obtenerJugadoresPrestados(3);
    }

    private void dadoQueElJugadorExiste() {
        Jugador jugador = new Jugador();
        jugador.setNombre("Cristiano Ronaldo");
        jugador.setFechaNacimiento(LocalDate.of(1985,1,1));
        jugador.setMedia(93);
        jugador.setSalario(20000000);
        jugador.setValor(75000000);
        when(repositorioJugador.findById(anyLong())).thenReturn(java.util.Optional.of(jugador));
    }

    private void seVerificaQueSeEditaAlJugador() {
        verify(repositorioJugador, times(1)).save(jugadorAGuardar.capture());
        Jugador jugadorGuardado = jugadorAGuardar.getValue();
        assertThat(jugadorGuardado.getNombre()).isEqualTo("Cristiano Ronaldo");
        assertThat(jugadorGuardado.getFechaNacimiento().getYear()).isEqualTo(1985);
        assertThat(jugadorGuardado.getMedia()).isEqualTo(90);
        assertThat(jugadorGuardado.getSalario()).isEqualTo(100000);
        assertThat(jugadorGuardado.getValor()).isEqualTo(500000);
        assertThat(jugadorGuardado.getIdTransfermarkt()).isEqualTo(567L);
    }

    private void alEditarUnJugador() {
        RequestEdicionJugador requestEdicionJugador = new RequestEdicionJugador();
        requestEdicionJugador.setNombre("Cristiano Ronaldo");
        requestEdicionJugador.setFechaNacimiento(LocalDate.of(1985, 1, 1));
        requestEdicionJugador.setMedia(90);
        requestEdicionJugador.setSalario(100000);
        requestEdicionJugador.setValor(500000);
        requestEdicionJugador.setJugadorId(JUGADOR_ID);
        requestEdicionJugador.setIdTransfermarkt(567L);
        servicioPlantel.editarJugador(requestEdicionJugador);
    }

    private void dadoQueSePuedeEditarElJugador() {
        when(repositorioJugador.save(any(Jugador.class))).thenReturn(new Jugador());
    }

    private void seVerificaQueSeBorraAlJugador() {
        verify(repositorioJugador, times(1)).save(jugador);
    }

    private void seVerificaQueNoSeBorraAlJugador() {
        verify(repositorioJugador, times(0)).deleteById(JUGADOR_ID);
    }

    private void alBorrarUnJugador() {
        servicioPlantel.borrarJugador(JUGADOR_ID);
    }

    private void dadoQueElRepositorioPuedeBorrarElJugador() {
        when(repositorioJugador.save(jugador)).thenReturn(jugador);
    }

    private void seVerificaQueSeObtienenLosJugadores() {
        List<Jugador> jugadores = respuestaObtenerJugadoresEquipo;
        assertThat(jugadores.get(0).getNombre()).isEqualTo("Leo Ponzio");
        assertThat(jugadores.get(1).getNombre()).isEqualTo("Lucas Alario");
    }

    private void alConsultarLosJugadores() {
        RequestObtenerJugadoresEquipo requestObtenerJugadoresEquipo = new RequestObtenerJugadoresEquipo();
        requestObtenerJugadoresEquipo.setEquipoId(EQUIPO_ID);
        respuestaObtenerJugadoresEquipo = servicioPlantel.obtenerJugadoresEquipo(requestObtenerJugadoresEquipo);
    }

    private void dadoQueSePuedenObtenerLosJugadores() {
        Jugador jugador1 = new Jugador();
        jugador1.setNombre("Leo Ponzio");
        jugador1.setFechaNacimiento(LocalDate.now());
        jugador1.setActivo(true);
        Jugador jugador2 = new Jugador();
        jugador2.setNombre("Lucas Alario");
        jugador2.setFechaNacimiento(LocalDate.now());
        jugador2.setActivo(true);
        Jugador jugador3 = new Jugador();
        jugador3.setNombre("Lucas Beltran");
        jugador3.setFechaNacimiento(LocalDate.now());
        jugador3.setActivo(false);
        Equipo equipo = new Equipo();
        equipo.setJugadores(Lists.list(jugador1, jugador2, jugador3));

        when(repositorioEquipo.findById(anyLong())).thenReturn(java.util.Optional.of(equipo));
    }

    private void dadoQueSePuedeGuardarElJugador() {
        when(repositorioJugador.save(any(Jugador.class))).thenReturn(new Jugador());
    }

    private void alAgregarUnJugador() {
        servicioPlantel.agregarJugador(requestAgregarJugador);
    }

    private void seVerificaQueSeGuardoElJugador() {
        verify(repositorioJugador, times(1)).save(jugadorAGuardar.capture());
        Jugador jugadorGuardado = jugadorAGuardar.getValue();
        assertThat(jugadorGuardado.getNombre()).isEqualTo("Cristiano Ronaldo");
        assertThat(jugadorGuardado.getFechaNacimiento().getYear()).isEqualTo(1985);
        assertThat(jugadorGuardado.getMedia()).isEqualTo(93);
        assertThat(jugadorGuardado.getSalario()).isEqualTo(20000000);
        assertThat(jugadorGuardado.getValor()).isEqualTo(75000000);
        assertThat(jugadorGuardado.getEquipo().getNombreEquipo()).isEqualTo("River Plate");
        assertThat(jugadorGuardado.getEstadoJugador().getId()).isEqualTo(ID_ESTADO_JUGADOR_DISPONIBLE);
        assertThat(jugadorGuardado.getIdTransfermarkt()).isEqualTo(345L);
    }

    private void dadoUnRequestDeUnJugadorAAgregar() {
        requestAgregarJugador = new RequestAgregarJugador();
        requestAgregarJugador.setNombre("Cristiano Ronaldo");
        requestAgregarJugador.setFechaNacimiento(LocalDate.of(1985, 1, 1));
        requestAgregarJugador.setMedia(93);
        requestAgregarJugador.setSalario(20000000);
        requestAgregarJugador.setValor(75000000);
        requestAgregarJugador.setEquipoId(1L);
        requestAgregarJugador.setIdTransfermarkt(345L);
    }

    private void dadoQueElEquipoExiste() {
        crearEquipo();
        when(repositorioEquipo.findById(anyLong())).thenReturn(java.util.Optional.of(equipoJugador));
    }

    private void dadoQueElEquipoDelUsuarioExiste() {
        crearEquipo();
        equipoJugador.setId(4L);
        when(repositorioEquipo.findByIdUsuario(ID_USUARIO)).thenReturn(Collections.singletonList(equipoJugador));
    }

    private void crearEquipo() {
        equipoJugador = new Equipo();
        equipoJugador.setIdUsuario(ID_USUARIO);
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());
        equipoJugador.setEstadoEquipo(estadoEquipo);
        equipoJugador.setNombreEquipo("River Plate");
    }

    private void seVerificaQueSeRealizaLaConsulta() {
        verify(repositorioJugadoresPrestados, times(1)).obtenerJugadoresPrestados(anyInt(), anyInt());
    }

    private void seVerificaQueSeRealizaLaConsultaDeJugadoresFiltrados() {
        verify(repositorioJugadoresFiltrados, times(1)).obtenerJugadoresFiltrados(anyInt(), anyString());
    }

    private void seVerificaQueSeObtienenSoloLosJugadoresDisponiblesYPrestadosAOtrosEquipos() {
        assertThat(respuestaObtenerJugadoresDuenioDePase.size()).isEqualTo(3);
    }
}
