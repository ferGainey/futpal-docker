package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.repositorios.RepositorioPeriodoTemporada;
import com.tesis.futpal.repositorios.RepositorioTemporada;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServicioTemporadaTest {

    private static final String MITAD_TEMPORADA = "Mitad";
    private Temporada resultado;

    @Mock
    private RepositorioTemporada repositorioTemporada;
    @Mock
    private RepositorioPeriodoTemporada repositorioPeriodoTemporada;
    @Mock
    private ServicioTransferencia servicioTransferencia;
    @InjectMocks
    private ServicioTemporada servicioTemporada;

    @Test
    public void seObtieneLaTemporadaActualDeManeraExitosa() {
        dadoQueSeEstaAMitadDeLaTemporada2();

        cuandoSeObtieneLaTemporadaActual();

        seVerificaQueSeDevuelveLaTemporadaConPeriodo(2, MITAD_TEMPORADA);
    }

    @Test
    public void seEjecutanLosMovimientosAlAvanzarDeTemporadaYSeCancelanTransferenciasPendientes() {
        dadoQueSeEstaAMitadDeLaTemporada2();
        dadoQueSePuedeObtenerElProximoPeriodo();
        dadoQueSePuedeGuardarLaTemporadaActualViejaYLaNueva();
        dadoQueSePuedenCancelarLasTransferenciasPendientes();

        cuandoSeAvanzaDeTemporada();

        seVerificaQueSeEjecutanLosMovimientos();
        seVerificaQueSeCancelanLasTransferenciasPendientes();
    }

    private void dadoQueSePuedenCancelarLasTransferenciasPendientes() {
        doNothing().when(servicioTransferencia).cancelarTransferenciasPendientes();
    }

    private void seVerificaQueSeEjecutanLosMovimientos() {
        verify(servicioTransferencia, times(1)).ejecutarMovimientos(any(Temporada.class));
    }

    private void cuandoSeAvanzaDeTemporada() {
        servicioTemporada.avanzarTemporada();
    }

    private void dadoQueSePuedeGuardarLaTemporadaActualViejaYLaNueva() {
        when(repositorioTemporada.save(any())).thenReturn(new Temporada());
    }

    private void dadoQueSePuedeObtenerElProximoPeriodo() {
        when(repositorioPeriodoTemporada.findById(anyInt())).thenReturn(java.util.Optional.of(new PeriodoTemporada()));
    }

    private void dadoQueSeEstaAMitadDeLaTemporada2() {
        Temporada temporada = new Temporada();
        temporada.setNumero(2);
        PeriodoTemporada mitadTemporada = new PeriodoTemporada();
        mitadTemporada.setId(2);
        mitadTemporada.setNombre("Mitad");
        mitadTemporada.setProximaId(3);
        temporada.setPeriodoTemporada(mitadTemporada);
        when(repositorioTemporada.findByActual(true)).thenReturn(Collections.singletonList(temporada));
    }

    private void cuandoSeObtieneLaTemporadaActual() {
        resultado = servicioTemporada.obtenerTemporadaActual();
    }

    private void seVerificaQueSeDevuelveLaTemporadaConPeriodo(int numeroTemporada, String nombrePeriodo) {
        assertThat(resultado.getNumero()).isEqualTo(numeroTemporada);
        assertThat(resultado.getPeriodoTemporada().getNombre()).isEqualTo(nombrePeriodo);
    }


    private void seVerificaQueSeCancelanLasTransferenciasPendientes() {
        verify(servicioTransferencia, times(1)).cancelarTransferenciasPendientes();
    }
}
