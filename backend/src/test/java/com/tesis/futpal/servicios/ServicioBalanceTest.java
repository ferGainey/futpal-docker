package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.modelos.balance.CalculadoraDetalleDeBalance;
import com.tesis.futpal.modelos.balance.DetalleBalance;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.repositorios.RepositorioBalance;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServicioBalanceTest {

    private static final Integer NUMERO_TEMPORADA_DETALLE_BALANCE_SOLICITADA = 2;
    private static final Integer EQUIPO_ID = 1;
    private Balance balance;
    @Mock
    private RepositorioBalance repositorioBalance;
    @Mock
    private RepositorioEquipo repositorioEquipo;
    @Mock
    private CalculadoraDetalleDeBalance calculadoraDetalleDeBalance;
    @InjectMocks
    private ServicioBalance servicioBalance;
    private List<Balance> balances;
    private List<DetalleBalance> resultadoDetalleBalancesTodosLosEquipos;
    private Equipo equipo1;
    private DetalleBalance respuestaDetalleBalanceParaUnEquipo;
    private List<Balance> resultadoObtenerBalancesDeUnEquipoEnTemporada;

    @Test
    public void seCargaUnBalanceParaUnEquipoExitosamente() {
        dadoUnBalanceValido();

        cuandoSeCargaUnBalanceParaUnEquipo();

        seVerificaQueSeGuardaElBalance();
    }

    @Test
    public void seCargaUnBalanceParaUnaLigaExitosamente() {
        dadoUnaCargaDeBalancesParaUnaLigaValido();

        cuandoSeCargaUnBalanceParaUnaLiga();

        seVerificaQueSeGuardanLosBalances();
    }

    @Test
    public void seObtienenLosDetalleDeBalanceDeTodosLosEquiposExitosamente() {
        dadoQueSePuedenObtenerLosBalancesCuyasTemporadasSeanMenorOIgualALaIndicadaParaUnEquipo();
        dadoQueExisten2Equipos();
        dadoQueLaCalculadoraDeDetalleDeBalanceFuncionaCorrectamente();

        cuandoSeObtieneLosDetalleDeBalanceDeTodosLosEquipos();

        seVerificaQueSeObtienenLosDetallesParaLos2Equipos();
    }

    @Test
    public void seObtienenLosDetalleDeBalanceDeUnEquipoExitosamente() {
        dadoQueSePuedenObtenerLosBalancesCuyasTemporadasSeanMenorOIgualALaIndicadaParaUnEquipo();
        dadoQueSePuedeObtenerElEquipoBuscado();
        dadoQueLaCalculadoraDeDetalleDeBalanceFuncionaCorrectamente();

        cuandoSeObtieneLosDetalleDeBalanceDeUnEquipo();

        seVerificaQueSeObtienenLosDetallesParaUnEquipo();
    }

    @Test
    public void seObtienenLosBalancesDeUnEquipoParaUnaTemporadaExitosamente() {
        dadoQueSePuedenObtenerLosBalancesEnUnaTemporadaParaUnEquipo();
        dadoQueSePuedeObtenerElEquipoBuscado();

        seObtienenLosBalancesDeUnEquipoParaUnaTemporada();

        seVerificaQueSeObtienenLosBalancesDeUnEquipoParaUnaTemporada();
    }

    private void dadoQueSePuedenObtenerLosBalancesEnUnaTemporadaParaUnEquipo() {
        Balance balance = new Balance();
        when(repositorioBalance.findByNumeroTemporadaAndEquipo(anyInt(), any())).thenReturn(Collections.singletonList(balance));
    }

    private void seVerificaQueSeObtienenLosBalancesDeUnEquipoParaUnaTemporada() {
        assertThat(resultadoObtenerBalancesDeUnEquipoEnTemporada.get(0).getClass()).isEqualTo(Balance.class);
    }

    private void seObtienenLosBalancesDeUnEquipoParaUnaTemporada() {
        resultadoObtenerBalancesDeUnEquipoEnTemporada = servicioBalance.obtenerBalancesDeUnEquipoEnTemporada(NUMERO_TEMPORADA_DETALLE_BALANCE_SOLICITADA, EQUIPO_ID);
    }

    private void dadoQueSePuedeObtenerElEquipoBuscado() {
        equipo1 = new Equipo();

        when(repositorioEquipo.findById(EQUIPO_ID.longValue())).thenReturn(java.util.Optional.ofNullable(equipo1));
    }

    private void seVerificaQueSeObtienenLosDetallesParaUnEquipo() {
        assertThat(respuestaDetalleBalanceParaUnEquipo.getClass()).isEqualTo(DetalleBalance.class);
    }

    private void cuandoSeObtieneLosDetalleDeBalanceDeUnEquipo() {
        respuestaDetalleBalanceParaUnEquipo = servicioBalance.obtenerDetalleBalanceDeUnEquipo(NUMERO_TEMPORADA_DETALLE_BALANCE_SOLICITADA, EQUIPO_ID);
    }

    private void seVerificaQueSeObtienenLosDetallesParaLos2Equipos() {
        assertThat(resultadoDetalleBalancesTodosLosEquipos.size()).isEqualTo(2);
    }

    private void cuandoSeObtieneLosDetalleDeBalanceDeTodosLosEquipos() {
        resultadoDetalleBalancesTodosLosEquipos = servicioBalance.obtenerDetalleBalanceDeTodosLosEquipos(NUMERO_TEMPORADA_DETALLE_BALANCE_SOLICITADA);
    }

    private void dadoQueLaCalculadoraDeDetalleDeBalanceFuncionaCorrectamente() {
        DetalleBalance detalleBalance = new DetalleBalance();
        equipo1.setNombreEquipo("UnNombreDeEquipo");
        detalleBalance.setEquipo(equipo1);
        when(calculadoraDetalleDeBalance.calcular(any(Equipo.class), anyList(), anyInt())).thenReturn(detalleBalance);
    }

    private void dadoQueExisten2Equipos() {
        equipo1 = new Equipo();
        Equipo equipo2 = new Equipo();

        when(repositorioEquipo.findAll()).thenReturn(Lists.list(equipo1, equipo2));
    }

    private void dadoQueSePuedenObtenerLosBalancesCuyasTemporadasSeanMenorOIgualALaIndicadaParaUnEquipo() {
        Balance balance = new Balance();
        when(repositorioBalance.findByNumeroTemporadaLessThanEqualAndEquipo(anyInt(), any())).thenReturn(Collections.singletonList(balance));
    }

    private void seVerificaQueSeGuardanLosBalances() {
        verify(repositorioBalance, times(1)).saveAll(anyList());
    }

    private void cuandoSeCargaUnBalanceParaUnaLiga() {
        servicioBalance.cargarBalanceParaUnaLiga(balances);
    }

    private void dadoUnaCargaDeBalancesParaUnaLigaValido() {
        balances = Collections.singletonList(new Balance());
    }

    private void seVerificaQueSeGuardaElBalance() {
        verify(repositorioBalance, times(1)).save(any(Balance.class));
    }

    private void cuandoSeCargaUnBalanceParaUnEquipo() {
        servicioBalance.cargarBalance(balance);
    }

    private void dadoUnBalanceValido() {
        balance = new Balance();
    }
}
