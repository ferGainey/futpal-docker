package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.EstadoEquipoEnum;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipo;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.excepciones.UsuarioInexistenteException;
import com.tesis.futpal.comunicacion.requests.RequestEditarDatosEquipo;
import com.tesis.futpal.comunicacion.responses.ResponseDatosEquipo;
import com.tesis.futpal.comunicacion.responses.ResponseEquipoActualizado;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServicioEdicionEquipoTest {

    @Mock
    private RepositorioUsuario repositorioUsuario;
    @Mock
    private RepositorioEquipo repositorioEquipo;
    @InjectMocks
    private ServicioEdicionEquipo servicioEdicionEquipo;
    private ResponseDatosEquipo resultadoDatosEquipo;
    private ResponseEquipoActualizado resultadoEquipoActualizado;
    private static final Long ID_USUARIO = 1L;
    private static final Long ID_EQUIPO = 2L;
    private static final String NOMBRE_EQUIPO = "Gainey FC";
    private static final String MENSAJE_USUARIO_INEXISTENTE = "El id ingresado no está vinculado a ningún usuario.";
    private static final String NUEVO_NOMBRE_EQUIPO = "Afalp";

    @Test
    public void obtenerDatosDelEquipoSinTenerUnEquipoAsociado() throws UsuarioInexistenteException {
        dadoQueElUsuarioExiste();
        dadoQueNoTieneEquipoAsociado();
        alObtenerLosDatosDelEquipo();
        seVerificaQueLosDatosDelEquipoEstanVacios();
        seVerificaQueHayUnMensajeNotificandoQueNoTieneEquipo();
    }

    @Test
    public void obtenerDatosDelEquipoTeniendoUnEquipoAsociado() throws UsuarioInexistenteException {
        dadoQueElUsuarioExiste();
        dadoQueTieneEquipoAsociado();
        alObtenerLosDatosDelEquipo();
        seVerificaQueLosDatosDelEquipoEstanCorrectos();
        seVerificaQueNoHayUnMensajeNotificandoQueNoTieneEquipo();
    }

    @Test
    public void obtenerDatosDelEquipoTeniendoUnEquipoAsociadoRechazado() throws UsuarioInexistenteException {
        dadoQueElUsuarioExiste();
        dadoQueTieneEquipoAsociadoRechazado();
        alObtenerLosDatosDelEquipo();
        seVerificaQueLosDatosDelEquipoRechazadoEstanCorrectos();
    }

    @Test
    public void obtenerDatosDelEquipoTeniendoUnEquipoEnEstadoInicial() throws UsuarioInexistenteException {
        dadoQueElUsuarioExiste();
        dadoQueTieneEquipoAsociadoEnEstadoInicial();
        alObtenerLosDatosDelEquipo();
        seVerificaQueLosDatosDelEquipoEnEstadoInicialEstanCorrectos();
    }

    @Test
    public void obtenerDatosDelEquipoConUsuarioInexistenteLanzaExcepcion() {
        dadoQueElUsuarioNoExiste();
        alObtenerLosDatosDelEquipoSeLanzaExcepcionUsuarioInexistenteException();
        yNoSeObtienenLosDatosDelEquipo();
    }

    @Test
    public void actualizarDatosDelEquipoSinTenerUnEquipoAsociado() throws UsuarioInexistenteException {
        dadoQueElUsuarioExiste();
        dadoQueNoTieneEquipoAsociado();
        dadoQueElEquipoSeGuardaExitosamente();
        alActualizarLosDatosDelEquipo(ID_USUARIO, NUEVO_NOMBRE_EQUIPO);
        seVerificaQueSeActualizoElEquipoEnElRepositorioDeEquipos();
        seVerificaQueSeDevuelveElNuevoNombreDelEquipo();
    }

    @Test
    public void actualizarDatosDelEquipoTeniendoUnEquipoAsociado() throws UsuarioInexistenteException {
        dadoQueElUsuarioExiste();
        dadoQueTieneEquipoAsociado();
        dadoQueElEquipoSeGuardaExitosamente();
        alActualizarLosDatosDelEquipo(ID_USUARIO, NUEVO_NOMBRE_EQUIPO);
        seVerificaQueSeActualizoElEquipoEnElRepositorioDeEquipos();
        seVerificaQueSeDevuelveElNuevoNombreDelEquipo();
    }

    private void seVerificaQueLosDatosDelEquipoEnEstadoInicialEstanCorrectos() {
        assertThat(resultadoDatosEquipo.getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO);
        assertThat(resultadoDatosEquipo.getIdEquipo()).isEqualTo(ID_EQUIPO);
        assertThat(resultadoDatosEquipo.getEstadoEquipoId()).isEqualTo(EstadoEquipoEnum.INICIAL.getId());
        assertThat(resultadoDatosEquipo.getMensajeEstadoEquipo()).isEqualTo(EstadoEquipoEnum.INICIAL.getDescripcion());
        assertThat(resultadoDatosEquipo.getMotivoCambioEstado()).isEqualTo("");
    }

    private void dadoQueTieneEquipoAsociadoEnEstadoInicial() {
        Equipo equipoBuscado = new Equipo();
        equipoBuscado.setId(ID_EQUIPO);
        equipoBuscado.setNombreEquipo(NOMBRE_EQUIPO);
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.INICIAL.getId());
        estadoEquipo.setDescripcion(EstadoEquipoEnum.INICIAL.getDescripcion());
        equipoBuscado.setEstadoEquipo(estadoEquipo);
        equipoBuscado.setMotivoUltimoCambioEstado("");
        when(repositorioEquipo.findByIdUsuario(ID_USUARIO)).thenReturn(Collections.singletonList(equipoBuscado));
    }

    private void dadoQueElEquipoSeGuardaExitosamente() {
        Equipo equipo = new Equipo();
        equipo.setNombreEquipo(NUEVO_NOMBRE_EQUIPO);
        equipo.setId(ID_EQUIPO);
        when(repositorioEquipo.save(any())).thenReturn(equipo);
    }

    private void seVerificaQueSeDevuelveElNuevoNombreDelEquipo() {
        assertThat(resultadoEquipoActualizado.getNombreEquipoActualizado()).isEqualTo(NUEVO_NOMBRE_EQUIPO);
    }

    private void seVerificaQueSeActualizoElEquipoEnElRepositorioDeEquipos() {
        verify(repositorioEquipo, times(1)).save(any(Equipo.class));
    }

    private void alActualizarLosDatosDelEquipo(Long idUsuario, String nuevoNombreEquipo) throws UsuarioInexistenteException {
        RequestEditarDatosEquipo requestEditarDatosEquipo = new RequestEditarDatosEquipo();
        requestEditarDatosEquipo.setIdUsuario(idUsuario);
        requestEditarDatosEquipo.setNuevoNombreEquipo(nuevoNombreEquipo);
        resultadoEquipoActualizado = servicioEdicionEquipo.actualizarEquipo(requestEditarDatosEquipo);
    }

    private void yNoSeObtienenLosDatosDelEquipo() {
        verify(repositorioEquipo, times(0)).findByIdUsuario(any());
    }

    private void alObtenerLosDatosDelEquipoSeLanzaExcepcionUsuarioInexistenteException() {
        UsuarioInexistenteException exception = assertThrows(
                UsuarioInexistenteException.class, () -> {
                    servicioEdicionEquipo.obtenerDatosEquipo(ID_USUARIO);
                });
        assertThat(exception.getMessage()).isEqualTo(MENSAJE_USUARIO_INEXISTENTE);
    }

    private void dadoQueElUsuarioNoExiste() {
        when(repositorioUsuario.findById(ID_USUARIO)).thenReturn(Optional.empty());
    }

    private void seVerificaQueNoHayUnMensajeNotificandoQueNoTieneEquipo() {
        assertThat(resultadoDatosEquipo.getMensaje()).isEqualTo("");
    }

    private void seVerificaQueLosDatosDelEquipoRechazadoEstanCorrectos() {
        assertThat(resultadoDatosEquipo.getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO);
        assertThat(resultadoDatosEquipo.getIdEquipo()).isEqualTo(ID_EQUIPO);
        assertThat(resultadoDatosEquipo.getEstadoEquipoId()).isEqualTo(EstadoEquipoEnum.RECHAZADO.getId());
        assertThat(resultadoDatosEquipo.getMensajeEstadoEquipo()).isEqualTo(EstadoEquipoEnum.RECHAZADO.getDescripcion());
        assertThat(resultadoDatosEquipo.getMotivoCambioEstado()).isEqualTo("");
    }

    private void seVerificaQueLosDatosDelEquipoEstanCorrectos() {
        assertThat(resultadoDatosEquipo.getNombreEquipo()).isEqualTo(NOMBRE_EQUIPO);
        assertThat(resultadoDatosEquipo.getIdEquipo()).isEqualTo(ID_EQUIPO);
        assertThat(resultadoDatosEquipo.getEstadoEquipoId()).isEqualTo(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());
        assertThat(resultadoDatosEquipo.getMensajeEstadoEquipo()).isEqualTo(EstadoEquipoEnum.PENDIENTE_APROBACION.getDescripcion());
    }

    private void dadoQueTieneEquipoAsociadoRechazado() {
        Equipo equipoBuscado = new Equipo();
        equipoBuscado.setId(ID_EQUIPO);
        equipoBuscado.setNombreEquipo(NOMBRE_EQUIPO);
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.RECHAZADO.getId());
        estadoEquipo.setDescripcion(EstadoEquipoEnum.RECHAZADO.getDescripcion());
        equipoBuscado.setEstadoEquipo(estadoEquipo);
        equipoBuscado.setMotivoUltimoCambioEstado("");
        when(repositorioEquipo.findByIdUsuario(ID_USUARIO)).thenReturn(Collections.singletonList(equipoBuscado));
    }

    private void dadoQueTieneEquipoAsociado() {
        Equipo equipoBuscado = new Equipo();
        equipoBuscado.setId(ID_EQUIPO);
        equipoBuscado.setNombreEquipo(NOMBRE_EQUIPO);
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());
        estadoEquipo.setDescripcion(EstadoEquipoEnum.PENDIENTE_APROBACION.getDescripcion());
        equipoBuscado.setEstadoEquipo(estadoEquipo);
        equipoBuscado.setMotivoUltimoCambioEstado("");
        when(repositorioEquipo.findByIdUsuario(ID_USUARIO)).thenReturn(Collections.singletonList(equipoBuscado));
    }

    private void seVerificaQueHayUnMensajeNotificandoQueNoTieneEquipo() {
        assertThat(resultadoDatosEquipo.getMensaje()).isEqualTo("No posee un equipo asociado aún. Ingrese el nombre que desea para su equipo en \"Editar Equipo\" y confirme para asociarlo.");
    }

    private void seVerificaQueLosDatosDelEquipoEstanVacios() {
        assertThat(resultadoDatosEquipo.getNombreEquipo()).isEqualTo("");
        assertThat(resultadoDatosEquipo.getIdEquipo()).isEqualTo(null);
    }

    private void alObtenerLosDatosDelEquipo() throws UsuarioInexistenteException {
        resultadoDatosEquipo = servicioEdicionEquipo.obtenerDatosEquipo(ID_USUARIO);
    }

    private void dadoQueNoTieneEquipoAsociado() {
        when(repositorioEquipo.findByIdUsuario(ID_USUARIO)).thenReturn(Collections.emptyList());
    }

    private void dadoQueElUsuarioExiste() {
        when(repositorioUsuario.findById(ID_USUARIO)).thenReturn(java.util.Optional.of(new Usuario()));
    }
}
