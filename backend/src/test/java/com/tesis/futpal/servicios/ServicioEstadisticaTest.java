package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.liga.Liga;
import com.tesis.futpal.modelos.liga.PartidoLigaBase;
import com.tesis.futpal.modelos.estadisticas.*;
import com.tesis.futpal.modelos.plantel.JugadorEstadisticas;
import com.tesis.futpal.comunicacion.requests.RequestActualizarEstadisticas;
import com.tesis.futpal.comunicacion.requests.RequestObtenerEstadisticasPartido;
import com.tesis.futpal.comunicacion.responses.ResponseObtenerEstadisticasLiga;
import com.tesis.futpal.comunicacion.responses.ResponseObtenerEstadisticasPartido;
import com.tesis.futpal.repositorios.RepositorioLigas;
import com.tesis.futpal.repositorios.RepositorioResultadoLiga;
import com.tesis.futpal.repositorios.estadisticas.*;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class ServicioEstadisticaTest {

    private static final Integer PARTIDO1_ID = 1;
    private static final long JUGADOR1_ID = 1L;
    private static final Long EQUIPO1_ID = 1L;
    private static final Integer PARTIDO2_ID = 2;
    private static final long JUGADOR2_ID = 2L;
    private static final Long EQUIPO2_ID = 2L;
    private static final Integer LIGA_ID = 1;

    private RequestActualizarEstadisticas requestActualizarEstadisticasValido;
    @Mock
    private RepositorioEstadisticaGol repositorioEstadisticaGol;
    @Mock
    private RepositorioEstadisticaTarjetaAmarilla repositorioEstadisticaTarjetaAmarilla;
    @Mock
    private RepositorioEstadisticaTarjetaRoja repositorioEstadisticaTarjetaRoja;
    @Mock
    private RepositorioEstadisticaLesion repositorioEstadisticaLesion;
    @Mock
    private RepositorioEstadisticaMvp repositorioEstadisticaMvp;
    @Mock
    private RepositorioResultadoLiga repositorioPartidosLiga;
    @Mock
    private RepositorioLigas repositorioLiga;
    @InjectMocks
    private ServicioEstadistica servicioEstadistica;

    @Captor
    ArgumentCaptor<EstadisticaGol> estadisticaGolArgumentCaptor;
    @Captor
    ArgumentCaptor<EstadisticaTarjetaAmarilla> estadisticaTarjetaAmarillaArgumentCaptor;
    @Captor
    ArgumentCaptor<EstadisticaTarjetaRoja> estadisticaTarjetaRojaArgumentCaptor;
    @Captor
    ArgumentCaptor<EstadisticaLesion> estadisticaLesionArgumentCaptor;
    @Captor
    ArgumentCaptor<EstadisticaMvp> estadisticaMvpArgumentCaptor;
    private RequestObtenerEstadisticasPartido requestObtenerEstadisticasPartido;
    private ResponseObtenerEstadisticasPartido resultadoObtenerEstadisticasPartido;
    private ResponseObtenerEstadisticasLiga responseObtenerEstadisticasLiga;

    @Test
    public void seActualizanTodasLasEstadisticasDeUnPartido() {
        dadoUnRequestDeEstadisticaValido();
        dadoQueExisteElPartido();
        dadoQueSePuedenGuardarLasEstadisticasDeGoles();
        dadoQueSePuedenGuardarLasEstadisticasDeAmarillas();
        dadoQueSePuedenGuardarLasEstadisticasDeRojas();
        dadoQueSePuedenGuardarLasEstadisticasDeLesiones();
        dadoQueSePuedenGuardarLasEstadisticasDeMvp();
        dadoQueSePuedenBorrarLasEstadisticas();

        cuandoSeActualizanLasEstadisticas();

        seVerificaQueSeGuardenLosDatosNuevos();
    }

    @Test
    public void seObtienenTodasLasEstadisticasDeUnPartido() {
        dadoQueExisteElPartido();
        dadoUnRequestDeObtenerEstadisticasDeUnPartidoValido();
        dadoQueSePuedenObtenerLasEstadisticasDeGolesParaUnPartido();
        dadoQueSePuedenObtenerLasEstadisticasDeAmarillasParaUnPartido();
        dadoQueSePuedenObtenerLasEstadisticasDeRojasParaUnPartido();
        dadoQueSePuedenObtenerLasEstadisticasDeLesionesParaUnPartido();
        dadoQueSePuedenObtenerLasEstadisticasDeMvpParaUnPartido();

        cuandoSeObtienenLasEstadisticasDeUnPartido();

        seVerificaQueSeObtengasLasEstadisticasDelPartido();
    }

    @Test
    public void seObtienenTodasLasEstadisticasDeLaLiga() {
        dadoQueLaLigaExiste();
        dadoQueSePuedenObtenerLasEstadisticasDeGolesParaUnaLiga();
        dadoQueSePuedenObtenerLasEstadisticasDeAmarillasParaUnaLiga();
        dadoQueSePuedenObtenerLasEstadisticasDeRojasParaUnaLiga();
        dadoQueSePuedenObtenerLasEstadisticasDeLesionesParaUnaLiga();
        dadoQueSePuedenObtenerLasEstadisticasDeMvpParaUnaLiga();

        cuandoSeObtienenLasEstadisticasDeUnaLiga();

        seVerificaQueSeObtengasLasEstadisticasDeLaLiga();
    }

    private void seVerificaQueSeObtengasLasEstadisticasDeLaLiga() {
        List<JugadorEstadisticas> golesJugadores = responseObtenerEstadisticasLiga.getGolesJugadores();
        List<Long> jugadoresGolesIds = golesJugadores.stream().map(JugadorEstadisticas::getId).collect(Collectors.toList());
        assertThat(golesJugadores.size()).isEqualTo(2);
        assertThat(jugadoresGolesIds).contains(JUGADOR1_ID, JUGADOR2_ID);

        List<JugadorEstadisticas> amarillasJugadores = responseObtenerEstadisticasLiga.getTarjetasAmarillasJugadores();
        List<Long> jugadoresAmarillasIds = amarillasJugadores.stream().map(JugadorEstadisticas::getId).collect(Collectors.toList());
        assertThat(amarillasJugadores.size()).isEqualTo(2);
        assertThat(jugadoresAmarillasIds).contains(JUGADOR1_ID, JUGADOR2_ID);

        List<JugadorEstadisticas> rojasJugadores = responseObtenerEstadisticasLiga.getTarjetasRojasJugadores();
        List<Long> jugadoresRojasIds = rojasJugadores.stream().map(JugadorEstadisticas::getId).collect(Collectors.toList());
        assertThat(rojasJugadores.size()).isEqualTo(2);
        assertThat(jugadoresRojasIds).contains(JUGADOR1_ID, JUGADOR2_ID);

        List<JugadorEstadisticas> lesionesJugadores = responseObtenerEstadisticasLiga.getLesionesJugadores();
        List<Long> jugadoresLesionesIds = lesionesJugadores.stream().map(JugadorEstadisticas::getId).collect(Collectors.toList());
        assertThat(lesionesJugadores.size()).isEqualTo(2);
        assertThat(jugadoresLesionesIds).contains(JUGADOR1_ID, JUGADOR2_ID);

        List<JugadorEstadisticas> mvpJugadores = responseObtenerEstadisticasLiga.getMvpJugadores();
        List<Long> jugadoresMvpIds = mvpJugadores.stream().map(JugadorEstadisticas::getId).collect(Collectors.toList());
        assertThat(mvpJugadores.size()).isEqualTo(2);
        assertThat(jugadoresMvpIds).contains(JUGADOR1_ID, JUGADOR2_ID);
    }

    private void cuandoSeObtienenLasEstadisticasDeUnaLiga() {
        responseObtenerEstadisticasLiga = servicioEstadistica.obtenerEstadisticasLiga(LIGA_ID);
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeMvpParaUnaLiga() {
        EstadisticaMvp estadisticaMvp1 = new EstadisticaMvp();
        estadisticaMvp1.setEquipo(new Equipo(EQUIPO1_ID));
        estadisticaMvp1.setJugador(new Jugador(JUGADOR1_ID));
        estadisticaMvp1.setPartido(new PartidoLigaBase(PARTIDO1_ID));

        EstadisticaMvp estadisticaMvp2 = new EstadisticaMvp();
        estadisticaMvp2.setEquipo(new Equipo(EQUIPO2_ID));
        estadisticaMvp2.setJugador(new Jugador(JUGADOR2_ID));
        estadisticaMvp2.setPartido(new PartidoLigaBase(PARTIDO2_ID));

        when(repositorioEstadisticaMvp.findByLiga(LIGA_ID)).thenReturn(Lists.list(estadisticaMvp1, estadisticaMvp2));
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeLesionesParaUnaLiga() {
        EstadisticaLesion estadisticaLesion1 = new EstadisticaLesion();
        estadisticaLesion1.setEquipo(new Equipo(EQUIPO1_ID));
        estadisticaLesion1.setJugador(new Jugador(JUGADOR1_ID));
        estadisticaLesion1.setPartido(new PartidoLigaBase(PARTIDO1_ID));

        EstadisticaLesion estadisticaLesion2 = new EstadisticaLesion();
        estadisticaLesion2.setEquipo(new Equipo(EQUIPO2_ID));
        estadisticaLesion2.setJugador(new Jugador(JUGADOR2_ID));
        estadisticaLesion2.setPartido(new PartidoLigaBase(PARTIDO2_ID));

        when(repositorioEstadisticaLesion.findByLiga(LIGA_ID)).thenReturn(Lists.list(estadisticaLesion1, estadisticaLesion2));
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeRojasParaUnaLiga() {
        EstadisticaTarjetaRoja estadisticaRoja1 = new EstadisticaTarjetaRoja();
        estadisticaRoja1.setEquipo(new Equipo(EQUIPO1_ID));
        estadisticaRoja1.setJugador(new Jugador(JUGADOR1_ID));
        estadisticaRoja1.setPartido(new PartidoLigaBase(PARTIDO1_ID));

        EstadisticaTarjetaRoja estadisticaRoja2 = new EstadisticaTarjetaRoja();
        estadisticaRoja2.setEquipo(new Equipo(EQUIPO2_ID));
        estadisticaRoja2.setJugador(new Jugador(JUGADOR2_ID));
        estadisticaRoja2.setPartido(new PartidoLigaBase(PARTIDO2_ID));

        when(repositorioEstadisticaTarjetaRoja.findByLiga(LIGA_ID)).thenReturn(Lists.list(estadisticaRoja1, estadisticaRoja2));
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeAmarillasParaUnaLiga() {
        EstadisticaTarjetaAmarilla estadisticaAmarilla1 = new EstadisticaTarjetaAmarilla();
        estadisticaAmarilla1.setEquipo(new Equipo(EQUIPO1_ID));
        estadisticaAmarilla1.setJugador(new Jugador(JUGADOR1_ID));
        estadisticaAmarilla1.setPartido(new PartidoLigaBase(PARTIDO1_ID));

        EstadisticaTarjetaAmarilla estadisticaAmarilla2 = new EstadisticaTarjetaAmarilla();
        estadisticaAmarilla2.setEquipo(new Equipo(EQUIPO2_ID));
        estadisticaAmarilla2.setJugador(new Jugador(JUGADOR2_ID));
        estadisticaAmarilla2.setPartido(new PartidoLigaBase(PARTIDO2_ID));

        when(repositorioEstadisticaTarjetaAmarilla.findByLiga(LIGA_ID)).thenReturn(Lists.list(estadisticaAmarilla1, estadisticaAmarilla2));
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeGolesParaUnaLiga() {
        EstadisticaGol estadisticaGol1 = new EstadisticaGol();
        estadisticaGol1.setEquipo(new Equipo(EQUIPO1_ID));
        estadisticaGol1.setJugador(new Jugador(JUGADOR1_ID));
        estadisticaGol1.setPartido(new PartidoLigaBase(PARTIDO1_ID));
        estadisticaGol1.setCantidad(2);

        EstadisticaGol estadisticaGol2 = new EstadisticaGol();
        estadisticaGol2.setEquipo(new Equipo(EQUIPO2_ID));
        estadisticaGol2.setJugador(new Jugador(JUGADOR2_ID));
        estadisticaGol2.setPartido(new PartidoLigaBase(PARTIDO2_ID));
        estadisticaGol2.setCantidad(3);

        when(repositorioEstadisticaGol.findByLiga(LIGA_ID)).thenReturn(Lists.list(estadisticaGol1, estadisticaGol2));
    }

    private void dadoQueLaLigaExiste() {
        when(repositorioLiga.findById(LIGA_ID)).thenReturn(java.util.Optional.of(new Liga()));
    }

    private void dadoQueSePuedenBorrarLasEstadisticas() {
        doNothing().when(repositorioEstadisticaGol).deleteByPartidoId(any());
        doNothing().when(repositorioEstadisticaTarjetaAmarilla).deleteByPartidoId(any());
        doNothing().when(repositorioEstadisticaTarjetaRoja).deleteByPartidoId(any());
        doNothing().when(repositorioEstadisticaLesion).deleteByPartidoId(any());
        doNothing().when(repositorioEstadisticaMvp).deleteByPartidoId(any());
    }

    private void dadoQueExisteElPartido() {
        when(repositorioPartidosLiga.findById(PARTIDO1_ID)).thenReturn(java.util.Optional.of(new PartidoLigaBase(PARTIDO1_ID)));
    }

    private void seVerificaQueSeObtengasLasEstadisticasDelPartido() {
        List<JugadorEstadisticas> golesJugadores = resultadoObtenerEstadisticasPartido.getGolesJugadores();
        assertThat(golesJugadores.size()).isEqualTo(1);
        assertThat(golesJugadores.get(0).getId()).isEqualTo(JUGADOR1_ID);

        List<JugadorEstadisticas> tarjetasAmarillasJugadores = resultadoObtenerEstadisticasPartido.getTarjetasAmarillasJugadores();
        assertThat(tarjetasAmarillasJugadores.size()).isEqualTo(1);
        assertThat(tarjetasAmarillasJugadores.get(0).getId()).isEqualTo(JUGADOR1_ID);

        List<JugadorEstadisticas> tarjetasRojasJugadores = resultadoObtenerEstadisticasPartido.getTarjetasRojasJugadores();
        assertThat(tarjetasRojasJugadores.size()).isEqualTo(1);
        assertThat(tarjetasRojasJugadores.get(0).getId()).isEqualTo(JUGADOR1_ID);

        List<JugadorEstadisticas> lesionesJugadores = resultadoObtenerEstadisticasPartido.getLesionesJugadores();
        assertThat(lesionesJugadores.size()).isEqualTo(1);
        assertThat(lesionesJugadores.get(0).getId()).isEqualTo(JUGADOR1_ID);

        JugadorEstadisticas mvpJugador = resultadoObtenerEstadisticasPartido.getMvpJugador();
        assertThat(mvpJugador.getId()).isEqualTo(JUGADOR1_ID);
    }

    private void cuandoSeObtienenLasEstadisticasDeUnPartido() {
        resultadoObtenerEstadisticasPartido = servicioEstadistica.obtenerEstadisticas(requestObtenerEstadisticasPartido);
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeMvpParaUnPartido() {
        EstadisticaMvp estadisticaMvp = new EstadisticaMvp();
        Jugador jugador = new Jugador();
        jugador.setId(JUGADOR1_ID);
        estadisticaMvp.setJugador(jugador);
        Equipo equipo = new Equipo();
        equipo.setId(EQUIPO1_ID);
        estadisticaMvp.setEquipo(equipo);
        when(repositorioEstadisticaMvp.findByPartidoId(PARTIDO1_ID)).thenReturn(estadisticaMvp);
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeLesionesParaUnPartido() {
        EstadisticaLesion estadisticaLesion = new EstadisticaLesion();
        Jugador jugador = new Jugador();
        jugador.setId(JUGADOR1_ID);
        estadisticaLesion.setJugador(jugador);
        Equipo equipo = new Equipo();
        equipo.setId(EQUIPO1_ID);
        estadisticaLesion.setEquipo(equipo);
        when(repositorioEstadisticaLesion.findByPartidoId(PARTIDO1_ID)).thenReturn(Lists.list(estadisticaLesion));
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeRojasParaUnPartido() {
        EstadisticaTarjetaRoja estadisticaTarjetaRoja = new EstadisticaTarjetaRoja();
        Jugador jugador = new Jugador();
        jugador.setId(JUGADOR1_ID);
        estadisticaTarjetaRoja.setJugador(jugador);
        Equipo equipo = new Equipo();
        equipo.setId(EQUIPO1_ID);
        estadisticaTarjetaRoja.setEquipo(equipo);
        when(repositorioEstadisticaTarjetaRoja.findByPartidoId(PARTIDO1_ID)).thenReturn(Lists.list(estadisticaTarjetaRoja));
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeAmarillasParaUnPartido() {
        EstadisticaTarjetaAmarilla estadisticaTarjetaAmarilla = new EstadisticaTarjetaAmarilla();
        Jugador jugador = new Jugador();
        jugador.setId(JUGADOR1_ID);
        estadisticaTarjetaAmarilla.setJugador(jugador);
        Equipo equipo = new Equipo();
        equipo.setId(EQUIPO1_ID);
        estadisticaTarjetaAmarilla.setEquipo(equipo);
        when(repositorioEstadisticaTarjetaAmarilla.findByPartidoId(PARTIDO1_ID)).thenReturn(Lists.list(estadisticaTarjetaAmarilla));
    }

    private void dadoQueSePuedenObtenerLasEstadisticasDeGolesParaUnPartido() {
        EstadisticaGol estadisticaGol = new EstadisticaGol();
        Jugador jugador = new Jugador();
        jugador.setId(JUGADOR1_ID);
        estadisticaGol.setJugador(jugador);
        Equipo equipo = new Equipo();
        equipo.setId(EQUIPO1_ID);
        estadisticaGol.setEquipo(equipo);
        estadisticaGol.setCantidad(3);
        when(repositorioEstadisticaGol.findByPartidoId(PARTIDO1_ID)).thenReturn(Lists.list(estadisticaGol));
    }

    private void dadoUnRequestDeObtenerEstadisticasDeUnPartidoValido() {
        requestObtenerEstadisticasPartido = new RequestObtenerEstadisticasPartido();
        requestObtenerEstadisticasPartido.setPartidoId(PARTIDO1_ID);
    }

    private void seVerificaQueSeGuardenLosDatosNuevos() {
        verify(repositorioEstadisticaGol, times(1)).save(estadisticaGolArgumentCaptor.capture());
        EstadisticaGol estadisticaGolGuardada = estadisticaGolArgumentCaptor.getValue();
        assertThat(estadisticaGolGuardada.getCantidad()).isEqualTo(2);
        assertThat(estadisticaGolGuardada.getEquipo().getId()).isEqualTo(1L);
        assertThat(estadisticaGolGuardada.getJugador().getId()).isEqualTo(2L);
        assertThat(estadisticaGolGuardada.getPartido().getId()).isEqualTo(1);

        verify(repositorioEstadisticaTarjetaAmarilla, times(1)).save(estadisticaTarjetaAmarillaArgumentCaptor.capture());
        EstadisticaTarjetaAmarilla estadisticaTarjetaAmarillaGuardada = estadisticaTarjetaAmarillaArgumentCaptor.getValue();
        assertThat(estadisticaTarjetaAmarillaGuardada.getEquipo().getId()).isEqualTo(1L);
        assertThat(estadisticaTarjetaAmarillaGuardada.getJugador().getId()).isEqualTo(2L);
        assertThat(estadisticaTarjetaAmarillaGuardada.getPartido().getId()).isEqualTo(1);

        verify(repositorioEstadisticaTarjetaRoja, times(1)).save(estadisticaTarjetaRojaArgumentCaptor.capture());
        EstadisticaTarjetaRoja estadisticaTarjetaRojaGuardada = estadisticaTarjetaRojaArgumentCaptor.getValue();
        assertThat(estadisticaTarjetaRojaGuardada.getEquipo().getId()).isEqualTo(1L);
        assertThat(estadisticaTarjetaRojaGuardada.getJugador().getId()).isEqualTo(1L);
        assertThat(estadisticaTarjetaRojaGuardada.getPartido().getId()).isEqualTo(1);

        verify(repositorioEstadisticaLesion, times(1)).save(estadisticaLesionArgumentCaptor.capture());
        EstadisticaLesion estadisticaLesionGuardada = estadisticaLesionArgumentCaptor.getValue();
        assertThat(estadisticaLesionGuardada.getEquipo().getId()).isEqualTo(1L);
        assertThat(estadisticaLesionGuardada.getJugador().getId()).isEqualTo(2L);
        assertThat(estadisticaLesionGuardada.getPartido().getId()).isEqualTo(1);

        verify(repositorioEstadisticaMvp, times(1)).save(estadisticaMvpArgumentCaptor.capture());
        EstadisticaMvp estadisticaMvpGuardada = estadisticaMvpArgumentCaptor.getValue();
        assertThat(estadisticaMvpGuardada.getEquipo().getId()).isEqualTo(1L);
        assertThat(estadisticaMvpGuardada.getJugador().getId()).isEqualTo(2L);
        assertThat(estadisticaMvpGuardada.getPartido().getId()).isEqualTo(1);
    }

    private void cuandoSeActualizanLasEstadisticas() {
        servicioEstadistica.actualizarEstadisticas(requestActualizarEstadisticasValido);
    }

    private void dadoQueSePuedenGuardarLasEstadisticasDeMvp() {
        when(repositorioEstadisticaMvp.save(any(EstadisticaMvp.class))).thenReturn(new EstadisticaMvp());
    }

    private void dadoQueSePuedenGuardarLasEstadisticasDeLesiones() {
        when(repositorioEstadisticaLesion.save(any(EstadisticaLesion.class))).thenReturn(new EstadisticaLesion());
    }

    private void dadoQueSePuedenGuardarLasEstadisticasDeRojas() {
        when(repositorioEstadisticaTarjetaRoja.save(any(EstadisticaTarjetaRoja.class))).thenReturn(new EstadisticaTarjetaRoja());
    }

    private void dadoQueSePuedenGuardarLasEstadisticasDeAmarillas() {
        when(repositorioEstadisticaTarjetaAmarilla.save(any(EstadisticaTarjetaAmarilla.class))).thenReturn(new EstadisticaTarjetaAmarilla());
    }

    private void dadoQueSePuedenGuardarLasEstadisticasDeGoles() {
        when(repositorioEstadisticaGol.save(any(EstadisticaGol.class))).thenReturn(new EstadisticaGol());
    }

    private void dadoUnRequestDeEstadisticaValido() {
        JugadorEstadisticas jugadorEstadisticas1 = new JugadorEstadisticas();
        jugadorEstadisticas1.setId(1L);
        jugadorEstadisticas1.setEquipo(new Equipo(1L));
        jugadorEstadisticas1.setNombre("Pepe");

        JugadorEstadisticas jugadorEstadisticas2 = new JugadorEstadisticas();
        jugadorEstadisticas2.setId(2L);
        jugadorEstadisticas2.setEquipo(new Equipo(1L));
        jugadorEstadisticas2.setNombre("Driussi");
        jugadorEstadisticas2.setCantidad(2);

        requestActualizarEstadisticasValido = new RequestActualizarEstadisticas();
        requestActualizarEstadisticasValido.setGolesJugadores(Lists.list(jugadorEstadisticas2));
        requestActualizarEstadisticasValido.setTarjetasAmarillasJugadores(Lists.list(jugadorEstadisticas2));
        requestActualizarEstadisticasValido.setTarjetasRojasJugadores(Lists.list(jugadorEstadisticas1));
        requestActualizarEstadisticasValido.setLesionesJugadores(Lists.list(jugadorEstadisticas2));
        requestActualizarEstadisticasValido.setMvpJugador(jugadorEstadisticas2);
        requestActualizarEstadisticasValido.setPartidoId(1);
    }
}