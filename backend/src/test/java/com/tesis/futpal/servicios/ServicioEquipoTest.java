package com.tesis.futpal.servicios;

import com.tesis.futpal.modelos.equipo.EstadoEquipoEnum;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipo;
import com.tesis.futpal.comunicacion.requests.RequestRechazarEquipo;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServicioEquipoTest {

    @Captor
    ArgumentCaptor<Equipo> equipoGuardadoCaptor;
    public static final long ID_EQUIPO = 1L;
    private static final String MOTIVO_CAMBIO_ESTADO_RECHAZO = "Jugadores repetidos";
    @Mock
    private RepositorioEquipo repositorioEquipo;
    @InjectMocks
    private ServicioEquipo servicioEquipo;

    private List<Equipo> equiposAprobados;
    private List<Equipo> equiposPendienteAprobacion;
    private Equipo equipo1;
    private Equipo equipo2;

    @Test
    public void dadoQueExistenEquiposAprobadosPuedoObtenerlos() {
        dadoQueExistenDosEquiposAprobados();
        cuandoObtengoTodosLosEquiposAprobados();
        verificoQueObtuveDosEquiposAprobados();
        verificoQueLosDosEquiposAprobadosTenganLosDatosCorrectos();
    }

    @Test
    public void dadoQueExistenEquiposPendienteDeAprobacionPuedoObtenerlos() {
        dadoQueExistenDosEquiposPendienteDeAprobacion();
        cuandoObtengoTodosLosEquiposPendienteAprobacion();
        verificoQueObtuveDosEquiposPendienteDeAprobacion();
        verificoQueLosDosEquiposPendienteAprobacionTenganLosDatosCorrectos();
    }

    @Test
    public void seApruebaUnEquipo() {
        dadoQueExisteElEquipoPendienteDeAprobacion();
        dadoQueSePuedeCambiarElEstadoAUnEquipo();
        alAprobarUnEquipo();
        seVerificaQueElEquipoTieneElNuevoEstadoAprobado();
    }

    @Test
    public void seRechazaUnEquipo() {
        dadoQueExisteElEquipoPendienteDeAprobacion();
        dadoQueSePuedeCambiarElEstadoAUnEquipo();
        alRechazarUnEquipo();
        seVerificaQueElEquipoTieneElNuevoEstadoRechazado();
    }

    @Test
    public void seSolicitaAprobacionDeUnEquipo() {
        //el estado puede ser también rechazado
        dadoQueExisteElEquipoEnEstadoInicial();
        dadoQueSePuedeCambiarElEstadoAUnEquipo();
        alSolicitarAprobacionDeUnEquipo();
        seVerificaQueElEquipoTieneElNuevoEstadoPendienteAprobacion();
    }

    private void seVerificaQueElEquipoTieneElNuevoEstadoPendienteAprobacion() {
        verify(repositorioEquipo, times(1)).save(equipoGuardadoCaptor.capture());
        Equipo equipoGuardado = equipoGuardadoCaptor.getValue();
        assertThat(equipoGuardado.getEstadoEquipo().getId()).isEqualTo(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());
    }

    private void alSolicitarAprobacionDeUnEquipo() {
        servicioEquipo.solicitarAprobacionEquipo(ID_EQUIPO);
    }

    private void dadoQueExisteElEquipoEnEstadoInicial() {
        equipo1 = new Equipo();
        equipo1.setId(ID_EQUIPO);
        equipo1.setNombreEquipo("Equipo 1");
        equipo1.setIdUsuario(1L);
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.INICIAL.getId());
        equipo1.setEstadoEquipo(estadoEquipo);
        when(repositorioEquipo.findById(ID_EQUIPO)).thenReturn(java.util.Optional.ofNullable(equipo1));
    }

    private void seVerificaQueElEquipoTieneElNuevoEstadoRechazado() {
        verify(repositorioEquipo, times(1)).save(equipoGuardadoCaptor.capture());
        Equipo equipoGuardado = equipoGuardadoCaptor.getValue();
        assertThat(equipoGuardado.getEstadoEquipo().getId()).isEqualTo(EstadoEquipoEnum.RECHAZADO.getId());
        assertThat(equipoGuardado.getMotivoUltimoCambioEstado()).isEqualTo(MOTIVO_CAMBIO_ESTADO_RECHAZO);
    }

    private void alRechazarUnEquipo() {
        RequestRechazarEquipo requestRechazarEquipo = new RequestRechazarEquipo();
        requestRechazarEquipo.setEquipoId(ID_EQUIPO);
        requestRechazarEquipo.setMotivoRechazo(MOTIVO_CAMBIO_ESTADO_RECHAZO);
        servicioEquipo.rechazarEquipo(requestRechazarEquipo);
    }

    private void seVerificaQueElEquipoTieneElNuevoEstadoAprobado() {
        verify(repositorioEquipo, times(1)).save(equipoGuardadoCaptor.capture());
        Equipo equipoGuardado = equipoGuardadoCaptor.getValue();
        assertThat(equipoGuardado.getEstadoEquipo().getId()).isEqualTo(EstadoEquipoEnum.APROBADO.getId());
        assertThat(equipoGuardado.getMotivoUltimoCambioEstado()).isEqualTo("");
    }

    private void alAprobarUnEquipo() {
        servicioEquipo.aprobarEquipo(ID_EQUIPO);
    }

    private void dadoQueSePuedeCambiarElEstadoAUnEquipo() {
        when(repositorioEquipo.save(equipo1)).thenReturn(new Equipo());
    }

    private void dadoQueExisteElEquipoPendienteDeAprobacion() {
        equipo1 = new Equipo();
        equipo1.setId(ID_EQUIPO);
        equipo1.setNombreEquipo("Equipo 1");
        equipo1.setIdUsuario(1L);
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());
        equipo1.setEstadoEquipo(estadoEquipo);
        when(repositorioEquipo.findById(ID_EQUIPO)).thenReturn(java.util.Optional.ofNullable(equipo1));
    }

    private void verificoQueLosDosEquiposPendienteAprobacionTenganLosDatosCorrectos() {
        Equipo equipo1 = equiposPendienteAprobacion.get(0);
        Equipo equipo2 = equiposPendienteAprobacion.get(1);
        assertThat(equipo1.getId()).isEqualTo(1L);
        assertThat(equipo1.getNombreEquipo()).isEqualTo("Equipo 1");
        assertThat(equipo1.getIdUsuario()).isEqualTo(1L);
        assertThat(equipo2.getId()).isEqualTo(2L);
        assertThat(equipo2.getNombreEquipo()).isEqualTo("Equipo 2");
        assertThat(equipo2.getIdUsuario()).isEqualTo(2L);
    }

    private void verificoQueLosDosEquiposAprobadosTenganLosDatosCorrectos() {
        Equipo equipo1 = equiposAprobados.get(0);
        Equipo equipo2 = equiposAprobados.get(1);
        assertThat(equipo1.getId()).isEqualTo(1L);
        assertThat(equipo1.getNombreEquipo()).isEqualTo("Equipo 1");
        assertThat(equipo1.getIdUsuario()).isEqualTo(1L);
        assertThat(equipo2.getId()).isEqualTo(2L);
        assertThat(equipo2.getNombreEquipo()).isEqualTo("Equipo 2");
        assertThat(equipo2.getIdUsuario()).isEqualTo(2L);
    }

    private void verificoQueObtuveDosEquiposAprobados() {
        assertThat(equiposAprobados.size()).isEqualTo(2);
    }

    private void verificoQueObtuveDosEquiposPendienteDeAprobacion() {
        assertThat(equiposPendienteAprobacion.size()).isEqualTo(2);
    }

    private void cuandoObtengoTodosLosEquiposPendienteAprobacion() {
        equiposPendienteAprobacion = servicioEquipo.obtenerTodosLosEquiposPendienteAprobacion();
    }

    private void cuandoObtengoTodosLosEquiposAprobados() {
        equiposAprobados = servicioEquipo.obtenerTodosLosEquiposAprobados();
    }

    private void dadoQueExistenDosEquiposAprobados() {
        List<Equipo> equiposAprobados = new ArrayList<>();
        equipo1 = new Equipo();
        equipo1.setId(1L);
        equipo1.setNombreEquipo("Equipo 1");
        equipo1.setIdUsuario(1L);
        EstadoEquipo estadoEquipoAprobado1 = new EstadoEquipo();
        estadoEquipoAprobado1.setId(EstadoEquipoEnum.APROBADO.getId());
        equipo1.setEstadoEquipo(estadoEquipoAprobado1);
        equipo2 = new Equipo();
        equipo2.setId(2L);
        equipo2.setNombreEquipo("Equipo 2");
        equipo2.setIdUsuario(2L);
        EstadoEquipo estadoEquipoAprobado2 = new EstadoEquipo();
        estadoEquipoAprobado2.setId(EstadoEquipoEnum.APROBADO.getId());
        equipo2.setEstadoEquipo(estadoEquipoAprobado2);
        equiposAprobados.add(equipo1);
        equiposAprobados.add(equipo2);
        when(repositorioEquipo.findByEstadoEquipoIdOrderByNombreEquipo(EstadoEquipoEnum.APROBADO.getId())).thenReturn(equiposAprobados);
    }

    private void dadoQueExistenDosEquiposPendienteDeAprobacion() {
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());

        List<Equipo> equiposPendienteAprobacion = new ArrayList<>();
        equipo1 = new Equipo();
        equipo1.setId(1L);
        equipo1.setNombreEquipo("Equipo 1");
        equipo1.setIdUsuario(1L);
        equipo1.setEstadoEquipo(estadoEquipo);
        equipo2 = new Equipo();
        equipo2.setId(2L);
        equipo2.setNombreEquipo("Equipo 2");
        equipo2.setIdUsuario(2L);
        equipo2.setEstadoEquipo(estadoEquipo);
        equiposPendienteAprobacion.add(equipo1);
        equiposPendienteAprobacion.add(equipo2);
        when(repositorioEquipo.findByEstadoEquipoIdOrderByNombreEquipo(EstadoEquipoEnum.PENDIENTE_APROBACION.getId())).thenReturn(equiposPendienteAprobacion);
    }
}
