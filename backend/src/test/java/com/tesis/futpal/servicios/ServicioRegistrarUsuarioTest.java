package com.tesis.futpal.servicios;

import com.tesis.futpal.excepciones.CamposNulosException;
import com.tesis.futpal.excepciones.CamposVaciosException;
import com.tesis.futpal.excepciones.UsuarioExistenteException;
import com.tesis.futpal.comunicacion.requests.RequestRegistrarUsuario;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServicioRegistrarUsuarioTest {

	private static final String MENSAJE_CAMPOS_VACIOS = "No puede haber campos vacíos.";
	private static final String MENSAJE_CAMPOS_NULOS = "No puede haber campos nulos.";
	private static final String MENSAJE_USUARIO_EXISTENTE = "No se ha podido crear el usuario. ";
	private static final String ALIAS_EXISTENTE = "El alias ya existe.";
	private static final String MAIL_EXISTENTE = "El mail ya existe.";
	private static final Long ID_USUARIO = 1L;

	@Mock
	private RepositorioUsuario repositorioUsuario;

	@Mock
	private PasswordEncoder encoder;

	@InjectMocks
	private ServicioRegistrarUsuario servicioRegistrarUsuario;

	private RequestRegistrarUsuario requestRegistrarUsuario;

	@Test
	public void siHayCamposNulosSeLanzaExcepcionDeCamposNulosYNoSeCreaUsuario() {
		dadoUnRequestConCamposNulos();
		alIntentarDarDeAltaUnUsuarioSeLanzaExcepcionCamposNulosException();
		yNoSeCreaElUsuario();
	}


	@Test
	public void siNoHayCamposNulosYHayCamposVaciosSeLanzaExcepcionDeCamposVaciosYNoSeCreaElUsuario() {
		dadoUnRequestSinCamposNulosYConCamposVacios();
		alIntentarDarDeAltaUnUsuarioSeLanzaExcepcionCamposVaciosException();
		yNoSeCreaElUsuario();
	}

	@Test
	public void dadoUnRequestValidoSeEjecutaCorrectamenteElAltaDelUsuario() throws CamposVaciosException, CamposNulosException, UsuarioExistenteException {
		dadoUnRequestValido();
		dadoQueSePuedeGuardarElUsuario();
		alIntentarDarDeAltaUnUsuario();
		entoncesSeEjecutanEnOrdenLosPasos();
		entoncesSeCreaElUsuario();
	}

	private void dadoQueSePuedeGuardarElUsuario() {
		Usuario usuario = mock(Usuario.class);
		when(repositorioUsuario.save(any())).thenReturn(usuario);
	}


	@Test
	public void dadoUnRequestValidoPeroConAliasExistenteSeLanzaExcepcionYNoSeCreaElUsuario() {
		dadoUnRequestValido();
		dadoQueElAliasYaExiste();
		alIntentarDarDeAltaUnUsuarioSeLanzaExcepcionUsuarioExistenteException(MENSAJE_USUARIO_EXISTENTE + ALIAS_EXISTENTE);
		yNoSeCreaElUsuario();
	}

	@Test
	public void dadoUnRequestValidoPeroConMailExistenteSeLanzaExcepcionYNoSeCreaElUsuario() {
		dadoUnRequestValido();
		dadoQueElMailYaExiste();
		alIntentarDarDeAltaUnUsuarioSeLanzaExcepcionUsuarioExistenteException(MENSAJE_USUARIO_EXISTENTE + MAIL_EXISTENTE);
		yNoSeCreaElUsuario();
	}

	private void dadoQueElMailYaExiste() {
		when(repositorioUsuario.findByMailUsuario(requestRegistrarUsuario.getMail())).thenReturn(Collections.singletonList(new Usuario()));
	}

	private void alIntentarDarDeAltaUnUsuarioSeLanzaExcepcionUsuarioExistenteException(String mensajeEsperado) {
		UsuarioExistenteException exception = assertThrows(
				UsuarioExistenteException.class, () -> {
					servicioRegistrarUsuario.registrarUsuario(requestRegistrarUsuario);
				});
		assertThat(exception.getMessage()).isEqualTo(mensajeEsperado);
	}

	private void dadoQueElAliasYaExiste() {
		when(repositorioUsuario.findByAliasUsuario(requestRegistrarUsuario.getAlias())).thenReturn(Collections.singletonList(new Usuario()));
	}

	private void entoncesSeCreaElUsuario() {
		verify(repositorioUsuario, times(1)).save(any());
	}

	private void entoncesSeEjecutanEnOrdenLosPasos() {
		InOrder orderVerifier = Mockito.inOrder(requestRegistrarUsuario);
		orderVerifier.verify(requestRegistrarUsuario).verificarQueTodosLosCamposSeanNoNulos();
		orderVerifier.verify(requestRegistrarUsuario).eliminarEspaciosDelPrincipioYDelFinal();
		orderVerifier.verify(requestRegistrarUsuario).verificarQueTodosLosCamposSeanNoVacios();
	}

	private void alIntentarDarDeAltaUnUsuario() throws CamposVaciosException, CamposNulosException, UsuarioExistenteException {
		servicioRegistrarUsuario.registrarUsuario(requestRegistrarUsuario);
	}

	private void dadoUnRequestValido() {
		requestRegistrarUsuario = mock(RequestRegistrarUsuario.class);
		when(requestRegistrarUsuario.verificarQueTodosLosCamposSeanNoNulos()).thenReturn(true);
		when(requestRegistrarUsuario.verificarQueTodosLosCamposSeanNoVacios()).thenReturn(true);
	}

	//TODO: Tests para verificar que el alias y/o mail ya existe.

	private void alIntentarDarDeAltaUnUsuarioSeLanzaExcepcionCamposVaciosException() {
		CamposVaciosException exception = assertThrows(
				CamposVaciosException.class, () -> {
					servicioRegistrarUsuario.registrarUsuario(requestRegistrarUsuario);
				});
		assertThat(exception.getMessage()).isEqualTo(MENSAJE_CAMPOS_VACIOS);
	}

	private void dadoUnRequestSinCamposNulosYConCamposVacios() {
		requestRegistrarUsuario = mock(RequestRegistrarUsuario.class);
		when(requestRegistrarUsuario.verificarQueTodosLosCamposSeanNoNulos()).thenReturn(true);
		when(requestRegistrarUsuario.verificarQueTodosLosCamposSeanNoVacios()).thenReturn(false);
	}

	private void yNoSeCreaElUsuario() {
		verify(repositorioUsuario, times(0)).save(any());
	}

	private void alIntentarDarDeAltaUnUsuarioSeLanzaExcepcionCamposNulosException() {
		CamposNulosException exception = assertThrows(
				CamposNulosException.class, () -> {
					servicioRegistrarUsuario.registrarUsuario(requestRegistrarUsuario);
				});
		assertThat(exception.getMessage()).isEqualTo(MENSAJE_CAMPOS_NULOS);
	}

	private void dadoUnRequestConCamposNulos() {
		requestRegistrarUsuario = mock(RequestRegistrarUsuario.class);
		when(requestRegistrarUsuario.verificarQueTodosLosCamposSeanNoNulos()).thenReturn(false);
	}

}
