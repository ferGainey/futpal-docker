package stepDefinitions;

import com.tesis.futpal.AplicacionFutpalBackend;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@CucumberOptions(publish = true)
@CucumberContextConfiguration
@SpringBootTest(classes = AplicacionFutpalBackend.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)

//TODO: Revisar si están bien los parámetros configurados en el SpringBootTest
public class SpringAcceptanceTest {
    @LocalServerPort
    protected int serverPort;
    // executeGet implementation
}