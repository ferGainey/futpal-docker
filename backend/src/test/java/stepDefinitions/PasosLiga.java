package stepDefinitions;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.liga.*;
import com.tesis.futpal.modelos.usuario.Rol;
import com.tesis.futpal.modelos.usuario.TipoRol;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.util.Lists;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import paginas.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PasosLiga extends PasosComunes {
    private static final Integer ID_LIGA_DE_BRONCE = 1;
    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private RepositorioLigas repositorioLiga;
    @Autowired
    private RepositorioResultadoLiga repositorioResultadoLiga;
    @Autowired
    private RepositorioLigaEquipos repositorioLigaEquipos;
    @Autowired
    private PasswordEncoder encoder;

    private Usuario usuario1;
    private Usuario usuario2;
    private Usuario usuario3;
    private Usuario usuario4;
    private Usuario usuario5;
    private Equipo equipo1;
    private Equipo equipo2;
    private Equipo equipo3;
    private Equipo equipo4;
    private Equipo equipo5;
    private PartidoLigaBase partidoLiga;
    private Usuario usuarioAdministrador;

    @Given("existe un administrador {string}")
    public void existeUnAdministrador(String nombreAdministrador) {
        Usuario usuario = new Usuario();
        usuario.setAliasUsuario(nombreAdministrador);
        usuario.setNombreUsuario("Pablo");
        usuario.setApellidoUsuario("Drewsen");
        usuario.setMailUsuario("yid@army.com");
        usuario.setContraseniaUsuario(encoder.encode("12345678"));

        List<Rol> rolesUsuario = new ArrayList<>();

        Rol rolUsuario = new Rol();
        rolUsuario.setId(TipoRol.ROL_USUARIO.getId());
        rolesUsuario.add(rolUsuario);

        Rol rolAdministrador = new Rol();
        rolAdministrador.setId(TipoRol.ROL_ADMINISTRADOR.getId());
        rolesUsuario.add(rolAdministrador);

        usuario.setRoles(rolesUsuario);

        usuarioAdministrador = repositorioUsuario.save(usuario);
    }

    @And("el administrador {string} se encuentra en la pantalla de creacion de ligas")
    public void elAdministradorSeEncuentraEnLaPantallaDeCreacionDeLigas(String aliasAdministrador) {
        WebDriver driver = PasosContexto.webDriver;
        driver.get(ConfiguracionSelenium.getUrlBase());
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        paginaPantallaPrincipal.irAPantallaLogin();
        PaginaIngresoUsuario paginaIngresoUsuario = new PaginaIngresoUsuario(driver);
        paginaIngresoUsuario.loguearUsuario(aliasAdministrador, "12345678");
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(driver);
        paginaPantallaInicialUsuario.irAPaginaCrearLiga();
    }

    @Given("completo el nombre de la liga con {string}")
    public void completoElNombreDeLaLigaCon(String nombreLiga) {
        WebDriver driver = PasosContexto.webDriver;
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(driver);
        paginaCreacionLiga.cargarNombreDeLaLiga(nombreLiga);
    }

    @When("selecciona crear liga")
    public void seleccionaCrearLiga() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(driver);
        paginaCreacionLiga.crearLiga();
    }

    @Then("se le redirige a la pantalla de la liga {string}")
    public void seLeRedirigeALaPantallaDeLaLiga(String nombreLiga) {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        assertThat(paginaPantallaLiga.buscarTextoTituloLiga()).isEqualTo("Liga: " + nombreLiga);
    }

    @Then("se le muestra una alerta indicando que hay campos invalidos")
    public void seLeMuestraUnaAlertaIndicandoQueHayCamposInvalidos() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(driver);
        assertThat(paginaCreacionLiga.buscarTextoAlertaErrorAlRegistrarLiga()).isEqualTo("Se produjo un error al intentar crear la liga.");
    }

    @And("existe el equipo administrador {string}")
    public void existeElEquipoAdministrador(String nombreEquipo) {
        this.crearEquipo(usuarioAdministrador.getId(), nombreEquipo);
    }

    @Then("el equipo {string} aparece en la pantalla como equipo disponible a agregar")
    public void elEquipoApareceEnLaPantallaComoEquipoDisponibleAAgregar(String nombreEquipo) {
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(PasosContexto.webDriver);
        assertThat(paginaCreacionLiga.buscarEquiposDisponibles()).contains(nombreEquipo);
    }

    @Then("el equipo {string} aparece en la pantalla como equipo que se va a agregar")
    public void elEquipoApareceEnLaPantallaComoEquipoQueSeVaAAgregar(String nombreEquipo) {
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(PasosContexto.webDriver);
        List<String> nombresEquiposAgregados = paginaCreacionLiga.buscarNombresEquiposAgregados();
        assertThat(nombresEquiposAgregados).contains(nombreEquipo);
    }

    @When("agrego el equipo {string} a la liga")
    public void agregoElEquipoALaLiga(String nombreEquipo) {
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(PasosContexto.webDriver);
        paginaCreacionLiga.agregarEquipo(nombreEquipo);
    }

    @Given("el administrador agrego al equipo {string}")
    public void elAdministradorAgregoAlEquipo(String nombreEquipo) {
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(PasosContexto.webDriver);
        paginaCreacionLiga.agregarEquipo(nombreEquipo);
    }

    @When("el administrador borra el equipo {string} a la liga")
    public void elAdministradorBorraElEquipoALaLiga(String nombreEquipo) {
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(PasosContexto.webDriver);
        paginaCreacionLiga.borrarPrimerEquipo();
    }

    @Then("el equipo {string} no aparece en la pantalla como equipo que se va a agregar")
    public void elEquipoNoApareceEnLaPantallaComoEquipoQueSeVaAAgregar(String nombreEquipo) {
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(PasosContexto.webDriver);
        assertThat(paginaCreacionLiga.verificarExistenciaListaEquiposAgregados()).isFalse();
    }

    @Given("que la liga {string} existe")
    public void queLaLigaExiste(String nombreLiga) {
        Liga liga = new Liga();
        liga.setId(ID_LIGA_DE_BRONCE);
        liga.setNombre(nombreLiga);
        repositorioLiga.save(liga);
    }

    @When("el usuario {string} entra a la pantalla de Ligas")
    public void elUsuarioEntraALaPantallaDeLigas(String aliasUsuario) {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irAPaginaLigasDisponibles();
    }

    @Then("se le muestra la liga {string} como liga disponible")
    public void seLeMuestraLaLigaComoLigaDisponible(String nombreLiga) {
        PaginaLigasDisponibles paginaLigasDisponibles = new PaginaLigasDisponibles(PasosContexto.webDriver);
        assertThat(paginaLigasDisponibles.buscarLigasDisponibles()).contains(nombreLiga);
    }

    @When("el usuario selecciona la liga {string}")
    public void elUsuarioSeleccionaLaLiga(String nombreLiga) {
        PaginaLigasDisponibles paginaLigasDisponibles = new PaginaLigasDisponibles(PasosContexto.webDriver);
        paginaLigasDisponibles.irALigaDisponible(nombreLiga);
    }

    @Given("el administrador agrego 5 equipos a la liga al crearla")
    public void elAdministradorAgregoEquiposALaLigaAlCrearla() {
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(PasosContexto.webDriver);
        paginaCreacionLiga.agregarEquipo(equipo1.getNombreEquipo());
        paginaCreacionLiga.agregarEquipo(equipo2.getNombreEquipo());
        paginaCreacionLiga.agregarEquipo(equipo3.getNombreEquipo());
        paginaCreacionLiga.agregarEquipo(equipo4.getNombreEquipo());
        paginaCreacionLiga.agregarEquipo(equipo5.getNombreEquipo());
    }

    @Given("existen otros 5 equipos")
    public void existenEquipos() {
        usuario1 = this.guardarUsuarioEnBase("usuario1", "Nico", "Rid", "nic@rid.com", "12345678");
        usuario2 = this.guardarUsuarioEnBase("usuario2", "Gas", "Spe", "gas@spe.com", "12345678");
        usuario3 = this.guardarUsuarioEnBase("usuario3", "Marc", "Vil", "marc@vil.com", "12345678");
        usuario4 = this.guardarUsuarioEnBase("usuario4", "Mar", "Med", "mar@med.com", "12345678");
        usuario5 = this.guardarUsuarioEnBase("usuario5", "Gian", "Bel", "gian@bel.com", "12345678");
        equipo1 = this.crearEquipo(usuario1.getId(), "Nico FC");
        equipo2 = this.crearEquipo(usuario2.getId(), "Gas FC");
        equipo3 = this.crearEquipo(usuario3.getId(), "Marc FC");
        equipo4 = this.crearEquipo(usuario4.getId(), "Mar FC");
        equipo5 = this.crearEquipo(usuario5.getId(), "Gian FC");
    }

    @Then("ve la tabla con los {int} equipos agregados")
    public void veLaTablaConLosEquiposAgregados(int cantidadEquipos) {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        List<String> equipos = paginaPantallaLiga.obtenerEquipos();
        assertThat(equipos.size()).isEqualTo(cantidadEquipos);
        assertThat(equipos).containsAll(Lists.list(equipo1.getNombreEquipo(), equipo2.getNombreEquipo(), equipo3.getNombreEquipo(), equipo4.getNombreEquipo(), equipo5.getNombreEquipo()));
    }

    @Then("en la tabla aparece una columna de posiciones")
    public void enLaTablaApareceUnaColumnaDePosiciones() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        assertThat(paginaPantallaLiga.obtenerTextoEncabezadoColumnaPosiciones()).isEqualTo("Posición");
    }

    @And("en la tabla aparece una columna de puntos")
    public void enLaTablaApareceUnaColumnaDePuntos() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        assertThat(paginaPantallaLiga.obtenerTextoEncabezadoColumnaPuntos()).isEqualTo("Puntos");
    }

    @And("en la tabla aparece una columna de goles a favor")
    public void enLaTablaApareceUnaColumnaDeGolesAFavor() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        assertThat(paginaPantallaLiga.obtenerTextoEncabezadoColumnaGolesAFavor()).isEqualTo("GF");
    }

    @And("en la tabla aparece una columna de partidos jugados")
    public void enLaTablaApareceUnaColumnaDePartidosJugados() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        assertThat(paginaPantallaLiga.obtenerTextoEncabezadoColumnaPartidosJugados()).isEqualTo("PJ");
    }

    @And("en la tabla aparece una columna de goles en contra")
    public void enLaTablaApareceUnaColumnaDeGolesEnContra() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        assertThat(paginaPantallaLiga.obtenerTextoEncabezadoColumnaGolesEnContra()).isEqualTo("GE");
    }

    @And("en la tabla aparece una columna de diferencia de goles")
    public void enLaTablaApareceUnaColumnaDeDiferenciaDeGoles() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        assertThat(paginaPantallaLiga.obtenerTextoEncabezadoColumnaDiferenciaDeGoles()).isEqualTo("DG");
    }

    @When("el usuario selecciona la opcion Partidos")
    public void elUsuarioSeleccionaLaOpcionPartidos() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        paginaPantallaLiga.irAPartidos();
    }

    @Then("se le redirige a la pantalla de partidos de la liga {string}")
    public void seLeRedirigeALaPantallaDePartidosDeLaLiga(String nombreLiga) {
        PaginaPartidosLiga paginaPartidosLiga = new PaginaPartidosLiga(PasosContexto.webDriver);
        assertThat(paginaPartidosLiga.obtenerTituloPagina()).isEqualTo("Partidos de la liga: " + nombreLiga);
        assertThat(paginaPartidosLiga.obtenerEncabezadoFechaPartido()).isEqualTo("Fecha");
        assertThat(paginaPartidosLiga.obtenerTextoEncabezadoEquipoLocal()).isEqualTo("Equipo Local");
        assertThat(paginaPartidosLiga.obtenerTextoEncabezadoEquipoVisitante()).isEqualTo("Equipo Visitante");
        assertThat(paginaPartidosLiga.obtenerEncabezadoGolesEquipoLocal()).isNotNull();
        assertThat(paginaPartidosLiga.obtenerEncabezadoGolesEquipoVisitante()).isNotNull();
    }

    @When("el usuario selecciona la opcion Posiciones")
    public void elUsuarioSeleccionaLaOpcionPosiciones() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        paginaPantallaLiga.irAPosiciones();
    }

    @And("el administrador agrego 3 equipos a la liga al crearla")
    public void elAdministradorAgrego3EquiposALaLigaAlCrearla() {
        PaginaCreacionLiga paginaCreacionLiga = new PaginaCreacionLiga(PasosContexto.webDriver);
        paginaCreacionLiga.agregarEquipo(equipo1.getNombreEquipo());
        paginaCreacionLiga.agregarEquipo(equipo2.getNombreEquipo());
        paginaCreacionLiga.agregarEquipo(equipo3.getNombreEquipo());
    }

    @Then("se ven los partidos entre todos los equipos de la liga")
    public void seVenLosPartidosEntreTodosLosEquiposDeLaLiga() {
        PaginaPartidosLiga paginaPartidosLiga = new PaginaPartidosLiga(PasosContexto.webDriver);
        List<String> equiposLocales = paginaPartidosLiga.obtenerEquiposLocales();
        List<String> equiposVisitantes = paginaPartidosLiga.obtenerEquiposVisitantes();
        List<String> golesEquiposLocales = paginaPartidosLiga.obtenerGolesEquiposLocales();
        List<String> golesEquiposVisitantes = paginaPartidosLiga.obtenerGolesEquiposVisitantes();
        equipo1 = this.crearEquipo(usuario1.getId(), "Nico FC");
        equipo2 = this.crearEquipo(usuario2.getId(), "Gas FC");
        equipo3 = this.crearEquipo(usuario3.getId(), "Marc FC");
        assertThat(equiposLocales.get(0)).isEqualTo("Gas FC");
        assertThat(equiposLocales.get(1)).isEqualTo("Nico FC");
        assertThat(equiposLocales.get(2)).isEqualTo("Nico FC");
        assertThat(equiposVisitantes.get(0)).isEqualTo("Marc FC");
        assertThat(equiposVisitantes.get(1)).isEqualTo("Marc FC");
        assertThat(equiposVisitantes.get(2)).isEqualTo("Gas FC");
        golesEquiposLocales.stream().forEach(goles -> assertThat(goles).isEqualTo("-"));
        golesEquiposVisitantes.stream().forEach(goles -> assertThat(goles).isEqualTo("-"));
    }

    @And("hay 1 partido de liga")
    public void hayPartidoDeLiga() {
        PartidoLigaBase partidoLigaBase = new PartidoLigaBase();
        Liga liga = new Liga();
        liga.setId(ID_LIGA_DE_BRONCE);
        partidoLigaBase.setLiga(liga);
        partidoLigaBase.setEquipoLocal(equipo1);
        partidoLigaBase.setEquipoVisitante(equipo2);
        EstadoPartido estadoPartidoPendiente = new EstadoPartido();
        estadoPartidoPendiente.setId(EstadoPartidoEnum.PENDIENTE.getId());
        partidoLigaBase.setEstadoPartido(estadoPartidoPendiente);
        partidoLigaBase.setNumeroFecha(1);
        partidoLiga = repositorioResultadoLiga.save(partidoLigaBase);
    }

    @When("el usuario entra al editor de resultados")
    public void elUsuarioEntraAlEditorDeResultados() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        paginaPantallaLiga.irAPartidos();
        PaginaPartidosLiga paginaPartidosLiga = new PaginaPartidosLiga(PasosContexto.webDriver);
        paginaPartidosLiga.abrirEditorResultadoPrimerPartido();
    }

    @And("carga {int} goles al equipo local")
    public void cargaGolesAlEquipoLocal(int goles) {
        PaginaEditorResultadoLiga paginaEditorResultadoLiga = new PaginaEditorResultadoLiga(PasosContexto.webDriver);
        paginaEditorResultadoLiga.cargarGolesLocal(goles);
    }

    @And("carga {int} goles al equipo visitante")
    public void cargaGolesAlEquipoVisitante(int goles) {
        PaginaEditorResultadoLiga paginaEditorResultadoLiga = new PaginaEditorResultadoLiga(PasosContexto.webDriver);
        paginaEditorResultadoLiga.cargarGolesVisitante(goles);
    }

    @And("guarda el resultado")
    public void guardaElResultado() {
        PaginaEditorResultadoLiga paginaEditorResultadoLiga = new PaginaEditorResultadoLiga(PasosContexto.webDriver);
        paginaEditorResultadoLiga.clickEnActualizarResultado();
    }

    @Then("se ve el resultado actualizado")
    public void seVeElResultadoActualizado() {
        PaginaPartidosLiga paginaPartidosLiga = new PaginaPartidosLiga(PasosContexto.webDriver);
        paginaPartidosLiga.verificarQueSeHayaActualizadoElPartidoConGolesLocalYVisitante(3, 2);
    }

    @Then("se muestra mensaje de que no puede haber campos nulos")
    public void seMuestraMensajeDeQueNoPuedeHaberCamposNulos() {
        PaginaEditorResultadoLiga paginaEditorResultadoLiga = new PaginaEditorResultadoLiga(PasosContexto.webDriver);
        assertThat(paginaEditorResultadoLiga.obtenerTextoErrorCamposNulos()).isEqualTo("No puede haber campos nulos.");
    }

    @Then("los equipos tienen los datos actualizados en la tabla")
    public void losEquiposTienenLosDatosActualizadosEnLaTabla() {
        PaginaPantallaLiga paginaPantallaLiga = new PaginaPantallaLiga(PasosContexto.webDriver);
        assertThat(paginaPantallaLiga.obtenerNumeroPrimeraPosicion()).isEqualTo("1");
        assertThat(paginaPantallaLiga.obtenerColumnaNombreEquipoPrimeraPosicion()).isEqualTo("Nico FC");
        assertThat(paginaPantallaLiga.obtenerColumnaPartidosJugadosPrimeraPosicion()).isEqualTo("1");
        assertThat(paginaPantallaLiga.obtenerColumnaGolesAFavorPrimeraPosicion()).isEqualTo("3");
        assertThat(paginaPantallaLiga.obtenerColumnaGolesEnContraPrimeraPosicion()).isEqualTo("2");
        assertThat(paginaPantallaLiga.obtenerColumnaDiferenciaDeGolesPrimeraPosicion()).isEqualTo("1");
        assertThat(paginaPantallaLiga.obtenerColumnaPuntosPrimeraPosicion()).isEqualTo("3");
    }

    @And("los equipos estan vinculados a la liga")
    public void losEquiposEstanVinculadosALaLiga() {
        LigaEquipos ligaEquipos1 = new LigaEquipos();
        ligaEquipos1.setIdEquipo(2);
        ligaEquipos1.setIdLiga(1);
        LigaEquipos ligaEquipos2 = new LigaEquipos();
        ligaEquipos2.setIdEquipo(3);
        ligaEquipos2.setIdLiga(1);

        repositorioLigaEquipos.saveAll(Lists.list(ligaEquipos1, ligaEquipos2));
    }

    @Given("hay 1 partido de liga cargado")
    public void hayPartidoDeLigaCargado() {
        PartidoLigaBase partidoLigaBase = new PartidoLigaBase();
        Liga liga = new Liga();
        liga.setId(ID_LIGA_DE_BRONCE);
        partidoLigaBase.setLiga(liga);
        partidoLigaBase.setEquipoLocal(equipo1);
        partidoLigaBase.setEquipoVisitante(equipo2);
        EstadoPartido estadoPartidoFinalizado = new EstadoPartido();
        estadoPartidoFinalizado.setId(EstadoPartidoEnum.FINALIZADO.getId());
        partidoLigaBase.setEstadoPartido(estadoPartidoFinalizado);
        partidoLigaBase.setNumeroFecha(1);
        partidoLigaBase.setGolesEquipoLocal(3);
        partidoLigaBase.setGolesEquipoVisitante(1);
        partidoLiga = repositorioResultadoLiga.save(partidoLigaBase);
    }

    @Then("se ve el resultado actualizado en el editor de resultado")
    public void seVeElResultadoActualizadoEnElEditorDeResultado() {
        PaginaEditorResultadoLiga paginaEditorResultadoLiga = new PaginaEditorResultadoLiga(PasosContexto.webDriver);
        assertThat(paginaEditorResultadoLiga.obtenerGolesEquipoLocal()).isEqualTo("3");
        assertThat(paginaEditorResultadoLiga.obtenerGolesEquipoVisitante()).isEqualTo("1");
    }

    @And("se muestra que el usuario {string} actualizo el partido")
    public void seMuestraQueElUsuarioActualizoElPartido(String aliasUsuario) {
        PaginaEditorResultadoLiga paginaEditorResultadoLiga = new PaginaEditorResultadoLiga(PasosContexto.webDriver);
        assertThat(paginaEditorResultadoLiga.obtenerUltimoUsuarioEditor()).isEqualTo("Actualizado por " + aliasUsuario);
    }

    @Then("se verifica que la liga es de la temporada {int}")
    public void seVerificaQueLaLigaEsDeLaTemporada(int numeroTemporada) {
        Liga liga = repositorioLiga.findById(ID_LIGA_DE_BRONCE).get();
        assertThat(liga.getTemporada().getNumero()).isEqualTo(numeroTemporada);
    }

    @Then("se le muestran solo partidos de su equipo {string}")
    public void seLeMuestranSoloPartidosDeSuEquipo(String nombreEquipo) {
        PaginaPartidosLiga paginaPartidosLiga = new PaginaPartidosLiga(PasosContexto.webDriver);
        List<String> equiposLocales = paginaPartidosLiga.obtenerEquiposLocales();
        List<String> equiposVisitantes = paginaPartidosLiga.obtenerEquiposVisitantes();
        List<String> equiposPrimeraFecha = Lists.list(equiposLocales.get(0), equiposVisitantes.get(0));
        List<String> equiposSegundaFecha = Lists.list(equiposLocales.get(1), equiposVisitantes.get(1));
        List<String> equiposTerceraFecha = Lists.list(equiposLocales.get(2), equiposVisitantes.get(2));
        assertThat(equiposPrimeraFecha).contains(nombreEquipo);
        assertThat(equiposSegundaFecha).contains(nombreEquipo);
        assertThat(equiposTerceraFecha).contains(nombreEquipo);
    }

    @When("el usuario filtra los partidos por el equipo {string}")
    public void elUsuarioFiltraPorElEquipo(String nombreEquipo) {
        PaginaPartidosLiga paginaPartidosLiga = new PaginaPartidosLiga(PasosContexto.webDriver);
        paginaPartidosLiga.filtrarPartidosPorElEquipo(nombreEquipo);
    }
}
