package stepDefinitions;

import com.tesis.futpal.modelos.temporada.PeriodoTemporada;
import com.tesis.futpal.modelos.temporada.Temporada;
import com.tesis.futpal.repositorios.RepositorioPeriodoTemporada;
import com.tesis.futpal.repositorios.RepositorioTemporada;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import paginas.PaginaBase;
import paginas.PaginaEditorEstadisticasLiga;
import paginas.PaginaIngresoUsuario;
import paginas.PaginaPantallaInicialUsuario;

public class PasosTemporada extends PasosComunes {

    @Autowired
    private RepositorioTemporada repositorioTemporada;

    @Autowired
    private RepositorioPeriodoTemporada repositorioPeriodoTemporada;

    @Given("se está en la temporada {int} y periodo {string}")
    public void seEstáEnLaTemporadaYPeriodo(int numeroTemporada, String nombrePeriodo) {
        Temporada temporada = new Temporada();
        temporada.setNumero(numeroTemporada);
        temporada.setActual(true);
        PeriodoTemporada periodoTemporada = repositorioPeriodoTemporada.findByNombre(nombrePeriodo);
        temporada.setPeriodoTemporada(periodoTemporada);

        repositorioTemporada.save(temporada);
    }

    @Then("se le muestra que la temporada actual es {string} temporada {int}")
    public void seLeMuestraQueLaTemporadaActualEsLaNumeroYPeriodo(String nombrePeriodo, int numeroTemporada) {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.verificarQueElPeriodoYTemporadaMostradoEs(nombrePeriodo,numeroTemporada);
    }

    @When("avanza de periodo")
    public void avanzaDePeriodo() {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.avanzarPeriodo();
    }
}
