package stepDefinitions;

import com.tesis.futpal.modelos.equipo.EstadoEquipoEnum;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipo;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import com.tesis.futpal.repositorios.RepositorioJugador;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import paginas.PaginaIngresoUsuario;
import paginas.PaginaPantallaInicialUsuario;
import paginas.PaginaPantallaPlantel;
import paginas.PaginaPantallaPrincipal;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PasosMiPlantel extends PasosComunes {

    public static final long ID_EQUIPO_FERG = 1L;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private RepositorioJugador repositorioJugador;

    @Given("que el equipo del usuario {string} con id {int} esta en estado inicial")
    public void elEquipoNoEstaAprobado(String alias, Integer id) {
        Equipo equipo = new Equipo();
        equipo.setNombreEquipo("Ferg FC");
        equipo.setIdUsuario(id.longValue());
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.INICIAL.getId());
        equipo.setEstadoEquipo(estadoEquipo);
        repositorioEquipo.save(equipo);
    }

    @And("el usuario {string} esta en la pantalla del plantel")
    public void elUsuarioEstaEnLaPantallaDelPlantel(String alias) {
        WebDriver driver = PasosContexto.webDriver;
        driver.get(ConfiguracionSelenium.getUrlBase());
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        paginaPantallaPrincipal.irAPantallaLogin();
        PaginaIngresoUsuario paginaIngresoUsuario = new PaginaIngresoUsuario(driver);
        paginaIngresoUsuario.loguearUsuario(alias, "12345678");
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(driver);
        paginaPantallaInicialUsuario.irAPaginaMiPlantel();
    }

    @When("el usuario agrega a un jugador {string} con su nombre, fecha nacimiento, valor, salario, media y id de transfermarkt")
    public void elUsuarioAgregaAUnJugador(String nombre) {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        paginaPantallaPlantel.agregarJugador(nombre, LocalDate.of(1987, 1, 1), 100000000, 20000000, 93, 957);
    }

    @Then("se muestra al jugador {string} como agregado")
    public void seMuestraAlJugadorComoAgregado(String nombre) {
        Jugador jugador = repositorioJugador.findByNombre(nombre);
        assertThat(jugador.getNombre()).isEqualTo(nombre);
        assertThat(jugador.getFechaNacimiento().getYear()).isEqualTo(1987);
        assertThat(jugador.getValor()).isEqualTo(100000000);
        assertThat(jugador.getSalario()).isEqualTo(20000000);
        assertThat(jugador.getMedia()).isEqualTo(93);

        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        assertThat(paginaPantallaPlantel.obtenerNombrePrimerJugador()).isEqualTo(nombre);
    }

    @Then("se muestra mensaje de que el equipo esta pendiente de aprobacion")
    public void seMuestraMensajeDeQueElEquipoNoEstaAprobado() {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        assertThat(paginaPantallaPlantel.obtenerTextoMensajeEstadoEquipo()).isEqualTo(EstadoEquipoEnum.PENDIENTE_APROBACION.getDescripcion());
    }

    @Given("el equipo del usuario {string} no tiene nombre inicial registrado")
    public void elEquipoDelUsuarioNoTieneNombreInicialRegistrado(String nombreUsuario) {
        repositorioEquipo.deleteAll();
        PasosContexto.webDriver.navigate().refresh();
    }

    @Then("no le permite agregar jugadores")
    public void noLePermiteAgregarJugadores() {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        assertThat(paginaPantallaPlantel.estaBotonAgregarJugadorHabilitado()).isFalse();
    }

    @Then("le muestra mensaje de que no puede agregar jugadores hasta que no se le asigne un nombre inicial al equipo")
    public void leMuestraMensajeDeQueNoPuedeAgregarJugadoresHastaQueNoSeLeAsigneUnNombreInicialAlEquipo() {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        assertThat(paginaPantallaPlantel.obtenerTextoEquipo()).isEqualTo("No posee un equipo asociado aún. Ingrese el nombre que desea para su equipo en \"Editar Equipo\" y confirme para asociarlo.");
    }

    @Given("el usuario tiene 2 jugadores agregados")
    public void elUsuarioTieneJugadoresAgregados() {
        Equipo equipo = new Equipo();
        equipo.setId(ID_EQUIPO_FERG);

        this.agregarJugadorAEquipo("Leo Ponzio", LocalDate.of(1982, 1, 1), equipo, 78, 50000, 75000, 23L);
        this.agregarJugadorAEquipo("Jonatan Maidana", LocalDate.of(1985, 1, 1), equipo, 77, 40000, 85000, 13L);
    }

    @Then("se ven los 2 jugadores agregados")
    public void seVenLosJugadoresAgregados() {
        PasosContexto.webDriver.navigate().refresh();

        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        assertThat(paginaPantallaPlantel.obtenerNombrePrimerJugador()).isEqualTo("Leo Ponzio");
        assertThat(paginaPantallaPlantel.obtenerNombreSegundoJugador()).isEqualTo("Jonatan Maidana");
    }

    @Given("el equipo del usuario {string} con id {int} esta rechazado")
    public void elEquipoDelUsuarioConIdEstaRechazado(String arg0, Integer id) {
        Equipo equipo = new Equipo();
        equipo.setNombreEquipo("Ferg FC");
        equipo.setIdUsuario(id.longValue());
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());
        equipo.setEstadoEquipo(estadoEquipo);
        repositorioEquipo.save(equipo);
    }

    @When("el usuario selecciona solicitar aprobacion")
    public void elUsuarioSeleccionaSolicitarAprobacion() {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        paginaPantallaPlantel.solicitarAprobacionEquipo();
    }

    @Then("se muestra mensaje de que puede agregar jugadores y solicitar aprobacion al terminar")
    public void seMuestraMensajeDeQuePuedeAgregarJugadoresYSolicitarAprobacionAlTerminar() {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        assertThat(paginaPantallaPlantel.obtenerTextoMensajeEstadoEquipo()).isEqualTo(EstadoEquipoEnum.INICIAL.getDescripcion());
    }

    @When("va a la seccion de jugadores prestados")
    public void vaALaSeccionDeJugadoresPrestados() {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        paginaPantallaPlantel.irAJugadoresPrestados();
    }

    @Then("se ve al jugador {string} prestado en {string} hasta {string} temporada {int}")
    public void seVeAlJugadorPrestadoEnHastaTemporada(String nombreJugador, String equipo, String periodo, int numeroTemporada) {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);

        List<String> nombresJugadores = paginaPantallaPlantel.obtenerNombreJugadores();
        assertThat(nombresJugadores).contains(nombreJugador);

        List<String> temporadasPrestamos = paginaPantallaPlantel.obtenerNumerosTemporadas();
        assertThat(temporadasPrestamos).contains(String.valueOf(numeroTemporada));

        List<String> periodosPrestamos = paginaPantallaPlantel.obtenerNombrePeriodosVueltaPrestamo();
        assertThat(periodosPrestamos).contains(String.valueOf(periodo));
    }
}
