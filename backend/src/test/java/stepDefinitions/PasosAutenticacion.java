package stepDefinitions;

import com.tesis.futpal.modelos.usuario.Rol;
import com.tesis.futpal.modelos.usuario.TipoRol;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import paginas.PaginaIngresoUsuario;
import paginas.PaginaPantallaPrincipal;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public class PasosAutenticacion {

    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private PasswordEncoder encoder;

    private Long idUsuario;

    @Given("existe el usuario {string}")
    public void existeUnUsuario(String nombreUsuario) {
        Usuario usuario = new Usuario();
        usuario.setAliasUsuario(nombreUsuario);
        usuario.setNombreUsuario("Fernando");
        usuario.setApellidoUsuario("Gainey");
        usuario.setMailUsuario("fer@mail.com");
        usuario.setContraseniaUsuario(encoder.encode("12345678"));

        Rol rolUsuario = new Rol();
        rolUsuario.setId(TipoRol.ROL_USUARIO.getId());
        List<Rol> rolesUsuario = new ArrayList<>();
        rolesUsuario.add(rolUsuario);
        usuario.setRoles(rolesUsuario);

        repositorioUsuario.save(usuario);
    }

    @And("el usuario {string} esta logueado")
    public void elUsuarioEstaLogueado(String alias) {
        WebDriver driver = PasosContexto.webDriver;
        driver.get(ConfiguracionSelenium.getUrlBase());
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        paginaPantallaPrincipal.irAPantallaLogin();
        PaginaIngresoUsuario paginaIngresoUsuario = new PaginaIngresoUsuario(driver);
        paginaIngresoUsuario.loguearUsuario(alias, "12345678");
    }

    @And("el usuario {string} es administrador")
    @Transactional
    public void elUsuarioEsAdministrador(String aliasUsuario) {
        Usuario usuario = repositorioUsuario.findByAliasUsuario(aliasUsuario).get(0);
        Rol rolUsuario = new Rol();
        rolUsuario.setId(TipoRol.ROL_ADMINISTRADOR.getId());
        List<Rol> rolesUsuario = usuario.getRoles();
        rolesUsuario.add(rolUsuario);
        usuario.setRoles(rolesUsuario);

        repositorioUsuario.save(usuario);
    }
}
