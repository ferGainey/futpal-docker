package stepDefinitions;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipoEnum;
import com.tesis.futpal.modelos.plantel.EstadoJugador;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.usuario.Rol;
import com.tesis.futpal.modelos.usuario.TipoRol;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import com.tesis.futpal.repositorios.RepositorioJugador;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PasosComunes {

    private static final Long ID_ESTADO_JUGADOR_DISPONIBLE = 1L;
    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private RepositorioJugador repositorioJugador;
    @Autowired
    private PasswordEncoder encoder;

    public Usuario guardarUsuarioEnBase(String alias, String nombre, String apellido, String mail, String contrasenia) {
        Usuario usuario = new Usuario();
        usuario.setAliasUsuario(alias);
        usuario.setNombreUsuario(nombre);
        usuario.setApellidoUsuario(apellido);
        usuario.setMailUsuario(mail);
        usuario.setContraseniaUsuario(encoder.encode(contrasenia));
        return repositorioUsuario.save(usuario);
    }

    public Equipo crearEquipo(Long id, String nombreEquipo) {
        Equipo equipo = new Equipo();
        equipo.setNombreEquipo(nombreEquipo);
        equipo.setIdUsuario(id);
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.APROBADO.getId());
        equipo.setEstadoEquipo(estadoEquipo);
        return repositorioEquipo.save(equipo);
    }

    public Jugador agregarJugadorAEquipo(String nombreJugador, LocalDate fechaNacimiento, Equipo equipo, int media, int salario, int valor, Long idTransfermarkt) {
        Jugador jugador = new Jugador();
        jugador.setNombre(nombreJugador);
        jugador.setFechaNacimiento(fechaNacimiento);;
        jugador.setEquipo(equipo);
        jugador.setMedia(media);
        jugador.setSalario(salario);
        jugador.setValor(valor);
        jugador.setActivo(true);
        EstadoJugador disponible = new EstadoJugador();
        disponible.setId(ID_ESTADO_JUGADOR_DISPONIBLE);
        jugador.setEstadoJugador(disponible);
        jugador.setIdTransfermarkt(idTransfermarkt);
        return repositorioJugador.save(jugador);
    }

    public void asignarRolUsuario(Usuario usuario) {
        Rol rolUsuario = new Rol();
        rolUsuario.setId(TipoRol.ROL_USUARIO.getId());
        List<Rol> rolesUsuario = new ArrayList<>();
        rolesUsuario.add(rolUsuario);
        usuario.setRoles(rolesUsuario);

        repositorioUsuario.save(usuario);
    }
}
