package stepDefinitions;

import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import paginas.PaginaPantallaPrincipal;
import paginas.PaginaRegistroUsuario;

import static org.assertj.core.api.Assertions.assertThat;


public class PasosRegistroUsuario {

    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private RepositorioEquipo repositorioEquipo;


    @Given("el usuario esta en la pantalla de sign up")
    public void elUsuarioEstaEnLaPantallaDeSignUp() {
        WebDriver driver = PasosContexto.webDriver;
        driver.get(ConfiguracionSelenium.getUrlBase());
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        paginaPantallaPrincipal.irAPantallaSignUp();
    }

    @When("crea usuario con datos validos y alias {string}")
    public void creaUsuarioConDatosValidosYAlias(String aliasUsuario) {
        WebDriver driver = PasosContexto.webDriver;
        PaginaRegistroUsuario paginaRegistroUsuario = new PaginaRegistroUsuario(driver);
        String aliasValido = aliasUsuario;
        String nombreValido = "Vigorous";
        String apellidoValido = "Williams";
        String mailValido = "mail@mail.com";
        String contraseniaValida = "contrasenia";
        paginaRegistroUsuario.crearUsuario(aliasValido, nombreValido, apellidoValido, mailValido, contraseniaValida);
    }

    @And("se creo el usuario correspondiente con alias {string}")
    public void seCreoElUsuarioCorrespondienteConAlias(String aliasUsuario) {
        Usuario usuarioEncontrado = this.repositorioUsuario.findByAliasUsuario(aliasUsuario).get(0);
        assertThat(usuarioEncontrado.getAliasUsuario()).isEqualTo(aliasUsuario);
        //TODO: verificar los demas campos. Ver la manera de pasar prolijo los multiples campos
    }

    @Then("muestra alerta de que se creo exitosamente")
    public void muestraAlertaDeQueSeCreoExitosamente() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaRegistroUsuario paginaRegistroUsuario = new PaginaRegistroUsuario(driver);
        assertThat(paginaRegistroUsuario.existePopupUsuarioCreado()).isTrue();
    }

    @Given("hay un usuario creado con alias {string}")
    public void hayUnUsuarioCreadoConAlias(String alias) {
        Usuario usuarioExistente = new Usuario();
        usuarioExistente.setAliasUsuario(alias);
        usuarioExistente.setNombreUsuario("Pedro");
        usuarioExistente.setApellidoUsuario("Testing");
        usuarioExistente.setMailUsuario("pedro@testing.com");
        usuarioExistente.setContraseniaUsuario("12345678");
        repositorioUsuario.save(usuarioExistente);
    }

    @Then("no se creo el usuario con alias {string}")
    public void noSeCreoElUsuarioCorrespondiente(String alias) {
        assertThat(this.repositorioUsuario.findByAliasUsuario(alias).size()).isEqualTo(1);
    }

    @And("muestra alerta de que no se pudo crear el usuario por alias existente")
    public void muestraAlertaDeQueNoSePudoCrearElUsuario() {
        WebDriver driver = PasosContexto.webDriver;//ConfiguracionSelenium.getDriver();
        PaginaRegistroUsuario paginaRegistroUsuario = new PaginaRegistroUsuario(driver);
        assertThat(paginaRegistroUsuario.existeAlertaUsuarioNoCreadoConMensajeAliasExistente()).isTrue();
    }

    @Given("hay un usuario creado con mail {string}")
    public void hayUnUsuarioCreadoConMail(String mailExistente) {
        Usuario usuarioExistente = new Usuario();
        usuarioExistente.setAliasUsuario("unAlias");
        usuarioExistente.setNombreUsuario("Pedro");
        usuarioExistente.setApellidoUsuario("Testing");
        usuarioExistente.setMailUsuario(mailExistente);
        usuarioExistente.setContraseniaUsuario("12345678");
        repositorioUsuario.save(usuarioExistente);
    }

    @When("crea usuario con datos validos y mail {string}")
    public void creaUsuarioConDatosValidosYMail(String mailUsuarioACrear) {
        WebDriver driver = PasosContexto.webDriver;
        PaginaRegistroUsuario paginaRegistroUsuario = new PaginaRegistroUsuario(driver);
        String aliasValido = "otroAlias";
        String nombreValido = "Vigorous";
        String apellidoValido = "Williams";
        String mailValido = mailUsuarioACrear;
        String contraseniaValida = "contrasenia";
        paginaRegistroUsuario.crearUsuario(aliasValido, nombreValido, apellidoValido, mailValido, contraseniaValida);
    }

    @Then("no se creo el usuario con mail {string}")
    public void noSeCreoElUsuarioConMail(String mailUsuarioACrear) {
        assertThat(this.repositorioUsuario.findByMailUsuario(mailUsuarioACrear).size()).isEqualTo(1);
    }

    @And("muestra alerta de que no se pudo crear el usuario por mail existente")
    public void muestraAlertaDeQueNoSePudoCrearElUsuarioPorMailExistente() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaRegistroUsuario paginaRegistroUsuario = new PaginaRegistroUsuario(driver);
        assertThat(paginaRegistroUsuario.existeAlertaUsuarioNoCreadoConMensajeMailExistente()).isTrue();
    }
}
