package stepDefinitions;

import com.tesis.futpal.modelos.equipo.EstadoEquipoEnum;
import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipo;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import com.tesis.futpal.repositorios.RepositorioJugador;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import paginas.*;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PasosPlanteles extends PasosComunes {

    public static final long ID_TRANSFERMARKT_JUGADOR_1 = 9923L;
    private Usuario usuarioTitov;
    private Equipo equipoTitov;
    @Autowired
    private RepositorioJugador repositorioJugador;
    @Autowired
    private RepositorioEquipo repositorioEquipo;

    @And("el usuario {string} esta en la pantalla de planteles")
    public void elUsuarioEstaEnLaPantallaDePlanteles(String alias) {
        WebDriver driver = PasosContexto.webDriver;
        driver.get(ConfiguracionSelenium.getUrlBase());
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        paginaPantallaPrincipal.irAPantallaLogin();
        PaginaIngresoUsuario paginaIngresoUsuario = new PaginaIngresoUsuario(driver);
        paginaIngresoUsuario.loguearUsuario(alias, "12345678");
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(driver);
        paginaPantallaInicialUsuario.irAPaginaPlanteles();
    }

    @When("selecciona el equipo {string} del combo")
    public void seleccionaElEquipoDelCombo(String equipo) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        paginaPlanteles.seleccionarEquipo(equipo);
    }


    @And("el equipo {string} tiene a los jugadores {string} y {string}")
    public void elEquipoTieneALosJugadoresY(String equipo, String nombreJugador1, String nombreJugador2) {
        Equipo equipoRecibido = repositorioEquipo.findByNombreEquipo(equipo).get(0);
        this.agregarJugadorAEquipo(nombreJugador1, LocalDate.of(1982, 1, 1), equipoRecibido, 78, 50000, 75000, ID_TRANSFERMARKT_JUGADOR_1);
        this.agregarJugadorAEquipo(nombreJugador2, LocalDate.of(1985, 1, 1), equipoRecibido, 77, 40000, 85000, 992123L);

    }

    @And("existe el equipo {string} con usuario {string}")
    public void existeElEquipo(String nombreEquipo, String aliasUsuario) {
        usuarioTitov = this.guardarUsuarioEnBase(aliasUsuario, "Tomas", "Dima", "tomas@dima.com", "12345678");
        this.asignarRolUsuario(usuarioTitov);
        equipoTitov = this.crearEquipo(usuarioTitov.getId(), nombreEquipo);
    }

    @Then("se muestran los jugadores del equipo Dave Holland")
    public void seMuestranLosJugadoresDelEquipoDaveHolland() {
        PaginaPlanteles paginaPantallaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        assertThat(paginaPantallaPlanteles.obtenerNombrePrimerJugador()).isEqualTo("Pepe");
        assertThat(paginaPantallaPlanteles.obtenerNombreSegundoJugador()).isEqualTo("Mane");
    }

    @And("el equipo {string} esta pendiente de aprobacion")
    public void elEquipoEstaPendienteDeAprobacion(String nombreEquipo) {
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.PENDIENTE_APROBACION.getId());
        equipoTitov.setEstadoEquipo(estadoEquipo);
        repositorioEquipo.save(equipoTitov);
    }

    @And("el equipo {string} esta rechazado")
    public void elEquipoEstaRechazado(String nombreEquipo) {
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.RECHAZADO.getId());
        equipoTitov.setEstadoEquipo(estadoEquipo);
        repositorioEquipo.save(equipoTitov);
    }

    @And("el equipo {string} esta en estado inicial")
    public void elEquipoEstaEnEstadoInicial(String nombreEquipo) {
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.INICIAL.getId());
        equipoTitov.setEstadoEquipo(estadoEquipo);
        repositorioEquipo.save(equipoTitov);
    }

    @When("selecciona en el combo de planteles pendientes de aprobacion el equipo {string}")
    public void seleccionaEnElComboDePlantelesPendientesDeAprobacionElEquipo(String nombreEquipo) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        paginaPlanteles.seleccionarEquipoPendienteAprobacionDelCombo(nombreEquipo);
    }

    @And("el administrador confirma el equipo")
    public void elAdministradorConfirmaElEquipo() {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        paginaPlanteles.confirmarEquipoPendienteAprobacion();
    }

    @Then("el equipo {string} no aparece mas como pendiente de aprobacion")
    public void elEquipoYidArmyNoApareceMasComoPendienteDeAprobacion(String equipo) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        List<String> equiposPendienteAprobacion = paginaPlanteles.obtenerEquiposPendienteAprobacion();
        assertThat(equiposPendienteAprobacion).doesNotContain(equipo);
    }

    @Then("el equipo {string} aparece en el combo de planteles disponibles")
    public void elEquipoApareceEnElComboDePlantelesDisponibles(String equipo) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        List<String> equiposAprobados = paginaPlanteles.obtenerEquiposAprobados();
        assertThat(equiposAprobados).contains(equipo);
    }

    @And("el administrador rechaza el equipo")
    public void elAdministradorRechazaElEquipo() {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        paginaPlanteles.rechazarEquipoPendienteAprobacion();
    }

    @Then("el equipo {string} no aparece en el combo de planteles disponibles")
    public void elEquipoNoApareceEnElComboDePlantelesDisponibles(String equipo) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        List<String> equiposAprobados = paginaPlanteles.obtenerEquiposAprobados();
        assertThat(equiposAprobados).doesNotContain(equipo);
    }

    @When("el usuario borra al jugador {string} en la pantalla de Mi Plantel")
    public void elUsuarioBorraAlJugadorEnLaPantallaDeMiPlantel(String nombreJugador) {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        //el primer jugador es Pepe
        //ToDo ver cómo puedo buscar en la tabla por nombre de jugador
        paginaPantallaPlantel.borrarPrimerJugador(nombreJugador);
    }

    @Then("el jugador {string} no aparece mas en el plantel en la pantalla de Mi Plantel")
    public void elJugadorNoApareceMasEnElPlantelEnLaPantallaDeMiPlantel(String nombreJugador) {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        List<String> nombresJugadores = paginaPantallaPlantel.obtenerNombreJugadores();
        assertThat(nombresJugadores).doesNotContain(nombreJugador);
    }

    @When("el administrador borra al jugador {string} en la pantalla de planteles")
    public void elAdministradorBorraAlJugadorEnLaPantallaDePlanteles(String nombreJugador) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        //el primer jugador es Pepe
        //ToDo ver cómo puedo buscar en la tabla por nombre de jugador
        paginaPlanteles.borrarPrimerJugador(nombreJugador);
    }

    @Then("el jugador {string} no aparece mas en el plantel de {string} en la pantalla de planteles")
    public void elJugadorNoApareceMasEnElPlantelDeEnLaPantallaDePlanteles(String nombreJugador, String nombreEquipo) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        List<String> nombresJugadores = paginaPlanteles.obtenerNombreJugadores();
        assertThat(nombresJugadores).doesNotContain(nombreJugador);
    }

    @Then("se muestra mensaje que se confirmo equipo")
    public void seMuestraMensajeQueSeConfirmoEquipo() {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        String textoPopupEquipoAprobado = paginaPlanteles.obtenerTextoConfirmacionEquipoAprobado();
        assertThat(textoPopupEquipoAprobado).isEqualTo("Se ha aprobado el equipo exitosamente.");
    }

    @And("escribe como motivo de rechazo {string}")
    public void escribeComoMotivoDeRechazo(String motivoRechazo) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        paginaPlanteles.escribirMotivoRechazoEquipo(motivoRechazo);
    }

    @Then("el usuario ve que el motivo de rechazo del equipo es {string}")
    public void elUsuarioVeQueElMotivoDeRechazoDelEquipoEs(String motivoRechazoEsperado) {
        PaginaPantallaPlantel paginaPantallaPlantel = new PaginaPantallaPlantel(PasosContexto.webDriver);
        String textoMotivoCambioEstado = paginaPantallaPlantel.obtenerTextoMotivoCambioEstado();
        String textoEsperado = "Motivo: " + motivoRechazoEsperado;
        assertThat(textoMotivoCambioEstado).isEqualTo(textoEsperado);
    }

    @When("se edita al jugador {string} en la pantalla de planteles con nombre {string}")
    public void seEditaAlJugadorEnLaPantallaDePlantelesConNombre(String nombreJugadorOriginal, String nombreJugadorEditado) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        paginaPlanteles.editarNombrePrimerJugador(nombreJugadorEditado);
    }

    @Then("el jugador {string} aparece en el plantel en la pantalla de planteles")
    public void elJugadorApareceEnElPlantelDeEnLaPantallaDePlanteles(String nombreJugador) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        List<String> nombresJugadores = paginaPlanteles.obtenerNombreJugadores();
        assertThat(nombresJugadores).contains(nombreJugador);
    }

    @Given("que el equipo del usuario {string} con id {int} esta en estado aprobado")
    public void queElEquipoDelUsuarioConIdEstaEnEstadoAprobado(String alias, Integer id) {
        Equipo equipo = new Equipo();
        equipo.setNombreEquipo("Ferg FC");
        equipo.setIdUsuario(id.longValue());
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.APROBADO.getId());
        equipo.setEstadoEquipo(estadoEquipo);
        repositorioEquipo.save(equipo);
    }

    @When("va a la seccion de busqueda de jugadores")
    public void vaALaSeccionDeBusquedaDeJugadores() {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        paginaPlanteles.irABusquedaDeJugadores();
    }

    @And("filtra por id de transfermarkt {int} y nombre {string}")
    public void filtraPorIdDeTransfermarktYNombre(int idTransfermarkt, String nombreJugador) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);
        paginaPlanteles.filtrarJugadorPorIdTransfermarkt(idTransfermarkt);
        paginaPlanteles.filtrarJugadorPorNombre(nombreJugador);
        paginaPlanteles.hacerBusquedaJugadoresFiltrados();
    }

    @Then("se ve al jugador {string} en el equipo {string}")
    public void seVeAlJugadorEnElEquipo(String nombreJugador, String nombreEquipo) {
        PaginaPlanteles paginaPlanteles = new PaginaPlanteles(PasosContexto.webDriver);

        List<String> nombresJugadores = paginaPlanteles.obtenerNombreJugadoresFiltrados();
        assertThat(nombresJugadores).contains(nombreJugador);

        List<String> temporadasPrestamos = paginaPlanteles.obtenerEquiposJugadoresFiltrados();
        assertThat(temporadasPrestamos).contains(nombreEquipo);
    }
}
