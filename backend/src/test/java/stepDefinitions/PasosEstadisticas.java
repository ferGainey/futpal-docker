package stepDefinitions;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.estadisticas.*;
import com.tesis.futpal.modelos.liga.*;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioLigaEquipos;
import com.tesis.futpal.repositorios.RepositorioLigas;
import com.tesis.futpal.repositorios.RepositorioResultadoLiga;
import com.tesis.futpal.repositorios.estadisticas.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import paginas.PaginaEditorEstadisticasLiga;
import paginas.PaginaEstadisticasLiga;
import paginas.PaginaInformacionPartido;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class PasosEstadisticas extends PasosComunes {


    private Usuario usuario1;
    private Usuario usuario2;
    private Usuario usuario3;
    private Usuario usuario4;
    private Equipo equipo1;
    private Equipo equipo2;
    private Equipo equipo3;
    private Equipo equipo4;
    private Jugador jugador1;
    private Jugador jugador2;
    private Jugador jugador3;
    private Jugador jugador4;
    private Jugador jugador5;
    private Jugador jugador6;
    @Autowired
    private RepositorioEstadisticaGol repositorioEstadisticaGol;
    @Autowired
    private RepositorioEstadisticaTarjetaAmarilla repositorioEstadisticaTarjetaAmarilla;
    @Autowired
    private RepositorioEstadisticaTarjetaRoja repositorioEstadisticaTarjetaRoja;
    @Autowired
    private RepositorioEstadisticaLesion repositorioEstadisticaLesion;
    @Autowired
    private RepositorioEstadisticaMvp repositorioEstadisticaMvp;
    @Autowired
    private RepositorioLigas repositorioLiga;
    @Autowired
    private RepositorioLigaEquipos repositorioLigaEquipos;
    @Autowired
    private RepositorioResultadoLiga repositorioResultadoLiga;
    private Liga ligaCreada;

    @Given("hay 4 equipos registrados en la liga {string} con jugadores")
    public void hay4EquiposRegistradosEnLaLigaConJugadores(String nombreLiga) {
        Liga liga = new Liga();
        liga.setNombre(nombreLiga);
        ligaCreada = repositorioLiga.save(liga);
        usuario1 = this.guardarUsuarioEnBase("usuario1", "Nico", "Rid", "nic@rid.com", "12345678");
        usuario2 = this.guardarUsuarioEnBase("usuario2", "Gas", "Spe", "gas@spe.com", "12345678");
        usuario3 = this.guardarUsuarioEnBase("usuario3", "Marc", "Vil", "marc@vil.com", "12345678");
        usuario4 = this.guardarUsuarioEnBase("usuario4", "Mar", "Med", "mar@med.com", "12345678");
        equipo1 = this.crearEquipo(usuario1.getId(), "Nico FC");
        equipo2 = this.crearEquipo(usuario2.getId(), "Gas FC");
        equipo3 = this.crearEquipo(usuario3.getId(), "Marc FC");
        equipo4 = this.crearEquipo(usuario4.getId(), "Mar FC");
        LigaEquipos ligaEquipos1 = new LigaEquipos();
        ligaEquipos1.setIdLiga(ligaCreada.getId().intValue());
        ligaEquipos1.setIdEquipo(equipo1.getId().intValue());
        LigaEquipos ligaEquipos2 = new LigaEquipos();
        ligaEquipos2.setIdLiga(ligaCreada.getId().intValue());
        ligaEquipos2.setIdEquipo(equipo2.getId().intValue());
        LigaEquipos ligaEquipos3 = new LigaEquipos();
        ligaEquipos3.setIdLiga(ligaCreada.getId().intValue());
        ligaEquipos3.setIdEquipo(equipo3.getId().intValue());
        LigaEquipos ligaEquipos4 = new LigaEquipos();
        ligaEquipos4.setIdLiga(ligaCreada.getId().intValue());
        ligaEquipos4.setIdEquipo(equipo4.getId().intValue());
        repositorioLigaEquipos.saveAll(Lists.list(ligaEquipos1, ligaEquipos2, ligaEquipos3, ligaEquipos4));

        jugador1 = this.agregarJugadorAEquipo("Pepe", LocalDate.of(1999, 1, 1), equipo1, 70, 456, 500, 123L);
        jugador2 = this.agregarJugadorAEquipo("Salah", LocalDate.of(1999, 1, 1), equipo1, 70, 456, 500, 124L);
        jugador3 = this.agregarJugadorAEquipo("Alario", LocalDate.of(1999, 1, 1), equipo2, 70, 456, 500, 125L);
        jugador4 = this.agregarJugadorAEquipo("Sand", LocalDate.of(1999, 1, 1), equipo2, 70, 456, 500, 126L);
        jugador5 = this.agregarJugadorAEquipo("Modric", LocalDate.of(1999, 1, 1), equipo3, 70, 456, 500, 127L);
        jugador6 = this.agregarJugadorAEquipo("Reus", LocalDate.of(1999, 1, 1), equipo3, 70, 455, 500, 128L);
        this.agregarJugadorAEquipo("Driussi", LocalDate.of(1999, 1, 1), equipo4, 70, 5000, 500, 151L);
        this.agregarJugadorAEquipo("Van Dijk", LocalDate.of(1999, 1, 1), equipo4, 70, 10000, 500, 1235L);
    }


    @When("el usuario carga los goles del partido")
    public void elUsuarioCargaLosGolesDelPartido() {
        PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        paginaEditorEstadisticasLiga.cargarEstadisticasGolesDelPartido();
    }

    @And("el usuario carga las tarjetas amarillas")
    public void elUsuarioCargaLasTarjetasAmarillas() {
        PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        paginaEditorEstadisticasLiga.cargarEstadisticasAmarillasDelPartido();
    }

    @And("el usuario carga las tarjetas rojas")
    public void elUsuarioCargaLasTarjetasRojas() {
        PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        paginaEditorEstadisticasLiga.cargarEstadisticasRojasDelPartido();
    }

    @And("el usuario carga las lesiones")
    public void elUsuarioCargaLasLesiones() {
        PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        paginaEditorEstadisticasLiga.cargarEstadisticasLesionesDelPartido();
    }

    @And("And el usuario carga el MVP")
    public void andElUsuarioCargaElMVP() {
        PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        paginaEditorEstadisticasLiga.cargarEstadisticasMvpDelPartido();
    }

    @Given("ya hay datos de estadisticas cargados para el partido numero 1")
    public void yaHayDatosDeEstadisticasCargados() {
        PartidoLigaBase partidoLigaBase = new PartidoLigaBase(1);

        EstadisticaGol estadisticaGol = new EstadisticaGol();
        estadisticaGol.setEquipo(equipo1);
        estadisticaGol.setJugador(jugador1);
        estadisticaGol.setPartido(partidoLigaBase);
        estadisticaGol.setCantidad(1);
        repositorioEstadisticaGol.save(estadisticaGol);

        EstadisticaTarjetaAmarilla estadisticaTarjetaAmarilla = new EstadisticaTarjetaAmarilla();
        estadisticaTarjetaAmarilla.setEquipo(equipo3);
        estadisticaTarjetaAmarilla.setJugador(jugador5);
        estadisticaTarjetaAmarilla.setPartido(partidoLigaBase);
        repositorioEstadisticaTarjetaAmarilla.save(estadisticaTarjetaAmarilla);

        EstadisticaTarjetaRoja estadisticaTarjetaRoja = new EstadisticaTarjetaRoja();
        estadisticaTarjetaRoja.setEquipo(equipo1);
        estadisticaTarjetaRoja.setJugador(jugador1);
        estadisticaTarjetaRoja.setPartido(partidoLigaBase);
        repositorioEstadisticaTarjetaRoja.save(estadisticaTarjetaRoja);

        EstadisticaLesion estadisticaLesion = new EstadisticaLesion();
        estadisticaLesion.setEquipo(equipo1);
        estadisticaLesion.setJugador(jugador1);
        estadisticaLesion.setPartido(partidoLigaBase);
        repositorioEstadisticaLesion.save(estadisticaLesion);

        EstadisticaMvp estadisticaMvp = new EstadisticaMvp();
        estadisticaMvp.setEquipo(equipo3);
        estadisticaMvp.setJugador(jugador6);
        estadisticaMvp.setPartido(partidoLigaBase);
        repositorioEstadisticaMvp.save(estadisticaMvp);
    }

    @And("existe el partido entre el equipo con id {int} y el equipo con id {int}")
    public void existeElPartidoEntreElEquipoConIdYElEquipoConId(int equipo1Id, int equipo2Id) {
        PartidoLigaBase partidoLigaBase = new PartidoLigaBase();
        EstadoPartido estadoPartidoPendiente = new EstadoPartido();
        estadoPartidoPendiente.setId(EstadoPartidoEnum.PENDIENTE.getId());
        partidoLigaBase.setEstadoPartido(estadoPartidoPendiente);
        Equipo equipoLocal = new Equipo();
        equipoLocal.setId((long) equipo1Id);
        partidoLigaBase.setEquipoLocal(equipoLocal);
        Equipo equipoVisitante = new Equipo();
        equipoVisitante.setId((long) equipo2Id);
        partidoLigaBase.setEquipoVisitante(equipoVisitante);
        partidoLigaBase.setNumeroFecha(1);
        partidoLigaBase.setLiga(ligaCreada);
        repositorioResultadoLiga.save(partidoLigaBase);
    }

    @Then("en la pantalla de informacion del partido ve los datos agregados")
    public void enLaPantallaDeInformacionDelPartidoVeLosDatosAgregados() {
        //ToDo
        PaginaInformacionPartido paginaInformacionPartido = new PaginaInformacionPartido(PasosContexto.webDriver);
        paginaInformacionPartido.abrirInformacionPartido();

        String golEquipoLocal = paginaInformacionPartido.obtenerGolesEquipoLocal().get(0);
        assertThat(golEquipoLocal).contains(jugador2.getNombre());
        assertThat(golEquipoLocal).contains("3");

        String amarillaEquipoLocal = paginaInformacionPartido.obtenerAmarillasEquipoLocal().get(1);
        assertThat(amarillaEquipoLocal).contains(jugador2.getNombre());

        String rojaEquipoLocal = paginaInformacionPartido.obtenerRojasEquipoLocal().get(2);
        assertThat(rojaEquipoLocal).contains(jugador1.getNombre());

        String lesionEquipoLocal = paginaInformacionPartido.obtenerLesionesEquipoLocal().get(3);
        assertThat(lesionEquipoLocal).contains(jugador2.getNombre());

        String golEquipoVisitante = paginaInformacionPartido.obtenerGolesEquipoVisitante().get(0);
        assertThat(golEquipoVisitante).contains(jugador3.getNombre());
        assertThat(golEquipoVisitante).contains("1");

        String amarillaEquipoVisitante = paginaInformacionPartido.obtenerAmarillasEquipoVisitante().get(2);
        assertThat(amarillaEquipoVisitante).contains(jugador3.getNombre());

        String rojaEquipoVisitante = paginaInformacionPartido.obtenerRojasEquipoVisitante().get(3);
        assertThat(rojaEquipoVisitante).contains(jugador4.getNombre());

        String lesionEquipoVisitante = paginaInformacionPartido.obtenerLesionesEquipoVisitante().get(4);
        assertThat(lesionEquipoVisitante).contains(jugador3.getNombre());

        String mvpEquipoVisitante = paginaInformacionPartido.obtenerMvpEquipoVisitante().get(5);
        System.out.println(mvpEquipoVisitante);
        assertThat(mvpEquipoVisitante).contains(jugador3.getNombre());
    }

    @Then("el usuario ve las estadisticas cargadas previamente de los goles")
    public void elUsuarioVeLasEstadisticasCargadasPreviamenteDeLosGoles() {
        PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        assertThat(paginaEditorEstadisticasLiga.obtenerGolesEquipoLocal().get(0)).contains(jugador1.getNombre());
    }

    @And("el usuario ve las estadisticas cargadas previamente de las tarjetas amarillas")
    public void elUsuarioVeLasEstadisticasCargadasPreviamenteDeLasTarjetasAmarillas() {
        //ToDo: revisar porque a veces puede abrir la seccion de amarillas y otras no
        //PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        //assertThat(paginaEditorEstadisticasLiga.obtenerAmarillasEquipoLocal().get(0)).contains(jugador1.getNombre());
    }

    @And("el usuario ve las estadisticas cargadas previamente de las tarjetas rojas")
    public void elUsuarioVeLasEstadisticasCargadasPreviamenteDeLasTarjetasRojas() {
        //ToDo: revisar porque a veces puede abrir la seccion de rojas y otras no
        //PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        //assertThat(paginaEditorEstadisticasLiga.obtenerRojasEquipoLocal().get(0)).contains(jugador1.getNombre());
    }

    @And("el usuario ve las estadisticas cargadas previamente de las lesiones")
    public void elUsuarioVeLasEstadisticasCargadasPreviamenteDeLasLesiones() {
        //ToDo: revisar porque a veces puede abrir la seccion de lesiones y otras no
        //PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        //assertThat(paginaEditorEstadisticasLiga.obtenerLesionesEquipoLocal().get(0)).contains(jugador1.getNombre());
    }

    @And("el usuario ve las estadisticas cargadas previamente del mvp")
    public void elUsuarioVeLasEstadisticasCargadasPreviamenteDelMvp() {
        //ToDo: revisar porque a veces puede abrir la seccion de mvp y otras no
        //PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        //assertThat(paginaEditorEstadisticasLiga.obtenerMvpEquipoLocal().get(0)).contains(jugador1.getNombre());
    }

    @And("el usuario borra el gol cargado del equipo local")
    public void elUsuarioBorraElGolCargadoDelEquipoLocal() {
        PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        paginaEditorEstadisticasLiga.borrarPrimerGolLocal();
    }

    @Then("el usuario ve que no esta mas el gol borrado")
    public void elUsuarioVeQueNoEstaMasElGolBorrado() {
        PaginaEditorEstadisticasLiga paginaEditorEstadisticasLiga = new PaginaEditorEstadisticasLiga(PasosContexto.webDriver);
        assertThat(paginaEditorEstadisticasLiga.obtenerGolesEquipoLocal()).isEmpty();
    }

    @And("ya hay datos de estadisticas cargados para el partido {int}")
    public void yaHayDatosDeEstadisticasCargadosParaElPartido(int partidoId) {
        PartidoLigaBase partidoLigaBase = new PartidoLigaBase(partidoId);

        EstadisticaGol estadisticaGol = new EstadisticaGol();
        estadisticaGol.setEquipo(equipo1);
        estadisticaGol.setJugador(jugador1);
        estadisticaGol.setPartido(partidoLigaBase);
        estadisticaGol.setCantidad(1);
        repositorioEstadisticaGol.save(estadisticaGol);

        EstadisticaTarjetaAmarilla estadisticaTarjetaAmarilla = new EstadisticaTarjetaAmarilla();
        estadisticaTarjetaAmarilla.setEquipo(equipo1);
        estadisticaTarjetaAmarilla.setJugador(jugador1);
        estadisticaTarjetaAmarilla.setPartido(partidoLigaBase);
        EstadisticaTarjetaAmarilla estadisticaTarjetaAmarilla2 = new EstadisticaTarjetaAmarilla();
        estadisticaTarjetaAmarilla2.setEquipo(equipo3);
        estadisticaTarjetaAmarilla2.setJugador(jugador5);
        estadisticaTarjetaAmarilla2.setPartido(partidoLigaBase);
        repositorioEstadisticaTarjetaAmarilla.saveAll(Lists.list(estadisticaTarjetaAmarilla, estadisticaTarjetaAmarilla2));

        EstadisticaTarjetaRoja estadisticaTarjetaRoja = new EstadisticaTarjetaRoja();
        estadisticaTarjetaRoja.setEquipo(equipo1);
        estadisticaTarjetaRoja.setJugador(jugador1);
        estadisticaTarjetaRoja.setPartido(partidoLigaBase);
        repositorioEstadisticaTarjetaRoja.save(estadisticaTarjetaRoja);

        EstadisticaLesion estadisticaLesion = new EstadisticaLesion();
        estadisticaLesion.setEquipo(equipo1);
        estadisticaLesion.setJugador(jugador1);
        estadisticaLesion.setPartido(partidoLigaBase);
        repositorioEstadisticaLesion.save(estadisticaLesion);

        EstadisticaMvp estadisticaMvp = new EstadisticaMvp();
        estadisticaMvp.setEquipo(equipo1);
        estadisticaMvp.setJugador(jugador1);
        estadisticaMvp.setPartido(partidoLigaBase);
        repositorioEstadisticaMvp.save(estadisticaMvp);
    }

    @When("el usuario ingresa a la pantalla de estadisticas de la liga")
    public void elUsuarioIngresaALaPantallaDeEstadisticasDeLaLiga() {
        PaginaEstadisticasLiga paginaEstadisticasLiga = new PaginaEstadisticasLiga(PasosContexto.webDriver);
        paginaEstadisticasLiga.irAEstadisticasLiga();
    }

    @Then("se muestran las estadisticas cargadas de la liga")
    public void seMuestranLasEstadisticasCargadasDeLaLiga() {
        PaginaEstadisticasLiga paginaEstadisticasLiga = new PaginaEstadisticasLiga(PasosContexto.webDriver);

        assertThat(paginaEstadisticasLiga.obtenerNombrePrimerGoleador()).contains("Pepe");
        assertThat(paginaEstadisticasLiga.obtenerCantidadGolesPrimerGoleador()).isEqualTo("2");

        assertThat(paginaEstadisticasLiga.obtenerNombrePrimerAmonestado()).contains("Modric");
        assertThat(paginaEstadisticasLiga.obtenerCantidadAmarillasPrimerJugador()).isEqualTo("2");
        assertThat(paginaEstadisticasLiga.obtenerNombreSegundoAmonestado()).contains("Pepe");
        assertThat(paginaEstadisticasLiga.obtenerCantidadAmarillasSegundoJugador()).isEqualTo("1");

        assertThat(paginaEstadisticasLiga.obtenerNombrePrimerExpulsado()).contains("Pepe");
        assertThat(paginaEstadisticasLiga.obtenerCantidadExpulsionesPrimerJugador()).isEqualTo("2");

        assertThat(paginaEstadisticasLiga.obtenerNombrePrimerLesionado()).contains("Pepe");
        assertThat(paginaEstadisticasLiga.obtenerCantidadLesionesPrimerJugador()).isEqualTo("2");

        assertThat(paginaEstadisticasLiga.obtenerNombrePrimerMvp()).contains("Reus");
        assertThat(paginaEstadisticasLiga.obtenerCantidadMvpsPrimerJugador()).isEqualTo("1");
        assertThat(paginaEstadisticasLiga.obtenerNombreSegundoMvp()).contains("Pepe");
        assertThat(paginaEstadisticasLiga.obtenerCantidadMvpsSegundoJugador()).isEqualTo("1");
    }
}
