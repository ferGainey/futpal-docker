package stepDefinitions;

import com.tesis.futpal.modelos.balance.Balance;
import com.tesis.futpal.repositorios.RepositorioBalance;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import com.tesis.futpal.repositorios.RepositorioPeriodoTemporada;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import paginas.PaginaBalances;
import paginas.PaginaPantallaInicialUsuario;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class PasosBalance extends PasosComunes {

    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private RepositorioPeriodoTemporada repositorioPeriodo;
    @Autowired
    private RepositorioBalance repositorioBalance;

    @Given("el equipo {string} tiene cargado {int} de la temporada {int} periodo {string}")
    public void elEquipoTieneCargadoDeLaTemporadaPeriodo(String nombreEquipo, int monto, int numeroTemporada, String periodo) {
        Balance balance = new Balance();
        balance.setDetalle("Detalle del registro");
        balance.setEquipo(repositorioEquipo.findByNombreEquipo(nombreEquipo).get(0));
        balance.setMonto(monto);
        balance.setNumeroTemporada(numeroTemporada);
        balance.setPeriodo(repositorioPeriodo.findByNombre(periodo));

        repositorioBalance.save(balance);
    }

    @When("el administrador le carga {int} al equipo {string} en la temporada {int} periodo {string}")
    public void elAdministradorLeCargaAlEquipoEnLaTemporadaPeriodo(int monto, String nombreEquipo, int numeroTemporada, String periodo) {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irABalances();
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.irACargaParaUnEquipo();
        paginaBalances.completarDetalle("Detalle de prueba");
        paginaBalances.seleccionarEquipo(nombreEquipo);
        paginaBalances.completarMonto(monto);
        paginaBalances.seleccionarTemporada(numeroTemporada);
        paginaBalances.seleccionarPeriodo(periodo);

        paginaBalances.cargarBalance();
    }

    @Then("el equipo {string} tiene {int}")
    public void elEquipoTiene(String nombreEquipo, int montoEsperado) {
        List<Balance> balances = repositorioBalance.findAll();
        List<Balance> balancesEquipo = balances.stream().filter(balance -> balance.getEquipo().getNombreEquipo().equals(nombreEquipo)).collect(Collectors.toList());
        List<Integer> montosEquipo = balancesEquipo.stream().map(Balance::getMonto).collect(Collectors.toList());
        int montoDelEquipo = 0;
        for (int monto : montosEquipo) {
            montoDelEquipo += monto;
        }
        assertThat(montoDelEquipo).isEqualTo(montoEsperado);
    }

    @When("el administrador le carga por liga {int} a todos los equipos en la temporada y periodo actual de la liga {string}")
    public void elAdministradorLeCargaPorLigaATodosLosEquiposEnLaTemporadaYPeriodoActual(int monto, String liga) {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irABalances();
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.irACargaParaUnaLiga();
        paginaBalances.seleccionarLiga(liga);
        paginaBalances.cargarMontoATodosLosEquiposDeUnaLiga(monto);

        paginaBalances.cargarBalance();
    }

    @Then("se muestra mensaje que los balances fueron cargados exitosamente")
    public void seMuestraMensajeQueLosBalancesFueronCargadosExitosamente() {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarQueSeMuestraMensajeDeCargaExitosaParaUnaLiga();
    }

    @Then("se muestra mensaje que los salarios fueron cargados exitosamente")
    public void seMuestraMensajeQueLosSalariosFueronCargadosExitosamente() {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarQueSeMuestraMensajeDeCargaExitosaDeSalariosParaUnaLiga();
    }

    @Then("se muestra mensaje que el balance fue cargado exitosamente")
    public void seMuestraMensajeQueElBalanceFueCargadoExitosamente() {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarQueSeMuestraMensajeDeCargaExitosaParaUnEquipo();
    }

    @When("el usuario entra a la pantalla de vista de balances de todos los equipos")
    public void elUsuarioEntraALaPantallaDeVistaDeBalancesDeTodosLosEquipos() {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irABalances();
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.irABalancesTodosLosEquipos();
    }

    @Then("se muestran los balances de los 4 equipos")
    public void seMuestranLosBalancesDeLosEquipos() {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarQueEstaElDetalleDeLos4Equipos();
    }

    @Then("el equipo {string} tiene cargado {string} en la columna total a principio de temporada")
    public void elEquipoTieneCargadoEnLaColumnaTotalAPrincipioDeTemporada(String nombreEquipo, String montoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarMontoDelEquipoEnColumna(nombreEquipo, montoEsperado, "total-principio");
    }

    @Then("el equipo {string} tiene cargado {string} en la columna total a mitad de temporada")
    public void elEquipoTieneCargadoEnLaColumnaTotalAMitadDeTemporada(String nombreEquipo, String montoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarMontoDelEquipoEnColumna(nombreEquipo, montoEsperado, "total-mitad");
    }

    @Then("el equipo {string} tiene cargado {string} en la columna total a final de temporada")
    public void elEquipoTieneCargadoEnLaColumnaTotalAFinalDeTemporada(String nombreEquipo, String montoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarMontoDelEquipoEnColumna(nombreEquipo, montoEsperado, "total-final");
    }

    @Then("el equipo {string} tiene cargado {string} en la columna ingresos a final de temporada")
    public void elEquipoTieneCargadoEnLaColumnaIngresosAFinalDeTemporada(String nombreEquipo, String montoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarMontoDelEquipoEnColumna(nombreEquipo, montoEsperado, "ingresos-final");
    }

    @Then("el equipo {string} tiene cargado {string} en la columna ingresos a mitad de temporada")
    public void elEquipoTieneCargadoEnLaColumnaIngresosAMitadDeTemporada(String nombreEquipo, String montoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarMontoDelEquipoEnColumna(nombreEquipo, montoEsperado, "ingresos-mitad");
    }

    @Then("el equipo {string} tiene cargado {string} en la columna ingresos a principio de temporada")
    public void elEquipoTieneCargadoEnLaColumnaIngresosAPrincipioDeTemporada(String nombreEquipo, String montoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarMontoDelEquipoEnColumna(nombreEquipo, montoEsperado, "ingresos-principio");
    }

    @Then("el equipo {string} tiene cargado {string} en la columna gastos a principio de temporada")
    public void elEquipoTieneCargadoEnLaColumnaGastosAPrincipioDeTemporada(String nombreEquipo, String montoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarMontoDelEquipoEnColumna(nombreEquipo, montoEsperado, "gastos-principio");
    }

    @Then("el equipo {string} tiene cargado {string} en la columna gastos a mitad de temporada")
    public void elEquipoTieneCargadoEnLaColumnaGastosAMitadDeTemporada(String nombreEquipo, String montoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarMontoDelEquipoEnColumna(nombreEquipo, montoEsperado, "gastos-mitad");
    }

    @Then("el equipo {string} tiene cargado {string} en la columna gastos a final de temporada")
    public void elEquipoTieneCargadoEnLaColumnaGastosAFinalDeTemporada(String nombreEquipo, String montoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarMontoDelEquipoEnColumna(nombreEquipo, montoEsperado, "gastos-final");
    }

    @And("el usuario selecciona la temporada {int} en la pantalla de vista de balances de todos los equipos")
    public void elUsuarioSeleccionaLaTemporadaEnLaPantallaDeVistaDeBalancesDeTodosLosEquipos(Integer numeroTemporadaSeleccionada) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.seleccionarTemporada(numeroTemporadaSeleccionada);
    }

    @Given("el equipo {string} tiene cargado {int} de la temporada {int} periodo {string} con detalle {string}")
    public void elEquipoTieneCargadoDeLaTemporadaPeriodoConDetalle(String nombreEquipo, int monto, int numeroTemporada, String periodo, String detalle) {
        Balance balance = new Balance();
        balance.setDetalle(detalle);
        balance.setEquipo(repositorioEquipo.findByNombreEquipo(nombreEquipo).get(0));
        balance.setMonto(monto);
        balance.setNumeroTemporada(numeroTemporada);
        balance.setPeriodo(repositorioPeriodo.findByNombre(periodo));

        repositorioBalance.save(balance);
    }

    @When("el usuario entra a la pantalla de vista de balances de un equipo")
    public void elUsuarioEntraALaPantallaDeVistaDeBalancesDeUnEquipo() {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irABalances();
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.irABalancesDeUnEquipo();
    }

    @And("el usuario selecciona la temporada {int} en la pantalla de vista de balances de un equipo")
    public void elUsuarioSeleccionaLaTemporadaEnLaPantallaDeVistaDeBalancesDeUnEquipo(int numeroTemporada) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.seleccionarTemporada(numeroTemporada);
    }

    @And("el usuario selecciona el equipo {string} en la pantalla de vista de balances de un equipo")
    public void elUsuarioSeleccionaElEquipoEnLaPantallaDeVistaDeBalancesDeUnEquipo(String nombreEquipo) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.seleccionarEquipo(nombreEquipo);
    }

    @Then("cargada la transaccion con detalle {string} y monto {string} en el periodo {string}")
    public void cargadaLaTransaccionConDetalleYMontoEnElPeriodo(String detalleEsperado, String montoEsperado, String periodoEsperado) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.verificarQueEstaCargadaLaTransaccionConDetalleYMontoEnElPeriodo(detalleEsperado, montoEsperado, periodoEsperado);
    }

    @When("el administrador le carga le carga los salarios a los equipos de la liga {string}")
    public void elAdministradorLeCargaLeCargaLosSalariosALosEquiposDeLaLiga(String liga) {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irABalances();
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.irACargaDeSalarios();
        paginaBalances.seleccionarLiga(liga);
        paginaBalances.confirmarCargaSalarios();
    }

    @When("el usuario ve los salarios de los equipos de la liga {string}")
    public void elUsuarioVeLosSalariosDeLosEquiposDeLaLiga(String liga) {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irABalances();
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.irAVistaDeSalarios();
        paginaBalances.seleccionarLiga(liga);
    }

    @Then("se muestra que el equipo {string} paga {int}")
    public void seMuestraQueElEquipoPaga(String nombreEquipo, int montoQuePaga) {
        PaginaBalances paginaBalances = new PaginaBalances(PasosContexto.webDriver);
        paginaBalances.validarSalariosEquipo(nombreEquipo, montoQuePaga);
    }
}
