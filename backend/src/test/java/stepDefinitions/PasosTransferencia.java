package stepDefinitions;

import com.tesis.futpal.modelos.plantel.EstadoJugador;
import com.tesis.futpal.modelos.plantel.Jugador;
import com.tesis.futpal.modelos.transferencia.*;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import paginas.PaginaPantallaInicialUsuario;
import paginas.PaginaTransferencias;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

public class PasosTransferencia extends PasosComunes{

    private static final Integer ID_ESTADO_TRANSFERENCIA_CANCELADA = 2;
    private static final Integer ID_ESTADO_TRANSFERENCIA_PENDIENTE = 1;
    private static final Integer ID_ESTADO_TRANSFERENCIA_CONFIRMADA = 3;
    private static final Integer ID_TIPO_MOVIMIENTO_TRASPASO = 1;
    private static final Integer ID_TIPO_MOVIMIENTO_PRESTAMO = 2;
    private static final String CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA = "CARGA_TRANSFERENCIAS";
    @Autowired
    private RepositorioMovimientoJugador repositorioMovimientoJugador;

    @Autowired
    private RepositorioJugador repositorioJugador;

    @Autowired
    private RepositorioEquipo repositorioEquipo;

    @Autowired
    private RepositorioPeriodoTemporada repositorioPeriodoTemporada;

    @Autowired
    private RepositorioTransferencia repositorioTransferencia;
    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private RepositorioPrestamo repositorioPrestamo;
    @Autowired
    private RepositorioMercado repositorioMercado;

    @And("el usuario {string} entra a la pantalla de transferencias y elige al equipo {string}")
    public void elUsuarioEntraALaPantallaDeTransferenciasYEligeAlEquipo(String nombreUsuario, String nombreEquipo) {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irATransferencias();
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.irACarga();
        paginaTransferencias.seleccionarEquipo(nombreEquipo);
    }

    @And("agrega para entregar en la transferencia al jugador {string} y {int}")
    public void agregaParaEntregarEnLaTransferenciaAlJugadorY(String nombreJugador, int monto) {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.cargarJugadorPropio(nombreJugador);
        paginaTransferencias.cargarMontoPropio(monto);
    }

    @And("agrega para recibir en la transferencia al jugador {string} y {int}")
    public void agregaParaRecibirEnLaTransferenciaAlJugadorY(String nombreJugador, int monto) {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.cargarJugadorOtroEquipo(nombreJugador);
        paginaTransferencias.cargarMontoOtroEquipo(monto);
    }

    @When("realiza la transferencia")
    public void realizaLaTransferencia() {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.cargarTransferencia();
    }

    @Then("se muestra que la transferencia se realizo exitosamente")
    public void seMuestraQueLaTransferenciaSeRealizoExitosamente() {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.seVerificaQueSeMuestraMensajeTransferenciaExitosa();
    }

    @Then("se muestra que al menos unos de los jugadores fue transferido")
    public void seMuestraQueAlMenosUnosDeLosJugadoresFueTransferido() {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.seVerificaQueSeMuestraMensajeDeErrorQueAlMenosUnJUgadorFueTransferido();
    }

    @And("los jugadores {string} y {string} fueron transferidos en la temporada {int} y periodo {string} de {string} a {string}")
    public void losJugadoresYFueronTransferidosEnLaTemporadaYPeriodo(String nombreJugador1, String nombreJugador2,
                                                                     int numeroTemporada, String nombrePeriodo,
                                                                     String nombreEquipoOrigen, String nombreEquipoDestino) {
        List<Jugador> jugadores = repositorioJugador.findAll();
        Usuario usuarioTitov = repositorioUsuario.findByAliasUsuario("titov").get(0);
        Usuario usuarioFerg = repositorioUsuario.findByAliasUsuario("ferg").get(0);
        jugadores.forEach(jugador -> {
            if (jugador.getNombre().equals(nombreJugador1) || jugador.getNombre().equals(nombreJugador2)) {
                MovimientoJugador movimientoJugador = new MovimientoJugador();
                movimientoJugador.setJugador(jugador);
                movimientoJugador.setEquipoOrigen(repositorioEquipo.findByNombreEquipo(nombreEquipoOrigen).get(0));
                movimientoJugador.setEquipoDestino(repositorioEquipo.findByNombreEquipo(nombreEquipoDestino).get(0));
                movimientoJugador.setNumeroTemporada(numeroTemporada);
                movimientoJugador.setPeriodo(repositorioPeriodoTemporada.findByNombre(nombrePeriodo));
                Transferencia transferencia = new Transferencia();
                EstadoTransferencia estadoTransferencia = new EstadoTransferencia();
                estadoTransferencia.setId(ID_ESTADO_TRANSFERENCIA_PENDIENTE);
                transferencia.setEstadoTransferencia(estadoTransferencia);
                transferencia.setUsuarioOrigen(usuarioFerg);
                transferencia.setUsuarioDestino(usuarioTitov);
                TipoMovimiento tipoMovimiento = new TipoMovimiento();
                tipoMovimiento.setId(ID_TIPO_MOVIMIENTO_TRASPASO);
                movimientoJugador.setTipoMovimiento(tipoMovimiento);
                Transferencia transferenciaGuardada = repositorioTransferencia.save(transferencia);
                movimientoJugador.setTransferencia(transferenciaGuardada);
                repositorioMovimientoJugador.save(movimientoJugador);
            }
        });
    }

    @Then("se mueven los jugadores a su nuevo equipo {string}")
    public void seMuevenLosJugadoresASuNuevoEquipo(String nombreNuevoEquipo) {
        await().atMost(5, TimeUnit.SECONDS).until(this.validarQueElJugadorEsteEnElNuevoEquipo(nombreNuevoEquipo));
    }

    private Callable<Boolean> validarQueElJugadorEsteEnElNuevoEquipo(String nombreNuevoEquipo) {
        return () -> {
            List<Jugador> jugadores = repositorioJugador.findAll();
            return jugadores.get(0).getEquipo().getNombreEquipo().equals(nombreNuevoEquipo)
                    && jugadores.get(1).getEquipo().getNombreEquipo().equals(nombreNuevoEquipo);
        };
    }

    @When("ingresa a sus transferencias")
    public void ingresaASusTransferencias() {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irATransferencias();
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.irAMisTransferencias();
    }

    @Then("se ve la transferencia cargada")
    public void seVeLaTransferenciaCargada() {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.irAMisTransferencias();
    }

    @Then("se ve la transferencia cargada de {string}")
    public void seVeLaTransferenciaCargadaDe(String nombreJugador) {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.seVeEnTransferenciasLaTransferenciaDelJugador(nombreJugador);
    }

    @When("rechaza la primera transferencia")
    public void rechazaLaPrimeraTransferencia() {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.rechazarTransferencia(1);
    }

    @And("no se ve la transferencia cargada de {string}")
    public void noSeVeLaTransferenciaCargadaDe(String nombreJugador) {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.noSeVeEnTransferenciasLaTransferenciaDelJugador(nombreJugador);
    }

    @And("la primera transferencia queda rechazada")
    public void laPrimeraTransferenciaQuedaRechazada() {
        Transferencia primeraTransferencia = repositorioTransferencia.findById(1).get();
        assertThat(primeraTransferencia.getEstadoTransferencia().getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_CANCELADA);

    }

    @When("confirma la primera transferencia")
    public void confirmaLaPrimeraTransferencia() {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.confirmarTransferencia(1);
    }

    @And("la primera transferencia queda confirmada")
    public void laPrimeraTransferenciaQuedaConfirmada() {
        Transferencia primeraTransferencia = repositorioTransferencia.findById(1).get();
        assertThat(primeraTransferencia.getEstadoTransferencia().getId()).isEqualTo(ID_ESTADO_TRANSFERENCIA_CONFIRMADA);
    }

    @And("todas las transferencias estan confirmadas")
    public void todasLasTransferenciasEstanConfirmadas() {
        repositorioTransferencia.findAll()
                .forEach(transferencia -> {
                    EstadoTransferencia estadoTransferencia = new EstadoTransferencia();
                    estadoTransferencia.setId(ID_ESTADO_TRANSFERENCIA_CONFIRMADA);
                    transferencia.setEstadoTransferencia(estadoTransferencia);
                    repositorioTransferencia.save(transferencia);
                });
    }

    @When("ingresa a todas las transferencias")
    public void ingresaATodasLasTransferencias() {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irATransferencias();
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.irATodasLasTransferenciasConfirmadas();
    }

    @And("se ve el prestamo cargado de {string} desde la temporada {int} periodo {string} hasta la temporada {int} periodo {string}")
    public void seVeElPrestamoCargadoDeDesdeLaTemporadaPeriodoHastaLaTemporadaPeriodo(String nombreJugador, int numeroTemporadaComienzo,
                                                                                      String periodoComienzo, int numeroTemporadaFin,
                                                                                      String periodoFin) {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.seVeEnTransferenciasElPrestamoDelJugadorCon(nombreJugador, numeroTemporadaComienzo, periodoComienzo, numeroTemporadaFin, periodoFin);
    }

    @And("el jugador {string} fue prestado de {string} del usuario {string} a {string} del usuario {string} desde la temporada {int} periodo {string} hasta la temporada {int} periodo {string}")
    public void elJugadorFuePrestadoDeADesdeLaTemporadaPeriodoHastaLaTemporadaPeriodo(String nombreJugador, String nombreEquipoOrigen,
                                                                                      String usuarioVendedor, String nombreEquipoDestino,
                                                                                      String usuarioComprador, int numeroTemporadaComienzo,
                                                                                      String periodoComienzo, int numeroTemporadaFin,
                                                                                      String periodoFin) {
        List<Jugador> jugadores = repositorioJugador.findAll();
        Usuario usuarioVendedorActual = repositorioUsuario.findByAliasUsuario(usuarioVendedor).get(0);
        Usuario usuarioCompradorActual = repositorioUsuario.findByAliasUsuario(usuarioComprador).get(0);
        jugadores.forEach(jugador -> {
            if (jugador.getNombre().equals(nombreJugador)) {
                MovimientoJugador movimientoJugadorComienzo = new MovimientoJugador();
                movimientoJugadorComienzo.setJugador(jugador);
                movimientoJugadorComienzo.setEquipoOrigen(repositorioEquipo.findByNombreEquipo(nombreEquipoOrigen).get(0));
                movimientoJugadorComienzo.setEquipoDestino(repositorioEquipo.findByNombreEquipo(nombreEquipoDestino).get(0));
                movimientoJugadorComienzo.setNumeroTemporada(numeroTemporadaComienzo);
                movimientoJugadorComienzo.setPeriodo(repositorioPeriodoTemporada.findByNombre(periodoComienzo));
                Transferencia transferencia = new Transferencia();
                EstadoTransferencia estadoTransferencia = new EstadoTransferencia();
                estadoTransferencia.setId(ID_ESTADO_TRANSFERENCIA_PENDIENTE);
                transferencia.setEstadoTransferencia(estadoTransferencia);
                transferencia.setUsuarioOrigen(usuarioVendedorActual);
                transferencia.setUsuarioDestino(usuarioCompradorActual);
                TipoMovimiento tipoMovimiento = new TipoMovimiento();
                tipoMovimiento.setId(ID_TIPO_MOVIMIENTO_PRESTAMO);
                movimientoJugadorComienzo.setTipoMovimiento(tipoMovimiento);
                Transferencia transferenciaGuardada = repositorioTransferencia.save(transferencia);
                movimientoJugadorComienzo.setTransferencia(transferenciaGuardada);
                MovimientoJugador movimientoComienzoGuardado = repositorioMovimientoJugador.save(movimientoJugadorComienzo);

                MovimientoJugador movimientoJugadorFinal = new MovimientoJugador();
                movimientoJugadorFinal.setJugador(jugador);
                movimientoJugadorFinal.setEquipoOrigen(repositorioEquipo.findByNombreEquipo(nombreEquipoDestino).get(0));
                movimientoJugadorFinal.setEquipoDestino(repositorioEquipo.findByNombreEquipo(nombreEquipoOrigen).get(0));
                movimientoJugadorFinal.setNumeroTemporada(numeroTemporadaFin);
                movimientoJugadorFinal.setPeriodo(repositorioPeriodoTemporada.findByNombre(periodoFin));
                movimientoJugadorFinal.setTipoMovimiento(tipoMovimiento);
                movimientoJugadorFinal.setTransferencia(transferenciaGuardada);
                MovimientoJugador movimientoFinalGuardado = repositorioMovimientoJugador.save(movimientoJugadorFinal);

                PrestamoJugador prestamoJugadorComienzo = new PrestamoJugador();
                prestamoJugadorComienzo.setEsFinDePrestamo(false);
                prestamoJugadorComienzo.setMovimientoJugador(movimientoComienzoGuardado);
                repositorioPrestamo.save(prestamoJugadorComienzo);

                PrestamoJugador prestamoJugadorFinal = new PrestamoJugador();
                prestamoJugadorFinal.setEsFinDePrestamo(true);
                prestamoJugadorFinal.setMovimientoJugador(movimientoFinalGuardado);
                repositorioPrestamo.save(prestamoJugadorFinal);
            }
        });
    }

    @And("agrega para prestar al jugador propio {string}")
    public void agregaParaPrestarAlJugador(String nombreJugador) {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.cargarPrestamoJugadorPropio(nombreJugador);
    }

    @And("se ejecuto el movimiento que mueve prestamo al jugador {string} al {string}")
    public void seEjecutoElMovimientoQueMuevePrestamoALosJugadoresYAl(String nombreJugador, String nombreEquipoDestino) {
        List<Jugador> jugadores = repositorioJugador.findAll();
        jugadores.forEach(jugador -> {
            if (jugador.getNombre().equals(nombreJugador)) {
                EstadoJugador prestado = new EstadoJugador();
                prestado.setId(2L);
                jugador.setEstadoJugador(prestado);
                jugador.setEquipo(repositorioEquipo.findByNombreEquipo(nombreEquipoDestino).get(0));
                repositorioJugador.save(jugador);
            }
        });
    }

    @Given("el mercado esta abierto")
    public void elMercadoEstaAbierto() {
        Mercado mercado = repositorioMercado.findById(CARACTERISTICA_MERCADO_CARGA_TRANSFERENCIA).get();
        mercado.setActivo(true);
        repositorioMercado.save(mercado);
    }

    @When("el usuario administrador deshabilita el mercado")
    public void elUsuarioAdministradorDeshabilitaElMercado() {
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(PasosContexto.webDriver);
        paginaPantallaInicialUsuario.irATransferencias();
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.deshabilitarMercado();
    }

    @Then("no se ve la opcion de carga de transferencias")
    public void noSeVeLaOpcionDeCargaDeTransferencias() {
        PaginaTransferencias paginaTransferencias = new PaginaTransferencias(PasosContexto.webDriver);
        paginaTransferencias.noseVeLaCargaDeTransferencias();
    }
}
