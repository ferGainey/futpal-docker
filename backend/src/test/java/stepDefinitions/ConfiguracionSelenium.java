package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class ConfiguracionSelenium {

    private static final String URL_BASE = "http://frontend:3080/";
    private static WebDriver driver;
    private static String urlBase;

    public static WebDriver getDriver() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setHeadless(true);
        driver = new FirefoxDriver(firefoxOptions);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        urlBase = URL_BASE;
        driver.manage().window().maximize();//esto estaba en los pasos
        return driver;
    }

    static {
        System.setProperty("webdriver.gecko.driver", findFile("webdrivers/geckodriver-linux"));
    }

    static private String findFile(String filename) {
        String paths[] = {"", "bin/", "target/classes"};
        for (String path : paths) {
            if (new File(path + filename).exists())
                return path + filename;
        }
        return "";
    }

    public static String getUrlBase() {
        return urlBase;
    }

}