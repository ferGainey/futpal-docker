package stepDefinitions;

import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.excepciones.CamposNulosException;
import com.tesis.futpal.excepciones.CamposVaciosException;
import com.tesis.futpal.excepciones.UsuarioExistenteException;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import paginas.PaginaIngresoUsuario;
import paginas.PaginaPantallaInicialUsuario;
import paginas.PaginaPantallaPrincipal;

import static org.assertj.core.api.Assertions.assertThat;

public class PasosIngresoUsuario {

    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private PasswordEncoder encoder;

    @Given("el usuario con alias {string} esta registrado.")
    public void elUsuarioConAliasEstaRegistrado(String alias) throws UsuarioExistenteException, CamposVaciosException, CamposNulosException {
        Usuario usuario = new Usuario();
        usuario.setAliasUsuario(alias);
        usuario.setNombreUsuario("Pedro");
        usuario.setApellidoUsuario("TestingLogin");
        usuario.setMailUsuario("pedro@testing.com");
        usuario.setContraseniaUsuario(encoder.encode("12345678"));
        repositorioUsuario.save(usuario);
    }

    @When("el usuario con alias {string} se loguea")
    public void elUsuarioConAliasSeLoguea(String alias) {
        WebDriver driver = PasosContexto.webDriver;
        driver.get(ConfiguracionSelenium.getUrlBase());
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        paginaPantallaPrincipal.irAPantallaLogin();
        PaginaIngresoUsuario paginaIngresoUsuario = new PaginaIngresoUsuario(driver);
        paginaIngresoUsuario.loguearUsuario(alias, "12345678");
    }


    @Then("se redirige a la pantalla de inicio del usuario {string}")
    public void seRedirigeALaPantallaDeInicioDelUsuario(String alias) {
        WebDriver driver = PasosContexto.webDriver;
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(driver);
        assertThat(paginaPantallaInicialUsuario.buscarTextoBienvenidaUsuario()).isEqualTo("¡Hola " + alias + "!");
    }

    @And("el usuario con alias {string} esta logueado")
    public void elUsuarioConAliasEstaLogueado(String alias) {
        WebDriver driver = PasosContexto.webDriver;
        driver.get(ConfiguracionSelenium.getUrlBase());
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        paginaPantallaPrincipal.irAPantallaLogin();
        PaginaIngresoUsuario paginaIngresoUsuario = new PaginaIngresoUsuario(driver);
        paginaIngresoUsuario.loguearUsuario(alias, "12345678");
    }

    @When("el usuario se desloguea")
    public void elUsuarioSeDesloguea() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(driver);
        paginaPantallaInicialUsuario.desloguearse();
    }

    @Then("se redirige a la pantalla principal de la aplicacion")
    public void seRedirigeALaPantallaPrincipalDeLaAplicacion() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        assertThat(paginaPantallaPrincipal.buscarTituloBienvenidaPantallaPrincipal()).isEqualTo("¡Bienvenido a Futpal!");
    }

    @Given("el usuario con alias {string} no esta registrado")
    public void elUsuarioConAliasNoEstaRegistrado(String alias) {
        //Como no está logueado simplemente no lo agrego a la base
        //Este paso queda por cuestiones legibilidad para entender el escenario
    }

    @Then("se le muestra alerta de que las credenciales no son validas")
    public void seLeMuestraAlertaDeQueLasCredencialesNoSonValidas() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaIngresoUsuario paginaIngresoUsuario = new PaginaIngresoUsuario(driver);
        assertThat(paginaIngresoUsuario.buscarMensajeAlertaCredencialesInvalidas()).isEqualTo("Las credenciales ingresadas no son válidas.");
    }
}
