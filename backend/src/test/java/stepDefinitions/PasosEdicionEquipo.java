package stepDefinitions;

import com.tesis.futpal.modelos.equipo.Equipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipo;
import com.tesis.futpal.modelos.equipo.EstadoEquipoEnum;
import com.tesis.futpal.modelos.usuario.Rol;
import com.tesis.futpal.modelos.usuario.TipoRol;
import com.tesis.futpal.modelos.usuario.Usuario;
import com.tesis.futpal.repositorios.RepositorioEquipo;
import com.tesis.futpal.repositorios.RepositorioUsuario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import paginas.PaginaEdicionEquipo;
import paginas.PaginaIngresoUsuario;
import paginas.PaginaPantallaInicialUsuario;
import paginas.PaginaPantallaPrincipal;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PasosEdicionEquipo {

    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private PasswordEncoder encoder;

    private Usuario usuario;

    @And("el usuario {string} no tiene un equipo asociado")
    public void elUsuarioNoTieneUnEquipoAsociado(String alias) {
        //no hago nada. Esta por cuestiones de legibilidad
    }

    @Given("el usuario {string} esta en la pantalla de edicion de equipo")
    public void elUsuarioEstaEnLaPantallaDeEdicionDeEquipo(String alias) {
        WebDriver driver = PasosContexto.webDriver;
        driver.get(ConfiguracionSelenium.getUrlBase());
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        paginaPantallaPrincipal.irAPantallaLogin();
        PaginaIngresoUsuario paginaIngresoUsuario = new PaginaIngresoUsuario(driver);
        paginaIngresoUsuario.loguearUsuario(alias, "12345678");
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(driver);
        paginaPantallaInicialUsuario.irAPaginaEditarEquipo();
    }

    @When("el usuario {string} entra a la pantalla de edicion de equipo")
    public void elUsuarioEntraALaPantallaDeEdicionDeEquipo(String aliasUsuario) {
        WebDriver driver = PasosContexto.webDriver;
        driver.get(ConfiguracionSelenium.getUrlBase());
        PaginaPantallaPrincipal paginaPantallaPrincipal = new PaginaPantallaPrincipal(driver);
        paginaPantallaPrincipal.irAPantallaLogin();
        PaginaIngresoUsuario paginaIngresoUsuario = new PaginaIngresoUsuario(driver);
        paginaIngresoUsuario.loguearUsuario(aliasUsuario, "12345678");
        PaginaPantallaInicialUsuario paginaPantallaInicialUsuario = new PaginaPantallaInicialUsuario(driver);
        paginaPantallaInicialUsuario.irAPaginaEditarEquipo();
    }


    @Given("existe un usuario {string}")
    public void existeUnUsuario(String aliasUsuario) {
        Usuario usuarioACrear = new Usuario();
        usuarioACrear.setAliasUsuario(aliasUsuario);
        usuarioACrear.setNombreUsuario("Fernando");
        usuarioACrear.setApellidoUsuario("Gainey");
        usuarioACrear.setMailUsuario("fer@gainey.com");
        usuarioACrear.setContraseniaUsuario(encoder.encode("12345678"));

        Rol rolUsuario = new Rol();
        rolUsuario.setId(TipoRol.ROL_USUARIO.getId());
        List<Rol> rolesUsuario = new ArrayList<>();
        rolesUsuario.add(rolUsuario);
        usuarioACrear.setRoles(rolesUsuario);

        usuario =repositorioUsuario.save(usuarioACrear);
    }

    @Then("muestra un mensaje avisando de que no tiene equipo asociado")
    public void muestraUnMensajeAvisandoDeQueNoTieneEquipoAsociado() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaEdicionEquipo paginaEdicionEquipo = new PaginaEdicionEquipo(driver);
        assertThat(paginaEdicionEquipo.buscarTextoEquipo()).isEqualTo("No posee un equipo asociado aún. Ingrese el nombre que desea para su equipo en \"Editar Equipo\" y confirme para asociarlo.");
    }

    @And("carga el nombre {string} a su equipo")
    public void cargaElNombreASuEquipo(String nombreEquipo) {
        WebDriver driver = PasosContexto.webDriver;
        PaginaEdicionEquipo paginaEdicionEquipo = new PaginaEdicionEquipo(driver);
        paginaEdicionEquipo.ingresarNombreEquipo(nombreEquipo);
    }

    @When("selecciona actualizar")
    public void seleccionaActualizar() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaEdicionEquipo paginaEdicionEquipo = new PaginaEdicionEquipo(driver);
        paginaEdicionEquipo.seleccionarActualizar();
    }

    @Then("muestra un mensaje avisando de que se actualizo el equipo correctamente")
    public void muestraUnMensajeAvisandoDeQueSeActualizoElEquipoCorrectamente() {
        WebDriver driver = PasosContexto.webDriver;
        PaginaEdicionEquipo paginaEdicionEquipo = new PaginaEdicionEquipo(driver);
        assertThat(paginaEdicionEquipo.buscarTextoEquipoEditadoExitosamente()).isEqualTo("Equipo actualizado correctamente.");
    }

    @And("muestra en la pantalla el nombre del equipo {string}")
    public void muestraEnLaPantallaElNombreDelEquipo(String nuevoNombreEquipo) {
        WebDriver driver = PasosContexto.webDriver;
        PaginaEdicionEquipo paginaEdicionEquipo = new PaginaEdicionEquipo(driver);
        assertThat(paginaEdicionEquipo.buscarTextoEquipo()).isEqualTo(nuevoNombreEquipo);
    }

    @Given("el usuario {string} tiene un equipo asociado {string}")
    public void elUsuarioTieneUnEquipoAsociado(String alias, String nombreEquipo) {
        Equipo equipo = new Equipo();
        equipo.setNombreEquipo(nombreEquipo);
        equipo.setIdUsuario(usuario.getId());
        EstadoEquipo estadoEquipo = new EstadoEquipo();
        estadoEquipo.setId(EstadoEquipoEnum.APROBADO.getId());
        equipo.setEstadoEquipo(estadoEquipo);
        repositorioEquipo.save(equipo);
    }
}
