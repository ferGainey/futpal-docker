package stepDefinitions;

import com.tesis.futpal.repositorios.*;
import com.tesis.futpal.repositorios.estadisticas.*;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

@DataJpaTest
public class PasosContexto {

    @Autowired
    private RepositorioUsuario repositorioUsuario;
    @Autowired
    private RepositorioEquipo repositorioEquipo;
    @Autowired
    private RepositorioLigas repositorioLiga;
    @Autowired
    private RepositorioLigaEquipos repositorioLigaEquipos;
    @Autowired
    private RepositorioResultadoLiga repositorioResultadoLiga;
    @Autowired
    private RepositorioJugador repositorioJugador;
    @Autowired
    private RepositorioEstadisticaGol repositorioEstadisticaGol;
    @Autowired
    private RepositorioEstadisticaTarjetaAmarilla repositorioEstadisticaTarjetaAmarilla;
    @Autowired
    private RepositorioEstadisticaTarjetaRoja repositorioEstadisticaTarjetaRoja;
    @Autowired
    private RepositorioEstadisticaLesion repositorioEstadisticaLesion;
    @Autowired
    private RepositorioEstadisticaMvp repositorioEstadisticaMvp;
    @Autowired
    private RepositorioTemporada repositorioTemporada;
    @Autowired
    private RepositorioBalance repositorioBalance;
    @Autowired
    private RepositorioMovimientoJugador repositorioMovimientoJugador;
    @Autowired
    private RepositorioTransferencia repositorioTransferencia;
    @Autowired
    private RepositorioPrestamo repositorioPrestamo;

    @PersistenceContext
    private EntityManager entityManager;
    public static WebDriver webDriver;

    @Before
    public void setUp() {
        webDriver = ConfiguracionSelenium.getDriver();
        limpioBaseDeDatos();
    }

    @After
    public void tearDown() {
        webDriver.quit();
    }

    private void limpioBaseDeDatos() {
        this.repositorioPrestamo.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE prestamo_jugador_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioMovimientoJugador.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE movimiento_jugador_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioBalance.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE balance_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioTransferencia.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE transferencia_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioEstadisticaGol.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE estadistica_gol_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioEstadisticaTarjetaAmarilla.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE estadistica_tarjeta_amarilla_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioEstadisticaTarjetaRoja.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE estadistica_tarjeta_roja_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioEstadisticaLesion.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE estadistica_lesion_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioEstadisticaMvp.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE estadistica_mvp_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioResultadoLiga.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE partido_liga_base_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioLigaEquipos.deleteAll();

        this.repositorioLiga.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE liga_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioTemporada.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE temporada_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioJugador.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE jugador_id_seq RESTART WITH 1").executeUpdate();

        this.repositorioEquipo.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE equipo_id_equipo_seq RESTART WITH 1").executeUpdate();

        this.repositorioUsuario.deleteAll();
        entityManager.createNativeQuery("ALTER SEQUENCE usuario_id_seq RESTART WITH 1").executeUpdate();
    }
}
