package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class PaginaBase {

    protected WebDriver driver;
    protected WebDriverWait webDriverWait;

    public PaginaBase(WebDriver driver) {
        this.driver = driver;
        this.webDriverWait = new WebDriverWait(this.driver, 10L);
    }

    protected WebElement encontrarElemento(By selector) {
        return webDriverWait.until(ExpectedConditions.elementToBeClickable(selector));
    }

    protected List<WebElement> encontrarElementos(By selector) {
        return webDriverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(selector));
    }
}
