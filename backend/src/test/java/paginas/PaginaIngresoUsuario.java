package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaginaIngresoUsuario extends PaginaBase {

    private static final By SELECTOR_CAMPO_ALIAS = By.id("campo-alias-login");
    private static final By SELECTOR_CAMPO_CONTRASENIA = By.id("campo-contrasenia-login");
    private static final By SELECTOR_BOTON_LOGUEAR_USUARIO = By.id("boton-login");
    private static final By SELECTOR_MENSAJE_ALERTA_CREDENCIALES_INVALIDAS = By.id("texto-error-login-usuario");

    public PaginaIngresoUsuario(WebDriver driver) {
        super(driver);
    }

    public void loguearUsuario(String alias, String contrasenia) {
        this.encontrarElemento(SELECTOR_CAMPO_ALIAS).sendKeys(alias);
        this.encontrarElemento(SELECTOR_CAMPO_CONTRASENIA).sendKeys(contrasenia);
        this.encontrarElemento(SELECTOR_BOTON_LOGUEAR_USUARIO).click();
    }

    public String buscarMensajeAlertaCredencialesInvalidas() {
        return this.encontrarElemento(SELECTOR_MENSAJE_ALERTA_CREDENCIALES_INVALIDAS).getText();
    }
}
