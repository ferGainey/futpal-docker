package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

public class PaginaBalances extends PaginaBase {

    private static final By SELECTOR_COMBO_EQUIPOS_APROBADOS = By.id("balances-equipos-aprobados");
    private static final By SELECTOR_DETALLE = By.id("balances-detalle");
    private static final By SELECTOR_MONTO = By.id("balances-monto");
    private static final By SELECTOR_TEMPORADA = By.id("balances-temporadas");
    private static final By SELECTOR_PERIODOS = By.id("balances-periodos");
    private static final By SELECTOR_CARGAR_BALANCE = By.id("boton-cargar-balance");
    private static final By SELECTOR_CARGA_BALANCE_UN_EQUIPO = By.id("carga-balance-un-equipo-side-nav");
    private static final By SELECTOR_CARGA_BALANCE_LIGA = By.id("carga-balance-liga-side-nav");
    private static final By SELECTOR_MONTO_CARGA_LIGA = By.className("carga-balances-liga-monto");
    private static final By SELECTOR_MENSAJE_CARGA_EXITOSA = By.id("texto-mensaje-carga-exitosa-balance");;
    private static final By SELECTOR_LIGAS = By.id("carga-balance-ligas-disponibles");
    private static final By SELECTOR_VER_BALANCES_TODOS_LOS_EQUIPOS = By.id("ver-balances-todos-side-nav");
    private static final By SELECTOR_EQUIPO_1 = By.id("equipo0");
    private static final By SELECTOR_EQUIPO_2 = By.id("equipo1");
    private static final By SELECTOR_EQUIPO_3 = By.id("equipo2");
    private static final By SELECTOR_EQUIPO_4 = By.id("equipo3");
    private static final By SELECTOR_VER_BALANCES_DE_UN_EQUIPO = By.id("ver-balances-uno-side-nav");
    private static final By SELECTOR_DETALLE_TRANSACCION_0 = By.id("detalle0");
    private static final By SELECTOR_MONTO_TRANSACCION_0 = By.id("monto0");
    private static final By SELECTOR_PERIODO_TRANSACCION_0 = By.id("periodo0");
    private static final By SELECTOR_IR_A_CARGA_SALARIOS = By.id("carga-salarios-side-nav");
    private static final By SELECTOR_CONFIRMAR_CARGA_SALARIOS = By.id("boton-cargar-balance");
    private static final By SELECTOR_IR_A_VISTA_SALARIOS = By.id("ver-salarios-side-nav");

    public PaginaBalances(WebDriver webDriver) {
        super(webDriver);
    }

    public void seleccionarEquipo(String nombreEquipo) {
        Select select = new Select(this.encontrarElemento(SELECTOR_COMBO_EQUIPOS_APROBADOS));
        select.selectByVisibleText(nombreEquipo);
    }

    public void completarDetalle(String detalleDePrueba) {
        this.encontrarElemento(SELECTOR_DETALLE).sendKeys(detalleDePrueba);
    }

    public void completarMonto(int monto) {
        this.encontrarElemento(SELECTOR_MONTO).sendKeys(String.valueOf(monto));
    }

    public void seleccionarTemporada(int numeroTemporada) {
        String temporada = "Temp. " + numeroTemporada;
        Select select = new Select(this.encontrarElemento(SELECTOR_TEMPORADA));
        select.selectByVisibleText(temporada);
    }

    public void seleccionarPeriodo(String periodo) {
        Select select = new Select(this.encontrarElemento(SELECTOR_PERIODOS));
        select.selectByVisibleText(periodo);
    }

    public void cargarBalance() {
        this.encontrarElemento(SELECTOR_CARGAR_BALANCE).click();
    }

    public void irACargaParaUnEquipo() {
        this.encontrarElemento(SELECTOR_CARGA_BALANCE_UN_EQUIPO).click();
    }

    public void irACargaParaUnaLiga() {
        this.encontrarElemento(SELECTOR_CARGA_BALANCE_LIGA).click();
    }

    public void cargarMontoATodosLosEquiposDeUnaLiga(int monto) {
        this.encontrarElementos(SELECTOR_MONTO_CARGA_LIGA).forEach(cargaMonto -> cargaMonto.sendKeys(String.valueOf(monto)));
    }

    public void verificarQueSeMuestraMensajeDeCargaExitosaParaUnaLiga() {
        assertThat(this.encontrarElemento(SELECTOR_MENSAJE_CARGA_EXITOSA).getText()).isEqualTo("Los balances han sido cargados exitosamente.");
    }

    public void verificarQueSeMuestraMensajeDeCargaExitosaDeSalariosParaUnaLiga() {
        assertThat(this.encontrarElemento(SELECTOR_MENSAJE_CARGA_EXITOSA).getText()).isEqualTo("Los salarios han sido cargados exitosamente.");
    }

    public void seleccionarLiga(String liga) {
        Select select = new Select(this.encontrarElemento(SELECTOR_LIGAS));
        select.selectByVisibleText(liga);
    }

    public void verificarQueSeMuestraMensajeDeCargaExitosaParaUnEquipo() {
        assertThat(this.encontrarElemento(SELECTOR_MENSAJE_CARGA_EXITOSA).getText()).isEqualTo("El balance ha sido cargado exitosamente.");
    }

    public void irABalancesTodosLosEquipos() {
        this.encontrarElemento(SELECTOR_VER_BALANCES_TODOS_LOS_EQUIPOS).click();
    }

    public void verificarQueEstaElDetalleDeLos4Equipos() {
        await().atMost(5, TimeUnit.SECONDS).until(this.validarLos4Equipos("Gas FC", "Mar FC", "Marc FC", "Nico FC"));
    }

    public void verificarMontoDelEquipoEnColumna(String nombreEquipo, String montoEsperado, String columna) {
        String selectorColumna = nombreEquipo + columna;
        await().atMost(5, TimeUnit.SECONDS).until(this.validarMontoDelEquipoEnColumna(selectorColumna, montoEsperado));
    }

    private Callable<Boolean> validarMontoDelEquipoEnColumna(String selectorColumna, String montoEsperado) {
        return () -> this.encontrarElemento(By.id(selectorColumna)).getText().equals(montoEsperado);
    }

    private Callable<Boolean> validarLos4Equipos(String equipo1, String equipo2, String equipo3, String equipo4) {
        return () -> this.encontrarElemento(SELECTOR_EQUIPO_1).getText().equals(equipo1) &&
                this.encontrarElemento(SELECTOR_EQUIPO_2).getText().equals(equipo2) &&
                this.encontrarElemento(SELECTOR_EQUIPO_3).getText().equals(equipo3) &&
                this.encontrarElemento(SELECTOR_EQUIPO_4).getText().equals(equipo4);
    }

    public void irABalancesDeUnEquipo() {
        this.encontrarElemento(SELECTOR_VER_BALANCES_DE_UN_EQUIPO).click();
    }

    public void verificarQueEstaCargadaLaTransaccionConDetalleYMontoEnElPeriodo(String detalleEsperado, String montoEsperado, String periodoEsperado) {
        await().atMost(5, TimeUnit.SECONDS).until(this.validarLaTransaccionCon(detalleEsperado, montoEsperado, periodoEsperado));
    }

    private Callable<Boolean> validarLaTransaccionCon(String detalleEsperado, String montoEsperado, String periodoEsperado) {
        return () -> this.encontrarElemento(SELECTOR_DETALLE_TRANSACCION_0).getText().equals(detalleEsperado) &&
                this.encontrarElemento(SELECTOR_MONTO_TRANSACCION_0).getText().equals(montoEsperado) &&
                this.encontrarElemento(SELECTOR_PERIODO_TRANSACCION_0).getText().equals(periodoEsperado);
    }

    public void irACargaDeSalarios() {
        this.encontrarElemento(SELECTOR_IR_A_CARGA_SALARIOS).click();
    }

    public void confirmarCargaSalarios() {
        this.encontrarElemento(SELECTOR_CONFIRMAR_CARGA_SALARIOS).click();
    }

    public void irAVistaDeSalarios() {
        this.encontrarElemento(SELECTOR_IR_A_VISTA_SALARIOS).click();
    }

    public void validarSalariosEquipo(String nombreEquipo, int montoQuePaga) {
        String selectorColumnaNombre = "nombre" + nombreEquipo;
        assertThat(this.encontrarElemento(By.id(selectorColumnaNombre)).getText().equals(nombreEquipo));

        String selectorColumnaMonto = "monto" + nombreEquipo;
        assertThat(this.encontrarElemento(By.id(selectorColumnaMonto)).getText().equals(String.valueOf(montoQuePaga)));
    }
}
