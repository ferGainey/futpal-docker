package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaginaEditorResultadoLiga extends PaginaBase {

    private static final By SELECTOR_GOLES_EQUIPO_LOCAL = By.id("editor-resultado-goles-local");
    private static final By SELECTOR_GOLES_EQUIPO_VISITANTE = By.id("editor-resultado-goles-visitante");
    private static final By SELECTOR_BOTON_ACTUALIZAR_RESULTADO = By.id("editor-resultado-boton-actualizar");
    private static final By SELECTOR_TEXTO_ERROR_CAMPOS_NULOS = By.id("editor-resultado-texto-error-actualizar-partido");
    private static final By SELECTOR_ULTIMO_USUARIO_EDITOR = By.id("editor-resultado-ultimo-usuario-editor");

    public PaginaEditorResultadoLiga(WebDriver driver) {
        super(driver);
    }

    public void cargarGolesLocal(int goles) {
        this.encontrarElemento(SELECTOR_GOLES_EQUIPO_LOCAL).sendKeys(String.valueOf(goles));
    }

    public void cargarGolesVisitante(int goles) {
        this.encontrarElemento(SELECTOR_GOLES_EQUIPO_VISITANTE).sendKeys(String.valueOf(goles));
    }

    public void clickEnActualizarResultado() {
        this.encontrarElemento(SELECTOR_BOTON_ACTUALIZAR_RESULTADO).click();
    }

    public String obtenerTextoErrorCamposNulos() {
        return this.encontrarElemento(SELECTOR_TEXTO_ERROR_CAMPOS_NULOS).getText();
    }

    public String obtenerGolesEquipoLocal() {
        return this.encontrarElemento(SELECTOR_GOLES_EQUIPO_LOCAL).getAttribute("value");
    }

    public String obtenerGolesEquipoVisitante() {
        return this.encontrarElemento(SELECTOR_GOLES_EQUIPO_VISITANTE).getAttribute("value");
    }

    public String obtenerUltimoUsuarioEditor() {
        return this.encontrarElemento(SELECTOR_ULTIMO_USUARIO_EDITOR).getText();
    }
}
