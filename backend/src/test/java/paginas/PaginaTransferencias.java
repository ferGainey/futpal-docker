package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import static org.assertj.core.api.Assertions.assertThat;

public class PaginaTransferencias extends PaginaBase {

    private static final By SELECTOR_COMBO_EQUIPOS_APROBADOS = By.id("transferencias-equipos-aprobados");
    private static final By SELECTOR_MONTO = By.id("transferencias-monto");
    private static final By SELECTOR_TEMPORADA = By.id("transferencias-temporadas");
    private static final By SELECTOR_PERIODOS = By.id("transferencias-periodos");
    private static final By SELECTOR_CARGAR_MONTO = By.id("boton-cargar-monto");
    private static final By SELECTOR_AGREGADOR_JUGADOR_PROPIO = By.id("agregador-jugador-propio");
    private static final By SELECTOR_AGREGADOR_PRESTAMO_JUGADOR_PROPIO = By.id("agregador-jugador-prestamo-propio");
    private static final By SELECTOR_JUGADORES = By.id("jugadores");
    private static final By SELECTOR_CARGAR_JUGADOR = By.id("boton-cargar-jugador");
    private static final By SELECTOR_AGREGADOR_MONTO_PROPIO = By.id("agregador-monto-propio");
    private static final By SELECTOR_CARGAR_TRANSFERENCIA = By.id("boton-cargar-transferencia");
    private static final By SELECTOR_AGREGADOR_JUGADOR_OTRO_EQUIPO = By.id("agregador-jugador-otro-equipo");
    private static final By SELECTOR_AGREGADOR_MONTO_OTRO_EQUIPO = By.id("agregador-monto-otro-equipo");
    private static final By SELECTOR_MENSAJE_CARGA_EXITOSA = By.id("texto-mensaje-carga-exitosa-transferencia");
    private static final By SELECTOR_MENSAJE_CARGA_ERRONEA = By.id("texto-mensaje-carga-erronea-transferencia");
    private static final By SELECTOR_IR_A_CARGA = By.id("carga-transferencia-side-nav");
    private static final By SELECTOR_IR_A_MIS_TRANSFERENCIAS = By.id("mis-transferencias-side-nav");
    private static final String PREFIJO_SELECTOR_JUGADOR = "transferenciaJugador";
    private static final String PREFIJO_RECHAZAR_TRANSFERENCIA = "boton-rechazar";
    private static final String PREFIJO_CONFIRMAR_TRANSFERENCIA = "boton-confirmar";
    private static final By SELECTOR_IR_A_TODAS_LAS_TRANSFERENCIAS = By.id("todas-las-transferencias-confirmadas-side-nav");

    private static final By SELECTOR_TEMPORADA_COMIENZO_PRESTAMO = By.id("transferencias-temporadas-comienzo");
    private static final By SELECTOR_PERIODOS_COMIENZO_PRESTAMO = By.id("transferencias-periodos-comienzo");
    private static final By SELECTOR_TEMPORADA_FINAL_PRESTAMO = By.id("transferencias-temporadas-final");
    private static final By SELECTOR_PERIODOS_FINAL_PRESTAMO = By.id("transferencias-periodos-final");
    private static final By SELECTOR_DESHABILITAR_MERCADO = By.id("deshabilitacion-mercado-side-nav");

    public PaginaTransferencias(WebDriver webDriver) {
        super(webDriver);
    }

    public void seleccionarEquipo(String nombreEquipo) {
        Select select = new Select(this.encontrarElemento(SELECTOR_COMBO_EQUIPOS_APROBADOS));
        select.selectByVisibleText(nombreEquipo);
    }


    public void cargarTransferencia() {
        this.encontrarElemento(SELECTOR_CARGAR_TRANSFERENCIA).click();
    }

    public void cargarJugadorPropio(String nombreJugador) {
        this.encontrarElemento(SELECTOR_AGREGADOR_JUGADOR_PROPIO).click();
        Select select = new Select(this.encontrarElemento(SELECTOR_JUGADORES));
        select.selectByVisibleText(nombreJugador);
        this.seleccionarTemporada(2);
        this.seleccionarPeriodo("Mitad");
        this.encontrarElemento(SELECTOR_CARGAR_JUGADOR).click();
    }

    public void cargarJugadorOtroEquipo(String nombreJugador) {
        this.encontrarElemento(SELECTOR_AGREGADOR_JUGADOR_OTRO_EQUIPO).click();
        Select select = new Select(this.encontrarElemento(SELECTOR_JUGADORES));
        select.selectByVisibleText(nombreJugador);
        this.seleccionarTemporada(2);
        this.seleccionarPeriodo("Mitad");
        this.encontrarElemento(SELECTOR_CARGAR_JUGADOR).click();
    }

    public void cargarMontoPropio(int monto) {
        this.encontrarElemento(SELECTOR_AGREGADOR_MONTO_PROPIO).click();
        this.encontrarElemento(SELECTOR_MONTO).sendKeys(String.valueOf(monto));
        this.seleccionarTemporada(2);
        this.seleccionarPeriodo("Mitad");
        this.encontrarElemento(SELECTOR_CARGAR_MONTO).click();
    }

    public void cargarMontoOtroEquipo(int monto) {
        this.encontrarElemento(SELECTOR_AGREGADOR_MONTO_OTRO_EQUIPO).click();
        this.encontrarElemento(SELECTOR_MONTO).sendKeys(String.valueOf(monto));
        this.seleccionarTemporada(2);
        this.seleccionarPeriodo("Mitad");
        this.encontrarElemento(SELECTOR_CARGAR_MONTO).click();
    }

    private void seleccionarTemporada(int numeroTemporada) {
        String temporada = "Temp. " + numeroTemporada;
        Select select = new Select(this.encontrarElemento(SELECTOR_TEMPORADA));
        select.selectByVisibleText(temporada);
    }

    private void seleccionarTemporadaComienzoPrestamo(int numeroTemporada) {
        String temporada = "Temp. " + numeroTemporada;
        Select select = new Select(this.encontrarElemento(SELECTOR_TEMPORADA_COMIENZO_PRESTAMO));
        select.selectByVisibleText(temporada);
    }

    private void seleccionarTemporadaFinalPrestamo(int numeroTemporada) {
        String temporada = "Temp. " + numeroTemporada;
        Select select = new Select(this.encontrarElemento(SELECTOR_TEMPORADA_FINAL_PRESTAMO));
        select.selectByVisibleText(temporada);
    }

    private void seleccionarPeriodo(String periodo) {
        Select select = new Select(this.encontrarElemento(SELECTOR_PERIODOS));
        select.selectByVisibleText(periodo);
    }

    private void seleccionarPeriodoComienzoPrestamo(String periodo) {
        Select select = new Select(this.encontrarElemento(SELECTOR_PERIODOS_COMIENZO_PRESTAMO));
        select.selectByVisibleText(periodo);
    }

    private void seleccionarPeriodoFinalPrestamo(String periodo) {
        Select select = new Select(this.encontrarElemento(SELECTOR_PERIODOS_FINAL_PRESTAMO));
        select.selectByVisibleText(periodo);
    }

    public void seVerificaQueSeMuestraMensajeTransferenciaExitosa() {
        assertThat(this.encontrarElemento(SELECTOR_MENSAJE_CARGA_EXITOSA).getText()).isEqualTo("La transferencia ha sido cargada exitosamente.");
    }

    public void seVerificaQueSeMuestraMensajeDeErrorQueAlMenosUnJUgadorFueTransferido() {
        assertThat(this.encontrarElemento(SELECTOR_MENSAJE_CARGA_ERRONEA).getText()).isEqualTo("La transferencia no es válida.");
    }

    public void irACarga() {
        this.encontrarElemento(SELECTOR_IR_A_CARGA).click();
    }

    public void irAMisTransferencias() {
        this.encontrarElemento(SELECTOR_IR_A_MIS_TRANSFERENCIAS).click();
    }

    public void seVeEnTransferenciasLaTransferenciaDelJugador(String nombreJugador) {
        assertThat(this.encontrarElemento(By.id(PREFIJO_SELECTOR_JUGADOR + nombreJugador)).getText()).contains(nombreJugador);
    }

    public void rechazarTransferencia(Integer idTransferencia) {
        this.encontrarElemento(By.id(PREFIJO_RECHAZAR_TRANSFERENCIA + idTransferencia)).click();
    }

    public void noSeVeEnTransferenciasLaTransferenciaDelJugador(String nombreJugador) {
        assertThat(this.driver.findElements(By.id(PREFIJO_SELECTOR_JUGADOR + nombreJugador))).isEmpty();
    }

    public void confirmarTransferencia(Integer idTransferencia) {
        this.encontrarElemento(By.id(PREFIJO_CONFIRMAR_TRANSFERENCIA + idTransferencia)).click();
    }

    public void irATodasLasTransferenciasConfirmadas() {
        this.encontrarElemento(SELECTOR_IR_A_TODAS_LAS_TRANSFERENCIAS).click();
    }

    public void cargarPrestamoJugadorPropio(String nombreJugador) {
        this.encontrarElemento(SELECTOR_AGREGADOR_PRESTAMO_JUGADOR_PROPIO).click();
        Select select = new Select(this.encontrarElemento(SELECTOR_JUGADORES));
        select.selectByVisibleText(nombreJugador);
        this.seleccionarTemporadaComienzoPrestamo(2);
        this.seleccionarTemporadaFinalPrestamo(3);
        this.seleccionarPeriodoComienzoPrestamo("Mitad");
        this.seleccionarPeriodoFinalPrestamo("Mitad");
        this.encontrarElemento(SELECTOR_CARGAR_JUGADOR).click();
    }

    public void seVeEnTransferenciasElPrestamoDelJugadorCon(String nombreJugador, int numeroTemporadaComienzo, String periodoComienzo, int numeroTemporadaFin, String periodoFin) {
        assertThat(this.encontrarElemento(By.id(PREFIJO_SELECTOR_JUGADOR + nombreJugador)).getText()).contains(nombreJugador,
                String.valueOf(numeroTemporadaComienzo), periodoComienzo);
        //ToDo: ver cómo manejar el id para verificar la parte del "hasta"
    }

    public void deshabilitarMercado() {
        this.encontrarElemento(SELECTOR_DESHABILITAR_MERCADO).click();
        this.driver.switchTo().alert().accept();
    }

    public void noseVeLaCargaDeTransferencias() {
        assertThat(this.driver.findElements(By.id(String.valueOf(SELECTOR_IR_A_CARGA)))).isEmpty();
    }
}
