package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PaginaRegistroUsuario extends PaginaBase {

    //private WebDriver driver;
    private static final By SELECTOR_CAMPO_ALIAS = By.id("campo-alias");
    private static final By SELECTOR_CAMPO_NOMBRE = By.id("campo-nombre");
    private static final By SELECTOR_CAMPO_APELLIDO = By.id("campo-apellido");
    private static final By SELECTOR_CAMPO_MAIL = By.id("campo-mail");
    private static final By SELECTOR_CAMPO_CONTRASENIA = By.id("campo-contrasenia");
    private static final By SELECTOR_BOTON_CREAR_USUARIO = By.id("boton-crear-usuario");
    private static final By SELECTOR_ALERTA_USUARIO_CREADO = By.id("alerta-usuario-creado");
    private static final By SELECTOR_ALERTA_USUARIO_NO_CREADO = By.id("alerta-error-creando-usuario");
    private static final By SELECTOR_MENSAJE_USUARIO_NO_CREADO = By.id("texto-error-alta-usuario");

    private static final String TEXTO_ALIAS_EXISTENTE = "El alias ya existe.";
    private static final String TEXTO_MAIL_EXISTENTE = "El mail ya existe.";

    public PaginaRegistroUsuario(WebDriver driver) {
        super(driver);
    }

    public void crearUsuario(String alias, String nombre, String apellido, String mail, String contrasenia) {
        this.encontrarElemento(SELECTOR_CAMPO_ALIAS).sendKeys(alias);
        this.encontrarElemento(SELECTOR_CAMPO_NOMBRE).sendKeys(nombre);
        this.encontrarElemento(SELECTOR_CAMPO_APELLIDO).sendKeys(apellido);
        this.encontrarElemento(SELECTOR_CAMPO_MAIL).sendKeys(mail);
        this.encontrarElemento(SELECTOR_CAMPO_CONTRASENIA).sendKeys(contrasenia);
        this.encontrarElemento(SELECTOR_BOTON_CREAR_USUARIO).click();
    }

    public boolean existePopupUsuarioCreado() {
        return this.encontrarElemento(SELECTOR_ALERTA_USUARIO_CREADO).isDisplayed();
    }

    public boolean existeAlertaUsuarioNoCreadoConMensajeAliasExistente() {
        WebElement alertaUsuarioNoCreado = this.encontrarElemento(SELECTOR_ALERTA_USUARIO_NO_CREADO);
        WebElement mensajeUsuarioNoCreado = this.encontrarElemento(SELECTOR_MENSAJE_USUARIO_NO_CREADO);
        return (alertaUsuarioNoCreado.isDisplayed() && mensajeUsuarioNoCreado.getText().contains(TEXTO_ALIAS_EXISTENTE));

    }

    public boolean existeAlertaUsuarioNoCreadoConMensajeMailExistente() {
        WebElement alertaUsuarioNoCreado = this.encontrarElemento(SELECTOR_ALERTA_USUARIO_NO_CREADO);
        WebElement mensajeUsuarioNoCreado = this.encontrarElemento(SELECTOR_MENSAJE_USUARIO_NO_CREADO);
        return (alertaUsuarioNoCreado.isDisplayed() && mensajeUsuarioNoCreado.getText().contains(TEXTO_MAIL_EXISTENTE));
    }
}
