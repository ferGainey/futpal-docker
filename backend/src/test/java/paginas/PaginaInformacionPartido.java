package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class PaginaInformacionPartido extends PaginaBase {

    private static final By SELECTOR_INFORMACION_PARTIDO = By.className("informacion-partido-liga");
    private static final By SELECTOR_VER_INFORMACION_GOLES = By.id("informacion-partido-ver-goles");
    private static final By SELECTOR_VER_INFORMACION_AMARILLAS = By.id("informacion-partido-ver-amarillas");
    private static final By SELECTOR_VER_INFORMACION_ROJAS = By.id("informacion-partido-ver-rojas");
    private static final By SELECTOR_VER_INFORMACION_LESIONES = By.id("informacion-partido-ver-lesiones");
    private static final By SELECTOR_VER_INFORMACION_MVP = By.id("informacion-partido-ver-mvp");
    private static final By INFORMACION_JUGADORES_LOCALES = By.className("jugador-local-agregado");
    private static final By INFORMACION_JUGADORES_VISITANTES = By.className("jugador-visitante-agregado");

    private static final By ESTADISTICA_GOLES = By.id("informacion-partido-estadistica-goles");
    private static final By ESTADISTICA_AMARILLAS = By.id("informacion-partido-estadistica-amarillas");
    private static final By ESTADISTICA_ROJAS = By.id("informacion-partido-estadistica-rojas");
    private static final By ESTADISTICA_LESIONES = By.id("informacion-partido-estadistica-lesiones");
    private static final By ESTADISTICA_MVP = By.id("informacion-partido-estadistica-mvp");

    public PaginaInformacionPartido(WebDriver driver) {
        super(driver);
    }

    public void abrirInformacionPartido() {
        List<WebElement> informacionPartidosLiga = this.encontrarElementos(SELECTOR_INFORMACION_PARTIDO);
        informacionPartidosLiga.get(0).click();
    }

    public List<String> obtenerGolesEquipoLocal() {
        this.encontrarElemento(SELECTOR_VER_INFORMACION_GOLES).click();
        try {
            this.encontrarElemento(ESTADISTICA_GOLES);
        } catch (Exception e) {
            this.encontrarElemento(SELECTOR_VER_INFORMACION_GOLES).click();
        }
        this.encontrarElemento(ESTADISTICA_GOLES);
        return driver.findElements(INFORMACION_JUGADORES_LOCALES).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerGolesEquipoVisitante() {
        this.encontrarElemento(SELECTOR_VER_INFORMACION_GOLES).click();
        try {
            this.encontrarElemento(ESTADISTICA_GOLES);
        } catch (Exception e) {
            this.encontrarElemento(SELECTOR_VER_INFORMACION_GOLES).click();
        }
        this.encontrarElemento(ESTADISTICA_GOLES);
        return driver.findElements(INFORMACION_JUGADORES_VISITANTES).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerAmarillasEquipoLocal() {
        this.encontrarElemento(SELECTOR_VER_INFORMACION_AMARILLAS).click();
        try {
            this.encontrarElemento(ESTADISTICA_AMARILLAS);
        } catch (Exception e) {
            this.encontrarElemento(SELECTOR_VER_INFORMACION_AMARILLAS).click();
        }
        this.encontrarElemento(ESTADISTICA_AMARILLAS);
        return driver.findElements(INFORMACION_JUGADORES_LOCALES).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerAmarillasEquipoVisitante() {
        this.encontrarElemento(SELECTOR_VER_INFORMACION_AMARILLAS).click();
        try {
            this.encontrarElemento(ESTADISTICA_AMARILLAS);
        } catch (Exception e) {
            this.encontrarElemento(SELECTOR_VER_INFORMACION_AMARILLAS).click();
        }
        this.encontrarElemento(ESTADISTICA_AMARILLAS);
        return driver.findElements(INFORMACION_JUGADORES_VISITANTES).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerRojasEquipoLocal() {
        this.encontrarElemento(SELECTOR_VER_INFORMACION_ROJAS).click();
        try {
            this.encontrarElemento(ESTADISTICA_ROJAS);
        } catch (Exception e) {
            this.encontrarElemento(SELECTOR_VER_INFORMACION_ROJAS).click();
        }
        this.encontrarElemento(ESTADISTICA_ROJAS);
        return driver.findElements(INFORMACION_JUGADORES_LOCALES).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerRojasEquipoVisitante() {
        this.encontrarElemento(SELECTOR_VER_INFORMACION_ROJAS).click();
        try {
            this.encontrarElemento(ESTADISTICA_ROJAS);
        } catch (Exception e) {
            this.encontrarElemento(SELECTOR_VER_INFORMACION_ROJAS).click();
        }
        return driver.findElements(INFORMACION_JUGADORES_VISITANTES).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerLesionesEquipoLocal() {
        this.encontrarElemento(SELECTOR_VER_INFORMACION_LESIONES).click();
        try {
            this.encontrarElemento(ESTADISTICA_LESIONES);
        } catch (Exception e) {
            this.encontrarElemento(SELECTOR_VER_INFORMACION_LESIONES).click();
        }
        this.encontrarElemento(ESTADISTICA_LESIONES);
        return driver.findElements(INFORMACION_JUGADORES_LOCALES).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerLesionesEquipoVisitante() {
        this.encontrarElemento(SELECTOR_VER_INFORMACION_LESIONES).click();
        try {
            this.encontrarElemento(ESTADISTICA_LESIONES);
        } catch (Exception e) {
            this.encontrarElemento(SELECTOR_VER_INFORMACION_LESIONES).click();
        }
        this.encontrarElemento(ESTADISTICA_LESIONES);
        return driver.findElements(INFORMACION_JUGADORES_VISITANTES).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerMvpEquipoVisitante() {
        this.encontrarElemento(SELECTOR_VER_INFORMACION_MVP).click();
        try {
            this.encontrarElemento(ESTADISTICA_MVP);
        } catch (Exception e) {
            this.encontrarElemento(SELECTOR_VER_INFORMACION_MVP).click();
        }
        this.encontrarElemento(ESTADISTICA_MVP);
        return driver.findElements(INFORMACION_JUGADORES_VISITANTES).stream().map(WebElement::getText).collect(Collectors.toList());
    }
}
