package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

public class PaginaPantallaInicialUsuario extends PaginaBase {

    private static final By SELECTOR_TEXTO_BIENVENIDA_USUARIO = By.id("titulo-pantalla-inicial-usuario");
    private static final By SELECTOR_LOG_OUT = By.id("boton-navbar-logout");
    private static final By SELECTOR_CREAR_LIGA = By.id("boton-navbar-crear-liga");
    private static final By SELECTOR_EDITAR_EQUIPO = By.id("boton-navbar-editar-equipo");
    private static final By SELECTOR_LIGAS_DISPONIBLES = By.id("boton-navbar-ligas-disponibles");
    private static final By SELECTOR_MI_PLANTEL = By.id("boton-navbar-plantel");
    private static final By SELECTOR_PLANTELES = By.id("boton-navbar-planteles");
    private static final By SELECTOR_TEMPORADA = By.id("boton-navbar-temporada");
    private static final By SELECTOR_AVANZAR_PERIODO = By.id("boton-avanzar-periodo");
    private static final By SELECTOR_BALANCES = By.id("boton-navbar-balances");
    private static final By SELECTOR_TRANSFERENCIAS = By.id("boton-navbar-transferencias");

    public PaginaPantallaInicialUsuario(WebDriver driver) {
        super(driver);
    }

    public String buscarTextoBienvenidaUsuario() {
        return this.encontrarElemento(SELECTOR_TEXTO_BIENVENIDA_USUARIO).getText();
    }

    public void desloguearse() {
        this.encontrarElemento(SELECTOR_LOG_OUT).click();
    }

    public void irAPaginaCrearLiga() {
        this.encontrarElemento(SELECTOR_CREAR_LIGA).click();
    }

    public void irAPaginaEditarEquipo() {
        this.encontrarElemento(SELECTOR_EDITAR_EQUIPO).click();
    }

    public void irAPaginaLigasDisponibles() {
        this.encontrarElemento(SELECTOR_LIGAS_DISPONIBLES).click();
    }

    public void irAPaginaMiPlantel() {
        this.encontrarElemento(SELECTOR_MI_PLANTEL).click();
    }

    public void irAPaginaPlanteles() {
        this.encontrarElemento(SELECTOR_PLANTELES).click();
    }

    public void verificarQueElPeriodoYTemporadaMostradoEs(String nombrePeriodo, int numeroTemporada) {
        await().atMost(5, TimeUnit.SECONDS).until(this.validarPeriodoYTemporada(nombrePeriodo, numeroTemporada));
    }

    private Callable<Boolean> validarPeriodoYTemporada(String nombrePeriodo, int numeroTemporada) {
        String temporadaEsperada = nombrePeriodo + " Temp. " + numeroTemporada;
        return () -> this.encontrarElemento(SELECTOR_TEMPORADA).getText().equals(temporadaEsperada);
    }

    public void avanzarPeriodo() {
        this.encontrarElemento(SELECTOR_TEMPORADA).click();
        this.encontrarElemento(SELECTOR_AVANZAR_PERIODO).click();
    }

    public void irABalances() {
        this.encontrarElemento(SELECTOR_BALANCES).click();
    }

    public void irATransferencias() {
        this.encontrarElemento(SELECTOR_TRANSFERENCIAS).click();
    }
}
