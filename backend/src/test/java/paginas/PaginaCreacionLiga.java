package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.stream.Collectors;

public class PaginaCreacionLiga extends PaginaBase {


    private static final By SELECTOR_CAMPO_NOMBRE_LIGA = By.id("campo-nombre-liga-creacion");
    private static final By SELECTOR_BOTON_CREAR_LIGA = By.id("boton-crear-liga");
    private static final By SELECTOR_TEXTO_ALERTA_CREAR_LIGA = By.id("texto-error-crear-liga");
    private static final By SELECTOR_COMBO_EQUIPOS_DISPONIBLES = By.id("agregador-equipo-equipos-registrados");
    private static final By SELECTOR_BOTON_AGREGAR_EQUIPO = By.id("creacion-liga-agregador-equipo-boton-agregar");
    private static final By SELECTOR_EQUIPOS_AGREGADOS = By.className("list-group-item");
    private static final By SELECTOR_EQUIPOS_AGREGADOS_BORRAR_PRIMER_EQUIPO = By.id("borrar0");

    public PaginaCreacionLiga(WebDriver driver) {
        super(driver);
    }

    public void cargarNombreDeLaLiga(String nombreLiga) {
        this.encontrarElemento(SELECTOR_CAMPO_NOMBRE_LIGA).sendKeys(nombreLiga);
    }

    public void crearLiga() {
        this.encontrarElemento(SELECTOR_BOTON_CREAR_LIGA).click();
    }

    public String buscarTextoAlertaErrorAlRegistrarLiga() {
        return this.encontrarElemento(SELECTOR_TEXTO_ALERTA_CREAR_LIGA).getText();
    }

    public List<String> buscarEquiposDisponibles() {
        return new Select(this.encontrarElemento(SELECTOR_COMBO_EQUIPOS_DISPONIBLES)).getOptions()
                .stream()
                .map(equipo -> equipo.getText())
                .collect(Collectors.toList());
    }

    public void agregarEquipo(String nombreEquipo) {
        Select select = new Select(this.encontrarElemento(SELECTOR_COMBO_EQUIPOS_DISPONIBLES));
        select.selectByVisibleText(nombreEquipo);
        this.encontrarElemento(SELECTOR_BOTON_AGREGAR_EQUIPO).click();
    }

    public List<String> buscarNombresEquiposAgregados() {
        return this.encontrarElementos(SELECTOR_EQUIPOS_AGREGADOS)
                .stream()
                .map(elemento -> elemento.getText())
                .collect(Collectors.toList());
    }

    public void borrarPrimerEquipo() {
        this.encontrarElemento(SELECTOR_EQUIPOS_AGREGADOS_BORRAR_PRIMER_EQUIPO).click();
    }

    public boolean verificarExistenciaListaEquiposAgregados() {
        return !this.driver.findElements(SELECTOR_EQUIPOS_AGREGADOS).isEmpty();
    }
}
