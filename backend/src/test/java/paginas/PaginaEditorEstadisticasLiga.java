package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.stream.Collectors;

public class PaginaEditorEstadisticasLiga extends PaginaBase {

    private static final By SELECTOR_VER_GOLES = By.id("editor-estadistica-ver-goles");
    private static final By SELECTOR_VER_AMARILLAS = By.id("editor-estadistica-ver-amarillas");
    private static final By SELECTOR_VER_ROJAS = By.id("editor-estadistica-ver-rojas");
    private static final By SELECTOR_VER_LESIONES = By.id("editor-estadistica-ver-lesiones");
    private static final By SELECTOR_VER_MVP = By.id("editor-estadistica-ver-mvp");

    private static final By COMBO_GOLES_LOCAL = By.id("agregador-goles-estadisticas-jugadores-locales");
    private static final By INPUT_GOLES_LOCAL = By.id("agregador-goles-estadisticas-goles-jugador-local");
    private static final By COMBO_AMARILLAS_LOCAL = By.id("agregador-tarjetas-amarillas-estadisticas-jugadores-locales");
    private static final By COMBO_ROJAS_LOCAL = By.id("agregador-tarjetas-rojas-estadisticas-jugadores-locales");
    private static final By COMBO_LESIONES_LOCAL = By.id("agregador-lesiones-estadisticas-jugadores-locales");
    private static final By COMBO_MVP_LOCAL = By.id("agregador-mvp-estadisticas-jugadores-locales");

    private static final By COMBO_GOLES_VISITANTE = By.id("agregador-goles-estadisticas-jugadores-visitantes");
    private static final By INPUT_GOLES_VISITANTE = By.id("agregador-goles-estadisticas-goles-jugador-visitante");
    private static final By COMBO_AMARILLAS_VISITANTE = By.id("agregador-tarjetas-amarillas-estadisticas-jugadores-visitantes");
    private static final By COMBO_ROJAS_VISITANTE = By.id("agregador-tarjetas-rojas-estadisticas-jugadores-visitantes");
    private static final By COMBO_LESIONES_VISITANTE = By.id("agregador-lesiones-estadisticas-jugadores-visitantes");
    private static final By COMBO_MVP_VISITANTE = By.id("agregador-mvp-estadisticas-jugadores-visitantes");

    private static final By BOTON_AGREGAR_GOL_JUGADOR_LOCAL = By.id("agregador-goles-estadisticas-boton-agregar-gol-jugador-local");
    private static final By BOTON_AGREGAR_AMARILLA_JUGADOR_LOCAL = By.id("agregador-amarillas-estadisticas-boton-agregar-amarilla-jugador-local");
    private static final By BOTON_AGREGAR_ROJA_JUGADOR_LOCAL = By.id("agregador-rojas-estadisticas-boton-agregar-roja-jugador-local");
    private static final By BOTON_AGREGAR_LESION_JUGADOR_LOCAL = By.id("agregador-lesiones-estadisticas-boton-agregar-lesion-jugador-local");
    private static final By BOTON_AGREGAR_MVP_JUGADOR_LOCAL = By.id("agregador-mvp-estadisticas-boton-agregar-mvp-jugador-local");

    private static final By BOTON_AGREGAR_GOL_JUGADOR_VISITANTE = By.id("agregador-goles-estadisticas-boton-agregar-gol-jugador-visitante");
    private static final By BOTON_AGREGAR_AMARILLA_JUGADOR_VISITANTE = By.id("agregador-amarillas-estadisticas-boton-agregar-amarilla-jugador-visitante");
    private static final By BOTON_AGREGAR_ROJA_JUGADOR_VISITANTE = By.id("agregador-rojas-estadisticas-boton-agregar-roja-jugador-visitante");
    private static final By BOTON_AGREGAR_LESION_JUGADOR_VISITANTE = By.id("agregador-lesiones-estadisticas-boton-agregar-lesion-jugador-visitante");
    private static final By BOTON_AGREGAR_MVP_JUGADOR_VISITANTE = By.id("agregador-mvp-estadisticas-boton-agregar-mvp-jugador-visitante");

    private static final By GOLEADORES_LOCAL = By.className("goles-jugador-local-agregado");
    private static final By AMARILLAS_LOCAL = By.className("amarillas-jugador-local-agregado");
    private static final By ROJAS_LOCAL = By.className("rojas-jugador-local-agregado");
    private static final By LESIONES_LOCAL = By.className("lesiones-jugador-local-agregado");
    private static final By MVP_LOCAL = By.className("mvp-jugador-local-agregado");

    private static final By BORRAR_JUGADOR_LOCAL_GOL = By.id("agregador-gol-borrar-jugador-local");

    public PaginaEditorEstadisticasLiga(WebDriver driver) {
        super(driver);
    }

    public void cargarEstadisticasGolesDelPartido() {
        this.encontrarElemento(SELECTOR_VER_GOLES).click();

        Select selectLocal = new Select(this.encontrarElemento(COMBO_GOLES_LOCAL));
        selectLocal.selectByVisibleText("Salah");
        this.encontrarElemento(INPUT_GOLES_LOCAL).sendKeys("3");
        this.encontrarElemento(BOTON_AGREGAR_GOL_JUGADOR_LOCAL).click();

        Select selectVisitante = new Select(this.encontrarElemento(COMBO_GOLES_VISITANTE));
        selectVisitante.selectByVisibleText("Alario");
        this.encontrarElemento(INPUT_GOLES_VISITANTE).sendKeys("1");
        this.encontrarElemento(BOTON_AGREGAR_GOL_JUGADOR_VISITANTE).click();
        selectVisitante.selectByVisibleText("Sand");
        this.encontrarElemento(INPUT_GOLES_VISITANTE).sendKeys("1");
        this.encontrarElemento(BOTON_AGREGAR_GOL_JUGADOR_VISITANTE).click();
    }

    public void cargarEstadisticasAmarillasDelPartido() {
        this.encontrarElemento(SELECTOR_VER_AMARILLAS).click();

        Select selectLocal = new Select(this.encontrarElemento(COMBO_AMARILLAS_LOCAL));
        selectLocal.selectByVisibleText("Salah");
        this.encontrarElemento(BOTON_AGREGAR_AMARILLA_JUGADOR_LOCAL).click();

        Select selectVisitante = new Select(this.encontrarElemento(COMBO_AMARILLAS_VISITANTE));
        selectVisitante.selectByVisibleText("Alario");
        this.encontrarElemento(BOTON_AGREGAR_AMARILLA_JUGADOR_VISITANTE).click();
    }

    public void cargarEstadisticasRojasDelPartido() {
        this.encontrarElemento(SELECTOR_VER_ROJAS).click();

        Select selectLocal = new Select(this.encontrarElemento(COMBO_ROJAS_LOCAL));
        selectLocal.selectByVisibleText("Pepe");
        this.encontrarElemento(BOTON_AGREGAR_ROJA_JUGADOR_LOCAL).click();

        Select selectVisitante = new Select(this.encontrarElemento(COMBO_ROJAS_VISITANTE));
        selectVisitante.selectByVisibleText("Sand");
        this.encontrarElemento(BOTON_AGREGAR_ROJA_JUGADOR_VISITANTE).click();
    }

    public void cargarEstadisticasLesionesDelPartido() {
        this.encontrarElemento(SELECTOR_VER_LESIONES).click();

        Select selectLocal = new Select(this.encontrarElemento(COMBO_LESIONES_LOCAL));
        selectLocal.selectByVisibleText("Salah");
        this.encontrarElemento(BOTON_AGREGAR_LESION_JUGADOR_LOCAL).click();

        Select selectVisitante = new Select(this.encontrarElemento(COMBO_LESIONES_VISITANTE));
        selectVisitante.selectByVisibleText("Alario");
        this.encontrarElemento(BOTON_AGREGAR_LESION_JUGADOR_VISITANTE).click();
    }

    public void cargarEstadisticasMvpDelPartido() {
        this.encontrarElemento(SELECTOR_VER_MVP).click();

        Select selectVisitante = new Select(this.encontrarElemento(COMBO_MVP_VISITANTE));
        selectVisitante.selectByVisibleText("Alario");
        this.encontrarElemento(BOTON_AGREGAR_MVP_JUGADOR_VISITANTE).click();
    }

    public List<String> obtenerGolesEquipoLocal() {
        this.encontrarElemento(SELECTOR_VER_GOLES).click();
        return driver.findElements(GOLEADORES_LOCAL).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerAmarillasEquipoLocal() {
        this.encontrarElemento(SELECTOR_VER_AMARILLAS).click();
        return this.encontrarElementos(AMARILLAS_LOCAL).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerRojasEquipoLocal() {
        this.encontrarElemento(SELECTOR_VER_ROJAS).click();
        if (driver.findElements(ROJAS_LOCAL) == null || driver.findElements(ROJAS_LOCAL).isEmpty()) {
            this.encontrarElemento(SELECTOR_VER_ROJAS).sendKeys(Keys.ENTER);
        }
        return this.encontrarElementos(ROJAS_LOCAL).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerLesionesEquipoLocal() {
        this.encontrarElemento(SELECTOR_VER_LESIONES).click();
        if (driver.findElements(LESIONES_LOCAL) == null || driver.findElements(LESIONES_LOCAL).isEmpty()) {
            this.encontrarElemento(SELECTOR_VER_LESIONES).sendKeys(Keys.ENTER);
        }
        return this.encontrarElementos(LESIONES_LOCAL).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerMvpEquipoLocal() {
        this.encontrarElemento(SELECTOR_VER_MVP).click();
        System.out.println(driver.findElements(MVP_LOCAL));
        if (driver.findElements(MVP_LOCAL) == null || driver.findElements(MVP_LOCAL).isEmpty()) {
            this.encontrarElemento(SELECTOR_VER_MVP).sendKeys(Keys.ENTER);
        }
        return this.encontrarElementos(MVP_LOCAL).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public void borrarPrimerGolLocal() {
        //ToDo: no hubo forma de arreglar esto en el tiempo dedicado: Element <a id="partidos-liga-side-nav" class="nav-link text-center pointer"> is not clickable at point (71,116) because another element <div class="fade modal show"> obscures it
        this.encontrarElemento(SELECTOR_VER_GOLES).click();
        this.encontrarElemento(BORRAR_JUGADOR_LOCAL_GOL).click();
    }
}
