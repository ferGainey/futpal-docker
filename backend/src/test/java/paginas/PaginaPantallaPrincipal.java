package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaginaPantallaPrincipal extends PaginaBase{

    private static final By SELECTOR_SIGN_UP = By.id("boton-signup");
    private static final By SELECTOR_LOG_IN = By.id("boton-navbar-login");
    private static final By SELECTOR_TEXTO_BIENVENIDA = By.id("titulo-bienvenida-pantalla-inicial");

    public PaginaPantallaPrincipal(WebDriver driver) {
        super(driver);
    }

    public void irAPantallaSignUp() {
        this.encontrarElemento(SELECTOR_SIGN_UP).click();
    }

    public void irAPantallaLogin() {
        this.encontrarElemento(SELECTOR_LOG_IN).click();
    }

    public String buscarTituloBienvenidaPantallaPrincipal() {
        return this.encontrarElemento(SELECTOR_TEXTO_BIENVENIDA).getText();
    }
}
