package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaginaEdicionEquipo extends PaginaBase {

    private static final By SELECTOR_TEXTO_EQUIPO = By.id("id-texto-equipo-edicion");
    private static final By SELECTOR_INPUT_NOMBRE_EQUIPO = By.id("campo-nombre-equipo-edicion");
    private static final By SELECTOR_BOTON_ACTUALIZAR = By.id("boton-actualizar-equipo");
    private static final By SELECTOR_TEXTO_EQUIPO_EDITADO_EXITOSAMENTE = By.id("texto-mensaje-edicion-equipo");

    public PaginaEdicionEquipo(WebDriver driver) {
        super(driver);
    }

    public String buscarTextoEquipo() {
        return this.encontrarElemento(SELECTOR_TEXTO_EQUIPO).getText();
    }

    public void ingresarNombreEquipo(String nombreEquipo) {
        this.encontrarElemento(SELECTOR_INPUT_NOMBRE_EQUIPO).sendKeys(nombreEquipo);
    }

    public void seleccionarActualizar() {
        this.encontrarElemento(SELECTOR_BOTON_ACTUALIZAR).click();
    }

    public String buscarTextoEquipoEditadoExitosamente() {
        return this.encontrarElemento(SELECTOR_TEXTO_EQUIPO_EDITADO_EXITOSAMENTE).getText();
    }
}
