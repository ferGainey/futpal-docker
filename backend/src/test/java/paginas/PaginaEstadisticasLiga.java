package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaginaEstadisticasLiga extends PaginaBase {

    private static final By SELECTOR_ESTADISTICAS_LIGA = By.id("estadisticas-liga-side-nav");

    //Ver de encontrar la vuelta para no usar xpath. Dificultad: son elementos reutilizados y dinámicos
    private static final By SELECTOR_NOMBRE_PRIMER_GOLEADOR = By.xpath("/html/body/div/div/div/div[2]/div[1]/div[1]/table/tbody/tr/td[1]");
    private static final By SELECTOR_CANTIDAD_GOLES_PRIMER_GOLEADOR = By.xpath("/html/body/div/div/div/div[2]/div[1]/div[1]/table/tbody/tr/td[3]");

    private static final By SELECTOR_NOMBRE_PRIMER_AMONESTADO = By.xpath("/html/body/div/div/div/div[2]/div[1]/div[2]/table/tbody/tr[1]/td[1]");
    private static final By SELECTOR_CANTIDAD_AMARILLAS_PRIMER_JUGADOR = By.xpath("/html/body/div/div/div/div[2]/div[1]/div[2]/table/tbody/tr[1]/td[3]");
    private static final By SELECTOR_NOMBRE_SEGUNDO_AMONESTADO = By.xpath("/html/body/div/div/div/div[2]/div[1]/div[2]/table/tbody/tr[2]/td[1]");
    private static final By SELECTOR_CANTIDAD_AMARILLAS_SEGUNDO_JUGADOR = By.xpath("/html/body/div/div/div/div[2]/div[1]/div[2]/table/tbody/tr[2]/td[3]");

    private static final By SELECTOR_NOMBRE_PRIMER_EXPULSADO = By.xpath("/html/body/div/div/div/div[2]/div[1]/div[3]/table/tbody/tr/td[1]");
    private static final By SELECTOR_CANTIDAD_ROJAS_PRIMER_JUGADOR = By.xpath("/html/body/div/div/div/div[2]/div[1]/div[3]/table/tbody/tr/td[3]");

    private static final By SELECTOR_NOMBRE_PRIMER_LESIONADO = By.xpath("/html/body/div/div/div/div[2]/div[2]/div[1]/table/tbody/tr[1]/td[1]");
    private static final By SELECTOR_CANTIDAD_LESIONES_PRIMER_JUGADOR = By.xpath("/html/body/div/div/div/div[2]/div[2]/div[1]/table/tbody/tr[1]/td[3]");

    private static final By SELECTOR_NOMBRE_PRIMER_MVP = By.xpath("/html/body/div/div/div/div[2]/div[2]/div[2]/table/tbody/tr[1]/td[1]");
    private static final By SELECTOR_CANTIDAD_MVP_PRIMER_JUGADOR = By.xpath("/html/body/div/div/div/div[2]/div[2]/div[2]/table/tbody/tr[1]/td[3]");
    private static final By SELECTOR_NOMBRE_SEGUNDO_MVP = By.xpath("/html/body/div/div/div/div[2]/div[2]/div[2]/table/tbody/tr[2]/td[1]");
    private static final By SELECTOR_CANTIDAD_MVP_SEGUNDO_JUGADOR = By.xpath("/html/body/div/div/div/div[2]/div[2]/div[2]/table/tbody/tr[2]/td[3]");

    public PaginaEstadisticasLiga(WebDriver driver) {
        super(driver);
    }

    public void irAEstadisticasLiga() {
        this.encontrarElemento(SELECTOR_ESTADISTICAS_LIGA).click();
    }

    public String obtenerNombrePrimerGoleador() {
        return this.encontrarElemento(SELECTOR_NOMBRE_PRIMER_GOLEADOR).getText();
    }

    public String obtenerCantidadGolesPrimerGoleador() {
        return this.encontrarElemento(SELECTOR_CANTIDAD_GOLES_PRIMER_GOLEADOR).getText();
    }

    public String obtenerNombrePrimerAmonestado() {
        return this.encontrarElemento(SELECTOR_NOMBRE_PRIMER_AMONESTADO).getText();
    }

    public String obtenerCantidadAmarillasPrimerJugador() {
        return this.encontrarElemento(SELECTOR_CANTIDAD_AMARILLAS_PRIMER_JUGADOR).getText();
    }

    public String obtenerNombreSegundoAmonestado() {
        return this.encontrarElemento(SELECTOR_NOMBRE_SEGUNDO_AMONESTADO).getText();
    }

    public String obtenerCantidadAmarillasSegundoJugador() {
        return this.encontrarElemento(SELECTOR_CANTIDAD_AMARILLAS_SEGUNDO_JUGADOR).getText();
    }

    public String obtenerNombrePrimerExpulsado() {
        return this.encontrarElemento(SELECTOR_NOMBRE_PRIMER_EXPULSADO).getText();
    }

    public String obtenerCantidadExpulsionesPrimerJugador() {
        return this.encontrarElemento(SELECTOR_CANTIDAD_ROJAS_PRIMER_JUGADOR).getText();
    }

    public String obtenerNombrePrimerLesionado() {
        return this.encontrarElemento(SELECTOR_NOMBRE_PRIMER_LESIONADO).getText();
    }

    public String obtenerCantidadLesionesPrimerJugador() {
        return this.encontrarElemento(SELECTOR_CANTIDAD_LESIONES_PRIMER_JUGADOR).getText();
    }

    public String obtenerNombrePrimerMvp() {
        return this.encontrarElemento(SELECTOR_NOMBRE_PRIMER_MVP).getText();
    }

    public String obtenerCantidadMvpsPrimerJugador() {
        return this.encontrarElemento(SELECTOR_CANTIDAD_MVP_PRIMER_JUGADOR).getText();
    }

    public String obtenerNombreSegundoMvp() {
        return this.encontrarElemento(SELECTOR_NOMBRE_SEGUNDO_MVP).getText();
    }

    public String obtenerCantidadMvpsSegundoJugador() {
        return this.encontrarElemento(SELECTOR_CANTIDAD_MVP_SEGUNDO_JUGADOR).getText();
    }
}
