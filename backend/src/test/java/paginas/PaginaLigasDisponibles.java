package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.stream.Collectors;

public class PaginaLigasDisponibles extends PaginaBase {

    private static final By SELECTOR_LIGAS_DISPONIBLES = By.className("list-group-item");

    public PaginaLigasDisponibles(WebDriver driver) {
        super(driver);
    }

    public List<String> buscarLigasDisponibles() {
        return this.encontrarElementos(SELECTOR_LIGAS_DISPONIBLES)
                .stream()
                .map(elemento -> elemento.getText())
                .collect(Collectors.toList());
    }

    public void irALigaDisponible(String nombreLiga) {
        this.encontrarElementos(SELECTOR_LIGAS_DISPONIBLES)
                .stream()
                .forEach(elemento -> {
                    if (elemento.getText().equals(nombreLiga)) {
                        elemento.click();
                        return;
                    }
                });
    }
}
