package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class PaginaPantallaPlantel extends PaginaBase {

    private static final By SELECTOR_AGREGAR_JUGADOR_INPUT_NOMBRE = By.id("agregador-jugador-plantel-input-nombre");
    private static final By SELECTOR_AGREGAR_JUGADOR_INPUT_ANIO_NACIMIENTO = By.id("agregador-jugador-plantel-input-fecha-nacimiento");
    private static final By SELECTOR_AGREGAR_JUGADOR_INPUT_VALOR = By.id("agregador-jugador-plantel-input-valor");
    private static final By SELECTOR_AGREGAR_JUGADOR_INPUT_SALARIO = By.id("agregador-jugador-plantel-input-salario");
    private static final By SELECTOR_AGREGAR_JUGADOR_INPUT_MEDIA = By.id("agregador-jugador-plantel-input-media");
    private static final By SELECTOR_BOTON_AGREGAR_JUGADOR = By.id("agregador-jugador-boton-agregar");
    private static final By SELECTOR_TEXTO_ESTADO_EQUIPO = By.id("mi-plantel-mensaje-estado-equipo");
    private static final By SELECTOR_BOTON_IR_A_POPUP_AGREGAR_JUGADOR = By.id("mi-plantel-boton-agregar-jugador");
    private static final By SELECTOR_TEXTO_EQUIPO = By.id("mi-plantel-texto-equipo");
    private static final By SELECTOR_NOMBRE_JUGADOR = By.className("columna-nombre-jugador");
    private static final By SELECTOR_SOLICITAR_APROBACION_EQUIPO = By.id("mi-plantel-solicitar-aprobacion");
    private static final By SELECTOR_ACCION_BORRAR_JUGADOR = By.id("tabla-jugadores-accion-borrado-jugador");
    private static final By TEXTO_MOTIVO_CAMBIO_ESTADO = By.id("mi-plantel-mensaje-cambio-estado");
    private static final By SELECTOR_AGREGAR_JUGADOR_INPUT_ID_TRANSFERMARKT = By.id("agregador-jugador-plantel-input-id_transfermarkt");
    private static final By JUGADORES_PRESTADOS = By.id("mis-jugadores-prestados-side-nav");
    private static final By SELECTOR_TEMPORADA_VUELTA_PRESTAMO = By.className("columna-numero-temporada");
    private static final By SELECTOR_PERIODOS_VUELTA_PRESTAMO = By.className("columna-periodo");

    public PaginaPantallaPlantel(WebDriver webDriver) {
        super(webDriver);
    }

    public void agregarJugador(String nombre, LocalDate fechaNacimiento, int valor, int salario, int media, int idTransfermarkt) {
        this.encontrarElemento(SELECTOR_BOTON_IR_A_POPUP_AGREGAR_JUGADOR).click();
        this.encontrarElemento(SELECTOR_AGREGAR_JUGADOR_INPUT_NOMBRE).sendKeys(nombre);
        this.encontrarElemento(SELECTOR_AGREGAR_JUGADOR_INPUT_ANIO_NACIMIENTO).sendKeys(String.valueOf(fechaNacimiento));
        this.encontrarElemento(SELECTOR_AGREGAR_JUGADOR_INPUT_VALOR).sendKeys(String.valueOf(valor));
        this.encontrarElemento(SELECTOR_AGREGAR_JUGADOR_INPUT_SALARIO).sendKeys(String.valueOf(salario));
        this.encontrarElemento(SELECTOR_AGREGAR_JUGADOR_INPUT_MEDIA).sendKeys(String.valueOf(media));
        this.encontrarElemento(SELECTOR_AGREGAR_JUGADOR_INPUT_ID_TRANSFERMARKT).sendKeys(String.valueOf(idTransfermarkt));
        this.encontrarElemento(SELECTOR_BOTON_AGREGAR_JUGADOR).click();
    }

    public String obtenerTextoMensajeEstadoEquipo() {
        return this.encontrarElemento(SELECTOR_TEXTO_ESTADO_EQUIPO).getText();
    }

    public boolean estaBotonAgregarJugadorHabilitado() {
        return this.driver.findElement(SELECTOR_BOTON_IR_A_POPUP_AGREGAR_JUGADOR).isEnabled();
    }

    public String obtenerTextoEquipo() {
        return this.encontrarElemento(SELECTOR_TEXTO_EQUIPO).getText();
    }

    public String obtenerNombrePrimerJugador() {
        return this.encontrarElementos(SELECTOR_NOMBRE_JUGADOR).get(0).getText();
    }

    public String obtenerNombreSegundoJugador() {
        return this.encontrarElementos(SELECTOR_NOMBRE_JUGADOR).get(1).getText();
    }

    public void solicitarAprobacionEquipo() {
        this.encontrarElemento(SELECTOR_SOLICITAR_APROBACION_EQUIPO).click();
    }

    public void borrarPrimerJugador(String nombreJugador) {
        this.encontrarElementos(SELECTOR_ACCION_BORRAR_JUGADOR).get(0).click();
    }

    public List<String> obtenerNombreJugadores() {
        return this.encontrarElementos(SELECTOR_NOMBRE_JUGADOR).stream().map(elemento -> elemento.getText()).collect(Collectors.toList());
    }

    public String obtenerTextoMotivoCambioEstado() {
        return this.encontrarElemento(TEXTO_MOTIVO_CAMBIO_ESTADO).getText();
    }

    public void irAJugadoresPrestados() {
        this.encontrarElemento(JUGADORES_PRESTADOS).click();
    }

    public List<String> obtenerNumerosTemporadas() {
        return this.encontrarElementos(SELECTOR_TEMPORADA_VUELTA_PRESTAMO).stream().map(elemento -> elemento.getText()).collect(Collectors.toList());
    }

    public List<String> obtenerNombrePeriodosVueltaPrestamo() {
        return this.encontrarElementos(SELECTOR_PERIODOS_VUELTA_PRESTAMO).stream().map(elemento -> elemento.getText()).collect(Collectors.toList());
    }
}
