package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.stream.Collectors;

public class PaginaPlanteles extends PaginaBase {

    private static final By SELECTOR_COMBO_EQUIPOS_APROBADOS = By.id("planteles-equipos-aprobados");
    private static final By SELECTOR_COMBO_EQUIPOS_PENDIENTE_APROBACION = By.id("aprobador-equipo-equipos-pendiente-aprobacion");
    private static final By SELECTOR_NOMBRE_JUGADOR = By.className("columna-nombre-jugador");
    private static final By BOTON_ACEPTAR_EQUIPO_PENDIENTE_APROBACION = By.id("aprobador-equipo-aceptar-equipo-pendiente-aprobacion");
    private static final By BOTON_RECHAZAR_EQUIPO_PENDIENTE_APROBACION = By.id("aprobador-equipo-rechazar-equipo-pendiente-aprobacion");
    private static final By SELECTOR_ACCION_BORRAR_JUGADOR = By.id("tabla-jugadores-accion-borrado-jugador");
    private static final By TEXTO_POPUP_EQUIPO_APROBADO = By.id("popup-equipo-aprobado-texto");
    private static final By TEXT_AREA_MOTIVO_RECHAZO = By.id("popup-equipo-rechazo-motivo");
    private static final By ENVIAR_MOTIVO_RECHAZO = By.id("popup-equipo-rechazado-enviar");
    private static final By BOTON_EDITAR_PRIMER_JUGADOR = By.id("tabla-jugadores-accion-edicion-jugador");
    private static final By CAMPO_NOMBRE_EDICION_JUGADOR = By.id("editor-jugador-input-nombre");
    private static final By BOTON_ENVIAR_EDICION_JUGADOR = By.id("editor-jugador-boton-enviar");
    private static final By IR_A_PAGINA_BUSQUEDA_JUGADORES = By.id("buscador-jugadores-side-nav");
    private static final By HABILITAR_BUSQUEDA_POR_ID_TRANSFERMARKT = By.id("busqueda-por-id-transfermarkt");
    private static final By HABILITAR_BUSQUEDA_POR_NOMBRE_JUGADOR = By.id("busqueda-por-nombre");
    private static final By INPUT_BUSQUEDA_POR_ID_TRANSFERMARKT = By.id("input-id-transfermarkt");
    private static final By INPUT_BUSQUEDA_POR_NOMBRE_JUGADOR = By.id("input-nombre");
    private static final By BUSCAR_JUGADORES_FILTRADOS = By.id("buscar-jugadores");
    private static final By SELECTOR_EQUIPO_JUGADOR = By.className("columna-equipo-jugador");


    public PaginaPlanteles(WebDriver webDriver) {
        super(webDriver);
    }

    public void seleccionarEquipo(String nombreEquipo) {
        Select select = new Select(this.encontrarElemento(SELECTOR_COMBO_EQUIPOS_APROBADOS));
        select.selectByVisibleText(nombreEquipo);
    }

    public String obtenerNombrePrimerJugador() {
        return this.encontrarElementos(SELECTOR_NOMBRE_JUGADOR).get(0).getText();
    }

    public String obtenerNombreSegundoJugador() {
        return this.encontrarElementos(SELECTOR_NOMBRE_JUGADOR).get(1).getText();
    }

    public void seleccionarEquipoPendienteAprobacionDelCombo(String nombreEquipo) {
        Select select = new Select(this.encontrarElemento(SELECTOR_COMBO_EQUIPOS_PENDIENTE_APROBACION));
        select.selectByVisibleText(nombreEquipo);
    }

    public void confirmarEquipoPendienteAprobacion() {
        this.encontrarElemento(BOTON_ACEPTAR_EQUIPO_PENDIENTE_APROBACION).click();
    }

    public List<String> obtenerEquiposPendienteAprobacion() {
        Select select = new Select(this.encontrarElemento(SELECTOR_COMBO_EQUIPOS_PENDIENTE_APROBACION));
        return select.getOptions().stream().map(opcion -> opcion.getText()).collect(Collectors.toList());
    }

    public List<String> obtenerEquiposAprobados() {
        Select select = new Select(this.encontrarElemento(SELECTOR_COMBO_EQUIPOS_APROBADOS));
        return select.getOptions().stream().map(opcion -> opcion.getText()).collect(Collectors.toList());
    }

    public void rechazarEquipoPendienteAprobacion() {
        this.encontrarElemento(BOTON_RECHAZAR_EQUIPO_PENDIENTE_APROBACION).click();
    }

    public void borrarPrimerJugador(String nombreJugador) {
        this.encontrarElementos(SELECTOR_ACCION_BORRAR_JUGADOR).get(0).click();
    }

    public List<String> obtenerNombreJugadores() {
        return this.encontrarElementos(SELECTOR_NOMBRE_JUGADOR).stream().map(elemento -> elemento.getText()).collect(Collectors.toList());
    }

    public String obtenerTextoConfirmacionEquipoAprobado() {
        return this.encontrarElemento(TEXTO_POPUP_EQUIPO_APROBADO).getText();
    }

    public void escribirMotivoRechazoEquipo(String motivoRechazo) {
        this.encontrarElemento(TEXT_AREA_MOTIVO_RECHAZO).sendKeys(motivoRechazo);
        this.encontrarElemento(ENVIAR_MOTIVO_RECHAZO).click();
    }

    public void editarNombrePrimerJugador(String nombreJugadorEditado) {
        this.encontrarElementos(BOTON_EDITAR_PRIMER_JUGADOR).get(0).click();
        this.encontrarElemento(CAMPO_NOMBRE_EDICION_JUGADOR).clear();
        this.encontrarElemento(CAMPO_NOMBRE_EDICION_JUGADOR).sendKeys(nombreJugadorEditado);
        this.encontrarElemento(BOTON_ENVIAR_EDICION_JUGADOR).click();
    }

    public void irABusquedaDeJugadores() {
        this.encontrarElemento(IR_A_PAGINA_BUSQUEDA_JUGADORES).click();
    }

    public void filtrarJugadorPorIdTransfermarkt(int idTransfermarkt) {
        this.encontrarElemento(HABILITAR_BUSQUEDA_POR_ID_TRANSFERMARKT).click();
        this.encontrarElemento(INPUT_BUSQUEDA_POR_ID_TRANSFERMARKT).sendKeys(String.valueOf(idTransfermarkt));
    }

    public void filtrarJugadorPorNombre(String nombre) {
        this.encontrarElemento(HABILITAR_BUSQUEDA_POR_NOMBRE_JUGADOR).click();
        this.encontrarElemento(INPUT_BUSQUEDA_POR_NOMBRE_JUGADOR).sendKeys(nombre);
    }

    public void hacerBusquedaJugadoresFiltrados() {
        this.encontrarElemento(BUSCAR_JUGADORES_FILTRADOS).click();
    }

    public List<String> obtenerNombreJugadoresFiltrados() {
        return this.encontrarElementos(SELECTOR_NOMBRE_JUGADOR).stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerEquiposJugadoresFiltrados() {
        return this.encontrarElementos(SELECTOR_EQUIPO_JUGADOR).stream().map(WebElement::getText).collect(Collectors.toList());
    }
}
