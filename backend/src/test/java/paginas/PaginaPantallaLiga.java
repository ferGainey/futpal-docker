package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class PaginaPantallaLiga extends PaginaBase {

    private static final By SELECTOR_TITULO_LIGA = By.id("titulo-pantalla-liga");
    private static final By SELECTOR_COLUMNA_EQUIPOS = By.className("columna-nombre-equipo");
    private static final By SELECTOR_ENCABEZADO_COLUMNA_POSICIONES = By.id("encabezado-columna-posiciones-liga");
    private static final By SELECTOR_ENCABEZADO_COLUMNA_PUNTOS = By.id("encabezado-columna-puntos-liga");
    private static final By SELECTOR_ENCABEZADO_COLUMNA_PARTIDOS_JUGADOS = By.id("encabezado-columna-partidos-jugados-liga");
    private static final By SELECTOR_ENCABEZADO_COLUMNA_GOLES_A_FAVOR = By.id("encabezado-columna-goles-a-favor-liga");
    private static final By SELECTOR_ENCABEZADO_COLUMNA_GOLES_EN_CONTRA = By.id("encabezado-columna-goles-en-contra-liga");
    private static final By SELECTOR_ENCABEZADO_COLUMNA_DIFERENCIA_DE_GOLES = By.id("encabezado-columna-diferencia-de-goles-liga");
    private static final By SELECTOR_PARTIDOS_LIGA = By.id("partidos-liga-side-nav");
    private static final By SELECTOR_POSICIONES_LIGA = By.id("posiciones-liga-side-nav");
    private static final By SELECTOR_COLUMNA_POSICIONES = By.className("columna-numero-posicion");
    private static final By SELECTOR_COLUMNA_NOMBRE_EQUIPO = By.className("columna-nombre-equipo");
    private static final By SELECTOR_COLUMNA_PARTIDOS_JUGADOS = By.className("columna-partidos-jugados");
    private static final By SELECTOR_COLUMNA_GOLES_A_FAVOR = By.className("columna-goles-a-favor");
    private static final By SELECTOR_COLUMNA_GOLES_EN_CONTRA = By.className("columna-goles-en-contra");
    private static final By SELECTOR_COLUMNA_DIFERENCIA_DE_GOLES = By.className("columna-diferencia-de-goles");
    private static final By SELECTOR_COLUMNA_PUNTOS = By.className("columna-puntos");

    public PaginaPantallaLiga(WebDriver driver) {
        super(driver);
    }

    public String buscarTextoTituloLiga() {
        return this.encontrarElemento(SELECTOR_TITULO_LIGA).getText();
    }

    //ARREGLAR: SI NO HAY RESULTADOS, ENTONCES DEBERÍA MOSTRARME EL NOMBRE DE LOS EQUIPOS AL MENOS. VER SI NO LO HACE YA
    public List<String> obtenerEquipos() {
        List<WebElement> columnaNombresEquipos = this.encontrarElementos(SELECTOR_COLUMNA_EQUIPOS);
        return columnaNombresEquipos.stream().map(fila -> fila.getText()).collect(Collectors.toList());
    }

    public String obtenerTextoEncabezadoColumnaPosiciones() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_COLUMNA_POSICIONES).getText();
    }

    public String obtenerTextoEncabezadoColumnaPuntos() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_COLUMNA_PUNTOS).getText();
    }

    public String obtenerTextoEncabezadoColumnaPartidosJugados() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_COLUMNA_PARTIDOS_JUGADOS).getText();
    }

    public String obtenerTextoEncabezadoColumnaGolesAFavor() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_COLUMNA_GOLES_A_FAVOR).getText();
    }

    public String obtenerTextoEncabezadoColumnaGolesEnContra() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_COLUMNA_GOLES_EN_CONTRA).getText();
    }

    public String obtenerTextoEncabezadoColumnaDiferenciaDeGoles() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_COLUMNA_DIFERENCIA_DE_GOLES).getText();
    }

    public void irAPartidos() {
        this.encontrarElemento(SELECTOR_PARTIDOS_LIGA).click();
    }

    public void irAPosiciones() {
        this.encontrarElemento(SELECTOR_POSICIONES_LIGA).click();
    }

    public String obtenerNumeroPrimeraPosicion() {
        return this.encontrarElementos(SELECTOR_COLUMNA_POSICIONES).get(0).getText();
    }

    public String obtenerColumnaNombreEquipoPrimeraPosicion() {
        return this.encontrarElementos(SELECTOR_COLUMNA_NOMBRE_EQUIPO).get(0).getText();
    }

    public String obtenerColumnaPartidosJugadosPrimeraPosicion() {
        return this.encontrarElementos(SELECTOR_COLUMNA_PARTIDOS_JUGADOS).get(0).getText();
    }

    public String obtenerColumnaGolesAFavorPrimeraPosicion() {
        return this.encontrarElementos(SELECTOR_COLUMNA_GOLES_A_FAVOR).get(0).getText();
    }

    public String obtenerColumnaGolesEnContraPrimeraPosicion() {
        return this.encontrarElementos(SELECTOR_COLUMNA_GOLES_EN_CONTRA).get(0).getText();
    }

    public String obtenerColumnaDiferenciaDeGolesPrimeraPosicion() {
        return this.encontrarElementos(SELECTOR_COLUMNA_DIFERENCIA_DE_GOLES).get(0).getText();
    }

    public String obtenerColumnaPuntosPrimeraPosicion() {
        return this.encontrarElementos(SELECTOR_COLUMNA_PUNTOS).get(0).getText();
    }
}
