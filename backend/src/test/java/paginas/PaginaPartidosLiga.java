package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.awaitility.Awaitility.await;

public class PaginaPartidosLiga extends PaginaBase {

    private static final By SELECTOR_TITULO_PAGINA_PARTIDOS_LIGA = By.id("titulo-partidos-liga");
    private static final By SELECTOR_ENCABEZADO_EQUIPO_LOCAL = By.id("partidos-liga-equipo-local");
    private static final By SELECTOR_ENCABEZADO_EQUIPO_VISITANTE = By.id("partidos-liga-equipo-visitante");
    private static final By SELECTOR_ENCABEZADO_GOLES_EQUIPO_LOCAL = By.id("partidos-liga-goles-equipo-local");
    private static final By SELECTOR_ENCABEZADO_GOLES_EQUIPO_VISITANTE = By.id("partidos-liga-goles-equipo-visitante");
    private static final By SELECTOR_ENCABEZADO_FECHA_PARTIDO = By.id("partidos-liga-encabezado-fecha-partido");
    private static final By SELECTOR_COLUMNA_EQUIPOS_LOCALES = By.className("partidos-liga-columna-equipo-local");
    private static final By SELECTOR_COLUMNA_EQUIPOS_VISITANTES = By.className("partidos-liga-columna-equipo-visitante");
    private static final By SELECTOR_COLUMNA_GOLES_EQUIPOS_LOCALES = By.className("partidos-liga-columna-goles-equipo-local");
    private static final By SELECTOR_COLUMNA_GOLES_EQUIPOS_VISITANTES = By.className("partidos-liga-columna-goles-equipo-visitante");
    private static final By SELECTOR_COLUMNA_ESTADO_PARTIDOS = By.className("partidos-liga-columna-estado-partido");
    private static final By SELECTOR_EDITOR_PARTIDO = By.className("editor-partido-liga");

    public PaginaPartidosLiga(WebDriver driver) {
        super(driver);
    }


    public String obtenerTituloPagina() {
        return this.encontrarElemento(SELECTOR_TITULO_PAGINA_PARTIDOS_LIGA).getText();
    }

    public String obtenerTextoEncabezadoEquipoLocal() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_EQUIPO_LOCAL).getText();
    }

    public String obtenerTextoEncabezadoEquipoVisitante() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_EQUIPO_VISITANTE).getText();
    }

    public WebElement obtenerEncabezadoGolesEquipoLocal() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_GOLES_EQUIPO_LOCAL);
    }

    public WebElement obtenerEncabezadoGolesEquipoVisitante() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_GOLES_EQUIPO_VISITANTE);
    }

    public String obtenerEncabezadoFechaPartido() {
        return this.encontrarElemento(SELECTOR_ENCABEZADO_FECHA_PARTIDO).getText();
    }

    public List<String> obtenerEquiposLocales() {
        List<WebElement> columnaNombresEquiposLocales = this.encontrarElementos(SELECTOR_COLUMNA_EQUIPOS_LOCALES);
        return columnaNombresEquiposLocales.stream().map(fila -> fila.getText()).collect(Collectors.toList());
    }

    public List<String> obtenerEquiposVisitantes() {
        List<WebElement> columnaNombresEquiposVisitantes = this.encontrarElementos(SELECTOR_COLUMNA_EQUIPOS_VISITANTES);
        return columnaNombresEquiposVisitantes.stream().map(fila -> fila.getText()).collect(Collectors.toList());
    }

    public List<String> obtenerGolesEquiposLocales() {
        List<WebElement> columnaGolesEquiposLocales = this.encontrarElementos(SELECTOR_COLUMNA_GOLES_EQUIPOS_LOCALES);
        return columnaGolesEquiposLocales.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerGolesEquiposVisitantes() {
        List<WebElement> columnaGolesEquiposVisitantes = this.encontrarElementos(SELECTOR_COLUMNA_GOLES_EQUIPOS_VISITANTES);
        return columnaGolesEquiposVisitantes.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public List<String> obtenerEstadosPartidos() {
        List<WebElement> estadosPartidos = this.encontrarElementos(SELECTOR_COLUMNA_ESTADO_PARTIDOS);
        return estadosPartidos.stream().map(fila -> fila.getText()).collect(Collectors.toList());
    }

    public void abrirEditorResultadoPrimerPartido() {
        List<WebElement> editoresPartidosLiga = this.encontrarElementos(SELECTOR_EDITOR_PARTIDO);
        editoresPartidosLiga.get(0).click();
    }

    public void verificarQueSeHayaActualizadoElPartidoConGolesLocalYVisitante(int golesLocal, int golesVisitante) {
        await().atMost(5, TimeUnit.SECONDS).until(this.validarGolesLocalPrimerPartido(String.valueOf(golesLocal)));
        await().atMost(5, TimeUnit.SECONDS).until(this.validarGolesVisitantePrimerPartido(String.valueOf(golesVisitante)));
    }

    private Callable<Boolean> validarGolesLocalPrimerPartido(String golesEsperados) {
        return () -> this.encontrarElementos(SELECTOR_COLUMNA_GOLES_EQUIPOS_LOCALES).get(0).getText().equals(golesEsperados);
    }

    private Callable<Boolean> validarGolesVisitantePrimerPartido(String golesEsperados) {
        return () -> this.encontrarElementos(SELECTOR_COLUMNA_GOLES_EQUIPOS_VISITANTES).get(0).getText().equals(golesEsperados);
    }

    public void filtrarPartidosPorElEquipo(String nombreEquipo) {
        List<WebElement> columnaNombresEquipos = this.encontrarElementos(SELECTOR_COLUMNA_EQUIPOS_LOCALES);
        columnaNombresEquipos.addAll(this.encontrarElementos(SELECTOR_COLUMNA_EQUIPOS_VISITANTES));
        columnaNombresEquipos.stream()
                .filter(tupla -> tupla.getText().contains(nombreEquipo))
                .collect(Collectors.toList())
                .get(0)
                .click();
    }
}
