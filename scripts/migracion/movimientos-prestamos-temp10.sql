DO $$

DECLARE TEMP_11 integer := 11;
DECLARE TEMP_12 integer := 12;
DECLARE PERIODO_PRINCIPIO integer := 1;
DECLARE ESTADO_TRANSFERENCIA_CONFIRMADA integer := 3;
DECLARE TIPO_MOVIMIENTO_PRESTAMO integer := 2;

DECLARE USUARIO_YID_ARMY integer := 3;
DECLARE USUARIO_LOS_VASCOS integer := 20;
DECLARE USUARIO_PROVINCIANOS integer := 11;
DECLARE USUARIO_LOS_VENGADORES integer := 65;
DECLARE USUARIO_DEFE_DI_CAPI integer := 77;
DECLARE USUARIO_VARELA integer := 12;
DECLARE USUARIO_LOS_IRRESPETUOSOS integer := 15;
DECLARE USUARIO_TREVELIN integer := 70;
DECLARE USUARIO_POLLERA integer := 66;
DECLARE USUARIO_MATOS_ROCK integer := 78;
DECLARE USUARIO_YO_DUERMO integer := 73;
DECLARE USUARIO_DAVE_HOLLAND integer := 21;
DECLARE USUARIO_TIKI_TIKI integer := 69;
DECLARE USUARIO_ST_PAULI integer := 17;
DECLARE USUARIO_TERMIDOR integer := 60;
DECLARE USUARIO_BARRILETE_COSMICO integer := 25;
DECLARE USUARIO_IL_RITORNI integer := 64;
DECLARE USUARIO_NIUPI integer := 14;
DECLARE USUARIO_ESQUIMBO integer := 63;
DECLARE USUARIO_CHAMPAGNE integer := 26;
DECLARE USUARIO_LA_CAPRICHOSA integer := 59;
DECLARE USUARIO_CEBOLLITAS integer := 24;
DECLARE USUARIO_TETRABRICK integer := 9;
DECLARE USUARIO_UYNOSE integer := 18;
DECLARE USUARIO_A_PURO_FIDEO integer := 62;
DECLARE USUARIO_ESTUPIDO_FLANDERS integer := 13;
DECLARE USUARIO_EL_TACO_NO integer := 16;
DECLARE USUARIO_LOS_CHELITOS integer := 61;
DECLARE USUARIO_SUPER420 integer := 68;
DECLARE USUARIO_EL_BICHO integer := 23;
DECLARE USUARIO_A_LA_TIBIA integer := 8;
DECLARE USUARIO_NO_ROOM integer := 6;
DECLARE USUARIO_BRANCA_UNITED integer := 4;
DECLARE USUARIO_TERCERA_POSICION integer := 19;
DECLARE USUARIO_LASTIMA_A_NADIE integer := 75;

DECLARE EQUIPO_YID_ARMY integer := 1;
DECLARE EQUIPO_LOS_VASCOS integer := 14;
DECLARE EQUIPO_PROVINCIANOS integer := 5;
DECLARE EQUIPO_LOS_VENGADORES integer := 63;
DECLARE EQUIPO_DEFE_DI_CAPI integer := 75;
DECLARE EQUIPO_VARELA integer := 9;
DECLARE EQUIPO_LOS_IRRESPETUOSOS integer := 8;
DECLARE EQUIPO_TREVELIN integer := 68;
DECLARE EQUIPO_POLLERA integer := 64;
DECLARE EQUIPO_MATOS_ROCK integer := 76;
DECLARE EQUIPO_YO_DUERMO integer := 71;
DECLARE EQUIPO_DAVE_HOLLAND integer := 16;
DECLARE EQUIPO_TIKI_TIKI integer := 67;
DECLARE EQUIPO_ST_PAULI integer := 57;
DECLARE EQUIPO_TERMIDOR integer := 58;
DECLARE EQUIPO_BARRILETE_COSMICO integer := 21;
DECLARE EQUIPO_IL_RITORNI integer := 61;
DECLARE EQUIPO_NIUPI integer := 7;
DECLARE EQUIPO_ESQUIMBO integer := 60;
DECLARE EQUIPO_CHAMPAGNE integer := 23;
DECLARE EQUIPO_LA_CAPRICHOSA integer := 56;
DECLARE EQUIPO_CEBOLLITAS integer := 20;
DECLARE EQUIPO_TETRABRICK integer := 18;
DECLARE EQUIPO_UYNOSE integer := 10;
DECLARE EQUIPO_A_PURO_FIDEO integer := 59;
DECLARE EQUIPO_ESTUPIDO_FLANDERS integer := 6;
DECLARE EQUIPO_EL_TACO_NO integer := 15;
DECLARE EQUIPO_LOS_CHELITOS integer := 62;
DECLARE EQUIPO_SUPER420 integer := 66;
DECLARE EQUIPO_EL_BICHO integer := 19;
DECLARE EQUIPO_A_LA_TIBIA integer := 12;
DECLARE EQUIPO_NO_ROOM integer := 11;
DECLARE EQUIPO_BRANCA_UNITED integer := 22;
DECLARE EQUIPO_TERCERA_POSICION integer := 13;
DECLARE EQUIPO_LASTIMA_A_NADIE integer := 73;

DECLARE DUVAN_ZAPATA integer := 828;
DECLARE TIMO_WERNER integer := 68;
DECLARE EDUARDO_SALVIO integer := 54;
DECLARE LISANDRO_MARTINEZ integer := 303;
DECLARE WILFRIED_ZAHA integer := 149;
DECLARE NGOLO_KANTE integer := 1624;
DECLARE ALEXIS_SANCHEZ integer := 2065;
DECLARE INAKI_WILLIAMS integer := 738;
DECLARE MATEJ_VYDRA integer := 1348;
DECLARE HARVEY_BARNES integer := 831;
DECLARE TAKUMI_MINAMINO integer := 2067;
DECLARE ERLING_HAALAND integer := 739;
DECLARE FRANCESCO_ACERBI integer := 1623;
DECLARE ROMELU_LUKAKU integer := 1215;
DECLARE ENZO_PEREZ integer := 1214;
DECLARE ALEX_SANDRO integer := 1347;
DECLARE DIOGO_JOTA integer := 1091;
DECLARE AARON_LONG integer := 987;
DECLARE SZOBOSZLAI integer := 2209;
DECLARE ODSONNE_EDOUARD integer := 2208;
DECLARE CRISTIANO_RONALDO integer := 2158;
DECLARE DAVID_NERES integer := 1671;
DECLARE YANNICK_CARRASCO integer := 262;
DECLARE SEBASTIAN_GIOVINCO integer := 1977;
DECLARE DOMAGOJ_VIDA integer := 985;
DECLARE JURRIEN_TIMBER integer := 1928;
DECLARE DE_PAUL integer := 829;
DECLARE CHIMY_AVILA integer := 1305;
DECLARE JESSE_LINGARD integer := 1304;
DECLARE MARTIN_ZUBIMENDI integer := 1980;
DECLARE ILKAY_GUNDOGAN integer := 1309;
DECLARE RYAN_FRASER integer := 1396;
DECLARE JEAN_KOUASSI integer := 1397;
DECLARE ADAMA_TRAORE integer := 1216;
DECLARE PALAVECINO integer := 1534;
DECLARE TINO_KADEWERE integer := 2207;
DECLARE NACHO_FERNANDEZ_IGLESIAS integer := 55;
DECLARE THOMAS_MULLER integer := 1395;
DECLARE ANTONIO_MARIN integer := 306;
DECLARE MASON_MOUNT integer := 304;
DECLARE EL_SHAARAWY integer := 1181;
DECLARE JESUS_CORONA integer := 1670;
DECLARE JEANCLAIR_TODIBO integer := 1672;
DECLARE SERGIO_CANALES integer := 1180;
DECLARE IVAN_PERISIC integer := 1979;
DECLARE LUIS_MURIEL integer := 1978;
DECLARE WILLEM_GEUBBELS integer := 1535;
DECLARE TANNER_TESSMANN integer := 2181;
DECLARE PIERRE_HOJBJERG integer := 830;
DECLARE NEYMAR integer := 2066;
DECLARE PEDRO_NETO integer := 1184;
DECLARE FRANCISCO_ORTEGA integer := 2104;
DECLARE KONSTANTINOS_MANOLAS integer := 1625;
DECLARE PEDRO_AQUINO integer := 988;
DECLARE TROY_PARROT integer := 832;
DECLARE MARTIN_VITIK integer := 742;
DECLARE HANNIBAL_MEJBRI integer := 741;
DECLARE EDUARDO_QUARESMA integer := 740;
DECLARE PIETRO_PELLEGRI integer := 1673;
DECLARE KAIO_JORGE integer := 2211;
DECLARE KARIM_BENZEMA integer := 1622;
DECLARE OLIVER_PETERSEN integer := 557;
DECLARE LIEL_ABADA integer := 2210;
DECLARE HECTOR_VILLALBA integer := 1349;
DECLARE KELECHI_IHEANACHO integer := 1533;
DECLARE NAZIM_SANGARE integer := 1306;
DECLARE SOULEYMANE_TOURE integer := 294;
DECLARE BRIAN_BROBBEY integer := 2070;
DECLARE TOCHI_CHUKWUANI integer := 233;
DECLARE CHEM_CAMPBELL integer := 239;
DECLARE TAKUMA_OMINAMI integer := 1350;
DECLARE DECLAN_RICE integer := 1183;
DECLARE BEN_WHITE integer := 1182;
DECLARE SERGINO_DEST integer := 1927;
DECLARE KOEN_CASTEELS integer := 1669;

DECLARE TRANSFERENCIA_1 integer;
DECLARE PRESTAMO_1_INICIO integer;
DECLARE PRESTAMO_1_FIN integer;

DECLARE TRANSFERENCIA_2 integer;
DECLARE PRESTAMO_2_INICIO integer;
DECLARE PRESTAMO_2_FIN integer;

DECLARE TRANSFERENCIA_3 integer;
DECLARE PRESTAMO_3_INICIO integer;
DECLARE PRESTAMO_3_FIN integer;

DECLARE TRANSFERENCIA_4 integer;
DECLARE PRESTAMO_4_INICIO integer;
DECLARE PRESTAMO_4_FIN integer;

DECLARE TRANSFERENCIA_5 integer;
DECLARE PRESTAMO_5_INICIO integer;
DECLARE PRESTAMO_5_FIN integer;

DECLARE TRANSFERENCIA_6 integer;
DECLARE PRESTAMO_6_INICIO integer;
DECLARE PRESTAMO_6_FIN integer;

DECLARE TRANSFERENCIA_7 integer;
DECLARE PRESTAMO_7_INICIO integer;
DECLARE PRESTAMO_7_FIN integer;

DECLARE TRANSFERENCIA_8 integer;
DECLARE PRESTAMO_8_INICIO integer;
DECLARE PRESTAMO_8_FIN integer;

DECLARE TRANSFERENCIA_9 integer;
DECLARE PRESTAMO_9_INICIO integer;
DECLARE PRESTAMO_9_FIN integer;

DECLARE TRANSFERENCIA_10 integer;
DECLARE PRESTAMO_10_INICIO integer;
DECLARE PRESTAMO_10_FIN integer;

DECLARE TRANSFERENCIA_11 integer;
DECLARE PRESTAMO_11_INICIO integer;
DECLARE PRESTAMO_11_FIN integer;

DECLARE TRANSFERENCIA_12 integer;
DECLARE PRESTAMO_12_INICIO integer;
DECLARE PRESTAMO_12_FIN integer;

DECLARE TRANSFERENCIA_13 integer;
DECLARE PRESTAMO_13_INICIO integer;
DECLARE PRESTAMO_13_FIN integer;

DECLARE TRANSFERENCIA_14 integer;
DECLARE PRESTAMO_14_INICIO integer;
DECLARE PRESTAMO_14_FIN integer;

DECLARE TRANSFERENCIA_15 integer;
DECLARE PRESTAMO_15_INICIO integer;
DECLARE PRESTAMO_15_FIN integer;

DECLARE TRANSFERENCIA_16 integer;
DECLARE PRESTAMO_16_INICIO integer;
DECLARE PRESTAMO_16_FIN integer;

DECLARE TRANSFERENCIA_17 integer;
DECLARE PRESTAMO_17_INICIO integer;
DECLARE PRESTAMO_17_FIN integer;

DECLARE TRANSFERENCIA_18 integer;
DECLARE PRESTAMO_18_INICIO integer;
DECLARE PRESTAMO_18_FIN integer;

DECLARE TRANSFERENCIA_19 integer;
DECLARE PRESTAMO_19_INICIO integer;
DECLARE PRESTAMO_19_FIN integer;

DECLARE TRANSFERENCIA_20 integer;
DECLARE PRESTAMO_20_INICIO integer;
DECLARE PRESTAMO_20_FIN integer;

DECLARE TRANSFERENCIA_21 integer;
DECLARE PRESTAMO_21_INICIO integer;
DECLARE PRESTAMO_21_FIN integer;

DECLARE TRANSFERENCIA_22 integer;
DECLARE PRESTAMO_22_INICIO integer;
DECLARE PRESTAMO_22_FIN integer;

DECLARE TRANSFERENCIA_23 integer;
DECLARE PRESTAMO_23_INICIO integer;
DECLARE PRESTAMO_23_FIN integer;

DECLARE TRANSFERENCIA_24 integer;
DECLARE PRESTAMO_24_INICIO integer;
DECLARE PRESTAMO_24_FIN integer;

DECLARE TRANSFERENCIA_25 integer;
DECLARE PRESTAMO_25_INICIO integer;
DECLARE PRESTAMO_25_FIN integer;

DECLARE TRANSFERENCIA_26 integer;
DECLARE PRESTAMO_26_INICIO integer;
DECLARE PRESTAMO_26_FIN integer;

DECLARE TRANSFERENCIA_27 integer;
DECLARE PRESTAMO_27_INICIO integer;
DECLARE PRESTAMO_27_FIN integer;

DECLARE TRANSFERENCIA_28 integer;
DECLARE PRESTAMO_28_INICIO integer;
DECLARE PRESTAMO_28_FIN integer;

DECLARE TRANSFERENCIA_29 integer;
DECLARE PRESTAMO_29_INICIO integer;
DECLARE PRESTAMO_29_FIN integer;

DECLARE TRANSFERENCIA_30 integer;
DECLARE PRESTAMO_30_INICIO integer;
DECLARE PRESTAMO_30_FIN integer;

DECLARE TRANSFERENCIA_31 integer;
DECLARE PRESTAMO_31_INICIO integer;
DECLARE PRESTAMO_31_FIN integer;

DECLARE TRANSFERENCIA_32 integer;
DECLARE PRESTAMO_32_INICIO integer;
DECLARE PRESTAMO_32_FIN integer;

DECLARE TRANSFERENCIA_33 integer;
DECLARE PRESTAMO_33_INICIO integer;
DECLARE PRESTAMO_33_FIN integer;

DECLARE TRANSFERENCIA_34 integer;
DECLARE PRESTAMO_34_INICIO integer;
DECLARE PRESTAMO_34_FIN integer;

DECLARE TRANSFERENCIA_35 integer;
DECLARE PRESTAMO_35_INICIO integer;
DECLARE PRESTAMO_35_FIN integer;

DECLARE TRANSFERENCIA_36 integer;
DECLARE PRESTAMO_36_INICIO integer;
DECLARE PRESTAMO_36_FIN integer;

DECLARE TRANSFERENCIA_37 integer;
DECLARE PRESTAMO_37_INICIO integer;
DECLARE PRESTAMO_37_FIN integer;

DECLARE TRANSFERENCIA_38 integer;
DECLARE PRESTAMO_38_INICIO integer;
DECLARE PRESTAMO_38_FIN integer;

DECLARE TRANSFERENCIA_39 integer;
DECLARE PRESTAMO_39_INICIO integer;
DECLARE PRESTAMO_39_FIN integer;

DECLARE TRANSFERENCIA_40 integer;
DECLARE PRESTAMO_40_INICIO integer;
DECLARE PRESTAMO_40_FIN integer;

DECLARE TRANSFERENCIA_41 integer;
DECLARE PRESTAMO_41_INICIO integer;
DECLARE PRESTAMO_41_FIN integer;

DECLARE TRANSFERENCIA_42 integer;
DECLARE PRESTAMO_42_INICIO integer;
DECLARE PRESTAMO_42_FIN integer;

DECLARE TRANSFERENCIA_43 integer;
DECLARE PRESTAMO_43_INICIO integer;
DECLARE PRESTAMO_43_FIN integer;

DECLARE TRANSFERENCIA_44 integer;
DECLARE PRESTAMO_44_INICIO integer;
DECLARE PRESTAMO_44_FIN integer;

DECLARE TRANSFERENCIA_45 integer;
DECLARE PRESTAMO_45_INICIO integer;
DECLARE PRESTAMO_45_FIN integer;

DECLARE TRANSFERENCIA_46 integer;
DECLARE PRESTAMO_46_INICIO integer;
DECLARE PRESTAMO_46_FIN integer;

DECLARE TRANSFERENCIA_47 integer;
DECLARE PRESTAMO_47_INICIO integer;
DECLARE PRESTAMO_47_FIN integer;

DECLARE TRANSFERENCIA_48 integer;
DECLARE PRESTAMO_48_INICIO integer;
DECLARE PRESTAMO_48_FIN integer;

DECLARE TRANSFERENCIA_49 integer;
DECLARE PRESTAMO_49_INICIO integer;
DECLARE PRESTAMO_49_FIN integer;

DECLARE TRANSFERENCIA_50 integer;
DECLARE PRESTAMO_50_INICIO integer;
DECLARE PRESTAMO_50_FIN integer;

DECLARE TRANSFERENCIA_51 integer;
DECLARE PRESTAMO_51_INICIO integer;
DECLARE PRESTAMO_51_FIN integer;

DECLARE TRANSFERENCIA_52 integer;
DECLARE PRESTAMO_52_INICIO integer;
DECLARE PRESTAMO_52_FIN integer;

DECLARE TRANSFERENCIA_53 integer;
DECLARE PRESTAMO_53_INICIO integer;
DECLARE PRESTAMO_53_FIN integer;

DECLARE TRANSFERENCIA_54 integer;
DECLARE PRESTAMO_54_INICIO integer;
DECLARE PRESTAMO_54_FIN integer;

DECLARE TRANSFERENCIA_55 integer;
DECLARE PRESTAMO_55_INICIO integer;
DECLARE PRESTAMO_55_FIN integer;

DECLARE TRANSFERENCIA_56 integer;
DECLARE PRESTAMO_56_INICIO integer;
DECLARE PRESTAMO_56_FIN integer;

DECLARE TRANSFERENCIA_57 integer;
DECLARE PRESTAMO_57_INICIO integer;
DECLARE PRESTAMO_57_FIN integer;

DECLARE TRANSFERENCIA_58 integer;
DECLARE PRESTAMO_58_INICIO integer;
DECLARE PRESTAMO_58_FIN integer;

DECLARE TRANSFERENCIA_59 integer;
DECLARE PRESTAMO_59_INICIO integer;
DECLARE PRESTAMO_59_FIN integer;

DECLARE TRANSFERENCIA_60 integer;
DECLARE PRESTAMO_60_INICIO integer;
DECLARE PRESTAMO_60_FIN integer;

DECLARE TRANSFERENCIA_61 integer;
DECLARE PRESTAMO_61_INICIO integer;
DECLARE PRESTAMO_61_FIN integer;

DECLARE TRANSFERENCIA_62 integer;
DECLARE PRESTAMO_62_INICIO integer;
DECLARE PRESTAMO_62_FIN integer;

DECLARE TRANSFERENCIA_63 integer;
DECLARE PRESTAMO_63_INICIO integer;
DECLARE PRESTAMO_63_FIN integer;

DECLARE TRANSFERENCIA_64 integer;
DECLARE PRESTAMO_64_INICIO integer;
DECLARE PRESTAMO_64_FIN integer;

DECLARE TRANSFERENCIA_65 integer;
DECLARE PRESTAMO_65_INICIO integer;
DECLARE PRESTAMO_65_FIN integer;

DECLARE TRANSFERENCIA_66 integer;
DECLARE PRESTAMO_66_INICIO integer;
DECLARE PRESTAMO_66_FIN integer;

DECLARE TRANSFERENCIA_67 integer;
DECLARE PRESTAMO_67_INICIO integer;
DECLARE PRESTAMO_67_FIN integer;

DECLARE TRANSFERENCIA_68 integer;
DECLARE PRESTAMO_68_INICIO integer;
DECLARE PRESTAMO_68_FIN integer;

DECLARE TRANSFERENCIA_69 integer;
DECLARE PRESTAMO_69_INICIO integer;
DECLARE PRESTAMO_69_FIN integer;

DECLARE TRANSFERENCIA_70 integer;
DECLARE PRESTAMO_70_INICIO integer;
DECLARE PRESTAMO_70_FIN integer;

DECLARE TRANSFERENCIA_71 integer;
DECLARE PRESTAMO_71_INICIO integer;
DECLARE PRESTAMO_71_FIN integer;

DECLARE TRANSFERENCIA_72 integer;
DECLARE PRESTAMO_72_INICIO integer;
DECLARE PRESTAMO_72_FIN integer;

DECLARE TRANSFERENCIA_73 integer;
DECLARE PRESTAMO_73_INICIO integer;
DECLARE PRESTAMO_73_FIN integer;

DECLARE TRANSFERENCIA_74 integer;
DECLARE PRESTAMO_74_INICIO integer;
DECLARE PRESTAMO_74_FIN integer;

DECLARE TRANSFERENCIA_75 integer;
DECLARE PRESTAMO_75_INICIO integer;
DECLARE PRESTAMO_75_FIN integer;

BEGIN

    -- Prestamo de Duvan Zapata
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_YID_ARMY, USUARIO_LOS_VASCOS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_1;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DUVAN_ZAPATA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_YID_ARMY, EQUIPO_LOS_VASCOS, TRANSFERENCIA_1, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_1_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DUVAN_ZAPATA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_VASCOS, EQUIPO_YID_ARMY, TRANSFERENCIA_1, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_1_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_1_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_1_FIN, true);


    -- Prestamo de Timo Werner
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_PROVINCIANOS, USUARIO_LOS_VENGADORES, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_2;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TIMO_WERNER, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_PROVINCIANOS, EQUIPO_LOS_VENGADORES, TRANSFERENCIA_2, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_2_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TIMO_WERNER, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_VENGADORES, EQUIPO_PROVINCIANOS, TRANSFERENCIA_2, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_2_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_2_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_2_FIN, true);


    -- Prestamo de Eduardo Salvio
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_PROVINCIANOS, USUARIO_DEFE_DI_CAPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_3;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(EDUARDO_SALVIO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_PROVINCIANOS, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_3, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_3_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(EDUARDO_SALVIO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_PROVINCIANOS, TRANSFERENCIA_3, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_3_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_3_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_3_FIN, true);


    -- Prestamo de Lisandro Martinez
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_VARELA, USUARIO_LOS_VENGADORES, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_4;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(LISANDRO_MARTINEZ, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_VARELA, EQUIPO_LOS_VENGADORES, TRANSFERENCIA_4, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_4_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(LISANDRO_MARTINEZ, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_VENGADORES, EQUIPO_VARELA, TRANSFERENCIA_4, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_4_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_4_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_4_FIN, true);


    -- Prestamo de Wilfried Zaha
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_IRRESPETUOSOS, USUARIO_DEFE_DI_CAPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_5;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(WILFRIED_ZAHA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_IRRESPETUOSOS, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_5, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_5_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(WILFRIED_ZAHA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_LOS_IRRESPETUOSOS, TRANSFERENCIA_5, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_5_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_5_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_5_FIN, true);


    -- Prestamo de Ngolo Kante
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_TREVELIN, USUARIO_POLLERA, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_6;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(NGOLO_KANTE, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_TREVELIN, EQUIPO_POLLERA, TRANSFERENCIA_6, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_6_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(NGOLO_KANTE, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_POLLERA, EQUIPO_TREVELIN, TRANSFERENCIA_6, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_6_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_6_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_6_FIN, true);


    -- Prestamo de Alexis Sanchez
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_MATOS_ROCK, USUARIO_YO_DUERMO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_7;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ALEXIS_SANCHEZ, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_MATOS_ROCK, EQUIPO_YO_DUERMO, TRANSFERENCIA_7, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_7_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ALEXIS_SANCHEZ, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_YO_DUERMO, EQUIPO_MATOS_ROCK, TRANSFERENCIA_7, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_7_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_7_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_7_FIN, true);


    -- Prestamo de INAKI_WILLIAMS
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_DAVE_HOLLAND, USUARIO_TIKI_TIKI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_8;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(INAKI_WILLIAMS, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_DAVE_HOLLAND, EQUIPO_TIKI_TIKI, TRANSFERENCIA_8, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_8_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(INAKI_WILLIAMS, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_TIKI_TIKI, EQUIPO_DAVE_HOLLAND, TRANSFERENCIA_8, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_8_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_8_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_8_FIN, true);


    -- Prestamo de MATEJ_VYDRA
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_VENGADORES, USUARIO_ST_PAULI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_9;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(MATEJ_VYDRA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_VENGADORES, EQUIPO_ST_PAULI, TRANSFERENCIA_9, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_9_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(MATEJ_VYDRA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_ST_PAULI, EQUIPO_LOS_VENGADORES, TRANSFERENCIA_9, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_9_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_9_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_9_FIN, true);


    -- Prestamo de HARVEY_BARNES
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_YID_ARMY, USUARIO_TERMIDOR, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_10;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(HARVEY_BARNES, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_YID_ARMY, EQUIPO_TERMIDOR, TRANSFERENCIA_10, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_10_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(HARVEY_BARNES, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_TERMIDOR, EQUIPO_YID_ARMY, TRANSFERENCIA_10, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_10_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_10_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_10_FIN, true);


    -- Prestamo de TAKUMI_MINAMINO
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_MATOS_ROCK, USUARIO_BARRILETE_COSMICO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_11;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TAKUMI_MINAMINO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_MATOS_ROCK, EQUIPO_BARRILETE_COSMICO, TRANSFERENCIA_11, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_11_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TAKUMI_MINAMINO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_BARRILETE_COSMICO, EQUIPO_MATOS_ROCK, TRANSFERENCIA_11, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_11_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_11_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_11_FIN, true);


    -- Prestamo de ERLING_HAALAND
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_DAVE_HOLLAND, USUARIO_IL_RITORNI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_12;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ERLING_HAALAND, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_DAVE_HOLLAND, EQUIPO_IL_RITORNI, TRANSFERENCIA_12, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_12_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ERLING_HAALAND, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_IL_RITORNI, EQUIPO_DAVE_HOLLAND, TRANSFERENCIA_12, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_12_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_12_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_12_FIN, true);


    -- Prestamo de FRANCESCO_ACERBI
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_TREVELIN, USUARIO_NIUPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_13;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(FRANCESCO_ACERBI, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_TREVELIN, EQUIPO_NIUPI, TRANSFERENCIA_13, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_13_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(FRANCESCO_ACERBI, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_NIUPI, EQUIPO_TREVELIN, TRANSFERENCIA_13, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_13_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_13_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_13_FIN, true);


    -- Prestamo de ROMELU_LUKAKU
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_ESQUIMBO, USUARIO_CHAMPAGNE, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_14;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ROMELU_LUKAKU, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_ESQUIMBO, EQUIPO_CHAMPAGNE, TRANSFERENCIA_14, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_14_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ROMELU_LUKAKU, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_CHAMPAGNE, EQUIPO_ESQUIMBO, TRANSFERENCIA_14, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_14_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_14_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_14_FIN, true);


    -- Prestamo de ENZO_PEREZ
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_ESQUIMBO, USUARIO_LA_CAPRICHOSA, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_15;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ENZO_PEREZ, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_ESQUIMBO, EQUIPO_LA_CAPRICHOSA, TRANSFERENCIA_15, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_15_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ENZO_PEREZ, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LA_CAPRICHOSA, EQUIPO_ESQUIMBO, TRANSFERENCIA_15, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_15_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_15_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_15_FIN, true);


    -- Prestamo de ALEX_SANDRO
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_VENGADORES, USUARIO_POLLERA, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_16;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ALEX_SANDRO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_VENGADORES, EQUIPO_POLLERA, TRANSFERENCIA_16, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_16_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ALEX_SANDRO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_POLLERA, EQUIPO_LOS_VENGADORES, TRANSFERENCIA_16, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_16_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_16_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_16_FIN, true);


    -- Prestamo de DIOGO_JOTA
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_CEBOLLITAS, USUARIO_POLLERA, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_17;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DIOGO_JOTA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_CEBOLLITAS, EQUIPO_POLLERA, TRANSFERENCIA_17, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_17_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DIOGO_JOTA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_POLLERA, EQUIPO_CEBOLLITAS, TRANSFERENCIA_17, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_17_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_17_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_17_FIN, true);


    -- Prestamo de AARON_LONG
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_ST_PAULI, USUARIO_LOS_VASCOS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_18;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(AARON_LONG, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_ST_PAULI, EQUIPO_LOS_VASCOS, TRANSFERENCIA_18, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_18_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(AARON_LONG, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_VASCOS, EQUIPO_ST_PAULI, TRANSFERENCIA_18, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_18_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_18_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_18_FIN, true);


    -- Prestamo de SZOBOSZLAI
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_TETRABRICK, USUARIO_DEFE_DI_CAPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_19;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SZOBOSZLAI, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_TETRABRICK, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_19, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_19_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SZOBOSZLAI, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_TETRABRICK, TRANSFERENCIA_19, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_19_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_19_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_19_FIN, true);


    -- Prestamo de ODSONNE_EDOUARD
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_TETRABRICK, USUARIO_DEFE_DI_CAPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_20;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ODSONNE_EDOUARD, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_TETRABRICK, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_20, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_20_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ODSONNE_EDOUARD, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_TETRABRICK, TRANSFERENCIA_20, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_20_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_20_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_20_FIN, true);


    -- Prestamo de CRISTIANO_RONALDO
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_UYNOSE, USUARIO_CHAMPAGNE, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_21;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(CRISTIANO_RONALDO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_UYNOSE, EQUIPO_CHAMPAGNE, TRANSFERENCIA_21, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_21_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(CRISTIANO_RONALDO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_CHAMPAGNE, EQUIPO_UYNOSE, TRANSFERENCIA_21, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_21_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_21_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_21_FIN, true);


    -- Prestamo de DAVID NERES
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_PURO_FIDEO, USUARIO_YO_DUERMO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_22;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DAVID_NERES, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_PURO_FIDEO, EQUIPO_YO_DUERMO, TRANSFERENCIA_22, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_22_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DAVID_NERES, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_YO_DUERMO, EQUIPO_A_PURO_FIDEO, TRANSFERENCIA_22, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_22_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_22_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_22_FIN, true);


    -- Prestamo de Yannick Carrasco
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_ESTUPIDO_FLANDERS, USUARIO_BARRILETE_COSMICO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_23;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(YANNICK_CARRASCO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_ESTUPIDO_FLANDERS, EQUIPO_BARRILETE_COSMICO, TRANSFERENCIA_23, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_23_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(YANNICK_CARRASCO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_BARRILETE_COSMICO, EQUIPO_ESTUPIDO_FLANDERS, TRANSFERENCIA_23, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_23_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_23_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_23_FIN, true);


    -- Prestamo de Giovinco
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_CHAMPAGNE, USUARIO_EL_TACO_NO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_24;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SEBASTIAN_GIOVINCO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_CHAMPAGNE, EQUIPO_EL_TACO_NO, TRANSFERENCIA_24, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_24_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SEBASTIAN_GIOVINCO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_EL_TACO_NO, EQUIPO_CHAMPAGNE, TRANSFERENCIA_24, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_24_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_24_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_24_FIN, true);


    -- Prestamo de D.Vida
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_ST_PAULI, USUARIO_MATOS_ROCK, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_25;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DOMAGOJ_VIDA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_ST_PAULI, EQUIPO_MATOS_ROCK, TRANSFERENCIA_25, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_25_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DOMAGOJ_VIDA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_MATOS_ROCK, EQUIPO_ST_PAULI, TRANSFERENCIA_25, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_25_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_25_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_25_FIN, true);


    -- Prestamo de Timber
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_ESQUIMBO, USUARIO_DAVE_HOLLAND, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_26;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JURRIEN_TIMBER, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_ESQUIMBO, EQUIPO_DAVE_HOLLAND, TRANSFERENCIA_26, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_26_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JURRIEN_TIMBER, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DAVE_HOLLAND, EQUIPO_ESQUIMBO, TRANSFERENCIA_26, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_26_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_26_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_26_FIN, true);


    -- Prestamo de De Paul
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_YID_ARMY, USUARIO_ESTUPIDO_FLANDERS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_27;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DE_PAUL, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_YID_ARMY, EQUIPO_ESTUPIDO_FLANDERS, TRANSFERENCIA_27, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_27_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DE_PAUL, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_ESTUPIDO_FLANDERS, EQUIPO_YID_ARMY, TRANSFERENCIA_27, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_27_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_27_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_27_FIN, true);


    -- Prestamo de Avila
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_CHELITOS, USUARIO_POLLERA, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_28;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(CHIMY_AVILA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_CHELITOS, EQUIPO_POLLERA, TRANSFERENCIA_28, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_28_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(CHIMY_AVILA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_POLLERA, EQUIPO_LOS_CHELITOS, TRANSFERENCIA_28, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_28_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_28_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_28_FIN, true);


    -- Prestamo de Lingard
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_CHELITOS, USUARIO_POLLERA, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_29;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JESSE_LINGARD, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_CHELITOS, EQUIPO_POLLERA, TRANSFERENCIA_29, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_29_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JESSE_LINGARD, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_POLLERA, EQUIPO_LOS_CHELITOS, TRANSFERENCIA_29, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_29_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_29_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_29_FIN, true);


    -- Prestamo de Zubimendi
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_CHAMPAGNE, USUARIO_LOS_VASCOS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_30;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(MARTIN_ZUBIMENDI, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_CHAMPAGNE, EQUIPO_LOS_VASCOS, TRANSFERENCIA_30, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_30_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(MARTIN_ZUBIMENDI, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_VASCOS, EQUIPO_CHAMPAGNE, TRANSFERENCIA_30, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_30_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_30_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_30_FIN, true);


    -- Prestamo de Gundogan
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_VARELA, USUARIO_LOS_VENGADORES, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_31;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ILKAY_GUNDOGAN, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_VARELA, EQUIPO_LOS_VENGADORES, TRANSFERENCIA_31, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_31_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ILKAY_GUNDOGAN, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_VENGADORES, EQUIPO_VARELA, TRANSFERENCIA_31, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_31_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_31_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_31_FIN, true);


    -- Prestamo de Fraser
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_POLLERA, USUARIO_LOS_CHELITOS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_32;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(RYAN_FRASER, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_POLLERA, EQUIPO_LOS_CHELITOS, TRANSFERENCIA_32, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_32_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(RYAN_FRASER, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_CHELITOS, EQUIPO_POLLERA, TRANSFERENCIA_32, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_32_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_32_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_32_FIN, true);


    -- Prestamo de Kouassi
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_POLLERA, USUARIO_LOS_CHELITOS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_33;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JEAN_KOUASSI, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_POLLERA, EQUIPO_LOS_CHELITOS, TRANSFERENCIA_33, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_33_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JEAN_KOUASSI, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_CHELITOS, EQUIPO_POLLERA, TRANSFERENCIA_33, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_33_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_33_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_33_FIN, true);


    -- Prestamo de Adama Traore
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_ESQUIMBO, USUARIO_ESTUPIDO_FLANDERS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_34;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ADAMA_TRAORE, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_ESQUIMBO, EQUIPO_ESTUPIDO_FLANDERS, TRANSFERENCIA_34, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_34_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ADAMA_TRAORE, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_ESTUPIDO_FLANDERS, EQUIPO_ESQUIMBO, TRANSFERENCIA_34, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_34_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_34_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_34_FIN, true);


    -- Prestamo de Palavecino
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_EL_TACO_NO, USUARIO_SUPER420, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_35;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PALAVECINO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_EL_TACO_NO, EQUIPO_SUPER420, TRANSFERENCIA_35, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_35_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PALAVECINO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_SUPER420, EQUIPO_EL_TACO_NO, TRANSFERENCIA_35, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_35_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_35_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_35_FIN, true);


    -- Prestamo de Kadewere
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_TETRABRICK, USUARIO_EL_BICHO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_36;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TINO_KADEWERE, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_TETRABRICK, EQUIPO_EL_BICHO, TRANSFERENCIA_36, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_36_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PALAVECINO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_EL_BICHO, EQUIPO_TETRABRICK, TRANSFERENCIA_36, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_36_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_36_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_36_FIN, true);


    -- Prestamo de Nacho Fernandez Iglesias
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_PROVINCIANOS, USUARIO_NIUPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_37;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(NACHO_FERNANDEZ_IGLESIAS, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_PROVINCIANOS, EQUIPO_NIUPI, TRANSFERENCIA_37, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_37_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(NACHO_FERNANDEZ_IGLESIAS, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_NIUPI, EQUIPO_PROVINCIANOS, TRANSFERENCIA_37, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_37_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_37_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_37_FIN, true);


    -- Prestamo de Muller
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_POLLERA, USUARIO_LOS_VENGADORES, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_38;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(THOMAS_MULLER, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_POLLERA, EQUIPO_LOS_VENGADORES, TRANSFERENCIA_38, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_38_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(THOMAS_MULLER, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_VENGADORES, EQUIPO_POLLERA, TRANSFERENCIA_38, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_38_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_38_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_38_FIN, true);


    -- Prestamo de Marin
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_VARELA, USUARIO_LOS_IRRESPETUOSOS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_39;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ANTONIO_MARIN, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_VARELA, EQUIPO_LOS_IRRESPETUOSOS, TRANSFERENCIA_39, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_39_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(ANTONIO_MARIN, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_IRRESPETUOSOS, EQUIPO_VARELA, TRANSFERENCIA_39, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_39_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_39_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_39_FIN, true);


    -- Prestamo de Mount
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_VARELA, USUARIO_YO_DUERMO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_40;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(MASON_MOUNT, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_VARELA, EQUIPO_YO_DUERMO, TRANSFERENCIA_40, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_40_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(MASON_MOUNT, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_YO_DUERMO, EQUIPO_VARELA, TRANSFERENCIA_40, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_40_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_40_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_40_FIN, true);


    -- Prestamo de El Shaarawy
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_LA_TIBIA, USUARIO_A_PURO_FIDEO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_41;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(EL_SHAARAWY, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_LA_TIBIA, EQUIPO_A_PURO_FIDEO, TRANSFERENCIA_41, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_41_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(EL_SHAARAWY, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_A_PURO_FIDEO, EQUIPO_A_LA_TIBIA, TRANSFERENCIA_41, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_41_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_41_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_41_FIN, true);


    -- Prestamo de Jesús Corona
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_PURO_FIDEO, USUARIO_DEFE_DI_CAPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_42;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JESUS_CORONA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_PURO_FIDEO, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_42, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_42_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JESUS_CORONA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_A_PURO_FIDEO, TRANSFERENCIA_42, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_42_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_42_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_42_FIN, true);


    -- Prestamo de Todibo
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_PURO_FIDEO, USUARIO_EL_TACO_NO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_43;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JEANCLAIR_TODIBO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_PURO_FIDEO, EQUIPO_EL_TACO_NO, TRANSFERENCIA_43, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_43_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(JEANCLAIR_TODIBO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_EL_TACO_NO, EQUIPO_A_PURO_FIDEO, TRANSFERENCIA_43, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_43_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_43_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_43_FIN, true);


    -- Prestamo de Canales
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_LA_TIBIA, USUARIO_DEFE_DI_CAPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_44;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SERGIO_CANALES, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_LA_TIBIA, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_44, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_44_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SERGIO_CANALES, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_A_LA_TIBIA, TRANSFERENCIA_44, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_44_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_44_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_44_FIN, true);


    -- Prestamo de Perisic
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_CHAMPAGNE, USUARIO_LOS_CHELITOS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_45;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(IVAN_PERISIC, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_CHAMPAGNE, EQUIPO_LOS_CHELITOS, TRANSFERENCIA_45, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_45_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(IVAN_PERISIC, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_CHELITOS, EQUIPO_CHAMPAGNE, TRANSFERENCIA_45, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_45_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_45_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_45_FIN, true);


    -- Prestamo de Muriel
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_CHAMPAGNE, USUARIO_CEBOLLITAS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_46;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(LUIS_MURIEL, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_CHAMPAGNE, EQUIPO_CEBOLLITAS, TRANSFERENCIA_46, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_46_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(LUIS_MURIEL, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_CEBOLLITAS, EQUIPO_CHAMPAGNE, TRANSFERENCIA_46, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_46_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_46_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_46_FIN, true);


    -- Prestamo de Geubbels
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_EL_TACO_NO, USUARIO_NO_ROOM, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_47;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(WILLEM_GEUBBELS, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_EL_TACO_NO, EQUIPO_NO_ROOM, TRANSFERENCIA_47, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_47_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(WILLEM_GEUBBELS, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_NO_ROOM, EQUIPO_EL_TACO_NO, TRANSFERENCIA_47, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_47_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_47_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_47_FIN, true);


    -- Prestamo de Tessmann
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_NO_ROOM, USUARIO_EL_TACO_NO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_48;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TANNER_TESSMANN, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_NO_ROOM, EQUIPO_EL_TACO_NO, TRANSFERENCIA_48, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_48_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TANNER_TESSMANN, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_EL_TACO_NO, EQUIPO_NO_ROOM, TRANSFERENCIA_48, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_48_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_48_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_48_FIN, true);


    -- Prestamo de Hojbjerg
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_YID_ARMY, USUARIO_EL_BICHO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_49;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PIERRE_HOJBJERG, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_YID_ARMY, EQUIPO_EL_BICHO, TRANSFERENCIA_49, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_49_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PIERRE_HOJBJERG, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_EL_BICHO, EQUIPO_YID_ARMY, TRANSFERENCIA_49, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_49_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_49_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_49_FIN, true);


    -- Prestamo de Neymar
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_MATOS_ROCK, USUARIO_NO_ROOM, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_50;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(NEYMAR, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_MATOS_ROCK, EQUIPO_NO_ROOM, TRANSFERENCIA_50, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_50_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(NEYMAR, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_NO_ROOM, EQUIPO_MATOS_ROCK, TRANSFERENCIA_50, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_50_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_50_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_50_FIN, true);


    -- Prestamo de Pedro Neto
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_LA_TIBIA, USUARIO_LOS_CHELITOS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_51;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PEDRO_NETO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_LA_TIBIA, EQUIPO_LOS_CHELITOS, TRANSFERENCIA_51, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_51_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PEDRO_NETO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_CHELITOS, EQUIPO_A_LA_TIBIA, TRANSFERENCIA_51, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_51_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_51_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_51_FIN, true);


    -- Prestamo de Francisco Ortega
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_DEFE_DI_CAPI, USUARIO_CHAMPAGNE, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_52;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(FRANCISCO_ORTEGA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_CHAMPAGNE, TRANSFERENCIA_52, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_52_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(FRANCISCO_ORTEGA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_CHAMPAGNE, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_52, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_52_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_52_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_52_FIN, true);


    -- Prestamo de Manolas
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_TREVELIN, USUARIO_EL_BICHO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_53;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KONSTANTINOS_MANOLAS, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_TREVELIN, EQUIPO_EL_BICHO, TRANSFERENCIA_53, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_53_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KONSTANTINOS_MANOLAS, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_EL_BICHO, EQUIPO_TREVELIN, TRANSFERENCIA_53, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_53_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_53_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_53_FIN, true);


    -- Prestamo de Aquino
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_ST_PAULI, USUARIO_BRANCA_UNITED, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_54;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PEDRO_AQUINO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_ST_PAULI, EQUIPO_BRANCA_UNITED, TRANSFERENCIA_54, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_54_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PEDRO_AQUINO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_BRANCA_UNITED, EQUIPO_ST_PAULI, TRANSFERENCIA_54, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_54_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_54_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_54_FIN, true);


    -- Prestamo de Parrott
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_YID_ARMY, USUARIO_TERMIDOR, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_55;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TROY_PARROT, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_YID_ARMY, EQUIPO_TERMIDOR, TRANSFERENCIA_55, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_55_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TROY_PARROT, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_TERMIDOR, EQUIPO_YID_ARMY, TRANSFERENCIA_55, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_55_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_55_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_55_FIN, true);


    -- Prestamo de Vitik
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_DAVE_HOLLAND, USUARIO_TERMIDOR, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_56;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(MARTIN_VITIK, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_DAVE_HOLLAND, EQUIPO_TERMIDOR, TRANSFERENCIA_56, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_56_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(MARTIN_VITIK, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_TERMIDOR, EQUIPO_DAVE_HOLLAND, TRANSFERENCIA_56, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_56_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_56_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_56_FIN, true);


    -- Prestamo de Hannibal Mejbri
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_DAVE_HOLLAND, USUARIO_TERMIDOR, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_57;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(HANNIBAL_MEJBRI, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_DAVE_HOLLAND, EQUIPO_TERMIDOR, TRANSFERENCIA_57, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_57_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(HANNIBAL_MEJBRI, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_TERMIDOR, EQUIPO_DAVE_HOLLAND, TRANSFERENCIA_57, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_57_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_57_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_57_FIN, true);


    -- Prestamo de Eduardo Quaresma
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_DAVE_HOLLAND, USUARIO_TERMIDOR, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_58;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(EDUARDO_QUARESMA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_DAVE_HOLLAND, EQUIPO_TERMIDOR, TRANSFERENCIA_58, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_58_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(EDUARDO_QUARESMA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_TERMIDOR, EQUIPO_DAVE_HOLLAND, TRANSFERENCIA_58, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_58_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_58_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_58_FIN, true);


    -- Prestamo de Pietro Pellegri
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_PURO_FIDEO, USUARIO_TERMIDOR, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_59;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PIETRO_PELLEGRI, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_PURO_FIDEO, EQUIPO_TERMIDOR, TRANSFERENCIA_59, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_59_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(PIETRO_PELLEGRI, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_TERMIDOR, EQUIPO_A_PURO_FIDEO, TRANSFERENCIA_59, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_59_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_59_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_59_FIN, true);


    -- Prestamo de Kaio Jorge
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_TETRABRICK, USUARIO_TERCERA_POSICION, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_60;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KAIO_JORGE, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_TETRABRICK, EQUIPO_TERCERA_POSICION, TRANSFERENCIA_60, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_60_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KAIO_JORGE, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_TERCERA_POSICION, EQUIPO_TETRABRICK, TRANSFERENCIA_60, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_60_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_60_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_60_FIN, true);


    -- Prestamo de Benzema
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_TREVELIN, USUARIO_DEFE_DI_CAPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_61;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KARIM_BENZEMA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_TREVELIN, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_61, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_61_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KARIM_BENZEMA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_TREVELIN, TRANSFERENCIA_61, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_61_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_61_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_61_FIN, true);


    -- Prestamo de Petersen
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_VASCOS, USUARIO_ST_PAULI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_62;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(OLIVER_PETERSEN, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_VASCOS, EQUIPO_ST_PAULI, TRANSFERENCIA_62, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_62_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(OLIVER_PETERSEN, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_ST_PAULI, EQUIPO_LOS_VASCOS, TRANSFERENCIA_62, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_62_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_62_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_62_FIN, true);


    -- Prestamo de Abada
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_TETRABRICK, USUARIO_EL_BICHO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_63;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(LIEL_ABADA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_TETRABRICK, EQUIPO_EL_BICHO, TRANSFERENCIA_63, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_63_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(LIEL_ABADA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_EL_BICHO, EQUIPO_TETRABRICK, TRANSFERENCIA_63, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_63_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_63_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_63_FIN, true);


    -- Prestamo de Villalba
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_VENGADORES, USUARIO_YO_DUERMO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_64;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(HECTOR_VILLALBA, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_VENGADORES, EQUIPO_YO_DUERMO, TRANSFERENCIA_64, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_64_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(HECTOR_VILLALBA, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_YO_DUERMO, EQUIPO_LOS_VENGADORES, TRANSFERENCIA_64, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_64_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_64_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_64_FIN, true);


    -- Prestamo de Iheanacho
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_EL_TACO_NO, USUARIO_LOS_CHELITOS, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_65;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KELECHI_IHEANACHO, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_EL_TACO_NO, EQUIPO_LOS_CHELITOS, TRANSFERENCIA_65, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_65_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KELECHI_IHEANACHO, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LOS_CHELITOS, EQUIPO_EL_TACO_NO, TRANSFERENCIA_65, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_65_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_65_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_65_FIN, true);


    -- Prestamo de Sangare
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_CHELITOS, USUARIO_EL_TACO_NO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_66;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(NAZIM_SANGARE, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_CHELITOS, EQUIPO_EL_TACO_NO, TRANSFERENCIA_66, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_66_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(NAZIM_SANGARE, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_EL_TACO_NO, EQUIPO_LOS_CHELITOS, TRANSFERENCIA_66, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_66_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_66_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_66_FIN, true);


    -- Prestamo de Souleymane Toure
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_VARELA, USUARIO_EL_BICHO, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_67;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SOULEYMANE_TOURE, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_VARELA, EQUIPO_EL_BICHO, TRANSFERENCIA_67, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_67_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SOULEYMANE_TOURE, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_EL_BICHO, EQUIPO_VARELA, TRANSFERENCIA_67, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_67_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_67_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_67_FIN, true);


    -- Prestamo de Brobbey
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_MATOS_ROCK, USUARIO_LASTIMA_A_NADIE, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_68;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(BRIAN_BROBBEY, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_MATOS_ROCK, EQUIPO_LASTIMA_A_NADIE, TRANSFERENCIA_68, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_68_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(BRIAN_BROBBEY, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_LASTIMA_A_NADIE, EQUIPO_MATOS_ROCK, TRANSFERENCIA_68, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_68_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_68_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_68_FIN, true);


    -- Prestamo de Chukwuani
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_IRRESPETUOSOS, USUARIO_ST_PAULI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_69;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TOCHI_CHUKWUANI, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_IRRESPETUOSOS, EQUIPO_ST_PAULI, TRANSFERENCIA_69, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_69_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TOCHI_CHUKWUANI, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_ST_PAULI, EQUIPO_LOS_IRRESPETUOSOS, TRANSFERENCIA_69, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_69_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_69_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_69_FIN, true);


    -- Prestamo de Campbell
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_IRRESPETUOSOS, USUARIO_ST_PAULI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_70;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(CHEM_CAMPBELL, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_IRRESPETUOSOS, EQUIPO_ST_PAULI, TRANSFERENCIA_70, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_70_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(CHEM_CAMPBELL, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_ST_PAULI, EQUIPO_LOS_IRRESPETUOSOS, TRANSFERENCIA_70, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_70_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_70_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_70_FIN, true);


    -- Prestamo de Ominami
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LOS_VENGADORES, USUARIO_IL_RITORNI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_71;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TAKUMA_OMINAMI, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LOS_VENGADORES, EQUIPO_IL_RITORNI, TRANSFERENCIA_71, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_71_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(TAKUMA_OMINAMI, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_IL_RITORNI, EQUIPO_LOS_VENGADORES, TRANSFERENCIA_71, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_71_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_71_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_71_FIN, true);


    -- Prestamo de Rice
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_LA_TIBIA, USUARIO_IL_RITORNI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_72;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DECLAN_RICE, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_LA_TIBIA, EQUIPO_IL_RITORNI, TRANSFERENCIA_72, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_72_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(DECLAN_RICE, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_IL_RITORNI, EQUIPO_A_LA_TIBIA, TRANSFERENCIA_72, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_72_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_72_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_72_FIN, true);


    -- Prestamo de Ben White
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_LA_TIBIA, USUARIO_IL_RITORNI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_73;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(BEN_WHITE, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_LA_TIBIA, EQUIPO_IL_RITORNI, TRANSFERENCIA_73, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_73_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(BEN_WHITE, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_IL_RITORNI, EQUIPO_A_LA_TIBIA, TRANSFERENCIA_73, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_73_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_73_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_73_FIN, true);


    -- Prestamo de Sergino Dest
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_LASTIMA_A_NADIE, USUARIO_DEFE_DI_CAPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_74;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SERGINO_DEST, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_LASTIMA_A_NADIE, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_74, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_74_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(SERGINO_DEST, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_LASTIMA_A_NADIE, TRANSFERENCIA_74, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_74_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_74_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_74_FIN, true);


    -- Prestamo de Casteels
    INSERT INTO transferencia
    (usuario_origen_id, usuario_destino_id, estado_transferencia_id)
    VALUES(USUARIO_A_PURO_FIDEO, USUARIO_DEFE_DI_CAPI, ESTADO_TRANSFERENCIA_CONFIRMADA) RETURNING id INTO TRANSFERENCIA_75;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KOEN_CASTEELS, TEMP_11, PERIODO_PRINCIPIO, EQUIPO_A_PURO_FIDEO, EQUIPO_DEFE_DI_CAPI, TRANSFERENCIA_75, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_75_INICIO;

    INSERT INTO movimiento_jugador
    (jugador_id, numero_temporada, periodo_id, equipo_origen_id, equipo_destino_id, transferencia_id, tipo_movimiento_id)
    VALUES(KOEN_CASTEELS, TEMP_12, PERIODO_PRINCIPIO, EQUIPO_DEFE_DI_CAPI, EQUIPO_A_PURO_FIDEO, TRANSFERENCIA_75, TIPO_MOVIMIENTO_PRESTAMO) RETURNING id INTO PRESTAMO_75_FIN;

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_75_INICIO, false);

    INSERT INTO prestamo_jugador
    (movimiento_jugador_id, es_fin_de_prestamo)
    VALUES(PRESTAMO_75_FIN, true);

END $$;