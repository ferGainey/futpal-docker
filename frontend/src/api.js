import axios from 'axios';

const instanciaAxios = axios.create({
  baseURL: window.location.protocol + '//' + document.querySelector("html").dataset.backendurl,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    Accept: 'application/json'
  },
  timeout: 10000
});

instanciaAxios.interceptors.request.use(function (config) {
  config.headers.Authorization =  obtenerAccessToken();

  return config;
});

function obtenerAccessToken () {
  let token = localStorage.getItem("usuario");
  if (token != null) {
    return 'Bearer ' + JSON.parse(token).accessToken;
  }
  return null;
}

export default instanciaAxios;
