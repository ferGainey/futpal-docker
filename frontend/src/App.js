import './App.css';

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Registro from './componentes/autenticacion/Registro.jsx';
import Ingreso from './componentes/autenticacion/Ingreso.jsx';
import PantallaInicial from './componentes/autenticado/PantallaInicial.jsx';
import PantallaInicialUsuario from './componentes/autenticado/PantallaInicialUsuario.jsx';
import CreacionLiga from './componentes/autenticado/liga/CreacionLiga.jsx';
import PantallaLiga from './componentes/autenticado/liga/PantallaLiga.jsx';
import EdicionEquipo from './componentes/autenticado/equipo/EdicionEquipo.jsx';
import LigasDisponibles from './componentes/autenticado/liga/LigasDisponibles.jsx';
import PartidosLiga from './componentes/autenticado/liga/PartidosLiga.jsx';
import EstadisticasLiga from './componentes/autenticado/liga/estadisticas/visualizacion/EstadisticasLiga.jsx';
import Balances from "./componentes/autenticado/balance/Balances";
import Transferencias from "./componentes/autenticado/transferencias/Transferencias";
import MiPlantel from "./componentes/autenticado/plantel/MiPlantel";
import Planteles from "./componentes/autenticado/plantel/Planteles";

const App = () => {

  return (
    <Router>
      <div>
        <Switch>

          <Route path="/front/registro">
            <Registro />
          </Route>

          <Route path="/front/ingreso">
            <Ingreso />
          </Route>

          <Route path="/front/pantalla-inicial-usuario">
            <PantallaInicialUsuario />
          </Route>

          <Route path="/front/crear-liga">
            <CreacionLiga />
          </Route>

          <Route path="/front/liga/:id/:nombre">
            <PantallaLiga />
          </Route>

          <Route path="/front/editar-equipo">
            <EdicionEquipo />
          </Route>

          <Route path="/front/ligas-disponibles">
            <LigasDisponibles />
          </Route>

          <Route path="/front/partidos-liga/:idLiga/:nombreLiga">
            <PartidosLiga />
          </Route>

          <Route path="/front/estadisticas-liga/:idLiga/:nombreLiga">
            <EstadisticasLiga />
          </Route>

          <Route path="/front/plantel">
            <MiPlantel />
          </Route>

          <Route path="/front/planteles">
            <Planteles />
          </Route>

          <Route path="/front/balances">
            <Balances />
          </Route>

          <Route path="/front/transferencias">
            <Transferencias />
          </Route>

          <Route path="/">
            <PantallaInicial />
          </Route>

        </Switch>
      </div>
    </Router>
  );
};
export default App;
