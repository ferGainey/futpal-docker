import React, {useEffect, useState} from 'react';
import BarraNavegacionLogueado from './BarraNavegacionLogueado.jsx';
import {useHistory} from "react-router-dom";

const PantallaInicialUsuario = () => {

    const [textoTituloUsuarioLogueado, setTextoTituloUsuarioLogueado] = useState();
    const history = useHistory();

    useEffect(() => {
      let tokenUsuario = localStorage.getItem("usuario");
      if (tokenUsuario != null) {
        let aliasUsuarioLogueado = JSON.parse(localStorage.getItem("usuario")).aliasUsuario;
        setTextoTituloUsuarioLogueado(`¡Hola ${aliasUsuarioLogueado}!`)
      }
      else {
        history.push("/");
      }
    }, [history]);

  return (
    <div>
    <BarraNavegacionLogueado />
      <div className="mensaje-bienvenida-inicio letra-blanca">
          <h2 id="titulo-pantalla-inicial-usuario">{textoTituloUsuarioLogueado}</h2>
      </div>
    </div>
          );
};
export default PantallaInicialUsuario;
