import React, {useEffect, useState} from 'react';
import BarraNavegacionLogueado from '../BarraNavegacionLogueado.jsx';
import API from '../../../api';
import {useHistory} from "react-router-dom";

const LigasDisponibles = () => {

    const [ligasDisponibles, setligasDisponibles] = useState([]);
    const history = useHistory();

    useEffect(() => {
        obtenerLigasDisponibles();
      }, []);

    const obtenerLigasDisponibles = async () => {
        const respuesta = await servicioObtenerLigasDisponibles();
        if (respuesta != null && respuesta.status === 200) {
            setligasDisponibles(respuesta.data);
        }
    }

    const servicioObtenerLigasDisponibles = async () => {
        return await API('/api/obtener-ligas-disponibles', {
            method: 'get',
          }).catch(error => {
            console.log(error.response);
          });
    }

    const irAPantallaLiga = (idLiga, nombreLiga) => {
        history.push(`/front/liga/${idLiga}/${nombreLiga}`);
    }

  return (
        <div>
            <BarraNavegacionLogueado />
            <div className="letra-blanca">
              <h3 id="titulo-ligas-disponibles" className="titulo-login pt-3">Ligas disponibles:</h3>
                <ul className="list-group d-flex align-items-center">
                    {ligasDisponibles.map((liga) => {
                    return <li className="list-group-item col-9 col-md-4 d-flex justify-content-center align-items-center letra-negra btn btn-light" data-value={liga.id} key={liga.id} onClick={() => irAPantallaLiga(liga.id, liga.nombre)}>
                        {liga.nombre}
                        </li>
                    })}
                </ul>
            </div>
        </div>
          );
};
export default LigasDisponibles;
