import React, {useEffect, useState} from 'react';
import BarraNavegacionLogueado from '../BarraNavegacionLogueado.jsx';
import {useParams} from "react-router";
import BarraLateralLiga from './BarraLateralLiga.jsx';
import EditorResultado from './EditorResultado.jsx';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit, faInfoCircle, faUndo} from '@fortawesome/free-solid-svg-icons';
import InformacionPartido from './estadisticas/visualizacion/InformacionPartido.jsx';
import ServicioLiga from "../../../servicios/ServicioLiga";
import $ from "jquery";

const PartidosLiga = () => {

    let {idLiga, nombreLiga} = useParams();
    const [resultados, setResultados] = useState([]);
    const [resultadosSinFiltro, setResultadosSinFiltro] = useState([]);
    const [resultadoSeleccionado, setResultadoSeleccionado] = useState([]);
    const [mostrarEditorResultado, setMostrarEditorResultado] = useState(false);
    const [mostrarInformacionPartido, setMostrarInformacionPartido] = useState(false);
    const [mostrarPartidos, setMostrarPartidos] = useState(false);

    useEffect(() => {
        $('#deshacer-filtros').css("display", "none");
        if (mostrarEditorResultado === false) {
            obtenerPartidosLiga(idLiga);
        }
    }, [mostrarEditorResultado]);

    const obtenerPartidosLiga = async (idLiga) => {
        ServicioLiga.obtenerPartidos(idLiga).then((respuesta) => {
            if (respuesta != null && respuesta.status === 200) {
                let partidos = respuesta.data;
                if (partidos != null) {
                    let array = partidos;
                    array.sort(function (a, b) {
                        return a.numeroFecha - b.numeroFecha
                    });
                    setResultados(array);
                    setResultadosSinFiltro(array);
                }
            }
            setMostrarPartidos(true);
        });
    }

    const golesEquipo = (goles) => {
        if (goles == null || goles === "") {
            if (goles === 0) {
                return "0";
            }
            return "-";
        }
        return goles;
    }

    const abrirEditorResultado = (resultado) => {
        setMostrarEditorResultado(true);
        setResultadoSeleccionado(resultado);
    }

    const abrirInformacionPartido = (resultado) => {
        setMostrarInformacionPartido(true);
        setResultadoSeleccionado(resultado);
    }

    const filtrarPartidos = (idEquipo) => {
        setResultados(resultadosSinFiltro.filter(partido => partido.equipoLocal.id === idEquipo || partido.equipoVisitante.id === idEquipo));
        $('#deshacer-filtros').css("display", "initial");
    }

    const deshacerFiltro = () => {
        setResultados(resultadosSinFiltro);
        $('#deshacer-filtros').css("display", "none");
    }

    return (
        <div>
            <BarraNavegacionLogueado/>
            <div className="row ml-0 mr-0">
                <BarraLateralLiga idLiga={idLiga} nombreLiga={nombreLiga}/>
                <div className="col-9 col-md-10 col-xl-11 letra-blanca">
                    <h3 id="titulo-partidos-liga" className="titulo-login mt-4 mb-4">Partidos de la liga: {nombreLiga}</h3>
                    <div id="deshacer-filtros" className="pointer" onClick={() => deshacerFiltro()}>
                        <u>Deshacer filtro</u>
                        <FontAwesomeIcon className="ml-2" icon={faUndo}/>
                    </div>
                    {mostrarEditorResultado ?
                        <EditorResultado mostrar={mostrarEditorResultado} setMostrar={setMostrarEditorResultado}
                                         resultado={resultadoSeleccionado}/> : null}
                    {mostrarInformacionPartido ? <InformacionPartido mostrar={mostrarInformacionPartido}
                                                                     setMostrar={setMostrarInformacionPartido}
                                                                     resultado={resultadoSeleccionado}/> : null}
                    <table id="tabla-partidos-liga" className="table table-dark mt-2">
                        <thead>
                        <tr>
                            <th id="partidos-liga-encabezado-fecha-partido" scope="col">Fecha</th>
                            <th id="partidos-liga-equipo-local" scope="col">Equipo Local</th>
                            <th id="partidos-liga-goles-equipo-local" scope="col"/>
                            <th id="partidos-liga-goles-equipo-visitante" scope="col"/>
                            <th id="partidos-liga-equipo-visitante" scope="col">Equipo Visitante</th>
                            <th id="partidos-liga-equipo-visitante" scope="col"/>
                        </tr>
                        </thead>
                        <tbody>
                            {mostrarPartidos ?
                                <>
                                    {resultados.map((resultado) => {
                                        return <tr className="fila-partidos-liga" key={resultado.id}>
                                            <td className="partidos-liga-columna-fecha-partido">{resultado.numeroFecha}</td>
                                            <td className="partidos-liga-columna-equipo-local pointer btn-dark" onClick={() => filtrarPartidos(resultado.equipoLocal.id)}>{resultado.equipoLocal.nombreEquipo}</td>
                                            <td className="partidos-liga-columna-goles-equipo-local">{golesEquipo(resultado.golesEquipoLocal)}</td>
                                            <td className="partidos-liga-columna-goles-equipo-visitante">{golesEquipo(resultado.golesEquipoVisitante)}</td>
                                            <td className="partidos-liga-columna-equipo-visitante pointer btn-dark" onClick={() => filtrarPartidos(resultado.equipoVisitante.id)}>{resultado.equipoVisitante.nombreEquipo}</td>
                                            <td>
                                                <FontAwesomeIcon className="pointer informacion-partido-liga mr-3"
                                                                 icon={faInfoCircle}
                                                                 onClick={() => abrirInformacionPartido(resultado)}/>
                                                <FontAwesomeIcon className="pointer editor-partido-liga" icon={faEdit}
                                                                 onClick={() => abrirEditorResultado(resultado)}/>
                                            </td>
                                        </tr>
                                    })}
                                </>
                            : null}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};
export default PartidosLiga;
