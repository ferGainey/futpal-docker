import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrash} from '@fortawesome/free-solid-svg-icons';

const AgregadorTarjetasAmarillasEstadisticas = ({jugadoresLocal, jugadoresVisitante, setTarjetasAmarillasJugadores, tarjetasAmarillasJugadores, idEquipoLocal, idEquipoVisitante}) => {

    const [tarjetasAmarillasEquipoLocal, setTarjetasAmarillasEquipoLocal] = useState([]);
    const [tarjetasAmarillasEquipoVisitante, setTarjetasAmarillasEquipoVisitante] = useState([]);

    useEffect(() => {
        //cargar jugadores a las listas de amarillas locales y visitantes si es que ya se cargo el partido antes. Para eso usar los jugadores de cada equipo, y si esta en la lista dividirlo
        //Posible problema, si el jugador no esta mas en ese equipo no va a aparecer. Pero no es grave, porque si eso pasa es porque se está queriendo actualizar un partido muy viejo, y no es algo esperable.
        //ver si lo puedo traer dividido del back
        let ids_jugadores_locales = tarjetasAmarillasEquipoLocal.map(function(jugador) {
            return jugador.id;
        });
        let ids_jugadores_visitantes = tarjetasAmarillasEquipoVisitante.map(function(jugador) {
            return jugador.id;
        });

        let nuevoArrayEquipoLocal = [...tarjetasAmarillasEquipoLocal];
        let nuevoArrayEquipoVisitante = [...tarjetasAmarillasEquipoVisitante];
        tarjetasAmarillasJugadores.forEach(jugador => {
            if (jugador.equipo.id === idEquipoLocal && !ids_jugadores_locales.includes(jugador.id)) {
                nuevoArrayEquipoLocal.push(jugador);
                setTarjetasAmarillasEquipoLocal(nuevoArrayEquipoLocal);
            }
            else if (jugador.equipo.id === idEquipoVisitante && !ids_jugadores_visitantes.includes(jugador.id)) {
                nuevoArrayEquipoVisitante.push(jugador);
                setTarjetasAmarillasEquipoVisitante(nuevoArrayEquipoVisitante);
            }
        });
      }, [tarjetasAmarillasJugadores]);


    const agregarJugadorLocal = () => {
        let ids_jugadores = tarjetasAmarillasJugadores.map(function(jugador) {
            return jugador.id;
        });
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-tarjetas-amarillas-estadisticas-jugadores-locales").value);
        if (!ids_jugadores.includes(jugadorSeleccionado.id)) {
            let nuevoArray = [...tarjetasAmarillasEquipoLocal];
            let datosJugador = {
                id: jugadorSeleccionado.id,
                nombre: jugadorSeleccionado.nombre,
                equipo: {
                    id: idEquipoLocal
                }
            }
            nuevoArray.push(datosJugador);
            setTarjetasAmarillasEquipoLocal(nuevoArray);
            agregarJugadorAListaTarjetasAmarillas(datosJugador);  
        }        
    }

    const agregarJugadorVisitante = () => {
        let ids_jugadores = tarjetasAmarillasJugadores.map(function(jugador) {
            return jugador.id;
        });
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-tarjetas-amarillas-estadisticas-jugadores-visitantes").value);
        if (!ids_jugadores.includes(jugadorSeleccionado.id)) {
            let nuevoArray = [...tarjetasAmarillasEquipoVisitante];
            let datosJugador = {
                id: jugadorSeleccionado.id,
                nombre: jugadorSeleccionado.nombre,
                equipo: {
                    id: idEquipoVisitante
                }                    
            }
            nuevoArray.push(datosJugador);
            setTarjetasAmarillasEquipoVisitante(nuevoArray);
            agregarJugadorAListaTarjetasAmarillas(datosJugador);
        }
    }
    
    const agregarJugadorAListaTarjetasAmarillas = (jugadorNuevo) => {
        let nuevoArray = [...tarjetasAmarillasJugadores];
        nuevoArray.push(jugadorNuevo);
        setTarjetasAmarillasJugadores(nuevoArray);
    }

    const borrarJugadorLocal = (jugadorId, amonestados, setAmonestados) => {
        let array = [...amonestados];
        let ids_jugadores = array.map(function(jugador) {
            return jugador.id;
        });
        let index = ids_jugadores.indexOf(jugadorId);        
        if (index !== -1) {
            array.splice(index, 1);
            setAmonestados(array);
        }

        borrarJugadorDeAmarillas(jugadorId);
    }

    const borrarJugadorDeAmarillas = (jugadorId) => {
        let array = [...tarjetasAmarillasJugadores];
        let ids_jugadores = array.map(function(jugador) {
            return jugador.id;
        });
        let index = ids_jugadores.indexOf(jugadorId);        
        if (index !== -1) {
            array.splice(index, 1);
            setTarjetasAmarillasJugadores(array);
        }
    }


    return (
          <div>
            <div className="row">
                <select id="agregador-tarjetas-amarillas-estadisticas-jugadores-locales" className="form-control col-4 ml-1" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresLocal.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <span className="col-2"/>

                <select id="agregador-tarjetas-amarillas-estadisticas-jugadores-visitantes" className="form-control col-4" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresVisitante.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <span className="col-1 p-1 ml-1"/>
            </div>

            <div className="row">
                <button id="agregador-amarillas-estadisticas-boton-agregar-amarilla-jugador-local" className="btn btn-secondary btn-sm col-2 ml-1 form-control mt-1" type="button" onClick={() => agregarJugadorLocal()}>Agregar</button>
                <span className="col-4"/>
                <button id="agregador-amarillas-estadisticas-boton-agregar-amarilla-jugador-visitante" className="btn btn-secondary btn-sm col-2 form-control mt-1" type="button" onClick={() => agregarJugadorVisitante()}>Agregar</button>
            </div>
            <div className="row">
                <div id="agregador-amarillas-amonestados-local" className="col-5 ml-1 mt-1">
                {tarjetasAmarillasEquipoLocal.map((jugador) => {
                    return <div className="row">
                        <p className="amarillas-jugador-local-agregado mr-2" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}
                        </p>
                        <FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorLocal(jugador.id, tarjetasAmarillasEquipoLocal, setTarjetasAmarillasEquipoLocal)} />
                    </div>
                })}
                </div>
                <span className="col-1"/>
                <div id="agregador-amarillas-amonestados-visitante" className="col-5 ml-1 mt-1">
                {tarjetasAmarillasEquipoVisitante.map((jugador) => {
                    return <div className="row">
                        <p className="amarillas-jugador-visitante-agregado mr-2" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}
                        </p>
                        <FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorLocal(jugador.id, tarjetasAmarillasEquipoVisitante, setTarjetasAmarillasEquipoVisitante)} />
                    </div>
                })}
                </div>
            </div>
          </div>
          );
};
export default AgregadorTarjetasAmarillasEstadisticas;
