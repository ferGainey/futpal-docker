import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrash} from '@fortawesome/free-solid-svg-icons';


const AgregadorLesionesEstadisticas = ({jugadoresLocal, jugadoresVisitante, setLesionesJugadores, lesionesJugadores, idEquipoLocal, idEquipoVisitante}) => {

    const [lesionesEquipoLocal, setLesionesEquipoLocal] = useState([]);
    const [lesionesEquipoVisitante, setLesionesEquipoVisitante] = useState([]);

    useEffect(() => {
        //cargar jugadores a las listas de lesiones locales y visitantes si es que ya se cargo el partido antes. Para eso usar los jugadores de cada equipo, y si esta en la lista dividirlo
        //Posible problema, si el jugador no esta mas en ese equipo no va a aparecer. Pero no es grave, porque si eso pasa es porque se está queriendo actualizar un partido muy viejo, y no es algo esperable.
        //ver si lo puedo traer dividido del back
        let ids_jugadores_locales = lesionesEquipoLocal.map(function(jugador) {
            return jugador.id;
        });
        let ids_jugadores_visitantes = lesionesEquipoVisitante.map(function(jugador) {
            return jugador.id;
        });

        let nuevoArrayEquipoLocal = [...lesionesEquipoLocal];
        let nuevoArrayEquipoVisitante = [...lesionesEquipoVisitante];
        lesionesJugadores.forEach(jugador => {
            if (jugador.equipo.id === idEquipoLocal && !ids_jugadores_locales.includes(jugador.id)) {
                nuevoArrayEquipoLocal.push(jugador);
                setLesionesEquipoLocal(nuevoArrayEquipoLocal);
            }
            else if (jugador.equipo.id === idEquipoVisitante && !ids_jugadores_visitantes.includes(jugador.id)) {
                nuevoArrayEquipoVisitante.push(jugador);
                setLesionesEquipoVisitante(nuevoArrayEquipoVisitante);
            }
        });
      }, [lesionesJugadores]);


    const agregarJugadorLocal = () => {
        let ids_jugadores = lesionesJugadores.map(function(jugador) {
            return jugador.id;
        });
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-lesiones-estadisticas-jugadores-locales").value);
        if (!ids_jugadores.includes(jugadorSeleccionado.id)) {
            let nuevoArray = [...lesionesEquipoLocal];
        
            let datosJugador = {
                id: jugadorSeleccionado.id,
                nombre: jugadorSeleccionado.nombre,
                equipo: {
                    id: idEquipoLocal
                }
            }
            nuevoArray.push(datosJugador);
            setLesionesEquipoLocal(nuevoArray);
            agregarJugadorAListaLesiones(datosJugador);  
        }              
    }

    const agregarJugadorVisitante = () => {
        let ids_jugadores = lesionesJugadores.map(function(jugador) {
            return jugador.id;
        });
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-lesiones-estadisticas-jugadores-visitantes").value);
        if (!ids_jugadores.includes(jugadorSeleccionado.id)) {
            let nuevoArray = [...lesionesEquipoVisitante];
            let datosJugador = {
                id: jugadorSeleccionado.id,
                nombre: jugadorSeleccionado.nombre,
                equipo: {
                    id: idEquipoVisitante
                }
            }
            nuevoArray.push(datosJugador);
            setLesionesEquipoVisitante(nuevoArray);
            agregarJugadorAListaLesiones(datosJugador);
        }
    }
    
    const agregarJugadorAListaLesiones = (jugadorNuevo) => {
        let nuevoArray = [...lesionesJugadores];
        nuevoArray.push(jugadorNuevo);
        setLesionesJugadores(nuevoArray);
    }

    const borrarJugadorLocal = (jugadorId, lesionados, setLesionados) => {
        let array = [...lesionados];
        let ids_jugadores = array.map(function(jugador) {
            return jugador.id;
        });
        let index = ids_jugadores.indexOf(jugadorId);        
        if (index !== -1) {
            array.splice(index, 1);
            setLesionados(array);
        }

        borrarJugadorDeLesiones(jugadorId);
    }

    const borrarJugadorDeLesiones = (jugadorId) => {
        let array = [...lesionesJugadores];
        let ids_jugadores = array.map(function(jugador) {
            return jugador.id;
        });
        let index = ids_jugadores.indexOf(jugadorId);        
        if (index !== -1) {
            array.splice(index, 1);
            setLesionesJugadores(array);
        }
    }

    return (
          <div>
            <div className="row">
                <select id="agregador-lesiones-estadisticas-jugadores-locales" className="form-control col-4 ml-1" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresLocal.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <span className="col-2"/>

                <select id="agregador-lesiones-estadisticas-jugadores-visitantes" className="form-control col-4" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresVisitante.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <span className="col-1 p-1 ml-1"/>
            </div>

            <div className="row">
                <button id="agregador-lesiones-estadisticas-boton-agregar-lesion-jugador-local" className="btn btn-secondary btn-sm col-2 ml-1 form-control mt-1" type="button" onClick={() => agregarJugadorLocal()}>Agregar</button>
                <span className="col-4"/>
                <button id="agregador-lesiones-estadisticas-boton-agregar-lesion-jugador-visitante" className="btn btn-secondary btn-sm col-2 form-control mt-1" type="button" onClick={() => agregarJugadorVisitante()}>Agregar</button>
            </div>
            <div className="row">
                <div id="agregador-lesiones-lesionados-local" className="col-5 ml-1 mt-1">
                {lesionesEquipoLocal.map((jugador) => {
                    return <div className="row">
                        <p className="lesiones-jugador-local-agregado mr-2" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}
                        </p>
                        <FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorLocal(jugador.id, lesionesEquipoLocal, setLesionesEquipoLocal)} />
                    </div>
                })}
                </div>
                <span className="col-1"/>
                <div id="agregador-lesiones-lesionados-visitante" className="col-5 ml-1 mt-1">
                {lesionesEquipoVisitante.map((jugador) => {
                    return <div className="row">
                        <p className="lesiones-jugador-visitante-agregado mr-2" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}
                        </p>
                        <FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorLocal(jugador.id, lesionesEquipoVisitante, setLesionesEquipoVisitante)} />
                    </div>
                })}
                </div>
            </div>
          </div>
          );
};
export default AgregadorLesionesEstadisticas;
