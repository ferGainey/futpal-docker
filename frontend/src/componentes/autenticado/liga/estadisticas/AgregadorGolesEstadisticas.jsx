import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrash} from '@fortawesome/free-solid-svg-icons';

const AgregadorGolesEstadisticas = ({jugadoresLocal, jugadoresVisitante, setGolesJugadores, golesJugadores, idEquipoLocal, idEquipoVisitante, golesLocal, golesVisitante}) => {

    const [cantidadGolesJugadorLocal, setCantidadGolesJugadorLocal] = useState();
    const [cantidadGolesJugadorVisitante, setCantidadGolesJugadorVisitante] = useState();
    const [goleadoresEquipoLocal, setGoleadoresEquipoLocal] = useState([]);
    const [goleadoresEquipoVisitante, setGoleadoresEquipoVisitante] = useState([]);
    const [mostrarAdvertenciaGoles, setMostrarAdvertenciaGoles] = useState(true);

    useEffect(() => {
        //cargar jugadores a las listas de goleadores locales y visitantes si es que ya se cargo el partido antes. Para eso usar los jugadores de cada equipo, y si esta en la lista dividirlo
        //Posible problema, si el jugador no esta mas en ese equipo no va a aparecer. Pero no es grave, porque si eso pasa es porque se está queriendo actualizar un partido muy viejo, y no es algo esperable.
        //ver si lo puedo traer dividido del back
        separarGoleadoresEnLocalYVisitante();
        revisarSiCoincidenGolesResultadoConGolesAgregados();
      }, [golesJugadores]);


    useEffect(() => {
        revisarSiCoincidenGolesResultadoConGolesAgregados();
    }, [golesLocal, golesVisitante, goleadoresEquipoLocal, goleadoresEquipoVisitante])


    const separarGoleadoresEnLocalYVisitante = () => {
        let ids_jugadores_locales = goleadoresEquipoLocal.map(function(jugador) {
            return jugador.id;
        });
        let ids_jugadores_visitantes = goleadoresEquipoVisitante.map(function(jugador) {
            return jugador.id;
        });
        let nuevoArrayEquipoLocal = [...goleadoresEquipoLocal];
        let nuevoArrayEquipoVisitante = [...goleadoresEquipoVisitante];
        golesJugadores.forEach(jugador => {
            if (jugador.equipo.id === idEquipoLocal && !ids_jugadores_locales.includes(jugador.id)) {
                nuevoArrayEquipoLocal.push(jugador);
                setGoleadoresEquipoLocal(nuevoArrayEquipoLocal);
            }
            else if (jugador.equipo.id === idEquipoVisitante && !ids_jugadores_visitantes.includes(jugador.id)) {
                nuevoArrayEquipoVisitante.push(jugador);
                setGoleadoresEquipoVisitante(nuevoArrayEquipoVisitante);
            }
        });
    }

    const revisarSiCoincidenGolesResultadoConGolesAgregados = () => {
        let golesLocalesAgregados = 0;
        let golesVisitantesAgregados = 0;
        
        goleadoresEquipoLocal.forEach(jugador => {
            golesLocalesAgregados += jugador.cantidad;
        });
        goleadoresEquipoVisitante.forEach(jugador => {
            golesVisitantesAgregados += jugador.cantidad;
        });
        
        if (parseInt(golesLocal) !== golesLocalesAgregados || parseInt(golesVisitante) !== golesVisitantesAgregados) {
            setMostrarAdvertenciaGoles(true);
        }
        else {
            setMostrarAdvertenciaGoles(false);
        }
    }

    const agregarJugadorLocal = () => {
        let ids_jugadores = golesJugadores.map(function(jugador) {
            return jugador.id;
        });
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-goles-estadisticas-jugadores-locales").value);
        if (!ids_jugadores.includes(jugadorSeleccionado.id)) {
            let nuevoArray = [...goleadoresEquipoLocal];
            let datosJugador = {
                id: jugadorSeleccionado.id,
                nombre: jugadorSeleccionado.nombre,
                cantidad: cantidadGolesJugadorLocal,
                equipo: {
                    id: idEquipoLocal
                }
            }
            nuevoArray.push(datosJugador);
            setGoleadoresEquipoLocal(nuevoArray);
            agregarJugadorAListaGoleadores(datosJugador);  
        }      
    }

    const agregarJugadorVisitante = () => {
        let ids_jugadores = golesJugadores.map(function(jugador) {
            return jugador.id;
        });
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-goles-estadisticas-jugadores-visitantes").value);
        if (!ids_jugadores.includes(jugadorSeleccionado.id)) {
            let nuevoArray = [...goleadoresEquipoVisitante];
            let datosJugador = {
                id: jugadorSeleccionado.id,
                nombre: jugadorSeleccionado.nombre,
                cantidad: cantidadGolesJugadorVisitante,
                equipo: {
                    id: idEquipoVisitante
                }
            }
            nuevoArray.push(datosJugador);
            setGoleadoresEquipoVisitante(nuevoArray);
            agregarJugadorAListaGoleadores(datosJugador);
        }
    }
    
    const agregarJugadorAListaGoleadores = (jugadorNuevo) => {
        let nuevoArray = [...golesJugadores];
        nuevoArray.push(jugadorNuevo);
        setGolesJugadores(nuevoArray);
    }

    const borrarJugadorLocal = (jugadorId, goleadores, setGoleadores) => {
        let array = [...goleadores];
        let ids_jugadores = array.map(function(jugador) {
            return jugador.id;
        });
        let index = ids_jugadores.indexOf(jugadorId);        
        if (index !== -1) {
            array.splice(index, 1);
            setGoleadores(array);
        }

        borrarJugadorDeGoleadores(jugadorId);
    }

    const borrarJugadorDeGoleadores = (jugadorId) => {
        let array = [...golesJugadores];
        let ids_jugadores = array.map(function(jugador) {
            return jugador.id;
        });
        let index = ids_jugadores.indexOf(jugadorId);        
        if (index !== -1) {
            array.splice(index, 1);
            setGolesJugadores(array);
        }
    }

    return (
        <div>
            <div className="row">
                <select id="agregador-goles-estadisticas-jugadores-locales" className="form-control col-4 ml-1" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresLocal.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <input id="agregador-goles-estadisticas-goles-jugador-local" value={cantidadGolesJugadorLocal} type="number" className="col-1 p-1 text-center ml-1" onChange={e => setCantidadGolesJugadorLocal(e.target.value != "" ? Math.abs(e.target.value) : e.target.value)} min="0"/>
                <span className="col-1"/>

                <select id="agregador-goles-estadisticas-jugadores-visitantes" className="form-control col-4" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresVisitante.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <input id="agregador-goles-estadisticas-goles-jugador-visitante" value={cantidadGolesJugadorVisitante} type="number" className="col-1 p-1 text-center ml-1" onChange={e => setCantidadGolesJugadorVisitante(e.target.value != "" ? Math.abs(e.target.value) : e.target.value)} min="0"/>
            </div>

            <div className="row">
                <button id="agregador-goles-estadisticas-boton-agregar-gol-jugador-local" className="btn btn-secondary btn-sm col-2 ml-1 form-control mt-1" type="button" onClick={() => agregarJugadorLocal()}>Agregar</button>
                <span className="col-4"/>
                <button id="agregador-goles-estadisticas-boton-agregar-gol-jugador-visitante" className="btn btn-secondary btn-sm col-2 form-control mt-1" type="button" onClick={() => agregarJugadorVisitante()}>Agregar</button>
            </div>
            <div className="row">
                <div id="agregador-goles-goleadores-local" className="col-5 ml-1 mt-1">
                {goleadoresEquipoLocal.map((jugador) => {
                    return <div className="row">
                        <p className="goles-jugador-local-agregado mr-2" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}: {jugador.cantidad}
                        </p>
                        <span id="agregador-gol-borrar-jugador-local"><FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorLocal(jugador.id, goleadoresEquipoLocal, setGoleadoresEquipoLocal)} /></span>
                        
                    </div>
                })}
                </div>
                <span className="col-1"/>
                <div id="agregador-goles-goleadores-visitante" className="col-5 ml-1 mt-1">
                {goleadoresEquipoVisitante.map((jugador) => {
                    return <div className="row">
                        <p className="goles-jugador-visitante-agregado mr-2" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}: {jugador.cantidad}
                        </p>
                        <FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorLocal(jugador.id, goleadoresEquipoVisitante, setGoleadoresEquipoVisitante)} />
                        </div>
                })}
                </div>
            </div>
            {mostrarAdvertenciaGoles ? 
            <div className="alert alert-warning" role="alert">
                Los cantidad de goles no coinciden con la del resultado.
            </div>
             : null}
            
        </div>
          );
};
export default AgregadorGolesEstadisticas;
