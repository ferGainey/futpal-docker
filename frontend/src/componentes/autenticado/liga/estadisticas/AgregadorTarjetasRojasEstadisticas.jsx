import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrash} from '@fortawesome/free-solid-svg-icons';

const AgregadorTarjetasRojasEstadisticas = ({jugadoresLocal, jugadoresVisitante, setTarjetasRojasJugadores, tarjetasRojasJugadores, idEquipoLocal, idEquipoVisitante}) => {

    const [tarjetasRojasEquipoLocal, setTarjetasRojasEquipoLocal] = useState([]);
    const [tarjetasRojasEquipoVisitante, setTarjetasRojasEquipoVisitante] = useState([]);

    useEffect(() => {
        //cargar jugadores a las listas de rojas locales y visitantes si es que ya se cargo el partido antes. Para eso usar los jugadores de cada equipo, y si esta en la lista dividirlo
        //Posible problema, si el jugador no esta mas en ese equipo no va a aparecer. Pero no es grave, porque si eso pasa es porque se está queriendo actualizar un partido muy viejo, y no es algo esperable.
        //ver si lo puedo traer dividido del back
        let ids_jugadores_locales = tarjetasRojasEquipoLocal.map(function(jugador) {
            return jugador.id;
        });
        let ids_jugadores_visitantes = tarjetasRojasEquipoVisitante.map(function(jugador) {
            return jugador.id;
        });

        let nuevoArrayEquipoLocal = [...tarjetasRojasEquipoLocal];
        let nuevoArrayEquipoVisitante = [...tarjetasRojasEquipoVisitante];
        tarjetasRojasJugadores.forEach(jugador => {
            if (jugador.equipo.id === idEquipoLocal && !ids_jugadores_locales.includes(jugador.id)) {
                nuevoArrayEquipoLocal.push(jugador);
                setTarjetasRojasEquipoLocal(nuevoArrayEquipoLocal);
            }
            else if (jugador.equipo.id === idEquipoVisitante && !ids_jugadores_visitantes.includes(jugador.id)) {
                nuevoArrayEquipoVisitante.push(jugador);
                setTarjetasRojasEquipoVisitante(nuevoArrayEquipoVisitante);
            }
        });
      }, [tarjetasRojasJugadores]);


    const agregarJugadorLocal = () => {
        let ids_jugadores = tarjetasRojasJugadores.map(function(jugador) {
            return jugador.id;
        });
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-tarjetas-rojas-estadisticas-jugadores-locales").value);
        if (!ids_jugadores.includes(jugadorSeleccionado.id)) {
            let nuevoArray = [...tarjetasRojasEquipoLocal];
        let datosJugador = {
            id: jugadorSeleccionado.id,
            nombre: jugadorSeleccionado.nombre,
            equipo: {
                id: idEquipoLocal
            }
        }
        nuevoArray.push(datosJugador);
        setTarjetasRojasEquipoLocal(nuevoArray);
        agregarJugadorAListaTarjetasRojas(datosJugador);
        }  
    }

    const agregarJugadorVisitante = () => {
        let ids_jugadores = tarjetasRojasJugadores.map(function(jugador) {
            return jugador.id;
        });
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-tarjetas-rojas-estadisticas-jugadores-visitantes").value);
        if (!ids_jugadores.includes(jugadorSeleccionado.id)) {
            let nuevoArray = [...tarjetasRojasEquipoVisitante];
            let datosJugador = {
                id: jugadorSeleccionado.id,
                nombre: jugadorSeleccionado.nombre,
                equipo: {
                    id: idEquipoVisitante
                }
            }
            nuevoArray.push(datosJugador);
            setTarjetasRojasEquipoVisitante(nuevoArray);
            agregarJugadorAListaTarjetasRojas(datosJugador);
        }
    }
    
    const agregarJugadorAListaTarjetasRojas = (jugadorNuevo) => {
        let nuevoArray = [...tarjetasRojasJugadores];
        nuevoArray.push(jugadorNuevo);
        setTarjetasRojasJugadores(nuevoArray);
    }

    const borrarJugadorLocal = (jugadorId, expulsados, setExpulsados) => {
        let array = [...expulsados];
        let ids_jugadores = array.map(function(jugador) {
            return jugador.id;
        });
        let index = ids_jugadores.indexOf(jugadorId);        
        if (index !== -1) {
            array.splice(index, 1);
            setExpulsados(array);
        }

        borrarJugadorDeRojas(jugadorId);
    }

    const borrarJugadorDeRojas = (jugadorId) => {
        let array = [...tarjetasRojasJugadores];
        let ids_jugadores = array.map(function(jugador) {
            return jugador.id;
        });
        let index = ids_jugadores.indexOf(jugadorId);        
        if (index !== -1) {
            array.splice(index, 1);
            setTarjetasRojasJugadores(array);
        }
    }

    return (
          <div>
            <div className="row">
                <select id="agregador-tarjetas-rojas-estadisticas-jugadores-locales" className="form-control col-4 ml-1" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresLocal.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <span className="col-2"/>

                <select id="agregador-tarjetas-rojas-estadisticas-jugadores-visitantes" className="form-control col-4" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresVisitante.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <span className="col-1 p-1 ml-1"/>
            </div>

            <div className="row">
                <button id="agregador-rojas-estadisticas-boton-agregar-roja-jugador-local" className="btn btn-secondary btn-sm col-2 ml-1 form-control mt-1" type="button" onClick={() => agregarJugadorLocal()}>Agregar</button>
                <span className="col-4"/>
                <button id="agregador-rojas-estadisticas-boton-agregar-roja-jugador-visitante" className="btn btn-secondary btn-sm col-2 form-control mt-1" type="button" onClick={() => agregarJugadorVisitante()}>Agregar</button>
            </div>
            <div className="row">
                <div id="agregador-rojas-expulsados-local" className="col-5 ml-1 mt-1">
                {tarjetasRojasEquipoLocal.map((jugador) => {
                    return <div className="row">
                        <p className="rojas-jugador-local-agregado mr-2" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}
                        </p>
                        <FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorLocal(jugador.id, tarjetasRojasEquipoLocal, setTarjetasRojasEquipoLocal)} />
                    </div>
                })}
                </div>
                <span className="col-1"/>
                <div id="agregador-rojas-expulsados-visitante" className="col-5 ml-1 mt-1">
                {tarjetasRojasEquipoVisitante.map((jugador) => {
                    return <div className="row">
                        <p className="rojas-jugador-visitante-agregado mr-2" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}
                        </p>
                        <FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorLocal(jugador.id, tarjetasRojasEquipoVisitante, setTarjetasRojasEquipoVisitante)} />
                    </div> 
                })}
                </div>
            </div>
          </div>
          );
};
export default AgregadorTarjetasRojasEstadisticas;
