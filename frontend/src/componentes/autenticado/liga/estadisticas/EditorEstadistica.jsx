import React, {useEffect, useState} from 'react';
import 'bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAmbulance, faFutbol, faStar, faStickyNote} from '@fortawesome/free-solid-svg-icons';
import ServicioJugador from '../../../../servicios/ServicioJugador';
import AgregadorGolesEstadisticas from './AgregadorGolesEstadisticas.jsx';
import AgregadorTarjetasAmarillasEstadisticas from './AgregadorTarjetasAmarillasEstadisticas.jsx';
import AgregadorTarjetasRojasEstadisticas from './AgregadorTarjetasRojasEstadisticas.jsx';
import AgregadorLesionesEstadisticas from './AgregadorLesionesEstadisticas.jsx';
import AgregadorMvpEstadisticas from './AgregadorMvpEstadisticas.jsx';
import ServicioEstadistica from '../../../../servicios/ServicioEstadistica';

const EditorEstadistica = ({resultado, setEstadisticas, golesLocal, golesVisitante}) => {


    const [jugadoresLocal, setJugadoresLocal] = useState([]);
    const [jugadoresVisitante, setJugadoresVisitante] = useState([]);
    const [golesJugadores, setGolesJugadores] = useState([]);
    const [tarjetasAmarillasJugadores, setTarjetasAmarillasJugadores] = useState([]);
    const [tarjetasRojasJugadores, setTarjetasRojasJugadores] = useState([]);
    const [lesionesJugadores, setLesionesJugadores] = useState([]);
    const [mvpJugador, setMvpJugador] = useState();

    useEffect(() => {
        cargarJugadores(resultado.equipoLocal.id, resultado.equipoVisitante.id);
        precargarEstadisticas(resultado.id);
        //obtener estadisticas si existen
    }, []);

    useEffect(() => {
        let estadisticasActualizadas = {
            golesJugadores: golesJugadores,
            tarjetasAmarillasJugadores: tarjetasAmarillasJugadores,
            tarjetasRojasJugadores: tarjetasRojasJugadores,
            lesionesJugadores: lesionesJugadores,
            mvpJugador: mvpJugador,
            partidoId: resultado.id
        };
        setEstadisticas(estadisticasActualizadas);
    }, [golesJugadores, tarjetasAmarillasJugadores, tarjetasRojasJugadores, lesionesJugadores, mvpJugador]);

    const precargarEstadisticas = async (partidoId) => {
        let respuestaObtenerEstadisticas = await ServicioEstadistica.obtenerEstadisticasPartido(partidoId);
        if (respuestaObtenerEstadisticas.data != null) {
            if (respuestaObtenerEstadisticas.data.golesJugadores != null) {
                setGolesJugadores(respuestaObtenerEstadisticas.data.golesJugadores);
            }
            if (respuestaObtenerEstadisticas.data.tarjetasAmarillasJugadores != null) {
                setTarjetasAmarillasJugadores(respuestaObtenerEstadisticas.data.tarjetasAmarillasJugadores);
            }
            if (respuestaObtenerEstadisticas.data.tarjetasRojasJugadores != null) {
                setTarjetasRojasJugadores(respuestaObtenerEstadisticas.data.tarjetasRojasJugadores);
            }
            if (respuestaObtenerEstadisticas.data.lesionesJugadores != null) {
                setLesionesJugadores(respuestaObtenerEstadisticas.data.lesionesJugadores);
            }
            if (respuestaObtenerEstadisticas.data.mvpJugador != null) {
                setMvpJugador(respuestaObtenerEstadisticas.data.mvpJugador);
            }
        }  
    }

    const cargarJugadores = async (equipoIdLocal, equipoIdVisitante) => {
        let respuestaObtenerJugadoresEquipoLocal = await ServicioJugador.obtenerJugadores(equipoIdLocal);
        setJugadoresLocal(respuestaObtenerJugadoresEquipoLocal.data);

        let respuestaObtenerJugadoresEquipoVisitante = await ServicioJugador.obtenerJugadores(equipoIdVisitante);
        setJugadoresVisitante(respuestaObtenerJugadoresEquipoVisitante.data);
    }

    return (
    <>
    <div className="accordion" id="editor-estadisticas-editores">
        <div className="card">
            <div className="card-header" id="editor-estadisticas-goles">
                <h2 className="mb-0">
                    <button id="editor-estadistica-ver-goles" type="button" className="btn btn-link" data-toggle="collapse" data-target="#colapsar-goles">
                    <FontAwesomeIcon icon={faFutbol}/> Goles</button>									
                </h2>
            </div>
            <div id="colapsar-goles" className="collapse" aria-labelledby="editor-estadisticas-goles" data-parent="#editor-estadisticas-editores">
                <div className="card-body">
                    <AgregadorGolesEstadisticas jugadoresLocal={jugadoresLocal} jugadoresVisitante={jugadoresVisitante} setGolesJugadores={setGolesJugadores} golesJugadores={golesJugadores} idEquipoLocal={resultado.equipoLocal.id} idEquipoVisitante={resultado.equipoVisitante.id} golesLocal={golesLocal} golesVisitante={golesVisitante}/>
                </div>
            </div>
        </div>
        <div className="card">
            <div className="card-header" id="editor-estadisticas-amarillas">
                <h2 className="mb-0">
                    <button id="editor-estadistica-ver-amarillas" type="button" className="btn btn-link collapsed" data-toggle="collapse" data-target="#colapsar-amarillas">
                    <FontAwesomeIcon icon={faStickyNote}/> Tarjetas amarillas</button>
                </h2>
            </div>
            <div id="colapsar-amarillas" className="collapse" aria-labelledby="editor-estadisticas-amarillas" data-parent="#editor-estadisticas-editores">
                <div className="card-body">
                    <AgregadorTarjetasAmarillasEstadisticas jugadoresLocal={jugadoresLocal} jugadoresVisitante={jugadoresVisitante} setTarjetasAmarillasJugadores={setTarjetasAmarillasJugadores} tarjetasAmarillasJugadores={tarjetasAmarillasJugadores} idEquipoLocal={resultado.equipoLocal.id} idEquipoVisitante={resultado.equipoVisitante.id}/>
                </div>
            </div>
        </div>
        <div className="card">
            <div className="card-header" id="editor-estadisticas-rojas">
                <h2 className="mb-0">
                    <button id="editor-estadistica-ver-rojas" type="button" className="btn btn-link collapsed" data-toggle="collapse" data-target="#colapsar-rojas">
                    <FontAwesomeIcon icon={faStickyNote}/> Tarjetas rojas</button>
                </h2>
            </div>
            <div id="colapsar-rojas" className="collapse" aria-labelledby="editor-estadisticas-rojas" data-parent="#editor-estadisticas-editores">
                <div className="card-body">
                    <AgregadorTarjetasRojasEstadisticas jugadoresLocal={jugadoresLocal} jugadoresVisitante={jugadoresVisitante} setTarjetasRojasJugadores={setTarjetasRojasJugadores} tarjetasRojasJugadores={tarjetasRojasJugadores} idEquipoLocal={resultado.equipoLocal.id} idEquipoVisitante={resultado.equipoVisitante.id}/>
                </div>
            </div>
        </div>
        <div className="card">
            <div className="card-header" id="headingThree">
                <h2 className="mb-0">
                    <button id="editor-estadistica-ver-lesiones" type="button" className="btn btn-link collapsed" data-toggle="collapse" data-target="#colapsar-lesiones">
                    <FontAwesomeIcon icon={faAmbulance}/> Lesiones</button>                     
                </h2>
            </div>
            <div id="colapsar-lesiones" className="collapse" aria-labelledby="headingThree" data-parent="#editor-estadisticas-editores">
                <div className="card-body">
                    <AgregadorLesionesEstadisticas jugadoresLocal={jugadoresLocal} jugadoresVisitante={jugadoresVisitante} setLesionesJugadores={setLesionesJugadores} lesionesJugadores={lesionesJugadores} idEquipoLocal={resultado.equipoLocal.id} idEquipoVisitante={resultado.equipoVisitante.id}/>
                </div>
            </div>
        </div>
        <div className="card">
            <div className="card-header" id="headingThree">
                <h2 className="mb-0">
                    <button id="editor-estadistica-ver-mvp" type="button" className="btn btn-link collapsed" data-toggle="collapse" data-target="#colapsar-mvp">
                    <FontAwesomeIcon icon={faStar}/> MVP</button>                     
                </h2>
            </div>
            <div id="colapsar-mvp" className="collapse" aria-labelledby="headingThree" data-parent="#editor-estadisticas-editores">
                <div className="card-body">
                    <AgregadorMvpEstadisticas jugadoresLocal={jugadoresLocal} jugadoresVisitante={jugadoresVisitante} setMvpJugador={setMvpJugador} mvpJugador={mvpJugador} idEquipoLocal={resultado.equipoLocal.id} idEquipoVisitante={resultado.equipoVisitante.id}/>
                </div>
            </div>
        </div>
    </div>
    </>
    );
};
export default EditorEstadistica;
