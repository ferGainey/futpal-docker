import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrash} from '@fortawesome/free-solid-svg-icons';

const AgregadorMvpEstadisticas = ({jugadoresLocal, jugadoresVisitante, setMvpJugador, mvpJugador, idEquipoLocal, idEquipoVisitante}) => {

    const [mvpEquipoLocal, setMvpEquipoLocal] = useState();
    const [mvpEquipoVisitante, setMvpEquipoVisitante] = useState();

    useEffect(() => {
        //cargar jugadores al mvp local o visitante si es que ya se cargo el partido antes. Para eso usar los jugadores de cada equipo, y si esta en la lista dividirlo
        //Posible problema, si el jugador no esta mas en ese equipo no va a aparecer. Pero no es grave, porque si eso pasa es porque se está queriendo actualizar un partido muy viejo, y no es algo esperable.
        //ver si lo puedo traer dividido del back
            if (mvpJugador != null && mvpJugador.equipo.id == idEquipoLocal) {
                setMvpEquipoLocal(mvpJugador);
            }
            else {
                setMvpEquipoVisitante(mvpJugador);
            }
      }, [mvpJugador]);


    const agregarJugadorLocal = () => {
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-mvp-estadisticas-jugadores-locales").value);
        let datosJugador = {
            id: jugadorSeleccionado.id,
            nombre: jugadorSeleccionado.nombre,
            equipo: {
                id: idEquipoLocal
            }
        }
        setMvpEquipoLocal(datosJugador);
        agregarJugadorAListaMvp(datosJugador);        
    }

    const agregarJugadorVisitante = () => {
        let jugadorSeleccionado = JSON.parse(document.getElementById("agregador-mvp-estadisticas-jugadores-visitantes").value);
        let datosJugador = {
            id: jugadorSeleccionado.id,
            nombre: jugadorSeleccionado.nombre,
            equipo: {
                id: idEquipoVisitante
            }
        }
        setMvpEquipoVisitante(datosJugador);
        agregarJugadorAListaMvp(datosJugador);
    }
    
    const agregarJugadorAListaMvp = (jugadorNuevo) => {
        setMvpJugador(jugadorNuevo);
    }

    const borrarJugadorMvp = (setJugadorMvpEquipo) => {
        setJugadorMvpEquipo(null);
        setMvpJugador(null);
    }

    return (
          <div>
            <div className="row">
                <select id="agregador-mvp-estadisticas-jugadores-locales" className="form-control col-4 ml-1" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresLocal.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <span className="col-2"/>

                <select id="agregador-mvp-estadisticas-jugadores-visitantes" className="form-control col-4" defaultValue={'DEFAULT'}>
                    <option value="DEFAULT" disabled>Elija un jugador...</option>
                    {jugadoresVisitante.map((jugador, indice) => {
                        return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                    })}
                </select>
                <span className="col-1 p-1 ml-1"/>
            </div>

            <div className="row">
                <button id="agregador-mvp-estadisticas-boton-agregar-mvp-jugador-local" className="btn btn-secondary btn-sm col-2 ml-1 form-control mt-1" type="button" onClick={() => agregarJugadorLocal()}>Agregar</button>
                <span className="col-4"/>
                <button id="agregador-mvp-estadisticas-boton-agregar-mvp-jugador-visitante" className="btn btn-secondary btn-sm col-2 form-control mt-1" type="button" onClick={() => agregarJugadorVisitante()}>Agregar</button>
            </div>
            <div className="row">
                <div id="agregador-mvp-elegido-local" className="col-5 ml-1 mt-1">
                    { mvpEquipoLocal != null ?        
                    <div className="row">             
                        <p className="mvp-jugador-local-agregado mr-2">{mvpEquipoLocal.nombre}</p>
                        <FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorMvp(setMvpEquipoLocal)} />
                    </div>
                    : null }
                </div>
                <span className="col-1"/>
                <div id="agregador-mvp-elegido-visitante" className="col-5 ml-1 mt-1">
                    { mvpEquipoVisitante != null ?        
                    <div className="row">             
                        <p className="mvp-jugador-local-agregado mr-2">{mvpEquipoVisitante.nombre}</p>
                        <FontAwesomeIcon className="letra-negra pointer mt-1" icon={faTrash} onClick={() => borrarJugadorMvp(setMvpEquipoVisitante)} />
                    </div>
                    : null }
                </div>
            </div>
          </div>
          );
};
export default AgregadorMvpEstadisticas;
