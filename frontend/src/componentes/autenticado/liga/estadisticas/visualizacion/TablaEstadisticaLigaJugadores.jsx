import React from 'react';
import 'bootstrap';

const TablaEstadisticaLigaJugadores = ({jugadores, nombreEstadistica}) => {


    return (
      <>
        <h4>{nombreEstadistica}</h4>
        <table id="tabla-jugadores-estadistica" className="table table-dark">
            <thead>
                <tr>                
                <th id="encabezado-nombre-jugador" scope="col">Nombre</th>
                <th id="encabezado-equipo-jugador" scope="col">Equipo</th>
                <th id="encabezado-cantidad-jugador" scope="col">Cantidad</th>         
                </tr>
            </thead>
            <tbody>
            {jugadores.map((jugador) => {
                return <tr className="fila-jugadores" key={jugador.id}>
                    <td className="columna-nombre-jugador">{jugador.nombre}</td>
                    <td className="columna-equipo-jugador">{jugador.equipo.nombreEquipo}</td>                    
                    <td className="columna-cantidad-jugador">{jugador.cantidad}</td>

                    </tr>
                })}
            </tbody>
        </table>
      </>
    );
};
export default TablaEstadisticaLigaJugadores;
