import React, {useEffect, useState} from 'react';
import 'bootstrap';
import {useParams} from "react-router";
import BarraNavegacionLogueado from '../../../BarraNavegacionLogueado.jsx';
import BarraLateralLiga from '../../BarraLateralLiga.jsx';
import ServicioEstadistica from '../../../../../servicios/ServicioEstadistica.js';
import TablaEstadisticaLigaJugadores from './TablaEstadisticaLigaJugadores.jsx';

const EstadisticasLiga = () => {

    let { idLiga, nombreLiga } = useParams();

    const [golesJugadores, setGolesJugadores] = useState([]);
    const [tarjetasAmarillasJugadores, setTarjetasAmarillasJugadores] = useState([]);
    const [tarjetasRojasJugadores, setTarjetasRojasJugadores] = useState([]);
    const [lesionesJugadores, setLesionesJugadores] = useState([]);
    const [mvpJugadores, setMvpJugadores] = useState([]);


    useEffect(() => {
        obtenerEstadisticasLiga();
    }, []);

    const obtenerEstadisticasLiga = async () => {
        let resultado = await ServicioEstadistica.obtenerEstadisticasLiga(idLiga);
        setGolesJugadores(resultado.data.golesJugadores);
        setTarjetasAmarillasJugadores(resultado.data.tarjetasAmarillasJugadores);
        setTarjetasRojasJugadores(resultado.data.tarjetasRojasJugadores);
        setLesionesJugadores(resultado.data.lesionesJugadores);
        setMvpJugadores(resultado.data.mvpJugadores);
    }

    return (
      <>
        <BarraNavegacionLogueado />
        <div className="row ml-0 mr-0">
            <BarraLateralLiga idLiga={idLiga} nombreLiga={nombreLiga} />
            <div className="col letra-blanca overflow-auto">
                <h3 id="titulo-pantalla-liga" className="titulo-login mt-3">Liga: {nombreLiga}</h3>
                <div className="row">
                    <div className="col-md-3">
                        <TablaEstadisticaLigaJugadores jugadores={golesJugadores} nombreEstadistica="Goleadores"/>
                    </div>
                    <div className="col-md-3 ml-md-3">
                        <TablaEstadisticaLigaJugadores jugadores={tarjetasAmarillasJugadores} nombreEstadistica="Tarjetas amarillas"/>
                    </div>
                    <div className="col-md-3 ml-md-3">
                        <TablaEstadisticaLigaJugadores jugadores={tarjetasRojasJugadores} nombreEstadistica="Tarjetas rojas"/>
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-md-3">
                        <TablaEstadisticaLigaJugadores jugadores={lesionesJugadores} nombreEstadistica="Lesiones"/>
                    </div>
                    <div className="col-md-3 ml-md-3">
                        <TablaEstadisticaLigaJugadores jugadores={mvpJugadores} nombreEstadistica="MVP"/>
                    </div>
                </div>
            </div>
        </div>
      </>
    );
};
export default EstadisticasLiga;
