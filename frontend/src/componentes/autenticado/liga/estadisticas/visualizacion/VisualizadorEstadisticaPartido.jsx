import React from 'react';


const VisualizadorEstadisticaPartido = ({jugadoresLocal, jugadoresVisitante}) => {

    const hayGoles = (jugador) => {
        return jugador.cantidad != null && jugador.cantidad !== 0;

    }

    return (
          <div>
            <div className="row">
                <div id="visualizador-partido-local" className="col-5 ml-1 mt-1">
                {jugadoresLocal.map((jugador) => {
                    return <div className="row">
                        <p className="jugador-local-agregado ml-5" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}{hayGoles(jugador) ? <span>: {jugador.cantidad}</span> : null}
                        </p>                                        
                    </div>
                })}
                </div>
                <span className="col-1"/>
                <div id="visualizador-partido-visitante" className="col-5 ml-1 mt-1">
                {jugadoresVisitante.map((jugador) => {
                    return <div className="row">
                        <p className="jugador-visitante-agregado mr-2" data-value={jugador.id} key={jugador.id}>
                            {jugador.nombre}{hayGoles(jugador) ? <span>: {jugador.cantidad}</span> : null}
                        </p>                        
                        </div>
                })}
                </div>
            </div>
          </div>
          );
};
export default VisualizadorEstadisticaPartido;
