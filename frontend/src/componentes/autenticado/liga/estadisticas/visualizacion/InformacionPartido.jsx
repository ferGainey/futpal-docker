import React, {useEffect, useState} from 'react';
import 'bootstrap';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ServicioEstadistica from '../../../../../servicios/ServicioEstadistica';
import VisualizadorEstadisticaPartido from './VisualizadorEstadisticaPartido.jsx';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faAmbulance, faFutbol, faStar, faStickyNote} from '@fortawesome/free-solid-svg-icons';

const InformacionPartido = ({mostrar, setMostrar, resultado}) => {

    const [show, setShow] = useState(false);

    const [golesLocal, setGolesLocal] = useState([]);
    const [golesVisitante, setGolesVisitante] = useState([]);

    const [amarillasLocal, setAmarillasLocal] = useState([]);
    const [amarillasVisitante, setAmarillasVisitante] = useState([]);

    const [rojasLocal, setRojasLocal] = useState([]);
    const [rojasVisitante, setRojasVisitante] = useState([]);

    const [lesionesLocal, setLesionesLocal] = useState([]);
    const [lesionesVisitante, setLesionesVisitante] = useState([]);

    const [mvpLocal, setMvpLocal] = useState([]);
    const [mvpVisitante, setMvpVisitante] = useState([]);

    useEffect(() => {
        clasificarEstadisticas();
    }, []);

    useEffect(() => {
        setShow(mostrar);
    }, [mostrar]);

    const clasificarEstadisticas = async () => {
        let estadisticas = await ServicioEstadistica.obtenerEstadisticasPartido(resultado.id);
        clasificarEstadistica(estadisticas.data.golesJugadores, golesLocal, golesVisitante, setGolesLocal, setGolesVisitante);
        clasificarEstadistica(estadisticas.data.tarjetasAmarillasJugadores, amarillasLocal, amarillasVisitante, setAmarillasLocal, setAmarillasVisitante);
        clasificarEstadistica(estadisticas.data.tarjetasRojasJugadores, rojasLocal, rojasVisitante, setRojasLocal, setRojasVisitante);
        clasificarEstadistica(estadisticas.data.lesionesJugadores, lesionesLocal, lesionesVisitante, setLesionesLocal, setLesionesVisitante);
        clasificarEstadisticaMvp(estadisticas.data.mvpJugador);
    }

    const cerrarPopUp = () => {
        setShow(false);
        setMostrar(false);
    }

    const clasificarEstadistica = (estadisticas, jugadoresLocal, jugadoresVisitante, setJugadoresLocal, setJugadoresVisitante) => {
        let nuevoArrayJugadoresLocal = [...jugadoresLocal];
        let nuevoArrayJugadoresVisitante = [...jugadoresVisitante];
        estadisticas.forEach(jugador => {
            if (jugador.equipo.id === resultado.equipoLocal.id) {
                nuevoArrayJugadoresLocal.push(jugador);
            }
            else {
                nuevoArrayJugadoresVisitante.push(jugador);
            }
        });
        setJugadoresLocal(nuevoArrayJugadoresLocal);
        setJugadoresVisitante(nuevoArrayJugadoresVisitante);
    }

    const clasificarEstadisticaMvp = (mvpJugador) => {        
        if (mvpJugador != null) {
            let mvp = [];
            mvp.push(mvpJugador);
            if (mvpJugador.equipo.id === resultado.equipoLocal.id) {
                setMvpLocal(mvp);
            }
            else {
                setMvpVisitante(mvp);
            }
        }                
    }

    return (
      <>
        <Modal
          show={show}
          onHide={cerrarPopUp}
          backdrop="static"
          keyboard={false}
          size="lg"
        >
          <Modal.Header closeButton>
            <Modal.Title>Información Partido</Modal.Title>
          </Modal.Header>
          <Modal.Body>
                <div className="row">
                    <h4 className="col-3 ml-5">{resultado.equipoLocal.nombreEquipo}</h4>
                    <h3 className="col-1 ml-4">{resultado.golesEquipoLocal}</h3>
                    <h3 className="col-1 ml-4">-</h3>
                    <h3 className="col-1 ml-4">{resultado.golesEquipoVisitante}</h3>
                    <h4 className="col-3 mr-5">{resultado.equipoVisitante.nombreEquipo}</h4>
                </div>
                <div className="accordion" id="informacion-partido-ver">
                    <div className="card">
                        <div className="card-header" id="informacion-partido-goles">
                            <h2 className="mb-0">
                                <button id="informacion-partido-ver-goles" type="button" className="btn btn-link" data-toggle="collapse" data-target="#colapsar-goles">
                                <FontAwesomeIcon icon={faFutbol}/> Goles</button>									
                            </h2>
                        </div>
                        <div id="colapsar-goles" className="collapse" aria-labelledby="informacion-partido-goles" data-parent="#informacion-partido-ver">
                            <div id="informacion-partido-estadistica-goles" className="card-body">
                                <VisualizadorEstadisticaPartido jugadoresLocal={golesLocal} jugadoresVisitante={golesVisitante}/>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" id="informacion-partido-amarillas">
                            <h2 className="mb-0">
                                <button id="informacion-partido-ver-amarillas" type="button" className="btn btn-link collapsed" data-toggle="collapse" data-target="#colapsar-amarillas">
                                <FontAwesomeIcon icon={faStickyNote}/> Tarjetas amarillas</button>
                            </h2>
                        </div>
                        <div id="colapsar-amarillas" className="collapse" aria-labelledby="informacion-partido-amarillas" data-parent="#informacion-partido-ver">
                            <div id="informacion-partido-estadistica-amarillas" className="card-body">
                                <VisualizadorEstadisticaPartido jugadoresLocal={amarillasLocal} jugadoresVisitante={amarillasVisitante}/>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" id="informacion-partido-rojas">
                            <h2 className="mb-0">
                                <button id="informacion-partido-ver-rojas" type="button" className="btn btn-link collapsed" data-toggle="collapse" data-target="#colapsar-rojas">
                                <FontAwesomeIcon icon={faStickyNote}/> Tarjetas rojas</button>
                            </h2>
                        </div>
                        <div id="colapsar-rojas" className="collapse" aria-labelledby="informacion-partido-rojas" data-parent="#informacion-partido-ver">
                            <div id="informacion-partido-estadistica-rojas" className="card-body">
                                <VisualizadorEstadisticaPartido jugadoresLocal={rojasLocal} jugadoresVisitante={rojasVisitante}/>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" id="headingThree">
                            <h2 className="mb-0">
                                <button id="informacion-partido-ver-lesiones" type="button" className="btn btn-link collapsed" data-toggle="collapse" data-target="#colapsar-lesiones">
                                <FontAwesomeIcon icon={faAmbulance}/> Lesiones</button>                     
                            </h2>
                        </div>
                        <div id="colapsar-lesiones" className="collapse" aria-labelledby="headingThree" data-parent="#informacion-partido-ver">
                            <div id="informacion-partido-estadistica-lesiones" className="card-body">
                                <VisualizadorEstadisticaPartido jugadoresLocal={lesionesLocal} jugadoresVisitante={lesionesVisitante}/>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" id="headingThree">
                            <h2 className="mb-0">
                                <button id="informacion-partido-ver-mvp" type="button" className="btn btn-link collapsed" data-toggle="collapse" data-target="#colapsar-mvp">
                                <FontAwesomeIcon icon={faStar}/> MVP</button>                     
                            </h2>
                        </div>
                        <div id="colapsar-mvp" className="collapse" aria-labelledby="headingThree" data-parent="#informacion-partido-ver">
                            <div id="informacion-partido-estadistica-mvp" className="card-body">
                                <VisualizadorEstadisticaPartido jugadoresLocal={mvpLocal} jugadoresVisitante={mvpVisitante}/>
                            </div>
                        </div>
                    </div>
                </div>
          </Modal.Body>
          
          <Modal.Footer className="d-flex justify-content-between">
              <div>
                  {resultado.ultimoUsuarioEditor != null ? <p id="editor-resultado-ultimo-usuario-editor" className="font-italic text-secondary">Actualizado por {resultado.ultimoUsuarioEditor.aliasUsuario}</p> : null}
              </div>
              <Button variant="secondary" onClick={cerrarPopUp}>
                Cerrar
              </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};
export default InformacionPartido;
