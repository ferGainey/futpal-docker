import React, {useEffect, useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faTrash} from '@fortawesome/free-solid-svg-icons';
import ServicioEquipo from "../../../servicios/ServicioEquipo";

const AgregadorEquipo = ({cargarEquipos}) => {

    const [equiposRegistrados, setEquiposRegistrados] = useState([]);
    const [idsEquiposAgregados, setIdsEquiposAgregados] = useState([]);

    useEffect(() => {
        obtenerEquiposDisponibles();
      }, []);

    const obtenerEquiposDisponibles = async () => {
        ServicioEquipo.obtenerEquiposAprobados().then((equipos) => {
            if (equipos != null && equipos.status === 200) {
                setEquiposRegistrados(equipos.data);
            }
        });
    }

    const agregarEquipoALiga = (equipoSeleccionado) => {
        let nuevoArray = [...idsEquiposAgregados];
        nuevoArray.push(equipoSeleccionado);
        setIdsEquiposAgregados(nuevoArray);
        cargarEquipos(nuevoArray);
    }
    
    const obtenerEquipoAPartirId = (idEquipo) => {
        let nombreEquipo = "";
        equiposRegistrados.forEach((equipo) => {
            if (equipo.id == idEquipo) {
                nombreEquipo = equipo.nombreEquipo;
            }
        })
        return nombreEquipo;
    }

    const borrarEquipo = (elemento) => {
        let array = [...idsEquiposAgregados];
        let index = array.indexOf(elemento)
        if (index !== -1) {
            array.splice(index, 1);
            setIdsEquiposAgregados(array);
            cargarEquipos(array);
        }
    }

    return (
          <div className="mt-3">
            <label>Equipos disponibles</label>
            <select id="agregador-equipo-equipos-registrados" className="form-control">
            {equiposRegistrados.map((equipo, indice) => {
                return <option key={indice} value={equipo.id}>{equipo.nombreEquipo}</option>
            })}
            </select>
            <button id="creacion-liga-agregador-equipo-boton-agregar" className="btn btn-secondary btn-sm mt-2" type="button" onClick={() => agregarEquipoALiga(document.getElementById("agregador-equipo-equipos-registrados").value)}>Agregar equipo</button>
            <ul className="list-group mt-3">
                {idsEquiposAgregados.length > 0 ? <label>Equipos agregados:</label> : null}
                {idsEquiposAgregados.map((idEquipo, index) => {
                    return <li className="list-group-item d-flex justify-content-between align-items-center letra-negra" data-value={idEquipo} key={idEquipo}>
                        {obtenerEquipoAPartirId(idEquipo)}
                        <FontAwesomeIcon id={'borrar' + index} className="letra-negra pointer" icon={faTrash} onClick={() => borrarEquipo(idEquipo)} />
                        </li>
            })}
            </ul>
          </div>
          );
};
export default AgregadorEquipo;
