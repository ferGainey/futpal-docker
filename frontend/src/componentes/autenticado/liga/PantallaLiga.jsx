import React, {useEffect, useState} from 'react';
import {useParams} from "react-router";
import BarraNavegacionLogueado from '../BarraNavegacionLogueado.jsx';
import API from '../../../api';
import BarraLateralLiga from './BarraLateralLiga.jsx';

const PantallaLiga = () => {

  let { id, nombre } = useParams();
  const [posiciones, setPosiciones] = useState([]);

  useEffect(() => {
    obtenerPosiciones(id);
  }, []);

  const obtenerPosiciones = async (idLiga) => {
    let respuesta = await servicioObtenerPosicionesLiga(idLiga);
    if (respuesta != null && respuesta.status === 200) {
      if (respuesta.data != null) {
          setPosiciones(respuesta.data);
      }
    }
  }

  const servicioObtenerPosicionesLiga = async (idLiga) => {
    return await API('/api/obtener-posiciones-liga', {
      method: 'get',
      params: {idLiga: idLiga}
    }).catch(error => {
      console.log(error.response);
    });
  }

  return (
          <div>
            <BarraNavegacionLogueado />
            <div className="row ml-0 mr-0">
                <BarraLateralLiga idLiga={id} nombreLiga={nombre} />
                <div className="col-9 col-md-10 col-xl-11 letra-blanca">
                  <h3 id="titulo-pantalla-liga" className="titulo-login">Liga: {nombre}</h3>
                  <table id="tabla-posiciones-liga" className="table table-dark">
                    <thead>
                    <tr>
                      <th id="encabezado-columna-posiciones-liga" scope="col">Posición</th>
                      <th scope="col">Equipo</th>
                      <th id="encabezado-columna-partidos-jugados-liga" scope="col">PJ</th>
                      <th id="encabezado-columna-goles-a-favor-liga" scope="col">GF</th>
                      <th id="encabezado-columna-goles-en-contra-liga" scope="col">GE</th>
                      <th id="encabezado-columna-diferencia-de-goles-liga" scope="col">DG</th>
                      <th id="encabezado-columna-puntos-liga" scope="col">Puntos</th>
                    </tr>
                    </thead>
                    <tbody>
                    {posiciones.map((posicion, indice) => {
                      return <tr className="fila-posiciones-liga" key={posicion.equipo.id}>
                        <td className="columna-numero-posicion">{indice + 1}</td>
                        <td className="columna-nombre-equipo">{posicion.equipo.nombreEquipo}</td>
                        <td className="columna-partidos-jugados">{posicion.partidosJugados}</td>
                        <td className="columna-goles-a-favor">{posicion.golesAFavor}</td>
                        <td className="columna-goles-en-contra">{posicion.golesEnContra}</td>
                        <td className="columna-diferencia-de-goles">{posicion.diferenciaDeGoles}</td>
                        <td className="columna-puntos">{posicion.puntosLiga}</td>
                      </tr>
                    })}
                    </tbody>
                  </table>
                </div>
            </div>
          </div>
          );
};
export default PantallaLiga;
