import React, {useEffect, useState} from 'react';
import API from '../../../api';
import $ from "jquery";
import 'bootstrap';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import EditorEstadistica from './estadisticas/EditorEstadistica.jsx';
import ServicioEstadistica from '../../../servicios/ServicioEstadistica';


const EditorResultado = ({mostrar, setMostrar, resultado}) => {

    const [show, setShow] = useState(false);
    const [golesLocal, setGolesLocal] = useState();
    const [golesVisitante, setGolesVisitante] = useState();
    //agregar aca un use state que sea un objeto estadistica: va con {}. Primero completar con lo que hay del backend.
    const [estadisticas, setEstadisticas] = useState({});
    //Luego pasar el setEstadistica al EditorEstadistica para que cargue el nuevo objeto. También el estadísticas para que tenga los datos
    
    //cuando se le da a actualizar hace primero el llamado para actualizar el resultado y luego al de las estadisticas. Se puede agregar un booleano para que pregunte si debe actualizar estadisticas


    useEffect(() => {
        setShow(mostrar);
        precargarResultado();
        //obtener resultado si existe el partido
    }, [mostrar]);

    const cerrarPopUp = () => {
        setShow(false);
        setMostrar(false);
    }

    const precargarResultado = () => {
        if (resultado.golesEquipoLocal != null) {
          setGolesLocal(resultado.golesEquipoLocal);
        }
        if (resultado.golesEquipoVisitante != null) {
          setGolesVisitante(resultado.golesEquipoVisitante);
        }
    }

    const onSubmit =  async (evento) => {
      if (vericarCamposSeanNoNulos()) {
        evento.preventDefault()

        let usuarioId = null;
        let token = localStorage.getItem("usuario");
        if (token != null) {
          usuarioId = JSON.parse(token).id;
        }

        let requestResultadoPartido = {
          idPartido: resultado.id,
          golesLocal: golesLocal,
          golesVisitante: golesVisitante,
          usuarioEditorId: usuarioId
        };
        await servicioActualizarPartido(requestResultadoPartido).then(async () => {
            await ServicioEstadistica.cargarEstadisticas(estadisticas).then(() => {
                setShow(false);
                setMostrar(false);
            });
        });
      }
    }
  
    const servicioActualizarPartido = async (partido) => {
      return await API('/api/actualizar-partido-liga', {
        method: 'post',
        data: partido
      }).catch(error => {
        console.log(error.response);
      });   
    }
  
    const vericarCamposSeanNoNulos = () => {
      if (golesLocal == null || golesLocal === "" ||
         golesVisitante == null || golesVisitante === "") {
        let alerta = $('#editor-resultado-alerta-error-actualizar-partido');
        alerta.show();
        return false;
      }
      return true;
    }

    return (
      <>
        <Modal
          show={show}
          onHide={cerrarPopUp}
          backdrop="static"
          keyboard={false}
          size="lg"
        >
          <Modal.Header closeButton>
            <Modal.Title>Resultado Partido</Modal.Title>
          </Modal.Header>
          <Modal.Body>              
              <div className="row">
                  <h4 className="col-3 ml-5">{resultado.equipoLocal.nombreEquipo}</h4>
                  <input id="editor-resultado-goles-local" value={golesLocal} className="form-control col-1 ml-4 p-1 text-center h3" type="number" onChange={e => setGolesLocal(e.target.value != "" ? Math.abs(e.target.value) : e.target.value)} min="0" required/>
                  <h3 className="col-1 ml-4">-</h3>                    
                  <input id="editor-resultado-goles-visitante" value={golesVisitante} className="form-control col-1 p-1 text-center h3" type="number" onChange={e => setGolesVisitante(e.target.value != "" ? Math.abs(e.target.value) : e.target.value)} min="0" required/>
                  <h4 className="col-3 mr-5">{resultado.equipoVisitante.nombreEquipo}</h4>
              </div>
            <div id="editor-resultado-alerta-error-actualizar-partido" className="alert alert-danger alert-dismissible collapse alerta-futpal" role="alert">
                <p id="editor-resultado-texto-error-actualizar-partido">No puede haber campos nulos.</p>
                <button type="button" className="close" onClick={() => $('#editor-resultado-alerta-error-actualizar-partido').hide()} aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </Modal.Body>
          <EditorEstadistica resultado={resultado} estadisticas={estadisticas} setEstadisticas={setEstadisticas} golesLocal={golesLocal} golesVisitante={golesVisitante}/>
          
          <Modal.Footer className="d-flex justify-content-between">
            <div>
              {resultado.ultimoUsuarioEditor != null ? <p id="editor-resultado-ultimo-usuario-editor" className="font-italic text-secondary">Actualizado por {resultado.ultimoUsuarioEditor.aliasUsuario}</p> : null}
            </div>
            <div>
              <Button className="mr-2" variant="secondary" onClick={cerrarPopUp}>Cancelar</Button>
              <Button className="mr-2" id="editor-resultado-boton-actualizar" variant="primary" type="submit" onClick={e => onSubmit(e)}>Actualizar</Button>
            </div>
          </Modal.Footer>
        </Modal>
      </>
    );
};
export default EditorResultado;
