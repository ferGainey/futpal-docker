import React from 'react';
import {useHistory} from "react-router-dom";

const BarraLateralLiga = ({idLiga, nombreLiga}) => {

    const history = useHistory();

    const irAPantallaPartidosLiga = () => {
        history.push(`/front/partidos-liga/${idLiga}/${nombreLiga}`);
    }

    const irAPantallaPosiciones = () => {
        history.push(`/front/liga/${idLiga}/${nombreLiga}`);
    }

    const irAPantallaEstadisticasLiga = () => {
        history.push(`/front/estadisticas-liga/${idLiga}/${nombreLiga}`);
    }

    return (
        <div className="col-3 col-md-2 col-xl-1 px-1 bg-dark" id="sticky-sidebar">
            <div className="nav flex-column flex-nowrap overflow-auto">
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="posiciones-liga-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irAPantallaPosiciones()}>Posiciones</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="partidos-liga-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irAPantallaPartidosLiga()}>Partidos</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="estadisticas-liga-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irAPantallaEstadisticasLiga()}>Estadísticas</a>
            </div>
        </div>
    );
};
export default BarraLateralLiga;
