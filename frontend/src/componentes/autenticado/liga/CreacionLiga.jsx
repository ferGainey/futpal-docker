import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import BarraNavegacionLogueado from '../BarraNavegacionLogueado.jsx';
import AgregadorEquipo from './AgregadorEquipo.jsx';
import API from '../../../api';
import $ from "jquery";

const CreacionLiga = () => {

  const [nombreLiga, setNombreLiga] = useState();
  const [esIdaYVuelta, setEsIdaYVuelta] = useState();

  const [equiposAgregados, setEquiposAgregados] = useState([]);
  const history = useHistory();

  useEffect(() => {
    let tokenUsuario = localStorage.getItem("usuario");
    if (tokenUsuario == null) {
      history.push("/");
    }
  }, [history]);

  const crearLiga = async (evento) => {
    evento.preventDefault()
    let nuevaLiga = {
      nombreLiga: nombreLiga,
      idEquipos: equiposAgregados,
      esIdaYVuelta: esIdaYVuelta
    };
    const respuesta = await servicioCargarLiga(nuevaLiga);
    mostrarRespuestaGuardado(respuesta);
  }

  const servicioCargarLiga = async (liga) => {
    return await API('/api/registrar-liga', {
      method: 'post',
      data: liga
    }).catch(error => {
      console.log(error.response);
      mostrarRespuestaErrorAlIntentarRegistrarLiga();
    });
  }

  const mostrarRespuestaGuardado = (respuesta) => {
    if(respuesta != null && respuesta.status === 200) {
      history.push(`/front/liga/${respuesta.data.id}/${respuesta.data.nombre}`);
    }
  }

  const mostrarRespuestaErrorAlIntentarRegistrarLiga = () => {
    let alerta = $('#alerta-error-creacion-liga');
    alerta.show();
  }


  return (
          <div>
            <BarraNavegacionLogueado />
            <div className="letra-blanca">
                <h3 className="titulo-login pt-3">Crear Liga</h3>
                <div className="container">
                  <form onSubmit={(e) => crearLiga(e)}>
                    <div className="form-group">
                      <label>Nombre de la liga</label>
                      <input id="campo-nombre-liga-creacion" className="form-control" type="text" onChange={e => setNombreLiga(e.target.value)} required/>
                    </div>
                    <AgregadorEquipo cargarEquipos={setEquiposAgregados} />
                    <div className="form-check mt-4">
                      <input className="form-check-input pointer" type="checkbox" checked={esIdaYVuelta}
                             onClick={() => setEsIdaYVuelta(!esIdaYVuelta)}/>
                        <label className="form-check-label">Es ida y vuelta</label>
                    </div>
                      <button id="boton-crear-liga" className="btn btn-success confirmacion-liga" type="submit">Crear Liga</button>
                  </form>
                  <div id="alerta-error-creacion-liga" className="alert alert-danger alert-dismissible collapse alerta-futpal" role="alert">
                      <p id="texto-error-crear-liga">Se produjo un error al intentar crear la liga.</p>
                      <button type="button" className="close" onClick={() => $('#alerta-error-creacion-liga').hide()} aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                </div>
            </div>
          </div>
          );
};
export default CreacionLiga;
