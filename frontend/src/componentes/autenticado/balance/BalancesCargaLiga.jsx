import React, {useEffect, useState} from 'react';
import ServicioTemporada from "../../../servicios/ServicioTemporada";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import ServicioLiga from "../../../servicios/ServicioLiga";
import ServicioBalance from "../../../servicios/ServicioBalance";
import $ from "jquery";
import CurrencyFormat from "react-currency-format";

const BalancesCargaLiga = () => {

    const [ligasCombo, setLigasCombo] = useState([]);
    const [equiposLiga, setEquiposLiga] = useState([]);
    const [ligaSeleccionada, setLigaSeleccionada] = useState();

    const LIMITE_TEMPORADAS_A_FUTURO = 10;
    const [temporadasCombo, setTemporadasCombo] = useState([]);
    const [periodosCombo, setPeriodosCombo] = useState([]);
    const [temporadaActual, setTemporadaActual] = useState();
    const [periodoActual, setPeriodoActual] = useState();

    const [balancesCargados, setBalancesCargados] = useState([]);

    useEffect(() => {
        ServicioLiga.obtenerLigasDisponibles().then((respuestaLigas) => {
           setLigasCombo(respuestaLigas.data);
        });
        ServicioTemporada.obtenerTemporada().then((temporadaObtenida) => {
            llenarComboTemporadas(temporadaObtenida.data);
            llenarComboPeriodo();
            setTemporadaActual(temporadaObtenida.data.numero);
            setPeriodoActual(temporadaObtenida.data.periodoTemporada);
        });
    }, []);

    useEffect(() => {
        if (ligaSeleccionada != null) {
            ServicioLiga.obtenerEquipos(ligaSeleccionada).then((respuestaEquipos) => {
                setEquiposLiga(respuestaEquipos.data)
                llenarBalancesIniciales(respuestaEquipos.data);
            });
        }
    }, [ligaSeleccionada]);

    const llenarComboTemporadas = (temporadaActual) => {
        let temporadasParaCombo = [];
        for (let i = temporadaActual.numero; i <= temporadaActual.numero + LIMITE_TEMPORADAS_A_FUTURO; i++) {
            let temporada = {
                valor: i,
                descripcion: "Temp. " + i
            }
            temporadasParaCombo.push(temporada);
        }
        setTemporadasCombo(temporadasParaCombo);
    }

    const llenarComboPeriodo = () => {
        let periodosParaCombo = [];
        let principio = {
            id: 1,
            descripcion: "Principio"
        }
        periodosParaCombo.push(principio);
        let mitad = {
            id: 2,
            descripcion: "Mitad"
        }
        periodosParaCombo.push(mitad);
        let final = {
            id: 3,
            descripcion: "Final"
        }
        periodosParaCombo.push(final);
        setPeriodosCombo(periodosParaCombo);
    }

    const llenarBalancesIniciales = (equipos) => {
        let balancesIniciales = [];

        equipos.forEach(equipo => {
            let balance = {
                detalle: "",
                equipoNombre: equipo.nombreEquipo,
                equipoId: equipo.id,
                monto: 0,
                numeroTemporada: null,
                periodoId: null
            }
            balancesIniciales.push(balance)
        });

        setBalancesCargados(balancesIniciales);
    }

    const cargarBalances = () => {
        $('#boton-cargar-balance').prop('disabled', true);
        ServicioBalance.cargarBalancesParaLiga(balancesCargados).then(() => {
            setBalancesCargados([]);
            $('#alerta-balance-cargado').show();
            $('#boton-cargar-balance').prop('disabled', false);
            llenarBalancesIniciales(equiposLiga);
        }, () => {
            $('#alerta-balance-cargado-error').show();
            $('#boton-cargar-balance').prop('disabled', false);
        });
    }

    const esTemporadaActual = (temporada, balance) => {
        if (temporada.valor === temporadaActual) {
            balance.numeroTemporada = temporadaActual;
            return true;
        }
        return false;
    }

    const esPeriodoActual = (periodo, balance) => {
        if (periodo.id === periodoActual.id) {
            balance.periodoId = periodoActual.id;
            return true;
        }
        return false;
    }

    const borrarBalance = (balance) => {
        let array = [...balancesCargados];
        let index = array.indexOf(balance)
        if (index !== -1) {
            array.splice(index, 1);
            setBalancesCargados(array);
        }
    }

    return (
        <>
            <div className="letra-blanca col-9 col-md-10 col-xl-11">
                <h3 className="titulo-login mb-5 pt-3">Cargar balances de una liga</h3>
                <div className="col-md-12">
                    <div className="d-flex justify-content-center">
                        <select id="carga-balance-ligas-disponibles" className="form-control mt-4 mb-4 col-md-5" onChange={e => setLigaSeleccionada(e.target.value)}>
                            <option key="placeholder" disabled selected>Seleccionar...</option>
                            {ligasCombo.map((liga, indice) => {
                                return <option key={indice} value={liga.id}>{liga.nombre}</option>
                            })}
                        </select>
                    </div>

                    <table id="tabla-carga-balances-liga" className="table table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Detalle</th>
                            <th scope="col">Equipo</th>
                            <th scope="col">Monto</th>
                            <th scope="col">Temporada</th>
                            <th scope="col">Periodo</th>
                            <th scope="col"/>
                        </tr>
                        </thead>
                        <tbody>
                            <>
                                {balancesCargados.map((balance, indice) => {
                                    return <tr className="fila-carga-balances-liga" key={indice}>
                                        <td>
                                            <input id="balances-detalle" className="form-control" onChange={e => balance.detalle = e.target.value}/>
                                        </td>
                                        <td>{balance.equipoNombre}</td>
                                        <td>
                                            <CurrencyFormat id="balances-monto" className="carga-balances-liga-monto form-control" thousandSeparator={'.'}
                                                            decimalSeparator={','} decimalScale={0} prefix={'$'} onValueChange={value => balance.monto = value.floatValue}
                                                            placeholder="Ingrese monto" required/>
                                        </td>
                                        <td>
                                            <select id="balances-temporadas" className="form-control" onChange={e => balance.numeroTemporada = e.target.value} required>
                                                {temporadasCombo.map((temporada, indice) => {
                                                    return <option key={indice} value={temporada.valor} selected={esTemporadaActual(temporada, balance)}>{temporada.descripcion}</option>
                                                })}
                                            </select>
                                        </td>
                                        <td>
                                            <select id="balances-periodos" className="form-control" onChange={e => balance.periodoId = e.target.value} required>
                                                {periodosCombo.map((periodo, indice) => {
                                                    return <option key={indice} value={periodo.id} selected={esPeriodoActual(periodo, balance)}>{periodo.descripcion}</option>
                                                })}
                                            </select>
                                        </td>
                                        <td>
                                            <FontAwesomeIcon className="pointer" icon={faTrash} onClick={() => borrarBalance(balance)}/>
                                        </td>
                                    </tr>
                                })}
                            </>
                        </tbody>
                    </table>
                    <div className="d-flex justify-content-center mt-4">
                        <button id="boton-cargar-balance" className="btn btn-success" type="submit" onClick={() => cargarBalances()}>Cargar</button>
                    </div>
                    <div id="alerta-balance-cargado" className="alert alert-success alert-dismissible collapse alerta-futpal" role="alert">
                        <p id="texto-mensaje-carga-exitosa-balance">Los balances han sido cargados exitosamente.</p>
                        <button type="button" className="close" onClick={() => $('#alerta-balance-cargado').hide()} aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="alerta-balance-cargado-error" className="alert alert-danger alert-dismissible collapse alerta-futpal" role="alert">
                        <p id="texto-mensaje-carga-erronea-balance">No se pudieron cargar los balances.</p>
                        <button type="button" className="close" onClick={() => $('#alerta-balance-cargado-error').hide()} aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};
export default BalancesCargaLiga;
