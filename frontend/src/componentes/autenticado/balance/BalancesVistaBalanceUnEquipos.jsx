import React, {useEffect, useState} from 'react';
import ServicioTemporada from "../../../servicios/ServicioTemporada";
import ServicioBalance from "../../../servicios/ServicioBalance";
import ServicioEquipo from "../../../servicios/ServicioEquipo";
import $ from "jquery";
import CurrencyFormat from "react-currency-format";

const BalancesVistaBalancesUnEquipo = () => {

    const [detalleBalanceEquipo, setDetalleBalancesEquipo] = useState();
    const [balancesEquipo, setBalancesEquipo] = useState([]);
    const [equipoDelUsuarioId, setEquipoDelUsuarioId] = useState();
    const [equipoSeleccionadoId, setEquipoSeleccionadoId] = useState();
    const [equiposCombo, setEquiposCombo] = useState([]);

    const LIMITE_TEMPORADAS_A_FUTURO = 10;
    const [temporadasCombo, setTemporadasCombo] = useState([]);
    const [temporadaMostrada, setTemporadaMostrada] = useState();
    const [numeroTemporadaActual, setNumeroTemporadaActual] = useState();


    useEffect(() => {
        let tokenUsuario = localStorage.getItem("usuario");
        let idUsuarioToken = JSON.parse(tokenUsuario).id;

        ServicioTemporada.obtenerTemporada().then((temporadaObtenida) => {
            llenarComboTemporadas(temporadaObtenida.data);
            setNumeroTemporadaActual(temporadaObtenida.data.numero);

            ServicioEquipo.obtenerDatosEquipo(idUsuarioToken).then((respuestaDatosEquipo) => {
                if (respuestaDatosEquipo.data.idEquipo != null) {
                    setEquipoDelUsuarioId(respuestaDatosEquipo.data.idEquipo);
                    setEquipoSeleccionadoId(respuestaDatosEquipo.data.idEquipo);

                    ServicioBalance.obtenerDetalleBalanceUnEquipo(temporadaObtenida.data.numero, respuestaDatosEquipo.data.idEquipo).then((respuestaDetalleBalanceEquipo) => {
                        setDetalleBalancesEquipo(respuestaDetalleBalanceEquipo.data);
                    });

                    ServicioBalance.obtenerBalancesUnEquipoEnUnaTemporada(temporadaObtenida.data.numero, respuestaDatosEquipo.data.idEquipo).then(
                        (respuestaBalancesUnEquipo) => {
                            setBalancesEquipo(respuestaBalancesUnEquipo.data);
                        }
                    );
                }
                else {
                    $('#balances-equipos-aprobados').prop('selectedIndex', 0);
                }
                ServicioEquipo.obtenerEquiposAprobados().then((respuestaEquiposAprobados) => {
                    llenarComboEquipos(respuestaEquiposAprobados.data);
                });
            })
        });
    }, []);

    useEffect(() => {
        if (temporadaMostrada && equipoSeleccionadoId) {
            ServicioBalance.obtenerDetalleBalanceUnEquipo(temporadaMostrada, equipoSeleccionadoId).then((respuestaDetalleBalanceEquipo) => {
                setDetalleBalancesEquipo(respuestaDetalleBalanceEquipo.data);
            });
            ServicioBalance.obtenerBalancesUnEquipoEnUnaTemporada(temporadaMostrada, equipoSeleccionadoId).then(
                (respuestaBalancesUnEquipo) => {
                    setBalancesEquipo(respuestaBalancesUnEquipo.data);
                }
            );
        }
    }, [temporadaMostrada, equipoSeleccionadoId]);

    const llenarComboTemporadas = (temporadaActual) => {
        let temporadasParaCombo = [];
        for (let i = 1; i <= temporadaActual.numero + LIMITE_TEMPORADAS_A_FUTURO; i++) {
            let temporada = {
                valor: i,
                descripcion: "Temp. " + i
            }
            temporadasParaCombo.push(temporada);
        }
        setTemporadasCombo(temporadasParaCombo);
        setTemporadaMostrada(temporadaActual.numero);
    }

    const llenarComboEquipos = (equipos) => {
        let equiposParaCombo = [];
        equipos.forEach(equipo => {
            equiposParaCombo.push(equipo);
        });
        setEquiposCombo(equiposParaCombo);
    }

    const aplicarEstiloPositivoONegativo = (valor) => {
        if (valor >= 0) {
            return 'total-positivo';
        }
        return 'total-negativo';
    }

    const esTemporadaActual = (temporada) => {
        return temporada.valor === numeroTemporadaActual;
    }

    const esEquipoDelUsuario = (equipo) => {
        return equipo.id === equipoDelUsuarioId;
    }

    return (
        <>
            <div className="letra-blanca col-9 col-md-10 col-xl-11">
                <h3 className="titulo-login mb-5 pt-3">Balances del equipo</h3>
                <div className="col-md-12">

                    <div className="d-flex justify-content-center">
                        <select id="balances-temporadas" className="form-control mt-4 mb-4 col-md-3 mr-1 mr-md-3" onChange={e => setTemporadaMostrada(e.target.value)}>
                            <option key="placeholder" disabled>Seleccionar...</option>
                            {temporadasCombo.map((temporada, indice) => {
                                return <option key={indice} value={temporada.valor} selected={esTemporadaActual(temporada)}>{temporada.descripcion}</option>
                            })}
                        </select>
                        <select id="balances-equipos-aprobados" className="form-control mt-4 mb-4 col-md-3 ml-1 ml-md-3" onChange={e => setEquipoSeleccionadoId(e.target.value)}>
                            <option key="placeholder" disabled>Seleccionar...</option>
                            {equiposCombo.map((equipo, indice) => {
                                return <option key={indice} value={equipo.id} selected={esEquipoDelUsuario(equipo)}>{equipo.nombreEquipo}</option>
                            })}
                        </select>
                    </div>
                    {detalleBalanceEquipo && equipoSeleccionadoId ?
                        <div>
                            <table id="tabla-carga-balances-liga" className="table table-dark">
                                <thead>
                                <tr>
                                    <th scope="col" colSpan="3">Gastos</th>
                                    <th scope="col" colSpan="3">Ingresos</th>
                                    <th scope="col" colSpan="3">Total</th>
                                </tr>
                                <tr>
                                    <td>Principio</td>
                                    <td>Mitad</td>
                                    <td>Final</td>
                                    <td>Principio</td>
                                    <td>Mitad</td>
                                    <td>Final</td>
                                    <td>Principio</td>
                                    <td>Mitad</td>
                                    <td>Final</td>
                                </tr>
                                </thead>
                                <tbody>
                                <>
                                    <tr className="fila-carga-balances-liga">
                                        <td id={detalleBalanceEquipo.equipo.nombreEquipo + 'gastos-principio'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalanceEquipo.gastosPeriodoPrincipio}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalanceEquipo.equipo.nombreEquipo + 'gastos-mitad'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalanceEquipo.gastosPeriodoMitad}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalanceEquipo.equipo.nombreEquipo + 'gastos-final'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalanceEquipo.gastosPeriodoFinal}
                                                            displayType={"text"} required/>
                                        </td>

                                        <td id={detalleBalanceEquipo.equipo.nombreEquipo + 'ingresos-principio'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalanceEquipo.ingresosPeriodoPrincipio}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalanceEquipo.equipo.nombreEquipo + 'ingresos-mitad'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalanceEquipo.ingresosPeriodoMitad}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalanceEquipo.equipo.nombreEquipo + 'ingresos-final'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalanceEquipo.ingresosPeriodoFinal}
                                                            displayType={"text"} required/>
                                        </td>

                                        <td id={detalleBalanceEquipo.equipo.nombreEquipo + 'total-principio'} className={aplicarEstiloPositivoONegativo(detalleBalanceEquipo.totalPeriodoPrincipio)}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalanceEquipo.totalPeriodoPrincipio}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalanceEquipo.equipo.nombreEquipo + 'total-mitad'} className={aplicarEstiloPositivoONegativo(detalleBalanceEquipo.totalPeriodoMitad)}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalanceEquipo.totalPeriodoMitad}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalanceEquipo.equipo.nombreEquipo + 'total-final'} className={aplicarEstiloPositivoONegativo(detalleBalanceEquipo.totalPeriodoFinal)}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalanceEquipo.totalPeriodoFinal}
                                                            displayType={"text"} required/>
                                        </td>
                                    </tr>
                                </>
                                </tbody>
                            </table>

                            <h5 className="ml-2 mt-5">Transacciones</h5>
                            <table className="table table-dark">
                                <thead>
                                <tr>
                                    <th scope="col">Detalle</th>
                                    <th scope="col">Monto</th>
                                    <th scope="col">Periodo</th>
                                </tr>
                                </thead>
                                <tbody>
                                <>
                                    {balancesEquipo.map((balance, indice) => {
                                        return <tr key={indice}>
                                            <td id={"detalle" + indice}>
                                                {balance.detalle}
                                            </td>
                                            <td id={"monto" + indice}>
                                                <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                                decimalScale={0} prefix={'$'} value={balance.monto}
                                                                displayType={"text"} required/>
                                            </td>
                                            <td id={"periodo" + indice}>
                                                {balance.periodo.nombre}
                                            </td>
                                        </tr>
                                    })}
                                </>
                                </tbody>
                            </table>
                        </div>
                        : null
                    }

                </div>
            </div>
        </>
    );
}
export default BalancesVistaBalancesUnEquipo;
