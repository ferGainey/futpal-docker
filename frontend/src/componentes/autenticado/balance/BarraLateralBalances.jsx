import React, {useEffect} from 'react';
import {useHistory} from "react-router-dom";
import $ from "jquery";

const BarraLateralBalances = ({setBalancesCargaUnEquipoActivo, setBalancesCargaUnaLigaActivo, setBalancesVistaTodosLosEquipos, setBalancesVistaUnEquipoActivo, setBalancesCargaSalariosActivo, setBalancesVistaSalariosActivo}) => {

    const history = useHistory();

    useEffect(() => {
        let tokenUsuario = localStorage.getItem("usuario");
        if (tokenUsuario == null) {
            history.push("/");
        }
        else {
            let rolesUsuarioLogueado = JSON.parse(localStorage.getItem("usuario")).roles;
            if (!rolesUsuarioLogueado.includes("ROL_ADMINISTRADOR")) {
                ocultarCargasDeBalances();
            }
        }
    }, [history]);

    const ocultarCargasDeBalances = () => {
        $('#carga-balance-un-equipo-side-nav').css("display", "none");
        $('#carga-balance-liga-side-nav').css("display", "none");
        $('#carga-salarios-side-nav').css("display", "none");
    }

    const irABalancesCargaUnEquipo = () => {
        setBalancesCargaUnaLigaActivo(false);
        setBalancesVistaTodosLosEquipos(false);
        setBalancesCargaSalariosActivo(false);
        setBalancesVistaUnEquipoActivo(false);
        setBalancesCargaUnEquipoActivo(true);
        setBalancesVistaSalariosActivo(false);
    }

    const irABalancesCargaLiga = () => {
        setBalancesCargaUnEquipoActivo(false);
        setBalancesVistaTodosLosEquipos(false);
        setBalancesCargaSalariosActivo(false);
        setBalancesVistaUnEquipoActivo(false);
        setBalancesCargaUnaLigaActivo(true);
        setBalancesVistaSalariosActivo(false);
    }

    const irABalancesTodosLosEquipos = () => {
        setBalancesCargaUnEquipoActivo(false);
        setBalancesCargaUnaLigaActivo(false);
        setBalancesCargaSalariosActivo(false);
        setBalancesVistaUnEquipoActivo(false);
        setBalancesVistaTodosLosEquipos(true);
        setBalancesVistaSalariosActivo(false);
    }

    const irABalancesDeUnEquipos = () => {
        setBalancesCargaUnEquipoActivo(false);
        setBalancesCargaUnaLigaActivo(false);
        setBalancesCargaSalariosActivo(false);
        setBalancesVistaTodosLosEquipos(false);
        setBalancesVistaUnEquipoActivo(true);
        setBalancesVistaSalariosActivo(false);
    }

    const irABalancesCargaSalarios = () => {
        setBalancesCargaUnEquipoActivo(false);
        setBalancesCargaUnaLigaActivo(false);
        setBalancesCargaSalariosActivo(true);
        setBalancesVistaTodosLosEquipos(false);
        setBalancesVistaUnEquipoActivo(false);
        setBalancesVistaSalariosActivo(false);
    }

    const irABalancesVerSalarios = () => {
        setBalancesCargaUnEquipoActivo(false);
        setBalancesCargaUnaLigaActivo(false);
        setBalancesCargaSalariosActivo(false);
        setBalancesVistaTodosLosEquipos(false);
        setBalancesVistaUnEquipoActivo(false);
        setBalancesVistaSalariosActivo(true);
    }

    return (
        <div className="col-3 col-md-2 col-xl-1 px-1 bg-dark" id="sticky-sidebar">
            <div className="nav flex-column flex-nowrap overflow-auto">
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="carga-balance-un-equipo-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irABalancesCargaUnEquipo()}>Carga para un equipo</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="carga-balance-liga-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irABalancesCargaLiga()}>Carga para una liga</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="carga-salarios-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irABalancesCargaSalarios()}>Carga de salarios</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="ver-balances-uno-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irABalancesDeUnEquipos()}>Balances de un equipo</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="ver-balances-todos-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irABalancesTodosLosEquipos()}>Balances todos los equipos</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="ver-salarios-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irABalancesVerSalarios()}>Salarios equipos</a>
            </div>
        </div>
    );
};
export default BarraLateralBalances;
