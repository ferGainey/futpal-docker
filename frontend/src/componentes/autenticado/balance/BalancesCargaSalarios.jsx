import React, {useEffect, useState} from 'react';
import ServicioTemporada from "../../../servicios/ServicioTemporada";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import ServicioLiga from "../../../servicios/ServicioLiga";
import ServicioBalance from "../../../servicios/ServicioBalance";
import $ from "jquery";
import CurrencyFormat from "react-currency-format";

const BalancesCargaSalarios = () => {

    const [ligasCombo, setLigasCombo] = useState([]);
    const [ligaSeleccionada, setLigaSeleccionada] = useState();

    const [temporadaActual, setTemporadaActual] = useState();
    const [periodoActual, setPeriodoActual] = useState();

    const [balancesSalarios, setBalancesSalarios] = useState([]);

    useEffect(() => {
        ServicioLiga.obtenerLigasDisponibles().then((respuestaLigas) => {
           setLigasCombo(respuestaLigas.data);
        });
        ServicioTemporada.obtenerTemporada().then((temporadaObtenida) => {
            setTemporadaActual(temporadaObtenida.data.numero);
            setPeriodoActual(temporadaObtenida.data.periodoTemporada);
        });
    }, []);

    useEffect(() => {
        if (ligaSeleccionada != null) {
            ServicioBalance.obtenerSalarios(ligaSeleccionada).then((balancesSalariosLiga) => {
                setBalancesSalarios(balancesSalariosLiga.data);
            });
        }
    }, [ligaSeleccionada]);

    const cargarBalances = () => {
        $('#boton-cargar-balance').prop('disabled', true);
        let balancesParaCarga = [];
        balancesSalarios.forEach(balance => {
            let balanceParaCarga = {
                detalle: "Salarios",
                equipoNombre: balance.nombreEquipo,
                equipoId: balance.idEquipo,
                monto: -balance.monto,
                numeroTemporada: temporadaActual,
                periodoId: periodoActual.id
            }
            balancesParaCarga.push(balanceParaCarga);
        })
        ServicioBalance.cargarBalancesParaLiga(balancesParaCarga).then(() => {
            setBalancesSalarios([]);
            $('#alerta-balance-cargado').show();
            $('#boton-cargar-balance').prop('disabled', false);
        }, () => {
            $('#alerta-balance-cargado-error').show();
            $('#boton-cargar-balance').prop('disabled', false);
        });
    }

    const borrarBalance = (balance) => {
        let array = [...balancesSalarios];
        let index = array.indexOf(balance)
        if (index !== -1) {
            array.splice(index, 1);
            setBalancesSalarios(array);
        }
    }

    return (
        <>
            <div className="letra-blanca col-9 col-md-10 col-xl-11">
                <h3 className="titulo-login mb-5 pt-3">Cargar balances de una liga</h3>
                <div className="col-md-12">
                    <div className="d-flex justify-content-center">
                        <select id="carga-balance-ligas-disponibles" className="form-control mt-4 mb-4 col-md-5" onChange={e => setLigaSeleccionada(e.target.value)}>
                            <option key="placeholder" disabled selected>Seleccionar...</option>
                            {ligasCombo.map((liga, indice) => {
                                return <option key={indice} value={liga.id}>{liga.nombre}</option>
                            })}
                        </select>
                    </div>

                    <table id="tabla-carga-balances-liga" className="table table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Equipo</th>
                            <th scope="col">Monto</th>
                            <th scope="col">Temporada</th>
                            <th scope="col">Periodo</th>
                            <th scope="col"/>
                        </tr>
                        </thead>
                        <tbody>
                            <>
                                {balancesSalarios.map((balance, indice) => {
                                    return <tr className="fila-carga-balances-liga" key={indice}>
                                        <td>{balance.nombreEquipo}</td>
                                        <td>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={balance.monto}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td>
                                            <select id="balances-temporadas" className="form-control" disabled={true} defaultValue='TEMP_ACTUAL'>
                                                <option key='TEMP_ACTUAL' selected={true}>{temporadaActual}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="balances-periodos" className="form-control" disabled={true} defaultValue='PERIODO_ACTUAL'>
                                                <option key='PERIODO_ACTUAL' selected={true}>{periodoActual.nombre}</option>
                                            </select>
                                        </td>
                                        <td>
                                            <FontAwesomeIcon className="pointer" icon={faTrash} onClick={() => borrarBalance(balance)}/>
                                        </td>
                                    </tr>
                                })}
                            </>
                        </tbody>
                    </table>
                    <div className="d-flex justify-content-center mt-4">
                        <button id="boton-cargar-balance" className="btn btn-success" type="submit" onClick={() => cargarBalances()}>Cargar</button>
                    </div>
                    <div id="alerta-balance-cargado" className="alert alert-success alert-dismissible collapse alerta-futpal" role="alert">
                        <p id="texto-mensaje-carga-exitosa-balance">Los salarios han sido cargados exitosamente.</p>
                        <button type="button" className="close" onClick={() => $('#alerta-balance-cargado').hide()} aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="alerta-balance-cargado-error" className="alert alert-danger alert-dismissible collapse alerta-futpal" role="alert">
                        <p id="texto-mensaje-carga-erronea-balance">No se pudieron cargar los salarios.</p>
                        <button type="button" className="close" onClick={() => $('#alerta-balance-cargado-error').hide()} aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};
export default BalancesCargaSalarios;
