import React, {useEffect, useState} from 'react';
import ServicioLiga from "../../../servicios/ServicioLiga";
import ServicioBalance from "../../../servicios/ServicioBalance";
import CurrencyFormat from "react-currency-format";

const BalancesVistaSalariosLiga = () => {

    const [ligasCombo, setLigasCombo] = useState([]);
    const [ligaSeleccionada, setLigaSeleccionada] = useState();

    const [balancesSalarios, setBalancesSalarios] = useState([]);

    useEffect(() => {
        ServicioLiga.obtenerLigasDisponibles().then((respuestaLigas) => {
            setLigasCombo(respuestaLigas.data);
        });
    }, []);

    useEffect(() => {
        if (ligaSeleccionada != null) {
            ServicioBalance.obtenerSalarios(ligaSeleccionada).then((balancesSalariosLiga) => {
                setBalancesSalarios(balancesSalariosLiga.data);
            });
        }
    }, [ligaSeleccionada]);

    return (
        <>
            <div className="letra-blanca col-9 col-md-10 col-xl-11">
                <h3 className="titulo-login mb-5 pt-3">Cargar balances de una liga</h3>
                <div className="col-md-12">
                    <div className="d-flex justify-content-center">
                        <select id="carga-balance-ligas-disponibles" className="form-control mt-4 mb-4 col-md-5" onChange={e => setLigaSeleccionada(e.target.value)}>
                            <option key="placeholder" disabled selected>Seleccionar...</option>
                            {ligasCombo.map((liga, indice) => {
                                return <option key={indice} value={liga.id}>{liga.nombre}</option>
                            })}
                        </select>
                    </div>

                    <table id="tabla-carga-balances-liga" className="table table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Equipo</th>
                            <th scope="col">Monto</th>
                        </tr>
                        </thead>
                        <tbody>
                        <>
                            {balancesSalarios.map((balance, indice) => {
                                return <tr className="fila-carga-balances-liga" key={indice}>
                                    <td id={'nombre' + balance.nombreEquipo}>{balance.nombreEquipo}</td>
                                    <td id={'monto' + balance.nombreEquipo}>
                                        <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                        decimalScale={0} prefix={'$'} value={balance.monto}
                                                        displayType={"text"} required/>
                                    </td>
                                </tr>
                            })}
                        </>
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
};
export default BalancesVistaSalariosLiga;
