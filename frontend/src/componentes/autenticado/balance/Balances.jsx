import React, {useEffect, useState} from 'react';
import BarraNavegacionLogueado from '../BarraNavegacionLogueado.jsx';
import BarraLateralBalances from "./BarraLateralBalances";
import BalancesCargaUnEquipo from "./BalancesCargaUnEquipo";
import BalancesCargaLiga from "./BalancesCargaLiga";
import BalancesVistaBalancesTodosLosEquipos from "./BalancesVistaBalancesTodosLosEquipos";
import BalancesVistaBalancesUnEquipo from "./BalancesVistaBalanceUnEquipos";
import BalancesCargaSalarios from "./BalancesCargaSalarios";
import BalancesVistaSalariosLiga from "./BalancesVistaSalariosLiga";

const Balances = () => {

    const [balancesCargaUnEquipoActivo, setBalancesCargaUnEquipoActivo] = useState(false);
    const [balancesCargaUnaLigaActivo, setBalancesCargaUnaLigaActivo] = useState(false);
    const [balancesVistaTodosLosEquiposActivo, setBalancesVistaTodosLosEquiposActivo] = useState(false);
    const [balancesVistaUnEquipoActivo, setBalancesVistaUnEquipoActivo] = useState(false);
    const [balancesCargaSalariosActivo, setBalancesCargaSalariosActivo] = useState(false);
    const [balancesVistaSalariosActivo, setBalancesVistaSalariosActivo] = useState(false);

    useEffect(() => {
        setBalancesVistaUnEquipoActivo(true);
    }, []);


    return (
        <>
            <BarraNavegacionLogueado/>
            <div className="row ml-0 mr-0">
                <BarraLateralBalances setBalancesCargaUnEquipoActivo={setBalancesCargaUnEquipoActivo}
                                      setBalancesCargaUnaLigaActivo={setBalancesCargaUnaLigaActivo}
                                      setBalancesVistaTodosLosEquipos={setBalancesVistaTodosLosEquiposActivo}
                                      setBalancesVistaUnEquipoActivo={setBalancesVistaUnEquipoActivo}
                                      setBalancesCargaSalariosActivo={setBalancesCargaSalariosActivo}
                                      setBalancesVistaSalariosActivo={setBalancesVistaSalariosActivo}/>
                {balancesVistaTodosLosEquiposActivo ? <BalancesVistaBalancesTodosLosEquipos/> : null}
                {balancesCargaUnEquipoActivo ? <BalancesCargaUnEquipo/> : null}
                {balancesCargaUnaLigaActivo ? <BalancesCargaLiga/> : null}
                {balancesVistaUnEquipoActivo ? <BalancesVistaBalancesUnEquipo/> : null}
                {balancesCargaSalariosActivo ? <BalancesCargaSalarios/> : null}
                {balancesVistaSalariosActivo ? <BalancesVistaSalariosLiga/> : null}
            </div>
        </>
    );
};
export default Balances;
