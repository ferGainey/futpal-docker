import React, {useEffect, useState} from 'react';
import ServicioEquipo from "../../../servicios/ServicioEquipo";
import ServicioTemporada from "../../../servicios/ServicioTemporada";
import ServicioBalance from "../../../servicios/ServicioBalance";
import $ from "jquery";
import CurrencyFormat from "react-currency-format";

const BalancesCargaUnEquipo = () => {

    const LIMITE_TEMPORADAS_A_FUTURO = 10;
    const [equiposAprobados, setEquiposAprobados] = useState([]);
    const [temporadasCombo, setTemporadasCombo] = useState([]);
    const [periodosCombo, setPeriodosCombo] = useState([]);

    const [detalle, setDetalle] = useState("");
    const [equipoId, setEquipoId] = useState();
    const [monto, setMonto] = useState();
    const [numeroTemporada, setNumeroTemporada] = useState();
    const [periodoId, setPeriodoId] = useState();

    useEffect(() => {
        ServicioTemporada.obtenerTemporada().then((temporadaObtenida) => {
            llenarComboTemporadas(temporadaObtenida.data);
            llenarComboPeriodo();
        });
        obtenerEquiposDisponibles();
    }, []);

    const llenarComboTemporadas = (temporadaActual) => {
        let temporadasParaCombo = [];
        for (let i = temporadaActual.numero; i <= temporadaActual.numero + LIMITE_TEMPORADAS_A_FUTURO; i++) {
            let temporada = {
                valor: i,
                descripcion: "Temp. " + i
            }
            temporadasParaCombo.push(temporada);
        }
        setTemporadasCombo(temporadasParaCombo);
    }

    const llenarComboPeriodo = () => {
        let periodosParaCombo = [];
        let principio = {
            id: 1,
            descripcion: "Principio"
        }
        periodosParaCombo.push(principio);
        let mitad = {
            id: 2,
            descripcion: "Mitad"
        }
        periodosParaCombo.push(mitad);
        let final = {
            id: 3,
            descripcion: "Final"
        }
        periodosParaCombo.push(final);
        setPeriodosCombo(periodosParaCombo);
    }

    const obtenerEquiposDisponibles = () => {
        ServicioEquipo.obtenerEquiposAprobados().then((equipos) => {
            if (equipos != null && equipos.status === 200) {
                setEquiposAprobados(equipos.data);
            }
        });
    }

    const cargarBalance = () => {
        $('#boton-cargar-balance').prop('disabled', true);

        let balance = {
            detalle: detalle,
            equipoId: equipoId,
            monto: monto,
            numeroTemporada: numeroTemporada != null ? numeroTemporada : temporadasCombo[0].valor,
            periodoId: periodoId != null ? periodoId : periodosCombo[0].id
        }
        ServicioBalance.cargarBalance(balance).then(() => {
            $('#alerta-balance-cargado').show();
            $('#boton-cargar-balance').prop('disabled', false);
            reiniciarSelecciones();
        }, () => {
            $('#alerta-balance-cargado-error').show();
            $('#boton-cargar-balance').prop('disabled', false);
        });
    }

    const reiniciarSelecciones = () => {
        $('#balances-temporadas').val(temporadasCombo[0].valor);
        $('#balances-periodos').val(periodosCombo[0].id);
        $('#balances-equipos-aprobados').prop('selectedIndex', 0);
        setMonto("");
        setDetalle("");
    }

    return (
        <>
            <div className="letra-blanca col-9 col-md-10 col-xl-11">
                <h3 className="titulo-login mb-5 pt-3">Editar Equipo</h3>
                <div className="col-md-12">
                    <div className="form-inline d-flex justify-content-center">
                        <div className="form-group mr-4 flex-column">
                            <p>Detalle</p>
                            <input id="balances-detalle" className="form-control" value={detalle} onChange={e => setDetalle(e.target.value)}/>
                        </div>
                        <div className="form-group mr-4 flex-column">
                            <p>Equipo</p>
                            <select id="balances-equipos-aprobados" className="form-control" onChange={e => setEquipoId(e.target.value)} required>
                                <option key="placeholder" disabled selected>Seleccionar...</option>
                                {equiposAprobados.map((equipo, indice) => {
                                    return <option key={indice} value={equipo.id}>{equipo.nombreEquipo}</option>
                                })}
                            </select>
                        </div>
                        <div className="form-group mr-4 flex-column">
                            <p>Monto</p>
                            <CurrencyFormat id="balances-monto" value={monto} className="form-control" thousandSeparator={'.'}
                                            decimalSeparator={','} decimalScale={0} prefix={'$'} onValueChange={value => setMonto(value.floatValue)}
                                            placeholder="Ingrese monto" required/>
                        </div>
                        <div className="form-group mr-4 flex-column">
                            <p>Temporada</p>
                            <select id="balances-temporadas" className="form-control" onChange={e => setNumeroTemporada(e.target.value)} required>
                                {temporadasCombo.map((temporada, indice) => {
                                    return <option key={indice} value={temporada.valor}>{temporada.descripcion}</option>
                                })}
                            </select>
                        </div>
                        <div className="form-group flex-column">
                            <p>Periodo</p>
                            <select id="balances-periodos" className="form-control" onChange={e => setPeriodoId(e.target.value)} required>
                                {periodosCombo.map((periodo, indice) => {
                                    return <option key={indice} value={periodo.id}>{periodo.descripcion}</option>
                                })}
                            </select>
                        </div>
                    </div>
                    <div className="d-flex justify-content-center mt-4">
                        <button id="boton-cargar-balance" className="btn btn-success" type="submit" onClick={() => cargarBalance()}>Cargar</button>
                    </div>
                    <div id="alerta-balance-cargado" className="alert alert-success alert-dismissible collapse alerta-futpal" role="alert">
                        <p id="texto-mensaje-carga-exitosa-balance">El balance ha sido cargado exitosamente.</p>
                        <button type="button" className="close" onClick={() => $('#alerta-balance-cargado').hide()} aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="alerta-balance-cargado-error" className="alert alert-danger alert-dismissible collapse alerta-futpal" role="alert">
                        <p id="texto-mensaje-carga-erronea-balance">No se pudieron cargar los balances.</p>
                        <button type="button" className="close" onClick={() => $('#alerta-balance-cargado-error').hide()} aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};
export default BalancesCargaUnEquipo;
