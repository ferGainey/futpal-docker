import React, {useEffect, useState} from 'react';
import ServicioTemporada from "../../../servicios/ServicioTemporada";
import ServicioBalance from "../../../servicios/ServicioBalance";
import CurrencyFormat from "react-currency-format";

const BalancesVistaBalancesTodosLosEquipos = () => {

    const [balanceEquipos, setBalancesEquipos] = useState([]);

    const LIMITE_TEMPORADAS_A_FUTURO = 10;
    const [temporadasCombo, setTemporadasCombo] = useState([]);
    const [temporadaMostrada, setTemporadaMostrada] = useState();
    const [numeroTemporadaActual, setNumeroTemporadaActual] = useState();


    useEffect(() => {
        ServicioTemporada.obtenerTemporada().then((temporadaObtenida) => {
            llenarComboTemporadas(temporadaObtenida.data);
            setNumeroTemporadaActual(temporadaObtenida.data.numero);
            ServicioBalance.obtenerBalanceTodosLosEquipos(temporadaObtenida.data.numero).then((respuestaBalanceEquipos) => {
                setBalancesEquipos(respuestaBalanceEquipos.data);
            });
        });
    }, []);

    useEffect(() => {
        if (temporadaMostrada) {
            ServicioBalance.obtenerBalanceTodosLosEquipos(temporadaMostrada).then((respuestaBalanceEquipos) => {
                setBalancesEquipos(respuestaBalanceEquipos.data);
            });
        }
    }, [temporadaMostrada]);

    const llenarComboTemporadas = (temporadaActual) => {
        let temporadasParaCombo = [];
        for (let i = 1; i <= temporadaActual.numero + LIMITE_TEMPORADAS_A_FUTURO; i++) {
            let temporada = {
                valor: i,
                descripcion: "Temp. " + i
            }
            temporadasParaCombo.push(temporada);
        }
        setTemporadasCombo(temporadasParaCombo);
        setTemporadaMostrada(temporadaActual.numero);
    }

    const aplicarEstiloPositivoONegativo = (valor) => {
        if (valor >= 0) {
            return 'total-positivo';
        }
        return 'total-negativo';
    }

    const esTemporadaActual = (temporada) => {
        return temporada.valor === numeroTemporadaActual;
    }

    return (
        <>
            <div className="letra-blanca col-9 col-md-10 col-xl-11">
                <h3 className="titulo-login mb-5 pt-3">Balance de todos los equipos</h3>
                <div className="col-md-12">

                    <div className="d-flex justify-content-center">
                        <select id="balances-temporadas" className="form-control mt-4 mb-4 col-md-3" onChange={e => setTemporadaMostrada(e.target.value)}>
                            <option key="placeholder" disabled>Seleccionar...</option>
                            {temporadasCombo.map((temporada, indice) => {
                                return <option key={indice} value={temporada.valor} selected={esTemporadaActual(temporada)}>{temporada.descripcion}</option>
                            })}
                        </select>
                    </div>

                    <table id="tabla-carga-balances-liga" className="table table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Equipo</th>
                            <th scope="col" colSpan="3">Gastos</th>
                            <th scope="col" colSpan="3">Ingresos</th>
                            <th scope="col" colSpan="3">Total</th>
                        </tr>
                        <tr>
                            <th scope="row"/>
                            <td>Principio</td>
                            <td>Mitad</td>
                            <td>Final</td>
                            <td>Principio</td>
                            <td>Mitad</td>
                            <td>Final</td>
                            <td>Principio</td>
                            <td>Mitad</td>
                            <td>Final</td>
                        </tr>
                        </thead>
                        <tbody>
                            <>
                                {balanceEquipos.map((detalleBalance, indice) => {
                                    return <tr className="fila-carga-balances-liga" key={indice}>
                                        <td id={'equipo' + indice}>
                                            {detalleBalance.equipo.nombreEquipo}
                                        </td>

                                        <td id={detalleBalance.equipo.nombreEquipo + 'gastos-principio'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalance.gastosPeriodoPrincipio}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalance.equipo.nombreEquipo + 'gastos-mitad'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalance.gastosPeriodoMitad}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalance.equipo.nombreEquipo + 'gastos-final'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalance.gastosPeriodoFinal}
                                                            displayType={"text"} required/>
                                        </td>

                                        <td id={detalleBalance.equipo.nombreEquipo + 'ingresos-principio'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalance.ingresosPeriodoPrincipio}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalance.equipo.nombreEquipo + 'ingresos-mitad'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalance.ingresosPeriodoMitad}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td id={detalleBalance.equipo.nombreEquipo + 'ingresos-final'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalance.ingresosPeriodoFinal}
                                                            displayType={"text"} required/>
                                        </td>

                                        <td className={aplicarEstiloPositivoONegativo(detalleBalance.totalPeriodoPrincipio)} id={detalleBalance.equipo.nombreEquipo + 'total-principio'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalance.totalPeriodoPrincipio}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td className={aplicarEstiloPositivoONegativo(detalleBalance.totalPeriodoMitad)} id={detalleBalance.equipo.nombreEquipo + 'total-mitad'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalance.totalPeriodoMitad}
                                                            displayType={"text"} required/>
                                        </td>
                                        <td className={aplicarEstiloPositivoONegativo(detalleBalance.totalPeriodoFinal)} id={detalleBalance.equipo.nombreEquipo + 'total-final'}>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={detalleBalance.totalPeriodoFinal}
                                                            displayType={"text"} required/>
                                        </td>
                                    </tr>
                                })}
                            </>
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
};
export default BalancesVistaBalancesTodosLosEquipos;
