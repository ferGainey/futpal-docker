import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import $ from "jquery";
import ServicioTemporada from "../../servicios/ServicioTemporada";
import PopupTemporada from "./temporada/PopupTemporada";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons/faBars";

const BarraNavegacionLogueado = () => {

  const history = useHistory();
  const [temporada, setTemporada] = useState();
  const [textoTemporada, setTextoTemporada] = useState('');
  const [mostrarPopupTemporada, setMostrarPopupTemporada] = useState(false);

  useEffect(() => {
    let tokenUsuario = localStorage.getItem("usuario");
    if (tokenUsuario != null) {
      let rolesUsuarioLogueado = JSON.parse(localStorage.getItem("usuario")).roles;
      if (!rolesUsuarioLogueado.includes("ROL_ADMINISTRADOR")) {
        $('#boton-navbar-crear-liga').css("display", "none");
      }
    }
    ServicioTemporada.obtenerTemporada().then((temporadaObtenida) => {
      setTemporada(temporadaObtenida.data);
      setTextoTemporada(mostrarTemporada(temporadaObtenida.data));
    })
  }, []);

  useEffect(() => {
    if (mostrarPopupTemporada === false) {
      ServicioTemporada.obtenerTemporada().then((temporadaObtenida) => {
        setTemporada(temporadaObtenida.data);
        setTextoTemporada(mostrarTemporada(temporadaObtenida.data));
      })
    }
  }, [mostrarPopupTemporada]);

  const desloguearUsuario = () => {
    localStorage.removeItem("usuario");
    history.push("/");
  }

  const irAPantallaCreacionLiga = () => {
    history.push("/front/crear-liga");
  }

  const irAPantallaEditarEquipo = () => {
    history.push("/front/editar-equipo");
  }

  const irAPantallaLigasDisponibles = () => {
    history.push("/front/ligas-disponibles");
  }

  const irAPantallaMiPlantel = () => {
    history.push("/front/plantel");
  }

  const irAPantallaPlanteles = () => {
    history.push("/front/planteles");
  }

  const mostrarTemporada = (temporadaAFormatearTexto) => {
      return `${temporadaAFormatearTexto.periodoTemporada.nombre} Temp. ${temporadaAFormatearTexto.numero}`
  }

  const irAPantallaTemporada = () => {
    let tokenUsuario = localStorage.getItem("usuario");
    if (tokenUsuario != null) {
      let rolesUsuarioLogueado = JSON.parse(localStorage.getItem("usuario")).roles;
      if (rolesUsuarioLogueado.includes("ROL_ADMINISTRADOR")) {
        setMostrarPopupTemporada(true);
      }
    }
  }

  const irAPantallaBalances = () => {
    history.push("/front/balances");
  }

  const irAPantallaTransferencias = () => {
    history.push("/front/transferencias");
  }

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="navbar-header">
        <div className="row">
          <div className="col-12">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a className="navbar-brand">Futpal</a>
            <button type="button" className="btn navbar-toggle d-block d-lg-none bg-dark" data-toggle="collapse" data-target=".navbar-collapse">
              <FontAwesomeIcon className="letra-blanca" icon={faBars}/>
            </button>
          </div>
        </div>
      </div>
      <div className="navbar-collapse collapse" id="navBarLogueado">
        <div className="navbar-nav">
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a id="boton-navbar-crear-liga" className="nav-item nav-link pointer" onClick={() => irAPantallaCreacionLiga()}>Crear Liga</a>
        </div>
        <div className="navbar-nav">
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a id="boton-navbar-editar-equipo" className="nav-item nav-link pointer" onClick={() => irAPantallaEditarEquipo()}>Editar Equipo</a>
        </div>
        <div className="navbar-nav">
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a id="boton-navbar-ligas-disponibles" className="nav-item nav-link pointer" onClick={() => irAPantallaLigasDisponibles()}>Ligas Disponibles</a>
        </div>
        <div className="navbar-nav">
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a id="boton-navbar-plantel" className="nav-item nav-link pointer" onClick={() => irAPantallaMiPlantel()}>Mi Plantel</a>
        </div>
        <div className="navbar-nav">
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a id="boton-navbar-planteles" className="nav-item nav-link pointer" onClick={() => irAPantallaPlanteles()}>Planteles</a>
        </div>
        <div className="navbar-nav">
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a id="boton-navbar-transferencias" className="nav-item nav-link pointer" onClick={() => irAPantallaTransferencias()}>Transferencias</a>
        </div>
        <div className="navbar-nav">
          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
          <a id="boton-navbar-balances" className="nav-item nav-link pointer" onClick={() => irAPantallaBalances()}>Balances</a>
        </div>

        <div className="navbar-nav navbar-seccion-derecha">
          <div className="navbar-nav">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a id="boton-navbar-temporada" className="nav-item nav-link pointer" onClick={() => irAPantallaTemporada()}>{textoTemporada}</a>
          </div>
          <div className="navbar-nav">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a id="boton-navbar-logout" className="nav-item nav-link pointer" onClick={() => desloguearUsuario()}>Log Out</a>
          </div>
        </div>

      </div>
      {mostrarPopupTemporada ? <PopupTemporada mostrar={mostrarPopupTemporada} setMostrar={setMostrarPopupTemporada} temporada={temporada}/> : null}
    </nav>
          );
}
export default BarraNavegacionLogueado;
