import React, {useEffect, useState} from 'react';
import BarraNavegacionLogueado from '../BarraNavegacionLogueado.jsx';
import CargaTransferencia from "./CargaTransferencia";
import BarraLateralTransferencias from "./BarraLateralTransferencias";
import MisTransferencias from "./MisTransferencias";
import TodasLasTransferenciasConfirmadas from "./TodasLasTransferenciasConfirmadas";

const Transferencias = () => {

    const [transferenciasCargaActivo, setTransferenciasCargaActivo] = useState(false);
    const [misTransferenciasActivo, setMisTransferenciasActivo] = useState(false);
    const [todasLasTransferenciasConfirmadasActivo, setTodasLasTransferenciasConfirmadasActivo] = useState(false);

    useEffect(() => {
        setTransferenciasCargaActivo(false);
        setMisTransferenciasActivo(true);
        setTodasLasTransferenciasConfirmadasActivo(false);
    }, []);


    return (
        <>
            <BarraNavegacionLogueado/>
            <div className="row ml-0 mr-0">
                <BarraLateralTransferencias
                    setTransferenciasCargaActivo={setTransferenciasCargaActivo}
                    setMisTransferenciasActivo={setMisTransferenciasActivo}
                    setTodasLasTransferenciasConfirmadasActivo={setTodasLasTransferenciasConfirmadasActivo}/>
                {transferenciasCargaActivo ? <CargaTransferencia/> : null}
                {misTransferenciasActivo ? <MisTransferencias/> : null}
                {todasLasTransferenciasConfirmadasActivo ? <TodasLasTransferenciasConfirmadas/> : null}
            </div>
        </>
    );
};
export default Transferencias;
