import React, {useEffect, useState} from 'react';
import ServicioTransferencia from "../../../servicios/ServicioTransferencia";
import Transferencia from "./Transferencia";

const TodasLasTransferenciasConfirmadas = () => {

    const CANTIDAD_TRANSFERENCIAS_POR_PAGINA = 20;

    const [transferencias, setTransferencias] = useState([]);

    const [paginaActual, setPaginaActual] = useState(1);
    const [offsetActual, setOffsetActual] = useState(0);
    const [estaCargando, setEstaCargando] = useState(true);

    useEffect(() => {
        ServicioTransferencia.obtenerTodasLasTransferenciasConfirmadas(paginaActual).then((respuestaTransferencias) => {
            setTransferencias(respuestaTransferencias.data);
            setEstaCargando(false);
        });
    }, []);

    // Invoke when user click to request another page.
    const irAPaginaAnterior = () => {
        const paginaALaQueSeCambia = paginaActual - 1;
        const nuevoOffset = (paginaALaQueSeCambia - 1) * CANTIDAD_TRANSFERENCIAS_POR_PAGINA;
        setEstaCargando(true);
        ServicioTransferencia.obtenerTodasLasTransferenciasConfirmadas(paginaALaQueSeCambia).then((respuestaTransferencias) => {
            setTransferencias(respuestaTransferencias.data);
            setEstaCargando(false);
        });

        setPaginaActual(paginaALaQueSeCambia);
        setOffsetActual(nuevoOffset);
    };

    const irAPaginaSiguiente = () => {
        const paginaALaQueSeCambia = paginaActual + 1;
        const nuevoOffset = (paginaALaQueSeCambia - 1) * CANTIDAD_TRANSFERENCIAS_POR_PAGINA;
        setEstaCargando(true);
        ServicioTransferencia.obtenerTodasLasTransferenciasConfirmadas(paginaALaQueSeCambia).then((respuestaTransferencias) => {
            setTransferencias(respuestaTransferencias.data);
            setEstaCargando(false);
        });

        setPaginaActual(paginaALaQueSeCambia);
        setOffsetActual(nuevoOffset);
    };

    return (
        <>
            <div className="letra-blanca col-9 col-md-10 col-xl-11">
                <h3 className="titulo-login mb-1 pt-3">Todas las transferencias confirmadas</h3>
                <div>
                    {transferencias ? transferencias.map((transferencia) => {
                        return <Transferencia key={transferencia.id} datos={transferencia} equipoPropioId={null} setTransferencias={setTransferencias}/>
                    }) : null}
                </div>

                <div className="container d-flex justify-content-center mt-5 mb-5">
                    {offsetActual > 0 ? <button className="btn btn-light mr-3" onClick={() => irAPaginaAnterior()} disabled={estaCargando}>&lt; Anterior</button> : null}
                    {transferencias.length > 0 && transferencias.length === CANTIDAD_TRANSFERENCIAS_POR_PAGINA ?
                        <button className="btn btn-light ml-3" onClick={() => irAPaginaSiguiente()} disabled={estaCargando}>Siguiente &gt;</button> : null}
                </div>

            </div>
        </>
    );
}
export default TodasLasTransferenciasConfirmadas;
