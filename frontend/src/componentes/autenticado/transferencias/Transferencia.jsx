import React, {useEffect, useState} from 'react';
import ServicioTransferencia from "../../../servicios/ServicioTransferencia";
import {faArrowRight, faCheck, faTimes} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Button from "react-bootstrap/Button";
import VistaTextoDetalleJugadoresTransferencia from "./VistaTextoDetalleJugadoresTransferencia";
import CurrencyFormat from "react-currency-format";

const Transferencia = ({datos, equipoPropioId, setTransferencias}) => {

    const ESTADO_TRANSFERENCIA_PENDIENTE = "PENDIENTE";

    const [jugadoresARecibirEquipoOrigenTransferencia, setJugadoresARecibirEquipoOrigenTransferencia] = useState([]);
    const [jugadoresARecibirEquipoDestinoTransferencia, setJugadoresARecibirEquipoDestinoTransferencia] = useState([]);

    const [montosARecibirEquipoOrigenTransferencia, setMontosARecibirEquipoOrigenTransferencia] = useState([]);
    const [montosARecibirEquipoDestinoTransferencia, setMontosARecibirEquipoDestinoTransferencia] = useState([]);

    useEffect(() => {
        completarMontos();
        completarJugadores();
    }, []);

    const completarMontos = () => {
        let montosEquipoOrigenActualizados = [...montosARecibirEquipoOrigenTransferencia];
        let montosEquipoDestinoActualizados = [...montosARecibirEquipoDestinoTransferencia];
        datos.montos.forEach(balance => {
            if (balance.monto > 0) {
                if (balance.equipo.id === datos.equipoOrigenTransferencia.id) {
                    montosEquipoOrigenActualizados.push(balance);
                    setMontosARecibirEquipoOrigenTransferencia(montosEquipoOrigenActualizados);
                }
                else {
                    montosEquipoDestinoActualizados.push(balance);
                    setMontosARecibirEquipoDestinoTransferencia(montosEquipoDestinoActualizados);
                }
            }
        });
    }

    const completarJugadores = () => {
        let jugadoresEquipoOrigenActualizados = [...jugadoresARecibirEquipoOrigenTransferencia];
        let jugadoresEquipoDestinoActualizados = [...jugadoresARecibirEquipoDestinoTransferencia];
        datos.movimientoJugadores.forEach(movimiento => {
            if ((movimiento.equipoDestino.id === datos.equipoOrigenTransferencia.id &&
                (movimiento.prestamoJugador == null || (movimiento.prestamoJugador.esFinDePrestamo === false))) ||
                (movimiento.equipoDestino.id !== datos.equipoOrigenTransferencia.id &&
                    (movimiento.prestamoJugador != null && (movimiento.prestamoJugador.esFinDePrestamo === true)))
            ) {
                jugadoresEquipoOrigenActualizados.push(movimiento);
            }
            else {
                jugadoresEquipoDestinoActualizados.push(movimiento);
            }
            setJugadoresARecibirEquipoOrigenTransferencia(jugadoresEquipoOrigenActualizados);
            setJugadoresARecibirEquipoDestinoTransferencia(jugadoresEquipoDestinoActualizados);
        });
    }

    const pendienteAprobacionEquipoQueRecibe = () => {
        return datos.estadoTransferencia &&
            datos.estadoTransferencia.nombre === ESTADO_TRANSFERENCIA_PENDIENTE &&
            datos.equipoDestinoTransferencia.id === equipoPropioId;
    }

    const pendienteAprobacionEquipoQueRealizaOferta = () => {
        return datos.estadoTransferencia &&
            datos.estadoTransferencia.nombre === ESTADO_TRANSFERENCIA_PENDIENTE &&
            datos.equipoOrigenTransferencia.id === equipoPropioId;
    }

    const rechazarTransferencia = () => {
        ServicioTransferencia.rechazarTransferencia(datos.id).then(() => {
            ServicioTransferencia.obtenerMisTransferencias().then((respuestaTransferencias) => {
                setTransferencias(respuestaTransferencias.data);
            });
        });
    }

    const confirmarTransferencia = () => {
        ServicioTransferencia.confirmarTransferencia(datos.id).then(() => {
            ServicioTransferencia.obtenerMisTransferencias().then((respuestaTransferencias) => {
                setTransferencias(respuestaTransferencias.data);
            });
        });
    }

    const calcularMontoTotalRecibido = (balances) => {
        let total = 0;
        balances.forEach(balance => total += balance.monto);
        return total;
    }

    return (
        <>
            <div className="d-flex justify-content-center">
                <div className="card letra-negra col-11 col-md-6 text-center mt-5 pl-0 pr-0">
                    <div className="card-header">
                        <small className="float-left font-italic text-secondary">{datos.estadoTransferencia.nombre}</small>
                        <span>{datos.equipoOrigenTransferencia.nombreEquipo} <FontAwesomeIcon className="letra-negra ml-2 mr-2" icon={faArrowRight} /> {datos.equipoDestinoTransferencia.nombreEquipo}</span>

                    </div>
                    <div className="card-body">
                            <div className="col-12">
                                <div className="row">
                                <div className="col-12 col-md-6 text-center pr-md-5">
                                    {jugadoresARecibirEquipoOrigenTransferencia.map((movimiento) => {
                                        return <div key={movimiento.id}>
                                            <VistaTextoDetalleJugadoresTransferencia movimiento={movimiento}/>
                                        </div>

                                    })}
                                    {calcularMontoTotalRecibido(montosARecibirEquipoOrigenTransferencia) > 0 ?
                                        <div><span>- {datos.equipoOrigenTransferencia.nombreEquipo} recibe en TOTAL </span>
                                        <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                        decimalScale={0} prefix={'$'} value={calcularMontoTotalRecibido(montosARecibirEquipoOrigenTransferencia)}
                                                        displayType={"text"} required/>
                                        </div>
                                        : null}
                                    {montosARecibirEquipoOrigenTransferencia.map((balance) => {
                                        return <div key={balance.id}>
                                            <span>- {datos.equipoOrigenTransferencia.nombreEquipo} recibe </span>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={balance.monto}
                                                            displayType={"text"} required/>
                                            <span> en {balance.periodo.nombre} temporada {balance.numeroTemporada}</span>
                                        </div>
                                    })}
                                </div>

                                <div className="col-12 col-md-6 text-center pl-md-5">
                                    {jugadoresARecibirEquipoDestinoTransferencia.map((movimiento) => {
                                        return <div key={movimiento.id}>
                                            <VistaTextoDetalleJugadoresTransferencia movimiento={movimiento}/>
                                        </div>

                                    })}
                                    {calcularMontoTotalRecibido(montosARecibirEquipoDestinoTransferencia) > 0 ?
                                        <div><span>- {datos.equipoDestinoTransferencia.nombreEquipo} recibe en TOTAL </span>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={calcularMontoTotalRecibido(montosARecibirEquipoDestinoTransferencia)}
                                                            displayType={"text"} required/>
                                        </div>
                                        : null}
                                    {montosARecibirEquipoDestinoTransferencia.map((balance) => {
                                        return <div key={balance.id}>
                                            <span>- {datos.equipoDestinoTransferencia.nombreEquipo} recibe </span>
                                            <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                            decimalScale={0} prefix={'$'} value={balance.monto}
                                                            displayType={"text"} required/>
                                            <span> en {balance.periodo.nombre} temporada {balance.numeroTemporada}</span>
                                        </div>
                                    })}
                                </div>
                            </div>

                        </div>

                    </div>
                    {pendienteAprobacionEquipoQueRecibe() || pendienteAprobacionEquipoQueRealizaOferta() ?
                        <div className="card-footer">
                            <span>
                                {pendienteAprobacionEquipoQueRecibe() ?
                                    <div className="d-flex justify-content-around">
                                        <Button id={'boton-rechazar' + datos.id} onClick={() => rechazarTransferencia()}
                                                variant="outline-danger"><FontAwesomeIcon className="text-danger mr-1"
                                                                                          icon={faTimes}/> Rechazar</Button>
                                        <Button id={'boton-confirmar' + datos.id} onClick={() => confirmarTransferencia()}
                                                variant="outline-success"><FontAwesomeIcon className="text-success mr-1"
                                                                                           icon={faCheck}/> Confirmar</Button>
                                    </div>
                                    : null}
                                {pendienteAprobacionEquipoQueRealizaOferta() ?
                                    <Button id={'boton-rechazar' + datos.id} onClick={() => rechazarTransferencia()}
                                            variant="outline-danger"><FontAwesomeIcon className="text-danger mr-1"
                                                                                      icon={faTimes}/> Cancelar</Button>
                                : null}
                            </span>
                        </div>
                        : null
                    }

                </div>
            </div>
        </>
    );
};
export default Transferencia;
