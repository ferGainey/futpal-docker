import React from 'react';

const VistaTextoDetalleJugadoresTransferencia = ({movimiento}) => {

    const descripcion = () => {
        if (movimiento.prestamoJugador) {
            return descripcionPrestamo();
        }
        else {
            return descripcionTraspaso();
        }
    }

    const descripcionPrestamo = () => {
        if (!movimiento.prestamoJugador.esFinDePrestamo) {
            return `- ${movimiento.equipoDestino.nombreEquipo} recibe prestado a ${movimiento.jugador.nombre} desde ${movimiento.periodo.nombre} Temporada ${movimiento.numeroTemporada} `;
        }
        else {
            return `hasta ${movimiento.periodo.nombre} Temporada ${movimiento.numeroTemporada}`;
        }
    }

    const descripcionTraspaso = () => {
        return `- ${movimiento.equipoDestino.nombreEquipo} recibe a ${movimiento.jugador.nombre} en ${movimiento.periodo.nombre} Temporada ${movimiento.numeroTemporada}`;
    }

    return (
        <>
            <span id={'transferenciaJugador' + movimiento.jugador.nombre}>{descripcion()}</span>
        </>
    );
};
export default VistaTextoDetalleJugadoresTransferencia;
