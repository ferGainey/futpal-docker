import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import $ from "jquery";
import ServicioEquipo from "../../../servicios/ServicioEquipo";
import ServicioTransferencia from "../../../servicios/ServicioTransferencia";

const BarraLateralTransferencias = ({setTransferenciasCargaActivo, setMisTransferenciasActivo, setTodasLasTransferenciasConfirmadasActivo}) => {

    const history = useHistory();
    const ESTADO_APROBADO_ID = 1;
    const [mercadoActivo, setMercadoActivo] = useState()

    useEffect(() => {
        $('#habilitacion-mercado-side-nav').css("display", "none");
        $('#deshabilitacion-mercado-side-nav').css("display", "none");
        $('#carga-transferencia-side-nav').css("display", "none");
        let tokenUsuario = JSON.parse(localStorage.getItem("usuario"));
        if (tokenUsuario == null) {
            history.push("/");
        }
        else {
            if (tokenUsuario.id) {
                ServicioEquipo.obtenerDatosEquipo(tokenUsuario.id).then((equipo) => {
                    ServicioTransferencia.obtenerEstadoMercado().then((mercadoActivo) => {
                        setMercadoActivo(mercadoActivo.data);
                        if (equipo.data.estadoEquipoId === ESTADO_APROBADO_ID && mercadoActivo.data === true) {
                            $('#carga-transferencia-side-nav').css("display", "initial");
                        }
                    });
                    let rolesUsuarioLogueado = tokenUsuario.roles;
                    if (rolesUsuarioLogueado.includes("ROL_ADMINISTRADOR")) {
                        habilitarMercado();
                    }
                });
            }
        }

    }, []);

    useEffect(() => {
        let tokenUsuario = JSON.parse(localStorage.getItem("usuario"));
        let rolesUsuarioLogueado = tokenUsuario.roles;
        if (mercadoActivo) {
            $('#carga-transferencia-side-nav').css("display", "initial");
            if (rolesUsuarioLogueado.includes("ROL_ADMINISTRADOR")) {
                $('#deshabilitacion-mercado-side-nav').css("display", "initial");
                $('#habilitacion-mercado-side-nav').css("display", "none");
            }

        }
        else {
            $('#carga-transferencia-side-nav').css("display", "none");
            if (rolesUsuarioLogueado.includes("ROL_ADMINISTRADOR")) {
                $('#deshabilitacion-mercado-side-nav').css("display", "none");
                $('#habilitacion-mercado-side-nav').css("display", "initial");
            }
        }
    }, [mercadoActivo])

    const habilitarMercado = () => {
        $('#habilitacion-mercado-side-nav').css("display", "initial");
    }

    const irACargaTransferencia = () => {
        setTransferenciasCargaActivo(true);
        setMisTransferenciasActivo(false);
        setTodasLasTransferenciasConfirmadasActivo(false);
    }

    const irAMisTransferencias = () => {
        setTransferenciasCargaActivo(false);
        setMisTransferenciasActivo(true);
        setTodasLasTransferenciasConfirmadasActivo(false);
    }

    const irATodasLasTransferenciasConfirmadas = () => {
        setTransferenciasCargaActivo(false);
        setMisTransferenciasActivo(false);
        setTodasLasTransferenciasConfirmadasActivo(true);
    }

    const cerrarMercado = () => {
        if (window.confirm("¿Está seguro que desea cerrar el mercado?")) {
            ServicioTransferencia.cambiarEstadoMercado(!mercadoActivo).then((estadoMercadoActualizado) => {
                setMercadoActivo(estadoMercadoActualizado.data);
            });
        }
    }

    const abrirMercado = () => {
        if (window.confirm("¿Está seguro que desea abrir el mercado?")) {
            ServicioTransferencia.cambiarEstadoMercado(!mercadoActivo).then((estadoMercadoActualizado) => {
                setMercadoActivo(estadoMercadoActualizado.data);
            });
        }
    }

    return (
        <div className="col-3 col-md-2 col-xl-1 px-1 bg-dark" id="sticky-sidebar">
            <div className="nav flex-column flex-nowrap overflow-auto text-white ">
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="mis-transferencias-side-nav" className="nav-link text-center pointer letra-blanca"
                   onClick={() => irAMisTransferencias()}>Mis transferencias</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="todas-las-transferencias-confirmadas-side-nav" className="nav-link text-center pointer letra-blanca"
                   onClick={() => irATodasLasTransferenciasConfirmadas()}>Todas las transferencias</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="carga-transferencia-side-nav" className="nav-link text-center pointer letra-blanca"
                   onClick={() => irACargaTransferencia()}>Cargar transferencia</a>
                <button id="deshabilitacion-mercado-side-nav" className="btn btn-danger mb-3"
                   onClick={() => cerrarMercado()}>Cerrar mercado</button>
                <button id="habilitacion-mercado-side-nav" className="btn btn-success mb-3"
                        onClick={() => abrirMercado()}>Abrir mercado</button>
            </div>
        </div>
    );
};
export default BarraLateralTransferencias;
