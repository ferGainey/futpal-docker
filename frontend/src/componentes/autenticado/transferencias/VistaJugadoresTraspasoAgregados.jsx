import React, {useEffect} from 'react';
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const VistaJugadoresTraspasoAgregados = ({jugadoresAgregados, equipoOrigenId, setJugadoresAgregados}) => {

    useEffect(() => {
    }, []);

    const descripcionJugadorAgregado = (jugador) => {
        return `${jugador.jugador.nombre} en ${jugador.periodo.descripcion} de temporada ${jugador.numeroTemporada}`;
    }

    const borrarJugador = (jugador) => {
        let array = [...jugadoresAgregados];
        let index = array.indexOf(jugador)
        if (index !== -1) {
            array.splice(index, 1);
            setJugadoresAgregados(array);
        }
    }

    return (
        <>
            <div>
                {jugadoresAgregados.map((jugador, indice) => {
                    if (equipoOrigenId === jugador.idEquipoOrigen)
                        return (<div className="d-block row mt-2 text-center" key={indice}>
                        {descripcionJugadorAgregado(jugador)}
                        <FontAwesomeIcon className="pointer ml-2" icon={faTrash} onClick={() => borrarJugador(jugador)}/>
                    </div>)
                })}
            </div>
        </>
    );
};
export default VistaJugadoresTraspasoAgregados;
