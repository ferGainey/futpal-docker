import React, {useEffect, useState} from 'react';
import ServicioEquipo from "../../../servicios/ServicioEquipo";
import ServicioTemporada from "../../../servicios/ServicioTemporada";
import ServicioJugador from "../../../servicios/ServicioJugador";
import AgregadorTraspasoJugador from "./AgregadorTraspasoJugador";
import VistaJugadoresTraspasoAgregados from "./VistaJugadoresTraspasoAgregados";
import AgregadorMonto from "./AgregadorMonto";
import VistaMontosAgregados from "./VistaMontosAgregados";
import $ from "jquery";
import ServicioTransferencia from "../../../servicios/ServicioTransferencia";
import AgregadorPrestamoJugador from "./AgregadorPrestamoJugador";
import VistaJugadoresPrestamoAgregados from "./VistaJugadoresPrestamoAgregados";

const CargaTransferencia = () => {

    const LIMITE_TEMPORADAS_A_FUTURO = 10;
    const [equiposCombo, setEquiposCombo] = useState([]);
    const [temporadasCombo, setTemporadasCombo] = useState([]);

    const [jugadoresPropios, setJugadoresPropios] = useState([]);
    const [jugadoresOtroEquipo, setJugadoresOtroEquipo] = useState([]);
    const [otroEquipoId, setOtroEquipoId] = useState();
    const [equipoPropioId, setEquipoPropioId] = useState();

    const [jugadoresPrestamoAgregados, setJugadoresPrestamoAgregados] = useState([]);
    const [jugadoresTraspasoAgregados, setJugadoresTraspasoAgregados] = useState([]);
    const [montosAgregados, setMontosAgregados] = useState([]);


    const [mostrarAgregadorJugadorPrestamoPropio, setMostrarAgregadorJugadorPrestamoPropio] = useState(false);
    const [mostrarAgregadorJugadorPrestamoOtroEquipo, setMostrarAgregadorJugadorPrestamoOtroEquipo] = useState(false);
    const [mostrarAgregadorJugadorTraspasoPropio, setMostrarAgregadorJugadorTraspasoPropio] = useState(false);
    const [mostrarAgregadorJugadorTraspasoOtroEquipo, setMostrarAgregadorJugadorTraspasoOtroEquipo] = useState(false);
    const [mostrarAgregadorMontoPropio, setMostrarAgregadorMontoPropio] = useState(false);
    const [mostrarAgregadorMontoOtroEquipo, setMostrarAgregadorMontoOtroEquipo] = useState(false);

    useEffect(() => {
        let tokenUsuario = localStorage.getItem("usuario");
        let idUsuarioToken = JSON.parse(tokenUsuario).id;

        ServicioTemporada.obtenerTemporada().then((temporadaObtenida) => {
            llenarComboTemporadas(temporadaObtenida.data);
        });

        ServicioEquipo.obtenerDatosEquipo(idUsuarioToken).then((respuestaDatosEquipo) => {
            // ToDo: validar que tenga equipo
            setEquipoPropioId(respuestaDatosEquipo.data.idEquipo);
            ServicioJugador.obtenerJugadoresDuenioDePase(respuestaDatosEquipo.data.idEquipo).then((respuestaJugadoresEquipo) => {
                setJugadoresPropios(respuestaJugadoresEquipo.data);
            });

            ServicioEquipo.obtenerEquiposAprobados().then((respuestaEquiposAprobados) => {
                llenarComboEquipos(respuestaDatosEquipo.data.idEquipo, respuestaEquiposAprobados.data);
            });
        });
    }, []);

    const llenarComboTemporadas = (temporadaActual) => {
        let temporadasParaCombo = [];
        for (let i = temporadaActual.numero; i <= temporadaActual.numero + LIMITE_TEMPORADAS_A_FUTURO; i++) {
            let temporada = {
                valor: i,
                descripcion: "Temp. " + i
            }
            temporadasParaCombo.push(temporada);
        }
        setTemporadasCombo(temporadasParaCombo);
    }

    const llenarComboEquipos = (idEquipoPropio, equipos) => {
        let equiposParaCombo = [];
        equipos.forEach(equipo => {
            if (equipo.id !== idEquipoPropio) {
                equiposParaCombo.push(equipo);
            }
        });
        setEquiposCombo(equiposParaCombo);
    }

    const mostrarAgregarJugadorPrestamoPropio = () => {
        setMostrarAgregadorJugadorPrestamoPropio(true);
        setMostrarAgregadorJugadorTraspasoPropio(false);
        setMostrarAgregadorMontoPropio(false);
    }

    const mostrarAgregarJugadorPrestamoOtroEquipo = () => {
        setMostrarAgregadorJugadorPrestamoOtroEquipo(true);
        setMostrarAgregadorJugadorTraspasoOtroEquipo(false);
        setMostrarAgregadorMontoOtroEquipo(false);
    }

    const mostrarAgregarJugadorTraspasoPropio = () => {
        setMostrarAgregadorJugadorPrestamoPropio(false);
        setMostrarAgregadorJugadorTraspasoPropio(true);
        setMostrarAgregadorMontoPropio(false);
    }

    const mostrarAgregarJugadorTraspasoOtroEquipo = () => {
        setMostrarAgregadorJugadorPrestamoOtroEquipo(false);
        setMostrarAgregadorJugadorTraspasoOtroEquipo(true);
        setMostrarAgregadorMontoOtroEquipo(false);
    }

    const mostrarAgregarMontoPropio = () => {
        setMostrarAgregadorJugadorPrestamoPropio(false);
        setMostrarAgregadorJugadorTraspasoPropio(false);
        setMostrarAgregadorMontoPropio(true);
    }

    const mostrarAgregarMontoOtroEquipo = () => {
        setMostrarAgregadorJugadorPrestamoOtroEquipo(false);
        setMostrarAgregadorJugadorTraspasoOtroEquipo(false);
        setMostrarAgregadorMontoOtroEquipo(true);
    }

    const actualizarRecibidoEnTransferencia = (idOtroEquipo) => {
        let jugadoresActualizados = [...jugadoresTraspasoAgregados];
        setOtroEquipoId(idOtroEquipo);
        setJugadoresTraspasoAgregados(jugadoresActualizados.filter(jugador => jugador.idEquipoOrigen === equipoPropioId || jugador.idEquipoOrigen === idOtroEquipo));
        ServicioJugador.obtenerJugadoresDuenioDePase(idOtroEquipo).then((respuestaJugadoresEquipo) => {
            setJugadoresOtroEquipo(respuestaJugadoresEquipo.data);
        });
    }

    const realizarTransferencia = () => {
        $('#boton-cargar-transferencia').prop('disabled', true);
        let transferencia = {
            otroEquipoId: otroEquipoId,
            jugadoresTransferencia: jugadoresTraspasoAgregados,
            jugadoresPrestamoTransferencia: jugadoresPrestamoAgregados,
            montosTransferencias: montosAgregados
        }
        ServicioTransferencia.cargarTransferencia(transferencia).then(() => {
            setJugadoresTraspasoAgregados([]);
            setJugadoresPrestamoAgregados([]);
            setMontosAgregados([]);
            $('#alerta-transferencia-exitosa').show();
            $('#boton-cargar-transferencia').prop('disabled', false);
        }, () => {
            $('#alerta-transferencia-error').show();
            $('#boton-cargar-transferencia').prop('disabled', false);
        });
    }

    return (
        <>
            <div className="letra-blanca col-9 col-md-10 col-xl-11">
                <h3 className="titulo-login mb-5 pt-3">Transferencia</h3>
                <div className="d-flex justify-content-center row">
                    <label className="mr-2 pt-2 col-12 col-md-auto">Equipo involucrado</label>
                    <select id="transferencias-equipos-aprobados" className="form-control col-10 col-md-4" onChange={e => actualizarRecibidoEnTransferencia(e.target.value)} required>
                        <option key="placeholder" disabled selected>Seleccionar...</option>
                        {equiposCombo.map((equipo, indice) => {
                            return <option key={indice} value={equipo.id}>{equipo.nombreEquipo}</option>
                        })}
                    </select>
                </div>
                { otroEquipoId ?
                    <div>
                        <div className="row mt-4">
                            <div className="col-12 col-md-6">
                                <h4 className="mt-4 pt-2 text-center">ENTREGO</h4>
                                <div className="d-flex justify-content-center mb-3">
                                    <button id="agregador-jugador-propio" className="btn btn-success mr-2" type="submit" onClick={() => mostrarAgregarJugadorTraspasoPropio()}>Agregar traspaso</button>
                                    <button id="agregador-jugador-prestamo-propio" className="btn btn-success mr-2" type="submit" onClick={() => mostrarAgregarJugadorPrestamoPropio()}>Agregar préstamo</button>
                                    <button id="agregador-monto-propio" className="btn btn-success ml-2" type="submit" onClick={() => mostrarAgregarMontoPropio()}>Agregar monto</button>
                                </div>
                                {mostrarAgregadorJugadorTraspasoPropio ? <AgregadorTraspasoJugador jugadoresAgregados={jugadoresTraspasoAgregados}
                                                                                           setJugadoresAgregados={setJugadoresTraspasoAgregados}
                                                                                           jugadores={jugadoresPropios}
                                                                                           temporadasCombo={temporadasCombo}
                                                                                           setMostrar={setMostrarAgregadorJugadorTraspasoPropio}
                                                                                           idEquipoOrigen={equipoPropioId}
                                                                                           idEquipoDestino={otroEquipoId}/> : null}

                                {mostrarAgregadorJugadorPrestamoPropio ? <AgregadorPrestamoJugador jugadoresPrestamoAgregados={jugadoresPrestamoAgregados}
                                                                                                   setJugadoresAgregados={setJugadoresPrestamoAgregados}
                                                                                                   jugadores={jugadoresPropios}
                                                                                                   temporadasCombo={temporadasCombo}
                                                                                                   setMostrar={setMostrarAgregadorJugadorPrestamoPropio}
                                                                                                   idEquipoOrigen={equipoPropioId}
                                                                                                   idEquipoDestino={otroEquipoId}/> : null}

                                {mostrarAgregadorMontoPropio ? <AgregadorMonto montosAgregados={montosAgregados}
                                                                                    setMontosAgregados={setMontosAgregados}
                                                                                   temporadasCombo={temporadasCombo}
                                                                                   setMostrar={setMostrarAgregadorMontoPropio}
                                                                                   idEquipoOrigen={equipoPropioId}
                                                                                   idEquipoDestino={otroEquipoId}/> : null}
                                <VistaJugadoresTraspasoAgregados equipoOrigenId={equipoPropioId} jugadoresAgregados={jugadoresTraspasoAgregados} setJugadoresAgregados={setJugadoresTraspasoAgregados}/>
                                <VistaJugadoresPrestamoAgregados equipoOrigenId={equipoPropioId} jugadoresAgregados={jugadoresPrestamoAgregados} setJugadoresAgregados={setJugadoresPrestamoAgregados}/>
                                <VistaMontosAgregados equipoOrigenId={equipoPropioId} montosAgregados={montosAgregados} setMontosAgregados={setMontosAgregados}/>
                            </div>
                            <div className="col-12 col-md-6">
                                <h4 className="mt-4 pt-2 text-center">RECIBO</h4>
                                <div className="d-flex justify-content-center mb-3">
                                    <button id="agregador-jugador-otro-equipo" className="btn btn-success mr-2" type="submit" onClick={() => mostrarAgregarJugadorTraspasoOtroEquipo()}>Agregar traspaso</button>
                                    <button id="agregador-jugador-prestamo-otro-equipo" className="btn btn-success mr-2" type="submit" onClick={() => mostrarAgregarJugadorPrestamoOtroEquipo()}>Agregar préstamo</button>
                                    <button id="agregador-monto-otro-equipo" className="btn btn-success ml-2" type="submit" onClick={() => mostrarAgregarMontoOtroEquipo()}>Agregar monto</button>
                                </div>
                                {mostrarAgregadorJugadorTraspasoOtroEquipo ? <AgregadorTraspasoJugador jugadoresAgregados={jugadoresTraspasoAgregados}
                                                                                               setJugadoresAgregados={setJugadoresTraspasoAgregados}
                                                                                               jugadores={jugadoresOtroEquipo}
                                                                                               temporadasCombo={temporadasCombo}
                                                                                               setMostrar={setMostrarAgregadorJugadorTraspasoOtroEquipo}
                                                                                               idEquipoOrigen={otroEquipoId}
                                                                                               idEquipoDestino={equipoPropioId}/> : null}

                                {mostrarAgregadorJugadorPrestamoOtroEquipo ? <AgregadorPrestamoJugador jugadoresPrestamoAgregados={jugadoresPrestamoAgregados}
                                                                                                   setJugadoresAgregados={setJugadoresPrestamoAgregados}
                                                                                                   jugadores={jugadoresOtroEquipo}
                                                                                                   temporadasCombo={temporadasCombo}
                                                                                                   setMostrar={setMostrarAgregadorJugadorPrestamoOtroEquipo}
                                                                                                   idEquipoOrigen={otroEquipoId}
                                                                                                   idEquipoDestino={equipoPropioId}/> : null}

                                {mostrarAgregadorMontoOtroEquipo ? <AgregadorMonto montosAgregados={montosAgregados}
                                                                                    setMontosAgregados={setMontosAgregados}
                                                                                 temporadasCombo={temporadasCombo}
                                                                                 setMostrar={setMostrarAgregadorMontoOtroEquipo}
                                                                                 idEquipoOrigen={otroEquipoId}
                                                                                 idEquipoDestino={equipoPropioId}/> : null}
                                <VistaJugadoresTraspasoAgregados equipoOrigenId={otroEquipoId} jugadoresAgregados={jugadoresTraspasoAgregados} setJugadoresAgregados={setJugadoresTraspasoAgregados}/>
                                <VistaJugadoresPrestamoAgregados equipoOrigenId={otroEquipoId} jugadoresAgregados={jugadoresPrestamoAgregados} setJugadoresAgregados={setJugadoresPrestamoAgregados}/>
                                <VistaMontosAgregados equipoOrigenId={otroEquipoId} montosAgregados={montosAgregados} setMontosAgregados={setMontosAgregados}/>
                            </div>
                        </div>

                        { jugadoresTraspasoAgregados.length > 0 || jugadoresPrestamoAgregados.length > 0 ?
                            <div className="d-flex justify-content-center">
                                <button id="boton-cargar-transferencia" className="btn btn-primary mt-5 mt-md-0" type="submit" onClick={() => realizarTransferencia()}>REALIZAR TRANSFERENCIA</button>
                            </div>
                        : null}

                        <div className="d-flex justify-content-center">
                            <div id="alerta-transferencia-exitosa" className="alert alert-success alert-dismissible collapse alerta-futpal w-25 text-center" role="alert">
                                <p id="texto-mensaje-carga-exitosa-transferencia">La transferencia ha sido cargada exitosamente.</p>
                                <button type="button" className="close" onClick={() => $('#alerta-transferencia-exitosa').hide()} aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                        <div className="d-flex justify-content-center">
                            <div id="alerta-transferencia-error" className="alert alert-danger alert-dismissible collapse alerta-futpal w-25 text-center" role="alert">
                                <p id="texto-mensaje-carga-erronea-transferencia">La transferencia no es válida.</p>
                                <button type="button" className="close" onClick={() => $('#alerta-transferencia-error').hide()} aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                : null}

            </div>
        </>
    );
}
export default CargaTransferencia;
