import React, {useEffect} from 'react';
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import CurrencyFormat from "react-currency-format";

const VistaMontosAgregados = ({montosAgregados, equipoOrigenId, setMontosAgregados}) => {

    useEffect(() => {
    }, []);

    const descripcionMontoAgregado = (monto) => {
        return ` en ${monto.periodo.descripcion} de temporada ${monto.numeroTemporada}`;
    }

    const borrarMonto = (monto) => {
        let array = [...montosAgregados];
        let index = array.indexOf(monto)
        if (index !== -1) {
            array.splice(index, 1);
            setMontosAgregados(array);
        }
    }

    return (
        <>
            <div>
                {montosAgregados.map((monto, indice) => {
                    if (equipoOrigenId === monto.idEquipoOrigen)
                        return (
                            <div className="d-block row mt-2 text-center" key={indice}>
                                <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                                decimalScale={0} prefix={'$'} value={monto.monto}
                                                displayType={"text"} required/>
                                {descripcionMontoAgregado(monto)}
                                <FontAwesomeIcon className="pointer ml-2" icon={faTrash} onClick={() => borrarMonto(monto)}/>
                            </div>)
                })}
            </div>
        </>
    );
};
export default VistaMontosAgregados;
