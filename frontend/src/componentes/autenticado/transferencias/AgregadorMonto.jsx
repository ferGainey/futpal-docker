import React, {useEffect, useState} from 'react';
import CurrencyFormat from "react-currency-format";

const AgregadorMonto = ({setMontosAgregados, montosAgregados, temporadasCombo, setMostrar, idEquipoOrigen, idEquipoDestino}) => {

    const [numeroTemporada, setNumeroTemporada] = useState();
    const [periodo, setPeriodo] = useState();
    const [monto, setMonto] = useState();
    const [periodosCombo, setPeriodosCombo] = useState([]);

    useEffect(() => {
        llenarComboPeriodo();
    }, []);

    const agregarMonto = () => {
        let montosActualizados = montosAgregados;
        let montoAAgregar = {
            monto: monto,
            numeroTemporada: numeroTemporada,
            periodo: periodo,
            idEquipoOrigen: idEquipoOrigen,
            idEquipoDestino: idEquipoDestino
        }
        montosActualizados.push(montoAAgregar);
        setMontosAgregados(montosActualizados);
        setMostrar(false);
    }

    const llenarComboPeriodo = () => {
        let periodosParaCombo = [];
        let principio = {
            id: 1,
            descripcion: "Principio"
        }
        periodosParaCombo.push(principio);
        let mitad = {
            id: 2,
            descripcion: "Mitad"
        }
        periodosParaCombo.push(mitad);
        let final = {
            id: 3,
            descripcion: "Final"
        }
        periodosParaCombo.push(final);
        setPeriodosCombo(periodosParaCombo);
    }

    const hayCamposSinCompletar = () => {
        return monto == null || numeroTemporada == null || periodo == null;
    }

    return (
        <>
            <div className="row justify-content-center">
                <div className="mr-2 col-10 col-md-auto">
                    <CurrencyFormat id="transferencias-monto" value={monto} className="form-control" thousandSeparator={'.'}
                                    decimalSeparator={','} decimalScale={0} prefix={'$'} onValueChange={value => setMonto(value.floatValue)}
                                    placeholder="Ingrese monto" required/>
                </div>

                <div className="mr-2 col-md-auto mt-1 mt-md-0">
                    <select id="transferencias-temporadas" className="form-control" onChange={e => setNumeroTemporada(e.target.value)} required>
                        <option key="placeholder" disabled selected>Temporada...</option>
                        <option value={temporadasCombo[0].valor - 1}>Temp. {temporadasCombo[0].valor - 1}</option>
                        {temporadasCombo.map((temporada, indice) => {
                            return <option key={indice} value={temporada.valor}>{temporada.descripcion}</option>
                        })}
                    </select>
                </div>

                <div className="mr-2 col-md-auto mt-1 mt-md-0">
                    <select id="transferencias-periodos" className="form-control" onChange={e => setPeriodo(JSON.parse(e.target.value))} required>
                        <option key="placeholder" disabled selected>Período...</option>
                        {periodosCombo.map((periodo, indice) => {
                            return <option key={indice} value={JSON.stringify(periodo)}>{periodo.descripcion}</option>
                        })}
                    </select>
                </div>

                <button id="boton-cargar-monto" className="btn btn-success col-3 col-md-auto mt-1 mt-md-0" type="submit" onClick={() => agregarMonto()} disabled={hayCamposSinCompletar()}>+</button>
            </div>
        </>
    );
};
export default AgregadorMonto;
