import React, {useEffect, useState} from 'react';


const AgregadorTraspasoJugador = ({setJugadoresAgregados, jugadoresAgregados, jugadores, temporadasCombo, setMostrar, idEquipoOrigen, idEquipoDestino}) => {

    const [numeroTemporada, setNumeroTemporada] = useState();
    const [periodo, setPeriodo] = useState();
    const [jugador, setJugador] = useState();
    const [periodosCombo, setPeriodosCombo] = useState([]);

    useEffect(() => {
        llenarComboPeriodo();
    }, []);

    const agregarJugador = () => {
        let jugadoresActualizados = jugadoresAgregados;
        let jugadorAAgregar = {
            jugador: jugador,
            numeroTemporada: numeroTemporada,
            periodo: periodo,
            idEquipoOrigen: idEquipoOrigen,
            idEquipoDestino: idEquipoDestino
        }
        jugadoresActualizados.push(jugadorAAgregar);
        setJugadoresAgregados(jugadoresActualizados);
        setMostrar(false);
    }

    const hayCamposSinCompletar = () => {
        return jugador == null || numeroTemporada == null || periodo == null;
    }

    const llenarComboPeriodo = () => {
        let periodosParaCombo = [];
        let principio = {
            id: 1,
            descripcion: "Principio"
        }
        periodosParaCombo.push(principio);
        let mitad = {
            id: 2,
            descripcion: "Mitad"
        }
        periodosParaCombo.push(mitad);

        setPeriodosCombo(periodosParaCombo);
    }

    return (
        <>
            <div className="row justify-content-center">
                <div className="mr-2">
                    <select id="jugadores" className="form-control col-10 col-md-auto" onChange={e => setJugador(JSON.parse(e.target.value))} required>
                        <option key="placeholder" disabled selected>Jugador...</option>
                        {jugadores.map((jugador, indice) => {
                            return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                        })}
                    </select>
                </div>

                <div className="mr-2">
                    <select id="transferencias-temporadas" className="form-control mt-1 mt-md-0" onChange={e => setNumeroTemporada(e.target.value)} required>
                        <option key="placeholder" disabled selected>Temporada...</option>
                        {temporadasCombo.map((temporada, indice) => {
                            return <option key={indice} value={temporada.valor}>{temporada.descripcion}</option>
                        })}
                    </select>
                </div>

                <div className="mr-2">
                    <select id="transferencias-periodos" className="form-control mt-1 mt-md-0" onChange={e => setPeriodo(JSON.parse(e.target.value))} required>
                        <option key="placeholder" disabled selected>Período...</option>
                        {periodosCombo.map((periodo, indice) => {
                            return <option key={indice} value={JSON.stringify(periodo)}>{periodo.descripcion}</option>
                        })}
                    </select>
                </div>

                <button id="boton-cargar-jugador" className="btn btn-success mt-1 mt-md-0" type="submit" onClick={() => agregarJugador()} disabled={hayCamposSinCompletar()}>+</button>
            </div>
        </>
    );
};
export default AgregadorTraspasoJugador;
