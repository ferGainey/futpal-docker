import React, {useEffect, useState} from 'react';
import ServicioTransferencia from "../../../servicios/ServicioTransferencia";
import Transferencia from "./Transferencia";
import ServicioEquipo from "../../../servicios/ServicioEquipo";

const MisTransferencias = () => {


    const [transferencias, setTransferencias] = useState([]);
    const [equipoPropioId, setEquipoPropioId] = useState();

    useEffect(() => {
        let tokenUsuario = localStorage.getItem("usuario");
        let idUsuarioToken = JSON.parse(tokenUsuario).id;
        ServicioEquipo.obtenerDatosEquipo(idUsuarioToken).then((respuestaDatosEquipo) => {
            setEquipoPropioId(respuestaDatosEquipo.data.idEquipo);
        });

        ServicioTransferencia.obtenerMisTransferencias().then((respuestaTransferencias) => {
            setTransferencias(respuestaTransferencias.data);
        });
    }, []);


    return (
        <>
            <div className="letra-blanca col-9 col-md-10 col-xl-11">
                <h3 className="titulo-login mb-1 pt-3">Mis Transferencias</h3>
                <div>
                    {transferencias ? transferencias.map((transferencia) => {
                        return <Transferencia key={transferencia.id} datos={transferencia} equipoPropioId={equipoPropioId} setTransferencias={setTransferencias}/>
                    }) : null}
                </div>
            </div>
        </>
    );
}
export default MisTransferencias;
