import React, {useEffect, useState} from 'react';

const AgregadorPrestamoJugador = ({setJugadoresAgregados, jugadoresPrestamoAgregados, jugadores, temporadasCombo, setMostrar, idEquipoOrigen, idEquipoDestino}) => {

    const [numeroTemporadaComienzo, setNumeroTemporadaComienzo] = useState();
    const [numeroTemporadaFinal, setNumeroTemporadaFinal] = useState();
    const [periodoComienzo, setPeriodoComienzo] = useState();
    const [periodoFinal, setPeriodoFinal] = useState();
    const [jugador, setJugador] = useState();
    const [periodosCombo, setPeriodosCombo] = useState([]);

    useEffect(() => {
        llenarComboPeriodo();
    }, []);

    const agregarJugador = () => {
        let jugadoresActualizados = jugadoresPrestamoAgregados;
        let jugadorAAgregar = {
            jugador: jugador,
            numeroTemporadaComienzo: numeroTemporadaComienzo,
            numeroTemporadaFinal: numeroTemporadaFinal,
            periodoComienzo: periodoComienzo,
            periodoFinal: periodoFinal,
            idEquipoOrigen: idEquipoOrigen,
            idEquipoDestino: idEquipoDestino
        }
        jugadoresActualizados.push(jugadorAAgregar);
        setJugadoresAgregados(jugadoresActualizados);
        setMostrar(false);
    }

    const llenarComboPeriodo = () => {
        let periodosParaCombo = [];
        let principio = {
            id: 1,
            descripcion: "Principio"
        }
        periodosParaCombo.push(principio);
        let mitad = {
            id: 2,
            descripcion: "Mitad"
        }
        periodosParaCombo.push(mitad);

        setPeriodosCombo(periodosParaCombo);
    }

    const hayCamposSinCompletar = () => {
        return jugador == null || numeroTemporadaComienzo == null || periodoComienzo == null
            || numeroTemporadaFinal == null || periodoFinal == null;
    }

    return (
        <>
            <div className="row justify-content-center">
                <div className="mr-2">
                    <select id="jugadores" className="form-control" onChange={e => setJugador(JSON.parse(e.target.value))} required>
                        <option key="placeholder" disabled selected>Jugador...</option>
                        {jugadores.map((jugador, indice) => {
                            return <option key={indice} value={JSON.stringify(jugador)}>{jugador.nombre}</option>
                        })}
                    </select>
                </div>

                <div className="mr-2">
                    <select id="transferencias-temporadas-comienzo" className="form-control mt-1 mt-md-0" onChange={e => setNumeroTemporadaComienzo(e.target.value)} required>
                        <option key="placeholder" disabled selected>Temporada comienzo...</option>
                        {temporadasCombo.map((temporada, indice) => {
                            return <option key={indice} value={temporada.valor}>{temporada.descripcion}</option>
                        })}
                    </select>
                </div>

                <div className="mr-2">
                    <select id="transferencias-periodos-comienzo" className="form-control mt-1 mt-md-0" onChange={e => setPeriodoComienzo(JSON.parse(e.target.value))} required>
                        <option key="placeholder" disabled selected>Período comienzo...</option>
                        {periodosCombo.map((periodo, indice) => {
                            return <option key={indice} value={JSON.stringify(periodo)}>{periodo.descripcion}</option>
                        })}
                    </select>
                </div>

                <button id="boton-cargar-jugador" className="btn btn-success d-none d-md-block" type="submit" onClick={() => agregarJugador()} disabled={hayCamposSinCompletar()}>+</button>
            </div>
            <div className="row justify-content-center mt-3">
                <div className="mr-2">
                    <select id="transferencias-temporadas-final" className="form-control" onChange={e => setNumeroTemporadaFinal(e.target.value)} required>
                        <option key="placeholder" disabled selected>Temporada fin...</option>
                        {temporadasCombo.map((temporada, indice) => {
                            return <option key={indice} value={temporada.valor}>{temporada.descripcion}</option>
                        })}
                    </select>
                </div>

                <div className="mr-2">
                    <select id="transferencias-periodos-final" className="form-control mt-1 mt-md-0" onChange={e => setPeriodoFinal(JSON.parse(e.target.value))} required>
                        <option key="placeholder" disabled selected>Período fin...</option>
                        {periodosCombo.map((periodo, indice) => {
                            return <option key={indice} value={JSON.stringify(periodo)}>{periodo.descripcion}</option>
                        })}
                    </select>
                </div>
            </div>

            <div className="row justify-content-center mt-3">
                <button id="boton-cargar-jugador" className="btn btn-success d-md-none" type="submit" onClick={() => agregarJugador()} disabled={hayCamposSinCompletar()}>+</button>
            </div>
        </>
    );
};
export default AgregadorPrestamoJugador;
