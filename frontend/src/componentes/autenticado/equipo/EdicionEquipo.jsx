import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import BarraNavegacionLogueado from '../BarraNavegacionLogueado.jsx';
import API from '../../../api';
import $ from "jquery";

const EdicionEquipo = () => {

  const [nombreEquipo, setNombreEquipo] = useState();
  const [textoEquipo, setTextoEquipo] = useState();
  const [idUsuarioLogueado, setIdUsuarioLogueado] = useState();
  const history = useHistory();

  useEffect(() => {
    let tokenUsuario = localStorage.getItem("usuario");
    if (tokenUsuario == null) {
      history.push("/");
    }
    else {
      let idUsuarioToken = JSON.parse(tokenUsuario).id
      setIdUsuarioLogueado(idUsuarioToken);
      completarTextoEquipo(idUsuarioToken);
    }
  }, [history]);

  const completarTextoEquipo = async (idUsuario) => {
    const respuesta = await obtenerDatosEquipo(idUsuario);
    if (respuesta != null && respuesta.status === 200) {
        if (respuesta.data.nombreEquipo !== "" && respuesta.data.nombreEquipo != null) {
            setTextoEquipo(respuesta.data.nombreEquipo);
        }
        else {
            setTextoEquipo(respuesta.data.mensaje);
        }
    }
  }

  // ToDo: usar ServicioEquipo en lugar de esto. Resolver el tema del manejo de errores antes
  const obtenerDatosEquipo = async (idUsuario) => {
    return await API('/api/datos-equipo', {
        method: 'get',
        params: {idUsuario: idUsuario}
      }).catch(error => {
        console.log(error.response);
        mostrarRespuestaErrorAlIntentarEditarEquipo();
      });
  }

  const editarEquipo = async (evento) => {
    evento.preventDefault()
    let datosEquipo = {
        idUsuario: idUsuarioLogueado,
        nuevoNombreEquipo: nombreEquipo
    };

    const respuesta = await actualizarEquipo(datosEquipo);
    mostrarRespuestaGuardado(respuesta);
  }

  const actualizarEquipo = async (equipo) => {
    return await API('/api/editar-equipo', {
      method: 'post',
      data: equipo
    }).catch(error => {
      console.log(error.response);
      mostrarRespuestaErrorAlIntentarEditarEquipo();
    });
}

  const mostrarRespuestaGuardado = (respuesta) => {
    if(respuesta != null && respuesta.status === 200) {
        let alerta = $('#alerta-equipo-editado');
        alerta.show();
        setTextoEquipo(respuesta.data.nombreEquipoActualizado);
    }
  }

  const mostrarRespuestaErrorAlIntentarEditarEquipo = () => {
    let alerta = $('#alerta-error-editar-equipo');
    alerta.show();
  }


  return (
          <div>
            <BarraNavegacionLogueado />
            <div className="letra-blanca">
                <h3 className="titulo-login pt-3">Editar Equipo</h3>
                <p id="id-texto-equipo-edicion" className="titulo-login">{textoEquipo}</p>
                <div className="container">
                  <form onSubmit={(e) => editarEquipo(e)}>
                    <div className="form-group">
                      <label>Nombre del equipo</label>
                      <input id="campo-nombre-equipo-edicion" className="form-control" type="text" onChange={e => setNombreEquipo(e.target.value)} required/>
                    </div>
                      <button id="boton-actualizar-equipo" className="btn btn-success" type="submit">Actualizar</button>
                  </form>
                  <div id="alerta-equipo-editado" className="alert alert-success alert-dismissible collapse alerta-futpal" role="alert">
                    <p id="texto-mensaje-edicion-equipo">Equipo actualizado correctamente.</p>
                      <button type="button" className="close" onClick={() => $('#alerta-equipo-editado').hide()} aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div id="alerta-error-editar-equipo" className="alert alert-danger alert-dismissible collapse alerta-futpal" role="alert">
                      <p id="texto-error-editar-equipo">Se produjo un error al intentar editar el equipo.</p>
                      <button type="button" className="close" onClick={() => $('#alerta-error-editar-equipo').hide()} aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                </div>
            </div>
          </div>
          );
};
export default EdicionEquipo;
