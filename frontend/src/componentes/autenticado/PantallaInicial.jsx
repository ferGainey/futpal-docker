import React from 'react';
import BarraNavegacionNoLogueado from '../autenticacion/BarraNavegacionNoLogueado.jsx';

const PantallaInicial = () => {

  return (
    <div>
    <BarraNavegacionNoLogueado />
      <div className="mensaje-bienvenida-inicio letra-blanca overflow-hidden">
          <h2 id="titulo-bienvenida-pantalla-inicial">¡Bienvenido a Futpal!</h2>
          <p>Registrate para poder participar de las ligas creadas.</p>
      </div>
    </div>
          );
};
export default PantallaInicial;
