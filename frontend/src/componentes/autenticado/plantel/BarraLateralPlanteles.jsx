import React from 'react';

const BarraLateralPlanteles = ({setPlantelesDeEquiposActivo, setBuscadorJugadoresActivo}) => {

    const irAPlanteles = () => {
        setPlantelesDeEquiposActivo(true);
        setBuscadorJugadoresActivo(false);
    }

    const irABuscadorJugadores = () => {
        setPlantelesDeEquiposActivo(false);
        setBuscadorJugadoresActivo(true);
    }

    return (
        <div className="col-3 col-md-2 col-xl-1 px-1 bg-dark" id="sticky-sidebar">
            <div className="nav flex-column flex-nowrap overflow-auto text-white ">
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="planteles-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irAPlanteles()}>Planteles</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="buscador-jugadores-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irABuscadorJugadores()}>Buscador jugadores</a>
            </div>
        </div>
    );
};
export default BarraLateralPlanteles;
