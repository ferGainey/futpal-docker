import React, {useEffect, useState} from 'react';
import 'bootstrap';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const PopupEquipoAprobado = ({mostrar, setMostrar}) => {

    const [show, setShow] = useState(false);

    useEffect(() => {
        setShow(mostrar);
    }, [mostrar]);

    const cerrarPopUp = () => {
        setShow(false);
        setMostrar(false);
    }

    return (
        <>
        <Modal
          show={show}
          onHide={cerrarPopUp}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Equipo aprobado</Modal.Title>
          </Modal.Header>
          <Modal.Body> 
            <p id="popup-equipo-aprobado-texto">Se ha aprobado el equipo exitosamente.</p>
          </Modal.Body>
          <Modal.Footer>
            <Button id="agregador-jugador-boton-agregar" variant="primary" onClick={cerrarPopUp}>Aceptar</Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};
export default PopupEquipoAprobado;
