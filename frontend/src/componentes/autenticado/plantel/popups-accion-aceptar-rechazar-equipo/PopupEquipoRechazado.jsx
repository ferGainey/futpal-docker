import React, {useEffect, useState} from 'react';
import 'bootstrap';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import API from '../../../../api';

const PopupEquipoRechazado = ({mostrar, setMostrar, equipoId, setIdEquipoElegido}) => {

  const [show, setShow] = useState(false);
  const [motivoRechazo, setMotivoRechazo] = useState("");

    useEffect(() => {
        setShow(mostrar);
    }, [mostrar]);

    const cerrarPopUp = () => {
        setShow(false);
        setMostrar(false);
    }

    const rechazarEquipo = async () => {
      let requestRechazarEquipo = {
          equipoId: equipoId,
          motivoRechazo: motivoRechazo
      };

      await API('/api/rechazar-equipo-pendiente-aprobacion', {
      method: 'put',
      data: requestRechazarEquipo
      }).catch(error => {
      console.log(error.response);
      });
      setIdEquipoElegido(null);
      cerrarPopUp();
  }

    return (
        <>
        <Modal
          show={show}
          onHide={cerrarPopUp}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Equipo rechazado</Modal.Title>
          </Modal.Header>
          <Modal.Body>             
            <div className="form-group">
              <label>Escriba el motivo por el cual rechazó el equipo:</label>
              <textarea className="form-control" id="popup-equipo-rechazo-motivo" rows="3" onChange={e => setMotivoRechazo(e.target.value)}/>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button id="popup-equipo-rechazado-enviar" variant="primary" onClick={rechazarEquipo}>Enviar</Button>
          </Modal.Footer>
        </Modal>
      </>
    );
};
export default PopupEquipoRechazado;
