import React, {useEffect, useState} from 'react';
import BarraNavegacionLogueado from '../BarraNavegacionLogueado.jsx';
import Plantel from "./Plantel";
import JugadoresPrestados from "./JugadoresPrestados";
import BarraLateralMiPlantel from "./BarraLateralMiPlantel";

const MiPlantel = () => {

    const [plantelActivo, setPlantelActivo] = useState(false);
    const [misJugadoresPrestadosActivo, setMisJugadoresPrestadosActivo] = useState(false);

    useEffect(() => {
        setPlantelActivo(true);
    }, []);


    return (
        <>
            <BarraNavegacionLogueado/>
            <div className="row ml-0 mr-0">
                <BarraLateralMiPlantel setPlantelActivo={setPlantelActivo}
                                       setMisJugadoresPrestadosActivo={setMisJugadoresPrestadosActivo}/>
                {plantelActivo ? <Plantel/> : null}
                {misJugadoresPrestadosActivo ? <JugadoresPrestados/> : null}
            </div>
        </>
    );
};
export default MiPlantel;
