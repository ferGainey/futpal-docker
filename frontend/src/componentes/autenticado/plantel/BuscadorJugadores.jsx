import React, {useEffect, useState} from 'react';
import ServicioJugador from "../../../servicios/ServicioJugador";
import TablaJugadoresBuscador from "./TablaJugadoresBuscador";

const BuscadorJugadores = () => {

    const [busquedaPorIdTransfermarktActiva, setBusquedaPorIdTransfermarktActiva] = useState(false);
    const [busquedaPorNombreActiva, setBusquedaPorNombreActiva] = useState(false);

    const [idTransfermarkt, setIdTransfermarkt] = useState();
    const [nombre, setNombre] = useState();

    const [jugadoresFiltrados, setJugadoresFiltrados] = useState([]);
    const [mensajeBusqueda, setMensajeBusqueda] = useState("Cargue al menos un filtro para realizar la búsqueda");

    useEffect(() => {
    }, []);

    const buscarJugadores = () => {
        const filtroIdTransfermarkt = busquedaPorIdTransfermarktActiva ? idTransfermarkt : null;
        const filtroNombreJugador = busquedaPorNombreActiva ? nombre : null;
        ServicioJugador.obtenerJugadoresFiltrados(filtroIdTransfermarkt, filtroNombreJugador).then((jugadores) => {
            console.log(jugadores.data)
            setJugadoresFiltrados(jugadores.data);
            if (jugadores.data.length === 0) {
                setMensajeBusqueda("No se encontraron jugadores para los filtros seleccionados");
            }
        });
    }

    const estaLaBusquedaHabilitada = () => {
        const CANTIDAD_MINIMA_CARACTERES_NOMBRE = 3;
        return (idTransfermarkt != null && idTransfermarkt !== 0 && idTransfermarkt !== '' && busquedaPorIdTransfermarktActiva) ||
            (nombre != null && nombre.toString().length >= CANTIDAD_MINIMA_CARACTERES_NOMBRE && busquedaPorNombreActiva);
    }

    const hayJugadores = () => {
        return jugadoresFiltrados.length > 0;
    }

    return (
        <div className="letra-blanca col-9 col-md-10 col-xl-11">
            <h3 className="ml-5 pt-3">
                Buscador jugadores
            </h3>
            <div className="form-check mt-4 d-flex align-items-center">
                <input id="busqueda-por-id-transfermarkt" className="form-check-input pointer" type="checkbox" checked={busquedaPorIdTransfermarktActiva}
                       onClick={() => setBusquedaPorIdTransfermarktActiva(!busquedaPorIdTransfermarktActiva)}/>
                <input id="input-id-transfermarkt" placeholder="ID Transfermarkt" className="form-control col-6 col-md-3" type="number"
                       onChange={e => setIdTransfermarkt(e.target.value)} disabled={!busquedaPorIdTransfermarktActiva}/>
            </div>
            <div className="form-check mt-3 d-flex align-items-center">
                <input id="busqueda-por-nombre" className="form-check-input pointer" type="checkbox" checked={busquedaPorNombreActiva}
                       onClick={() => setBusquedaPorNombreActiva(!busquedaPorNombreActiva)}/>
                <input id="input-nombre" placeholder="Nombre" className="form-control col-6 col-md-3" type="text"
                       onChange={e => setNombre(e.target.value)} disabled={!busquedaPorNombreActiva}/>
            </div>
            <button id="buscar-jugadores" type="button" className="btn btn-primary mt-3 ml-4" onClick={() => buscarJugadores()}
                    disabled={!estaLaBusquedaHabilitada()}>Buscar</button>
            {hayJugadores() ? <TablaJugadoresBuscador jugadores={jugadoresFiltrados}/> : <p className="mt-4 text-center">{mensajeBusqueda}</p>}
        </div>
    );
};
export default BuscadorJugadores;
