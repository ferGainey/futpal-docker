import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import API from '../../../api';
import AgregadorJugador from './AgregadorJugador.jsx';
import TablaJugadores from './TablaJugadores.jsx';
import $ from "jquery";
import ServicioEquipo from "../../../servicios/ServicioEquipo";

const Plantel = () => {

  const [textoEquipo, setTextoEquipo] = useState();
  const [mensajeEstadoEquipo, setMensajeEstadoEquipo] = useState();
  const [mensajeCambioEstado, setMensajeCambioEstado] = useState();
  const [equipoId, setEquipoId] = useState();
  const [estadoEquipoId, setEstadoEquipoId] = useState();
  const [jugadores, setJugadores] = useState([]);
  const [permisoParaAdministrarJugador, setPermisoParaAdministrarJugador] = useState();
  const history = useHistory();

  const [mostrarAgregadorJugador, setMostrarAgregadorJugador] = useState(false);

  useEffect(() => {
    if (mostrarAgregadorJugador === false && equipoId != null) {
      obtenerJugadores();
    }
  }, [mostrarAgregadorJugador]);

  useEffect(() => {
    permitirSolicitarAprobacionSegunEstado(estadoEquipoId, equipoId);
    permitirBorradoJugadorSegunEstado(estadoEquipoId, equipoId);
    mostrarMensajeCambioEstadoSoloSiEsRechazado(estadoEquipoId);
  }, [estadoEquipoId]);

  useEffect(() => {
    let tokenUsuario = localStorage.getItem("usuario");
    if (tokenUsuario == null) {
      history.push("/");
    }
    else {
      let idUsuarioToken = JSON.parse(tokenUsuario).id
      cargarDatosEquipo(idUsuarioToken);
    }
  }, []);

  useEffect(() => {
    if (equipoId != null) {
      obtenerJugadores();
    }
  }, [equipoId]);

  const permitirSolicitarAprobacionSegunEstado = (idEstado, idEquipo) => {
    //4 es rechazado y 5 inicial
    if ((idEstado === 4 || idEstado === 5) && idEquipo != null) {
      $('#mi-plantel-solicitar-aprobacion').css("display", "initial");
      $("#mi-plantel-boton-agregar-jugador").css("display", "initial");
    }
    else {
      $('#mi-plantel-solicitar-aprobacion').css("display", "none");
      $("#mi-plantel-boton-agregar-jugador").css("display", "none");
    }
  }

  const mostrarMensajeCambioEstadoSoloSiEsRechazado = (idEstado) => {
    //4 es rechazado
    if ((idEstado === 4)) {
      $('#mi-plantel-mensaje-cambio-estado').css("display", "initial");
    }
    else {
      $('#mi-plantel-mensaje-cambio-estado').css("display", "none");
    }
  }

  const permitirBorradoJugadorSegunEstado = (idEstado, idEquipo) => {
    //4 es rechazado y 5 inicial
    if ((idEstado === 4 || idEstado === 5) && idEquipo != null) {
      setPermisoParaAdministrarJugador(true);
    }
    else {
      setPermisoParaAdministrarJugador(false);
    }
  }

  const cargarDatosEquipo = async (idUsuario) => {
    const respuesta = await ServicioEquipo.obtenerDatosEquipo(idUsuario);
    if (respuesta != null && respuesta.status === 200) {
        if (respuesta.data.nombreEquipo !== "" && respuesta.data.nombreEquipo != null) {
            setTextoEquipo(respuesta.data.nombreEquipo);
            setMensajeEstadoEquipo(respuesta.data.mensajeEstadoEquipo);
            setEquipoId(respuesta.data.idEquipo);
            setEstadoEquipoId(respuesta.data.estadoEquipoId);
            setMensajeCambioEstado(respuesta.data.motivoCambioEstado);
        }
        else {
            setTextoEquipo(respuesta.data.mensaje);
        }
        permitirSolicitarAprobacionSegunEstado(respuesta.data.estadoEquipoId, respuesta.data.idEquipo);
        permitirBorradoJugadorSegunEstado(respuesta.data.estadoEquipoId, respuesta.data.idEquipo);
    }
  }

  const obtenerJugadores = async () => {
    const respuesta = await servicioObtenerJugadores();
    if (respuesta != null && respuesta.status === 200) {
        if (respuesta.data !== "" && respuesta.data != null) {
            setJugadores(respuesta.data);        
        }
    }
  }

  const servicioObtenerJugadores = async () => {
    return await API('/api/jugadores', {
        method: 'get',
        params: {equipoId: equipoId}
      }).catch(error => {
        console.log(error.response);
      });
  }

  const abrirAgregadorJugador = () => {
    setMostrarAgregadorJugador(true);
  }

  const solicitarAprobacionEquipo = async () => {
    let requestSolicitarAprobacion = {
      equipoId: equipoId
    };

    const respuesta = await API('/api/solicitar-aprobacion-equipo', {
    method: 'put',
    data: requestSolicitarAprobacion
    }).catch(error => {
    console.log(error.response);
    });
    
    if (respuesta != null && respuesta.status === 200) {
      let tokenUsuario = localStorage.getItem("usuario");
      let idUsuarioToken = JSON.parse(tokenUsuario).id;
      cargarDatosEquipo(idUsuarioToken);
    }
  }

  return (
          <>
            <div className="col-9 col-md-10 col-xl-11 letra-blanca">
              <h3 className="titulo-login pt-3">
              <span> {equipoId != null ? "Mi plantel " : ""} </span>
              <span id="mi-plantel-texto-equipo">{textoEquipo}</span>
              </h3>
              <div>
                <p id="mi-plantel-mensaje-estado-equipo">{mensajeEstadoEquipo}</p>
                <p id="mi-plantel-mensaje-cambio-estado">Motivo: {mensajeCambioEstado}</p>
              </div>
              <div>
                {mostrarAgregadorJugador ? <AgregadorJugador equipoId={equipoId} mostrar={mostrarAgregadorJugador} setMostrar={setMostrarAgregadorJugador}/> : null}
                <button id="mi-plantel-boton-agregar-jugador" disabled={equipoId == null} className="btn btn-success" onClick={() => abrirAgregadorJugador()}>Agregar jugador</button>
                <button id="mi-plantel-solicitar-aprobacion" type="button" className="btn btn-primary ml-3" onClick={() => solicitarAprobacionEquipo()}>Solicitar aprobación</button>
                <TablaJugadores jugadores={jugadores} permisoParaAdministrarJugador={permisoParaAdministrarJugador} setJugadores={setJugadores} equipoId={equipoId}/>
              </div>
            </div>
          </>
          );
};
export default Plantel;
