import React from 'react';

const BarraLateralMiPlantel = ({setPlantelActivo, setMisJugadoresPrestadosActivo}) => {

    const irAPlantel = () => {
        setPlantelActivo(true);
        setMisJugadoresPrestadosActivo(false);
    }

    const irAMisJugadoresPrestados = () => {
        setPlantelActivo(false);
        setMisJugadoresPrestadosActivo(true);
    }

    return (
        <div className="col-3 col-md-2 col-xl-1 px-1 bg-dark" id="sticky-sidebar">
            <div className="nav flex-column flex-nowrap overflow-auto text-white ">
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="plantel-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irAPlantel()}>Plantel</a>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a id="mis-jugadores-prestados-side-nav" className="nav-link text-center pointer letra-blanca" onClick={() => irAMisJugadoresPrestados()}>Mis jugadores prestados</a>
            </div>
        </div>
    );
};
export default BarraLateralMiPlantel;
