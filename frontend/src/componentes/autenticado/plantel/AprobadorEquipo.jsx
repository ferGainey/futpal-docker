import React, {useEffect, useState} from 'react';
import API from '../../../api';
import $ from "jquery";
import 'bootstrap';
import ServicioEquipo from '../../../servicios/ServicioEquipo';
import PopupEquipoAprobado from './popups-accion-aceptar-rechazar-equipo/PopupEquipoAprobado.jsx';
import PopupEquipoRechazado from './popups-accion-aceptar-rechazar-equipo/PopupEquipoRechazado.jsx';

const AprobadorEquipo = ({idEquipoElegido, setIdEquipoElegido, setEquiposAprobados}) => {

    const [equiposPendienteAprobacion, setEquiposPendienteAprobacion] = useState([]);
    const [mostrarPopupEquipoAprobado, setMostrarPopupEquipoAprobado] = useState(false);
    const [mostrarPopupEquipoRechazado, setMostrarPopupEquipoRechazado] = useState(false);

    useEffect(() => {
        let rolesUsuarioLogueado = JSON.parse(localStorage.getItem("usuario")).roles;
        if (rolesUsuarioLogueado.includes("ROL_ADMINISTRADOR")) {
            obtenerEquiposPendienteDeAprobacion();
            $('#aprobador-equipo-botones-aprobacion-equipo').css("display", "none");
        }
        else {
            $('#aprobador-equipo-label-equipos-pendiente-aprobacion').css("display", "none");
            $('#aprobador-equipo-equipos-pendiente-aprobacion').css("display", "none");
            $('#aprobador-equipo-botones-aprobacion-equipo').css("display", "none");
        }
    }, []);

    useEffect(() => {
        if (idEquipoElegido != null) {
            $('#aprobador-equipo-botones-aprobacion-equipo').css("display", "none");
            equiposPendienteAprobacion.forEach(equipo => {
                //2 es NO_APROBADO, que es lo mismo que pendiente de aprobación
                if (equipo.id == idEquipoElegido && equipo.estadoEquipo.id == 2) {
                    $('#aprobador-equipo-botones-aprobacion-equipo').css("display", "initial");
                    $('#planteles-equipos-aprobados').val("DEFAULT");
                }
            })
        }
    }, [idEquipoElegido]);

    useEffect(() => {
        actualizarDatosEquipos();
    }, [mostrarPopupEquipoRechazado])

    const obtenerEquiposPendienteDeAprobacion = async () => {
        const respuesta = await servicioObtenerEquiposPendienteAprobacion();
        if (respuesta != null && respuesta.status === 200) {
            setEquiposPendienteAprobacion(respuesta.data);
        }
    }

    const aprobarEquipo = async () => {
        let requestAprobarEquipo = {
            equipoId: idEquipoElegido
        };

        const respuesta = await API('/api/aprobar-equipo-pendiente-aprobacion', {
        method: 'put',
        data: requestAprobarEquipo
        }).catch(error => {
        console.log(error.response);
        });
        
        if (respuesta != null && respuesta.status === 200) {
            actualizarDatosEquipos();
            setMostrarPopupEquipoAprobado(true);
            setIdEquipoElegido(null);
            //actualizar jugadores
        }
    }

    const actualizarDatosEquipos = async () => {
        obtenerEquiposPendienteDeAprobacion();
        let respuestaEquiposAprobados = await ServicioEquipo.obtenerEquiposAprobados(); 
        setEquiposAprobados(respuestaEquiposAprobados.data);
        $('#aprobador-equipo-equipos-pendiente-aprobacion').val("DEFAULT");
        $('#aprobador-equipo-botones-aprobacion-equipo').css("display", "none");
    }

    const rechazarEquipo = async () => {
        setMostrarPopupEquipoRechazado(true);
    }

    const servicioObtenerEquiposPendienteAprobacion = async () => {
    return await API('/api/obtener-equipos-pendiente-aprobacion', {
        method: 'get',
      }).catch(error => {
        console.log(error.response);
      });   
    }

    return (
      <>
        <label id="aprobador-equipo-label-equipos-pendiente-aprobacion">Equipos pendiente de aprobación</label>
        <select id="aprobador-equipo-equipos-pendiente-aprobacion" className="form-control w-25" onChange={(opcion)=> setIdEquipoElegido(opcion.target.value)} defaultValue={'DEFAULT'} >
            <option value="DEFAULT" disabled>Elija un equipo...</option>
            {equiposPendienteAprobacion.map((equipo, indice) => {
                return <option key={indice} value={equipo.id}>{equipo.nombreEquipo}</option>
            })}
        </select>
        <div className="mt-1" id="aprobador-equipo-botones-aprobacion-equipo">
            <button id="aprobador-equipo-rechazar-equipo-pendiente-aprobacion" type="button" className="btn btn-danger mr-1 mt-1" onClick={() => rechazarEquipo()}>Rechazar</button>
            <button id="aprobador-equipo-aceptar-equipo-pendiente-aprobacion" type="button" className="btn btn-success mt-1" onClick={() => aprobarEquipo()}>Aprobar</button>
        </div>

        {mostrarPopupEquipoAprobado ? <PopupEquipoAprobado mostrar={mostrarPopupEquipoAprobado} setMostrar={setMostrarPopupEquipoAprobado}/> : null}
        {mostrarPopupEquipoRechazado ? <PopupEquipoRechazado mostrar={mostrarPopupEquipoRechazado} setMostrar={setMostrarPopupEquipoRechazado} equipoId={idEquipoElegido} setIdEquipoElegido={setIdEquipoElegido}/> : null}
      </>
    );
};
export default AprobadorEquipo;
