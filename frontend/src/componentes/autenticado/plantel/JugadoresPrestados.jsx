import React, {useEffect, useState} from 'react';
import {useHistory} from "react-router-dom";
import ServicioJugador from "../../../servicios/ServicioJugador";
import TablaJugadoresPrestados from "./TablaJugadoresPrestados";
import ServicioEquipo from "../../../servicios/ServicioEquipo";

const JugadoresPrestados = () => {

    const [jugadoresPrestados, setJugadoresPrestados] = useState([]);
    const history = useHistory();


    useEffect(() => {
        let tokenUsuario = localStorage.getItem("usuario");
        if (tokenUsuario == null) {
            history.push("/");
        }
        else {
            let idUsuarioToken = JSON.parse(tokenUsuario).id;
            ServicioEquipo.obtenerDatosEquipo(idUsuarioToken).then((respuesta) => {
                ServicioJugador.obtenerJugadoresPrestados(respuesta.data.idEquipo).then((jugadores) => {
                    setJugadoresPrestados(jugadores.data);
                });
            })

        }

    }, [history]);

    return (
        <>
            <div className="col-9 col-md-10 col-xl-11 letra-blanca text-center">
                <h3 className="mt-3 mb-3">Jugadores prestados a otros equipos</h3>
                <TablaJugadoresPrestados jugadores={jugadoresPrestados}/>
            </div>
        </>
    );
}
export default JugadoresPrestados;
