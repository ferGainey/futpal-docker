import React, {useEffect, useState} from 'react';
import BarraNavegacionLogueado from '../BarraNavegacionLogueado.jsx';
import BarraLateralPlanteles from "./BarraLateralPlanteles";
import PlantelesDeEquipos from "./PlantelesDeEquipos";
import BuscadorJugadores from "./BuscadorJugadores";

const Planteles = () => {

    const [plantelesDeEquiposActivo, setPlantelesDeEquiposActivo] = useState(false);
    const [buscadorJugadoresActivo, setBuscadorJugadoresActivo] = useState(false);

    useEffect(() => {
        setPlantelesDeEquiposActivo(true);
    }, []);


    return (
        <>
            <BarraNavegacionLogueado/>
            <div className="row ml-0 mr-0">
                <BarraLateralPlanteles setPlantelesDeEquiposActivo={setPlantelesDeEquiposActivo}
                                       setBuscadorJugadoresActivo={setBuscadorJugadoresActivo}/>
                {plantelesDeEquiposActivo ? <PlantelesDeEquipos/> : null}
                {buscadorJugadoresActivo ? <BuscadorJugadores/> : null}
            </div>
        </>
    );
};
export default Planteles;
