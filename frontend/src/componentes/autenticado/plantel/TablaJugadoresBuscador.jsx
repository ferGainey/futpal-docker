import React from 'react';
import $ from "jquery";
import 'bootstrap';
import CurrencyFormat from "react-currency-format";


const TablaJugadoresBuscador = ({jugadores}) => {

    function estaElJugadorPrestado(jugador) {
        return jugador.estadoJugador.nombre === 'PRESTADO';
    }

    return (
      <>
          <table id="tabla-jugadores-plantel" className="table table-dark overflow-auto mt-4">
            <thead>
                <tr>
                    <th id="encabezado-nombre-jugador" scope="col">Nombre</th>
                    <th id="encabezado-salario-jugador" scope="col">Salario</th>
                    <th id="encabezado-fecha-nacimiento-jugador" scope="col">Fecha nacimiento</th>
                    <th id="encabezado-equipo-actual-jugador" scope="col">Equipo actual</th>
                    <th id="encabezado-id-transfermarkt" scope="col">ID Transfermarkt</th>
                </tr>
            </thead>
            <tbody>
            {jugadores.map((jugadorFiltrado, indice) => {
                return <tr key={indice} className={`fila-jugadores ${estaElJugadorPrestado(jugadorFiltrado.jugador) ? 'bg-secondary' : ''}`}
                           data-tip='' data-for={estaElJugadorPrestado(jugadorFiltrado.jugador) ? 'tooltipEstadoJugador' : ''} >
                    <td className="columna-nombre-jugador">{jugadorFiltrado.jugador.nombre}</td>
                    <td className="columna-salario-jugador">
                        <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                        decimalScale={0} prefix={'$'} value={jugadorFiltrado.jugador.salario}
                                        displayType={"text"} required/>
                    </td>
                    <td className="columna-fecha-nacimiento-jugador">{jugadorFiltrado.jugador.fechaNacimiento}</td>
                    <td className="columna-equipo-jugador">{jugadorFiltrado.nombreEquipoActual}</td>
                    <td className="columna-id-transfermarkt">{jugadorFiltrado.jugador.idTransfermarkt}</td>
                    </tr>
                })}
            </tbody>
        </table>
      </>
    );
};
export default TablaJugadoresBuscador;
