import React, {useEffect, useState} from 'react';
import API from '../../../api';
import TablaJugadores from './TablaJugadores.jsx';
import AprobadorEquipo from './AprobadorEquipo.jsx';
import {useHistory} from "react-router-dom";
import $ from "jquery";

const PlantelesDeEquipos = () => {

    const history = useHistory();

    const [equiposAprobados, setEquiposAprobados] = useState([]);
    const [idEquipoElegido, setIdEquipoElegido] = useState();
    const [jugadores, setJugadores] = useState([]);
    const [permisoParaBorrarJugador, setPermisoParaBorrarJugador] = useState(false);

    useEffect(() => {
        let tokenUsuario = localStorage.getItem("usuario");
        if (tokenUsuario == null) {
            history.push("/");
        }
        else {
            obtenerEquiposAprobados();
            let rolesUsuarioLogueado = JSON.parse(localStorage.getItem("usuario")).roles;
            if (rolesUsuarioLogueado.includes("ROL_ADMINISTRADOR")) {
                setPermisoParaBorrarJugador(true);
            }
        }
    }, []);

    useEffect(() => {
        if (idEquipoElegido != null) {
            obtenerJugadores();
            equiposAprobados.forEach(equipo => {
                if (equipo.id === idEquipoElegido) {
                    $('#aprobador-equipo-equipos-pendiente-aprobacion').val("DEFAULT");
                }
            })
        }
        else {
            setJugadores([]);
        }
    }, [idEquipoElegido]);

    const obtenerEquiposAprobados = async () => {
        const respuesta = await servicioObtenerEquiposAprobados();
        if (respuesta != null && respuesta.status === 200) {
            setEquiposAprobados(respuesta.data);
        }
    }

    const servicioObtenerEquiposAprobados = async () => {
    return await API('/api/obtener-equipos-aprobados', {
        method: 'get',
      }).catch(error => {
        console.log(error.response);
      });   
    }

    const obtenerJugadores = async () => {
        const respuesta = await servicioObtenerJugadores();
        if (respuesta != null && respuesta.status === 200) {
            if (respuesta.data !== "" && respuesta.data != null) {
                setJugadores(respuesta.data);        
            }
        }
      }
    
    const servicioObtenerJugadores = async () => {
        return await API('/api/jugadores', {
            method: 'get',
            params: {equipoId: idEquipoElegido}
          }).catch(error => {
            console.log(error.response);
          });
      }

    return (
        <div className="letra-blanca col-9 col-md-10 col-xl-11 overflow-auto">
            <h3 className="ml-5 pt-2">
                Planteles
            </h3>
            <div className="ml-5">
            <div className="form-group">
                <label>Equipos disponibles</label>
                <select id="planteles-equipos-aprobados" className="form-control col-auto col-md-3" onChange={(opcion)=> setIdEquipoElegido(opcion.target.value)} defaultValue={'DEFAULT'} >
                    <option value="DEFAULT" disabled>Elija un equipo...</option>
                    {equiposAprobados.map((equipo, indice) => {
                        return <option key={indice} value={equipo.id}>{equipo.nombreEquipo}</option>
                    })}
                </select>
            </div>
            <div className="form-group">
                <AprobadorEquipo idEquipoElegido={idEquipoElegido} setIdEquipoElegido={setIdEquipoElegido} setEquiposAprobados={setEquiposAprobados}/>
            </div>
            </div>
                <TablaJugadores jugadores={jugadores} permisoParaAdministrarJugador={permisoParaBorrarJugador} setJugadores={setJugadores} equipoId={idEquipoElegido}/>
        </div>
    );
};
export default PlantelesDeEquipos;
