import React from 'react';
import $ from "jquery";
import 'bootstrap';
import CurrencyFormat from "react-currency-format";


const TablaJugadoresPrestados = ({jugadores}) => {

    return (
      <>
          <table id="tabla-jugadores-prestados" className="table table-dark">
            <thead>
                <tr>
                    <th id="encabezado-nombre-jugador" scope="col">Nombre</th>
                    <th id="encabezado-salario-jugador" scope="col">Salario</th>
                    <th id="encabezado-fecha-nacimiento-jugador" scope="col">Fecha nacimiento</th>
                    <th id="encabezado-equipo-actual" scope="col">Equipo actual</th>
                    <th id="encabezado-temporada-vuelta" scope="col">Temporada vuelta</th>
                    <th id="encabezado-periodo-vuelta" scope="col">Período vuelta</th>
                </tr>
            </thead>
            <tbody>
            {jugadores.map((jugadorPrestado, indice) => {
                return <tr  key={indice}>
                    <td className="columna-nombre-jugador">{jugadorPrestado.jugador.nombre}</td>
                    <td className="columna-salario-jugador">
                        <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                        decimalScale={0} prefix={'$'} value={jugadorPrestado.jugador.salario}
                                        displayType={"text"} required/>
                    </td>
                    <td className="columna-fecha-nacimiento-jugador">{jugadorPrestado.jugador.fechaNacimiento}</td>
                    <td className="columna-equipo-actual">{jugadorPrestado.nombreEquipoActual}</td>
                    <td className="columna-numero-temporada">{jugadorPrestado.numeroTemporada}</td>
                    <td className="columna-periodo">{jugadorPrestado.periodo.nombre}</td>
                    </tr>
                })}
            </tbody>
        </table>
      </>
    );
};
export default TablaJugadoresPrestados;
