import React, {useEffect, useState} from 'react';
import 'bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faEdit, faTrash} from '@fortawesome/free-solid-svg-icons';
import ServicioJugador from '../../../servicios/ServicioJugador';
import EditorJugador from './EditorJugador.jsx';
import CurrencyFormat from "react-currency-format";
import ReactTooltip from 'react-tooltip';


const TablaJugadores = ({jugadores, setJugadores, permisoParaAdministrarJugador, equipoId}) => {

    const [mostrarEditorJugador, setMostrarEditorJugador] = useState(false);
    const [jugadorSeleccionadoId, setJugadorSeleccionadoId] = useState();

    useEffect(() => {
        ReactTooltip.rebuild();
    });

    const borrarJugador = async (idJugador) => {
        await ServicioJugador.borrarJugador(idJugador);
        let respuestaObtenerJugadores = await ServicioJugador.obtenerJugadores(equipoId);
        setJugadores(respuestaObtenerJugadores.data);
    }

    const editarJugador = async (idJugador) => {
        setJugadorSeleccionadoId(idJugador);
        setMostrarEditorJugador(true);
    }

    function estaElJugadorPrestado(jugador) {
        return jugador.estadoJugador.nombre === 'PRESTADO';
    }

    return (
      <>
        {mostrarEditorJugador ? <EditorJugador equipoId={equipoId} jugadorId={jugadorSeleccionadoId} mostrar={mostrarEditorJugador} setMostrar={setMostrarEditorJugador} setJugadores={setJugadores}/> : null}
          <ReactTooltip id="tooltipEstadoJugador">A préstamo</ReactTooltip>
          <table id="tabla-jugadores-plantel" className="table table-dark overflow-auto">
            <thead>
                <tr>
                <th id="encabezado-nombre-jugador" scope="col">Nombre</th>
                <th id="encabezado-salario-jugador" scope="col">Salario</th>
                <th id="encabezado-valor-jugador" scope="col">Valor</th>
                <th id="encabezado-media-jugador" scope="col">Media</th>
                <th id="encabezado-fecha-nacimiento-jugador" scope="col">Fecha nacimiento</th>
                {permisoParaAdministrarJugador ?
                <th id="tabla-jugadores-encabezado-acciones" scope="col">Acciones</th>
                : null }
                </tr>
            </thead>
            <tbody>
            {jugadores.map((jugador) => {
                return <tr className={`fila-jugadores ${estaElJugadorPrestado(jugador) ? 'bg-secondary' : ''}`} key={jugador.id} data-tip='' data-for={estaElJugadorPrestado(jugador) ? 'tooltipEstadoJugador' : ''} >
                    <td className="columna-nombre-jugador">{jugador.nombre}</td>
                    <td className="columna-salario-jugador">
                        <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                        decimalScale={0} prefix={'$'} value={jugador.salario}
                                        displayType={"text"} required/>
                    </td>
                    <td className="columna-valor-jugador">
                        <CurrencyFormat thousandSeparator={'.'} decimalSeparator={','}
                                        decimalScale={0} prefix={'$'} value={jugador.valor}
                                        displayType={"text"} required/>
                    </td>
                    <td className="columna-media-jugador">{jugador.media}</td>
                    <td className="columna-fecha-nacimiento-jugador">{jugador.fechaNacimiento}</td>
                    {permisoParaAdministrarJugador ?
                        <td className="tabla-jugadores-encabezado-acciones d-flex justify-content-around">
                            <FontAwesomeIcon id="tabla-jugadores-accion-borrado-jugador" className="pointer" icon={faTrash} onClick={() => borrarJugador(jugador.id)} />
                            <FontAwesomeIcon id="tabla-jugadores-accion-edicion-jugador" className="pointer" icon={faEdit} onClick={() => editarJugador(jugador.id)} />
                        </td>
                        : null
                    }
                    </tr>
                })}
            </tbody>
        </table>
      </>
    );
};
export default TablaJugadores;
