import React, {useEffect, useState} from 'react';
import 'bootstrap';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ServicioJugador from '../../../servicios/ServicioJugador';
import CurrencyFormat from "react-currency-format";

const EditorJugador = ({setJugadores, jugadorId, mostrar, setMostrar, equipoId}) => {

    const [show, setShow] = useState(false);
    const [nombre, setNombre] = useState();
    const [fechaNacimiento, setFechaNacimiento] = useState();
    const [valor, setValor] = useState();
    const [salario, setSalario] = useState();
    const [media, setMedia] = useState();
    const [idTransfermarkt, setIdTransfermarkt] = useState();

    useEffect(() => {
        setShow(mostrar);
        cargarDatosJugador(jugadorId);  

    }, [mostrar]);

    const cargarDatosJugador = async (jugadorId) => {
        let respuestaDatosJugador = await ServicioJugador.obtenerDatosJugador(jugadorId);
        setNombre(respuestaDatosJugador.data.nombre);
        setFechaNacimiento(respuestaDatosJugador.data.fechaNacimiento);
        setValor(respuestaDatosJugador.data.valor);
        setSalario(respuestaDatosJugador.data.salario);
        setMedia(respuestaDatosJugador.data.media);
        setIdTransfermarkt(respuestaDatosJugador.data.idTransfermarkt);
    }

    const cerrarPopUp = () => {
        setShow(false);
        setMostrar(false);
    }

    const onSubmit =  async () => {
        let requestEditarJugador = {
          nombre: nombre,
          fechaNacimiento: fechaNacimiento,
          valor: valor,
          salario: salario,
          media: media,
          jugadorId: jugadorId,
          idTransfermarkt: idTransfermarkt
        };
        await ServicioJugador.editarJugador(requestEditarJugador);
        setShow(false);
        setMostrar(false);
        let respuestaObtenerJugadores = await ServicioJugador.obtenerJugadores(equipoId);
        setJugadores(respuestaObtenerJugadores.data);
    }

    return (
      <>
        <Modal
          show={show}
          onHide={cerrarPopUp}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Editar jugador</Modal.Title>
          </Modal.Header>
              <Modal.Body>
                  <form>
                      <div className="form-group">
                      <label>Nombre:</label>
                        <input id="editor-jugador-input-nombre" value={nombre} className="form-control" type="text" onChange={e => setNombre(e.target.value)} required/>
                      </div>
                      <div className="form-group">
                      <label>Fecha nacimiento:</label>
                        <input id="editor-jugador-input-fecha-nacimiento" value={fechaNacimiento} className="form-control" type="date" onChange={e => setFechaNacimiento(e.target.value)} required/>
                      </div>
                      <div className="form-group">
                          <label>Valor:</label>
                          <CurrencyFormat id="editor-jugador-input-valor" value={valor} className="form-control" thousandSeparator={'.'}
                                          decimalSeparator={','} decimalScale={0} prefix={'$'} onValueChange={value => setValor(value.floatValue)} required/>
                      </div>
                      <div className="form-group">
                          <label>Salario:</label>
                          <CurrencyFormat id="editor-jugador-input-salario" value={salario} className="form-control" thousandSeparator={'.'}
                                          decimalSeparator={','} decimalScale={0} prefix={'$'} onValueChange={value => setSalario(value.floatValue)} required/>
                      </div>
                      <div className="form-group">
                      <label>Media:</label>
                        <input id="editor-jugador-input-media" value={media} className="form-control" type="number" onChange={e => setMedia(e.target.value)} required/>
                      </div>
                      <div className="form-group">
                          <label>ID Transfermarkt:</label>
                          <input id="editor-jugador-input-id-transfermarkt" value={idTransfermarkt} className="form-control" type="number" onChange={e => setIdTransfermarkt(e.target.value)} required/>
                      </div>
                </form>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={cerrarPopUp}>
                  Cancelar
                </Button>
                <Button id="editor-jugador-boton-enviar" variant="primary" type="submit" onClick={onSubmit}>Editar</Button>
              </Modal.Footer>
        </Modal>
      </>
    );
};
export default EditorJugador;
