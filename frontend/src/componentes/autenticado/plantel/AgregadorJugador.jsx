import React, {useEffect, useState} from 'react';
import $ from "jquery";
import 'bootstrap';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import CurrencyFormat from 'react-currency-format';
import ServicioJugador from "../../../servicios/ServicioJugador";

const AgregadorJugador = ({equipoId, mostrar, setMostrar}) => {

    const [show, setShow] = useState(false);
    const [nombre, setNombre] = useState();
    const [fechaNacimiento, setFechaNacimiento] = useState();
    const [valor, setValor] = useState();
    const [salario, setSalario] = useState();
    const [media, setMedia] = useState();
    const [idTransfermarkt, setIdTransfermarkt] = useState();

    useEffect(() => {
        setShow(mostrar);
    }, [mostrar]);

    const cerrarPopUp = () => {
        setShow(false);
        setMostrar(false);
    }

    const onSubmit = (e) => {
        e.preventDefault();
        $('#agregador-jugador-boton-agregar').prop('disabled', true);
        let requestAgregarJugador = {
          nombre: nombre,
          fechaNacimiento: fechaNacimiento,
          valor: valor,
          salario: salario,
          media: media,
          equipoId: equipoId,
          idTransfermarkt: idTransfermarkt
        };
        ServicioJugador.agregarJugador(requestAgregarJugador).then(() => {
            cerrarPopUp();
            $('#agregador-jugador-boton-agregar').prop('disabled', false);
        }, () => {
            cerrarPopUp();
            $('#agregador-jugador-boton-agregar').prop('disabled', false);
        });
    }

    return (
      <>
        <Modal
          show={show}
          onHide={cerrarPopUp}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Agregar jugador al equipo</Modal.Title>
          </Modal.Header>
            <form onSubmit={onSubmit}>
            <Modal.Body>
              <div className="form-group">
              <label>Nombre:</label>
                <input id="agregador-jugador-plantel-input-nombre" className="form-control" type="text" onChange={e => setNombre(e.target.value)} required/>
              </div>
              <div className="form-group">
              <label>Fecha nacimiento:</label>
                <input id="agregador-jugador-plantel-input-fecha-nacimiento" className="form-control" type="date" onChange={e => setFechaNacimiento(e.target.value)} required/>
              </div>
              <div className="form-group">
              <label>Valor:</label>
                <CurrencyFormat id="agregador-jugador-plantel-input-valor" className="form-control" thousandSeparator={'.'}
                                decimalSeparator={','} decimalScale={0} prefix={'$'} onValueChange={value => setValor(value.floatValue)} required/>
              </div>
              <div className="form-group">
              <label>Salario:</label>
                  <CurrencyFormat id="agregador-jugador-plantel-input-salario" className="form-control" thousandSeparator={'.'}
                                  decimalSeparator={','} decimalScale={0} prefix={'$'} onValueChange={value => setSalario(value.floatValue)} required/>
              </div>
              <div className="form-group">
              <label>Media:</label>
                <input id="agregador-jugador-plantel-input-media" className="form-control" type="number" onChange={e => setMedia(e.target.value)} required/>
              </div>
              <div className="form-group">
                  <label>ID Transfermarkt:</label>
                  <input id="agregador-jugador-plantel-input-id_transfermarkt" className="form-control" type="number" onChange={e => setIdTransfermarkt(e.target.value)} required/>
              </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={cerrarPopUp}>
              Cancelar
            </Button>
            <Button id="agregador-jugador-boton-agregar" variant="primary" type="submit">Agregar</Button>
          </Modal.Footer>
        </form>
        </Modal>
      </>
    );
};
export default AgregadorJugador;
