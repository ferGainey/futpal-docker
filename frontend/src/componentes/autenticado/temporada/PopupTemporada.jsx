import React, {useEffect, useState} from 'react';
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import ServicioTemporada from "../../../servicios/ServicioTemporada";


const PopupTemporada = ({mostrar, setMostrar, temporada}) => {

    const [show, setShow] = useState(false);

    useEffect(() => {
        setShow(mostrar);
    }, [mostrar]);

    const cerrarPopUp = () => {
        setShow(false);
        setMostrar(false);
    }

    const avanzarPeriodo = () => {
        ServicioTemporada.avanzarPeriodo().then(() => {
            cerrarPopUp();
        });
    }

    return (
        <>
            <Modal
                show={show}
                onHide={cerrarPopUp}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Avanzar temporada</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Período actual:</p>
                    <p className="font-weight-bold">{temporada.periodoTemporada.nombre} Temp. {temporada.numero}</p><br/>
                    <p className="font-italic">Si selecciona avanzar no se podrá deshacer la acción.</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" onClick={cerrarPopUp}>Cancelar</Button>
                    <Button id="boton-avanzar-periodo" variant="danger" onClick={avanzarPeriodo}>AVANZAR PERÍODO</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};
export default PopupTemporada;
