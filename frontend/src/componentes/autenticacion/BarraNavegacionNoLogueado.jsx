import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons/faBars";


const BarraNavegacionNoLogueado = () => {

  return (
<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
  <div className="navbar-header">
    <div className="row">
      <div className="col-12">
        <a className="navbar-brand" href="/">Futpal</a>
        <button type="button" className="btn navbar-toggle d-block d-lg-none bg-dark" data-toggle="collapse" data-target=".navbar-collapse">
          <FontAwesomeIcon className="letra-blanca" icon={faBars}/>
        </button>
      </div>
    </div>
  </div>
  <div className="navbar-collapse collapse" id="navBarNoLogueado">
    <div className="navbar-nav">
      <a id="boton-signup" className="nav-item nav-link" href="/front/registro">Registro</a>
    </div>
    <div className="navbar-nav">
      <a id="boton-navbar-login" className="nav-item nav-link" href="/front/ingreso">Ingresar</a>
    </div>
  </div>
</nav>
          );
};
export default BarraNavegacionNoLogueado;
