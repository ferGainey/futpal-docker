import React, {useState} from 'react';
import API from '../../api';
import $ from "jquery";
import BarraNavegacionNoLogueado from './BarraNavegacionNoLogueado.jsx';

const Registro = () => {
  const [nombre, setNombre] = useState();
  const [apellido, setApellido] = useState();
  const [alias, setAlias] = useState();
  const [mail, setMail] = useState();
  const [contrasenia, setContrasenia] = useState();

  const onSubmit =  async (evento) => {
    evento.preventDefault()
    let nuevoUsuario = {
      nombre: nombre,
      apellido: apellido,
      alias: alias,
      mail: mail,
      contrasenia: contrasenia
    };

    await cargarUsuario(nuevoUsuario);

  }

  const cargarUsuario = async (usuario) => {
      $('#boton-crear-usuario').prop('disabled', true);
      return await API('/api/registrar-usuario', {
        method: 'post',
        data: usuario
      }).then((respuesta) => {
          $('#boton-crear-usuario').prop('disabled', false);
          mostrarRespuestaGuardado(respuesta);
      },
      error => {
          $('#boton-crear-usuario').prop('disabled', false);
          console.log(error.response);
          mostrarRespuestaErrorAlIntentarAlta(error.response);
      }
      );
  }

  const mostrarRespuestaErrorAlIntentarAlta = (respuesta) => {
    let alerta = $('#alerta-error-creando-usuario');
    let textoRespuestaError = $('#texto-error-alta-usuario');
    if(respuesta != null) {
      textoRespuestaError.text(respuesta.data);
    }
    else {
        textoRespuestaError.text("No se pudo establecer la conexión con los servidores. Vuelva a intentar nuevamente.");
    }
    alerta.show();
  }

  const mostrarRespuestaGuardado = (respuesta) => {
    if(respuesta != null && respuesta.status === 200) {
        $('#alerta-usuario-creado').show();
    }
  }

  return (
        <div>
            <BarraNavegacionNoLogueado />
            <div className="letra-blanca">
                <h3 className="titulo-signup">Registro</h3>
                <div className="container">

                    <form onSubmit={(e) => onSubmit(e)}>
                      <div className="form-group">
                        <label>Alias</label>
                        <input id="campo-alias" className="form-control" type="text" onChange={e => setAlias(e.target.value)} required/>
                      </div>
                      <div className="form-group">
                        <label>Mail</label>
                        <input id="campo-mail" className="form-control" type="text" onChange={e => setMail(e.target.value)} required/>
                      </div>
                      <div className="form-group">
                        <label>Nombre</label>
                        <input id="campo-nombre" className="form-control" type="text" onChange={e => setNombre(e.target.value)} required/>
                      </div>
                      <div className="form-group">
                        <label>Apellido</label>
                        <input id="campo-apellido" className="form-control" type="text" onChange={e => setApellido(e.target.value)} required/>
                      </div>
                      <div className="form-group">
                        <label>Contraseña</label>
                        <input id="campo-contrasenia" className="form-control" type="password" onChange={e => setContrasenia(e.target.value)} required/>
                      </div>

                        <button id="boton-crear-usuario" className="btn btn-success" type="submit">Crear usuario</button>
                    </form>
                    <div id="alerta-usuario-creado" className="alert alert-success alert-dismissible collapse alerta-futpal" role="alert">
                        Usuario creado con éxito
                      <button type="button" className="close" onClick={() => $('#alerta-usuario-creado').hide()} aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div id="alerta-error-creando-usuario" className="alert alert-danger alert-dismissible collapse alerta-futpal" role="alert">
                        <p id="texto-error-alta-usuario">Hubo un error intentando crear el usuario</p>
                        <button type="button" className="close" onClick={() => $('#alerta-error-creando-usuario').hide()} aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                </div>
            </div>
          </div>
          );
};
export default Registro;
