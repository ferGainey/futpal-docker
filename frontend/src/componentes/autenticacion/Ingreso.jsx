import React, {useState} from 'react';
import {useHistory} from "react-router-dom";
import API from '../../api';
import $ from "jquery";
import BarraNavegacionNoLogueado from './BarraNavegacionNoLogueado.jsx';

const Ingreso = () => {
  const [alias, setAlias] = useState();
  const [contrasenia, setContrasenia] = useState();
  const [mensajeError, setMensajeError] = useState();
  const history = useHistory();

  const onSubmit =  async (event) => {
    event.preventDefault()
    let requestLogin = {
      alias: alias,
      contrasenia: contrasenia
    };
    await loguearse(requestLogin);
  }

  const loguearse = async (requestLogin) => {
      $('#boton-login').prop('disabled', true);
      return await API('/api/ingresar', {
        method: 'post',
        data: requestLogin
      }).then((respuesta) => {
              $('#boton-login').prop('disabled', false);
              redirigirPantallaInicialLogueado(respuesta);
          },
          error => {
              $('#boton-login').prop('disabled', false);
              console.log(error.response);
              console.log(error);
              mostrarRespuestaErrorAlIntentarLoguearse(error);
            }
      );
  }

  const mostrarRespuestaErrorAlIntentarLoguearse = (error) => {
      if (error.response != null && error.response.status === 401) {
          setMensajeError("Las credenciales ingresadas no son válidas.");
      }
      else {
          setMensajeError("No se pudo establecer la conexión con los servidores. Vuelva a intentar nuevamente.");
      }
      let alerta = $('#alerta-error-login-credenciales-invalidas');
    alerta.show();
  }

  const redirigirPantallaInicialLogueado = (respuesta) => {
    if (respuesta.data.accessToken) {
      localStorage.setItem("usuario", JSON.stringify(respuesta.data));
      history.push("/front/pantalla-inicial-usuario");
    }
  }

  return (
          <div>
            <BarraNavegacionNoLogueado />
            <div className="letra-blanca">
                <h3 className="titulo-login">Ingresar</h3>
                <div className="container">
                  <form onSubmit={(e) => onSubmit(e)}>
                    <div className="form-group">
                      <label>Alias</label>
                      <input id="campo-alias-login" className="form-control" type="text" onChange={e => setAlias(e.target.value)} required/>
                    </div>
                    <div className="form-group">
                      <label>Contraseña</label>
                      <input id="campo-contrasenia-login" className="form-control" type="password" onChange={e => setContrasenia(e.target.value)} required/>
                    </div>
                      <button id="boton-login" className="btn btn-success" type="submit">Ingresar</button>
                  </form>
                  <div id="alerta-error-login-credenciales-invalidas" className="alert alert-danger alert-dismissible collapse alerta-futpal" role="alert">
                      <p id="texto-error-login-usuario">{mensajeError}</p>
                      <button type="button" className="close" onClick={() => $('#alerta-error-login-credenciales-invalidas').hide()} aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                </div>
            </div>
          </div>
          );
};
export default Ingreso;
