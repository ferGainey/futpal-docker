import API from '../api';

const obtenerEquiposAprobados = async () => {
    return await API('/api/obtener-equipos-aprobados', {
        method: 'get',
        }).catch(error => {
        console.log(error.response);
    });  
}

const obtenerDatosEquipo = async (idUsuario) => {
    return await API('/api/datos-equipo', {
        method: 'get',
        params: {idUsuario: idUsuario}
    }).catch(error => {
        console.log(error.response);
    });
}

export default {obtenerEquiposAprobados, obtenerDatosEquipo};
