import API from '../api';

const cargarTransferencia = async (transferencia) => {
    return await API('/api/transferencia', {
        method: 'post',
        data: transferencia
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const obtenerMisTransferencias = async () => {
    return await API('/api/mis-transferencias', {
        method: 'get'
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const rechazarTransferencia = async (idTransferencia) => {
    return await API('/api/transferencia/rechazar', {
        method: 'patch',
        data: idTransferencia
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const confirmarTransferencia = async (idTransferencia) => {
    return await API('/api/transferencia/confirmar', {
        method: 'patch',
        data: idTransferencia
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const obtenerTodasLasTransferenciasConfirmadas = async (pagina) => {
    return await API('/api/todas-las-transferencias-confirmadas', {
        method: 'get',
        params: {pagina: pagina}
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const cambiarEstadoMercado = async (mercadoActivo) => {
    return await API('/api/mercado/estado', {
        method: 'patch',
        data: JSON.stringify(mercadoActivo)
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const obtenerEstadoMercado = async () => {
    return await API('/api/mercado/estado', {
        method: 'get'
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}


export default {cargarTransferencia, obtenerMisTransferencias, rechazarTransferencia, confirmarTransferencia, obtenerTodasLasTransferenciasConfirmadas
                ,cambiarEstadoMercado, obtenerEstadoMercado};
