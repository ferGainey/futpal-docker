import API from '../api';

const cargarBalance = async (balance) => {
    return await API('/api/balance/cargar', {
        method: 'post',
        data: balance
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const cargarBalancesParaLiga = async (balances) => {
    return await API('/api/balance/cargar-para-liga', {
        method: 'post',
        data: balances
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const obtenerBalanceTodosLosEquipos = async (numeroTemporada) => {
    return await API('/api/obtener-balances-todos-los-equipos', {
        method: 'get',
        params: {numeroTemporada: numeroTemporada}
    }).catch(error => {
        console.log(error.response);
    });
}

const obtenerDetalleBalanceUnEquipo = async (numeroTemporada, equipoId) => {
    return await API('/api/obtener-detalle-balances-un-equipo', {
        method: 'get',
        params: {numeroTemporada: numeroTemporada, equipoId: equipoId}
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const obtenerBalancesUnEquipoEnUnaTemporada = async (numeroTemporada, equipoId) => {
    return await API('/api/obtener-balances-un-equipo', {
        method: 'get',
        params: {numeroTemporada: numeroTemporada, equipoId: equipoId}
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const obtenerSalarios = async (ligaId) => {
    return await API('/api/obtener-salarios-liga', {
        method: 'get',
        params: {idLiga: ligaId}
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

export default {cargarBalance, cargarBalancesParaLiga, obtenerBalanceTodosLosEquipos, obtenerDetalleBalanceUnEquipo, obtenerBalancesUnEquipoEnUnaTemporada, obtenerSalarios};
