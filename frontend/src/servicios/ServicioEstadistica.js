import API from '../api';


const cargarEstadisticas = async (requestCargarEstadistica) => {
    return await API('/api/actualizar-estadistica', {
        method: 'post',
        data: requestCargarEstadistica
        }).catch(error => {
        console.log(error.response);
    });  
}

const obtenerEstadisticasPartido = async (partidoId) => {
    return await API('/api/obtener-estadisticas-partido', {
        method: 'get',
        params: {partidoId: partidoId}
        }).catch(error => {
        console.log(error.response);
    });  
}

const obtenerEstadisticasLiga = async (ligaId) => {
    return await API('/api/obtener-estadisticas-liga', {
        method: 'get',
        params: {ligaId: ligaId}
        }).catch(error => {
        console.log(error.response);
    });  
}

export default {cargarEstadisticas, obtenerEstadisticasPartido, obtenerEstadisticasLiga};
