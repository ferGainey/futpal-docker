import API from '../api';

const borrarJugador = async (jugadorId) => {
    let requestBorrarJugador = {
        jugadorId: jugadorId
    }
    return await API('/api/borrar-jugador', {
        method: 'delete',
        data: requestBorrarJugador
        }).catch(error => {
        console.log(error.response);
    });  
}

const obtenerJugadores = async (equipoId) => {
    return await API('/api/jugadores', {
        method: 'get',
        params: {equipoId: equipoId}
      }).catch(error => {
        console.log(error.response);
      });
}

const obtenerDatosJugador = async (jugadorId) => {
    return await API('/api/datos-jugador', {
        method: 'get',
        params: {jugadorId: jugadorId}
      }).catch(error => {
        console.log(error.response);
      });
}

const editarJugador = async (requestEditarJugador) => {
    return await API('/api/editar-jugador', {
        method: 'put',
        data: requestEditarJugador
        }).catch(error => {
        console.log(error.response);
    });  
}

const agregarJugador = async (jugador) => {
    return await API('/api/agregar-jugador', {
        method: 'post',
        data: jugador
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const obtenerJugadoresPrestados = async (equipoId) => {
    return await API('/api/jugadoresPrestados', {
        method: 'get',
        params: {equipoId: equipoId}
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const obtenerJugadoresDuenioDePase = async (equipoId) => {
    return await API('/api/jugadoresDuenioDePase', {
        method: 'get',
        params: {equipoId: equipoId}
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

const obtenerJugadoresFiltrados = async (transfermarktId, nombre) => {
    return await API('/api/jugadoresFiltrados', {
        method: 'get',
        params: {transfermarktId: transfermarktId,
                 nombre: nombre}
    }).catch(error => {
        console.log(error.response);
        throw error;
    });
}

export default {borrarJugador, obtenerJugadores, obtenerDatosJugador, editarJugador, agregarJugador,
    obtenerJugadoresPrestados, obtenerJugadoresDuenioDePase, obtenerJugadoresFiltrados};
