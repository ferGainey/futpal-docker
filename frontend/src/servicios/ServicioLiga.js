import API from '../api';

const obtenerPartidos = async (idLiga) => {
    return await API('/api/obtener-partidos-liga', {
        method: 'get',
        params: {idLiga: idLiga}
    }).catch(error => {
        console.log(error.response);
    });
}

//ToDo: reemplazar por estas funciones del servicio en los lugares donde se haga el llamado sin usar este servicio
const obtenerEquipos = async (idLiga) => {
    return await API('/api/obtener-equipos-liga', {
        method: 'get',
        params: {idLiga: idLiga}
    }).catch(error => {
        console.log(error.response);
    });
}

const obtenerLigasDisponibles = async () => {
    return await API('/api/obtener-ligas-disponibles', {
        method: 'get',
    }).catch(error => {
        console.log(error.response);
    });
}

export default {obtenerPartidos, obtenerEquipos, obtenerLigasDisponibles};
