import API from '../api';

const obtenerTemporada = async () => {
    return await API('/api/temporada', {
        method: 'get'
        }).catch(error => {
        console.log(error.response);
    });  
}

const avanzarPeriodo = async () => {
    return await API('/api/temporada/avanzar', {
        method: 'post'
    }).catch(error => {
        console.log(error.response);
    });
}

export default {obtenerTemporada, avanzarPeriodo};
