sed -i "s/REACT_APP_URL_BACKEND/$REACT_APP_URL_BACKEND/g" /usr/share/nginx/html/index.html
envsubst '\$PORT' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'