# **Futpal**

La gestión de las competencias es un trabajo no trivial que muchas veces, especialmente en las competencias amateur, son realizadas sin apoyo tecnológico o bien asistiéndose meramente con una planilla de cálculo.
Haciendo foco en los videojuegos de fútbol, los principales juegos, PES y FIFA, permiten jugar en línea contra otros jugadores, pero no proveen un modo que permita la gestión de una liga multijugador.

Futpal es un software que viene a solucionar esta problemática. Fue creado para facilitar, mediante un sitio web, la gestión de una liga virtual de manera externa a los videojuegos de fútbol.
<br>
<br>
## **Guía de despliegue**
Uno de los objetivos del proyecto Futpal fue facilitar el proceso de despliegue haciendo que la configuración del mismo sea breve y sencilla. Por este motivo se decidió utilizar Docker y Docker Compose.
La aplicación puede ser desplegada tanto en ambiente local, como en el servidor de integración continua (con el propósito de ejecutar pruebas automatizadas) y así también en un ambiente productivo. Se pasará a explicar cómo proceder para cada uno de los casos.
<br>
### **Despliegue en ambiente local**
Para desplegar la aplicación localmente o ejecutar las pruebas automatizadas de manera análoga al servidor de integración continua (pero en el ambiente local), se deben tener instalados Docker y Docker Compose.
1. Instalar Docker siguiendo la guía de instalación disponible en la documentación oficial.
2. Instalar Docker Compose siguiendo la guía de instalación disponible en la documentación oficial.
3. Ubicarse en la raíz del proyecto y según la acción deseada seguir los pasos de la opción a o b.
    - Ejecución de la aplicación:
        1. Ejecutar el comando docker-compose -f docker-compose-local.yml up
        2. Luego se podrá acceder desde localhost:3080 a la aplicación
    - Ejecución de pruebas:
        1. Ejecutar el comando docker-compose -f docker-compose-ci.yml run --rm maven-test sh app/correr-tests.sh
        2. Se ejecutarán las pruebas localmente de una manera muy similar a la ejecución en el servidor de integración continua.
4. Asignar manualmente en la tabla usuario_roles el rol de administrador al usuario que vaya a cumplir dicho rol.
<br>

### **Despliegue en servidor de integración continua**
En el servidor de integración continua se despliega la aplicación únicamente para propósitos de pruebas automatizadas en el marco de una de las etapas del pipeline.
Las instrucciones de esto se encuentran ya definidas en el gitlab-ci.yml, por lo tanto:
- Si se usa a GitLab como servidor de integración continua lo único que hay que realizar es agregar las variables presentes en el gitlab-ci-yml, junto con sus valores correspondientes, dentro de la configuración del proyecto de GitLab.
- En caso de utilizar otra herramienta, además de agregar las variables, se deberá revisar en su documentación si se debe realizar alguna otra acción.
<br>

### **Despliegue en producción**
Los pasos a seguir para desplegar la aplicación en un ambiente productivo pueden diferir según la plataforma que se vaya a utilizar para alojarla

#### **Con Heroku**
1. Crearse una cuenta en Heroku (omitir si ya se posee una)
2. Crear en la plataforma una aplicación para el backend y otra para el frontend siguiendo la documentación.
3. Asociar una base de datos a la aplicación del backend. Heroku provee una forma simplificada de realizarlo.
4. En este paso ya se posee tanto el nombre de las aplicaciones como el API Token de Heroku, por lo cual se procede a agregar esas variables al servidor de integración continua. Las variables en cuestión son HEROKU_APP_BACKEND_PROD, HEROKU_APP_FRONTEND_PROD y HEROKU_API_KEY. La estrategia elegida para el despliegue fue la del envío a Heroku de una imagen ya creada. Para más información se puede recurrir a la documentación oficial.
5. Agregar las variables de entorno en Heroku. En el caso del frontend es necesario configurar la dirección del backend en una variable llamada REACT_APP_URL_BACKEND. Mientras que para el backend se necesitan configurar las siguientes (se pueden observar en el application.properties):
    - JDBC_DATABASE_URL: es la url de la base de datos que se utilice.
    - JDBC_DATABASE_USERNAME: usuario de la base de datos.
    - JDBC_DATABASE_PASSWORD: contraseña de la base de datos.
    - JWT_SECRET_KEY: es la firma que utilizará el algoritmo para encodear y desencodear el token.
    - JWT_EXPIRACION_MS: es el tiempo en milisegundos de validez del token desde su creación.
6. Habiendo realizado todos los pasos anteriores, al finalizar la siguiente ejecución exitosa del pipeline de integración continua se va a encontrar ya desplegada la aplicación.
7. Para poder operar correctamente la aplicación, es necesario asignarle al usuario deseado el rol de administrador. Esto debe realizarse manualmente desde la tabla _usuario_roles_.

#### **Con otras plataformas**
Para este caso no se podría dar una serie de pasos concreta, pero sí hay ciertas acciones en común que deberán ser tenidas en cuenta.
1. Elegir una plataforma que soporte el despliegue con Docker.
2. Modificar la lógica de las etapas deploy-staging y deploy-prod del pipeline para introducir las instrucciones específicas de la plataforma elegida.
3. Configurar las variables de entorno en la plataforma elegida. 
4. Asignar el rol de administrador en la tabla usuario_roles al usuario que vaya a cumplir ese rol.



